<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>

<?php include('header.php'); ?>
    <!-- CSS -->
    <link rel="stylesheet" href="css/service.css" type="text/css">
    <!-- JS -->

    <div id="content" class="content service-page">
        <div class="container">
            <div class="row">
                <div class="col-md-11 col-md-offset-1">
                    <h1 class="heading-title">ข้อมูลส่วนตัว</h1>

                    <div class="customer-info update-profile">

                        <img src="../images/temp5/i_fail.png" alt="" class="i-update">
                        <p class="service-title">บันทึกข้อมูลไม่สำเร็จ</p>


                        <a href="" class="btn btn-submit">กลับหน้าข้อมูลส่วนตัว</a>

                    </div>

                </div>
            </div>


        </div>
    </div>


<?php include('footer.php'); ?>
