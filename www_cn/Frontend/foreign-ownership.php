<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

$page = 'foreign-ownership';
$page_index = 0;
$img = '';
?>
<?php
include('header.php');
elementMetaTitleDisTag($page);
?>
<!-- CSS -->
<link rel="stylesheet" href="<?= file_path('css/foreign-ownership.css') ?>" type="text/css">

<!-- JS -->

<div id="content" class="content foreign-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="">
                    
                    <?php
                    $SEO = getSEOUrl($actual_link);
                    if($actual_link == @$SEO->url_page){
                        echo '<h1 class="heading-title">'.$SEO->h1.'<h1>';
                    }else{
                     echo '<h1>由外籍人士持有的房地产</h1>'; 
                 }
                 ?>

                 <h2>“外籍人士”的定义</h2>

                 <p class="mb5"><strong>“外籍人士”</strong> 或 <strong>“外侨”</strong> 指的是没有泰国国籍的人或者依法被视为外籍人士的法人（外籍人士持股超过49%</p>
                 <p>所持有的<strong>“房地产”</strong>分为土地以及套房
                 关于持股模式分为所有权以及租赁</p>

                 <div class="foreign-block">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="topic-title-block">
                                <span class="topic-icon tpi1"></span><span class="topic-no">1</span>
                                <p class="foreign-title"><span>Real estate</span><br>
                                acquisition - Freehold</p>
                            </div>
                            <div class="">
                                <p>外籍人士获得土地以及建筑物的所有权受到土地法以及其他相关法规的限制，即<p>
                                  <p>  •   通过投资 </p>
                                   <p> 根据土地法规定，前来投资金额不少于4000万泰铢的外籍人士有权拥有为个人或家庭所用的面积不超过1莱的土地所有权，收购土地需要获得部长的批准，以上投资必须符合以下条件和标准：
                                </p>
                                <ul class="foreign-list">
                                    <li><i></i>The type of business which the foreigner invests must be economically and socially benefits the country or invest in the business which is declared under the laws related to the investment promotion by the Board of Investment.</li>
                                    <li><i></i>The period of maintaining the investment must not be less than three years.</li>
                                    <li><i></i>The land that the foreigner may acquire shall be within the locality of Bangkok Metropolitan Administration, the City of Pattaya, Municipality, or the zone designated to be the residential area under the law related to city planning.</li>
                                </ul>
                                <p>For the foreigner married to a Thai national who wishes to purchase real estate in Thailand, the foreigner and his/her Thai spouse are required to provide written consents to the officer that the money</p>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="content-img">
                                <img src="<?= file_path('images/foreign/fr_1.jpg') ?>" alt="">
                            </div>
                            <div class="foreigh-col-2">
                                <p>若泰国公民有外籍配偶并打算购买土地，夫妻双方必须同时向主管部门以书面形式确认所有用于购买土地的钱为私人财产或为拥有泰籍国籍的个人财产而非夫妻共同财产。在此，以上房地产的所有权必须视为泰籍配偶的私人财产。</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="foreign-block">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="content-img">
                                <img src="<?= file_path('images/foreign/fr_2.jpg') ?>" alt="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="topic-title-block fr2">
                                <span class="topic-icon tpi2"></span><span class="topic-no">2</span>
                                <p class="foreign-title"><span>Ownership</span><br>
                                of Condominium</p>
                            </div>
                            <div class="">
                                <p>In accordance with Condominium Act B.E 2522 any foreigner can buy and own a condominium within the foreign ownership quota which is 49% of the total size area of the condominium building.</p>

                                <p>The foreigner will be able to acquire a condominium unit by inheritance when the foreign ownership of such condominium does not exceed the foreign quota. However, if such condominium already has foreign ownership exceeding the quota, the foreigner who acquires the condominium will not be able to own the unit and must sell the unit within 1 year after the date of acquisition by inheritance, otherwise, the foreigner shall be forced to dispose or sell by auction and will be entitled to earn benefit from the selling after the deduction of all debts (if any).</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="foreign-block">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="topic-title-block fr3">
                                <span class="topic-icon tpi3"></span><span class="topic-no">3</span>
                                <p class="foreign-title"><span>Leasehold of</span><br>
                                Real Estate Property</p>
                            </div>
                            <div class="">
                                <p>Short-term lease refers to the lease for a period of not more than 3 years.  There is no requirement to register the lease with the Land Department. However, written lease agreement is important in order to be legally enforceable.</p>

                                <p>Long-term lease granted for a lease period of over 3 years. This type of lease must be done in written and register with the Land Department in order for it to be enforceable, otherwise, the lease shall have legal binding between the parties for 3- year period only. The long-term lease can also refer to the lease of land for commercial and industrial purposes pursuant to the Commercial and Industrial Leasing Act B.E. 2542 which allows the maximum lease term for up to 30 years but not over 50 years with possibility to renew for another 30 years but not over 50 years for the type of commercial and industrial granted by laws.</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="content-img">
                                <img src="<?= file_path('images/foreign/fr_3.jpg') ?>" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>




<?php include('footer.php'); ?>
