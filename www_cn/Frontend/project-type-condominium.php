<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';
require_once 'include/help/hex2rgba.php';

if(empty($_GET['zone_id'])){
    $project_ids = getProjectIDByProductView(3);
}else{
   $project_ids = getProjectIDByProductViewZone(3,$_GET['zone_id']);
} 
Helper::sortByPriceReturnAssoc($project_ids,'asc');

//$lang = !empty($_GET['lang']) ? $_GET['lang'] : 'th';
$page = 'condo-type';
$page_index = 8;
?>
<?php
$lang = !empty($_GET['lang']) ? $_GET['lang'] : 'th';
$swap_lang = $lang == 'th' ? 'th' : 'th';
?>


<?php
include('header.php');
//elementMetaTitleDisTag($page,$page_index);
?>
<!-- JS -->
<link rel="stylesheet" href="<?= file_path('css/project-type-condominium2.css') ?>" type="text/css">
<link rel="stylesheet" href="<?= file_path('css/video.css') ?>" type="text/css">

<script src="<?= file_path('js/project-type-condominium.js') ?>"></script>

<div class="page-banner page-scroll">
    <span id="scrollNext" class="i-scroll-next"></span>

    <div id="bannerSlide" class="owl-carousel">
        <?php
        $countProjectZone = 0;
        foreach ($project_ids as $i => $pro_sub) {
            $project_id = $pro_sub['project_id'];
            if(empty($_GET['zone_id'])){
                $project = getProjectByID($project_id);
            }else{
                $project = getProjectByIDZone_id($project_id,$_GET['zone_id']);
            }
            if (empty($_GET['zone_id']) || (!empty($_GET['zone_id']) && $_GET['zone_id'] == $project->zone_id)) {
                $countProjectZone++;
            }
        }
        ?>

        <?php
        renderBannerPageList(8);
        ?>

    </div>
</div>

<?php
renderHiddenLeadImage(8,'bannerSlide');
renderActivityResponsive();
?>
<?php  

$SEO = getSEOUrl($actual_link);
if($actual_link == @$SEO->url_page){
  $title_home_en = $SEO->h1;
  $title_home_th = $SEO->h1;
}else{
    $zones = getProjectViewZones(3);
    if(!empty($_GET['zone_id'])){
        foreach($zones as $zone) {
            if($_GET['zone_id'] == $zone->zone_id){
             $zone_names = $zone->zone_name_th;
         }
     }
 } 
 if(isset($_GET['zone_id'])){
    $title_home_th = "公寓项目 : ".$zone_names;
  $title_home_en = "Condominium Project : ".$zone_names; 
}else{
   $title_home_th = "LH的所有公寓项目";
   $title_home_en = "All condominium Project by LH";
}

}
?>


<div id="content" class="content project-type-page">
    <div class="container">
        <h1 style="cursor: pointer" onclick="location.href='<?= $router->generate('condominium-list') ?>'">
            <?= Helper::checkLangEnglish($lang) ?
            $title_home_en : $title_home_th;
            ?></h1>

            <div class="project-type-block">

                <h2><?= Helper::checkLangEnglish($lang) ?
                $countProjectZone.' Recommended Projects' : '推荐高品质公寓 '.$countProjectZone.'  项目';
                ?></h2>
                
               <!--  <div class="div-lang" >
                   <a href="<?//=$router->generate('condominium-list',['lang' =>  'th' ]) ?>" style="display: inline-block;" class="<?//=$lang == 'th' ? 'active' : '' ?>">TH</a> | <a href="<?//=$router->generate('condominium-list',[ 'lang' =>  'en' ]) ?>" style="display: inline-block;" class="<?//=$lang == 'en' ? 'active' : '' ?>">EN</a>
               </div>   -->   

               <div class="sort-container">


                <div class="sort-block">
                    <input type="hidden" name="sortSelect" value="">
                    <div class="sort" data-status="0">
                        <span>
                            <?php
                            $zones = getProjectViewZones(3);
                            if(!empty($_GET['zone_id'])){
                                foreach($zones as $zone) {
                                    if($_GET['zone_id'] == $zone->zone_id){
                                       echo Helper::checkLangEnglish($lang) ?
                                       $zone->zone_name_en : $zone->zone_name_th;

                                   }
                               }
                           }else{
                            echo Helper::checkLangEnglish($lang) ?
                            'Select by Project Location' : '选择所有您感兴趣的地段';

                        }
                        ?>
                    </span> <i class="i-sort"></i>
                </div>


                <ul id="sortLists">
                    <li data-value="เลือกทำเลที่สนใจทั้งหมด" onclick="location.href='<?= $router->generate('condominium-list') ?>'">
                        <?= Helper::checkLangEnglish($lang) ?
                        'Select by Project Location' : '选择所有您感兴趣的地段';
                        ?>
                    </li>
                    <?php
                    foreach($zones as $zone) {?>
                        <?php if($lang == 'th'){?>
                            <li data-value="<?= $zone->zone_name_th ?>" onclick="location.href='<?= $router->generate('condominium-list-by-zone',['zone_name' => str_replace(' ','-',$zone->zone_name_th),
                            'lang'         =>  $lang ]); ?>'">
                        <?php }else{?>
                            <li data-value="<?= $zone->zone_name_en ?>" onclick="location.href='<?= $router->generate('condominium-list-by-zone',['zone_name' => str_replace(' ','-',$zone->zone_name_en),
                            'lang'         =>  $lang ]); ?>'">

                        <?php }?>

                        <?= Helper::checkLangEnglish($lang) ?
                        $zone->zone_name_en : $zone->zone_name_th;
                        ?>

                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>

        <div class="view-block">
            <ul>
                <li><a href="" class="i-view-list active"></a></li>
                <li><a href="" class="i-view-grid"></a></li>
                <li><a href="" class="i-view-grid-more"></a></li>
            </ul>
            <div class="clearfix"></div>
        </div>

    </div>

    <input type="hidden" id="status_list" value="list">

    <div id="products" class="list-group">
        <?php
        foreach ($project_ids as $i => $pro_sub) {
            $project_id = $pro_sub['project_id'];
            $product_arr= Helper::getArrayProductOfProject($project_id);

            if(empty($_GET['zone_id'])){
                $project = getProjectByID($project_id);
            }else{
                $project = getProjectByIDZone_id($project_id,$_GET['zone_id']);
            }
            $banner  = getBannerByProjectID($project_id);
            $brand   = getBrandByID($project->brand_id);
            $price   = getProjectPrice($project_id);

            $logo           = getProjectMaps($project_id);
            $project_concept = getProjectConcept($project_id);
            $conceptOnly    = getProjectConceptOnly($project_id);

                        // filter by zone
            if( empty($_GET['zone_id']) || (!empty($_GET['zone_id']) && $_GET['zone_id'] == $project->zone_id ) ){
                ?>

                <div class="projecttype-list">
                    <div class="row veralign-middle">
                        <div class="col-md-8">
                            <?php
                            if($project->project_status == 'NP'){
                                ?>
                                <div class="tagbox2">
                                    <div class="tag2">
                                        新<br>項目
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if(in_array(1,$product_arr) || in_array(2,$product_arr) || !in_array(3,$product_arr)){
                                $url_see_more = isLadawan($project->project_id) ?
                                $router->generate('ladawan-detail',[
                                    'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                    'product_id'    =>  1
                                ]) :
                                $router->generate('single-home-detail',[
                                    'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                    'product_id'    =>  1
                                ]);
                            }
                            else{
                                $url_see_more = isLadawan($project->project_id) ?
                                $router->generate('ladawan-detail',[
                                    'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                    'product_id'    =>  3
                                ]) :
                                $router->generate('condominium-detail',[
                                    'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                    'lang'          =>  $lang
                                ]);
                            }
                            ?>
                            <?php
                            if($banner != false && ( $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'image' || $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'banner' ) ){
                                ?>
                                <a href="<?= $url_see_more ?>">
                                    <img src="<?= backend_url('base',$banner->LEAD_IMAGE_PROJECT_FILE_NAME)?>"
                                    alt="<?= $banner->LEAD_IMAGE_PROJECT_FILE_SEO ?>">
                                </a>
                                <?php
                            }elseif($banner != false && $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'youtube'){
                                $lead_img = backend_url('base', $banner->banner_img_thum);
                                $lead_youtube_url = str_replace('watch?v=', 'embed/', $banner->LEAD_IMAGE_PROJECT_FILE_NAME);
                                ?>

                                <a href="#" data-featherlight="#lbVdo<?= $i ?>">
                                    <span class="i-view-vdo"></span>
                                    <img src="<?= $lead_img ?>"
                                    alt="<?= $banner->LEAD_IMAGE_PROJECT_FILE_SEO ?>" class="hm-highl">
                                </a>
                                <iframe class="lightbox" src="<?= $lead_youtube_url . "?autoplay=0" ?>"
                                    width="1000"
                                    height="560" id="lbVdo<?= $i ?>" style="border:none;" webkitallowfullscreen
                                    mozallowfullscreen allowfullscreen></iframe>

                                    <?php
                                } elseif ($banner != false && $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'vdo') {
                                    ?>
                                    <a id="ban_vdo<?= $i ?>">
                                        <span class="i-view-vdo"></span>
                                        <img src="<?= backend_url('base', $banner->banner_img_thum) ?>"
                                        alt="<?= $banner->LEAD_IMAGE_PROJECT_FILE_SEO ?>" class="hm-highl">
                                    </a>
                                    <script>
                                        $(document).ready(function () {
                                            $('#ban_vdo<?= $i ?>').click(function () {
                                                $('#lbVdo<?= $i ?>').show();
                                                $.featherlight($('#lbVdo<?= $i ?>'),{});
                                                $('.featherlight-content #video_player<?= $i ?>')[0].play();
                                                $('#lbVdo<?= $i ?>').hide();
                                            });
                                        })
                                    </script>
                                    <div id="lbVdo<?= $i ?>" style="position:relative;width: 100%;display: none;">
                                        <video id='video_player<?= $i ?>' preload='none' controls>
                                            <source src="<?= backend_url('base',$banner->LEAD_IMAGE_PROJECT_FILE_NAME) ?>" type="video/mp4">
                                            </video>
                                        </div>

                                    <?php } ?>

                                </div>
                                <div class="col-md-4">
                                    <div class="projecttype-descrp">

                                        <?php if(!empty($logo->project_logo)) {
                                            ?>
                                            <img src="<?= is_object($logo) ? backend_url('base', $logo->project_logo) : '' ?>" title="<?= $logo->project_logo_seo ?>" alt="<?= $logo->project_logo_seo ?>" class="lo-150">
                                            <?php
                                        }?>
                                        <h1><?=$project->project_name_th?></h1>
                                        <p class="concept<?= $i ?> concept-in-list">
                                            <?php echo !empty($project->lacation_yan) ? '('.$project->lacation_yan.')<br>' : '';?>
                                            <?php  echo Helper::checkLangEnglish($lang) ? str_replace(array("\n\r", "\n","n/g"), '<br/>', $project_concept->concept_en) : str_replace(array("\n\r", "\n","n/g"), '<br/>', $project_concept->concept_th);  ?>

                                            </p>
                                         <?php if(!empty($price->project_price)) { ?>
                                            <br>
                                            <p>
                                                <?php 
                                                if($price->price_mode == '1'){
                                                    $mode_price = '起步价';
                                                    $start = 'Start ';
                                                }else{
                                                    $mode_price = '价格';
                                                    $start = 'Pricing from';
                                                }
                                                ?>
                                                <strong>
                                                <?= Helper::checkLangEnglish($lang) ?
                                                   $start : $mode_price;
                                                   ?>
                                                    
                                                    <?php
                                                    $project_price   = getProjectPrice($project_id);
                                                    $priceCondo = Helper::getProjectPrice($project_price->project_price,$project_price->price_mode,$lang)
                                                    ?>
                                                    <?= $priceCondo.' ' ?>
                                                    <?= is_object($project_price) ? Helper::getPriceModeName($project_price->price_mode,$project_price->project_price,$lang) : '' ?>


                                                </strong>
                                            </p>
                                            <?php }?>
                                            <a href="<?= $url_see_more ?>" class="btn btn-seemore">
                                               更多信息
                                            </a>

                                 </div>
                             </div>
                         </div>
                     </div>
                     <?php
                 }
             }
             ?>
         </div>

     </div>
 </div>
</div>


<?php include('footer.php'); ?>