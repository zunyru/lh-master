<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

$type_360 = !empty($_GET['type_360']) ? $_GET['type_360'] : null;

if(empty($type_360) || $type_360 == 'project'){
    $virtual_360 = getAll360Projects();
}else{
    $virtual_360 = getAll360Plans();
}
$page = '360-virtual-tour';
$page_index = 0;
$img = '';
?>

<?php
include('header.php');
//elementMetaTitleDisTag($page);
?>
<!-- CSS -->
<link rel="stylesheet" href="<?= file_path('css/360-virtual-tour.css') ?>" type="text/css">
<!-- JS -->
<script src="<?= file_path('js/360-virtual-tour.js') ?>"></script>

<script type="text/javascript" src="<?= file_path('fancybox/jquery.mousewheel-3.0.4.pack.js') ?>"></script>
<link rel="stylesheet" href="<?= file_path('fancybox/fancybox.min.css') ?>" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>

<div id="content" class="content">
    <div class="container">

        
        <?php
        $SEO = getSEOUrl($actual_link);
        if($actual_link == @$SEO->url_page){
            echo '<h1 class="heading-title">'.$SEO->h1.'<h1>';
        }else{
           echo '<h1 class="heading-title">360度画面</h1>'; 
       }
       ?>
       <div class="tpl1-nav">
        <ul>
            <li><a href="<?= $router->generate('360-virtual',['type_360' => 'project' ]) ?>" class="<?= empty($type_360) || $type_360 == 'project' ? 'active' : ''?>">360度房产项目画面展示</a></li>
            <li><a href="<?= $router->generate('360-virtual',['type_360' => 'plan' ]) ?>" class="<?= !empty($type_360) && $type_360 == 'plan' ? 'active' : ''?>">360度房间类型画面展示</a></li>
        </ul>
    </div>

        <?php
        $first_virtual_360  = null;
        $first_img_360      = null;
        $first_url          = null;
        $first_title_360    = null;
        if(!empty($virtual_360)){
            $first_virtual_360 = $virtual_360[0];

            array_shift($virtual_360);

            if(empty($type_360) || $type_360 == 'project'){

                $first_img_360   = backend_url('base',$first_virtual_360->thumnail);
                $first_url       = $first_virtual_360->c360_project_url;
                $first_title_360 = $first_virtual_360->project_name_th;
            }
            else{

                $first_img_360   = backend_url('base',$first_virtual_360->c360_img);
                $first_url       = $first_virtual_360->c360_plan_url;
                $first_title_360 = $first_virtual_360->plan_name_th;
            }
        }

        ?>

        <?php
        if(!empty($first_virtual_360)){
            ?>
            <div class="tpl1-block-1">
                <div class="tpl1-block-item">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="tpl1-block-item-img">
                                <a id="various" href="<?= $first_url ?>"><span class="i-view-360"></span><img src="<?= $first_img_360 ?>" alt=""></a>
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                        $("#various").fancybox({
                                            'width'				: '100%',
                                            'height'			: '100%',
                                            'autoScale'			: false,
                                            'transitionIn'		: 'none',
                                            'transitionOut'		: 'none',
                                            'type'				: 'iframe'
                                        });
                                    });
                                </script>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="tpl1-block-descrp">
                                <p class="title"><?= $first_title_360 ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>

        <?php
        if(!empty($virtual_360)){
            ?>
            <div id="tpl1Block2List" class="tpl1-block-2">
                <div class="row">
                    <div id="block2List"  class="col-slide">
                        <?php
                        //print_r($virtual_360);
                        $i=0;
                        foreach ($virtual_360 as $virtual) {

                            $img_360 = '';
                            $url = '';
                            $title_360 = '';
                            if(empty($type_360) || $type_360 == 'project'){

                                $img_360   = backend_url('base',$virtual->thumnail);
                                $url       = $virtual->c360_project_url;
                                $title_360 = $virtual->project_name_th;
                            }
                            else{

                                $img_360   = backend_url('base',$virtual->c360_img);
                                $url       = $virtual->c360_plan_url;
                                $title_360 = $virtual->plan_name_th;
                            }

                            ?>
                            <div class="item">
                                <div class="hm-tip">
<!--                                    <a href="">-->
                                        <div class="review-img">
                                            <a id="various<?=$i?>" href="<?= $url ?>"><span class="i-view-360"></span><img src="<?= $img_360 ?>" alt=""></a>
                                            <script type="text/javascript">
                                                $(document).ready(function() {
                                                    $("#various<?=$i?>").fancybox({
                                                        'width'				: '75%',
                                                        'height'			: '75%',
                                                        'autoScale'			: false,
                                                        'transitionIn'		: 'none',
                                                        'transitionOut'		: 'none',
                                                        'type'				: 'iframe'
                                                    });
                                                });
                                            </script>
                                        </div>
                                        <p class="title"><?= $title_360 ?></p>
<!--                                    </a>-->
                                </div>
                            </div>
                            <?php
                            $i++;
                        }
                        ?>

                    </div>
                </div>
            </div>
            <?php
        }
        ?>

    </div>
</div>





<?php include('footer.php'); ?>
