<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>

<?php include('header.php'); ?>
<!-- CSS -->
<link rel="stylesheet" href="css/corporate-news.css" type="text/css">
<!-- JS -->
<script src="js/corporate-news.js"></script>

<div id="content" class="content news-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">

                <h1>ข่าวจากสื่อสิ่งพิมพ์</h1>

                <div class="">
                    <img src="images/news/news_4.jpg" alt="" class="content-img">
                </div>

                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="select-year-block">
                            ปี
                            <div class="select-year">
                                <select name="" id="">
                                    <option value="2560">2560</option>
                                    <option value="2559">2559</option>
                                    <option value="2558">2558</option>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="news-lists">
                            <table id="news">
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>07 ธันวาคม 2559</td>
                                    <td>กำหนดเวลาการใช้สิทธิใบสำคัญแสดงสิทธิที่จะซื้อหุ้นสามัญ (LH-W3)</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>10 พฤศจิกายน 2559</td>
                                    <td>คำอธิบายและวิเคราะห์ของฝ่ายจัดการ ไตรมาสที่ 3 สิ้นสุดวันที่ 30 ก.ย. 2559</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>10 พฤศจิกายน 2559</td>
                                    <td>งบการเงินไตรมาสที่ 3/2559</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>10 พฤศจิกายน 2559</td>
                                    <td>สรุปผลการดำเนินงานของบจ.และรวมของบริษัทย่อย ไตรมาสที่ 3 (F45-3)</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>10 พฤศจิกายน 2559</td>
                                    <td>แต่งตั้งประธานกรรมการบริหารความเสี่ยง</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>10 ตุลาคม 2559</td>
                                    <td>หุ้นเพิ่มทุนของ LH เริ่มซื้อขายวันที่ 11 ตุลาคม 2559</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>06 ตุลาคม 2559</td>
                                    <td>แจ้งวันหยุดทำการปี 2560</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>05 ตุลาคม 2559</td>
                                    <td>ให้ผู้ถือหุ้นเสนอวาระการประชุมและเสนอชื่อบุคคล เพื่อเข้ารับการคัดเลือกเป็นกรรมการบริษัท</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>30 กันยายน 2559</td>
                                    <td>แบบรายงานผลการขายหลักทรัพย์ต่อตลาดหลักทรัพย์ (F53-5)</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>30 กันยายน 2559</td>
                                    <td>แบบรายงานผลการขายหลักทรัพย์ต่อตลาดหลักทรัพย์ (F53-5)</td>
                                </tr>

                                <tr>
                                    <td class="news-date"><span>&#0149;</span>07 ธันวาคม 2559</td>
                                    <td>กำหนดเวลาการใช้สิทธิใบสำคัญแสดงสิทธิที่จะซื้อหุ้นสามัญ (LH-W3)</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>10 พฤศจิกายน 2559</td>
                                    <td>คำอธิบายและวิเคราะห์ของฝ่ายจัดการ ไตรมาสที่ 3 สิ้นสุดวันที่ 30 ก.ย. 2559</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>10 พฤศจิกายน 2559</td>
                                    <td>งบการเงินไตรมาสที่ 3/2559</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>10 พฤศจิกายน 2559</td>
                                    <td>สรุปผลการดำเนินงานของบจ.และรวมของบริษัทย่อย ไตรมาสที่ 3 (F45-3)</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>10 พฤศจิกายน 2559</td>
                                    <td>แต่งตั้งประธานกรรมการบริหารความเสี่ยง</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>10 ตุลาคม 2559</td>
                                    <td>หุ้นเพิ่มทุนของ LH เริ่มซื้อขายวันที่ 11 ตุลาคม 2559</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>06 ตุลาคม 2559</td>
                                    <td>แจ้งวันหยุดทำการปี 2560</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>05 ตุลาคม 2559</td>
                                    <td>ให้ผู้ถือหุ้นเสนอวาระการประชุมและเสนอชื่อบุคคล เพื่อเข้ารับการคัดเลือกเป็นกรรมการบริษัท</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>30 กันยายน 2559</td>
                                    <td>แบบรายงานผลการขายหลักทรัพย์ต่อตลาดหลักทรัพย์ (F53-5)</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>30 กันยายน 2559</td>
                                    <td>แบบรายงานผลการขายหลักทรัพย์ต่อตลาดหลักทรัพย์ (F53-5)</td>
                                </tr>

                                <tr>
                                    <td class="news-date"><span>&#0149;</span>07 ธันวาคม 2559</td>
                                    <td>กำหนดเวลาการใช้สิทธิใบสำคัญแสดงสิทธิที่จะซื้อหุ้นสามัญ (LH-W3)</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>10 พฤศจิกายน 2559</td>
                                    <td>คำอธิบายและวิเคราะห์ของฝ่ายจัดการ ไตรมาสที่ 3 สิ้นสุดวันที่ 30 ก.ย. 2559</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>10 พฤศจิกายน 2559</td>
                                    <td>งบการเงินไตรมาสที่ 3/2559</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>10 พฤศจิกายน 2559</td>
                                    <td>สรุปผลการดำเนินงานของบจ.และรวมของบริษัทย่อย ไตรมาสที่ 3 (F45-3)</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>10 พฤศจิกายน 2559</td>
                                    <td>แต่งตั้งประธานกรรมการบริหารความเสี่ยง</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>10 ตุลาคม 2559</td>
                                    <td>หุ้นเพิ่มทุนของ LH เริ่มซื้อขายวันที่ 11 ตุลาคม 2559</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>06 ตุลาคม 2559</td>
                                    <td>แจ้งวันหยุดทำการปี 2560</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>05 ตุลาคม 2559</td>
                                    <td>ให้ผู้ถือหุ้นเสนอวาระการประชุมและเสนอชื่อบุคคล เพื่อเข้ารับการคัดเลือกเป็นกรรมการบริษัท</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>30 กันยายน 2559</td>
                                    <td>แบบรายงานผลการขายหลักทรัพย์ต่อตลาดหลักทรัพย์ (F53-5)</td>
                                </tr>
                                <tr>
                                    <td class="news-date"><span>&#0149;</span>30 กันยายน 2559</td>
                                    <td>แบบรายงานผลการขายหลักทรัพย์ต่อตลาดหลักทรัพย์ (F53-5)</td>
                                </tr>
                            </table>


                        </div>
                        <a href="" id="loadMore" class="btn btn-seemore">See more</a>



<!--                        <div class="test">Content</div>-->
<!--                        <div class="test">Content</div>-->
<!--                        <div class="test">Content</div>-->
<!--                        <div class="test">Content</div>-->
<!--                        <div class="test">Content</div>-->
<!--                        <div class="test">Content</div>-->
<!--                        <div class="test">Content</div>-->
<!--                        <div class="test">Content</div>-->
<!--                        <div class="test">Content</div>-->
<!--                        <div class="test">Content</div>-->
<!--                        <div class="test">Content</div>-->
<!--                        <div class="test">Content</div>-->
<!--                        <div class="test">Content</div>-->
<!--                        <div class="test">Content</div>-->
<!--                        <div class="test">Content</div>-->
<!--                        <div class="test">Content</div>-->
<!--                        <div class="test">Content</div>-->
<!--                        <div class="test">Content</div>-->
<!--                        <div class="test">Content</div>-->
<!--                        <div class="test">Content</div>-->
<!---->
<!--                        <div class="test">Content</div>-->
<!--                        <div class="test">Content</div>-->
<!--                        <div class="test">Content</div>-->
<!--                        <div class="test">Content</div>-->
<!---->
<!--                        <div class="test">Content</div>-->
<!--                        <div class="test">Content</div>-->
<!--                        <div class="test">Content</div>-->
<!--                        <div class="test">Content</div>-->
<!---->
<!---->
<!--                        <a href="#" id="loadMore">Load More</a>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include('footer.php'); ?>
