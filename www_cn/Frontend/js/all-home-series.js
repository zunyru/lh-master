$(document).ready(function () {
    //Page Load Start
    bannerSlide();
    projectPromoSlide();
    modelThumb();
    
    var winW = $(window).width();

    if( winW > 768 ) {
        otherHomeSlide()
    }
    //Page Load End
});

$(window).load(function () {
    var winW = $(window).width();

    if( winW <= 768 ) {
        otherHomeSlide()
    }
});

//Function Start

function bannerSlide() {
    var winH = $(window).height();
    //$('.banner-parallax').css('height', 360);

}


function projectPromoSlide() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        var stagePadding = 40;
    }
    else {
        var stagePadding = 0;
    }

    var isMulti = ($('#projectPromoSlide .item').length > 1) ? true : false;
    $('#projectPromoSlide').owlCarousel({
        loop:isMulti,
        margin: 20,
        stagePadding: stagePadding,
        nav:isMulti,
        dots: isMulti,
        responsive:{
            0:{
                items:1
            }
        }
    });
}
function otherHomeSlide() {

    var winW = $(window).width();
    // responsive
    if( winW <= 768 ) {
        var stagePadding = 25;
        var margin = -10;
        var autoH = true;
    }
    else {
        var stagePadding = 0;
        var margin = 10;
        var autoH = false;
    }

    $('#otherHome').owlCarousel({
        loop:true,
        margin: margin,
        stagePadding: stagePadding,
        nav:true,
        dots: true,
        autoHeight: autoH,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:1
            },
            979:{
                items:2
            },
            1199:{
                items:3
            }
        }
    });

    $(document).on('click','#otherHome a',function () {
        var amount_value = $('#amount_value').val();
        var index_value = $(this).find('#index_value').val();
        var title_home = $(this).find('#data-slider #title_home').val();
        var format_home = $(this).find('#data-slider #format_home').val();
        var id_home = $(this).find('#data-slider #id_home').val();
        var identity_home = $(this).find('#data-slider #identity_home').val();
        var price_home = $(this).find('#data-slider #price_home').val();
        var img_arr = [];
        var img_list = $(this).find('#data-slider #img_list').children('input').each(function () {
            img_arr.push($(this).val());
        });
        var li_img = '';
        var li_img_thumb = '';
        $(this).find('#img_list').children('input').each(function () {
            li_img = li_img + '<div class="gal-img item"><img src="'+ $(this).val() +'"/></div>';
            li_img_thumb = li_img_thumb + '<a class="item"><img src="'+ $(this).val() +'" alt=""></a>';
        });
        $('#projectSlide').html();
        $('#projectSlide').html(li_img);
        $('#imgThumb').html();
        $('#imgThumb').html(li_img_thumb);
        $('#projectSlide').trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
        $('#projectSlide').find('.owl-stage-outer').children().unwrap();
        $('#imgThumb').trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
        $('#imgThumb').find('.owl-stage-outer').children().unwrap();
        modelThumb();

        var desc_home = '';
        desc_home += '<p class="title">'+ title_home +'</p>';
        desc_home += '<p class="title green">แบบบ้าน '+ format_home +'</p>';
        desc_home += '<p class="title green">หมายเลขแปลง <span>'+id_home+'</span></p>';
        desc_home += '<p class="title">ลักษณะแปลงที่ดิน</p>'+identity_home;
        desc_home += '<p class="title green">ราคา '+ price_home +'</p>';
        desc_home += '<a href="" class="btn btn-seemore">See more</a>';

        var index_plan = $('#index_plan');
        index_plan.text(index_value+'/'+amount_value+' แปลง')

        $('#desc_home').html(desc_home);
    });
}
function modelThumb() {
    var winW = $(window).width();

    if( winW <= 768 ) {
        var slidesPerPage = 3;
    }
    else {
        var slidesPerPage = 6;
    }

    var sync1 = $('#projectSlide');
    var sync2 = $('#imgThumb');
    var flag = false;
    var duration = 300;

    sync1.owlCarousel({
        items: 1,
        animateOut: 'fadeOut',
        nav: true,
        dots: false,
        //autoplay: true
    })
        .on('change.owl.carousel', function (e) {
            if (e.namespace && e.property.name === 'position' && !flag) {
                flag = true;
                sync2.trigger('to.owl.carousel', [e.relatedTarget.relative(e.property.value), duration, true]);
                flag = false;
            }
        });

    sync2.owlCarousel({
        items: slidesPerPage,
        nav: false,
        slideBy: slidesPerPage,
        dots: false,
        margin: 10,
    })
        .on('click', '.owl-item', function (e) {
            e.preventDefault();
            sync1.trigger('to.owl.carousel', [$(this).index(), duration, true]);
        })
        .on('change.owl.carousel', function (e) {
            if (e.namespace && e.property.name === 'position' && !flag) {
                flag = true;
                sync1.trigger('to.owl.carousel', [e.relatedTarget.relative(e.property.value), duration, true]);
                flag = false;
            }
        });

    sync2.on('click', '.owl-item', function(e) {
        e.preventDefault();
        $('.item').removeClass('active');
        $(this).find('.item').addClass('active');
    });


    // $('#modelThumb').bxSlider({
    //     pagerCustom: '#modelThumb-pager'
    // });
    //
    // var winW = $(window).width();
    //
    // // responsive
    // if( winW < 768 ) {
    //     var items = 3;
    // }
    // else {
    //     var items = 6;
    // }
    //
    // $('#modelThumb-pager').owlCarousel({
    //     loop:true,
    //     margin: 5,
    //     nav:true,
    //     dots: false,
    //     autoHeight: true,
    //     responsive:{
    //         0:{
    //             items:items
    //         }
    //     }
    // });


}

//Function End