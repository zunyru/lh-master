$(function () {
    $("#news tr").slice(0, 10).show();
    $("#loadMore").on('click', function (e) {
        e.preventDefault();
        $("#news tr:hidden").slice(0, 10).slideDown();
        if ($("#news tr:hidden").length == 0) {
        }
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1200);
    });
});