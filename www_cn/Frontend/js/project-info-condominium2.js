$(document).ready(function () {
    //Page Load Start

    $('#pjHomeBanner').owlCarousel({
        loop:true,
        autoplay: true,
        autoplayTimeout: 7000,
        autoplaySpeed: 1000,
        margin:5,
        animateOut: 'fadeOut',
        nav:false,
        dots: true,
        video:true,
        lazyLoad:true,
        center:true,
        responsive:{
            0:{
                items:1
            }
        },
        onChanged: function (event) {
            setTimeout(function(){
                $('.slider').find('.center').addClass('animate');
            }, 3000);
            setTimeout(function(){
                $('.slider').find('.center').addClass('animate2');
            }, 3500);
            setTimeout(function(){
                $('.slider').find('.center').addClass('animate3');
            }, 4500);
            setTimeout(function () {
                $('#img_logo').fadeOut('slow');
                $('#desc_logo').fadeOut('slow');
            },7200);
        }
    });

    
    projectPromoSlide();
    stickyMenuProject();
    scrollNext();

    projectplanImg();
    prjProgress();
    radionAppoint();

    var winW = $(window).width();

    if( winW > 768 ) {
        gallery();
        slideFacilityImg();
    }

    $('#date_appointment').datepicker({
        startDate: new Date(),
        todayHighlight: true
    });

    $('.gallery').featherlightGallery();
    $('.prjprogress').featherlightGallery();
    //Page Load End
});

$(window).load(function () {
    var winW = $(window).width();

    if( winW <= 768 ) {
        gallery();
        slideFacilityImg();
    }
});


//Function Start
function scrollNext() {
    $("#scrollNext").click(function() {
        $('html, body').animate({
            scrollTop: $("#sec-information").offset().top - 60
        }, 800);
    });
}
function gridLayout5() {
    var wall = new Freewall("#tpl5-img");
    wall.reset({
        selector: '.item',
        animate: true,
        cellW: 300,
        cellH: 'auto',
        onResize: function() {
            wall.fitWidth();
        }
    });

    var images = wall.container.find('.item');
    images.find('img').load(function() {
        wall.fitWidth();
    });
}

function gallery() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        //var stagePadding = 40;
        $('#tpl5-img').owlCarousel({
            loop:true,
            margin: 30,
            //stagePadding: stagePadding,
            nav:true,
            dots: true,
            autoHeight: true,
            responsive:{
                0:{
                    items:1
                },
                768:{
                    items:1
                }
            }
        });
    }
    else {
        gridLayout5();
    }
}

function projectPromoSlide() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        //var stagePadding = 40;
        // var autoH = true;
    }
    else {
        //var stagePadding = 0;
        // var autoH = false;
    }

    // $('#projectPromoSlide').owlCarousel({
    //     loop:true,
    //     margin: 20,
    //     //stagePadding: stagePadding,
    //     nav:true,
    //     dots: true,
    //     // autoHeight: autoH,
    //     responsive:{
    //         0:{
    //             items:1
    //         }
    //     }
    // });
}


function slideFacilityImg() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        var stagePadding = 25;
        var margin = -10;
    }
    else {
        var stagePadding = 0;
        var margin = 10;
    }

    $('#facilityImg').owlCarousel({
        loop:true,
        margin: margin,
        stagePadding: stagePadding,
        nav:true,
        dots: true,
        autoHeight: false,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:1
            },
            979:{
                items:2
            },
            1199:{
                items:3
            }
        }
    });
}

function projectplanImg() {
    $('#projectplanImg, #roomplanImg').owlCarousel({
        loop:false,
        autoplay: false,
        autoplayTimeout: 7000,
        autoplaySpeed: 1000,
        margin:5,
        animateOut: 'fadeOut',
        nav:true,
        dots: true,
        video:true,
        lazyLoad:true,
        center:true,
        responsive:{
            0:{
                items:1
            },

        }
    });
}


function prjProgress() {
    $('#projProgress').owlCarousel({
        loop:false,
        autoplay: false,
        autoplayTimeout: 7000,
        autoplaySpeed: 1000,
        margin:15,
        animateOut: 'fadeOut',
        nav:false,
        dots: true,
        lazyLoad:true,
        slideBy: 4,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:1
            },
            979:{
                items:4
            },
            1199:{
                items:4
            }

        }
    });
}

function stickyMenuProject() {
    var winH = $(window).height();
    var pageH = winH - 70

    $('#main-nav li a').click(function() {
        $('#main-nav li a').removeClass('active');
        var id = $(this).attr('id');
        $(this).addClass('active');
        // console.log(id);
        var margin_top_animate = $(this).attr('data-family') == "child" ? 115 : 70;
        $('html, body').animate({
            scrollTop: $('#sec-'+id).offset().top - margin_top_animate
        }, 1000);
    });


}

function initMap() {
    // Create a map object and specify the DOM element for display.
    var pos = {lat: 13.7240107, lng: 100.5141862};
    var map = new google.maps.Map(document.getElementById('map'), {
        center: pos,
        scrollwheel: false,
        zoom: 15,
        zoomControlOptions: {
            position: google.maps.ControlPosition.TOP_RIGHT
        },
        streetViewControl: false,
        mapTypeControl: false,
        styles: [{"elementType":"geometry","stylers":[{"color":"#f5f5f5"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#f5f5f5"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"administrative.locality","elementType":"labels.text.fill","stylers":[{"color":"#c19838"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text.fill","stylers":[{"color":"#c19838"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels.text","stylers":[{"color":"#878787"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#9f720a"},{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#dadada"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#c9c9c9"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]}]
    });
    var icon = {
        url: 'images/global/icon_google_map_gd.png',
        scaledSize: new google.maps.Size(30, 42), // scaled size
        origin: new google.maps.Point(0,0), // origin
        anchor: new google.maps.Point(0, 0) // anchor
    };
    var marker = new google.maps.Marker({
        position: pos,
        map: map,
        icon: icon
    });
    // var contentString = $('#info-window').html();

    // var infowindow = new google.maps.InfoWindow({
    //     content: contentString
    // });

    // infowindow.open(map, marker);

    // marker.addListener('mouseover', function() {
    //     infowindow.open(map, marker);
    // });
    // marker.addListener('mouseout', function() {
    //     infowindow.close();
    // });
}



function radionAppoint() {

    $('.radio-checkmark input[type="radio"]').click(function() {
        if($('#check-appoint').is(':checked')) {
            $('input#date_appointment').css('display','block');
        }
        else {
            $('input#date_appointment').css('display','none');
        }
    });
}
//Function End