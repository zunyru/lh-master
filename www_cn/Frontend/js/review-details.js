$(document).ready(function () {
    //Page Load Start
    var winW = $(window).width();

    if( winW > 768 ) {
        relatePosts();
    }

    var winH = $(window).height();

    var pageH = winH - 140;
    $('.page-banner, .page-banner-content').css('height',pageH);

    //Page Load End
});

$(window).load(function () {
    var winW = $(window).width();

    if( winW <= 768 ) {
        relatePosts();
    }
});


//Function Start

function relatePosts() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        var stagePadding = 25;
        var margin = -10;
        var autoH = true;
    }
    else {
        var stagePadding = 0;
        var margin = 10;
        var autoH = false;
    }

    var isMulti = ($('#relatePost .item').length > 1) ? true : false;
    $('#relatePost').owlCarousel({
        loop:isMulti,
        margin: margin,
        stagePadding: stagePadding,
        nav:isMulti,
        dots: false,
        autoHeight: autoH,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:1
            },
            979:{
                items:2
            },
            1199:{
                items:3
            }
        }
    });
}


function menuHomeSeries() {
    $('.i-review-share').click(function (e) {

        var status = $(this).attr('data-status');
        if (status == 0) {
            $(this).attr('data-status', 1);
            $('.lnk-icons').css({'opacity': 1, 'visibility': 'visible'});
        }
        else {
            $(this).attr('data-status', 0);
            $('.lnk-icons').css({'opacity': 0, 'visibility': 'hidden'});
        }

        e.preventDefault();
    });
}


//Function End