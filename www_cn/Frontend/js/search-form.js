$(document).ready(function () {

    var product_id  =   null;
    var zone_id     =   null;
    var brand_id    =   null;
    var project_id  =   null;
    var project_url  =   null;

    $(document).on("click","ul#searchLists li",function()
    {
        product_id  =   $(this).attr('data-id');
        product_slug  =   $(this).attr('data-slug');
        var params      =   {
            product_id: product_id,
            zone_id:    null,
            brand_id:   null,
            project_id: null
        };
        console.log(params);
        getData(params);
        $('#product_id').val(product_slug);
        $('#zone_id').val('');
        $('#brand_id').val('');
    });

    $(document).on("click","ul#projectlocationLists li",function()
    {
        zone_id  =   $(this).attr('data-id');
        zone_slug  =   $(this).attr('data-slug');
        var params      =   {
            product_id: product_id,
            zone_id:    zone_id,
            brand_id:   null,
            project_id: null
        };
        getData(params);
        $('#zone_id').val(zone_slug);
        $('#brand_id').val('');
    });

    $(document).on("click","ul#projectbrandLists li",function()
    {
        brand_id  =   $(this).attr('data-id');
        brand_slug  =   $(this).attr('data-slug');
        var params      =   {
            product_id: product_id,
            zone_id:    zone_id,
            brand_id:   brand_id,
            project_id: null
        };
        getData(params);
        $('#brand_id').val(brand_slug);
    });

    $(document).on("click","ul#projectnameLists li",function()
    {
        var url = document.URL;
        project_id   =   $(this).attr('data-id');
        project_url  =   $(this).attr('data-url');
        if(parseInt(product_id) == 1 || parseInt(product_id) == 2){
            window.location.href = project_url;
        }else if(parseInt(product_id) == 3){
            window.location.href = project_url;
        }else{
            window.location.href = project_url;
        }
    });

    $('#search_form').submit(function (e) {
        if($('#product_id').val() == '' && $('#zone_id').val() == '' && $('#brand_id').val() == '' ){
            $('#validateSearchFilter').show();
            return false;
        }else{
            var path_product=($('#product_id').val()==''?"any-products":$('#product_id').val());
            var path_zone=($('#zone_id').val()==''?"":$('#zone_id').val());
            var path_brand=($('#brand_id').val()==''?"":"/"+$('#brand_id').val());
            var get_path=path_product+"/"+path_zone+path_brand;
                get_path=get_path.replace("//", "/any-zones/");
            console.log(get_path);
             console.log(path_product);
             console.log(path_zone);
              console.log(path_brand);
            //return false;
            window.location.href = "http://uat.lh.co.th/cn/search/"+get_path;
            e.preventDefault();
        }
    });

});

function renderSelect(result) {
    if(result.products){
        var products = result.products;
        $('#projectlocationLists').empty();
        $.each(products,function (val) {
            // $('#projectlocationLists').append('<li data-id="'+val.+'" data-value="'++'">'++'</li>')
        });
    }
    if(result.zones){
        var zones = result.zones;
        $('#projectlocationLists').empty();
        $.each(zones,function (i,val) {
            $('#projectlocationLists').append('<li data-id="'+val.zone_id+'" data-slug="'+val.slug+'" data-value="'+val.zone_name+'">'+val.zone_name+'</li>')
        });
        searchDropdown();
        $('#projectlocationLists').parent().find('.searchBlock span').text('选择地段');
    }
    if(result.brands){
        var brands = result.brands;
        $('#projectbrandLists').empty();
        $.each(brands,function (i,val) {
            $('#projectbrandLists').append('<li data-id="'+val.brand_id+'" data-slug="'+val.slug+'" data-value="'+val.brand_name_th+'">'+val.brand_name_th+'</li>')
        });
        searchDropdown();
        $('#projectbrandLists').parent().find('.searchBlock span').text('选择牌子．');
    }
    if(result.projects){
        var projects = result.projects;
        $('#projectnameLists').empty();
        $.each(projects,function (i,val) {
            $('#projectnameLists').append('<li data-url="'+val.project_url+'" data-value="'+val.project_name_th+'">'+val.project_name_th+'</li>')
        });
        searchDropdown();
        $('#projectnameLists').parent().find('.searchBlock span').text('选择项目名称．');
    }
}

function getData(params) {
    $.ajax({
        type: "POST",
        url: "http://uat.lh.co.th/cn/search-form-cn",
        data: params,
        async: false,
        cache: false
    }).success(function(data) {
        var aaa = params;
        renderSelect(data);
    });
}