$(document).ready(function () {
    //Page Load Start
    var winW = $(window).width();

    if( winW > 768 ) {
        gallery();
    }
    //Page Load End
    $('.gallery').featherlightGallery();
});

$(window).load(function () {
    var winW = $(window).width();

    if( winW <= 768 ) {
        gallery();
    }
});

//Function Start
function gridLayout5() {
    var wall = new Freewall("#tpl5-img");
    wall.reset({
        selector: '.item',
        animate: true,
        cellW: 300,
        cellH: 'auto',
        onResize: function() {
            wall.fitWidth();
        }
    });

    var images = wall.container.find('.item');
    images.find('img').load(function() {
        wall.fitWidth();
    });
}

function gallery() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        // var stagePadding = 40;
        $('#tpl5-img').owlCarousel({
            loop:true,
            margin: 20,
            //stagePadding: stagePadding,
            nav:true,
            dots: true,
            autoHeight: true,
            responsive:{
                0:{
                    items:1
                },
                768:{
                    items:1
                }
            }
        });
    }
    else {
        gridLayout5();
    }
}

//Function End