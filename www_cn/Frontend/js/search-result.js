$(document).ready(function () {
    //Page Load Start
    masonryList();

    $(document).ready(function()
    {
        $(document).resize();
    });
    //Page Load End
});


//Function Start

function masonryList() {
    var wall = new Freewall(".masonry-lists");
    /*wall.reset({
        selector: '.list-item',
        animate: true,
        cellW: 180,
        cellH: 'auto',
        onResize: function() {
            wall.fitWidth();
        }
    });*/

    wall.reset({
        selector: '.list-item',
        cellW: 350,
        cellH: 300,
        gutterY: 20,
        gutterX: 20,
        onResize: function() {
            wall.fitWidth();
        }
    })
    wall.fitWidth();

    wall.container.find('.list-item img').load(function() {
        wall.fitWidth();
    });
}


//Function End