$(document).ready(function () {
    //Page Load Start
    awardList();

    //Page Load End
});


//Function Start
function masonryList() {
    var wall = new Freewall(".award-lists");
    wall.reset({
        selector: '.list-item',
        animate: true,
        cellW: 180,
        cellH: 'auto',
        onResize: function() {
            wall.fitWidth();
        }
    });

    wall.container.find('.list-item img').load(function() {
        wall.fitWidth();
    });
}

function awardList() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {

    }
    else {
        masonryList();
    }



}


//Function End