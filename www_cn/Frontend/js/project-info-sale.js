$(document).ready(function () {
    //Page Load Start
    bannerSlide();
    gallery();
    stickyMenuProject();
    homePlanSlide();
    var winW = $(window).width();

    if( winW <= 768 ) {
        homePlanSlide();
    }
    // projectPromoSlide();
    //Page Load End
    $('.gallery').featherlightGallery();

    $(document).ready(function()
    {
        $(document).resize();
    });
});


$(window).load(function () {
    var winW = $(window).width();

    if( winW <= 768 ) {
        homePlanSlide();
    }
});

//Function Start
function bannerSlide() {

    var winW = $(window).width();

    if( winW <= 768 ) {
        bannerThumb();
    }
    else {

        var isMulti = ($('#bannerSlide.owl-carousel img').length > 1) ? true : false
        $('#bannerSlide').owlCarousel({
            loop:isMulti,
            autoplay: false,
            autoplayTimeout: 500,
            autoplaySpeed: 1000,
            margin:5,
            animateOut: 'fadeOut',
            nav:false,
            dots: isMulti,
            video:true,
            lazyLoad:true,
            center:true,
            responsive:{
                0:{
                    items:1
                },

            }
        });
    }



}

function gridLayout5() {
    var wall = new Freewall("#tpl5-img");
    wall.reset({
        selector: '.item',
        animate: true,
        cellW: 300,
        cellH: 'auto',
        onResize: function() {
            wall.fitWidth();
        }
    });

    var images = wall.container.find('.item');
    images.find('img').load(function() {
        wall.fitWidth();
    });
}

function gallery() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        //var stagePadding = 40;
        $('#tpl5-img').owlCarousel({
            loop:true,
            margin: 30,
            autoplay:true,
            autoplayTimeout:3000,
            autoplayHoverPause:true,
            //stagePadding: stagePadding,
            nav:true,
            dots: false,
            autoHeight: true,
            responsive:{
                0:{
                    items:1
                },
                768:{
                    items:1
                }
            }
        });
    }
    else {
        gridLayout5();
    }
}

function homePlanSlide() {

    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        var stagePadding = 0;
    }
    else {
        var stagePadding = 0;
    }

    var isMulti = ($('#homePlanSlide .item').length > 1) ? true : false
    var homeSlider = $("#homePlanSlide");
    homeSlider.owlCarousel({
        loop:isMulti,
        margin: 10,
        stagePadding: stagePadding,
        nav:isMulti,
        dots: isMulti,
        autoHeight: false,
        responsive:{
            0:{
                items:1
            }
        },
    });
    homeSlider.on("click", ".tabs-menu-plan-slide a", function () {
        var value = $(this).attr('data-value');
        homeSlider.trigger('to.owl.carousel', [value]);
    });
}


// function projectPromoSlide() {
    // var winW = $(window).width();
    //
    // // responsive
    // if( winW <= 768 ) {
    //     var stagePadding = 40;
    //     var margin = 0;
    //     var autoH = true;
    // }
    // else {
    //     var stagePadding = 0;
    //     var margin = 20;
    //     var autoH = false;
    // }
    //
    // $('#projectPromoSlide').owlCarousel({
    //     loop:true,
    //     margin: margin,
    //     stagePadding: stagePadding,
    //     nav:true,
    //     dots: true,
    //     autoHeight: autoH,
    //     responsive:{
    //         0:{
    //             items:1
    //         }
    //     }
    // });
// }


function stickyMenuProject() {
    var winH = $(window).height();
    var pageH = winH - 60

    $('#main-nav li a').click(function() {
        $('#main-nav li a').removeClass('active');
        var id = $(this).attr('id');
        $(this).addClass('active');
         console.log(id);
        var margin_top_animate = $(this).attr('data-family') == "child" ? 115 : 60;
        $('html, body').animate({
            scrollTop: $('#sec-'+id).offset().top - margin_top_animate
        }, 1000);
    });

}


function bannerThumb() {

    var sync1 = $('#projectSlide');
    var sync2 = $('#imgThumb');
    var flag = false;
    var duration = 300;

    sync1.owlCarousel({
        items: 1,
        animateOut: 'fadeOut',
        nav: false,
        dots: false,
        //autoplay: true
    })
        .on('change.owl.carousel', function (e) {
            if (e.namespace && e.property.name === 'position' && !flag) {
                flag = true;
                sync2.trigger('to.owl.carousel', [e.relatedTarget.relative(e.property.value), duration, true]);
                flag = false;
            }
        });

    sync2.owlCarousel({
        items: 3,
        nav: true,
        slideBy: 3,
        dots: false,
        margin: 10,
    })
        .on('click', '.owl-item', function (e) {
            e.preventDefault();
            sync1.trigger('to.owl.carousel', [$(this).index(), duration, true]);
        })
        .on('change.owl.carousel', function (e) {
            if (e.namespace && e.property.name === 'position' && !flag) {
                flag = true;
                sync1.trigger('to.owl.carousel', [e.relatedTarget.relative(e.property.value), duration, true]);
                flag = false;
            }
        });

    sync2.on('click', '.owl-item', function(e) {
        e.preventDefault();
        $('.item').removeClass('active');
        $(this).find('.item').addClass('active');
    });


}

function linkToUrl(url) {
    window.location = String(url);
}

//Function End