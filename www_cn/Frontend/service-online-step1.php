<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>
<?php include('header.php'); ?>
    <!-- CSS -->
    <link rel="stylesheet" href="<?= file_path('css/service-online.css') ?>" type="text/css">

    <div class="page-banner-md banner-hide">
        <img src="<?= file_path('images/temp2/banner_service_online.jpg') ?>" alt="">
    </div>

    <div id="content" class="content rps-content-pd">
        <div class="container">
            <h1 class="heading-title">แจ้งซ่อมออนไลน์</h1>
            <div class="">
                <div class="row">
                    <div class="col-md-4">
                        <form action="" class="service-form form-container">
                            <div class="">
                                <p class="title-label">Sign in</p>
                                <div class="form-group">
                                    <input type="text" name="" class="form-control" placeholder="USERNAME">
                                </div>

                                <div class="form-group">
                                    <input type="text" name="" class="form-control" placeholder="PASSWORD">
                                </div>

                                <button class="btn btn-submit">Sign in</button>
                                <button class="btn btn-cancel">Cancel</button>
                            </div>
                        </form>

                    </div>
                    <div class="col-md-6 col-md-offset-1">
                        <div class="service-details">
                            <p>บริการแจ้งซ่อม Online ให้ลูกช้าน LH สะดวก รวดเร็ว ในการแจ้งซ่อมเรื่องบ้านตลอด 24 ชม. สำหรับโครงการบ้านเดี่ยว และทาวน์โฮม ในกรุงเทพฯและปริมณฑล <span class="green">(บริการนี้ยังไม่รวมคอนโดและโครงการ LH ในจังหวัดอื่นๆ)</span> เมื่อแจ้งซ่อม Online แล้ว ทางเจ้าหน้าที่ Service Center จะติดต่อกลับในช่วงเวลาทำการ 9.00-17.30 น. ใน 24 ชม.</p>
                            <ul>
                                <li>ลูกบ้าน LH จะได้รับ Sign-in name และ Password เมื่อโอนบ้าน เพื่อเข้าใช้บริการ</li>
                                <li>สำหรับท่านที่ไม่ได้รับ Sign-in name และ Password สามารถติดต่อได้ที่ Service Center โทร 1198 กด 2 หรือ Email: <a href="mailto:c_service@lh.co.th">c_service@lh.co.th</a></li>
                                <li>ขอสงวนสิทธิ์ช่องทางนี้สำหรับการแจ้งซ่อมเท่านั้น หากลูกบ้านต้องการร้องเรียน เรื่องอื่นๆ ที่ไม่ใช่ซ่อมบ้าน กรุณาใช้บริการที่ "ร้องเรียนเรื่องบ้าน"</li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>


<?php include('footer.php'); ?>
