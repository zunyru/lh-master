<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>

<?php include('header.php'); ?>
<!-- CSS -->
<link rel="stylesheet" href="css/corporate-all.css" type="text/css">
<link rel="stylesheet" href="css/corporate-menu.css" type="text/css">
<link rel="stylesheet" href="css/governance.css" type="text/css">

<!-- JS -->
<script src="js/corporate-menu.js"></script>

<div id="content" class="content ethic-page corp-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">

                <h1>การกำกับดูแลกิจการที่ดี</h1>
                <div class="govern-block"></div>
                <div id="corpBlock" class="corp-block current">
                    <div>
                        <div class="">
                            <div class="col-govern-50">
                                <h2 class="title-gov">การกำกับดูแลกิจการที่ดี</h2>
                                <p>คณะกรรมการบริษัทมีความมุ่งมั่นที่จะบริหารงานขององค์กรให้เกิดประสิทธิภาพและบรรลุตามวัตถุประสงค์ โดยเชื่อมั่นเป็นอย่างยิ่งว่ากระบวนการกำกับดูแลกิจการที่ดี จะยกระดับผลการดำเนินงานของบริษัทได้อย่างยั่งยืน และเป็นหัวใจสำคัญที่จะนำไปสู่ความสำเร็จ อันได้แก่ การเพิ่มมูลค่าสูงสุดให้แก่ผู้ถือหุ้น ดังนั้น คณะกรรมการบริษัทจึงยึดมั่นในการปฏิบัติตามคุณลักษณะหลักของกระบวนการกำกับดูแลกิจการที่ดี อันประกอบด้วย
                                </p>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <ul class="pdl-40">
                                            <li>ความซื่อสัตย์</li>
                                            <li>ความโปร่งใส</li>
                                            <li>ความเป็นอิสระ</li>
                                        </ul>
                                    </div>

                                    <div class="col-sm-6">
                                        <ul>
                                            <li>ความรับผิดชอบต่อภาระหน้าที่</li>
                                            <li>ความเป็นธรรม</li>
                                            <li>ความรับผิดชอบต่อสังคม</li>
                                        </ul>
                                    </div>
                                </div>

                                    <p>โดยหลักการกำกับดูแลกิจการนี้ ได้สะท้อนถึงคุณค่า แนวปฏิบัติ และทิศทางที่ใช้ปฏิบัติภายในของบริษัทซึ่งจะช่วยสร้างความมั่นใจให้กับธุรกิจ สามารถแข่งขันได้ดี ภายใต้การดำเนินการอย่างมีจรรยาบรรณ มีมาตรฐานสากล และเป็นไปตามกฏหมาย โดยให้สอดคล้องกับหลักการกำกับดูแลกิจการที่ดีตามแนวทางของตลาดหลักทรัพย์แห่งประเทศไทย</p>
                            </div>
                            <div class="col-govern-50 col-govern-right">
                                <img src="images/governance/govern_business1.jpg" alt="" class="">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<?php include('footer.php'); ?>
