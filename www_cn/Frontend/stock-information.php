<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>

<?php include('header.php'); ?>
<!-- CSS -->
<link rel="stylesheet" href="css/investor.css" type="text/css">
<link rel="stylesheet" href="css/corporate-all.css" type="text/css">
<link rel="stylesheet" href="css/corporate-menu.css" type="text/css">

<!-- JS -->
<script src="js/corporate-menu.js"></script>

<div id="content" class="content invest-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">

                <h1>ข้อมูลราคาหลักทรัพย์</h1>

                <div id="corpMenu" class="corp-menu-tab">
                    <ul>
                        <li class="current"><a href="#corpBlock1">กราฟราคาหลักทรัพย์</a></li>
                        <li><a href="#corpBlock2">ราคาย้อนหลัง</a></li>
                        <li><a href="#corpBlock3">เครื่องคำนวณการลงทุน</a></li>
                    </ul>
                </div>

                <div id="corpBlock1" class="current corp-block">
                    <h2>กราฟราคาหลักทรัพย์</h2>
                    <p>ราคาหุ้น ณ เวลาปัจจุบัน
                        ณ เวลาทำการซื้อขายของตลาดหลักทรัพย์แห่งประเทศไทย</p>
                    <div class="stock-block">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table_stock">
                                    <tbody><tr class="row2">
                                        <td class="tb-label" width="43%">ชื่อย่อหุ้น:</td>
                                        <td class="price" width="57%">LH</td>
                                    </tr>
                                    <tr class="row1">
                                        <td class="tb-label">สกุลเงิน:</td>
                                        <td class="price">THB</td>
                                    </tr>
                                    <tr class="row2">
                                        <td class="tb-label">ราคาล่าสุด:</td>
                                        <td class="price">9.85</td>
                                    </tr>
                                    <tr class="row1">
                                        <td class="tb-label">ปริมาณซื้อขาย (หุ้น):</td>
                                        <td class="price">12,642,000</td>
                                    </tr>
                                    <tr class="row2">
                                        <td class="tb-label">เปลี่ยนแปลง</td>
                                        <td class="price"><span class="red">-0.10</span></td>
                                    </tr>
                                    <tr class="row1">
                                        <td class="tb-label">% เปลี่ยนแปลง</td>
                                        <td class="price"><span class="red">-1.01%</span></td>
                                    </tr>
                                    <tr class="row2">
                                        <td class="tb-label">Bid Price / Volume (Shares)</td>
                                        <td class="price">9.80 / 1,556,100</td>
                                    </tr>
                                    <tr class="row1">
                                        <td class="tb-label">ราคาเสนอขาย / ปริมาณ (หุ้น):</td>
                                        <td class="price">9.85 / 208,200</td>
                                    </tr>
                                    <tr class="row2">
                                        <td class="tb-label">วันก่อนหน้า:</td>
                                        <td class="price">9.95</td>
                                    </tr>
                                    <tr class="row1">
                                        <td class="tb-label">ราคาเปิด:</td>
                                        <td class="price">9.90</td>
                                    </tr>
                                    <tr class="row2">
                                        <td class="tb-label">ช่วงราคาระหว่างวัน:</td>
                                        <td class="price">9.80 - 9.95</td>
                                    </tr>
                                    <tr class="row1">
                                        <td class="tb-label">ช่วงราคาใน 52 สัปดาห์:</td>
                                        <td class="price">7.55 - 10.40</td>
                                    </tr>
                                    <tr class="row1">
                                        <td class="tb-label">ปรับปรุงเมื่อ:</td>
                                        <td class="price">01 กุมภาพันธ์ 2560 เวลา 16:37</td>
                                    </tr>
                                    </tbody></table>
                            </div>
                            <div class="col-md-6">
                                <img src="images/investor/stock_chart.jpg" alt="" class="stock-img">
                            </div>

                        </div>
                    </div>
                </div>

                <div id="corpBlock2" class="corp-block">
                    <h2>ราคาย้อนหลัง</h2>

                    <div>

                        <form name="date_filter" method="GET" action="">
                            <div class="ir_textMainHighlight"><span class="date-divide">เลือกช่วงวันที่:
                            จาก</span>

                            <div class="date-select">
                                <select name="start_day"><option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10" selected="selected">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            </div>
                            <span class="date-divide">/</span>
                            <div class="date-select">
                                <select name="start_month">
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11" selected="selected">11</option>
                                    <option value="12">12</option>
                                </select>
                            </div>
                            <span class="date-divide">/</span>
                            <div class="date-select">
                                <select name="start_year"><option value="2559" selected="selected">2559</option>
                                    <option value="2560">2560</option>
                                </select>
                            </div>
                            <span class="date-divide"> ถึง </span>

                            <input type="hidden" name="date_end" value="20170207">
                            <div class="date-select">
                                <select name="end_day"><option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07" selected="selected">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                </select>
                            </div>
                            <span class="date-divide">/</span>
                            <div class="date-select">
                                <select><option value="01">01</option>
                                    <option value="02" selected="selected">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                            </div>
                            <span class="date-divide">/</span>
                            <div class="date-select">
                                <select name="end_year"><option value="2559">2559</option>
                                    <option value="2560" selected="selected">2560</option>
                                </select>
                            </div>
                            <input type="submit" value=" ดูผล " class="submit-date">
                            </div>
                        </form>
                        <br>
                        ราคาย้อนหลัง จาก <b>10 พฤศจิกายน  2559</b> ถึง <b>07 กุมภาพันธ์  2560</b><br>
                        <br>
                        <table width="100%" cellspacing="1" cellpadding="3" border="0" class="table_historical_price ir_tableBorder si_right">
                            <thead>
                            <tr class="ir_tableTopRow">
                                <th class="si_left">วันที่</th>
                                <th>ราคาเปิด</th>
                                <th>สูงสุด</th>
                                <th>ต่ำสุด</th>
                                <th>ราคาปิด</th>
                                <th class="center">ปริมาณ</th>
                            </tr>
                            </thead>
                            <tbody>    <tr class="rowHighlight">
                                <td class="si_left" colspan="6"><strong>รายงานสรุป</strong></td>
                            </tr>   <tr class="row2">
                                <td class="si_left">ก่อนหน้า 2 สัปดาห์ (11/01/2560              to 24/01/2560)</td>
                                <td>10.00</td>
                                <td>10.30</td>
                                <td>9.35</td>
                                <td>9.75</td>
                                <td class="tb-right">226,053,300</td>
                            </tr>   <tr class="row1">
                                <td class="si_left">ก่อนหน้า 4 สัปดาห์ (09/12/2559              to 10/01/2560)</td>
                                <td>9.15</td>
                                <td>10.10</td>
                                <td>9.05</td>
                                <td>10.10</td>
                                <td class="tb-right">421,787,100</td>
                            </tr>    <tr class="rowHighlight">
                                <td class="si_left" colspan="6"><strong>ข้อมูลย้อนหลังระหว่างวัน</strong></td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">07/02/2560</td>
                                <td>9.90</td>
                                <td>9.95</td>
                                <td>9.80</td>
                                <td>9.85</td>
                                <td class="tb-right">12,642,000</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">06/02/2560</td>
                                <td>9.90</td>
                                <td>9.95</td>
                                <td>9.80</td>
                                <td>9.95</td>
                                <td class="tb-right">10,553,900</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">03/02/2560</td>
                                <td>9.75</td>
                                <td>9.95</td>
                                <td>9.70</td>
                                <td>9.90</td>
                                <td class="tb-right">30,533,500</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">02/02/2560</td>
                                <td>9.80</td>
                                <td>9.90</td>
                                <td>9.75</td>
                                <td>9.75</td>
                                <td class="tb-right">10,304,000</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">01/02/2560</td>
                                <td>9.65</td>
                                <td>9.90</td>
                                <td>9.65</td>
                                <td>9.85</td>
                                <td class="tb-right">9,555,800</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">31/01/2560</td>
                                <td>9.80</td>
                                <td>9.80</td>
                                <td>9.70</td>
                                <td>9.70</td>
                                <td class="tb-right">7,828,400</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">30/01/2560</td>
                                <td>9.80</td>
                                <td>9.90</td>
                                <td>9.75</td>
                                <td>9.80</td>
                                <td class="tb-right">8,212,400</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">27/01/2560</td>
                                <td>9.90</td>
                                <td>9.95</td>
                                <td>9.75</td>
                                <td>9.80</td>
                                <td class="tb-right">18,881,500</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">26/01/2560</td>
                                <td>9.95</td>
                                <td>10.10</td>
                                <td>9.80</td>
                                <td>10.00</td>
                                <td class="tb-right">24,230,000</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">25/01/2560</td>
                                <td>9.75</td>
                                <td>10.00</td>
                                <td>9.75</td>
                                <td>9.95</td>
                                <td class="tb-right">35,414,100</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">24/01/2560</td>
                                <td>9.80</td>
                                <td>9.85</td>
                                <td>9.70</td>
                                <td>9.75</td>
                                <td class="tb-right">8,671,400</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">23/01/2560</td>
                                <td>9.55</td>
                                <td>9.85</td>
                                <td>9.55</td>
                                <td>9.80</td>
                                <td class="tb-right">36,696,000</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">20/01/2560</td>
                                <td>9.50</td>
                                <td>9.60</td>
                                <td>9.45</td>
                                <td>9.55</td>
                                <td class="tb-right">21,961,600</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">19/01/2560</td>
                                <td>9.60</td>
                                <td>9.60</td>
                                <td>9.35</td>
                                <td>9.50</td>
                                <td class="tb-right">34,913,100</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">18/01/2560</td>
                                <td>9.85</td>
                                <td>9.85</td>
                                <td>9.55</td>
                                <td>9.60</td>
                                <td class="tb-right">40,725,800</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">17/01/2560</td>
                                <td>9.85</td>
                                <td>10.00</td>
                                <td>9.80</td>
                                <td>9.85</td>
                                <td class="tb-right">12,358,300</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">16/01/2560</td>
                                <td>10.10</td>
                                <td>10.10</td>
                                <td>9.85</td>
                                <td>9.85</td>
                                <td class="tb-right">16,191,700</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">13/01/2560</td>
                                <td>10.10</td>
                                <td>10.10</td>
                                <td>9.95</td>
                                <td>10.10</td>
                                <td class="tb-right">11,186,300</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">12/01/2560</td>
                                <td>10.20</td>
                                <td>10.20</td>
                                <td>9.95</td>
                                <td>10.00</td>
                                <td class="tb-right">13,774,500</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">11/01/2560</td>
                                <td>10.00</td>
                                <td>10.30</td>
                                <td>9.95</td>
                                <td>10.20</td>
                                <td class="tb-right">29,574,600</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">10/01/2560</td>
                                <td>9.70</td>
                                <td>10.10</td>
                                <td>9.70</td>
                                <td>10.10</td>
                                <td class="tb-right">34,947,100</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">09/01/2560</td>
                                <td>9.85</td>
                                <td>9.90</td>
                                <td>9.70</td>
                                <td>9.75</td>
                                <td class="tb-right">20,993,000</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">06/01/2560</td>
                                <td>9.80</td>
                                <td>9.95</td>
                                <td>9.70</td>
                                <td>9.85</td>
                                <td class="tb-right">29,494,500</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">05/01/2560</td>
                                <td>9.80</td>
                                <td>9.95</td>
                                <td>9.80</td>
                                <td>9.85</td>
                                <td class="tb-right">32,953,100</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">04/01/2560</td>
                                <td>9.70</td>
                                <td>9.85</td>
                                <td>9.70</td>
                                <td>9.70</td>
                                <td class="tb-right">18,656,100</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">30/12/2559</td>
                                <td>9.90</td>
                                <td>9.90</td>
                                <td>9.65</td>
                                <td>9.80</td>
                                <td class="tb-right">23,823,000</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">29/12/2559</td>
                                <td>9.85</td>
                                <td>10.00</td>
                                <td>9.65</td>
                                <td>9.90</td>
                                <td class="tb-right">29,027,300</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">28/12/2559</td>
                                <td>9.75</td>
                                <td>9.95</td>
                                <td>9.75</td>
                                <td>9.85</td>
                                <td class="tb-right">46,625,100</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">27/12/2559</td>
                                <td>9.45</td>
                                <td>9.80</td>
                                <td>9.45</td>
                                <td>9.75</td>
                                <td class="tb-right">35,212,800</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">26/12/2559</td>
                                <td>9.50</td>
                                <td>9.60</td>
                                <td>9.40</td>
                                <td>9.45</td>
                                <td class="tb-right">12,005,300</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">23/12/2559</td>
                                <td>9.25</td>
                                <td>9.55</td>
                                <td>9.20</td>
                                <td>9.45</td>
                                <td class="tb-right">24,638,900</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">22/12/2559</td>
                                <td>9.30</td>
                                <td>9.30</td>
                                <td>9.20</td>
                                <td>9.25</td>
                                <td class="tb-right">5,939,200</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">21/12/2559</td>
                                <td>9.30</td>
                                <td>9.35</td>
                                <td>9.20</td>
                                <td>9.25</td>
                                <td class="tb-right">8,717,800</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">20/12/2559</td>
                                <td>9.20</td>
                                <td>9.35</td>
                                <td>9.15</td>
                                <td>9.25</td>
                                <td class="tb-right">21,611,800</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">19/12/2559</td>
                                <td>9.20</td>
                                <td>9.30</td>
                                <td>9.15</td>
                                <td>9.15</td>
                                <td class="tb-right">21,905,300</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">16/12/2559</td>
                                <td>9.15</td>
                                <td>9.20</td>
                                <td>9.15</td>
                                <td>9.20</td>
                                <td class="tb-right">9,157,100</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">15/12/2559</td>
                                <td>9.20</td>
                                <td>9.25</td>
                                <td>9.05</td>
                                <td>9.10</td>
                                <td class="tb-right">11,549,900</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">14/12/2559</td>
                                <td>9.20</td>
                                <td>9.30</td>
                                <td>9.15</td>
                                <td>9.25</td>
                                <td class="tb-right">16,770,600</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">13/12/2559</td>
                                <td>9.15</td>
                                <td>9.20</td>
                                <td>9.05</td>
                                <td>9.15</td>
                                <td class="tb-right">7,676,400</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">09/12/2559</td>
                                <td>9.15</td>
                                <td>9.20</td>
                                <td>9.05</td>
                                <td>9.15</td>
                                <td class="tb-right">10,082,800</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">08/12/2559</td>
                                <td>9.00</td>
                                <td>9.20</td>
                                <td>9.00</td>
                                <td>9.15</td>
                                <td class="tb-right">19,567,200</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">07/12/2559</td>
                                <td>9.00</td>
                                <td>9.10</td>
                                <td>9.00</td>
                                <td>9.00</td>
                                <td class="tb-right">10,529,000</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">06/12/2559</td>
                                <td>9.05</td>
                                <td>9.15</td>
                                <td>9.00</td>
                                <td>9.05</td>
                                <td class="tb-right">12,046,900</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">02/12/2559</td>
                                <td>9.10</td>
                                <td>9.15</td>
                                <td>9.00</td>
                                <td>9.05</td>
                                <td class="tb-right">7,622,700</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">01/12/2559</td>
                                <td>9.30</td>
                                <td>9.35</td>
                                <td>9.10</td>
                                <td>9.20</td>
                                <td class="tb-right">14,850,600</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">30/11/2559</td>
                                <td>9.20</td>
                                <td>9.40</td>
                                <td>9.05</td>
                                <td>9.30</td>
                                <td class="tb-right">34,640,700</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">29/11/2559</td>
                                <td>9.10</td>
                                <td>9.20</td>
                                <td>9.05</td>
                                <td>9.15</td>
                                <td class="tb-right">13,392,400</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">28/11/2559</td>
                                <td>9.05</td>
                                <td>9.10</td>
                                <td>9.00</td>
                                <td>9.05</td>
                                <td class="tb-right">6,855,000</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">25/11/2559</td>
                                <td>9.00</td>
                                <td>9.05</td>
                                <td>8.95</td>
                                <td>9.05</td>
                                <td class="tb-right">5,147,200</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">24/11/2559</td>
                                <td>8.95</td>
                                <td>9.05</td>
                                <td>8.90</td>
                                <td>8.95</td>
                                <td class="tb-right">13,635,100</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">23/11/2559</td>
                                <td>8.75</td>
                                <td>8.95</td>
                                <td>8.75</td>
                                <td>8.95</td>
                                <td class="tb-right">15,948,300</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">22/11/2559</td>
                                <td>8.85</td>
                                <td>8.90</td>
                                <td>8.70</td>
                                <td>8.75</td>
                                <td class="tb-right">11,502,900</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">21/11/2559</td>
                                <td>8.75</td>
                                <td>8.85</td>
                                <td>8.75</td>
                                <td>8.80</td>
                                <td class="tb-right">7,222,400</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">18/11/2559</td>
                                <td>8.75</td>
                                <td>8.75</td>
                                <td>8.65</td>
                                <td>8.75</td>
                                <td class="tb-right">13,315,700</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">17/11/2559</td>
                                <td>8.90</td>
                                <td>8.95</td>
                                <td>8.65</td>
                                <td>8.70</td>
                                <td class="tb-right">24,394,800</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">16/11/2559</td>
                                <td>8.90</td>
                                <td>8.95</td>
                                <td>8.80</td>
                                <td>8.85</td>
                                <td class="tb-right">9,122,700</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">15/11/2559</td>
                                <td>8.95</td>
                                <td>9.00</td>
                                <td>8.80</td>
                                <td>8.90</td>
                                <td class="tb-right">19,860,600</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">14/11/2559</td>
                                <td>8.90</td>
                                <td>9.05</td>
                                <td>8.90</td>
                                <td>8.95</td>
                                <td class="tb-right">12,131,100</td>
                            </tr>
                            <tr class="row1">
                                <td class="si_left">11/11/2559</td>
                                <td>9.05</td>
                                <td>9.15</td>
                                <td>8.95</td>
                                <td>9.00</td>
                                <td class="tb-right">21,706,400</td>
                            </tr>
                            <tr class="row2">
                                <td class="si_left">10/11/2559</td>
                                <td>9.15</td>
                                <td>9.15</td>
                                <td>8.95</td>
                                <td>9.05</td>
                                <td class="tb-right">22,137,700</td>
                            </tr>  </tbody></table>

                        <p class="txt-stockremark right">หมายเหตุ: ปริมาณการซื้อขายบนกระดานหลัก (Main board) เท่านั้น</p>
                    </div>
                </div>

                <div id="corpBlock3" class="corp-block">
                    <h2>เครื่องคำนวณการลงทุน</h2>
                    <p><strong>วิธีการใช้งาน:</strong>
                        ในการคำนวณมูลค่ากำไร หรือ ขาดทุน กรุณากรอกข้อมูลใน 3 ช่องดังต่อไปนี้ : <span class="green">"ราคาที่ซื้อ", "จำนวนหุ้นที่ถือ" และ "ราคาขาย"</span></p>
                    <div class="stock-block cal-block">
                        <table class="tableCal" cellpadding="0" cellspacing="0" width="100%">
                            <tbody><tr>
                                <td class="tb-label">เครื่องคำนวณ การลงทุน</td>
                                <td><input type="text" name="shares_held" onfocus="document.calculator.shares_held_enter.select();" class="ir_investmentCalculator calTextbox"></td>
                                <td class="tb-label">จำนวนหุ้นที่ถือ</td>
                                <td><input type="text" name="shares_held" onfocus="document.calculator.shares_held_enter.select();" class="ir_investmentCalculator calTextbox"></td>
                            </tr>
                            <tr>
                                <td class="tb-label">ราคาซื้อ (บาท)</td>
                                <td><input type="text" name="price_purchase_enter" onchange="javascript:ir_calculateAmount();" class="calTextbox"></td>
                                <td class="tb-label">ราคาที่ซื้อต่อหุ้น (บาท)</td>
                                <td><input type="text" name="price_purchase" onfocus="document.calculator.price_purchase_enter.select();" class="ir_investmentCalculator calTextbox"></td>
                            </tr>
                            <tr>
                                <td class="tb-label">จำนวนหุ้นที่ถือ</td>
                                <td><input type="text" name="shares_held_enter" onchange="javascript:ir_calculateAmount();" class="calTextbox"></td>
                                <td class="tb-label">ราคาที่ขาย (บาท)</td>
                                <td><input type="text" name="price_sold" onfocus="document.calculator.price_sold_enter.select();" class="ir_investmentCalculator calTextbox"></td>
                            </tr>
                            <tr>
                                <td class="tb-label">ค่าคอมมิสชั่น (%)</td>
                                <td><input type="text" name="commission_enter" value="0.2578" onchange="javascript:ir_calculateAmount();" class="calTextbox"></td>
                                <td class="tb-label">กำไร(ขาดทุน) ขั้นต้น ก่อนหักค่าคอมมิชชั่น  (บาท)</td>
                                <td><input type="text" name="profit" onfocus="document.calculator.shares_held_enter.select();" class="ir_investmentCalculator calTextbox"></td>
                            </tr>
                            <tr>
                                <td class="tb-label">คอมมิสชั่นขั้นต่ำ (บาท)</td>
                                <td><input type="text" name="min_commission_enter" value="50" onchange="javascript:ir_calculateAmount();" class="calTextbox"></td>
                                <td class="tb-label">จำนวนค่าคอมมิชชั่น (บาท)</td>
                                <td><input type="text" name="commission" onfocus="document.calculator.commission_enter.select();" class="ir_investmentCalculator calTextbox"></td>
                            </tr>
                            <tr>
                                <td class="tb-label">ภาษีมูลค่าเพิ่ม</td>
                                <td><input type="text" name="vat_enter" value="7" onchange="javascript:ir_calculateAmount();" class="calTextbox"></td>
                                <td class="tb-label">ภาษีมูลค่าเพิ่มขั้นต่ำ (บาท)</td>
                                <td><input type="text" name="vat" onfocus="document.calculator.vat_enter.select();" class="ir_investmentCalculator calTextbox"></td>
                            </tr>
                            <tr>
                                <td class="tb-label">ราคาขาย (บาท) (%)</td>
                                <td><input type="text" name="price_sold_enter" value="" onchange="javascript:ir_calculateAmount();" class="calTextbox"></td>
                                <td class="tb-label">กำไร(ขาดทุน)สุทธิ (บาท)</td>
                                <td><input type="text" name="net_profit" onfocus="document.calculator.shares_held_enter.select();" class="ir_investmentCalculator calTextbox"></td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                                <td class="tb-label">คิดเป็นเป็นร้อยละของหน่วยลงทุน (%)</td>
                                <td><input type="text" name="invest_change_percent" onfocus="document.calculator.shares_held_enter.select();" class="ir_investmentCalculator calTextbox"></td>
                            </tr>
                            <tr>
                                <td class="tb-label"><strong>เงินปันผลต่อหุ้น</strong></td>
                                <td>&nbsp;</td>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="tb-label">หักภาษี (บาท)</td>
                                <td><input type="text" name="dividend_taxed_enter" onchange="javascript:ir_calculateAmount();" class="calTextbox"></td>
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <td class="tb-label">ยกเว้นภาษี (บาท)</td>
                                <td><input type="text" name="dividend_tax_exempt_enter" onchange="javascript:ir_calculateAmount();" class="calTextbox"></td>
                                <td class="tb-label">เงินปันผลขั้นต้น (บาท)</td>
                                <td><input type="text" name="net_dividend" onfocus="document.calculator.dividend_taxed_enter.select();" class="ir_investmentCalculator"></td>
                            </tr>
                            </tbody></table>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>


<?php include('footer.php'); ?>
