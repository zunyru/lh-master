<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>

<?php include('header.php'); ?>
    <!-- CSS -->
    <link rel="stylesheet" href="css/honor-awards.css" type="text/css">
    <!-- JS -->
    <script src="js/honor-awards.js"></script>

    <div id="content" class="content">
        <div class="container">

            <h1 class="heading-title">รางวัลเกียรติยศ</h1>

            <div id="awardList" class="home-section">
                <div class="row">
                    <div class="award-lists">
                        <div class="list-item item">
                            <div class="review-img">
                                <img src="images/temp4/award_1.jpg" alt="">
                            </div>
                            <p class="title green">โครงการคอนโดมิเนียม The Bangkok สาทร รับรางวัล International</p>
                        </div>
                        <div class="list-item item">
                            <div class="review-img">
                                <img src="images/temp4/award_2.jpg" alt="">
                            </div>
                            <p class="title green">แลนด์ แอนด์ เฮ้าส์ รับรางวัล Thailand Energy Awards 2013</p>
                        </div>
                        <div class="list-item item">
                            <div class="review-img">
                                <img src="images/temp4/award_3.jpg" alt="">
                            </div>
                            <p class="title green">Trusted Brand 2556</p>
                        </div>
                        <div class="list-item item">
                            <div class="review-img">
                                <img src="images/temp4/award_6.jpg" alt="">
                            </div>

                            <p class="title green">Trusted Brand 2011</p>
                        </div>
                        <div class="list-item item">
                            <div class="review-img">
                                <img src="images/temp4/award_4.jpg" alt="">
                            </div>
                            <p class="title green">แลนด์ แอนด์ เฮ้าส์ รับรางวัล โครงการอสังหาริมทรัพย์ดีเด่น</p>
                        </div>
                        <div class="list-item item">
                            <div class="review-img">
                                <img src="images/temp4/award_5.jpg" alt="">
                            </div>
                            <p class="title green">แลนด์ แอนด์ เฮ้าส์ รับรางวัล Trusted Brand 2012</p>
                        </div>
                        <div class="list-item item">
                            <div class="review-img">
                                <img src="images/temp4/award_7.jpg" alt="">
                            </div>
                            <p class="title green">แลนด์ แอนด์ เฮ้าส์ รับรางวัล Truehits Web Award 2010</p>
                        </div>
                        <div class="list-item item">
                            <div class="review-img">
                                <img src="images/temp4/award_8.jpg" alt="">
                            </div>
                            <p class="title green">แลนด์ แอนด์ เฮ้าส์ ได้รับเลือกเป็น 1 ใน 10 บริษัทชั้นนำของประเทศไทย</p>
                        </div>
                        <div class="list-item item">
                            <div class="review-img">
                                <img src="images/temp4/award_9.jpg" alt="">
                            </div>
                            <p class="title green">แลนด์ แอนด์ เฮ้าส์ รับรางวัล Trusted Brand 2009</p>
                        </div>
                    </div>
                </div>
                <a href="" class="btn btn-seemore">See more</a>
            </div>

        </div>
    </div>

<?php include('footer.php'); ?>