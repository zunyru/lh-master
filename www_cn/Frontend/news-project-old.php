<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>
<?php include('header.php'); ?>
    <!-- CSS -->
    <link rel="stylesheet" href="css/news-project.css" type="text/css">
    <!-- JS -->
    <script src="js/news-project.js"></script>

    <div class="page-banner page-scroll">
        <span id="scrollNext" class="i-scroll-next"></span>

        <div id="bannerSlide">
            <div class="item">
                <div class="banner-parallax" style="background-image: url(images/temp3/banner_newsproj.jpg);">
                </div>
            </div>
        </div>
    </div>

    <div id="content" class="content news-project-page">
        <div class="container">
            <h1>ข่าวโครงการใหม่</h1>
            <p class="title">พบข่าวโครงการใหม่ 10 รายการ</p>
            <div class="news-block">
                <div class="for-desktop">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="left">
                                <img src="images/logo/logo_mtn.jpg" alt="" class="logo-project">
                                <!--                            <h3>บ้านมัณฑนา ราชพฤกษ์-สะพานมหาเจษฎาบดินทร์ฯ</h3>-->
                            </div>
                        </div>
                        <div class="col-md-6">
                            <p>พิสูจน์แบบบ้านซีรี่ย์ล่าสุด ครั้งแรกที่โครงการใหม่ กับการปฏิวัติความคิดการออกแบบฟังก์ชั่นใหม่
                                โครงการใหม่ เพียง 5 กม. จากเซ็นทรัลรัตนาธิเบศร์ เริ่ม 7 - 16 ล้าน ลงทะเบียนเพื่อรับสิทธิ์ VIP
                                ก่อนใคร พิสูจน์แบบบ้านซีรี่ย์ล่าสุด ครั้งแรกที่โครงการใหม่ กับการปฏิวัติความคิดการออกแบบฟังก์
                                ชั่นใหม่ โครงการใหม่ เพียง 5 กม. จากเซ็นทรัลรัตนาธิเบศร์ เริ่ม 7 - 16 ล้าน ลงทะเบียนเพื่อรับสิทธิ์
                                VIP ก่อนใคร</p>
                            <a href="" class="btn btn-seemore">See more</a>
                            <a href="" class="btn btn-register">Register</a>
                        </div>

                        <div class="clear"></div>
                    </div>
                    <div class="news-images-block tpl5-img">
                        <div class="row">
                            <div id="tpl5-img" class="tpl5-img-slide">
                                <div class="item tpl5-img-1">
                                    <a class="gallery" href="images/temp/news_1.jpg">
                                        <img src="images/temp/news_1.jpg" alt="">
                                        <p class="title-gallery">หน้าโครงการ</p>
                                    </a>
                                </div>
                                <div class="item tpl5-img-2">
                                    <a class="gallery" href="images/temp/news_2.jpg">
                                        <img src="images/temp/news_2.jpg" alt="">
                                        <p class="title-gallery">สระว่ายน้ำขนาดใหญ่</p>
                                    </a>
                                </div>
                                <div class="item tpl5-img-3">
                                    <a class="gallery" href="images/temp/news_3.jpg">
                                        <img src="images/temp/news_3.jpg" alt="">
                                        <p class="title-gallery">สระว่ายน้ำขนาดใหญ่</p>
                                    </a>
                                </div>
                                <div class="item tpl5-img-4">
                                    <a class="gallery" href="images/temp/news_4.jpg">
                                        <img src="images/temp/news_4.jpg" alt="">
                                        <p class="title-gallery">สระว่ายน้ำขนาดใหญ่</p>
                                    </a>
                                </div>
                                <div class="item tpl5-img-5">
                                    <a class="gallery" href="images/temp/news_5.jpg">
                                        <img src="images/temp/news_5.jpg" alt="">
                                        <p class="title-gallery">สระว่ายน้ำขนาดใหญ่</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="for-mobile">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="left">
                                <img src="images/logo/logo_mtn.jpg" alt="" class="logo-project">
                                <!--                            <h3>บ้านมัณฑนา ราชพฤกษ์-สะพานมหาเจษฎาบดินทร์ฯ</h3>-->
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="news-images-block tpl5-img">
                        <div class="row">
                            <div id="tpl5-img" class="tpl5-img-slide">
                                <div class="item tpl5-img-1">
                                    <a class="gallery" href="images/temp/news_1.jpg">
                                        <img src="images/temp/news_1.jpg" alt="">
                                        <p class="title-gallery">หน้าโครงการ</p>
                                    </a>
                                </div>
                                <div class="item tpl5-img-2">
                                    <a class="gallery" href="images/temp/news_2.jpg">
                                        <img src="images/temp/news_2.jpg" alt="">
                                        <p class="title-gallery">สระว่ายน้ำขนาดใหญ่</p>
                                    </a>
                                </div>
                                <div class="item tpl5-img-3">
                                    <a class="gallery" href="images/temp/news_3.jpg">
                                        <img src="images/temp/news_3.jpg" alt="">
                                        <p class="title-gallery">สระว่ายน้ำขนาดใหญ่</p>
                                    </a>
                                </div>
                                <div class="item tpl5-img-4">
                                    <a class="gallery" href="images/temp/news_4.jpg">
                                        <img src="images/temp/news_4.jpg" alt="">
                                        <p class="title-gallery">สระว่ายน้ำขนาดใหญ่</p>
                                    </a>
                                </div>
                                <div class="item tpl5-img-5">
                                    <a class="gallery" href="images/temp/news_5.jpg">
                                        <img src="images/temp/news_5.jpg" alt="">
                                        <p class="title-gallery">สระว่ายน้ำขนาดใหญ่</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <p>พิสูจน์แบบบ้านซีรี่ย์ล่าสุด ครั้งแรกที่โครงการใหม่ กับการปฏิวัติความคิดการออกแบบฟังก์ชั่นใหม่
                                โครงการใหม่ เพียง 5 กม. จากเซ็นทรัลรัตนาธิเบศร์ เริ่ม 7 - 16 ล้าน ลงทะเบียนเพื่อรับสิทธิ์ VIP
                                ก่อนใคร พิสูจน์แบบบ้านซีรี่ย์ล่าสุด ครั้งแรกที่โครงการใหม่ กับการปฏิวัติความคิดการออกแบบฟังก์
                                ชั่นใหม่ โครงการใหม่ เพียง 5 กม. จากเซ็นทรัลรัตนาธิเบศร์ เริ่ม 7 - 16 ล้าน ลงทะเบียนเพื่อรับสิทธิ์
                                VIP ก่อนใคร</p>
                            <a href="" class="btn btn-seemore">See more</a>
                            <a href="" class="btn btn-register">Register</a>
                        </div>

                        <div class="clear"></div>
                    </div>
                </div>
            </div>

            <div class="news-block">
                <div class="for-desktop">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="left">
                                <img src="images/logo/logo_indy.jpg" alt="" class="logo-project">
                                <!--                            <h3>ทาวน์โฮม indy บางนา กม.7(2)</h3>-->
                            </div>
                        </div>
                        <div class="col-md-6">
                            <p>พิสูจน์แบบบ้านซีรี่ย์ล่าสุด ครั้งแรกที่โครงการใหม่ กับการปฏิวัติความคิดการออกแบบฟังก์ชั่นใหม่
                                โครงการใหม่ เพียง 5 กม. จากเซ็นทรัลรัตนาธิเบศร์ เริ่ม 7 - 16 ล้าน ลงทะเบียนเพื่อรับสิทธิ์ VIP
                                ก่อนใคร พิสูจน์แบบบ้านซีรี่ย์ล่าสุด ครั้งแรกที่โครงการใหม่ กับการปฏิวัติความคิดการออกแบบฟังก์
                                ชั่นใหม่ โครงการใหม่ เพียง 5 กม. จากเซ็นทรัลรัตนาธิเบศร์ เริ่ม 7 - 16 ล้าน ลงทะเบียนเพื่อรับสิทธิ์
                                VIP ก่อนใคร</p>
                            <a href="" class="btn btn-seemore">See more</a>
                        </div>

                        <div class="clear"></div>
                    </div>
                    <div class="news-images-block tpl4-img">
                        <div class="row">
                            <div id="tpl4-img" class="tpl4-img-slide">
                                <div class="item tpl4-img-1">
                                    <a class="gallery2" href="images/temp/news_6.jpg">
                                        <img src="images/temp/news_6.jpg" alt="">
                                        <p class="title-gallery">ตัวอย่างโครงการ</p>
                                    </a>
                                </div>
                                <div class="item tpl4-img-2">
                                    <a class="gallery2" href="images/temp/news_7.jpg">
                                        <img src="images/temp/news_7.jpg" alt="">
                                        <p class="title-gallery">ตัวอย่างโครงการ</p>
                                    </a>
                                </div>
                                <div class="item tpl4-img-3">
                                    <a class="gallery2" href="images/temp/news_8.jpg">
                                        <img src="images/temp/news_8.jpg" alt="">
                                        <p class="title-gallery">ตัวอย่างโครงการ</p>
                                    </a>
                                </div>
                                <div class="item tpl4-img-4">
                                    <a class="gallery2" href="images/temp/news_9.jpg">
                                        <img src="images/temp/news_9.jpg" alt="">
                                        <p class="title-gallery">ตัวอย่างโครงการ</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="for-mobile">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="left">
                                <img src="images/logo/logo_indy.jpg" alt="" class="logo-project">
                                <!--                            <h3>ทาวน์โฮม indy บางนา กม.7(2)</h3>-->
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="news-images-block tpl4-img">
                        <div class="row">
                            <div id="tpl4-img" class="tpl4-img-slide">
                                <div class="item tpl4-img-1">
                                    <a class="gallery2" href="images/temp/news_6.jpg">
                                        <img src="images/temp/news_6.jpg" alt="">
                                        <p class="title-gallery">ตัวอย่างโครงการ</p>
                                    </a>
                                </div>
                                <div class="item tpl4-img-2">
                                    <a class="gallery2" href="images/temp/news_7.jpg">
                                        <img src="images/temp/news_7.jpg" alt="">
                                        <p class="title-gallery">ตัวอย่างโครงการ</p>
                                    </a>
                                </div>
                                <div class="item tpl4-img-3">
                                    <a class="gallery2" href="images/temp/news_8.jpg">
                                        <img src="images/temp/news_8.jpg" alt="">
                                        <p class="title-gallery">ตัวอย่างโครงการ</p>
                                    </a>
                                </div>
                                <div class="item tpl4-img-4">
                                    <a class="gallery2" href="images/temp/news_9.jpg">
                                        <img src="images/temp/news_9.jpg" alt="">
                                        <p class="title-gallery">ตัวอย่างโครงการ</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <p>พิสูจน์แบบบ้านซีรี่ย์ล่าสุด ครั้งแรกที่โครงการใหม่ กับการปฏิวัติความคิดการออกแบบฟังก์ชั่นใหม่
                                โครงการใหม่ เพียง 5 กม. จากเซ็นทรัลรัตนาธิเบศร์ เริ่ม 7 - 16 ล้าน ลงทะเบียนเพื่อรับสิทธิ์ VIP
                                ก่อนใคร พิสูจน์แบบบ้านซีรี่ย์ล่าสุด ครั้งแรกที่โครงการใหม่ กับการปฏิวัติความคิดการออกแบบฟังก์
                                ชั่นใหม่ โครงการใหม่ เพียง 5 กม. จากเซ็นทรัลรัตนาธิเบศร์ เริ่ม 7 - 16 ล้าน ลงทะเบียนเพื่อรับสิทธิ์
                                VIP ก่อนใคร</p>
                            <a href="" class="btn btn-seemore">See more</a>
                        </div>

                        <div class="clear"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>


<?php include('footer.php'); ?>