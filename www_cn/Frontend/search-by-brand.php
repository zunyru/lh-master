<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

$brand_id   = $_GET['brand_id'];
$product_id = $_GET['product_id'];

if (empty($brand_id) || getProjectByBrand($brand_id) == false) {
    echo 'not have a home series for this series id';
}

$project_brands = $product_id != 4 ? getProjectByBrand($brand_id) : getHomeSellByBrandID($brand_id);
$brand    = getBrandByID($brand_id);
$logo_brand = $brand->logo_brand_th;

?>
<?php include('header.php'); ?>
    <!-- CSS -->
    <link rel="stylesheet" href="css/split-home-series.css" type="text/css">
    <!--JS-->
    <script src="js/split-home-series.js"></script>


    <div id="content" class="content project-info-page">

        <h1 class="heading-logo"><img src="<?= backend_url('logo',$logo_brand)?>" alt=""></h1>

        <div class="container">

            <?php
            foreach ($project_brands as $i => $project) {
                if(empty($project->home_sell_id)){
                    $project_id = $project->project_id;
                    $project = getProjectByID($project_id);
                    $banner  = getBannerByProjectID($project_id);
                    $brand   = getBrandByID($project->brand_id);
                    $price   = getProjectPrice($project_id);
                }else{
                    $home_sell = $project;
                    $project_sub = getProjectSubBySubID($home_sell->project_sub_id);
                    $project = getProjectByID($project_sub->project_id);
                    $banner  = getBannerByProjectID($project_sub->project_id);
                    $brand   = getBrandByID($project->brand_id);
                    $home_sell_imgs = getHomeSellImgByHomeSellID($home_sell->home_sell_id);
                    $lhPlan         = getPlanByPlanID($home_sell->plan_id);
                    $conditionHomeSell = getConditionHomeSellByHomeSellID($home_sell->home_sell_id);
                    $project_concept = getProjectConcept($project_sub->project_id);
                }

                ?>
                <div class="block-margin">
                    <div class="item">
                        <div class="row">
                            <div class="col-md-8 <?= $i % 2 == 0 ? 'pull-left' : 'pull-right' ?>">
                                <?php
                                if(empty($project->home_sell_id)){
                                    ?>
                                    <?php
                                    if($banner != false && $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'image'){
                                        ?>
                                        <img src="<?= backend_url('base',$banner->LEAD_IMAGE_PROJECT_FILE_NAME)?>" alt="">
                                        <?php
                                    }elseif($banner != false && $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'vdo'){
                                        parse_str(parse_url($banner->LEAD_IMAGE_PROJECT_FILE_NAME)['query'], $query_first);
                                        $lead_img = "https://img.youtube.com/vi/" . $query_first['v'] . "/maxresdefault.jpg";
                                        $lead_youtube_url = str_replace('watch?v=', 'embed/', $banner->LEAD_IMAGE_PROJECT_FILE_NAME);
                                        ?>

                                        <a href="#" data-featherlight="#lbVdo<?= $i ?>">
                                            <span class="i-view-vdo"></span>
                                            <img src="<?= $lead_img ?>" alt="" class="hm-highl">
                                        </a>
                                        <iframe class="lightbox" src="<?= $lead_youtube_url . "?autoplay=0" ?>"
                                                width="1000"
                                                height="560" id="lbVdo<?= $i ?>" style="border:none;" webkitallowfullscreen
                                                mozallowfullscreen allowfullscreen></iframe>

                                        <?php
                                    }
                                    ?>
                                    <?php
                                }else{
                                    ?>
                                    <?php
                                    if (!empty($home_sell_imgs) && $home_sell_imgs != false) {
                                        ?>
                                        <img src="<?= backend_url('base', $home_sell_imgs[0]->home_sell_img_name) ?>" alt="">
                                        <?php
                                    }
                                    ?>
                                <?php
                                }
                                ?>

                            </div>
                            <div class="col-md-4 pull-left">
                                <?php
                                if(empty($project->home_sell_id) && !empty($price)){
                                    ?>
                                    <div class="promo-descrp">
                                        <p class="title"><?= $project->project_name_th ?></p>
                                        <p>ราคาเริ่มต้น <?= $price->project_price  ?> ล้านบาท</p>
                                        <a href="<?php
                                        if($product_id == 1){
                                            url('project-info-home.php?project_id='.$project_id.'&product_id='.$product_id);
                                        }elseif($product_id == 2){
                                            url('project-info-home.php?project_id='.$project_id.'&product_id='.$product_id);
                                        }elseif($product_id == 3){
                                            url('project-info-condominium.php?project_id='.$project_id);
                                        }
                                        ?>"
                                           class="btn btn-seemore">See more</a>
                                    </div>
                                <?php
                                }else{
                                    ?>
                                    <div class="promo-descrp">
                                        <p class="title">แบบบ้าน <?= $lhPlan->plan_name_th ?></p>
                                        <p>ราคาเริ่มต้น <?= $conditionHomeSell->total_price ?> ล้านบาท</p>
                                        <a href="<?php url('/project-info-furnished.php?home_sell_id=' . $home_sell->home_sell_id) ?>"
                                           class="btn btn-seemore">See more</a>
                                    </div>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

        </div>

        <?php
        if (false) {
            ?>
            <div class="container">
                <p class="heading-title header-margin">ภาพ 360 องศา</p>
                <div class="project-info-other-block block-margin">
                    <div class="row">
                        <?php
                        foreach ($plan_series as $i => $plan_item) {
                            $planImg = getGalleryPlanByPlanID($plan_item->plan_id);
                            $lhPlan = getPlanByPlanID($plan_item->plan_id);
                            $lhPlan360 = get360Plan($plan_item->plan_id);
                            if ($lhPlan360 != false) {
                                ?>
                                <div class="col-md-6">
                                    <div class="project-info-other-img">
                                        <a href="#" data-featherlight="#lb360">
                                            <span class="i-view-360"></span>
                                            <img src="<?php if ($planImg != false) echo backend_url('plan_img', $planImg->galery_plan_name) ?>"
                                                 alt="" class="img100">
                                        </a>
                                        <iframe class="lightbox vdoview"
                                                src="<?php if ($lhPlan360 != false) echo $lhPlan360->c360_plan_url ?>"
                                                width="1000"
                                                height="560" id="lb360" style="border:none;" webkitallowfullscreen
                                                mozallowfullscreen allowfullscreen></iframe>
                                    </div>
                                    <p class="title"><?= $lhPlan->plan_name_th ?></p>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>


            <div class="container">
                <div class="project-info-detail-block block-margin">
                    <div class="gallery-block block-margin">
                        <p class="heading-title header-margin">แกลอรี</p>
                        <div class="tpl5-img">
                            <div id="tpl5-img">

                                <?php
                                $gl_plans = $plan_series != false ? getAllGalleryPlanByPlanID($plan_series[0]->plan_id) : [];
                                foreach ($gl_plans as $gl_plan) {
                                    ?>
                                    <div class="item tpl5-img-1">
                                        <a class="gallery"
                                           href="<?= backend_url('plan_img', $gl_plan->galery_plan_name) ?>">
                                            <img src="<?= backend_url('plan_img', $gl_plan->galery_plan_name) ?>"
                                                 alt="">
                                            <p class="title-gallery"></p>
                                        </a>
                                    </div>
                                    <?php

                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>

    </div>

<?php include('footer.php'); ?>