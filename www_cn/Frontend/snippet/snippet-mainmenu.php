<div class="submenu-bg">
    <div class="submenu-container">
        <div class="container">
            <div class="row">
                <div class="submenu-list">
                    <div class="col-md-7 col-md-offset-1">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="submenu-block">
                                    <p class="submenu-title">项目内容</p>
                                    <ul>
                                       <!--  <li><a href="<?= $router->generate('furnished-home-list') ?>">出售精装房</a></li> -->
                                        <li><a href="<?= $router->generate('single-home-list') ?>">房子</a></li>
                                        <li><a href="<?= $router->generate('town-home-list') ?>">连排别墅</a></li>
                                        <li><a href="<?= $router->generate('condominium-list',[
                                                 'lang'         =>  'th'
                                        ]) ?>">公寓</a></li>
                                        <li><a href="<?= $router->generate('ladawan-list') ?>">LADAWAN</a></li>
                                        <li><a href="<?= $router->generate('country-list') ?>">外府房产项目</a></li>
                                        <!-- <li><a href="<?= $router->generate('new-project-list') ?>">房产项目新信息</a></li> -->
                                        <!-- <li><a href="<?= $router->generate('promotion') ?>">优惠活动和优惠政策</a></li> -->
                                       <!--  <li><a href="<?= $router->generate('review') ?>">Project Review</a></li> -->
                                        <li><a href="<?= $router->generate('lh-project') ?>">Foreign Buyers</a></li>
                                        <!-- <li><a href="<?= $router->generate('tvc') ?>">TVC</a></li> -->
                                    </ul>
                                </div>
                            </div>

                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="submenu-block">
                                            <p class="submenu-title">设计概念</p>
                                            <ul>
                                                <li><a href="<?= $router->generate('home-series') ?>">房间类型</a></li>
                                                <li><a href="<?= $router->generate('360-virtual') ?>">360度画面</a></li>
                                                <li><a href="<?= $router->generate('air-plus') ?>">Air plus 技术</a></li>
                                                <!--  
                                                <li><a href="<?= $router->generate('concept') ?>">แนวคิดบ้านสบาย</a></li>
                                                -->
                                                <!-- <li><a href="<?= $router->generate('living-tips') ?>">LH Living Tips</a></li> -->
                                                <!-- <li ><a href="<?= $router->generate('motivo') ?>">นิตยสาร Motivo</a></li> -->
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="submenu-block">
                                            <p class="submenu-title">企业信息</p>
                                            <ul>
                                                <li><a href="http://lh.listedcompany.com/corporate_information_cn.html">企业信息</a></li>
                                                <!-- <li><a href="http://lh-th.listedcompany.com/right_shareholder.html">企业治理信息</a></li>
                                                <li><a href="http://lh-th.listedcompany.com/home.html">投资者信息</a></li>
                                                <li><a href="http://lh-th.listedcompany.com/company_news.html">日常新闻</a></li> -->
                                            </ul>
                                        </div>
                                    </div> 
                                    <div class="col-md-6">
                                        <div class="submenu-block">
                                            <p class="submenu-title">联系我们</p>
                                            <ul>
                                                <!-- <li><a href="<?= $router->generate('job-with-us') ?>">招聘</a></li> -->
                                                <!-- <li><a href="<?= $router->generate('land-offer') ?>">推介卖地</a></li> -->
                                                <li><a href="<?= $router->generate('report') ?>">报告线上使用的问题</a></li>
                                                <li hidden><a href="<?= $router->generate('contact-us') ?>">询问信息. </a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <!-- <div class="submenu-block">
                                            <p class="submenu-title">住户联欢活动</p>
                                            <ul>
                                                <li><a href="<?= $router->generate('community') ?>">LH 社区</a></li> 
                                                <li><a href="http://www7.lh.co.th/EService/LoginForm.do">在线报修</a></li>
                                                <li><a href="http://www7.lh.co.th/LHComplaint/index.jsp">关于公寓和房屋的投诉</a></li>
                                            </ul>
                                        </div> -->
                                    </div>
                                    <!-- <div class="col-md-6">
                                        <div class="submenu-block">
                                            <p class="submenu-title">联系我们</p>
                                            <ul>
                                                <li><a href="<?= $router->generate('job-with-us') ?>">招聘</a></li>
                                                <li><a href="<?= $router->generate('land-offer') ?>">推介卖地</a></li>
                                                <li><a href="<?= $router->generate('report') ?>">报告线上使用的问题</a></li>
                                                <li hidden><a href="<?= $router->generate('contact-us') ?>">询问信息. </a></li>
                                            </ul>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="submenu-tool">
                    <div class="col-md-4">
                        <div class="submenu-tools">
                            <a href="" id="topSearch" class="menu-tool-list" data-status="0">
                                <i class="i-subm-search"></i>
                                寻找您中意地段上的房产项
                            </a>
                            <a href="" id="topGetdirection" class="menu-tool-list" data-status="0">
                                <i class="i-subm-direction"></i>
                                搜索去往您项目的线路
                            </a>
                           <!--  <a href="" id="topContact" class="menu-tool-list"> -->
                            <a href="<?= $router->generate('contact-us') ?>" class="menu-tool-list">
                                <i class="i-subm-email"></i>
                                询问房产项目信息. 
                            </a>
                            <a href="#" class="menu-tool-list" id="call" style="cursor:text;">
                                <i class="i-subm-call"></i>
                                电话 1198<br>
                                <span>(每天09.00 - 17.30 .)</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script  type="text/javascript" >
    $('#call').click(function(){

        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            window.location.href='tel:1198';
        }else{
            //alert('กรุณาติดต่อ : 1198')
        }

    });
</script>