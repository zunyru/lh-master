<?php

require_once dirname(dirname(__FILE__)).'/include/help/begin.php';
require_once dirname(dirname(__FILE__)).'/include/help/query_function.php';

$projectMap_snippet = getProjectMaps($_GET['project_id']);
$project_snippet    = getProjectByID($_GET['project_id']);
$project_price_snippet          = getProjectPrice($_GET['project_id']);
$project_concept_snippet        = getProjectConcept($_GET['project_id']);
$project_promotion_snippet      = getProjectPromotion($_GET['project_id']);
$logo_snippet               = getProjectMaps($_GET['project_id']);
$project_sub_snippet = getProjectSub($_GET['project_id'],3);

?>
<div class="main-navigation">
    <ul id="main-nav">
        <?php   $lang = !empty($_GET['lang']) ? $_GET['lang'] : 'th';
                $swap_lang = $lang == 'th' ? 'en' : 'th';
        ?>
       
        <li><a class="m-submenu-innerpage" data-name="info" data-status="0"><?php if($lang == 'th')echo '项目内容'; else echo 'Project Information';?></a>
            <div class="submenu-innerpage info">
                <div class="container">
                    <div class="row">
                        <div class="submenu-list">
                            <ul>
                                <li><a id="information" data-family="child"><?php if($lang == 'th')echo '项目详情'; else echo 'Project Details';?></a></li>
                                <?php  if(!empty($project_snippet->map_link)) {
                                    ?>
                                    <li><a id="location" data-family="child"><?php if($lang == 'th')echo '项目地段'; else echo 'Location';?></a></li>
                                    <?php
                                }
                                ?>
                                <?php
                                $communities_snippet = !empty($project_sub_snippet) ? getCommunityFeature($project_sub_snippet->project_sub_id) : null;
                                if($communities_snippet != false){
                                ?>
                                    <li><a id="facility" data-family="child"><?php if($lang == 'th')echo '基础设施'; else echo 'Facilities';?></a></li>
                                    <?php
                                }
                                ?>
                                <?php
                                $galleryImgProjects_snippet = getGalleryImgProject($_GET['project_id']);
                                if($galleryImgProjects_snippet != false){
                                ?>
                                    <li><a id="gallery" data-family="child"><?php if($lang == 'th')echo '相册'; else echo 'GALLERY';?></a></li>
                                    <?php
                                }
                                ?>
                                <?php
                                $project360_snippet = get360Project($_GET['project_id']);
                                if($project360_snippet != false){
                                ?>
                                    <li><a id="media" data-family="child"><?php if($lang == 'th')echo '360度画面拍摄'; else echo '360 Virtual Tour';?></a></li>
                                    <?php
                                }
                                ?>
                                <?php
                                $vdoProject_snippet = getVDOProject($_GET['project_id']);
                                if($vdoProject_snippet != false){
                                ?>
                                    <li><a id="media" data-family="child"><?php if($lang == 'th')echo '视频'; else echo 'VDO';?></a></li>
                                    <?php
                                }
                                ?>
                                <?php
                                $vdoProject_snippet = getVDOProject($_GET['project_id']);
                                if($vdoProject_snippet != false){
                                ?>
                                    <li><a id="media" data-family="child"><?php if($lang == 'th')echo '项目显示动图'; else echo 'Walkthrough Animation';?></a></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <?php
        $master_plans = !empty($project_sub_snippet) ? getMasterPlan($project_sub_snippet->project_sub_id) : null;
        if(!empty($master_plans)){
        ?>
            <li><a id="projectplan" class="m-submenu-innerpage"><?php if($lang == 'th')echo '项目计划和房间'; else echo 'Master Plan & Unit Plan';?></a></li>
            <?php
        }
        ?>
        <?php if( !empty($project_promotion_snippet) && $project_promotion_snippet != false && $project_snippet->project_status != 'SO') {
        ?>
            <li><a id="promotion" class="m-submenu-innerpage"><?php if($lang == 'th')echo '优惠活动'; else echo 'Promotion';?></a></li>
            <?php
        }
        ?>
        <?php
        $progress_groups_snippet = !empty($project_sub_snippet) ? getProgressCondo($project_sub_snippet->project_sub_id) : null;

        if(!empty($progress_groups_snippet) && !empty($progress_groups[0]->computed)){

        ?>
        <li><a id="projectprogress" class="m-submenu-innerpage"><?php if($lang == 'th')echo '房产项目的建设完成度'; else echo 'Project Progress';?></a></li>
            <?php
        }
        ?>
        <li><a id="" class="m-submenu-innerpage" data-name="contact" data-status="0"><?php if($lang == 'th')echo '联系房产项目'; else echo 'Contact';?></a>
            <div class="submenu-innerpage contact">
                <div class="container">
                    <div class="row">
                        <div class="submenu-list">
                            <ul>
                                 <?php
                                 if(isset($project_id)){
                                     $edm = get_Emd_Project($project_id);
                                 }

                                 ?>
                                  <li>
                                
                                 <!--  <a href="http://edm.lh.co.th/Support/contact-form/new.contact-project-detail.php?project_id=<?//=$edm->project_type;?><?=$edm->project_edm_id;?>&brand_id=&website=LHWeb&mem_id=" target="_blank" data-family="child"><i class="i-subm-email"></i><?php //if($lang == 'th')echo 'สอบถามหรือนัดหมายเยี่ยมชมโครงการ'; else echo 'Contact & Appointment';?></a></li> -->

                                 <a id="contact" data-family="child"><i class="i-subm-email"></i><?php if($lang == 'th')echo '询问或预约参观'; else echo 'Contact & Appointment';?></a></li> 
                                 
                                <?php  if(!empty($project_snippet->map_link)) {
                                ?>
                                    <li><a id="location" data-family="child"><i class="i-subm-direction"></i><?php if($lang == 'th')echo '去往房产项目的线路'; else echo 'Get Direction';?></a></li>
                                    <?php
                                }
                                ?>
                               <!--  <li><a href="call:1198" data-family="child" style="cursor: text;"><i class="i-subm-call"></i><?php if($lang == 'th')echo ' 电话1198 (时间09:00-17:30)'; else echo 'Call 1198 (08.30 AM – 05.30 PM)';?></a></li> -->
                                  <li><a href="#sec-information" data-family="child" style="padding-left: 18px"><img style="    display: inline-block;width: 20px;height: 20px;margin-right: 5px;"  src="<?php echo backend_url('icon_line','Wechat2.png') ?>">官方中文销售微信号 </a></li>
                            </ul>
                        </div>
                    </div>
                </div>           
             </div>
        </li>
     
    </ul>
</div>