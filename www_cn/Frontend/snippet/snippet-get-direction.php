<?php

$zones      = getSearchSelectZoneBydirection();
$projects   = getSearchSelectProjectBydirection();

?>
<div class="getdirection-container topdropdown-container">
    <span class="panel-close">close</span>
    <form id="getDirection" action="<?= $router->generate('get-direction') ?>">
        <h3>获取方向</h3>
        <p>搜索去往您项目的线路</p>

        <div class="">
            <div class="directlocation-block topdropdown-block">
                <input type="hidden" id="project_zone_id" name="zone_id" value="">
                <div class="searchBlock" data-status="0">
                    <span>输入地段</span>
                    <div class="search-icon-dropdown"><i class="i-search-triangle"></i></div>
                </div>
                <ul id="directlocationLists">
                    <?php
                    foreach ($zones as $zone){
                        ?>
                        <li data-id="<?= $zone->zone_id ?>" data-slug="<?= $zone->slug ?>" data-value="<?= $zone->zone_name ?>"><?= $zone->zone_name ?></li>
                        <?php
                    }
                    ?>
                </ul>
                <span id="validateZone" class="txt-error" style="display: none;">请输入地段</span>
            </div>

            <div class="directproject-block topdropdown-block">
                <input type="hidden" id="project_direction_id" name="project_id" value="">
                <input type="hidden" id="project_real_id" name="project_real_id" value="">
                <div class="searchBlock" data-status="0">
                    <span>输入项目名称</span>
                    <div class="search-icon-dropdown"><i class="i-search-triangle"></i></div>
                </div>
                <ul id="directprojectLists">
                    <?php
                    foreach ($projects as $project){
                        ?>
                        <li data-id="<?= $project->project_id ?>" data-slug="<?= $project->latitude ?>" data-value="<?= $project->project_name_th ?>"><?= $project->project_name_th ?></li>
                        <?php
                    }
                    ?>
                </ul>
                <span id="validateProjectName" class="txt-error" style="display: none;">请输入项目名称</span>
            </div>

            <button class="btn-search"><i></i> 搜索</button>

        </div>

    </form>
</div>