<!-- <script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script> -->
<script src="https://www.google.com/recaptcha/api.js?onload=ReCaptchaCallback&render=explicit&hl=<?= !empty($_GET['lang']) ? $_GET['lang'] : 'zh-CN' ?>" async defer>
</script>
<script type="text/javascript">
    //         var CaptchaCallback = function() {
        
    //     grecaptcha.render('RecaptchaField1', 
    //                     {'sitekey' : '6LcOTx4UAAAAAIludSpeNgoh68Bwt4nbWK8Ksyy2',
    //                     callback: function () {
    //                         $('#chacheck1').hide();
    //                     }
    //                 });

    //     grecaptcha.render('RecaptchaField2', {'sitekey' : '6LcOTx4UAAAAAIludSpeNgoh68Bwt4nbWK8Ksyy2',
    //                     callback: function () {   
    //                          $('#chacheck2').hide();
    //                     }
    //     });
    // };
    
            var ReCaptchaCallback = function() {
    	       $('.g-recaptcha').each(function(){
    		      var el = $(this);
    		          grecaptcha.render(el.get(0), {'sitekey' : el.data("sitekey"),
    				         callback: function () {
                               
                                    $('#chacheck').hide();
                                    $('#chacheck1').hide();
                           
                             }
    			     });
    	        });  
            };
</script>
<style>
    .error{
        color: red;
    }
</style>
<script src="<?= file_path('../build/js/jquery.validate.js') ?>" /></script>

<div class="contact-container contact-container-ft topdropdown-container">
    <span class="panel-close">close</span>

    <h3>联系或预约参观房产项目</h3>
    <div>
        <form class="project-form form-container" id="CheckContForm" name="CheckContForm" action="mail-snippet-contact.php" method="POST">
            <p class="title-label"></p>
            <div class="">
                <div class="form-group radio-checkmark">
                    <input type="radio" id="check-contact-ft" name="check-type" value="contact" checked/>
                    <label for="check-contact-ft" id="popcontact" class="title-label" style="color: black"><span></span>询问</label>
                    <input type="radio" id="check-appoint-ft" name="check-type"/ value="appointment">
                    <label for="check-appoint-ft" id="popappoint" class="title-label" style="display: none; color: black;"><span></span>预约参观房产项目</label>
                </div>
                <div class="form-group check-type-radio">
                    <input type="text" id="date_appointment_ft" name="dateappointment" class="form-control"
                           placeholder="*任命日期" style="background-color: #ffffff;" readonly required>
                </div>
                <div class="form-group">
                    <input type="text" name="emailpro" value="lhch@lh.co.th" class="form-control" 
                    style="background-color: #ffffff;" readonly>
                </div>
                <div class="form-group">
                    <input type="text" name="fullname"  class="form-control" placeholder="*名字 ——姓氏" required>
                </div>
                <div class="form-group">
                    <input type="number" id="phonecon" name="phonecon" class="form-control" min="0" 
                    placeholder="*电话" required>
                </div>
                <div class="form-group">
                    <input type="email" name="email"  class="form-control" placeholder="*子邮件" required>
                </div>
                <div class="form-group">
                    <textarea name="detail"  placeholder="*消息" id="" class="form-control" cols="30"
                              rows="3" required></textarea>
                </div>
                <div class="title-block">
                    <p class="title">需要返回联系</p>
                </div>
                <div class="form-group">
                    <input type="checkbox" id="check-tel-ft" name="check-tel-ft"/>
                    <label for="check-tel-ft" class="title-label" style="color: black"><span></span>电话</label>
                    <input type="checkbox" id="check-mail-ft" name="check-mail-ft"/>
                    <label for="check-mail-ft" class="title-label"style="color: black"><span></span>电子邮件</label>
                </div>
                <!-- <div id="RecaptchaField1"></div> -->
                <div class="g-recaptcha" data-sitekey="6LcSzSEUAAAAAPTUl4maci5Pl4SyWVMmnNhly0Qw"></div>
                <span id="chacheck" style="display: none; color: red;">经验证您不是机器人  !</span>
                <br><br>
                <input type="submit" class="btn-search"  name="submit" value="发信息">
            </div>
        </form>
    </div>
</div>

<script>

  $(document).ready(function() {

            var email = $('#mailcont').val();
            // var email2;
            // $('#submenu').click(function(){
                    
            //         alert('dddd');
             
            //     })
  			
            
            if(email === undefined || email  === null && email2 ==""){
                 
                    $('#emailpro').val('info@lh.co.th'); 
            
            }else{
                
                $('#emailpro').val(email); 
   
            }
            
            $("#CheckContForm").validate({
                rules: {
                    fullname: "required",
                    phonecon: "required",
                    email: "required",
                    detail : "required",
                    dateappointment : "required",
                },
                messages: {
                    fullname: "請輸入您的全名。 !",
                    phonecon : "請輸入您的電話號碼。 !",
                    email : "請輸入電子郵件 !",
                    detail : "請輸入消息。 !",
                    dateappointment : "請說明預約日期。 !"

                }
            });

            $('#phonecon').keydown(function(event){
                var max = 10;
                var phone = $(this).val().length;
                
                if(phone >= max){

                    if(event.keyCode == 8 || event.keyCode == 46){
                        return true;                    
                    }                        
                        return false;
                    
                }
            });

            $('#CheckContForm').on('submit', function(){
                var googleResponse = $('#g-recaptcha-response').val();
                if(googleResponse==''){   
                    $('#chacheck').show();

                     return false;
           
                }

            });

            $('#popcontact').click(function(){
                
                    $('#date_appointment_ft-error').hide();
                

            })
             $('#popappoint').click(function(){
                

                   $('#date_appointment_ft-error').show();

            })


        });

</script>
