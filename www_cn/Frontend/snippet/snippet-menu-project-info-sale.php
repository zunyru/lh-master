<?php

require_once dirname(dirname(__FILE__)).'/include/help/begin.php';
require_once dirname(dirname(__FILE__)).'/include/help/query_function.php';

$home_sell_id = $_GET['home_sell_id'];

if(empty($home_sell_id) || getHomeSellByHomeSellID($home_sell_id) == false){
    header('Location: '.Helper::url_string('project-type.php'));
}

$proj_home_sell = getHomeSellByHomeSellID($home_sell_id);
$proj_subb      = getProjectSubBySubID($proj_home_sell->project_sub_id);

$_GET['project_id'] = $proj_subb->project_id;
$promo = getProjectPromotion($_GET['project_id']);
$proj  = getProjectByID($_GET['project_id']);
$homeS = getHomeSell($_GET['project_id'], $proj_subb->product_id);
$galls = getGalleryImgProject($_GET['project_id']);
$pr360 = get360Project($_GET['project_id']);
$prVdo = getVDOProject($_GET['project_id']);


$product_arr= Helper::getArrayProductOfProject($project_sub->project_id);

$project = getProjectByID($project_sub->project_id);

if(in_array(1,$product_arr)){
    $url_see_more = isLadawan($project->project_id) ?
        $router->generate('ladawan-detail',[
            'project_name'  =>  str_replace(' ','-',$project->project_name_th),
            'product_id'    =>  1
        ]) :
        $router->generate('single-home-detail',[
            'project_name'  =>  str_replace(' ','-',$project->project_name_th),
            'product_id'    =>  1
        ]);
}elseif(in_array(2,$product_arr)){
     $url_see_more = isLadawan($project->project_id) ?
        $router->generate('ladawan-detail',[
            'project_name'  =>  str_replace(' ','-',$project->project_name_th),
            'product_id'    =>  1
        ]) :
        $router->generate('town-home-detail',[
            'project_name'  =>  str_replace(' ','-',$project->project_name_th),
            'product_id'    =>  2
        ]);
}
elseif(in_array(3,$product_arr)){
     $url_see_more = isLadawan($project->project_id) ?
        $router->generate('ladawan-detail',[
            'project_name'  =>  str_replace(' ','-',$project->project_name_th),
            'product_id'    =>  1
        ]) :
        $router->generate('condominium-detail',[
            'project_name'  =>  str_replace(' ','-',$project->project_name_th),
            'product_id'    =>  3
        ]);
}
else{
    $url_see_more = isLadawan($project->project_id) ?
        $router->generate('ladawan-detail',[
            'project_name'  =>  str_replace(' ','-',$project->project_name_th),
            'product_id'    =>  $product_arr
        ]) :
        $router->generate('single-home-detail',[
            'project_name'  =>  str_replace(' ','-',$project->project_name_th),
            'lang'          =>  'th'
        ]);
}

  $url_see_more_project = $url_see_more;

?>
<div class="main-navigation">
    <ul id="main-nav">
        <li><a class="m-submenu-innerpage" data-name="info" data-status="0">项目内容</a>
            <div class="submenu-innerpage info">
                <div class="container">
                    <div class="row">
                        <div class="submenu-list">
                            <ul>
                                <li>
<!--                                    <a href="--><!--#sec-information" id="information" data-family="child">รายละเอียดโครงการ ขาย</a>-->
                                    <a href="<?=$url_see_more;?>#sec-information"  data-family="child">项目详情</a>
                                </li>
                                <?php
                                if($galls != false) {
                                    ?>
                                    <li><a href="<?=$url_see_more;?>#sec-gallery" id="gallery" data-family="child">相册</a></li>
                                    <?php
                                }
                                ?>
                                <?php
                                if ($pr360 != false) {
                                    ?>
                                    <li><a href="<?=$url_see_more;?>#sec-virtual" id="virtual" data-family="child">360 度画面拍摄</a></li>
                                    <?php
                                }
                                ?>
                                <?php
                                if ($prVdo != false) {
                                    ?>
                                    <li><a  href="<?=$url_see_more;?>#sec-tvc" id="tvc" data-family="child">VDO</a></li>
                                    <?php
                                }
                                ?>
                                <?php
                                $proj->latitude = str_replace(' ','',$proj->latitude);
                                $proj->longtitude = str_replace(' ','',$proj->longtitude);
                                if(!empty($proj->map_link)){
                                    ?>
                                    <li><a href="<?=$url_see_more;?>#sec-location" id="location" data-family="child">项目地段</a></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <?php
        if($homeS != false && $proj->project_status != 'SO'){
            ?>
            <li><a href="<?=$url_see_more;?>#sec-home-sale" id="home-sale" class="i-submenu-innerpage" >代售毛坯房</a></li>
            <?php
        }
        ?>
        <?php
        if($promo != false && $proj->project_status != 'SO'){
            ?>
            <li><a href="<?=$url_see_more;?>#sec-promotion" id="promotion" class="i-submenu-innerpage" >优惠活动</a></li>
            <?php
        }
        ?>
        <li><a id="" class="m-submenu-innerpage" data-name="contact" data-status="0">联系房产项目</a>
            <div class="submenu-innerpage contact">
                <div class="container">
                    <div class="row">
                        <div class="submenu-list">
                            <ul>
                                <li><a href="<?=$url_see_more?>#sec-contact" id="contact" data-family="child"><i class="i-subm-email"></i>询问或预约参观</a></li>
                                <?php
                                $proj->latitude = str_replace(' ','',$proj->latitude);
                                $proj->longtitude = str_replace(' ','',$proj->longtitude);
                                if(!empty($proj->map_link)){
                                    ?>
                                    <li><a href="<?=$url_see_more;?>#sec-location" id="location" data-family="child"><i class="i-subm-direction"></i>去往房产项目的线路</a></li>
                                <?php } ?>
                                <!-- <li><a href="call:1198" data-family="child" style="cursor: text;"><i class="i-subm-call"></i> 电话1198 (时间09:00-17:30)</a></li> -->
                                 <li><a href="<?=$url_see_more;?>#sec-information" data-family="child" style="cursor: text;"><span><img style="    display: inline-block;width: 20px;height: 20px;margin-right: 5px;"  src="<?php echo backend_url('icon_line','Wechat2.png') ?>"></span>官方中文销售微信号 </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </li>
    </ul>
</div>