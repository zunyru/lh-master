<?php 
$product_arr= Helper::getArrayProductOfProject($project_id);

$project = getProjectByID($project_id);

if(in_array(1,$product_arr)){
	$url_see_more = isLadawan($project_id) ?
	$router->generate('ladawan-detail',[
		'project_name'  =>  str_replace(' ','-',$project->project_name_th),
		'product_id'    =>  1
	]) :
	$router->generate('single-home-detail',[
		'project_name'  =>  str_replace(' ','-',$project->project_name_th),
		'product_id'    =>  1
	]);
}elseif(in_array(2,$product_arr)){
	$url_see_more = isLadawan($project->project_id) ?
	$router->generate('ladawan-detail',[
		'project_name'  =>  str_replace(' ','-',$project->project_name_th),
		'product_id'    =>  1
	]) :
	$router->generate('town-home-detail',[
		'project_name'  =>  str_replace(' ','-',$project->project_name_th),
		'product_id'    =>  2
	]);
}
elseif(in_array(3,$product_arr)){
	$url_see_more = isLadawan($project->project_id) ?
	$router->generate('ladawan-detail',[
		'project_name'  =>  str_replace(' ','-',$project->project_name_th),
		'product_id'    =>  1
	]) :
	$router->generate('condominium-detail',[
		'project_name'  =>  str_replace(' ','-',$project->project_name_th),
		'product_id'    =>  3
	]);
}
else{
	$url_see_more = isLadawan($project->project_id) ?
	$router->generate('ladawan-detail',[
		'project_name'  =>  str_replace(' ','-',$project->project_name_th),
		'product_id'    =>  $product_arr
	]) :
	$router->generate('single-home-detail',[
		'project_name'  =>  str_replace(' ','-',$project->project_name_th),
		'lang'          =>  'th'
	]);
}
?>
<ul>
	<li class="menu-hover rps"><a href="" id="topSearch" data-status="0"><i class="i-search"></i></a></li>
	<li class="menu-hover rps"><a onclick="toGoogleMap();" id="" data-status="0"><i class="i-direction"></i></a></li>

	<li><a href="<?=$url_see_more?>#sec-contact"  data-status="0"><i class="i-mail"></i></a></li>

    <li class="top-call"><a href="tel:1198"><i class="i-call"></i><span>1198</span></a></li>
    

    <li class="menu-hover dsktp"><a href="" id="topGetdirection" data-status="0"><i class="i-direction"></i></a></li>
    <li class="menu-hover dsktp"><a href="" id="topSearch" data-status="0"><i class="i-search"></i></a></li>
</ul>


<script>
	function toGoogleMap() {
		// window.open('https://maps.google.com?saddr=Current+Location&dirflg=d&daddr='+$('#lat').val()+','+$('#lng').val(),'_blank');
		if($('#lat').val() != ''){
       		window.open($('#lat').val(),'_blank');
     	}
		return false;
	}
</script>