<?php

require_once dirname(dirname(__FILE__)).'/include/help/begin.php';
require_once dirname(dirname(__FILE__)).'/include/help/query_function.php';


$promo = getProjectPromotion($_GET['project_id']);
$proj  = getProjectByID($_GET['project_id']);
$homeS = getHomeSell($_GET['project_id'], $_GET['product_id']);
$galls = getGalleryImgProject($_GET['project_id']);
$pr360 = get360Project($_GET['project_id']);
$prVdo = getVDOProject($_GET['project_id']);

?>
<div class="main-navigation">
    <ul id="main-nav">
        <li><a class="m-submenu-innerpage" data-name="info" data-status="0">项目内容</a>
            <div class="submenu-innerpage info">
                <div class="container">
                    <div class="row">
                        <div class="submenu-list">
                            <ul>
                                <li><a id="information" data-family="child">项目详情</a></li>
                                <?php
                                if($galls != false) {
                                    ?>
                                    <li><a id="gallery" data-family="child">相册</a></li>
                                    <?php
                                }
                                ?>
                                <?php
                                if ($pr360 != false) {
                                ?>
                                    <li><a id="virtual" data-family="child">360度画面拍摄</a></li>
                                    <?php
                                }
                                ?>
                                <?php
                                if ($prVdo != false) {
                                ?>
                                    <li><a id="tvc" data-family="child">视频</a></li>
                                    <?php
                                }
                                ?>
                                <?php
                                $proj->latitude = str_replace(' ','',$proj->latitude);
                                $proj->longtitude = str_replace(' ','',$proj->longtitude);
                                if(!empty($proj->map_link)){
                                ?>
                                    <li><a id="location" data-family="child">项目地段</a></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <?php
            if($homeS != false && $proj->project_status != 'SO'){
                ?>
                    <li><a id="home-sale" class="m-submenu-innerpage" >代售毛坯房</a></li>
                <?php
            }
        ?>
        <?php
            if($promo != false && $proj->project_status != 'SO'){
                ?>
                <li><a id="promotion" class="m-submenu-innerpage" >优惠活动</a></li>
        <?php
            }
        ?>
        <li><a id="" class="m-submenu-innerpage" data-name="contact" data-status="0">联系房产项目</a>
            <div class="submenu-innerpage contact">
                <div class="container">
                    <div class="row">
                        <div class="submenu-list">
                            <ul>
                                 <li>
                                <a id="contact" data-family="child"><i class="i-subm-email"></i>询问或预约参观</a>
                                 <?php
                                     //$edm = get_Emd_Project($project_id);

                                 ?>
                               
                                 
                                 
                               


                                </li>
                                <?php
                                $proj->latitude = str_replace(' ','',$proj->latitude);
                                $proj->longtitude = str_replace(' ','',$proj->longtitude);
                                    if(!empty($proj->map_link)){
                                ?>
                                    <li><a id="location" data-family="child"><i class="i-subm-direction"></i>去往房产项目的线路</a></li>
                                <?php } ?>
                                <!-- <li><a href="call:1198" data-family="child" style="cursor: text;"><i class="i-subm-call"></i>电话1198 (时间09:00-17:30)</a></li> -->

                                 <li><a href="#sec-information" data-family="child" style="padding-left: 18px"><img style="    display: inline-block;width: 20px;height: 20px;margin-right: 5px;"  src="<?php echo backend_url('icon_line','Wechat2.png') ?>">官方中文销售微信号 </a></li>

                               
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </li>
    </ul>
</div>