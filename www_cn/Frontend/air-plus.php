<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';
$page = 'airplus';
$page_index = 0;
$img = '';
?>
<?php
include('header.php');

?>
<!-- CSS -->
<link rel="stylesheet" href="<?= file_path('css/air-plus.css') ?>" type="text/css">
<!-- JS -->
<script src="<?= file_path('js/air-plus.js') ?>"></script>

<div class="page-banner">
    <div id="bannerSlide">
        <div class="item">
            <div class="banner-container banner-parallax">
                <img src="<?= file_path('images/temp3/air_1.jpg') ?>" alt="" class="vdo-banner">
                <video loop="loop" id="slideVdo" class="" style="display:none;">
                    <source src="<?= file_path('files/vdo/airplus.mp4') ?>" type="video/mp4">
                </video>

                <div class="vdo-control">
                    <div class="play-block">
                        <span id="vdoPlay" class="vdo-icon vdoControl vdoPlay"></span>
                        <span id="vdoPause" class="vdo-icon vdoControl vdoPause"></span>
                    </div>
                </div>
            </div>

<!--            <a href="#" data-featherlight="#lbVdo1">-->
<!--                <span class="i-view-vdo"></span>-->
<!--            </a>-->
<!--            <div class="banner-container banner-parallax" style="background-image: url(images/temp3/air_1.jpg);">-->
<!--            <iframe class="lightbox" src="https://www.youtube.com/embed/GJkPSkZ80bo" width="1000"-->
<!--                        height="560" id="lbVdo1" style="border:none;" webkitallowfullscreen-->
<!--                        mozallowfullscreen allowfullscreen></iframe>-->
<!--            </div>-->
        </div>
    </div>
</div>

                <div id="content" class="content airplus-page">
                    <div class="container">
                        <div class="airplus-content">

                            <?php
                            $SEO = getSEOUrl($actual_link);
                            if($actual_link == @$SEO->url_page){
                                echo '<h1 >'.$SEO->h1.'<h1>';
                            }else{
                               echo ' <h1>Air plus 技术</h1>'; 
                           }
                           ?>
                           <div class="row">
                            <div class="col-md-4">
                                <img src="<?= file_path('images/temp3/air_02.jpg') ?>" alt="">
                            </div>
                            <div class="col-md-6 col-md-offset-1">
                                <p class="title">家里的有毒空气来自哪里</p>
                                <ul class="ap-li">
                                    <li>在家里的任何活动下，在不知不觉中都会造成室内的空气质量受到污染。例如:粉刷墙壁的气味，做饭时候的油烟气味甚至来自宠物身上不干净的东西。</li>
                                    <li>开一整天的空调. 冷气从空调的空气压缩机里传来。其实是把室内原来的空气吸收到空调里，然后空调再把原来的空气放出来，这么循环使用。我们呼吸时吸进肺里的都是原来的空气，有些人会感觉呼吸不充沛，或者在睡醒的时候感觉到睡眠不足。</li>
                                    <li>在将家门关闭很长一段时间. 这个问题通常发生在我们需要离开家很长一段时间, 使得室内的空气不能够流通或者产生“有毒空气 Dead Air”将使室内空气产生一股霉味。</li>
                                    <li>在现如今室内空气的状况，潮湿气味通常因为紧闭门窗而避免了阳光和雨水, 使得室内会有潮湿的气味，或者变成生产质量上乘细菌的细菌温床。例如:霉菌，灰尘等等。</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

    <div class="airplus-lists-container">
        <div class="container">
            <div class="airplus-lists">
                <div class="row">
                    <div class="col-md-4">
                        <div class="airplus-list">
                            <span class="apicon i-aplist-1"></span>
                            <p class="title">自动运行<br>
                                (Automatic Control)</p>
                            <p>Air plus 的技术设计使得机器24小时自动运行，通过太阳能板可以使新鲜空气在室内24小时不断循环，使用太阳板可以节能还可以保持良好的环境保护. 除了空气通风系统可以自动运行以外, 使用者还可以根据自己指定的需求设置控制单元设备的开关。</p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="airplus-list">
                            <span class="apicon i-aplist-2"></span>
                            <p class="title">智能能源管理系统<br>
                                (Smart Power Manager)</p>
                            <p>1. <strong>太阳能发电系统 (Solar power)</strong> 太阳能是一种清洁能源<br><br>
                                2. <strong>混合用电系统 (Hybrid)</strong> 家庭用电与太阳能发电的混合能源.<br><br>
                                3. <strong>充分节能系统 (Sufficient)</strong> 节省家庭用电。</p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="airplus-list">
                            <span class="apicon i-aplist-3"></span>
                            <p class="title">空气流通性良好<br>
                                (Well-planned ventilation)</p>
                            <p>室内安装抽气机管(Ventilation Tube) 为了通过抽气机管吸收室内空气排出到室外，可以让室内的空气一直循环转换, 减少室内热量和潮气的积聚。</p>
                        </div>
                    </div>
                </div>


                <span class="img-leafs"></span>
                <span class="img-girl"></span>
            </div>
        </div>
    </div>

</div>


<?php include('footer.php'); ?>
