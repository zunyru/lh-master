<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

$project_direction_id = $request['project_id'];

if(empty($project_direction_id) || getProjectByID($project_direction_id) == false){
    echo 'not have a project for this project id';
    die();
}

$project_direction    = getProjectByID($project_direction_id);

$latitude   = $project_direction->latitude;
$longtitude = $project_direction->longtitude;

$project_id = $request['project_id'];

?>
<?php include('header.php'); ?>
    <link rel="stylesheet" href="<?= file_path('css/get-direction.css') ?>" type="text/css">

    <input type="hidden" id="latitude"      value="<?= $latitude ?>">
    <input type="hidden" id="longtitude"    value="<?= $longtitude ?>">


<!--    map -->

    <script>
        function initMap() {
            // Create a map object and specify the DOM element for display.
            $(document).ready(function () {
                var lat_proj = parseFloat($('#latitude').val())

                var lng_proj = parseFloat($('#longtitude').val());
                var pos = {lat: lat_proj, lng: lng_proj};
                var map = new google.maps.Map(document.getElementById('map'), {
                    center: pos,
                    scrollwheel: false,
                    zoom: 16,
                    zoomControlOptions: {
                        position: google.maps.ControlPosition.TOP_RIGHT
                    },
                    streetViewControl: false,
                    mapTypeControl: false,
                    styles: [{"elementType": "geometry", "stylers": [{"color": "#f5f5f5"}]}, {
                        "elementType": "labels.icon",
                        "stylers": [{"visibility": "off"}]
                    }, {
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#616161"}]
                    }, {
                        "elementType": "labels.text.stroke",
                        "stylers": [{"color": "#f5f5f5"}]
                    }, {
                        "featureType": "administrative.land_parcel",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#bdbdbd"}]
                    }, {
                        "featureType": "administrative.locality",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#c19838"}]
                    }, {
                        "featureType": "administrative.neighborhood",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#c19838"}]
                    }, {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [{"color": "#eeeeee"}]
                    }, {
                        "featureType": "poi",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#757575"}]
                    }, {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [{"color": "#e5e5e5"}]
                    }, {
                        "featureType": "poi.park",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#9e9e9e"}]
                    }, {
                        "featureType": "road",
                        "elementType": "geometry",
                        "stylers": [{"color": "#ffffff"}]
                    }, {
                        "featureType": "road",
                        "elementType": "labels.icon",
                        "stylers": [{"visibility": "simplified"}]
                    }, {
                        "featureType": "road",
                        "elementType": "labels.text",
                        "stylers": [{"color": "#878787"}]
                    }, {
                        "featureType": "road",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#9f720a"}, {"visibility": "off"}]
                    }, {
                        "featureType": "road.arterial",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#757575"}]
                    }, {
                        "featureType": "road.highway",
                        "elementType": "geometry",
                        "stylers": [{"color": "#dadada"}]
                    }, {
                        "featureType": "road.highway",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#616161"}]
                    }, {
                        "featureType": "transit.line",
                        "elementType": "geometry",
                        "stylers": [{"color": "#e5e5e5"}]
                    }, {
                        "featureType": "transit.station",
                        "elementType": "geometry",
                        "stylers": [{"color": "#eeeeee"}]
                    }, {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [{"color": "#c9c9c9"}]
                    }, {"featureType": "water", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]}]
                });
                var icon = {
                    url: '<?= file_path('images/global/icon_google_map.png') ?>',
                    scaledSize: new google.maps.Size(30, 42), // scaled size
                    origin: new google.maps.Point(0, 0), // origin
                    anchor: new google.maps.Point(0, 0) // anchor
                };
                var marker = new google.maps.Marker({
                    position: pos,
                    map: map,
                    icon: icon,
                    title: '<?= $project_direction->project_name_th?>',

                });

                var infowindow = new google.maps.InfoWindow({});

                infowindow.close();
                infowindow.setOptions({
                    content: '<?= $project_direction->project_name_th?>',
                    position: new google.maps.LatLng(lat_proj,lng_proj),
                    pixelOffset: new google.maps.Size(15,10),
                });
                infowindow.open(map);

                google.maps.event.addListener(marker, 'click', function() {
                    window.open('https://maps.google.com?saddr=Current+Location&daddr='+lat_proj+','+lng_proj,'_blank');
                });


            });
        }

        function toLocation() {
            window.open('https://maps.google.com?saddr=Current+Location&dirflg=d&daddr='+$('#latitude').val()+','+$('#longtitude').val(),'_blank');
        }
        function toLocationByWalk2() {
            window.open('https://maps.google.com?saddr=Current+Location&dirflg=w&daddr='+lat_proj+','+lng_proj,'_blank');
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDaz8JbxbUyy8acSAPy4ENzPhCaY4o_kj4&callback=initMap"
async defer></script>
<!-- end map -->


    <div id="content" class="content">
        <div class="container">
            <h1> <?= $project_direction->project_name_th ?> 项目</h1>
            <div class="getpage">
                <a href="" id="topGetdirection" data-status="0"><i class="i-subm-direction "></i>搜索去往您项目的线路</a>
            </div>
        </div>

        <div class="map-container">
            <div id="map"></div>
<!--            <span class="i-getdirection" onclick="toLocationByWalk2();"></span>-->
            <span class="i-getdirection-car" onclick="toLocation();"></span>
        </div>
    </div>


<!--    <div id="info-window" style="display: none;">-->
<!--        <div style="width: auto;">-->
<!--            <img src="images/logo/logo_mtn.jpg" class="background-logo" style="float:left; width: 100px;"/>-->
<!--            <p style="float:left;margin-top: 10px;">ราคาเริ่มต้น <br> <span>3.79 - 5 ล้านบาท</span></p>-->
<!--        </div>-->
<!--    </div>-->

<?php include('footer.php'); ?>