 <div class="">
    <form  class="project-form form-container" id="Projectcontact" >
        <p class="title-label"></p>
        <div class="">
            <div class="form-group radio-checkmark">
                <input type="radio" id="check-contact" name="check_type" checked/>
                <label for="check-contact" id="checkcont" class="title-label"><span></span>
                    <?= (Helper::checkLangEnglish($lang) ? 'Contact' : '询问') ?></label>
                    <input type="radio" id="check-appoint" name="check_type"/>
                    <label for="check-appoint" id="checkappoint"
                    class="title-label"><span></span>
                    <?= (Helper::checkLangEnglish($lang) ? 'Appointment' : '预约参观房产项目') ?></label>
                </div>
                <div class="form-group check-type-radio">
                    <input type="text" id="date_appointment" name="datecont" class="form-control"
                    placeholder="<?= (Helper::checkLangEnglish($lang) ? '*Appointment Date' : '*任命日期') ?>" readonly style="background-color: #ffffff;" required>
                </div>
                <div class="form-group">
                    <input type="hidden" name="project_id" id="project_ids" class="form-control" value="<?=$project_id ?>"  >
                    <input type="hidden" name="project_name_en" id="project_name_en" class="form-control" 
                    value="<?= $project->project_name_en ?>" >
                    <input type="hidden" name="project_name" id="project_name" class="form-control" 
                    value="<?= $project->project_name_th ?>" readonly style="background-color: #ffffff;">
                    <input type="text" name="mailcont" id="mailcont" class="form-control" value="<?='lhch@lh.co.th' ?>" readonly style="background-color: #ffffff;">
                </div>
                <div class="form-group">
                    <input type="text" name="namecont" id="nameconts" class="form-control" placeholder="*<?= (Helper::checkLangEnglish($lang) ? 'Fill name' : '名字 ——姓氏') ?>" required>
                </div>
                <div class="form-group">
                    <input type="tel" min="0" name="phonecont" id="phoneconts" class="form-control"
                    placeholder="*<?= (Helper::checkLangEnglish($lang) ? 'Tel No.' : '电话') ?>" required>
                </div>
                <div class="form-group">
                    <input type="text" name="emailcust" id="emailcusts" class="form-control" placeholder="*<?= (Helper::checkLangEnglish($lang) ? 'Email' : '子邮件') ?>" required>
                </div>
                <div class="form-group">
                    <textarea name="detailcont" placeholder="*<?= (Helper::checkLangEnglish($lang) ? 'Remark' : ' 消息') ?>" id="detailconts" class="form-control" cols="30"
                        rows="10" required></textarea>
                    </div>
                    <div class="title-block">
                        <p class="title"><?= (Helper::checkLangEnglish($lang) ? 'Call back by' : '需要返回联系') ?></p>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" id="check-tel" name="check-tel"/>
                        <label for="check-tel" class="title-label"><span></span><?= (Helper::checkLangEnglish($lang) ? 'Phone' : '电话') ?></label>
                        <input type="checkbox" id="check-mail" name="check-mail"/>
                        <label for="check-mail" class="title-label"><span></span><?= (Helper::checkLangEnglish($lang) ? 'Email' : '电子邮件') ?></label>
                    </div>
                    
                    <div class="g-recaptcha"  data-sitekey="6LcSzSEUAAAAAPTUl4maci5Pl4SyWVMmnNhly0Qw"></div>
                    <input type="hidden" class="hiddenRecaptcha " name="hiddenRecaptcha" id="hiddenRecaptcha">
                    <span id="chacheck1" style="display: none; color: red;">经验证您不是机器人 !</span>
                    

                    <input type="hidden" name="lang" id="lang" value="<?=(Helper::checkLangEnglish($lang)) ? 'EN' : 'TH'  ?>">
                    <button class="btn btn-submit" type="submit" id="btnsendmailhome" ><?= (Helper::checkLangEnglish($lang) ? 'Submit' : '发信息') ?></button>
                    
                </div>
            </form>
        </div>
        