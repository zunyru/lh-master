<div class="apply-job-container">
    <a id="backJobLists" class="btn-backjob">< Back</a>
    <form action="" class="apply-job-form">
        <div class="form-title">ข้อมูลส่วนตัว</div>
        <div class="personal_info form-section">
            <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-md-2"><label>ชื่อ<span class="f-required">*</span></label></div>
                    <div class="col-sm-4">
                        <div class="select-block">
                            <select name="cmbTitle" id="cmbTitle">
                                <option value="" selected="">คำนำหน้าชื่อ</option>
                                <option value="1">นาย</option>
                                <option value="2">นาง</option>
                                <option value="3">นางสาว</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-8">
                        <input type="text" class="textbox" name="txbFName" id="txbFName" value="" maxlength="50">
                    </div>
                </div>
                <div class="col-md-6 form-row">
                    <div class="col-md-2 col-sm-3"><label>นามสกุล<span class="f-required">*</span></label></div>
                    <div class="col-md-6 col-sm-9">
                        <input type="text" class="textbox" name="txbLName" id="txbLName" value="" maxlength="50">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-md-2 col-sm-3"><label>ชื่อเล่น<span class="f-required">*</span></label></div>
                    <div class="col-md-7 col-sm-9 mb10-m">
                        <input type="text" class="textbox w100-m" name="txbNickname" id="txbNickname" maxlength="30" value="">
                    </div>
                </div>
                <div class="col-md-6 form-row">
                    <div class="col-md-2 col-sm-3"><label>เพศ<span class="f-required">*</span></label></div>
                    <div class="col-md-4 col-sm-9 mb10-m">
                        <input type="radio" name="rdoGender" id="rdoGenderM" value="M"> <label for="rdoGenderM"><span></span>ชาย</label> &nbsp; <input type="radio" name="rdoGender" id="rdoGenderF" value="F"> <label for="rdoGenderF"><span></span>หญิง</label>
                    </div>
                </div>
                <div class="col-md-6 form-row">
                    <div class="col-md-2 col-sm-3"><label>วันเกิด<span class="f-required">*</span></label></div>
                    <div class="col-md-7 col-sm-9">
                        <input type="text" name="dateBirth" id="dateBirth" class="w100-m hasDatepicker" readonly="" value="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-md-2 col-sm-3"><label>ส่วนสูง<span class="f-required">*</span></label></div>
                    <div class="col-md-10 col-sm-9">
                        <input type="text" class="textbox" name="txbHeight" id="txbHeight" style="width:50px" maxlength="3" value="" onkeypress="return numbersonly(event)">&nbsp; เซนติเมตร
                    </div>
                </div>
                <div class="col-md-6 form-row">
                    <div class="col-md-2 col-sm-3"><label>น้ำหนัก</label></div>
                    <div class="col-md-10 col-sm-9">
                        <input type="text" class="textbox" name="txbWeight" id="txbWeight" style="width:50px" maxlength="3" value="" onkeypress="return numbersonly(event)">&nbsp; กิโลกรัม
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-md-2"><label>ศาสนา<span class="f-required">*</span></label></div>
                    <div class="col-sm-3">
                        <div class="select-block">
                            <select name="cmbReligion" id="cmbReligion" onchange="javascript:ChangeReligion();">
                                <option value="" selected="">ศาสนา</option>
                                <option value="B">พุทธ</option>
                                <option value="C">คริสต์</option>
                                <option value="I">อิสลาม</option>
                                <option value="O">อื่น ๆ</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-9">
                        อื่น ๆ โปรดระบุ &nbsp;<input type="text" class="textbox" name="txbReligionOther" id="txbReligionOther" style="width:120px; background-color:#F4F4F4" maxlength="30" value="" disabled="disabled">
                    </div>
                </div>
                <div class="col-md-6 form-row">
                    <div class="col-md-2 col-sm-3"><label>ภูมิลำเนา</label></div>
                    <div class="col-md-6 col-sm-9">
                        <div class="select-block">
                            <select name="cmbBirthProvince" id="cmbBirthProvince" class="w100-m">
                                <option value="">จังหวัด</option>
                                <option value="64">กระบี่</option>
                                <option value="1">กรุงเทพมหานคร</option>
                                <option value="56">กาญจนบุรี</option>
                                <option value="34">กาฬสินธุ์</option>
                                <option value="49">กำแพงเพชร</option>
                                <option value="28">ขอนแก่น</option>
                                <option value="13">จันทบุรี</option>
                                <option value="15">ฉะเชิงเทรา</option>
                                <option value="11">ชลบุรี</option>
                                <option value="9">ชัยนาท</option>
                                <option value="25">ชัยภูมิ</option>
                                <option value="69">ชุมพร</option>
                                <option value="45">เชียงราย</option>
                                <option value="38">เชียงใหม่</option>
                                <option value="72">ตรัง</option>
                                <option value="14">ตราด</option>
                                <option value="50">ตาก</option>
                                <option value="17">นครนายก</option>
                                <option value="58">นครปฐม</option>
                                <option value="36">นครพนม</option>
                                <option value="19">นครราชสีมา</option>
                                <option value="63">นครศรีธรรมราช</option>
                                <option value="47">นครสวรรค์</option>
                                <option value="3">นนทบุรี</option>
                                <option value="76">นราธิวาส</option>
                                <option value="43">น่าน</option>
                                <option value="20">บุรีรัมย์</option>
                                <option value="4">ปทุมธานี</option>
                                <option value="62">ประจวบคีรีขันธ์</option>
                                <option value="16">ปราจีนบุรี</option>
                                <option value="74">ปัตตานี</option>
                                <option value="5">พระนครศรีอยุธยา</option>
                                <option value="44">พะเยา</option>
                                <option value="65">พังงา</option>
                                <option value="73">พัทลุง</option>
                                <option value="53">พิจิตร</option>
                                <option value="52">พิษณุโลก</option>
                                <option value="61">เพชรบุรี</option>
                                <option value="54">เพชรบูรณ์</option>
                                <option value="42">แพร่</option>
                                <option value="66">ภูเก็ต</option>
                                <option value="32">มหาสารคาม</option>
                                <option value="37">มุกดาหาร</option>
                                <option value="46">แม่ฮ่องสอน</option>
                                <option value="24">ยโสธร</option>
                                <option value="75">ยะลา</option>
                                <option value="33">ร้อยเอ็ด</option>
                                <option value="68">ระนอง</option>
                                <option value="12">ระยอง</option>
                                <option value="55">ราชบุรี</option>
                                <option value="7">ลพบุรี</option>
                                <option value="40">ลำปาง</option>
                                <option value="39">ลำพูน</option>
                                <option value="30">เลย</option>
                                <option value="22">ศรีสะเกษ</option>
                                <option value="35">สกลนคร</option>
                                <option value="70">สงขลา</option>
                                <option value="71">สตูล</option>
                                <option value="2">สมุทรปราการ</option>
                                <option value="60">สมุทรสงคราม</option>
                                <option value="59">สมุทรสาคร</option>
                                <option value="18">สระแก้ว</option>
                                <option value="10">สระบุรี</option>
                                <option value="8">สิงห์บุรี</option>
                                <option value="51">สุโขทัย</option>
                                <option value="57">สุพรรณบุรี</option>
                                <option value="67">สุราษฎร์ธานี</option>
                                <option value="21">สุรินทร์</option>
                                <option value="31">หนองคาย</option>
                                <option value="27">หนองบัวลำภู</option>
                                <option value="6">อ่างทอง</option>
                                <option value="26">อำนาจเจริญ</option>
                                <option value="29">อุดรธานี</option>
                                <option value="41">อุตรดิตถ์</option>
                                <option value="48">อุทัยธานี</option>
                                <option value="23">อุบลราชธานี</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-md-2 col-sm-3"><label>เชื้อชาติ</label></div>
                    <div class="col-sm-2">
                        <input type="radio" name="rdoNationality" id="rdoNationalityT" value="T" checked="" onclick="javascript:ChangeOther(document.frm1.rdoNationality[1], 'txbNationalityOther');"> <label for="rdoNationalityT"><span></span>ไทย</label>
                    </div>
                    <div class="col-md-8 col-md-offset-0 col-sm-9 col-sm-offset-3">
                        <input type="radio" name="rdoNationality" id="rdoNationalityO" value="O" onclick="javascript:ChangeOther(document.frm1.rdoNationality[1], 'txbNationalityOther');"> <label for="rdoNationalityO" class="txtOther"><span></span>อื่น ๆ ระบุ</label><input type="text" class="textbox" name="txbNationalityOther" id="txbNationalityOther" style="width:120px; background-color:#F4F4F4" maxlength="30" value="">
                    </div>

                </div>
                <div class="col-md-6 form-row">
                    <div class="col-md-2 col-sm-3"><label>สัญชาติ</label></div>
                    <div class="col-sm-2">
                        <input type="radio" name="rdoRace" id="rdoRaceT" value="T" checked="" onclick="javascript:ChangeOther(document.frm1.rdoRace[1], 'txbRaceOther');"> <label for="rdoRaceT"><span></span>ไทย</label>
                    </div>
                    <div class="col-md-8 col-md-offset-0 col-sm-9 col-sm-offset-3">
                        <input type="radio" name="rdoRace" id="rdoRaceO" value="O" onclick="javascript:ChangeOther(document.frm1.rdoRace[1], 'txbRaceOther');">  <label for="rdoRaceO" class="txtOther"><span></span>อื่น ๆ ระบุ</label><input type="text" class="textbox" name="txbRaceOther" id="txbRaceOther" style="width:120px; background-color:#F4F4F4" maxlength="30" value="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 form-row">
                    <div class="col-md-5 col-sm-4"><label>เกณฑ์ทหาร</label></div>
                    <div class="col-sm-7">
                        <div class="select-block">
                            <select name="cmbConscription" id="cmbConscription" onchange="javascript:ChangeConscription();">
                                <option value="" selected="">เลือก</option>
                                <option value="1">ผ่อนผัน</option>
                                <option value="2">เกณฑ์แล้ว</option>
                                <option value="3">จบ ร.ด.</option>
                                <option value="4">ยกเว้น</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 form-row">
                    เลือกยกเว้น โปรดระบุเหตุผล &nbsp;<input type="text" class="textbox w100-m" name="txbConscription" id="txbConscription" style="width:200px; background-color:#F4F4F4" maxlength="50" value="">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 form-row">
                    <div class="col-md-3"><label class="text-left">เลขบัตรประจำตัวประชาชน<span class="f-required">*</span></label></div>
                    <div class="col-md-4">
                        <input type="text" class="textbox w100-m" name="txbIDNo" id="txbIDNo" style="width:200px" maxlength="13" onkeypress="return numbersonly(event)" value="">
                    </div>
                    <div class="col-md-5">
                        โปรดระบุตัวเลข 13 หลัก โดยไม่มีช่องว่าง หรือ -
                    </div>
                </div>
            </div>
        </div>
        <div class="form-title">ข้อมูลครอบครัว</div>
        <div class="family_info form-section">
            <div class="row">
                <div class="col-sm-12 form-row">
                    <div class="col-md-2 col-sm-4">
                        <label>สถานภาพสมรส</label>
                    </div>
                    <div class="col-md-9 col-sm-8">
                        <input type="radio" name="rdoMarried" id="rdoMarriedS" value="S"> <label for="rdoMarriedS"><span></span>โสด</label> &nbsp; <input type="radio" name="rdoMarried" id="rdoMarriedM" value="M"> <label for="rdoMarriedM"><span></span>สมรส</label>&nbsp; <input type="radio" name="rdoMarried" id="rdoMarriedD" value="D"> <label for="rdoMarriedD"><span></span>หย่าร้าง</label>
                    </div>
                </div>
            </div>
            <div class="row title hide-mobile">
                <div class="col-sm-2">ครอบครัว</div>
                <div class="col-sm-3">ชื่อ-นามสกุล</div>
                <div class="col-sm-1">อายุ</div>
                <div class="col-sm-3">อาชีพ/ตำแหน่ง</div>
                <div class="col-sm-3">ที่อยู่/ที่ทำงาน</div>
            </div>
            <div class="row form-row">
                <div class="col-md-2"><label>บิดา</label></div>
                <div class="show-mobile col-sm-4 mb10-m">ชื่อนาม-นามสกุล</div>
                <div class="col-md-3 col-sm-8 mb10-m"><input type="text" class="textbox" name="txbFatherName" id="txbFatherName" maxlength="50" value=""></div>
                <div class="show-mobile col-sm-4 mb10-m">อายุ</div>
                <div class="col-md-1 col-sm-8 mb10-m"><input type="text" class="textbox" name="txbFatherAge" id="txbFatherAge" maxlength="2" value="" onkeypress="return numbersonly(event)"></div>
                <div class="show-mobile col-sm-4 mb10-m">อาชีพ/ตำแหน่ง</div>
                <div class="col-md-3 col-sm-8 mb10-m"><input type="text" class="textbox" name="txbFatherOcc" id="txbFatherOcc" maxlength="100" value=""></div>
                <div class="show-mobile col-sm-4 mb10-m">ที่อยู่/ที่ทำงาน</div>
                <div class="col-md-3 col-sm-8 mb10-m"><input type="text" class="textbox" name="txbFatherAddr" id="txbFatherAddr" maxlength="100" value=""></div>
            </div>
            <div class="row form-row">
                <div class="col-md-2"><label>มารดา</label></div>
                <div class="show-mobile col-sm-4 mb10-m">ชื่อนาม-นามสกุล</div>
                <div class="col-md-3 col-sm-8 mb10-m"><input type="text" class="textbox" name="txbMotherName" id="txbMotherName" maxlength="50" value=""></div>
                <div class="show-mobile col-sm-4 mb10-m">อายุ</div>
                <div class="col-md-1 col-sm-8 mb10-m"><input type="text" class="textbox" name="txbMotherAge" id="txbMotherAge" maxlength="2" value="" onkeypress="return numbersonly(event)"></div>
                <div class="show-mobile col-sm-4 mb10-m">อาชีพ/ตำแหน่ง</div>
                <div class="col-md-3 col-sm-8 mb10-m"><input type="text" class="textbox" name="txbMotherOcc" id="txbMotherOcc" maxlength="100" value=""></div>
                <div class="show-mobile col-sm-4 mb10-m">ที่อยู่/ที่ทำงาน</div>
                <div class="col-md-3 col-sm-8 mb10-m"><input type="text" class="textbox" name="txbMotherAddr" id="txbMotherAddr" maxlength="100" value=""></div>
            </div>
            <div class="row form-row">
                <div class="col-md-2"><label>พี่น้อง 1</label></div>
                <div class="show-mobile col-sm-4 mb10-m">ชื่อนาม-นามสกุล</div>
                <div class="col-md-3 col-sm-8 mb10-m"><input type="text" class="textbox" name="txbCousin1Name" id="txbCousin1Name" maxlength="50" value=""></div>
                <div class="show-mobile col-sm-4 mb10-m">อายุ</div>
                <div class="col-md-1 col-sm-8 mb10-m"><input type="text" class="textbox" name="txbCousin1Age" id="txbCousin1Age" maxlength="2" value="" onkeypress="return numbersonly(event)"></div>
                <div class="show-mobile col-sm-4 mb10-m">อาชีพ/ตำแหน่ง</div>
                <div class="col-md-3 col-sm-8 mb10-m"><input type="text" class="textbox" name="txbCousin1Occ" id="txbCousin1Occ" maxlength="100" value=""></div>
                <div class="show-mobile col-sm-4 mb10-m">ที่อยู่/ที่ทำงาน</div>
                <div class="col-md-3 col-sm-8 mb10-m"><input type="text" class="textbox" name="txbCousin1Addr" id="txbCousin1Addr" maxlength="100" value=""></div>
            </div>
            <div class="row form-row">
                <div class="col-md-2"><label>พี่น้อง 2</label></div>
                <div class="show-mobile col-sm-4 mb10-m">ชื่อนาม-นามสกุล</div>
                <div class="col-md-3 col-sm-8 mb10-m"><input type="text" class="textbox" name="txbCousin2Name" id="txbCousin2Name" maxlength="50" value=""></div>
                <div class="show-mobile col-sm-4 mb10-m">อายุ</div>
                <div class="col-md-1 col-sm-8 mb10-m"><input type="text" class="textbox" name="txbCousin2Age" id="txbCousin2Age" maxlength="2" value="" onkeypress="return numbersonly(event)"></div>
                <div class="show-mobile col-sm-4 mb10-m">อาชีพ/ตำแหน่ง</div>
                <div class="col-md-3 col-sm-8 mb10-m"><input type="text" class="textbox" name="txbCousin2Occ" id="txbCousin2Occ" maxlength="100" value=""></div>
                <div class="show-mobile col-sm-4 mb10-m">ที่อยู่/ที่ทำงาน</div>
                <div class="col-md-3 col-sm-8 mb10-m"><input type="text" class="textbox" name="txbCousin2Addr" id="txbCousin2Addr" maxlength="100" value=""></div>
            </div>
            <div class="row form-row">
                <div class="col-md-2"><label>พี่น้อง 3</label></div>
                <div class="show-mobile col-sm-4 mb10-m">ชื่อนาม-นามสกุล</div>
                <div class="col-md-3 col-sm-8 mb10-m"><input type="text" class="textbox" name="txbCousin3Name" id="txbCousin3Name" maxlength="50" value=""></div>
                <div class="show-mobile col-sm-4 mb10-m">อายุ</div>
                <div class="col-md-1 col-sm-8 mb10-m"><input type="text" class="textbox" name="txbCousin3Age" id="txbCousin3Age" maxlength="2" value="" onkeypress="return numbersonly(event)"></div>
                <div class="show-mobile col-sm-4 mb10-m">อาชีพ/ตำแหน่ง</div>
                <div class="col-md-3 col-sm-8 mb10-m"><input type="text" class="textbox" name="txbCousin3Occ" id="txbCousin3Occ" maxlength="100" value=""></div>
                <div class="show-mobile col-sm-4 mb10-m">ที่อยู่/ที่ทำงาน</div>
                <div class="col-md-3 col-sm-8 mb10-m"><input type="text" class="textbox" name="txbCousin3Addr" id="txbCousin3Addr" maxlength="100" value=""></div>
            </div>
            <div class="row form-row">
                <div class="col-md-2"><label>คู่สมรส</label></div>
                <div class="show-mobile col-sm-4 mb10-m">ชื่อนาม-นามสกุล</div>
                <div class="col-md-3 col-sm-8 mb10-m"><input type="text" class="textbox" name="txbSpouseName" id="txbSpouseName" maxlength="50" value=""></div>
                <div class="show-mobile col-sm-4 mb10-m">อายุ</div>
                <div class="col-md-1 col-sm-8 mb10-m"><input type="text" class="textbox" name="txbSpouseAge" id="txbSpouseAge" maxlength="2" value="" onkeypress="return numbersonly(event)"></div>
                <div class="show-mobile col-sm-4 mb10-m">อาชีพ/ตำแหน่ง</div>
                <div class="col-md-3 col-sm-8 mb10-m"><input type="text" class="textbox" name="txbSpouseOcc" id="txbSpouseOcc" maxlength="100" value=""></div>
                <div class="show-mobile col-sm-4 mb10-m">ที่อยู่/ที่ทำงาน</div>
                <div class="col-md-3 col-sm-8 mb10-m"><input type="text" class="textbox" name="txbSpouseAddr" id="txbSpouseAddr" maxlength="100" value=""></div>
            </div>
            <div class="row form-row">
                <div class="col-md-2 col-sm-4"><label>จำนวนบุตร</label></div>
                <div class="col-sm-4"><input type="text" class="textbox" name="txbChildNum" id="txbChildNum" style="width:50px" maxlength="1" value="" onkeypress="return numbersonly(event)"> &nbsp; คน</div>
            </div>
        </div>
        <div class="form-title">ข้อมูลการติดต่อ</div>
        <div class="contact_info form-section">
            <p class="title">ที่อยู่ปัจจุบันที่ติดต่อได้</p>
            <div class="row">
                <div class="col-sm-12 form-row">
                    <div class="col-md-2 col-sm-3"><label>ที่อยู่<span class="f-required">*</span></label></div>
                    <div class="col-md-10 col-sm-9">
                        <input type="text" class="textbox" name="txbAddress" id="txbAddress" maxlength="255" value="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 form-row">
                    <div class="col-md-2 col-sm-3"><label>จังหวัด<span class="f-required">*</span><span class="f-required">*</span></label></div>
                    <div class="col-md-3 col-sm-9">
                        <div class="select-block">
                            <select name="cmbContactProvince" id="cmbContactProvince" class="w100-m">
                                <option value="">จังหวัด</option>
                                <option value="64">กระบี่</option>
                                <option value="1">กรุงเทพมหานคร</option>
                                <option value="56">กาญจนบุรี</option>
                                <option value="34">กาฬสินธุ์</option>
                                <option value="49">กำแพงเพชร</option>
                                <option value="28">ขอนแก่น</option>
                                <option value="13">จันทบุรี</option>
                                <option value="15">ฉะเชิงเทรา</option>
                                <option value="11">ชลบุรี</option>
                                <option value="9">ชัยนาท</option>
                                <option value="25">ชัยภูมิ</option>
                                <option value="69">ชุมพร</option>
                                <option value="45">เชียงราย</option>
                                <option value="38">เชียงใหม่</option>
                                <option value="72">ตรัง</option>
                                <option value="14">ตราด</option>
                                <option value="50">ตาก</option>
                                <option value="17">นครนายก</option>
                                <option value="58">นครปฐม</option>
                                <option value="36">นครพนม</option>
                                <option value="19">นครราชสีมา</option>
                                <option value="63">นครศรีธรรมราช</option>
                                <option value="47">นครสวรรค์</option>
                                <option value="3">นนทบุรี</option>
                                <option value="76">นราธิวาส</option>
                                <option value="43">น่าน</option>
                                <option value="20">บุรีรัมย์</option>
                                <option value="4">ปทุมธานี</option>
                                <option value="62">ประจวบคีรีขันธ์</option>
                                <option value="16">ปราจีนบุรี</option>
                                <option value="74">ปัตตานี</option>
                                <option value="5">พระนครศรีอยุธยา</option>
                                <option value="44">พะเยา</option>
                                <option value="65">พังงา</option>
                                <option value="73">พัทลุง</option>
                                <option value="53">พิจิตร</option>
                                <option value="52">พิษณุโลก</option>
                                <option value="61">เพชรบุรี</option>
                                <option value="54">เพชรบูรณ์</option>
                                <option value="42">แพร่</option>
                                <option value="66">ภูเก็ต</option>
                                <option value="32">มหาสารคาม</option>
                                <option value="37">มุกดาหาร</option>
                                <option value="46">แม่ฮ่องสอน</option>
                                <option value="24">ยโสธร</option>
                                <option value="75">ยะลา</option>
                                <option value="33">ร้อยเอ็ด</option>
                                <option value="68">ระนอง</option>
                                <option value="12">ระยอง</option>
                                <option value="55">ราชบุรี</option>
                                <option value="7">ลพบุรี</option>
                                <option value="40">ลำปาง</option>
                                <option value="39">ลำพูน</option>
                                <option value="30">เลย</option>
                                <option value="22">ศรีสะเกษ</option>
                                <option value="35">สกลนคร</option>
                                <option value="70">สงขลา</option>
                                <option value="71">สตูล</option>
                                <option value="2">สมุทรปราการ</option>
                                <option value="60">สมุทรสงคราม</option>
                                <option value="59">สมุทรสาคร</option>
                                <option value="18">สระแก้ว</option>
                                <option value="10">สระบุรี</option>
                                <option value="8">สิงห์บุรี</option>
                                <option value="51">สุโขทัย</option>
                                <option value="57">สุพรรณบุรี</option>
                                <option value="67">สุราษฎร์ธานี</option>
                                <option value="21">สุรินทร์</option>
                                <option value="31">หนองคาย</option>
                                <option value="27">หนองบัวลำภู</option>
                                <option value="6">อ่างทอง</option>
                                <option value="26">อำนาจเจริญ</option>
                                <option value="29">อุดรธานี</option>
                                <option value="41">อุตรดิตถ์</option>
                                <option value="48">อุทัยธานี</option>
                                <option value="23">อุบลราชธานี</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 form-row">
                    <div class="col-md-3"><label>เบอร์โทรศัพท์ที่สามารถติดต่อได้<span class="f-required">*</span></label></div>
                    <div class="col-md-9 col-md-offset-0 col-sm-9 col-sm-offset-3">
                        <input type="text" class="textbox w100-m" name="txbContactTel" id="txbContactTel" maxlength="50" style="width:300px" value="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 form-row">
                    <div class="col-md-2 col-sm-3"><label>E-mail</label></div>
                    <div class="col-md-4 col-sm-9">
                        <input type="text" class="textbox" name="txbContactEmail" id="txbContactEmail" maxlength="60" value="">
                    </div>
                </div>
            </div>
            <p class="title">บุคคลที่ติดต่อได้ในกรณีฉุกเฉิน</p>
            <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-md-4 col-sm-3"><label>ชื่อ<span class="f-required">*</span></label></div>
                    <div class="col-md-8 col-sm-9">
                        <input type="text" class="textbox" name="txbEmergencyPerson" id="txbEmergencyPerson" maxlength="100" value="">
                    </div>
                </div>
                <div class="col-md-6 form-row">
                    <div class="col-md-4 col-sm-4"><label>เบอร์โทรศัพท์<span class="f-required">*</span></label></div>
                    <div class="col-md-8 col-sm-8 ">
                        <input type="text" class="textbox" name="txbEmergencyTel" id="txbEmergencyTel" maxlength="50" value="">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-title">ข้อมูลการศึกษา</div>
        <div class="education_info form-section">
            <div class="row">
                <div class="col-sm-12 form-row">
                    <div class="col-md-2 col-sm-4"><label>การศึกษาสูงสุด<span class="f-required">*</span></label></div>
                    <div class="col-md-6 col-sm-8">
                        <div class="select-block">
                            <select name="cmbHighestGraduate" id="cmbHighestGraduate">
                                <option value="">เลือกวุฒิการศึกษา</option>
                                <option value="1">ปวส.</option>
                                <option value="2">ปริญญาตรี</option>
                                <option value="3">ปริญญาโท</option>
                                <option value="4">ปริญญาเอก</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <b>ประวัติการศึกษา</b>
            <div class="col-sm-12 wrap-tab-btn">
                <div class="row">
                    <div id="headTabStudy1" class="col-sm-4 tab-btn" style="background-color:#E0E0E0;"><a href="javascript:OpenTabStudy('headTabStudy1','tabStudy1');">การศึกษา 1</a></div>
                    <div id="headTabStudy2" class="col-sm-4 tab-btn"><a href="javascript:OpenTabStudy('headTabStudy2','tabStudy2');">การศึกษา 2</a></div>
                    <div id="headTabStudy3" class="col-sm-4 tab-btn"><a href="javascript:OpenTabStudy('headTabStudy3','tabStudy3');">การศึกษา 3</a></div>
                </div>
            </div>
            <div claas="col-sm-12">

                <div id="tabStudy1" style=" border-bottom:none">

                    <div class="row">
                        <div class="col-sm-12 form-row">
                            <div class="col-md-2">1. ระยะเวลา</div>
                            <div class="col-md-1 col-sm-4 mb10-m">
                                ตั้งแต่
                            </div>
                            <div class="col-md-3 col-sm-8 mb10-m">
                                <div class="select-block">
                                    <select name="cmbStudyFromMonth1" id="cmbStudyFromMonth1">
                                        <option value=""></option>
                                        <option value="1">มกราคม</option>
                                        <option value="2">กุมภาพันธ์</option>
                                        <option value="3">มีนาคม</option>
                                        <option value="4">เมษายน</option>
                                        <option value="5">พฤษภาคม</option>
                                        <option value="6">มิถุนายน</option>
                                        <option value="7">กรกฎาคม</option>
                                        <option value="8">สิงหาคม</option>
                                        <option value="9">กันยายน</option>
                                        <option value="10">ตุลาคม</option>
                                        <option value="11">พฤศจิกายน</option>
                                        <option value="12">ธันวาคม</option>
                                    </select>
                                </div>
                                <div class="select-block">
                                    <select name="cmbStudyFromYear1" id="cmbStudyFromYear1">
                                        <option value=""></option>
                                        <option value="2510">2510</option>
                                        <option value="2511">2511</option>
                                        <option value="2512">2512</option>
                                        <option value="2513">2513</option>
                                        <option value="2514">2514</option>
                                        <option value="2515">2515</option>
                                        <option value="2516">2516</option>
                                        <option value="2517">2517</option>
                                        <option value="2518">2518</option>
                                        <option value="2519">2519</option>
                                        <option value="2520">2520</option>
                                        <option value="2521">2521</option>
                                        <option value="2522">2522</option>
                                        <option value="2523">2523</option>
                                        <option value="2524">2524</option>
                                        <option value="2525">2525</option>
                                        <option value="2526">2526</option>
                                        <option value="2527">2527</option>
                                        <option value="2528">2528</option>
                                        <option value="2529">2529</option>
                                        <option value="2530">2530</option>
                                        <option value="2531">2531</option>
                                        <option value="2532">2532</option>
                                        <option value="2533">2533</option>
                                        <option value="2534">2534</option>
                                        <option value="2535">2535</option>
                                        <option value="2536">2536</option>
                                        <option value="2537">2537</option>
                                        <option value="2538">2538</option>
                                        <option value="2539">2539</option>
                                        <option value="2540">2540</option>
                                        <option value="2541">2541</option>
                                        <option value="2542">2542</option>
                                        <option value="2543">2543</option>
                                        <option value="2544">2544</option>
                                        <option value="2545">2545</option>
                                        <option value="2546">2546</option>
                                        <option value="2547">2547</option>
                                        <option value="2548">2548</option>
                                        <option value="2549">2549</option>
                                        <option value="2550">2550</option>
                                        <option value="2551">2551</option>
                                        <option value="2552">2552</option>
                                        <option value="2553">2553</option>
                                        <option value="2554">2554</option>
                                        <option value="2555">2555</option>
                                        <option value="2556">2556</option>
                                        <option value="2557">2557</option>
                                        <option value="2558">2558</option>
                                        <option value="2559">2559</option>
                                        <option value="2560">2560</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-4">
                                ถึง
                            </div>
                            <div class="col-md-3 col-sm-8">
                                <div class="select-block">
                                    <select name="cmbStudyToMonth1" id="cmbStudyToMonth1">
                                        <option value=""></option>
                                        <option value="1">มกราคม</option>
                                        <option value="2">กุมภาพันธ์</option>
                                        <option value="3">มีนาคม</option>
                                        <option value="4">เมษายน</option>
                                        <option value="5">พฤษภาคม</option>
                                        <option value="6">มิถุนายน</option>
                                        <option value="7">กรกฎาคม</option>
                                        <option value="8">สิงหาคม</option>
                                        <option value="9">กันยายน</option>
                                        <option value="10">ตุลาคม</option>
                                        <option value="11">พฤศจิกายน</option>
                                        <option value="12">ธันวาคม</option>
                                    </select>
                                </div>
                                <div class="select-block">
                                    <select name="cmbStudyToYear1" id="cmbStudyToYear1">
                                        <option value=""></option>
                                        <option value="2510">2510</option>
                                        <option value="2511">2511</option>
                                        <option value="2512">2512</option>
                                        <option value="2513">2513</option>
                                        <option value="2514">2514</option>
                                        <option value="2515">2515</option>
                                        <option value="2516">2516</option>
                                        <option value="2517">2517</option>
                                        <option value="2518">2518</option>
                                        <option value="2519">2519</option>
                                        <option value="2520">2520</option>
                                        <option value="2521">2521</option>
                                        <option value="2522">2522</option>
                                        <option value="2523">2523</option>
                                        <option value="2524">2524</option>
                                        <option value="2525">2525</option>
                                        <option value="2526">2526</option>
                                        <option value="2527">2527</option>
                                        <option value="2528">2528</option>
                                        <option value="2529">2529</option>
                                        <option value="2530">2530</option>
                                        <option value="2531">2531</option>
                                        <option value="2532">2532</option>
                                        <option value="2533">2533</option>
                                        <option value="2534">2534</option>
                                        <option value="2535">2535</option>
                                        <option value="2536">2536</option>
                                        <option value="2537">2537</option>
                                        <option value="2538">2538</option>
                                        <option value="2539">2539</option>
                                        <option value="2540">2540</option>
                                        <option value="2541">2541</option>
                                        <option value="2542">2542</option>
                                        <option value="2543">2543</option>
                                        <option value="2544">2544</option>
                                        <option value="2545">2545</option>
                                        <option value="2546">2546</option>
                                        <option value="2547">2547</option>
                                        <option value="2548">2548</option>
                                        <option value="2549">2549</option>
                                        <option value="2550">2550</option>
                                        <option value="2551">2551</option>
                                        <option value="2552">2552</option>
                                        <option value="2553">2553</option>
                                        <option value="2554">2554</option>
                                        <option value="2555">2555</option>
                                        <option value="2556">2556</option>
                                        <option value="2557">2557</option>
                                        <option value="2558">2558</option>
                                        <option value="2559">2559</option>
                                        <option value="2560">2560</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label class="text-left">สถาบันการศึกษา</label></div>
                            <div class="col-sm-8"><input type="text" class="textbox" name="txbGraduateInstitute1" id="txbGraduateInstitute1" maxlength="50" value=""></div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label>คณะ</label></div>
                            <div class="col-sm-8"><input type="text" class="textbox" name="txbGraduateFaculty1" id="txbGraduateFaculty1" maxlength="50" value=""></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label class="text-left">กลุ่มสาขาวิชา</label></div>
                            <div class="col-sm-8"><input type="text" class="textbox" name="txbGraduateField1" id="txbGraduateField1" maxlength="50" value=""></div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label>ระดับการศึกษา</label></div>
                            <div class="col-sm-8">
                                <div class="select-block">
                                    <select name="cmbGraduateGraduate1" id="cmbGraduateGraduate1" class="w100-m">
                                        <option value="">เลือกวุฒิการศึกษา</option>
                                        <option value="1">ปวส.</option>
                                        <option value="2">ปริญญาตรี</option>
                                        <option value="3">ปริญญาโท</option>
                                        <option value="4">ปริญญาเอก</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label class="text-left">เกรดเฉลี่ย</label></div>
                            <div class="col-sm-8"><input type="text" class="textbox" name="txbGraduatePoint1" id="txbGraduatePoint1" maxlength="5" style="width:70px" onkeypress="return decimalonly(event)" value=""></div>
                        </div>
                    </div>

                </div>

                <div id="tabStudy2" style="display:none; border-bottom:none">

                    <div class="row">
                        <div class="col-sm-12 form-row">
                            <div class="col-md-2">2. ระยะเวลา</div>
                            <div class="col-md-1 col-sm-4 mb10-m">
                                ตั้งแต่
                            </div>
                            <div class="col-md-3 col-sm-8 mb10-m">
                                <div class="select-block">
                                    <select name="cmbStudyFromMonth2" id="cmbStudyFromMonth2">
                                        <option value=""></option>
                                        <option value="1">มกราคม</option>
                                        <option value="2">กุมภาพันธ์</option>
                                        <option value="3">มีนาคม</option>
                                        <option value="4">เมษายน</option>
                                        <option value="5">พฤษภาคม</option>
                                        <option value="6">มิถุนายน</option>
                                        <option value="7">กรกฎาคม</option>
                                        <option value="8">สิงหาคม</option>
                                        <option value="9">กันยายน</option>
                                        <option value="10">ตุลาคม</option>
                                        <option value="11">พฤศจิกายน</option>
                                        <option value="12">ธันวาคม</option>
                                    </select>
                                </div>
                                <div class="select-block">
                                    <select name="cmbStudyFromYear2" id="cmbStudyFromYear2">
                                        <option value=""></option>
                                        <option value="2510">2510</option>
                                        <option value="2511">2511</option>
                                        <option value="2512">2512</option>
                                        <option value="2513">2513</option>
                                        <option value="2514">2514</option>
                                        <option value="2515">2515</option>
                                        <option value="2516">2516</option>
                                        <option value="2517">2517</option>
                                        <option value="2518">2518</option>
                                        <option value="2519">2519</option>
                                        <option value="2520">2520</option>
                                        <option value="2521">2521</option>
                                        <option value="2522">2522</option>
                                        <option value="2523">2523</option>
                                        <option value="2524">2524</option>
                                        <option value="2525">2525</option>
                                        <option value="2526">2526</option>
                                        <option value="2527">2527</option>
                                        <option value="2528">2528</option>
                                        <option value="2529">2529</option>
                                        <option value="2530">2530</option>
                                        <option value="2531">2531</option>
                                        <option value="2532">2532</option>
                                        <option value="2533">2533</option>
                                        <option value="2534">2534</option>
                                        <option value="2535">2535</option>
                                        <option value="2536">2536</option>
                                        <option value="2537">2537</option>
                                        <option value="2538">2538</option>
                                        <option value="2539">2539</option>
                                        <option value="2540">2540</option>
                                        <option value="2541">2541</option>
                                        <option value="2542">2542</option>
                                        <option value="2543">2543</option>
                                        <option value="2544">2544</option>
                                        <option value="2545">2545</option>
                                        <option value="2546">2546</option>
                                        <option value="2547">2547</option>
                                        <option value="2548">2548</option>
                                        <option value="2549">2549</option>
                                        <option value="2550">2550</option>
                                        <option value="2551">2551</option>
                                        <option value="2552">2552</option>
                                        <option value="2553">2553</option>
                                        <option value="2554">2554</option>
                                        <option value="2555">2555</option>
                                        <option value="2556">2556</option>
                                        <option value="2557">2557</option>
                                        <option value="2558">2558</option>
                                        <option value="2559">2559</option>
                                        <option value="2560">2560</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-4">
                                ถึง
                            </div>
                            <div class="col-md-3 col-sm-8">
                                <div class="select-block">
                                    <select name="cmbStudyToMonth2" id="cmbStudyToMonth2">
                                        <option value=""></option>
                                        <option value="1">มกราคม</option>
                                        <option value="2">กุมภาพันธ์</option>
                                        <option value="3">มีนาคม</option>
                                        <option value="4">เมษายน</option>
                                        <option value="5">พฤษภาคม</option>
                                        <option value="6">มิถุนายน</option>
                                        <option value="7">กรกฎาคม</option>
                                        <option value="8">สิงหาคม</option>
                                        <option value="9">กันยายน</option>
                                        <option value="10">ตุลาคม</option>
                                        <option value="11">พฤศจิกายน</option>
                                        <option value="12">ธันวาคม</option>
                                    </select>
                                </div>
                                <div class="select-block">
                                    <select name="cmbStudyToYear2" id="cmbStudyToYear2">
                                        <option value=""></option>
                                        <option value="2510">2510</option>
                                        <option value="2511">2511</option>
                                        <option value="2512">2512</option>
                                        <option value="2513">2513</option>
                                        <option value="2514">2514</option>
                                        <option value="2515">2515</option>
                                        <option value="2516">2516</option>
                                        <option value="2517">2517</option>
                                        <option value="2518">2518</option>
                                        <option value="2519">2519</option>
                                        <option value="2520">2520</option>
                                        <option value="2521">2521</option>
                                        <option value="2522">2522</option>
                                        <option value="2523">2523</option>
                                        <option value="2524">2524</option>
                                        <option value="2525">2525</option>
                                        <option value="2526">2526</option>
                                        <option value="2527">2527</option>
                                        <option value="2528">2528</option>
                                        <option value="2529">2529</option>
                                        <option value="2530">2530</option>
                                        <option value="2531">2531</option>
                                        <option value="2532">2532</option>
                                        <option value="2533">2533</option>
                                        <option value="2534">2534</option>
                                        <option value="2535">2535</option>
                                        <option value="2536">2536</option>
                                        <option value="2537">2537</option>
                                        <option value="2538">2538</option>
                                        <option value="2539">2539</option>
                                        <option value="2540">2540</option>
                                        <option value="2541">2541</option>
                                        <option value="2542">2542</option>
                                        <option value="2543">2543</option>
                                        <option value="2544">2544</option>
                                        <option value="2545">2545</option>
                                        <option value="2546">2546</option>
                                        <option value="2547">2547</option>
                                        <option value="2548">2548</option>
                                        <option value="2549">2549</option>
                                        <option value="2550">2550</option>
                                        <option value="2551">2551</option>
                                        <option value="2552">2552</option>
                                        <option value="2553">2553</option>
                                        <option value="2554">2554</option>
                                        <option value="2555">2555</option>
                                        <option value="2556">2556</option>
                                        <option value="2557">2557</option>
                                        <option value="2558">2558</option>
                                        <option value="2559">2559</option>
                                        <option value="2560">2560</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label class="text-left">สถาบันการศึกษา</label></div>
                            <div class="col-sm-8"><input type="text" class="textbox" name="txbGraduateInstitute2" id="txbGraduateInstitute2" maxlength="50" value=""></div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label>คณะ</label></div>
                            <div class="col-sm-8"><input type="text" class="textbox" name="txbGraduateFaculty2" id="txbGraduateFaculty2" maxlength="50" value=""></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label class="text-left">กลุ่มสาขาวิชา</label></div>
                            <div class="col-sm-8"><input type="text" class="textbox" name="txbGraduateField2" id="txbGraduateField2" maxlength="50" value=""></div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label>ระดับการศึกษา</label></div>
                            <div class="col-sm-8">
                                <div class="select-block">
                                    <select name="cmbGraduateGraduate2" id="cmbGraduateGraduate2" class="w100-m">
                                        <option value="">เลือกวุฒิการศึกษา</option>
                                        <option value="1">ปวส.</option>
                                        <option value="2">ปริญญาตรี</option>
                                        <option value="3">ปริญญาโท</option>
                                        <option value="4">ปริญญาเอก</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label class="text-left">เกรดเฉลี่ย</label></div>
                            <div class="col-sm-8"><input type="text" class="textbox" name="txbGraduatePoint2" id="txbGraduatePoint2" maxlength="5" style="width:70px" onkeypress="return decimalonly(event)" value=""></div>
                        </div>
                    </div>

                </div>

                <div id="tabStudy3" style="display:none; border-bottom:none">

                    <div class="row">
                        <div class="col-sm-12 form-row">
                            <div class="col-md-2">3. ระยะเวลา</div>
                            <div class="col-md-1 col-sm-4 mb10-m">
                                ตั้งแต่
                            </div>
                            <div class="col-md-3 col-sm-8 mb10-m">
                                <div class="select-block">
                                    <select name="cmbStudyFromMonth3" id="cmbStudyFromMonth3">
                                        <option value=""></option>
                                        <option value="1">มกราคม</option>
                                        <option value="2">กุมภาพันธ์</option>
                                        <option value="3">มีนาคม</option>
                                        <option value="4">เมษายน</option>
                                        <option value="5">พฤษภาคม</option>
                                        <option value="6">มิถุนายน</option>
                                        <option value="7">กรกฎาคม</option>
                                        <option value="8">สิงหาคม</option>
                                        <option value="9">กันยายน</option>
                                        <option value="10">ตุลาคม</option>
                                        <option value="11">พฤศจิกายน</option>
                                        <option value="12">ธันวาคม</option>
                                    </select>
                                </div>
                                <div class="select-block">
                                    <select name="cmbStudyFromYear3" id="cmbStudyFromYear3">
                                        <option value=""></option>
                                        <option value="2510">2510</option>
                                        <option value="2511">2511</option>
                                        <option value="2512">2512</option>
                                        <option value="2513">2513</option>
                                        <option value="2514">2514</option>
                                        <option value="2515">2515</option>
                                        <option value="2516">2516</option>
                                        <option value="2517">2517</option>
                                        <option value="2518">2518</option>
                                        <option value="2519">2519</option>
                                        <option value="2520">2520</option>
                                        <option value="2521">2521</option>
                                        <option value="2522">2522</option>
                                        <option value="2523">2523</option>
                                        <option value="2524">2524</option>
                                        <option value="2525">2525</option>
                                        <option value="2526">2526</option>
                                        <option value="2527">2527</option>
                                        <option value="2528">2528</option>
                                        <option value="2529">2529</option>
                                        <option value="2530">2530</option>
                                        <option value="2531">2531</option>
                                        <option value="2532">2532</option>
                                        <option value="2533">2533</option>
                                        <option value="2534">2534</option>
                                        <option value="2535">2535</option>
                                        <option value="2536">2536</option>
                                        <option value="2537">2537</option>
                                        <option value="2538">2538</option>
                                        <option value="2539">2539</option>
                                        <option value="2540">2540</option>
                                        <option value="2541">2541</option>
                                        <option value="2542">2542</option>
                                        <option value="2543">2543</option>
                                        <option value="2544">2544</option>
                                        <option value="2545">2545</option>
                                        <option value="2546">2546</option>
                                        <option value="2547">2547</option>
                                        <option value="2548">2548</option>
                                        <option value="2549">2549</option>
                                        <option value="2550">2550</option>
                                        <option value="2551">2551</option>
                                        <option value="2552">2552</option>
                                        <option value="2553">2553</option>
                                        <option value="2554">2554</option>
                                        <option value="2555">2555</option>
                                        <option value="2556">2556</option>
                                        <option value="2557">2557</option>
                                        <option value="2558">2558</option>
                                        <option value="2559">2559</option>
                                        <option value="2560">2560</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-4">
                                ถึง
                            </div>
                            <div class="col-md-3 col-sm-8">
                                <div class="select-block">
                                    <select name="cmbStudyToMonth3" id="cmbStudyToMonth3">
                                        <option value=""></option>
                                        <option value="1">มกราคม</option>
                                        <option value="2">กุมภาพันธ์</option>
                                        <option value="3">มีนาคม</option>
                                        <option value="4">เมษายน</option>
                                        <option value="5">พฤษภาคม</option>
                                        <option value="6">มิถุนายน</option>
                                        <option value="7">กรกฎาคม</option>
                                        <option value="8">สิงหาคม</option>
                                        <option value="9">กันยายน</option>
                                        <option value="10">ตุลาคม</option>
                                        <option value="11">พฤศจิกายน</option>
                                        <option value="12">ธันวาคม</option>
                                    </select>
                                </div>
                                <div class="select-block">
                                    <select name="cmbStudyToYear3" id="cmbStudyToYear3">
                                        <option value=""></option>
                                        <option value="2510">2510</option>
                                        <option value="2511">2511</option>
                                        <option value="2512">2512</option>
                                        <option value="2513">2513</option>
                                        <option value="2514">2514</option>
                                        <option value="2515">2515</option>
                                        <option value="2516">2516</option>
                                        <option value="2517">2517</option>
                                        <option value="2518">2518</option>
                                        <option value="2519">2519</option>
                                        <option value="2520">2520</option>
                                        <option value="2521">2521</option>
                                        <option value="2522">2522</option>
                                        <option value="2523">2523</option>
                                        <option value="2524">2524</option>
                                        <option value="2525">2525</option>
                                        <option value="2526">2526</option>
                                        <option value="2527">2527</option>
                                        <option value="2528">2528</option>
                                        <option value="2529">2529</option>
                                        <option value="2530">2530</option>
                                        <option value="2531">2531</option>
                                        <option value="2532">2532</option>
                                        <option value="2533">2533</option>
                                        <option value="2534">2534</option>
                                        <option value="2535">2535</option>
                                        <option value="2536">2536</option>
                                        <option value="2537">2537</option>
                                        <option value="2538">2538</option>
                                        <option value="2539">2539</option>
                                        <option value="2540">2540</option>
                                        <option value="2541">2541</option>
                                        <option value="2542">2542</option>
                                        <option value="2543">2543</option>
                                        <option value="2544">2544</option>
                                        <option value="2545">2545</option>
                                        <option value="2546">2546</option>
                                        <option value="2547">2547</option>
                                        <option value="2548">2548</option>
                                        <option value="2549">2549</option>
                                        <option value="2550">2550</option>
                                        <option value="2551">2551</option>
                                        <option value="2552">2552</option>
                                        <option value="2553">2553</option>
                                        <option value="2554">2554</option>
                                        <option value="2555">2555</option>
                                        <option value="2556">2556</option>
                                        <option value="2557">2557</option>
                                        <option value="2558">2558</option>
                                        <option value="2559">2559</option>
                                        <option value="2560">2560</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label class="text-left">สถาบันการศึกษา</label></div>
                            <div class="col-sm-8"><input type="text" class="textbox" name="txbGraduateInstitute3" id="txbGraduateInstitute3" maxlength="50" value=""></div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label>คณะ</label></div>
                            <div class="col-sm-8"><input type="text" class="textbox" name="txbGraduateFaculty3" id="txbGraduateFaculty3" maxlength="50" value=""></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label class="text-left">กลุ่มสาขาวิชา</label></div>
                            <div class="col-sm-8"><input type="text" class="textbox" name="txbGraduateField3" id="txbGraduateField3" maxlength="50" value=""></div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label>ระดับการศึกษา</label></div>
                            <div class="col-sm-8">
                                <div class="select-block">
                                    <select name="cmbGraduateGraduate3" id="cmbGraduateGraduate3" class="w100-m">
                                        <option value="">เลือกวุฒิการศึกษา</option>
                                        <option value="1">ปวส.</option>
                                        <option value="2">ปริญญาตรี</option>
                                        <option value="3">ปริญญาโท</option>
                                        <option value="4">ปริญญาเอก</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label class="text-left">เกรดเฉลี่ย</label></div>
                            <div class="col-sm-8"><input type="text" class="textbox" name="txbGraduatePoint3" id="txbGraduatePoint3" maxlength="5" style="width:70px" onkeypress="return decimalonly(event)" value=""></div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <div class="form-title">ข้อมูลการทำงาน</div>
        <div class="experience_info form-section">
            <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-md-4 col-sm-5"><label>เพิ่งสำเร็จการศึกษา<span class="f-required">*</span></label></div>
                    <div class="col-md-8 col-sm-7">
                        <input type="radio" name="rdoJustGraduate" id="rdoJustGraduateY" value="Y" onclick="javascript:JustGraduate();"> &nbsp; <label for="rdoJustGraduateY"><span></span>ใช่</label> &nbsp; &nbsp; <input type="radio" name="rdoJustGraduate" id="rdoJustGraduateN" value="N" onclick="javascript:JustGraduate();" checked=""> &nbsp; <label for="rdoJustGraduateN"><span></span>ไม่ใช่</label>
                    </div>
                </div>
                <div class="col-md-6 form-row">
                    <div class="col-md-4 col-sm-6"><label>ประสบการณ์การทำงาน</label></div>
                    <div class="col-md-8 col-sm-6">
                        <input type="text" class="textbox" name="txbWorkExperience" id="txbWorkExperience" maxlength="2" style="width:50px" onkeypress="return numbersonly(event)" value=""> &nbsp; ปี
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-sm-4">สายงานล่าสุด</div>
                    <div class="col-sm-8">
                        <div class="select-block">
                            <select name="cmbLastWorkField" id="cmbLastWorkField" class="w100-m">
                                <option value="">เลือกสายงาน</option>
                                <option value="1">กฏหมาย</option>
                                <option value="2">กราฟฟิก ดีไซน์</option>
                                <option value="7">การเกษตรกรรมและทรัพยากรธรรมชาติ</option>
                                <option value="3">การคมนาคมขนส่ง</option>
                                <option value="4">การค้าระหว่างประเทศ</option>
                                <option value="8">การเงิน</option>
                                <option value="5">การตลาด</option>
                                <option value="6">การศึกษา/การฝึกอบรม</option>
                                <option value="27">โฆษณา/ประชาสัมพันธ์/นิเทศศาสตร์</option>
                                <option value="9">งานขาย</option>
                                <option value="10">จัดซื้อ</option>
                                <option value="11">ทรัพยากรบุคคล</option>
                                <option value="12">ธุรการ</option>
                                <option value="13">บริการลูกค้า</option>
                                <option value="14">บัญชี</option>
                                <option value="15">ผู้บริหารระดับสูง</option>
                                <option value="16">ฝ่ายผลิต/ผลิตภัณฑ์</option>
                                <option value="24">เภสัชกร/แพทย์/สาธารณสุข</option>
                                <option value="17">มนุษยศาสตร์</option>
                                <option value="25">เลขานุการ</option>
                                <option value="18">วิจัยและพัฒนา/วิทยาศาสตร์</option>
                                <option value="19">วิศวกร/ช่างเทคนิค</option>
                                <option value="20">ศิลปกรรมศาสตร์</option>
                                <option value="26">เศรษฐศาสตร์</option>
                                <option value="21">สถาปนิก/มัณฑนากร</option>
                                <option value="22">สังคมสงเคราะห์</option>
                                <option value="23">อาหารและเครื่องดื่ม</option>
                                <option value="29">อื่น ๆ</option>
                                <option value="28">ไอที/คอมพิวเตอร์</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-sm-4"><label>ตำแหน่งสุดท้าย</label></div>
                    <div class="col-sm-8"><input type="text" class="textbox w100-m" name="txbLastPosition" id="txbLastPosition" maxlength="30" style="width:240px" value=""></div>
                </div>
                <div class="col-md-6 form-row">
                    <div class="col-sm-4"><label>เงินเดือนสุดท้าย</label></div>
                    <div class="col-sm-8"><input type="text" class="textbox w80-m" name="txbLastSalary" id="txbLastSalary" maxlength="7" style="width:100px" onkeypress="return decimalonly(event)" value=""> &nbsp; บาท</div>
                </div>
            </div>
            <b>ประวัติการทำงาน</b>
            <div class="col-sm-12 wrap-tab-btn">
                <div class="row">
                    <div id="headTab1" class="col-20 tab-btn" style="background-color:#E0E0E0;"><a href="javascript:OpenTab('headTab1','tab1');">การทำงาน 1</a></div>
                    <div id="headTab2" class="col-20 tab-btn"><a href="javascript:OpenTab('headTab2','tab2');">การทำงาน 2</a></div>
                    <div id="headTab3" class="col-20 tab-btn"><a href="javascript:OpenTab('headTab3','tab3');">การทำงาน 3</a></div>
                    <div id="headTab4" class="col-20 tab-btn"><a href="javascript:OpenTab('headTab4','tab4');">การทำงาน 4</a></div>
                    <div id="headTab5" class="col-20 tab-btn"><a href="javascript:OpenTab('headTab5','tab5');">การทำงาน 5</a></div>
                </div>
            </div>
            <div class="col-sm-12">

                <div id="tab1" style=" border-bottom:none">

                    <div class="row">
                        <div class="col-md-2">
                            <label>1. ระยะเวลา</label>
                        </div>
                        <div class="col-md-10 form-row">
                            <div class="col-md-1 col-sm-2 mb10-m flabel">ตั้งแต่ </div>
                            <div class="col-md-4 col-sm-10 mb10-m">
                                <div class="select-block block-2-col">
                                    <select name="cmbWorkFromMonth1" id="cmbWorkFromMonth1">
                                        <option value=""></option>
                                        <option value="1">มกราคม</option>
                                        <option value="2">กุมภาพันธ์</option>
                                        <option value="3">มีนาคม</option>
                                        <option value="4">เมษายน</option>
                                        <option value="5">พฤษภาคม</option>
                                        <option value="6">มิถุนายน</option>
                                        <option value="7">กรกฎาคม</option>
                                        <option value="8">สิงหาคม</option>
                                        <option value="9">กันยายน</option>
                                        <option value="10">ตุลาคม</option>
                                        <option value="11">พฤศจิกายน</option>
                                        <option value="12">ธันวาคม</option>
                                    </select>
                                </div>
                                <div class="select-block block-2-col">
                                    <select name="cmbWorkFromYear1" id="cmbWorkFromYear1">
                                        <option value=""></option>
                                        <option value="2510">2510</option>
                                        <option value="2511">2511</option>
                                        <option value="2512">2512</option>
                                        <option value="2513">2513</option>
                                        <option value="2514">2514</option>
                                        <option value="2515">2515</option>
                                        <option value="2516">2516</option>
                                        <option value="2517">2517</option>
                                        <option value="2518">2518</option>
                                        <option value="2519">2519</option>
                                        <option value="2520">2520</option>
                                        <option value="2521">2521</option>
                                        <option value="2522">2522</option>
                                        <option value="2523">2523</option>
                                        <option value="2524">2524</option>
                                        <option value="2525">2525</option>
                                        <option value="2526">2526</option>
                                        <option value="2527">2527</option>
                                        <option value="2528">2528</option>
                                        <option value="2529">2529</option>
                                        <option value="2530">2530</option>
                                        <option value="2531">2531</option>
                                        <option value="2532">2532</option>
                                        <option value="2533">2533</option>
                                        <option value="2534">2534</option>
                                        <option value="2535">2535</option>
                                        <option value="2536">2536</option>
                                        <option value="2537">2537</option>
                                        <option value="2538">2538</option>
                                        <option value="2539">2539</option>
                                        <option value="2540">2540</option>
                                        <option value="2541">2541</option>
                                        <option value="2542">2542</option>
                                        <option value="2543">2543</option>
                                        <option value="2544">2544</option>
                                        <option value="2545">2545</option>
                                        <option value="2546">2546</option>
                                        <option value="2547">2547</option>
                                        <option value="2548">2548</option>
                                        <option value="2549">2549</option>
                                        <option value="2550">2550</option>
                                        <option value="2551">2551</option>
                                        <option value="2552">2552</option>
                                        <option value="2553">2553</option>
                                        <option value="2554">2554</option>
                                        <option value="2555">2555</option>
                                        <option value="2556">2556</option>
                                        <option value="2557">2557</option>
                                        <option value="2558">2558</option>
                                        <option value="2559">2559</option>
                                        <option value="2560">2560</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-2 mb10-m flabel">ถึง </div>
                            <div class="col-md-6 col-sm-10 mb10-m">
                                <div class="select-block block-2-col">
                                    <select name="cmbWorkToMonth1" id="cmbWorkToMonth1">
                                        <option value=""></option>
                                        <option value="1">มกราคม</option>
                                        <option value="2">กุมภาพันธ์</option>
                                        <option value="3">มีนาคม</option>
                                        <option value="4">เมษายน</option>
                                        <option value="5">พฤษภาคม</option>
                                        <option value="6">มิถุนายน</option>
                                        <option value="7">กรกฎาคม</option>
                                        <option value="8">สิงหาคม</option>
                                        <option value="9">กันยายน</option>
                                        <option value="10">ตุลาคม</option>
                                        <option value="11">พฤศจิกายน</option>
                                        <option value="12">ธันวาคม</option>
                                    </select>
                                </div>
                                <div class="select-block block-2-col">
                                    <select name="cmbWorkToYear1" id="cmbWorkToYear1">
                                        <option value=""></option>
                                        <option value="2510">2510</option>
                                        <option value="2511">2511</option>
                                        <option value="2512">2512</option>
                                        <option value="2513">2513</option>
                                        <option value="2514">2514</option>
                                        <option value="2515">2515</option>
                                        <option value="2516">2516</option>
                                        <option value="2517">2517</option>
                                        <option value="2518">2518</option>
                                        <option value="2519">2519</option>
                                        <option value="2520">2520</option>
                                        <option value="2521">2521</option>
                                        <option value="2522">2522</option>
                                        <option value="2523">2523</option>
                                        <option value="2524">2524</option>
                                        <option value="2525">2525</option>
                                        <option value="2526">2526</option>
                                        <option value="2527">2527</option>
                                        <option value="2528">2528</option>
                                        <option value="2529">2529</option>
                                        <option value="2530">2530</option>
                                        <option value="2531">2531</option>
                                        <option value="2532">2532</option>
                                        <option value="2533">2533</option>
                                        <option value="2534">2534</option>
                                        <option value="2535">2535</option>
                                        <option value="2536">2536</option>
                                        <option value="2537">2537</option>
                                        <option value="2538">2538</option>
                                        <option value="2539">2539</option>
                                        <option value="2540">2540</option>
                                        <option value="2541">2541</option>
                                        <option value="2542">2542</option>
                                        <option value="2543">2543</option>
                                        <option value="2544">2544</option>
                                        <option value="2545">2545</option>
                                        <option value="2546">2546</option>
                                        <option value="2547">2547</option>
                                        <option value="2548">2548</option>
                                        <option value="2549">2549</option>
                                        <option value="2550">2550</option>
                                        <option value="2551">2551</option>
                                        <option value="2552">2552</option>
                                        <option value="2553">2553</option>
                                        <option value="2554">2554</option>
                                        <option value="2555">2555</option>
                                        <option value="2556">2556</option>
                                        <option value="2557">2557</option>
                                        <option value="2558">2558</option>
                                        <option value="2559">2559</option>
                                        <option value="2560">2560</option>
                                    </select>
                                </div>
                            </div>
                            <div class="">
                                <div class="col-md-10 form-row">
                                    <div class="">
                                        <div class="col-md-1 col-sm-2 mb10-m flabel">หรือ</div>
                                        <div class="col-md-5 col-sm-10">
                                            <input type="checkbox" value="Y" name="chbWorkEndPresent1" id="chbWorkEndPresent1" onclick="javascript:ClickWorkEndPresent(document.frm1.chbWorkEndPresent1, 'cmbWorkToMonth1', 'cmbWorkToYear1');" />
                                            <label for="chbWorkEndPresent1"><span></span>ถึงปัจจุบัน,</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>ตำแหน่ง</label></div>
                                <div class="col-md-8 col-sm-9">
                                    <div style="border-bottom:none;"><input type="text" class="textbox" name="txbWorkPosition1" id="txbWorkPosition1" maxlength="50" value=""></div>
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>บริษัท</label></div>
                                <div class="col-md-8 col-sm-9"><input type="text" class="textbox" name="txbWorkCompany1" id="txbWorkCompany1" maxlength="50" value=""></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>เงินเดือน</label></div>
                                <div class="col-md-8 col-sm-9"><input type="text" class="textbox w80-m" name="txbWorkSalary1" id="txbWorkSalary1" maxlength="7" style="width:100px" onkeypress="return decimalonly(event)" value=""> &nbsp; บาท</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>รายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-9"><input type="text" class="textbox w80-m" name="txbWorkIncome1" id="txbWorkIncome1" maxlength="7" style="width:100px" onkeypress="return decimalonly(event)" value=""> &nbsp; บาท</div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-4"><label>ระบุรายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-8"><input type="text" class="textbox w100-m" name="txbWorkIncomeOther1" id="txbWorkIncomeOther1" maxlength="100" value=""></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-row">
                                <div class="col-md-2 col-sm-4"><label>ลักษณะงานที่ทำ</label></div>
                                <div class="col-md-10 col-sm-8"><textarea class="textbox" name="txbWorkDetail1" id="txbWorkDetail1" style="height:70px;width:100%;"></textarea></div>
                            </div>
                        </div>
                    </div>

                    <div id="tab2" style="display:none; border-bottom:none">

                        <div class="row">
                            <div class="col-md-2">
                                <label>2. ระยะเวลา</label>
                            </div>
                            <div class="col-md-10 form-row">
                                <div class="col-md-1 col-sm-2 mb10-m">ตั้งแต่ </div>
                                <div class="col-md-4 col-sm-10 mb10-m">
                                    <div class="select-block">
                                        <select name="cmbWorkFromMonth2" id="cmbWorkFromMonth2">
                                            <option value=""></option>
                                            <option value="1">มกราคม</option>
                                            <option value="2">กุมภาพันธ์</option>
                                            <option value="3">มีนาคม</option>
                                            <option value="4">เมษายน</option>
                                            <option value="5">พฤษภาคม</option>
                                            <option value="6">มิถุนายน</option>
                                            <option value="7">กรกฎาคม</option>
                                            <option value="8">สิงหาคม</option>
                                            <option value="9">กันยายน</option>
                                            <option value="10">ตุลาคม</option>
                                            <option value="11">พฤศจิกายน</option>
                                            <option value="12">ธันวาคม</option>
                                        </select>
                                    </div>
                                    <div class="select-block">
                                        <select name="cmbWorkFromYear2" id="cmbWorkFromYear2">
                                            <option value=""></option>
                                            <option value="2510">2510</option>
                                            <option value="2511">2511</option>
                                            <option value="2512">2512</option>
                                            <option value="2513">2513</option>
                                            <option value="2514">2514</option>
                                            <option value="2515">2515</option>
                                            <option value="2516">2516</option>
                                            <option value="2517">2517</option>
                                            <option value="2518">2518</option>
                                            <option value="2519">2519</option>
                                            <option value="2520">2520</option>
                                            <option value="2521">2521</option>
                                            <option value="2522">2522</option>
                                            <option value="2523">2523</option>
                                            <option value="2524">2524</option>
                                            <option value="2525">2525</option>
                                            <option value="2526">2526</option>
                                            <option value="2527">2527</option>
                                            <option value="2528">2528</option>
                                            <option value="2529">2529</option>
                                            <option value="2530">2530</option>
                                            <option value="2531">2531</option>
                                            <option value="2532">2532</option>
                                            <option value="2533">2533</option>
                                            <option value="2534">2534</option>
                                            <option value="2535">2535</option>
                                            <option value="2536">2536</option>
                                            <option value="2537">2537</option>
                                            <option value="2538">2538</option>
                                            <option value="2539">2539</option>
                                            <option value="2540">2540</option>
                                            <option value="2541">2541</option>
                                            <option value="2542">2542</option>
                                            <option value="2543">2543</option>
                                            <option value="2544">2544</option>
                                            <option value="2545">2545</option>
                                            <option value="2546">2546</option>
                                            <option value="2547">2547</option>
                                            <option value="2548">2548</option>
                                            <option value="2549">2549</option>
                                            <option value="2550">2550</option>
                                            <option value="2551">2551</option>
                                            <option value="2552">2552</option>
                                            <option value="2553">2553</option>
                                            <option value="2554">2554</option>
                                            <option value="2555">2555</option>
                                            <option value="2556">2556</option>
                                            <option value="2557">2557</option>
                                            <option value="2558">2558</option>
                                            <option value="2559">2559</option>
                                            <option value="2560">2560</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-1 col-sm-2 mb10-m">ถึง </div>
                                <div class="col-md-6 col-sm-10 mb10-m">
                                    <div class="select-block">
                                        <select name="cmbWorkToMonth2" id="cmbWorkToMonth2">
                                            <option value=""></option>
                                            <option value="1">มกราคม</option>
                                            <option value="2">กุมภาพันธ์</option>
                                            <option value="3">มีนาคม</option>
                                            <option value="4">เมษายน</option>
                                            <option value="5">พฤษภาคม</option>
                                            <option value="6">มิถุนายน</option>
                                            <option value="7">กรกฎาคม</option>
                                            <option value="8">สิงหาคม</option>
                                            <option value="9">กันยายน</option>
                                            <option value="10">ตุลาคม</option>
                                            <option value="11">พฤศจิกายน</option>
                                            <option value="12">ธันวาคม</option>
                                        </select>
                                    </div>
                                    <div class="select-block">
                                        <select name="cmbWorkToYear2" id="cmbWorkToYear2">
                                            <option value=""></option>
                                            <option value="2510">2510</option>
                                            <option value="2511">2511</option>
                                            <option value="2512">2512</option>
                                            <option value="2513">2513</option>
                                            <option value="2514">2514</option>
                                            <option value="2515">2515</option>
                                            <option value="2516">2516</option>
                                            <option value="2517">2517</option>
                                            <option value="2518">2518</option>
                                            <option value="2519">2519</option>
                                            <option value="2520">2520</option>
                                            <option value="2521">2521</option>
                                            <option value="2522">2522</option>
                                            <option value="2523">2523</option>
                                            <option value="2524">2524</option>
                                            <option value="2525">2525</option>
                                            <option value="2526">2526</option>
                                            <option value="2527">2527</option>
                                            <option value="2528">2528</option>
                                            <option value="2529">2529</option>
                                            <option value="2530">2530</option>
                                            <option value="2531">2531</option>
                                            <option value="2532">2532</option>
                                            <option value="2533">2533</option>
                                            <option value="2534">2534</option>
                                            <option value="2535">2535</option>
                                            <option value="2536">2536</option>
                                            <option value="2537">2537</option>
                                            <option value="2538">2538</option>
                                            <option value="2539">2539</option>
                                            <option value="2540">2540</option>
                                            <option value="2541">2541</option>
                                            <option value="2542">2542</option>
                                            <option value="2543">2543</option>
                                            <option value="2544">2544</option>
                                            <option value="2545">2545</option>
                                            <option value="2546">2546</option>
                                            <option value="2547">2547</option>
                                            <option value="2548">2548</option>
                                            <option value="2549">2549</option>
                                            <option value="2550">2550</option>
                                            <option value="2551">2551</option>
                                            <option value="2552">2552</option>
                                            <option value="2553">2553</option>
                                            <option value="2554">2554</option>
                                            <option value="2555">2555</option>
                                            <option value="2556">2556</option>
                                            <option value="2557">2557</option>
                                            <option value="2558">2558</option>
                                            <option value="2559">2559</option>
                                            <option value="2560">2560</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-1 col-sm-2">หรือ</div>
                                <div class="col-md-2 col-sm-10"><input type="checkbox" value="Y" name="chbWorkEndPresent2" id="chbWorkEndPresent2" onclick="javascript:ClickWorkEndPresent(document.frm1.chbWorkEndPresent2, 'cmbWorkToMonth2', 'cmbWorkToYear2');"> ถึงปัจจุบัน,</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>ตำแหน่ง</label></div>
                                <div class="col-md-8 col-sm-9">
                                    <div style="border-bottom:none;"><input type="text" class="textbox" name="txbWorkPosition2" id="txbWorkPosition2" maxlength="50" value=""></div>
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>บริษัท</label></div>
                                <div class="col-md-8 col-sm-9"><input type="text" class="textbox" name="txbWorkCompany2" id="txbWorkCompany2" maxlength="50" value=""></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>เงินเดือน</label></div>
                                <div class="col-md-8 col-sm-9"><input type="text" class="textbox w80-m" name="txbWorkSalary2" id="txbWorkSalary2" maxlength="7" style="width:100px" onkeypress="return decimalonly(event)" value=""> &nbsp; บาท</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>รายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-9"><input type="text" class="textbox w80-m" name="txbWorkIncome2" id="txbWorkIncome2" maxlength="7" style="width:100px" onkeypress="return decimalonly(event)" value=""> &nbsp; บาท</div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-4"><label>ระบุรายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-8"><input type="text" class="textbox w100-m" name="txbWorkIncomeOther2" id="txbWorkIncomeOther2" maxlength="100" value=""></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-row">
                                <div class="col-md-2 col-sm-4"><label>ลักษณะงานที่ทำ</label></div>
                                <div class="col-md-10 col-sm-8"><textarea class="textbox" name="txbWorkDetail2" id="txbWorkDetail2" style="height:70px"></textarea></div>
                            </div>
                        </div>
                    </div>

                    <div id="tab3" style="display:none; border-bottom:none">

                        <div class="row">
                            <div class="col-md-2">
                                <label>3. ระยะเวลา</label>
                            </div>
                            <div class="col-md-10 form-row">
                                <div class="col-md-1 col-sm-2 mb10-m">ตั้งแต่ </div>
                                <div class="col-md-4 col-sm-10 mb10-m">
                                    <div class="select-block">
                                        <select name="cmbWorkFromMonth3" id="cmbWorkFromMonth3">
                                            <option value=""></option>
                                            <option value="1">มกราคม</option>
                                            <option value="2">กุมภาพันธ์</option>
                                            <option value="3">มีนาคม</option>
                                            <option value="4">เมษายน</option>
                                            <option value="5">พฤษภาคม</option>
                                            <option value="6">มิถุนายน</option>
                                            <option value="7">กรกฎาคม</option>
                                            <option value="8">สิงหาคม</option>
                                            <option value="9">กันยายน</option>
                                            <option value="10">ตุลาคม</option>
                                            <option value="11">พฤศจิกายน</option>
                                            <option value="12">ธันวาคม</option>
                                        </select>
                                    </div>
                                    <div class="select-block">
                                        <select name="cmbWorkFromYear3" id="cmbWorkFromYear3">
                                            <option value=""></option>
                                            <option value="2510">2510</option>
                                            <option value="2511">2511</option>
                                            <option value="2512">2512</option>
                                            <option value="2513">2513</option>
                                            <option value="2514">2514</option>
                                            <option value="2515">2515</option>
                                            <option value="2516">2516</option>
                                            <option value="2517">2517</option>
                                            <option value="2518">2518</option>
                                            <option value="2519">2519</option>
                                            <option value="2520">2520</option>
                                            <option value="2521">2521</option>
                                            <option value="2522">2522</option>
                                            <option value="2523">2523</option>
                                            <option value="2524">2524</option>
                                            <option value="2525">2525</option>
                                            <option value="2526">2526</option>
                                            <option value="2527">2527</option>
                                            <option value="2528">2528</option>
                                            <option value="2529">2529</option>
                                            <option value="2530">2530</option>
                                            <option value="2531">2531</option>
                                            <option value="2532">2532</option>
                                            <option value="2533">2533</option>
                                            <option value="2534">2534</option>
                                            <option value="2535">2535</option>
                                            <option value="2536">2536</option>
                                            <option value="2537">2537</option>
                                            <option value="2538">2538</option>
                                            <option value="2539">2539</option>
                                            <option value="2540">2540</option>
                                            <option value="2541">2541</option>
                                            <option value="2542">2542</option>
                                            <option value="2543">2543</option>
                                            <option value="2544">2544</option>
                                            <option value="2545">2545</option>
                                            <option value="2546">2546</option>
                                            <option value="2547">2547</option>
                                            <option value="2548">2548</option>
                                            <option value="2549">2549</option>
                                            <option value="2550">2550</option>
                                            <option value="2551">2551</option>
                                            <option value="2552">2552</option>
                                            <option value="2553">2553</option>
                                            <option value="2554">2554</option>
                                            <option value="2555">2555</option>
                                            <option value="2556">2556</option>
                                            <option value="2557">2557</option>
                                            <option value="2558">2558</option>
                                            <option value="2559">2559</option>
                                            <option value="2560">2560</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-1 col-sm-2 mb10-m">ถึง </div>
                                <div class="col-md-6 col-sm-10 mb10-m">
                                    <div class="select-block">
                                        <select name="cmbWorkToMonth3" id="cmbWorkToMonth3">
                                            <option value=""></option>
                                            <option value="1">มกราคม</option>
                                            <option value="2">กุมภาพันธ์</option>
                                            <option value="3">มีนาคม</option>
                                            <option value="4">เมษายน</option>
                                            <option value="5">พฤษภาคม</option>
                                            <option value="6">มิถุนายน</option>
                                            <option value="7">กรกฎาคม</option>
                                            <option value="8">สิงหาคม</option>
                                            <option value="9">กันยายน</option>
                                            <option value="10">ตุลาคม</option>
                                            <option value="11">พฤศจิกายน</option>
                                            <option value="12">ธันวาคม</option>
                                        </select>
                                    </div>
                                    <div class="select-block">
                                        <select name="cmbWorkToYear3" id="cmbWorkToYear3">
                                            <option value=""></option>
                                            <option value="2510">2510</option>
                                            <option value="2511">2511</option>
                                            <option value="2512">2512</option>
                                            <option value="2513">2513</option>
                                            <option value="2514">2514</option>
                                            <option value="2515">2515</option>
                                            <option value="2516">2516</option>
                                            <option value="2517">2517</option>
                                            <option value="2518">2518</option>
                                            <option value="2519">2519</option>
                                            <option value="2520">2520</option>
                                            <option value="2521">2521</option>
                                            <option value="2522">2522</option>
                                            <option value="2523">2523</option>
                                            <option value="2524">2524</option>
                                            <option value="2525">2525</option>
                                            <option value="2526">2526</option>
                                            <option value="2527">2527</option>
                                            <option value="2528">2528</option>
                                            <option value="2529">2529</option>
                                            <option value="2530">2530</option>
                                            <option value="2531">2531</option>
                                            <option value="2532">2532</option>
                                            <option value="2533">2533</option>
                                            <option value="2534">2534</option>
                                            <option value="2535">2535</option>
                                            <option value="2536">2536</option>
                                            <option value="2537">2537</option>
                                            <option value="2538">2538</option>
                                            <option value="2539">2539</option>
                                            <option value="2540">2540</option>
                                            <option value="2541">2541</option>
                                            <option value="2542">2542</option>
                                            <option value="2543">2543</option>
                                            <option value="2544">2544</option>
                                            <option value="2545">2545</option>
                                            <option value="2546">2546</option>
                                            <option value="2547">2547</option>
                                            <option value="2548">2548</option>
                                            <option value="2549">2549</option>
                                            <option value="2550">2550</option>
                                            <option value="2551">2551</option>
                                            <option value="2552">2552</option>
                                            <option value="2553">2553</option>
                                            <option value="2554">2554</option>
                                            <option value="2555">2555</option>
                                            <option value="2556">2556</option>
                                            <option value="2557">2557</option>
                                            <option value="2558">2558</option>
                                            <option value="2559">2559</option>
                                            <option value="2560">2560</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-1 col-sm-2">หรือ</div>
                                <div class="col-md-2 col-sm-10"><input type="checkbox" value="Y" name="chbWorkEndPresent3" id="chbWorkEndPresent3" onclick="javascript:ClickWorkEndPresent(document.frm1.chbWorkEndPresent3, 'cmbWorkToMonth3', 'cmbWorkToYear3');"> ถึงปัจจุบัน,</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>ตำแหน่ง</label></div>
                                <div class="col-md-8 col-sm-9">
                                    <div style="border-bottom:none;"><input type="text" class="textbox" name="txbWorkPosition3" id="txbWorkPosition3" maxlength="50" value=""></div>
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>บริษัท</label></div>
                                <div class="col-md-8 col-sm-9"><input type="text" class="textbox" name="txbWorkCompany3" id="txbWorkCompany3" maxlength="50" value=""></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>เงินเดือน</label></div>
                                <div class="col-md-8 col-sm-9"><input type="text" class="textbox w80-m" name="txbWorkSalary3" id="txbWorkSalary3" maxlength="7" style="width:100px" onkeypress="return decimalonly(event)" value=""> &nbsp; บาท</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>รายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-9"><input type="text" class="textbox w80-m" name="txbWorkIncome3" id="txbWorkIncome3" maxlength="7" style="width:100px" onkeypress="return decimalonly(event)" value=""> &nbsp; บาท</div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-4"><label>ระบุรายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-8"><input type="text" class="textbox w100-m" name="txbWorkIncomeOther3" id="txbWorkIncomeOther3" maxlength="100" value=""></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-row">
                                <div class="col-md-2 col-sm-4"><label>ลักษณะงานที่ทำ</label></div>
                                <div class="col-md-10 col-sm-8"><textarea class="textbox" name="txbWorkDetail3" id="txbWorkDetail3" style="height:70px"></textarea></div>
                            </div>
                        </div>
                    </div>

                    <div id="tab4" style="display:none; border-bottom:none">

                        <div class="row">
                            <div class="col-md-2">
                                <label>4. ระยะเวลา</label>
                            </div>
                            <div class="col-md-10 form-row">
                                <div class="col-md-1 col-sm-2 mb10-m">ตั้งแต่ </div>
                                <div class="col-md-4 col-sm-10 mb10-m">
                                    <div class="select-block">
                                        <select name="cmbWorkFromMonth4" id="cmbWorkFromMonth4">
                                            <option value=""></option>
                                            <option value="1">มกราคม</option>
                                            <option value="2">กุมภาพันธ์</option>
                                            <option value="3">มีนาคม</option>
                                            <option value="4">เมษายน</option>
                                            <option value="5">พฤษภาคม</option>
                                            <option value="6">มิถุนายน</option>
                                            <option value="7">กรกฎาคม</option>
                                            <option value="8">สิงหาคม</option>
                                            <option value="9">กันยายน</option>
                                            <option value="10">ตุลาคม</option>
                                            <option value="11">พฤศจิกายน</option>
                                            <option value="12">ธันวาคม</option>
                                        </select>
                                    </div>
                                    <div class="select-block">
                                        <select name="cmbWorkFromYear4" id="cmbWorkFromYear4">
                                            <option value=""></option>
                                            <option value="2510">2510</option>
                                            <option value="2511">2511</option>
                                            <option value="2512">2512</option>
                                            <option value="2513">2513</option>
                                            <option value="2514">2514</option>
                                            <option value="2515">2515</option>
                                            <option value="2516">2516</option>
                                            <option value="2517">2517</option>
                                            <option value="2518">2518</option>
                                            <option value="2519">2519</option>
                                            <option value="2520">2520</option>
                                            <option value="2521">2521</option>
                                            <option value="2522">2522</option>
                                            <option value="2523">2523</option>
                                            <option value="2524">2524</option>
                                            <option value="2525">2525</option>
                                            <option value="2526">2526</option>
                                            <option value="2527">2527</option>
                                            <option value="2528">2528</option>
                                            <option value="2529">2529</option>
                                            <option value="2530">2530</option>
                                            <option value="2531">2531</option>
                                            <option value="2532">2532</option>
                                            <option value="2533">2533</option>
                                            <option value="2534">2534</option>
                                            <option value="2535">2535</option>
                                            <option value="2536">2536</option>
                                            <option value="2537">2537</option>
                                            <option value="2538">2538</option>
                                            <option value="2539">2539</option>
                                            <option value="2540">2540</option>
                                            <option value="2541">2541</option>
                                            <option value="2542">2542</option>
                                            <option value="2543">2543</option>
                                            <option value="2544">2544</option>
                                            <option value="2545">2545</option>
                                            <option value="2546">2546</option>
                                            <option value="2547">2547</option>
                                            <option value="2548">2548</option>
                                            <option value="2549">2549</option>
                                            <option value="2550">2550</option>
                                            <option value="2551">2551</option>
                                            <option value="2552">2552</option>
                                            <option value="2553">2553</option>
                                            <option value="2554">2554</option>
                                            <option value="2555">2555</option>
                                            <option value="2556">2556</option>
                                            <option value="2557">2557</option>
                                            <option value="2558">2558</option>
                                            <option value="2559">2559</option>
                                            <option value="2560">2560</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-1 col-sm-2 mb10-m">ถึง </div>
                                <div class="col-md-6 col-sm-10 mb10-m">
                                    <div class="select-block">
                                        <select name="cmbWorkToMonth4" id="cmbWorkToMonth4">
                                            <option value=""></option>
                                            <option value="1">มกราคม</option>
                                            <option value="2">กุมภาพันธ์</option>
                                            <option value="3">มีนาคม</option>
                                            <option value="4">เมษายน</option>
                                            <option value="5">พฤษภาคม</option>
                                            <option value="6">มิถุนายน</option>
                                            <option value="7">กรกฎาคม</option>
                                            <option value="8">สิงหาคม</option>
                                            <option value="9">กันยายน</option>
                                            <option value="10">ตุลาคม</option>
                                            <option value="11">พฤศจิกายน</option>
                                            <option value="12">ธันวาคม</option>
                                        </select>
                                    </div>
                                    <div class="select-block">
                                        <select name="cmbWorkToYear4" id="cmbWorkToYear4">
                                            <option value=""></option>
                                            <option value="2510">2510</option>
                                            <option value="2511">2511</option>
                                            <option value="2512">2512</option>
                                            <option value="2513">2513</option>
                                            <option value="2514">2514</option>
                                            <option value="2515">2515</option>
                                            <option value="2516">2516</option>
                                            <option value="2517">2517</option>
                                            <option value="2518">2518</option>
                                            <option value="2519">2519</option>
                                            <option value="2520">2520</option>
                                            <option value="2521">2521</option>
                                            <option value="2522">2522</option>
                                            <option value="2523">2523</option>
                                            <option value="2524">2524</option>
                                            <option value="2525">2525</option>
                                            <option value="2526">2526</option>
                                            <option value="2527">2527</option>
                                            <option value="2528">2528</option>
                                            <option value="2529">2529</option>
                                            <option value="2530">2530</option>
                                            <option value="2531">2531</option>
                                            <option value="2532">2532</option>
                                            <option value="2533">2533</option>
                                            <option value="2534">2534</option>
                                            <option value="2535">2535</option>
                                            <option value="2536">2536</option>
                                            <option value="2537">2537</option>
                                            <option value="2538">2538</option>
                                            <option value="2539">2539</option>
                                            <option value="2540">2540</option>
                                            <option value="2541">2541</option>
                                            <option value="2542">2542</option>
                                            <option value="2543">2543</option>
                                            <option value="2544">2544</option>
                                            <option value="2545">2545</option>
                                            <option value="2546">2546</option>
                                            <option value="2547">2547</option>
                                            <option value="2548">2548</option>
                                            <option value="2549">2549</option>
                                            <option value="2550">2550</option>
                                            <option value="2551">2551</option>
                                            <option value="2552">2552</option>
                                            <option value="2553">2553</option>
                                            <option value="2554">2554</option>
                                            <option value="2555">2555</option>
                                            <option value="2556">2556</option>
                                            <option value="2557">2557</option>
                                            <option value="2558">2558</option>
                                            <option value="2559">2559</option>
                                            <option value="2560">2560</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-1 col-sm-2">หรือ</div>
                                <div class="col-md-2 col-sm-10"><input type="checkbox" value="Y" name="chbWorkEndPresent4" id="chbWorkEndPresent4" onclick="javascript:ClickWorkEndPresent(document.frm1.chbWorkEndPresent4, 'cmbWorkToMonth4', 'cmbWorkToYear4');"> ถึงปัจจุบัน,</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>ตำแหน่ง</label></div>
                                <div class="col-md-8 col-sm-9">
                                    <div style="border-bottom:none;"><input type="text" class="textbox" name="txbWorkPosition4" id="txbWorkPosition4" maxlength="50" value=""></div>
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>บริษัท</label></div>
                                <div class="col-md-8 col-sm-9"><input type="text" class="textbox" name="txbWorkCompany4" id="txbWorkCompany4" maxlength="50" value=""></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>เงินเดือน</label></div>
                                <div class="col-md-8 col-sm-9"><input type="text" class="textbox w80-m" name="txbWorkSalary4" id="txbWorkSalary4" maxlength="7" style="width:100px" onkeypress="return decimalonly(event)" value=""> &nbsp; บาท</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>รายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-9"><input type="text" class="textbox w80-m" name="txbWorkIncome4" id="txbWorkIncome4" maxlength="7" style="width:100px" onkeypress="return decimalonly(event)" value=""> &nbsp; บาท</div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-4"><label>ระบุรายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-8"><input type="text" class="textbox w100-m" name="txbWorkIncomeOther4" id="txbWorkIncomeOther4" maxlength="100" value=""></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-row">
                                <div class="col-md-2 col-sm-4"><label>ลักษณะงานที่ทำ</label></div>
                                <div class="col-md-10 col-sm-8"><textarea class="textbox" name="txbWorkDetail4" id="txbWorkDetail4" style="height:70px"></textarea></div>
                            </div>
                        </div>
                    </div>

                    <div id="tab5" style="display:none; border-bottom:none">

                        <div class="row">
                            <div class="col-md-2">
                                <label>5. ระยะเวลา</label>
                            </div>
                            <div class="col-md-10 form-row">
                                <div class="col-md-1 col-sm-2 mb10-m">ตั้งแต่ </div>
                                <div class="col-md-4 col-sm-10 mb10-m">
                                    <div class="select-block">
                                        <select name="cmbWorkFromMonth5" id="cmbWorkFromMonth5">
                                            <option value=""></option>
                                            <option value="1">มกราคม</option>
                                            <option value="2">กุมภาพันธ์</option>
                                            <option value="3">มีนาคม</option>
                                            <option value="4">เมษายน</option>
                                            <option value="5">พฤษภาคม</option>
                                            <option value="6">มิถุนายน</option>
                                            <option value="7">กรกฎาคม</option>
                                            <option value="8">สิงหาคม</option>
                                            <option value="9">กันยายน</option>
                                            <option value="10">ตุลาคม</option>
                                            <option value="11">พฤศจิกายน</option>
                                            <option value="12">ธันวาคม</option>
                                        </select>
                                    </div>
                                    <div class="select-block">
                                        <select name="cmbWorkFromYear5" id="cmbWorkFromYear5">
                                            <option value=""></option>
                                            <option value="2510">2510</option>
                                            <option value="2511">2511</option>
                                            <option value="2512">2512</option>
                                            <option value="2513">2513</option>
                                            <option value="2514">2514</option>
                                            <option value="2515">2515</option>
                                            <option value="2516">2516</option>
                                            <option value="2517">2517</option>
                                            <option value="2518">2518</option>
                                            <option value="2519">2519</option>
                                            <option value="2520">2520</option>
                                            <option value="2521">2521</option>
                                            <option value="2522">2522</option>
                                            <option value="2523">2523</option>
                                            <option value="2524">2524</option>
                                            <option value="2525">2525</option>
                                            <option value="2526">2526</option>
                                            <option value="2527">2527</option>
                                            <option value="2528">2528</option>
                                            <option value="2529">2529</option>
                                            <option value="2530">2530</option>
                                            <option value="2531">2531</option>
                                            <option value="2532">2532</option>
                                            <option value="2533">2533</option>
                                            <option value="2534">2534</option>
                                            <option value="2535">2535</option>
                                            <option value="2536">2536</option>
                                            <option value="2537">2537</option>
                                            <option value="2538">2538</option>
                                            <option value="2539">2539</option>
                                            <option value="2540">2540</option>
                                            <option value="2541">2541</option>
                                            <option value="2542">2542</option>
                                            <option value="2543">2543</option>
                                            <option value="2544">2544</option>
                                            <option value="2545">2545</option>
                                            <option value="2546">2546</option>
                                            <option value="2547">2547</option>
                                            <option value="2548">2548</option>
                                            <option value="2549">2549</option>
                                            <option value="2550">2550</option>
                                            <option value="2551">2551</option>
                                            <option value="2552">2552</option>
                                            <option value="2553">2553</option>
                                            <option value="2554">2554</option>
                                            <option value="2555">2555</option>
                                            <option value="2556">2556</option>
                                            <option value="2557">2557</option>
                                            <option value="2558">2558</option>
                                            <option value="2559">2559</option>
                                            <option value="2560">2560</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-1 col-sm-2 mb10-m">ถึง </div>
                                <div class="col-md-6 col-sm-10 mb10-m">
                                    <div class="select-block">
                                        <select name="cmbWorkToMonth5" id="cmbWorkToMonth5">
                                            <option value=""></option>
                                            <option value="1">มกราคม</option>
                                            <option value="2">กุมภาพันธ์</option>
                                            <option value="3">มีนาคม</option>
                                            <option value="4">เมษายน</option>
                                            <option value="5">พฤษภาคม</option>
                                            <option value="6">มิถุนายน</option>
                                            <option value="7">กรกฎาคม</option>
                                            <option value="8">สิงหาคม</option>
                                            <option value="9">กันยายน</option>
                                            <option value="10">ตุลาคม</option>
                                            <option value="11">พฤศจิกายน</option>
                                            <option value="12">ธันวาคม</option>
                                        </select>
                                    </div>
                                    <div class="select-block">
                                        <select name="cmbWorkToYear5" id="cmbWorkToYear5">
                                            <option value=""></option>
                                            <option value="2510">2510</option>
                                            <option value="2511">2511</option>
                                            <option value="2512">2512</option>
                                            <option value="2513">2513</option>
                                            <option value="2514">2514</option>
                                            <option value="2515">2515</option>
                                            <option value="2516">2516</option>
                                            <option value="2517">2517</option>
                                            <option value="2518">2518</option>
                                            <option value="2519">2519</option>
                                            <option value="2520">2520</option>
                                            <option value="2521">2521</option>
                                            <option value="2522">2522</option>
                                            <option value="2523">2523</option>
                                            <option value="2524">2524</option>
                                            <option value="2525">2525</option>
                                            <option value="2526">2526</option>
                                            <option value="2527">2527</option>
                                            <option value="2528">2528</option>
                                            <option value="2529">2529</option>
                                            <option value="2530">2530</option>
                                            <option value="2531">2531</option>
                                            <option value="2532">2532</option>
                                            <option value="2533">2533</option>
                                            <option value="2534">2534</option>
                                            <option value="2535">2535</option>
                                            <option value="2536">2536</option>
                                            <option value="2537">2537</option>
                                            <option value="2538">2538</option>
                                            <option value="2539">2539</option>
                                            <option value="2540">2540</option>
                                            <option value="2541">2541</option>
                                            <option value="2542">2542</option>
                                            <option value="2543">2543</option>
                                            <option value="2544">2544</option>
                                            <option value="2545">2545</option>
                                            <option value="2546">2546</option>
                                            <option value="2547">2547</option>
                                            <option value="2548">2548</option>
                                            <option value="2549">2549</option>
                                            <option value="2550">2550</option>
                                            <option value="2551">2551</option>
                                            <option value="2552">2552</option>
                                            <option value="2553">2553</option>
                                            <option value="2554">2554</option>
                                            <option value="2555">2555</option>
                                            <option value="2556">2556</option>
                                            <option value="2557">2557</option>
                                            <option value="2558">2558</option>
                                            <option value="2559">2559</option>
                                            <option value="2560">2560</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-1 col-sm-2">หรือ</div>
                                <div class="col-md-2 col-sm-10"><input type="checkbox" value="Y" name="chbWorkEndPresent5" id="chbWorkEndPresent5" onclick="javascript:ClickWorkEndPresent(document.frm1.chbWorkEndPresent5, 'cmbWorkToMonth5', 'cmbWorkToYear5');"> ถึงปัจจุบัน,</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>ตำแหน่ง</label></div>
                                <div class="col-md-8 col-sm-9">
                                    <div style="border-bottom:none;"><input type="text" class="textbox" name="txbWorkPosition5" id="txbWorkPosition5" maxlength="50" value=""></div>
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>บริษัท</label></div>
                                <div class="col-md-8 col-sm-9"><input type="text" class="textbox" name="txbWorkCompany5" id="txbWorkCompany5" maxlength="50" value=""></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>เงินเดือน</label></div>
                                <div class="col-md-8 col-sm-9"><input type="text" class="textbox w80-m" name="txbWorkSalary5" id="txbWorkSalary5" maxlength="7" style="width:100px" onkeypress="return decimalonly(event)" value=""> &nbsp; บาท</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>รายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-9"><input type="text" class="textbox w80-m" name="txbWorkIncome5" id="txbWorkIncome5" maxlength="7" style="width:100px" onkeypress="return decimalonly(event)" value=""> &nbsp; บาท</div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-4"><label>ระบุรายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-8"><input type="text" class="textbox w100-m" name="txbWorkIncomeOther5" id="txbWorkIncomeOther5" maxlength="100" value=""></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-row">
                                <div class="col-md-2 col-sm-4"><label>ลักษณะงานที่ทำ</label></div>
                                <div class="col-md-10 col-sm-8"><textarea class="textbox" name="txbWorkDetail5" id="txbWorkDetail5" style="height:70px"></textarea></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="form-title">ความสามารถด้านภาษา</div>
            <div class="language_form form-section">
                <div class="row hide-mobile">
                    <div class="col-sm-12">
                        <div class="col-sm-4 col-sm-offset-1">ภาษา</div>
                        <div class="col-sm-2">การพูด</div>
                        <div class="col-sm-2">การเขียน</div>
                        <div class="col-sm-2">การอ่าน</div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 form-row">
                        <div class="col-md-1 col-sm-3 mb10-m"><span class="show-mobile">ภาษาที่ </span>1.</div>
                        <div class="col-md-4 col-sm-9 mb10-m"><input type="text" class="textbox" name="txbLanguage1" id="txbLanguage1" maxlength="30" style="width:230px" value=""></div>
                        <div class="show-mobile col-sm-3 mb10-m">การพูด</div>
                        <div class="col-md-2 col-sm-9 mb10-m">
                            <div class="select-block">
                                <select name="cmbLanguageTalk1" id="cmbLanguageTalk1">
                                    <option value="">เลือก</option>
                                    <option value="1">ดีมาก</option>
                                    <option value="2">ดี</option>
                                    <option value="3">พอใช้</option>
                                </select>
                            </div>
                        </div>
                        <div class="show-mobile col-sm-3 mb10-m">การเขียน</div>
                        <div class="col-md-2 col-sm-9 mb10-m">
                            <div class="select-block">
                                <select name="cmbLanguageWrite1" id="cmbLanguageWrite1">
                                    <option value="">เลือก</option>
                                    <option value="1">ดีมาก</option>
                                    <option value="2">ดี</option>
                                    <option value="3">พอใช้</option>
                                </select>
                            </div>
                        </div>
                        <div class="show-mobile col-sm-3 mb10-m">การอ่าน</div>
                        <div class="col-md-2 col-sm-9 mb10-m">
                            <div class="select-block">
                                <select name="cmbLanguageRead1" id="cmbLanguageRead1">
                                    <option value="">เลือก</option>
                                    <option value="1">ดีมาก</option>
                                    <option value="2">ดี</option>
                                    <option value="3">พอใช้</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 form-row">
                        <div class="col-md-1 col-sm-3 mb10-m"><span class="show-mobile">ภาษาที่ </span>2.</div>
                        <div class="col-md-4 col-sm-9 mb10-m"><input type="text" class="textbox" name="txbLanguage2" id="txbLanguage2" maxlength="30" style="width:230px" value=""></div>
                        <div class="show-mobile col-sm-3 mb10-m">การพูด</div>
                        <div class="col-md-2 col-sm-9 mb10-m">
                            <div class="select-block">
                                <select name="cmbLanguageTalk2" id="cmbLanguageTalk2">
                                    <option value="">เลือก</option>
                                    <option value="1">ดีมาก</option>
                                    <option value="2">ดี</option>
                                    <option value="3">พอใช้</option>
                                </select>
                            </div>
                        </div>
                        <div class="show-mobile col-sm-3 mb10-m">การเขียน</div>
                        <div class="col-md-2 col-sm-9 mb10-m">
                            <div class="select-block">
                                <select name="cmbLanguageWrite2" id="cmbLanguageWrite2">
                                    <option value="">เลือก</option>
                                    <option value="1">ดีมาก</option>
                                    <option value="2">ดี</option>
                                    <option value="3">พอใช้</option>
                                </select>
                            </div>
                        </div>
                        <div class="show-mobile col-sm-3 mb10-m">การอ่าน</div>
                        <div class="col-md-2 col-sm-9 mb10-m">
                            <div class="select-block">
                                <select name="cmbLanguageRead2" id="cmbLanguageRead2">
                                    <option value="">เลือก</option>
                                    <option value="1">ดีมาก</option>
                                    <option value="2">ดี</option>
                                    <option value="3">พอใช้</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 form-row">
                        <div class="col-md-1 col-sm-3 mb10-m"><span class="show-mobile">ภาษาที่ </span>3.</div>
                        <div class="col-md-4 col-sm-9 mb10-m"><input type="text" class="textbox" name="txbLanguage3" id="txbLanguage3" maxlength="30" style="width:230px" value=""></div>
                        <div class="show-mobile col-sm-3 mb10-m">การพูด</div>
                        <div class="col-md-2 col-sm-9 mb10-m">
                            <div class="select-block">
                                <select name="cmbLanguageTalk3" id="cmbLanguageTalk3">
                                    <option value="">เลือก</option>
                                    <option value="1">ดีมาก</option>
                                    <option value="2">ดี</option>
                                    <option value="3">พอใช้</option>
                                </select>
                            </div>
                        </div>
                        <div class="show-mobile col-sm-3 mb10-m">การเขียน</div>
                        <div class="col-md-2 col-sm-9 mb10-m">
                            <div class="select-block">
                                <select name="cmbLanguageWrite3" id="cmbLanguageWrite3">
                                    <option value="">เลือก</option>
                                    <option value="1">ดีมาก</option>
                                    <option value="2">ดี</option>
                                    <option value="3">พอใช้</option>
                                </select>
                            </div>
                        </div>
                        <div class="show-mobile col-sm-3 mb10-m">การอ่าน</div>
                        <div class="col-md-2 col-sm-9 mb10-m">
                            <div class="select-block">
                                <select name="cmbLanguageRead3" id="cmbLanguageRead3">
                                    <option value="">เลือก</option>
                                    <option value="1">ดีมาก</option>
                                    <option value="2">ดี</option>
                                    <option value="3">พอใช้</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 form-row">
                        <div class="col-md-1 col-sm-3 mb10-m"><span class="show-mobile">ภาษาที่ </span>4.</div>
                        <div class="col-md-4 col-sm-9 mb10-m"><input type="text" class="textbox" name="txbLanguage4" id="txbLanguage4" maxlength="30" style="width:230px" value=""></div>
                        <div class="show-mobile col-sm-3 mb10-m">การพูด</div>
                        <div class="col-md-2 col-sm-9 mb10-m">
                            <div class="select-block">
                                <select name="cmbLanguageTalk4" id="cmbLanguageTalk4">
                                    <option value="">เลือก</option>
                                    <option value="1">ดีมาก</option>
                                    <option value="2">ดี</option>
                                    <option value="3">พอใช้</option>
                                </select>
                            </div>
                        </div>
                        <div class="show-mobile col-sm-3 mb10-m">การเขียน</div>
                        <div class="col-md-2 col-sm-9 mb10-m">
                            <div class="select-block">
                                <select name="cmbLanguageWrite4" id="cmbLanguageWrite4">
                                    <option value="">เลือก</option>
                                    <option value="1">ดีมาก</option>
                                    <option value="2">ดี</option>
                                    <option value="3">พอใช้</option>
                                </select>
                            </div>
                        </div>
                        <div class="show-mobile col-sm-3 mb10-m">การอ่าน</div>
                        <div class="col-md-2 col-sm-9 mb10-m">
                            <div class="select-block">
                                <select name="cmbLanguageRead4" id="cmbLanguageRead4">
                                    <option value="">เลือก</option>
                                    <option value="1">ดีมาก</option>
                                    <option value="2">ดี</option>
                                    <option value="3">พอใช้</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-4 col-sm-2">พิมพ์ดีด </div>
                        <div class="col-md-4 col-sm-5">
                            ไทย <input type="text" class="textbox" name="txbTypingThai" id="txbTypingThai" maxlength="2" style="width:50px" onkeypress="return decimalonly(event)" value="">
                        </div>
                        <div class="col-md-4 col-sm-5">
                            อังกฤษ <input type="text" class="textbox" name="txbTypingEng" id="txbTypingEng" maxlength="2" style="width:50px" onkeypress="return decimalonly(event)" value="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-title">ลักษณะงานที่สนใจ</div>
            <div class="job_interesting form-section">
                <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-4 col-sm-3"><label>ตำแหน่งงาน<span class="f-required">*</span></label></div>
                        <div class="col-md-8 col-sm-9">
                            <input type="text" class="textbox" name="txbInterestPosition" id="txbInterestPosition" maxlength="50" value="">
                        </div>
                    </div>
                    <div class="col-md-6 form-row">
                        <div class="col-md-5 col-sm-3"><label>ประเภทงาน</label></div>
                        <div class="col-md-7 col-sm-9">
                            <input type="radio" name="rdoInterestPositionType" id="rdoInterestPositionTypeF" value="F" checked=""> &nbsp; <label for="rdoInterestPositionTypeF"><span></span>Full Time</label> &nbsp; &nbsp; <input type="radio" name="rdoInterestPositionType" id="rdoInterestPositionTypeP" value="P"> &nbsp; <label for="rdoInterestPositionTypeP"><span></span>Part Time</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-4 col-sm-5"><label>เงินเดือนที่คาดหวัง<span class="f-required">*</span></label></div>
                        <div class="col-md-8 col-sm-7">
                            <input type="text" class="textbox" name="txbExpectSalary" id="txbExpectSalary" maxlength="7" style="width:100px" onkeypress="return decimalonly(event)" value=""> &nbsp; บาท
                        </div>
                    </div>
                    <div class="col-md-6 form-row">
                        <div class="col-sm-5"><label>พร้อมเริ่มงานตั้งแต่วันที่<span class="f-required">*</span></label></div>
                        <div class="col-sm-7">
                            <input type="text" name="dateWork" id="dateWork" readonly="" value="" class="hasDatepicker">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-title">บุคคลอ้างอิง (ไม่ใช่ญาติ หรือคนในครอบครัว)</div>
            <div class="ref_person form-section">
                <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-4 col-sm-3"><label>ชื่อ-นามสกุล</label></div>
                        <div class="col-md-8 col-sm-9"><input type="text" class="textbox" name="txbReferenceName" id="txbReferenceName" maxlength="50" value=""></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-4 col-sm-3"><label>ตำแหน่ง</label></div>
                        <div class="col-md-8 col-sm-9"><input type="text" class="textbox" name="txbReferencePosition" id="txbReferencePosition" maxlength="50" value=""></div>
                    </div>
                    <div class="col-md-6 form-row">
                        <div class="col-md-4 col-sm-3"><label>บริษัท</label></div>
                        <div class="col-md-8 col-sm-9"><input type="text" class="textbox" name="txbReferenceCompany" id="txbReferenceCompany" maxlength="50" value=""></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-4 col-sm-3"><label>ความสัมพันธ์</label></div>
                        <div class="col-md-8 col-sm-9"><input type="text" class="textbox" name="txbReferenceRelation" id="txbReferenceRelation" maxlength="50" value=""></div>
                    </div>
                    <div class="col-md-6 form-row">
                        <div class="col-md-4 col-sm-4"><label>เบอร์โทรติดต่อ</label></div>
                        <div class="col-md-8 col-sm-8"><input type="text" class="textbox" name="txbReferenceTel" id="txbReferenceTel" maxlength="30" value=""></div>
                    </div>
                </div>
            </div>
            <div class="form-title">ข้อมูลทั่วไป</div>
            <div class="common_info form-section">
                <div class="row">
                    <div class="col-sm-12">
                        <span>การไปปฏิบัติงาน ณ โครงการหมู่บ้านท่านขัดข้องหรือไม่<span class="f-required">*</span></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 form-row">
                        <div class="col-md-5"><label>เป็นประจำ</label></div>
                        <div class="col-md-4 mrl0">
                            <div class="col-sm-6">
                                <input type="radio" name="rdoWorkSite" id="rdoWorkSiteY" value="Y" onclick="javascript:ChangeOther(document.frm1.rdoWorkSite[1], 'txbWorkSite');" checked=""> &nbsp; <label for="rdoWorkSiteY"><span></span>ไม่ขัดข้อง</label>
                            </div>
                            <div class="col-sm-6">
                                <input type="radio" name="rdoWorkSite" id="rdoWorkSiteN" value="N" onclick="javascript:ChangeOther(document.frm1.rdoWorkSite[1], 'txbWorkSite');"> &nbsp; <label for="rdoWorkSiteN"><span></span>ขัดข้อง</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            โปรดระบุ &nbsp; <input type="text" class="textbox w80-m" name="txbWorkSite" id="txbWorkSite" maxlength="50" style="width:150px; background-color:#F4F4F4" value="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 form-row">
                        <div class="col-md-5"><label>เป็นครั้งคราว</label></div>
                        <div class="col-md-4 mrl0">
                            <div class="col-sm-6">
                                <input type="radio" name="rdoWorkSiteFew" id="rdoWorkSiteFewY" value="Y" onclick="javascript:ChangeOther(document.frm1.rdoWorkSiteFew[1], 'txbWorkSiteFew');" checked=""> &nbsp; <label for="rdoWorkSiteFewY"><span></span>ไม่ขัดข้อง</label>
                            </div>
                            <div class="col-sm-6">
                                <input type="radio" name="rdoWorkSiteFew" id="rdoWorkSiteFewN" value="N" onclick="javascript:ChangeOther(document.frm1.rdoWorkSiteFew[1], 'txbWorkSiteFew');"> &nbsp; <label for="rdoWorkSiteFewN"><span></span>ขัดข้อง</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            โปรดระบุ &nbsp; <input type="text" class="textbox w80-m" name="txbWorkSiteFew" id="txbWorkSiteFew" maxlength="50" style="width:150px; background-color:#F4F4F4" value="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 form-row">
                        <div class="col-md-5"><label>ท่านมีโรคเเรื้อรัง/โรคประจำตัวหรือไม่<span class="f-required">*</span></label></div>
                        <div class="col-md-4 mrl0">
                            <div class="col-sm-6">
                                <input type="radio" name="rdoHasDisease" id="rdoHasDiseaseN" value="N" onclick="javascript:ChangeOther(document.frm1.rdoHasDisease[1], 'txbHasDisease');" checked=""> &nbsp; <label for="rdoHasDiseaseN"><span></span>ไม่มี</label>
                            </div>
                            <div class="col-sm-6">
                                <input type="radio" name="rdoHasDisease" id="rdoHasDiseaseY" value="Y" onclick="javascript:ChangeOther(document.frm1.rdoHasDisease[1], 'txbHasDisease');"> &nbsp; <label for="rdoHasDiseaseY"><span></span>มี</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            โปรดระบุ &nbsp; <input type="text" class="textbox w80-m" name="txbHasDisease" id="txbHasDisease" maxlength="50" style="width:150px; background-color:#F4F4F4" value="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 form-row">
                        <div class="col-md-5"><label>ท่านเคยต้องโทษทางคดีอาญาหรือคดีแพ่งหรือไม่<span class="f-required">*</span></label></div>
                        <div class="col-md-4 mrl0">
                            <div class="col-sm-6">
                                <input type="radio" name="rdoHasAccuse" id="rdoHasAccuseN" value="N" onclick="javascript:ChangeOther(document.frm1.rdoHasAccuse[1], 'txbHasAccuse');" checked=""> &nbsp; <label for="rdoHasAccuseN"><span></span>ไม่เคย</label>
                            </div>
                            <div class="col-sm-6">
                                <input type="radio" name="rdoHasAccuse" id="rdoHasAccuse" value="Y" onclick="javascript:ChangeOther(document.frm1.rdoHasAccuse[1], 'txbHasAccuse');"> &nbsp; <label for="rdoHasAccuseY"><span></span>เคย</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            โปรดระบุ &nbsp; <input type="text" class="textbox w80-m" name="txbHasAccuse" id="txbHasAccuse" maxlength="50" style="width:150px; background-color:#F4F4F4" value="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 form-row">
                        <div class="col-md-5"><label>ท่ายเคยถูกศาลสั่งให้เป็นบุคคล้มละลาย หรือถูกศาลสั่ง<br>พิทักษ์ทรัพย์ หรือมีหนี้สินล้นพ้นตัวหรือไม่<span class="f-required">*</span></label></div>
                        <div class="col-md-4 mrl0">
                            <div class="col-sm-6">
                                <input type="radio" name="rdoHasBankrupt" id="rdoHasBankruptN" value="N" onclick="javascript:ChangeOther(document.frm1.rdoHasBankrupt[1], 'txbHasBankrupt');" checked=""> &nbsp; <label for="rdoHasBankruptN"><span></span>ไม่เคย</label>
                            </div>
                            <div class="col-sm-6">
                                <input type="radio" name="rdoHasBankrupt" id="rdoHasBankruptY" value="Y" onclick="javascript:ChangeOther(document.frm1.rdoHasBankrupt[1], 'txbHasBankrupt');"> &nbsp; <label for="rdoHasBankruptY"><span></span>เคย</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            โปรดระบุ &nbsp; <input type="text" class="textbox w80-m" name="txbHasBankrupt" id="txbHasBankrupt" maxlength="50" style="width:150px; background-color:#F4F4F4" value="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 form-row">
                        <div class="col-md-5"><label>ท่ายเคยมีประวัติถูกเลิกจ้างเพราะเหตุกระทำผิดมาก่อนหรือไม่<span class="f-required">*</span></label></div>
                        <div class="col-md-4 mrl0">
                            <div class="col-sm-6">
                                <input type="radio" name="rdoHasLayoff" id="rdoHasLayoffN" value="N" onclick="javascript:ChangeOther(document.frm1.rdoHasLayoff[1], 'txbHasLayoff');" checked=""> &nbsp; <label for="rdoHasLayoffN"><span></span>ไม่เคย</label>
                            </div>
                            <div class="col-sm-6">
                                <input type="radio" name="rdoHasLayoff" id="rdoHasLayoffY" value="Y" onclick="javascript:ChangeOther(document.frm1.rdoHasLayoff[1], 'txbHasLayoff');"> &nbsp; <label for="rdoHasLayoffY"><span></span>เคย</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            โปรดระบุ &nbsp; <input type="text" class="textbox w80-m" name="txbHasLayoff" id="txbHasLayoff" maxlength="50" style="width:150px; background-color:#F4F4F4" value="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-title">ข้อมูลเพิ่มเติม</div>
            <div class="form-section">
                <textarea name="txbFurtherInfo" id="txbFurtherInfo" class="w100-m" style="height:100px"></textarea>
            </div>
            <div class="form-title">เอกสารแนบ</div>
            <div class="document_form">
                <div class="row">
                    <div class="col-sm-12 form-row">
                        <div class="col-md-2 col-sm-4"><label>แนบ Resume</label></div>
                        <div class="col-md-4 col-sm-8"><input type="file" name="flResume" id="flResume" style="border:none;"></div>
                        <div class="col-md-6">ขนาดของ File ไม่เกิน 1 MB. ในกรณีมีหลาย File ให้ Zip เป็น 1 File</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 form-row">
                        <div class="col-md-2 col-sm-4"><label>แนบรูป</label></div>
                        <div class="col-md-4 col-sm-8"><input type="file" name="flPicture" id="flPicture" style="border:none;"></div>
                        <div class="col-md-6">ขนาดของ File ไม่เกิน 500 KB. ในรูปแบบของ .jpg, .gif หรือ .tif</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 form-row">
                        <input type="checkbox" id="chbAccept" name="chbAccept" value="Y" onclick="javascript:ClickAccept();" />
                        <label for="chbAccept"><span></span>ข้าพเจ้าขอรับรองว่า ข้อความทั้งหมดเป็นจริงทุกประการ การบิดเบือนหรือปิดบังข้อเท็จจริงใด ๆ ในใบสมัครนี้ ย่อมเป็นสาเหตุเพียงพอที่จะเลิกจ้างข้าพเจ้าได้ทันทีโดยไม่มีข้อแม้ใด ๆ ทั้งสิ้น</label>
                    </div>
                </div>
                <div class="row">
                    <div class="form-row wrap-btns">
                        <input type="button" name="btnPreview" id="btnPreview" value="  ดูใบสมัครก่อนส่ง  " onclick="javascript:ChkInput('P');"> &nbsp; &nbsp; &nbsp; <input type="button" name="btnSend" id="btnSend" value="  ส่งใบสมัคร  " onclick="javascript:ChkInput('A');" disabled="" style="background-color: #E1E1E1">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 form-row">
                        สอบถามข้อมูลการสมัครงาน ติดต่อ คุณยุพาวณี 02-230-8359
                    </div>
                </div>
            </div>
            </div>
            <a id="splash_app" class="iframe" href="app_preview.jsp?id=2240" style="display:none">Splash</a>
            <div class="clear"></div>
    </form>

</div>
<a id="backJobListsBtm" class="btn-backjob">< Back</a>