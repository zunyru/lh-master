<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

$page = 'foreign-ownership';
$page_index = 0;
$img = '';

?>
<?php include('header.php'); ?>
<!-- CSS -->
<link rel="stylesheet" href="<?= file_path('css/foreign-ownership.css') ?>" type="text/css">

<!-- JS -->

<div id="content" class="content foreign-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="">
                 <?php
                 $SEO = getSEOUrl($actual_link);
                 if($actual_link == @$SEO->url_page){
                    echo '<h1 class="heading-title">'.$SEO->h1.'<h1>';
                }else{
                   echo '<h1>由外籍人士持有的房地产</h1>'; 
               }
               ?>

               <h2>“外籍人士”的定义</h2>

                 <p class="mb5"><strong>“外籍人士”</strong> 或 <strong>“外侨”</strong> 指的是没有泰国国籍的人或者依法被视为外籍人士的法人（外籍人士持股超过49%</p>
                 <p>所持有的<strong>“房地产”</strong>分为土地以及套房
                 关于持股模式分为所有权以及租赁</p>

                 <div class="foreign-block">
                    <!-- <div class="row">
                        <div class="col-md-6">
                            <div class="topic-title-block">
                                <span class="topic-icon tpi1"></span><span class="topic-no">1</span>
                                <p class="foreign-title"><span>Real estate</span><br>
                                acquisition - Freehold</p>
                            </div>
                            <div class="">
                                <p>外籍人士获得土地以及建筑物的所有权受到土地法以及其他相关法规的限制，即<p>
                                  <p>  •   通过投资 </p>
                                   <p> 根据土地法规定，前来投资金额不少于4000万泰铢的外籍人士有权拥有为个人或家庭所用的面积不超过1莱的土地所有权，收购土地需要获得部长的批准，以上投资必须符合以下条件和标准：
                                </p>
                            <ul class="foreign-list">
                                <li><i></i>必须为对国家经济以及未来有利的业务投资，债券购买，房地产投资基金或投资委员会已规定的投资业务(Board of Investment – BOI)</li>
                                <li><i></i>投资期限至少3年</li>
                                <li><i></i>需持有曼谷，芭提雅市，管辖区或根据城市规划法指定的住宅区内持有土地。</li>
                            </ul>
                            <p>若泰国公民有外籍配偶并打算购买土地，夫妻双方必须同时向主管部门以书面形式确认所有用于购买土地的钱为私人财产或为拥有泰籍国籍的个人财产而非夫妻共同财产。在此，以上房地产的所有权必须视为泰籍配偶的私人财产。</p>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="content-img">
                            <img src="<?= file_path('images/foreign/fr_1.jpg') ?>" alt="">
                        </div>
                        <div class="foreigh-col-2">
                            <p>若泰国公民有外籍配偶并打算购买土地，夫妻双方必须同时向主管部门以书面形式确认所有用于购买土地的钱为私人财产或为拥有泰籍国籍的个人财产而非夫妻共同财产。在此，以上房地产的所有权必须视为泰籍配偶的私人财产。</p>
                            <p><strong>-  通过遗产继承</strong><br>
                           外籍人士可依法成为土地的法定继承人，每户对于与现有土地相结合而继承的住宅用地不超过1莱。</p>
                        </div>
                    </div>
                </div> -->
            </div>

            <div class="foreign-block">
                <div class="row">
                    <div class="col-md-6">
                        <div class="content-img">
                            <img src="<?= file_path('images/foreign/fr_2.jpg') ?>" alt="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="topic-title-block fr2">
                            <span class="topic-icon tpi2"></span><span class="topic-no">1</span>
                            <p class="foreign-title"><span>Ownership</span><br>
                            of Condominium</p>
                        </div>
                        <div class="">
                            <p>根据1979年“共管公寓法”，外籍人士可购买共管公寓并占有不超过49%的公寓总面积的所有权.</p>

                            <p>若建筑仍有外籍人士占有不超过49%的总面积，便可作为遗产继承，外籍人士可 通过法定继承或遗嘱继承的方式来继承。若共管公寓的外籍人士居住超出规定的配额，从获得所有权之日开始计算，在强制拍卖出售之前外籍人士延长居住期限不超过1年，外籍人士可获得扣除债务（若有）之后的所有拍卖收益。</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="foreign-block">
                <div class="row">
                    <div class="col-md-6">
                        <div class="topic-title-block fr3">
                            <span class="topic-icon tpi3"></span><span class="topic-no">2</span>
                            <p class="foreign-title"><span>Leasehold of</span><br>
                            Real Estate Property</p>
                        </div>
                        <div class="">
                            <p>短期租赁指时长不超过3年, 无须向主管部门登记，但负责任的一方必须以书面形式签署起法律效力的租赁。</p>

                            <p>长期租赁指时长超过3年, 必须有书面形式并向主管部门登记，否则其有效期仅限3年，包括根据1999年商业和工业用地租赁法而进行租赁的土地，其租赁年限可长达30年以上50年以下，且到期的租赁可根据商业以及工业类型依法进行30年以上50年以下租赁延期。</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="content-img">
                            <img src="<?= file_path('images/foreign/fr_3.jpg') ?>" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
</div>




<?php include('footer.php'); ?>
