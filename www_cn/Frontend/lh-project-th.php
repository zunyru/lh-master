<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

$page = 'lh-project';
$page_index = 0;
$img = '';

?>
<?php include('header.php'); ?>
<!-- CSS -->
<link rel="stylesheet" href="<?= file_path('css/lh-project.css') ?>" type="text/css">

<!-- JS -->
<script src="<?= file_path('js/lh-project.js') ?>"></script>

<div id="content" class="content lh-project-page">
    <div class="container">
        <h1 class="heading-title">LH Projects</h1>

        <div id="lhp1" class="project-section lhslide">
            <p class="heading-title">独立别墅</p>

            <div class="row">
                <div id="lhpSlide1" class="col-slide">

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_1.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">LADAWAN</p>
                                <p>
                                    卓越豪华独立别墅<br>
                                    起步价  50-200 万泰铢
                                </p>
                            </a>
<!--                            <a href="#" class="btn btn-seemore">See more</a>-->
                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_2.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">Nantawan</p>
                                <p>
                                    豪华独立别墅<br>
                                    起步价  20-50 万泰铢
                                </p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_3.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">Mantana</p>
                                <p>
                                    一个周围设施齐全氛围犹如度假村的大家庭住宅.<br>
                                    价格　6– 25万泰铢
                                </p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_4.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">Siwalee</p>
                                <p>
                                    住宅适合家庭成员正在壮大的家庭, 环境优美自然,地处位置特别.<br>
                                    价格 6百万- 2千万 泰铢.</p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_5.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">Chaiyapruek</p>
                                <p>
                                   别墅对于一个刚刚组建的家庭，出售前整个别墅结构完善，小区环境优美，周围设施便利完善。<br>
                                    起步价  6万泰铢
                                </p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_6.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">Phruek Lada</p>
                                <p>
                                    别墅对于一个刚刚组建的家庭，出售前整个别墅结构完善，小区环境优美，周围设施便利完善。<br>
                                    起步价  4万泰铢
                                </p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_7.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">Inizio</p>
                                <p>
                                   家庭住宅 ，出售前整个别墅结构完善，小区环境优美，周围设施便利完善。<br>
                                   起步价  3.6 万泰铢
                                </p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_8.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">Villaggio</p>
                                <p>
                                    现代别墅对于一个刚刚组建的家庭，出售前整个别墅结构完善，小区环境优美，周围设施便利完善。<br>
                                    起步价  3万泰铢
                                </p>
                            </a>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="lhp2" class="project-section lhslide">
            <p class="heading-title">连排别墅 </p>

            <div class="row">
                <div id="lhpSlide2" class="col-slide">

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_9.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">Landmark</p>
                                <p>
                                    豪华连排别墅,在非常便捷的地方其房价无法预估 爱噶卖-拉铭它(Ekkamai-Ramindra)<br>
                                    价格　7.5-15万泰铢
                                </p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_10.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">Villaggio (连排别墅)</p>
                                <p>
                                    在欧洲风格的小区里建筑最精美的独立别墅和联排别墅. 不仅外观好看而且宜居，再加上周围设施便利完善。<br>
                                    价格　2-5万泰铢
                                </p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_11.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">Indy</p>
                                <p>
                                    在欧洲风格的小区里建筑最精美的联排别墅. 不仅外观好看，而且宜居，再加上周围设施便利完善。<br>
                                    价格　2.2-3 万泰铢
                                </p>
                            </a>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="lhp3" class="project-section lhslide">
            <p class="heading-title">公寓 |<a href="https://www.lh.co.th/thebestcondominiumsinbangkok/">VDO HIGHLIGHTS</a></p>

            <div class="row">
                <div id="lhpSlide3" class="col-slide">

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_12.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">The Bangkok</p>
                                <p>
                                   .“Best in Life” Land & Houses 的卓越豪华公寓. 公寓具有着无穷与意义,只有生活在这的人才能体会到其最美好的意义.<br>
                                    价格　15 - 30万泰铢
                                </p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_13.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">The Room</p>
                                <p>
                                    .“Real Life Real Living” 虽是公寓但却犹如是在市中心的独立别墅一般，靠近地铁站，不仅只是提供每天生活上的方便还会增添生活的质量。<br>
                                   价格　5-16万泰铢
                                </p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_14.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">333 River side</p>
                                <p>
                                   在湄南河河湾处的公寓靠近地铁站，最好的地段，优越的地理位置和发展空间会使你的激情有感而发，充斥着生活的每一天.<br>
                                    起步价  5.89万泰铢
                                </p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_15.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">The Key</p>
                                <p>
                                    一个各方面齐全便捷的公寓，对于刚开始在这里生活的人来说，出行便捷，周围设施非常完善，可以接受多种生活方式。<br>
                                    起步价  3.9万泰铢
                                </p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_16.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">Wan Vayla</p>
                                <p>（无尽的休闲时光），我们各自都会需要在生活中取得一些成功，但有时我们会忘记成功可能买不回时间。又有多少个人会有属于自己的时光，欢迎来到Wan Vayla, 公寓位于华欣-龟岛海边，在海滩,大海和蓝天拥为一起的大自然中休憩。<br>
                                    起步价  12万泰铢
                                </p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_17.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">North Condo</p>
                                <p>.幸福共享的清迈市中心.身处大自然中的高级公寓，拥有景色秀丽的素贴山优美景观，周围环绕着一百多莱美丽的高山云海。
                                    <br>
                                    起步价2.4万泰铢　</p>
                            </a>

                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>
</div>


<?php include('footer.php'); ?>
