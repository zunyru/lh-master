<?php include('header.php'); ?>
    <!-- CSS -->
    <link rel="stylesheet" href="css/all-home-series-detail.css" type="text/css">
    <!-- JS -->
    <script src="js/all-home-series-detail.js"></script>


    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="heading-logo"><img src="images/logo/logo_nuntawan.jpg" alt=""></h1>
                <p class="top-header-title heading-title green">แบบบ้าน Viscaria <span>หมายเลขแปลง 4B-5</span></p>
            </div>
        </div>
    </div>

    <div class="page-banner">
        <div class="page-banner-content" style="background-image: url(images/temp2/banner_all_home_detail.jpg);"></div>
    </div>

    <div id="content" class="content project-info-furnished-page">
        <div class="container">
            <div class="project-info-detail-block">
                <p class="heading-title header-margin">แบบแปลนบ้าน</p>
                <div id="homePlanSlide">
                    <div class="item">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="project-info-plan-img">
                                    <a href="#" data-featherlight="images/temp/proj_info_furnished_img1.jpg">
                                        <img src="images/temp/proj_info_furnished_img1.jpg" alt="">
                                        <span class="i-zoom"></span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="tabs-container" class="">
                                    <div class="tab-dd dropdown">
                                        <div class="row">
                                            <div class="">
                                                <ul class="tabs-menu-plan-slide">
                                                    <li class="current">
                                                        <a data-value="0">
                                                            ชั้นหนึ่ง
                                                        </a>
                                                    </li>
                                                    <li class="">
                                                        <a data-value="1">
                                                            ชั้นสอง
                                                        </a>
                                                    </li>
                                                    <li class="">
                                                        <a data-value="2">
                                                            ชั้นสาม
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab">
                                        <div id="tab-1" class="tab-content">
                                            <div class="tab-block content-style">
                                                <ul>
                                                    <li>Foyer โถงทางเข้าหลัก หรูหรามีระดับ เชื่อมต่อห้องรับแขกขนาดใหญ่ เปิดรับ
                                                    วิวด้วยช่องกระจกบานเข้ามุม</li>
                                                    <li>Open Plan Concept พื้นที่เปิดโล่งเชื่อมต่อห้องรับแขก ห้องทานอาหาร
                                                    และส่วนเตรียมอาหาร Island Concept ลงตัวสำหรับทุกกิจกรรมของ
                                                    ครอบครัว</li>
                                                    <li>ห้องพักผ่อนชั้นล่าง โปร่งโล่งลงตัวด้วยวิวสวนข้างบ้าน
                                                    ห้องนอนชั้นล่างให้ความเป็นส่วนตัวเชื่อมต่อห้องน้ำชั้นล่าง สะดวกต่อการใช้งาน</li>
                                                    <li>ห้องครัวไทยแยกส่วน พร้อมส่วนซักรีด พื้นที่วางเครื่องซักผ้าและเคาน์เตอร์
                                                    อเนกประสงค์</li>
                                                    <li>ห้องคนรับใช้พร้อมห้องน้ำ</li>
                                                    <li>ที่จอดรถ3คัน</li>
                                                </ul>
                                                <ul>
                                                    หมายเหตุ: ตัวเลขขนาดในแปลนเป็นระยะโดยประมาณ รายละเอียดของแปลนและแบบบ้านนี้เพื่อเป็นแนวทางพิจารณา ท่านสามารถดูรายละเอียดของบ้านหลังจริง ณ โครงการ บริษัทฯ ขอสวนสิทธิ์ในการเปลี่ยนแปลงรายการวัสดุและรายละเอียดโดยไม่ต้องแจ้งให้ทราบล่วงหน้า.
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="project-info-plan-img">
                                    <a href="#" data-featherlight="images/temp/proj_info_furnished_img1.jpg">
                                        <img src="images/temp/proj_info_furnished_img1.jpg" alt="">
                                        <span class="i-zoom"></span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="tabs-container" class="">
                                    <div class="tab-dd dropdown">
                                        <div class="row">
                                            <div class="">
                                                <ul class="tabs-menu-plan-slide dropdown-menu" aria-labelledby="dropdownMenu1">
                                                    <li class="">
                                                        <a data-value="0">
                                                            ชั้นหนึ่ง
                                                        </a>
                                                    </li>
                                                    <li class="current">
                                                        <a data-value="1">
                                                            ชั้นสอง
                                                        </a>
                                                    </li>
                                                    <li class="">
                                                        <a data-value="2">
                                                            ชั้นสาม
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab">
                                        <div id="tab-1" class="tab-content">
                                            <div class="tab-block content-style">
                                                <ul>
                                                    <li>พื้นที่รับแขก และพื้นที่รับประทานอาหาร รายล้อมด้วยวิวสวน
                                                        กว้างขวางเป็นพิเศษพร้อม BAY WINDOW ขยายมุมมองกว้างขึ้น
                                                    </li>
                                                    <li>บานประตูและบานหน้าต่าง ขนาดกว้างใหญ่
                                                        เปิดเชื่อมพื้นที่สวนข้างบ้านเพิ่มอารมณ์
                                                    </li>
                                                </ul>
                                                <ul>
                                                    หมายเหตุ: ตัวเลขขนาดในแปลนเป็นระยะโดยประมาณ รายละเอียดของแปลนและแบบบ้านนี้เพื่อเป็นแนวทางพิจารณา ท่านสามารถดูรายละเอียดของบ้านหลังจริง ณ โครงการ บริษัทฯ ขอสวนสิทธิ์ในการเปลี่ยนแปลงรายการวัสดุและรายละเอียดโดยไม่ต้องแจ้งให้ทราบล่วงหน้า.
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="project-info-plan-img">
                                    <a href="#" data-featherlight="images/temp/proj_info_furnished_img1.jpg">
                                        <img src="images/temp/proj_info_furnished_img1.jpg" alt="">
                                        <span class="i-zoom"></span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="tabs-container" class="">
                                    <div class="tab-dd dropdown">
                                        <div class="row">
                                            <div class="">
                                                <ul class="tabs-menu-plan-slide dropdown-menu" aria-labelledby="dropdownMenu1">
                                                    <li class="">
                                                        <a data-value="0">
                                                            ชั้นหนึ่ง
                                                        </a>
                                                    </li>
                                                    <li class="">
                                                        <a data-value="1">
                                                            ชั้นสอง
                                                        </a>
                                                    </li>
                                                    <li class="current">
                                                        <a data-value="2">
                                                            ชั้นสาม
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab">
                                        <div id="tab-1" class="tab-content">
                                            <div class="tab-block content-style">
                                                <ul>
                                                    <li>พื้นที่รับแขก และพื้นที่รับประทานอาหาร รายล้อมด้วยวิวสวน
                                                        กว้างขวางเป็นพิเศษพร้อม BAY WINDOW ขยายมุมมองกว้างขึ้น
                                                    </li>
                                                </ul>
                                                <ul>
                                                    หมายเหตุ: ตัวเลขขนาดในแปลนเป็นระยะโดยประมาณ รายละเอียดของแปลนและแบบบ้านนี้เพื่อเป็นแนวทางพิจารณา ท่านสามารถดูรายละเอียดของบ้านหลังจริง ณ โครงการ บริษัทฯ ขอสวนสิทธิ์ในการเปลี่ยนแปลงรายการวัสดุและรายละเอียดโดยไม่ต้องแจ้งให้ทราบล่วงหน้า.
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="home-detail-block">
                <div class="row">
                    <div class="col-md-12">
                        <p class="heading-title">รายละเอียดบ้านพร้อมตกแต่ง</p>
                    </div>
                    <div class="col-md-4">
                        <p>แบบบ้าน Viscaria หมายเลขแปลง 4B-5</p>
                        <p class="title">โครงการบ้านนันทวัน ปิ่นเกล้า-ราชพฤกษ์</p>
                        <p>ลักษณะเด่นของแปลง : 4B-5 คฤหาสน์หรูตกแต่ง </p>
                        <p>เฟอร์นิเจอร์พร้อมเข้าอยู่หัวมุม หน้าสวน </p>
                        <p class="heading-title bold">ราคา 40.9 ล้านบาท</p>
                        <p class="heading-title">สิ่งอำนวยความสะดวก</p>
                        <img src="images/global/all_home_detail_icons.png" alt="">
                    </div>
                    <div class="col-md-4">
                        <div class="content-style txt-22">
                            <table class="content-style" width="80%">
                                <tr>
                                    <td class="tb-bullet"><i class="i-bullet right"></i></td>
                                    <td>พื้นที่ใช้สอย</td>
                                    <td>397</td>
                                    <td>ตร.ม</td>
                                </tr>
                                <tr>
                                    <td class="tb-bullet"><i class="i-bullet right"></i></td>
                                    <td>ขนาดที่ดิน</td>
                                    <td>129</td>
                                    <td>ตร.ว</td>
                                </tr>
                                <tr>
                                    <td class="tb-bullet"><i class="i-bullet right"></i></td>
                                    <td>ห้องนอน</td>
                                    <td>4</td>
                                    <td>ห้อง</td>
                                </tr>
                                <tr>
                                    <td class="tb-bullet"><i class="i-bullet right"></i></td>
                                    <td>ห้องน้ำ</td>
                                    <td>4</td>
                                    <td>ห้อง</td>
                                </tr>
                                <tr>
                                    <td class="tb-bullet"><i class="i-bullet right"></i></td>
                                    <td>ที่จอดรถ</td>
                                    <td>3</td>
                                    <td>คัน</td>
                                </tr>
                            </table>
                        </div>

                    </div>

                    <div class="col-md-4">
                        <div class="content-style">
                            <table class="content-style tb-home-detail" width="100%">
                                <thead>
                                    <tr>
                                        <th colspan="3">ราคาและเงื่อนไข</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>ขนาดที่ดิน</td>
                                        <td>129</td>
                                        <td>ตร.วา</td>
                                    </tr>
                                    <tr>
                                        <td>ราคา</td>
                                        <td>40,900,000</td>
                                        <td>บาท</td>
                                    </tr>
                                    <tr>
                                        <td>ดาวน์</td>
                                        <td>8,200,000</td>
                                        <td>บาท</td>
                                    </tr>
                                    <tr>
                                        <td>เงินจอง</td>
                                        <td>500,000</td>
                                        <td>บาท</td>
                                    </tr>
                                    <tr>
                                        <td>เงินทำสัญญา</td>
                                        <td>7,700,000</td>
                                        <td>บาท</td>
                                    </tr>
                                    <tr>
                                        <td>เงินโอน</td>
                                        <td>32,600,000</td>
                                        <td>บาท</td>
                                    </tr>
                                    <tr>
                                        <td>ผ่อนเริ่มต้น</td>
                                        <td>130,000</td>
                                        <td>บาท</td>
                                    </tr>
                                    <tr>
                                        <td>ดอกเบี้ย</td>
                                        <td>4.00</td>
                                        <td>%</td>
                                    </tr>
                                    <tr>
                                        <td>ระยะเวลาผ่อนชำระ</td>
                                        <td>30</td>
                                        <td>ปี</td>
                                    </tr>
                                </tbody>
                            </table>
                            <p class="txt-remark-sm"><i class="i-cal"></i>กรุณาตรวจสอบเงื่อนไขและราคากับทางโครงการอีกครั้ง<br>
                                อัตราดอกเบี้ย กรุณาตรวจสอบกับธนาคารที่ใช้บริการอีกครั้ง
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="gallery-block">
                <p class="heading-title header-margin">Gallery</p>
                <div class="tpl5-img">
                    <div id="tpl5-img">
                        <div class="item tpl5-img-1">
                            <a class="gallery" href="images/temp2/all_home_detail2.jpg">
                                <img src="images/temp2/all_home_detail2.jpg" alt="">
                            </a>
                        </div>
                        <div class="item tpl5-img-2">
                            <a class="gallery" href="images/temp2/all_home_detail3.jpg">
                                <img src="images/temp2/all_home_detail3.jpg" alt="">
                            </a>
                        </div>
                        <div class="item tpl5-img-3">
                            <a class="gallery" href="images/temp2/all_home_detail4.jpg">
                                <img src="images/temp2/all_home_detail4.jpg" alt="">
                            </a>
                        </div>
                        <div class="item tpl5-img-4">
                            <a class="gallery" href="images/temp2/all_home_detail5.jpg">
                                <img src="images/temp2/all_home_detail5.jpg" alt="">
                            </a>
                        </div>
                        <div class="item tpl5-img-5">
                            <a class="gallery" href="images/temp2/all_home_detail6.jpg">
                                <img src="images/temp2/all_home_detail6.jpg" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php include('footer.php'); ?>