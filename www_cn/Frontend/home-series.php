<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';
require_once 'include/help/hex2rgba.php';

$home_series = getHomeSeries();

//die(ddd($home_series));
$page = 'home-series';
$page_index = 9;
$img = '';
?>
<?php
include('header.php');
elementMetaTitleDisTag($page);
?>
    <!-- JS -->
    <link rel="stylesheet" href="<?= file_path('css/home-series.css') ?>" type="text/css">
    <link rel="stylesheet" href="<?= file_path('css/video.css') ?>" type="text/css">

    <script src="<?= file_path('js/home-series.js') ?>"></script>

    <div class="page-banner page-scroll">
        <span id="scrollNext" class="i-scroll-next"></span>

        <?php
            $checkBannerlead  = getBannerByTypePage(9,true);
            $num =  count($checkBannerlead);
            if($checkBannerlead == false){
               $num+= 1;
            }

        ?>
        <input type="hidden" name="banner" id="bannerSlide_count" value="<?=$num?>">
        <div id="bannerSlide" class="owl-carousel">

            <?php
            renderBannerPageList(9);
            ?>

        </div>
    </div>

    <?php
    renderHiddenLeadImage(9,'bannerSlide');
    renderActivityResponsive();
    ?>

    <div id="content" class="content project-type-page">
        <div class="container">
            <div class="home-series-top-logo">
                <h1 class="rps">LH 住房类型</h1>
                <div class="row">
                    <div id="hmsLogo" class="">
                        <?php
                        foreach ($home_series as $index => $series_item) {
                            $plan_series = getPlanSeries($series_item->series_id);
                            if (!empty($plan_series)) {
                        ?>
                                <a href="<?= $router->generate('split-home-series', [
                                    'series_name' => str_replace(' ','-',$series_item->series_name_th),
                                ]) ?>" class="hms-top-logo">

                                    <div class="col-xs-12 center-item">
                                        <img src="<?= backend_url('series_logo', $series_item->series_logo) ?>" alt="">
                                    </div>

                                </a>
                        <?php 
                            }
                        } ?>
                    </div>
                </div>
            </div>
        </div>

    <?php
    $countHomeSeries = 0;
    foreach ($home_series as $index => $series_item) {
        $plan_series = getPlanSeries($series_item->series_id);
        if (!empty($plan_series)) {
            $countHomeSeries++;
        }
    }
    ?>
    <?php
    $SEO = getSEOUrl($actual_link);
    if($actual_link == @$SEO->url_page){
        $title_ = $SEO->h1;
    }else{
       $title_  = 'LH 的所有房间类型'; 
   }
   ?>

   <div class="container">
    <h1 style="cursor: pointer" onclick="location.href='<?= $router->generate('home-series') ?>'"><?=$title_?></h1>
    <div class="project-type-block">
        <h2 style="color: #4d4c4d;">推荐高品质房间类型  <?= $countHomeSeries ?> 项目．</h2>

                <div id="products" class="list-group">

                    <?php foreach ($home_series as $index => $series_item) {
                        $plan_series = getPlanSeries($series_item->series_id);
                        if(!empty($plan_series)){
                    ?>
                        <div class="projecttype-list">
                            <div class="row veralign-middle">
                                <div class="col-md-8">
                                    <?php
                                    if($series_item->status == 'NEW'){
                                        ?>
                                        <div class="newseriestag">
                                            <div class="newtagtext">
                                                New<br>Series
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <a href="<?= $router->generate('split-home-series',[
                                            'series_name'  =>  str_replace(' ','-',$series_item->series_name_th),
                                        ]) ?>">
                                    <img src="<?= backend_url('series',$series_item->series_img_name) ?>" alt="" class="projt-img">
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <div class="projecttype-descrp">
                                        <img src="<?= backend_url('series_logo',$series_item->series_logo) ?>" alt="">
                                        <p class="desc_series_home<?= $index ?>">
                                        <?=!empty($series_item->series_des_th) ? str_replace(array("\n\r", "\n","n/g"), '<br/>', $series_item->series_des_th) : ''?>
                                            <!-- <script type="text/javascript">
                                                $(document).ready(function () {
                                                    $('.desc_series_home<?= $index ?>').html(`<?= $series_item->series_des_th ?>`.replace(/\r?\n/g, '<br/>'));
                                                });
                                            </script> -->
                                        </p>
                                        <a href="<?= $router->generate('split-home-series',[
                                            'series_name'  =>  str_replace(' ','-',$series_item->series_name_th),
                                        ]) ?>" class="btn btn-seemore">更多信息</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                        }
                    }
                    ?>
                </div>
            </div>

        </div>
    </div>


<?php include('footer.php'); ?>