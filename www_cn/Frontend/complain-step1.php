<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>
<?php include('header.php'); ?>
    <!-- CSS -->
    <link rel="stylesheet" href="<?= file_path('css/complain.css') ?>" type="text/css">
    <!-- JS -->
    <script src="<?= file_path('js/complain.js') ?>"></script>

    <div class="page-banner-md banner-hide">
        <div id="bannerSlide">
            <div class="item">
                <div class="banner-parallax">
                    <img src="<?= file_path('images/temp2/banner_complain.jpg') ?>" alt="">
                </div>
            </div>
        </div>
    </div>

    <div id="content" class="content rps-content-pd">
        <div class="container">
            <h1 class="heading-title">ร้องเรียนเรื่องบ้านและคอนโด</h1>

            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div>
<!--                       <p class="title">คุณสามารถค้นหาชื่อโครงการที่คุณอาศัย และส่งข้อความร้องเรียนบ้านและคอนโดมาที่ แลนด์ แอนด์เฮ้าส์</p>-->
                        <div class="complain-form-block">
                            <form action="" class="complain-form form-container">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="step-label">
                                            step 1
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <p class="title">เลือกประเภทโครงการที่ท่านต้องการ</p>
                                        <div class="form">
                                            <div class="form-group">
                                                <div>
                                                    <input type="radio" id="house" name="projectType" />
                                                    <label for="house"><span></span>บ้านเดี่ยว, ทาวน์โฮม</label>
                                                </div>

                                                <div>
                                                    <input type="radio" id="condo" name="projectType" />
                                                    <label for="condo"><span></span>คอนโด</label>
                                                </div>
                                            </div>
                                            <div class="txt-suggest">
                                                <p>วิธีการค้นหาแบบง่ายๆ เพียงแค่พิมพ์คำขึ้นต้นของชื่อหมู่บ้านหรืออาคารชุดของคุณ เช่น คุณต้องการค้นหา หมู่บ้านชัยพฤกษ์เพียงแค่พิมพ์ "ชัย" และคลิกที่ปุ่มค้นหาได้ทันที</p>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" name="" class="form-control" placeholder="ระบุชื่อโครงการ">
                                            </div>
                                            <a href="<?= $router->generate('complain-step',['step' => 2]) ?>" class="btn btn-submit next">Next</a>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


<?php include('footer.php'); ?>
