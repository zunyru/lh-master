<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';
$page  =  'report';
$page_index = 0;
$img = '';
?>

<?php
include('header.php');
elementMetaTitleDisTag($page);
?>
<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '342952452528044'); // Insert your pixel ID here.
    fbq('track', 'PageView');
    fbq('track', 'Lead');
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=342952452528044&ev=PageView&noscript=1"
    /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

    <!-- CSS -->
    <link rel="stylesheet" href="<?= file_path('css/contact.css') ?>" type="text/css">
    
    <style>
        
        .error {
            
            color: red;
        }
    
    </style>
    <!-- <script src='https://www.google.com/recaptcha/api.js?hl=th'></script> -->
    <!-- JS -->

    <script src="js/complain.js"></script>

    <div class="page-banner-md banner-hide">
        <div id="bannerSlide">
            <div class="item">
                <div class="banner-parallax">
                    <img src="<?= file_path('images/temp5/banner_report.jpg') ?>" alt="">
                </div>
            </div>
        </div>
    </div>

    <div id="content" class="content contact-page rps-content-pd">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div>
                        <h1 class="heading-title">报告线上使用的问题 </h1>  <a href="http://www7.lh.co.th/LHComplaint/index.jsp">
                            (需要投诉关于房间问题的，请点击这里)</a> <br>
                       <!--  <p class="txt-22">หากพบปัญหาการใช้งานบนเว็บไซต์ สามารถแจ้งปัญหาได้ที่นี่ กรุณากรอกข้อมูลตามแบบฟอร์มข้างล่างนี้</p> -->
                        <form action="mail-report" class="contact-form form-container" id="reportform" method="POST">
                            <div class="">
                                <div class="form-group">
                                    <input type="text" name="mailto" value="webmaster@lh.co.th" class="form-control" 
                                    placeholder="webmaster@lh.co.th" style="background-color: #ffffff;" readonly>
                                </div>

                                <div class="form-group">
                                    <input type="number" id="phone" name="phone" class="form-control" min="0" placeholder="*电话" 
                                    required>
                                </div>
                                
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="*Email Address" required>
                                </div>


                                <div class="form-group">
                                    <textarea name="detail" placeholder="消息" id="" class="form-control" cols="30" rows="10"></textarea>
                                </div>

                               <!--  <div id ="RecaptchaField2"></div> -->
                                <div class="g-recaptcha" data-sitekey="6LcSzSEUAAAAAPTUl4maci5Pl4SyWVMmnNhly0Qw"></div>
                                <span id="chacheck1" style="display: none; color: red;"> 经验证您不是机器人 !</span>
                                <button type="submit" class="btn btn-submit">发信息</button>

                            </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <p class="txt-inform">在 <a href="mailto:webmaster@lh.co.th">webmaster@lh.co.th</a>报告更多线上使用的问题.</p>
                </div>

            </div>


        </div>
    </div>

<!-- validate -->
 <script src="<?= file_path('js/lib/jquery.validate.js') ?>"></script>
    <script>

        $(document).ready(function() {

            $("#reportform").validate({
                rules: {
                    email: "required",
                    phone: "required",
                    // project_review_dis_th: "required",
                    // community_img : "required",
                    // living_tip_content_th : "required",
                    // fileupload_en : "required",
                },
                messages: {
                    email : "請輸入 Email Address !",
                    phone : "請使用電話號碼。 !",
                    // project_review_dis_th : "กรุณากรอกรายละเอียด Project Review ไทย !",
                    // community_img : " &nbsp; กรุณาเลือกรูป !",
                    // living_tip_content_th : "กรุณากรอกรายละเอียด Living Tips !",
                    // fileupload_en : " &nbsp; กรุณาเลือกรูป",
                }
            });

            // $("#lead_img_file").rules("add", {
            //     required:true,
            //     messages: {
            //         required: " &nbsp; ไม่มีรูป !"
            //     }
            // });


            $('#phone').keydown(function(event){
                var max = 10;
                var phone = $(this).val().length;
                
                if(phone >= max){

                    if(event.keyCode == 8 || event.keyCode == 46){

                        return true;
                    
                    }
                        
                        return false;
                    
                }
            });

            $('#reportform').on('submit', function(){
                 var googleResponse1 = $('#g-recaptcha-response-1').val();
                if(googleResponse1 ==''){   
           
                     $('#chacheck1').show();
                     return false;
           
                }else{

            
                    $('#chacheck1').hide();
                }


        })



        });
    </script>


<?php include('footer.php'); ?>
