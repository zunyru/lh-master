<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

$home_sell_id = $_GET['home_sell_id'];

if(empty($home_sell_id) || getHomeSellByHomeSellID($home_sell_id) == false){
    header('Location: '.Helper::url_string('furnished-home.php'));
}


$home_sell_imgs_banner = getHomeSellImgByHomeSellID_Banner($home_sell_id);

$home_sell      = getHomeSellByHomeSellID($home_sell_id);
$project_sub    = getProjectSubBySubID($home_sell->project_sub_id);
$planImg        = getPlanByPlanID($home_sell->plan_id);
$home_sell_imgs = getHomeSellImgByHomeSellID($home_sell_id);
$project = getProjectByID($project_sub->project_id);
$zone = getZoneByID($project->zone_id);
$product_arr= Helper::getArrayProductOfProject($project_sub->project_id);
$conditionHomeSell = getConditionHomeSellByHomeSellID($home_sell_id);



$title_page = "บ้านตกแต่งพร้อมขาย Land & Houses";
$description_page = "แบบบ้าน ".$planImg->plan_name_th ." ".$home_sell->features_convert." ราคา ".Helper::getProjectPrice($conditionHomeSell->total_price)." ล้านบาท";
if($planImg != false) {
                $imgBanners     = [];
                foreach ($home_sell_imgs_banner as $home_sell_img) {
                    $buff['path']     = $home_sell_img->path_funished;
                    $buff['seo']      = $home_sell_img->alt_seo;
                    $buff['folder_name'] = $home_sell_img->folder_name;
                    $imgBanners[]     = $buff;
                }
$image_page = str_replace('fileupload/galery_master/'.$imgBanners[0]['folder_name'].'/', 'http://www.lh.co.th/www/Backend/fileupload/galery_master/'.$imgBanners[0]['folder_name'].'/Thumbnails_',$imgBanners[0]['path']);

            }else{
                $image_page = str_replace('fileupload/images/galery_plan/', 'http://www.lh.co.th/www/Backend/fileupload/images/galery_plan/Thumbnails_',$planImg->plan_img);
            }

?>
<?php include('header.php'); ?>

    
    <!-- CSS -->
    <link rel="stylesheet" href="<?= file_path('css/project-info-furnished.css') ?>" type="text/css">
    <link rel="stylesheet" href="<?= file_path('tablesaw/tablesaw.css') ?>" type="text/css">
    <!-- JS -->
     <script src="<?= file_path('tablesaw/tablesaw.jquery.js') ?>"></script>
    <script src="<?= file_path('tablesaw/tablesaw-init.js') ?>"></script>
    <!-- <script src="<?php //echo file_path('js/project-info-furnished.js') ?>"></script> -->

    <div class="page-banner page-scroll">
        <span id="scrollNext" class="i-scroll-next"></span>
        <?php
            $home_sell      = getHomeSellByHomeSellID($home_sell_id);
            $project_sub    = getProjectSubBySubID($home_sell->project_sub_id);
            $planImg        = getPlanByPlanID($home_sell->plan_id);
            $home_sell_imgs = getHomeSellImgByHomeSellID($home_sell_id);
            $project = getProjectByID($project_sub->project_id);
            $zone = getZoneByID($project->zone_id);
            $product_arr= Helper::getArrayProductOfProject($project_sub->project_id);
        ?>
            
        <div id="bannerSlide">
            <?php
            if($planImg != false) {
                $imgBanners     = [];
                foreach ($home_sell_imgs_banner as $home_sell_img) {
                    $buff['path']     = $home_sell_img->path_funished;
                    $buff['seo']      = $home_sell_img->alt_seo;
                    $imgBanners[]     = $buff;
                }
                foreach ($imgBanners as $imgBanner){
                    ?>
                    <div class="item">
                        <div class="banner-container banner-parallax">
                            <img src="<?php echo backend_url('base',$imgBanner['path']) ?>"
                                 alt="<?= $imgBanner['seo'] ?>"
                            >
                        </div>
                    </div>
            <?php
                }
            }
            ?>
        </div>
    </div>

    <?php
        $plan = getPlanByPlanID($home_sell->plan_id);
        $price = getProjectPrice($project_sub->project_id);
        $conditionHomeSell = getConditionHomeSellByHomeSellID($home_sell_id);
        $series = getSeries($plan->series_id);
        $icon_activities = !empty($home_sell->icon_id) ? getIconActivity($home_sell->icon_id) : null;
    ?>

    <div id="content" class="content project-info-furnished-page">
        <div class="container">

             <div class="project-info-detail-block">
                <?php if(!empty($icon_activities)) {
                    if($icon_activities->icon_img !=''){
                        if($home_sell->home_sell_status != 'SO') {
                    ?>
                    <div class="logo-150"><img src="<?= backend_url('icon_activity',$icon_activities->icon_img) ?>" alt=""></div>
                <?php
                } } }?>

                <div class="row">
                    <?php if($home_sell->home_sell_status == 'SO') {
                    ?>
                    <div class="tagbox2_sold soldout-tag " style="position: absolute;margin-left: auto;right: 0;">
                        <div class="tag2">賣<br>光了</div>
                    </div>
                    <?php
                }?>
                    <div class="col-md-12">
                      <!-- H1 -->
                    <?php 
                      $SEO = getSEOUrl($actual_link);
                      if($actual_link == @$SEO->url_page){
                        echo '<h1>'.$SEO->h1.'</h1>';
			} 
                    ?>
                    <!-- End H1 -->
                    <div class="project-sale-logo">
                        <img src="<?= backend_url('series_logo',$series->series_logo) ?>" alt="">
                          <?php if(!empty($plan->plan_name_th)) { ?> <p class="title"><strong>住房类型 <span class="detail"> <?= $plan->plan_name_th ?> </span></strong> </p><?php } ?>
                          <?php if(!empty($plan->plan_code)) { ?> <p class="title"> 地皮编号 <span class="detail"> <?= $home_sell->number_converter ?> </span></p><?php } ?>
                          <?php if(!empty($home_sell->features_convert)) { ?> <p class="title hidden"> 地皮情况 </p><p class="detail hidden" id="features">
                           <?=!empty($home_sell->features_convert ) ? str_replace(array("\n\r", "\n","n/g"), '<br/>', $home_sell->features_convert ) : ''?>
                             
                               </p></p><?php } ?>
                          <?php if(!empty($plan->plan_feature_th)) { ?>
                              <p class="title"> 独特地皮的情况
                              <span class="detail detail-floor">
                               <?=!empty($plan->plan_feature_th ) ? str_replace(array("\n\r", "\n","n/g"), '<br/>', $plan->plan_feature_th ) : ''?>
                                </span>
                              </p><?php } ?>
                        <p class="title green price">价格 <?= Helper::getProjectPrice($conditionHomeSell->total_price) ?> 万泰铢</p>

                    </div>
                    </div>

                    <?php
                    $planFunctions     = getFunctionHomeSell($home_sell_id);
                    $planFunctions_parking = getFunctionHomeSell_Parking($home_sell_id);
                    $leftPlanFunction  = [];
                    $rightPlanFunction = [];
                    $leftPlanFunction[] = $plan->plan_useful;
                    $leftPlanFunction[] = $conditionHomeSell->land_size;
                    $leftPlanFunction[] = $planFunctions_parking[0];
                    //$conditionHomeSell->land_size;

                    foreach ($planFunctions as $i => $planFunction)
                    {
                        $leftPlanFunction[] = $planFunction;
                    }

                    ?>

                    <?php
                    if(in_array(1,$product_arr) || in_array(2,$product_arr) || !in_array(3,$product_arr)){
                        $url_see_more = isLadawan($project->project_id) ?
                            $router->generate('ladawan-detail',[
                                'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                'product_id'    =>  1
                            ]) :
                            $router->generate('single-home-detail',[
                                'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                'product_id'    =>  1
                            ]);
                    }
                    else{
                        $url_see_more = isLadawan($project->project_id) ?
                            $router->generate('ladawan-detail',[
                                'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                'product_id'    =>  3
                            ]) :
                            $router->generate('condominium-detail',[
                                'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                'lang'          =>  'th'
                            ]);
                    }
                    ?>

                    <div class="col-md-6">
                        <div class="content-style txt-22">
                            <a href="<?= $url_see_more ?>">
                              <p class="title grey"><?php if($project != false) echo $project->project_name_th ?> 项目</p>
                            </a>
                           <p class="title grey mbr-15">位置 <?php if($zone != false) echo $zone->zone_name_th ?></p>
                            <p class="title grey mbr-15 function-title">住宅功能表</p>
                            <div class="row">
                                <div class="tb-2col">

                                    <div class="col-md-12 cl">
                                        <table class="content-style tbs1" id="project-table" width="100%">
                                            <tbody>
                                            <?php  foreach ($leftPlanFunction as $i => $planFunction) { ?>
                                                <tr>
                                                    <td><?php if($i == 0){echo '用地面积'; }elseif($i == 1){echo '地皮面积';}elseif($i==2){echo '停车位';}else{ echo $planFunction->function_plan_name_th;}?></td>
                                                    <td><?php if($i == 0){echo $planFunction; }elseif($i == 1){ echo $conditionHomeSell->land_size;}elseif($i==2){echo $planFunctions_parking[0]->function_plan_sub_plan_count_room;}else{echo $planFunction->function_plan_sub_plan_count_room; }?></td>
                                                    <td><?php if($i == 0){echo '平方米以上';}elseif($i == 1){echo '搭朗瓦';}elseif($i==2){echo '个';}else{ echo $planFunction->function_plan_pronoun; }?></td>
                                                </tr>
                                            <?php }?>
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" style="margin-top: 54px;">
                        <div class="content-style txt-22 mtr-15">
                            <p class="title grey"> 其他详情</p>
                            <div class="row">

                                <?php
                                $peculalitys = getPeculality($home_sell_id);
                                $leftPeculality  = [];
                                $rightPeculality = [];
                                foreach ($peculalitys as $i => $peculality)
                                {
                                    if($i%2 == 0){
                                        $leftPeculality[] = $peculality;
                                    }elseif ($i%2 == 1){
                                        $rightPeculality[] = $peculality;
                                    }
                                }
                                ?>

                                <div class="col-md-6">
                                    <ul class="tbs1">
                                        <?php
                                        foreach ($leftPeculality as $peculality){
                                            ?>
                                            <li><?= $peculality->peculiarity_name_th ?></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>

                                <div class="col-md-6">
                                    <ul class="tbs1">
                                        <?php
                                        foreach ($rightPeculality as $peculality){
                                            ?>
                                            <li><?= $peculality->peculiarity_name_th ?></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php
            $floor_plans = getHomeSellFloorPlan($home_sell_id);
            if($floor_plans != false){

            ?>
            <div class="project-info-detail-block">
                <p class="heading-title">房屋平面图</p>

                <div id="homePlanSlide">

                            <?php
                            foreach ($floor_plans as $i => $floor_plan){
                                ?>
                                    <div class="item">
                                        <div class="row">
                                                <div class="col-md-6">
                                                    <div class="project-info-plan-img">
                                                        <a href="#" data-featherlight="<?= backend_url('base',$floor_plan->floor_plan_img_name)?>">
                                                            <img src="<?= backend_url('base',$floor_plan->floor_plan_img_name)?>" alt="">
                                                            <span class="i-zoom"></span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="tab-dd dropdown">
                                                        <div class="row">
                                                            <ul class="tabs-menu-plan-slide dropdown-menu" aria-labelledby="dropdownMenu1">
                                                                <?php
                                                                if(count($floor_plans) >= 1) {
                                                                    ?>
                                                                    <li class="<?= $i == 0 ? 'current' : '' ?>">
                                                                        <a data-value="0">
                                                                            一层
                                                                        </a>
                                                                    </li>
                                                                    <?php
                                                                }
                                                                if(count($floor_plans) >= 2) {
                                                                    ?>
                                                                    <li class="<?= $i == 1 ? 'current' : '' ?>">
                                                                        <a data-value="1">
                                                                            二层
                                                                        </a>
                                                                    </li>
                                                                    <?php
                                                                }
                                                                if(count($floor_plans) >= 3) {
                                                                    ?>
                                                                    <li class="<?= $i == 2 ? 'current' : '' ?>">
                                                                        <a data-value="2">
                                                                            三层
                                                                        </a>
                                                                    </li>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="tab">
                                                        <div id="tab-1" class="tab-content">
                                                            <div class="tab-block content-style">
                                                                <ul class="floor_plan<?=$i?>">

                                                                    <?=!empty($floor_plan->floor_plan_dis_th) ? '<li>'.str_replace(array("\n\r", "\n","n/g"), '</li><li>',  $floor_plan->floor_plan_dis_th) : ''; echo '</li>';?>

                                                                </ul>
                                                                <ul style="color: #999999;font-size: 15px;">
                                                                   备注：平面图上的数值为近似值，为了引导客户了解房屋细节以及样式，您可以到公司项目所在地实地考察，公司保留材料与细节变更的权利，变更将不另行通知
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                        </div>
                                    </div>
                                <?php
                            }
                            ?>

                </div>

            </div>

            <?php
            }
            ?>





            <?php
            $home_sell_gallery  = getGalleryHomeSellType($home_sell_id);
            $galleryHomeSells   = [];
            if($home_sell_gallery != false){
                switch ($home_sell_gallery->type_galery) {
                    case 1:
                        $original_gallerys  = getGalleryHomeSellTypeUpload($home_sell_id);
                        foreach ($original_gallerys as $original_gallery){
                            $buff['path'] = $original_gallery->path_upload_gallery;
                            $buff['desc'] = $original_gallery->dis_th;
                            $buff['seo']  = $original_gallery->alt_seo;
                            $galleryHomeSells[] = $buff;
                        }
                        break;
                    case 2:
                        $original_gallerys  = getHomeSellGalleryPlan($home_sell_id);
                        foreach ($original_gallerys as $original_gallery){
                            $buff['path'] = $original_gallery->galery_plan_name;
                            $buff['desc'] = $original_gallery->galery_plan_name_dis;
                            $buff['seo']  = $original_gallery->galery_plan_img_seo;
                            $galleryHomeSells[] = $buff;
                        }
                        break;
                    case 3:
                        $original_gallerys  = getGalleryHomeSellTypeFurnishedHome($home_sell_id);
                        foreach ($original_gallerys as $original_gallery){
                            $buff['path'] = $original_gallery->path_funished;
                            $buff['desc'] = $original_gallery->dis_th;
                            $buff['seo']  = $original_gallery->alt_seo;
                            $galleryHomeSells[] = $buff;
                        }
                        break;
                }
            }
            ?>

            <?php
            if($home_sell_gallery != false){
            ?>
                <div class="gallery-block">
                    <p class="heading-title">相册</p>
                    <div class="tpl5-img">
                        <div id="tpl5-img">
                            <?php
                                foreach ($galleryHomeSells as $i => $galleryHomeSell){
                                    ?>
                                    <div class="item tpl5-img-<?= $i+1 ?>">
                                        <a class="gallery" href="<?= backend_url('base', $galleryHomeSell['path']) ?>">
                                            <img src="<?= backend_url('base', $galleryHomeSell['path']) ?>"
                                                 alt="<?= $galleryHomeSell['seo'] ?>">
                                            <p class="title-gallery"><?= $galleryHomeSell['desc'] ?></p>
                                        </a>
                                    </div>
                            <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>

    <!--condition-->
<?php
$conditionHomeSell = getConditionHomeSellByHomeSellID($home_sell_id);
?>
    <div class="container">
    <div id="sec-tabledetail" class="project-info-detail-block">
        <div class="smartcal-block">
            <p class="heading-title">表格与条件简介</p>
            <!-- <a href="<?= $router->generate('calculator') ?>" target="_blank"
               class="calculate-link"><i class="i-smcal"></i><span class="cal-txt">计算按揭费用</span></a> -->
            <div class="clearfix"></div>
        </div>
        <table class="tablesaw content-style table-1" data-tablesaw-mode="stack" >
            <thead>
            <tr>
                <th class="tb-title-1st" data-tablesaw-sortable-col>住房类型</th>
                <?php if(!empty($conditionHomeSell->land_size)) { ?>
                    <th data-tablesaw-sortable-col>用地规模</th>
                <?php } if(!empty($conditionHomeSell->total_price)){ ?>
                    <th data-tablesaw-sortable-col>价格</th>
                <?php } if(!empty($conditionHomeSell->down && !empty($conditionHomeSell->money))){ ?>
                    <th data-tablesaw-sortable-col>首付 <?= $conditionHomeSell->down ?> %</th>
                <?php } if(!empty($conditionHomeSell->payments)){ ?>
                    <th data-tablesaw-sortable-col>定金</th>
                <?php } if(!empty($conditionHomeSell->contract)){ ?>
                    <th data-tablesaw-sortable-col>签合同</th>
                <?php } if(!empty($conditionHomeSell->transfer_money)){ ?>
                    <th data-tablesaw-sortable-col>转账金额</th>
                <?php } if(!empty($conditionHomeSell->starting_pay)){ ?>
                    <th data-tablesaw-sortable-col>每月最低按揭费用</th>
                <?php } if(!empty($conditionHomeSell->interest)){ ?>
                    <th data-tablesaw-sortable-col>利息 %</th>
                <?php } if(!empty($conditionHomeSell->repayment_period)){ ?>
                    <th data-tablesaw-sortable-col>年</th>
                <?php } ?>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="tb-plan-detail">
                    <?= $plan->plan_name_th ?>
                </td>
                <?php if(!empty($conditionHomeSell->land_size)) { ?>
                    <td><?= $conditionHomeSell->land_size ?> 搭朗瓦</td>
                <?php } if(!empty($conditionHomeSell->total_price)){ ?>
                    <td><?= Helper::groupThousand($conditionHomeSell->total_price) ?> 泰铢</td>
                <?php } if(!empty($conditionHomeSell->down) && !empty($conditionHomeSell->money)) {?>
                    <td><?= Helper::groupThousand($conditionHomeSell->money) ?> 泰铢</td>
                <?php } if(!empty($conditionHomeSell->payments)) {?>
                    <td><?= Helper::groupThousand($conditionHomeSell->payments) ?> 泰铢</td>
                <?php } if(!empty($conditionHomeSell->contract)) { ?>
                    <td><?= Helper::groupThousand($conditionHomeSell->contract) ?> 泰铢</td>
                <?php } if(!empty($conditionHomeSell->transfer_money))  { ?>
                    <td><?= Helper::groupThousand($conditionHomeSell->transfer_money) ?> 泰铢</td>
                <?php } if(!empty($conditionHomeSell->starting_pay)) {?>
                    <td><?= Helper::groupThousand($conditionHomeSell->starting_pay) ?> 泰铢</td>
                <?php } if(!empty($conditionHomeSell->interest)) {?>
                    <td><?= $conditionHomeSell->interest ?> %</td>
                <?php } if(!empty($conditionHomeSell->repayment_period)) {?>
                    <td><?= $conditionHomeSell->repayment_period ?> 年</td>
                <?php } ?>
            </tr>
            </tbody>
        </table>
        <p class="txt-remark"> * 请再次核对项目条件与价格．<br>
           ** 关于利率， 请再次与使用该服务的银行核对</p>
        <br>

        <a style="width: 30%" href="<?= $url_see_more ?>" class="btn btn-seemore f-btn-destop">
             浏览更多的房产项目资料
        </a>
        <a style="width: 95%" href="<?= $url_see_more ?>" class="btn btn-seemore f-btn-mobile">
             浏览更多的房产项目资料
        </a>

    </div>
    </div>


<?php include('footer.php'); ?>
<script type="text/javascript">
    $(document).ready(function () {
    //Page Load Start
    bannerSlide();
    gallery();


    var winW = $(window).width();

    if( winW > 768 ) {
        homePlanSlide();
        colSlide();
    }


    //Page Load End
    $('.gallery').featherlightGallery();

    $(document).ready(function()
    {
        $(document).resize();
    });
});


$(window).load(function () {
    var winW = $(window).width();

    if( winW <= 768 ) {
        homePlanSlide();
        colSlide();
    }
});

//Function Start
function bannerSlide() {
    var winH = $(window).height();
    //$('.banner-parallax').css('height', winH - 60);

    var isMulti = ($('#bannerSlide .item').length > 1) ? true : false
    $('#bannerSlide').owlCarousel({
        loop:isMulti,
        autoplay: true,
        autoplayTimeout: 4000,
        autoplaySpeed: 1000,
        margin:5,
        animateOut: 'fadeOut',
        nav:isMulti,
        dots: isMulti,
        video:true,
        lazyLoad:true,
        center:true,
        responsive:{
            0:{
                items:1
            },

        }
    });
}

function gridLayout5() {
    var wall = new Freewall("#tpl5-img");
    wall.reset({
        selector: '.item',
        animate: true,
        cellW: 300,
        cellH: 'auto',
        onResize: function() {
            wall.fitWidth();
        }
    });

    var images = wall.container.find('.item');
    images.find('img').load(function() {
        wall.fitWidth();
    });
}

function gallery() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        // var stagePadding = 40;
        $('#tpl5-img').owlCarousel({
            loop:true,
            margin: 30,
            //stagePadding: stagePadding,
            autoplay:true,
            autoplayTimeout:3000,
            autoplayHoverPause:true,
            nav:true,
            dots: false,
            autoHeight: true,
            responsive:{
                0:{
                    items:1
                },
                768:{
                    items:1
                }
            }
        });
    }
    else {
        gridLayout5();
    }
}


function homePlanSlide() {

    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        var stagePadding = 0;
    }
    else {
        var stagePadding = 0;
    }

    var isMulti = ($('#homePlanSlide .item').length > 1) ? true : false;
    var homeSlider = $("#homePlanSlide");
    homeSlider.owlCarousel({
        loop:isMulti,
        margin: 10,
        stagePadding: stagePadding,
        nav:isMulti,
        dots: isMulti,
        autoHeight: false,
        responsive:{
            0:{
                items:1
            }
        }
    });
    homeSlider.on("click", ".tabs-menu-plan-slide a", function () {
        var value = $(this).attr('data-value');
        homeSlider.trigger('to.owl.carousel', [value, 'fade']);
    });
}

function colSlide() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        var stagePadding = 25;
        var margin = -10;
        var autoH = true;
    }
    else {
        var stagePadding = 0;
        var margin = 10;
        var autoH = false;
    }

    var isMulti = ($('#colSlide .item').length > 1) ? true : false
    $('#colSlide').owlCarousel({
        loop:isMulti,
        margin: margin,
        stagePadding: stagePadding,
        nav:isMulti,
        dots: isMulti,
        autoHeight: autoH,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:1
            },
            979:{
                items:2
            },
            1199:{
                items:3
            }
        }
    });
}


//Function End
</script>