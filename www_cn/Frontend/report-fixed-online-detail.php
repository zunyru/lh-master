<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>
<?php include('header.php'); ?>
<!-- CSS -->
<link rel="stylesheet" href="css/report-fixed-detail.css" type="text/css">
<!-- JS -->
<script src="js/report-fixed.js"></script>

<div id="content" class="content">
    <div class="container">

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div>
                    <h1 class="heading-title">แจ้งซ่อมออนไลน์</h1>
                    <div class="complain-form-block">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="title">ขอบข่ายรับประกัน และการให้บริการงานซ่อม</p>
                                <div class="form">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p>หลังจากที่ท่านได้รับโอนกรรมสิทธิ์เป็นที่เรียบร้อยแล้ว บริษัทฯ ยินดีรับประกัน และให้บริการแก้ไข ความชำรุดบกพร่องที่อาจเกิดขึ้น </p>

                                                <p>สามารถติดต่อขอรับบริการได้ที่ส่วนบริการลูกค้า ประจำโครงการ โดยมีรายละเอียดขอบเขตการรับประกัน ดังนี้</p>
                                                <ul class="bullet-list">
                                                    <li>รับประกัน ระยะเวลา 5 ปี สำหรับโครงสร้างอาคาร อันได้แก่ เสาเข็ม ฐานราก เสา คาน พื้น(ไม่รวมการฉาบผิว ซึ่งอาจ แตกร้าวลายงา รับประกัน 1 ปี) โครงหลังคา และผนังที่รับน้ำหนัก (ผนังที่ทำด้วยคอนกรีตเสริมเหล็ก)</li>
                                                    <li>รับประกัน ระยะเวลา 1 ปี สำหรับส่วนควบ และอุปกรณ์อันเป็นส่วนประกอบที่สำคัญของอาคาร อาทิ ผนังก่อฉาบทั่วไป ระบบไฟฟ้า ระบบสุขาภิบาล การรั่ว การซึม ของน้ำฝนเข้าตามวงกบ ประตู หน้าต่าง</li>
                                                    <li>รับประกัน ระยะเวลา 1 ปี สำหรับรั้ว กำแพง (เฉพาะความชำรุดเสียหายที่เกิดกับโครงสร้าง ไม่รวมถึง ประตู อุปกรณ์ รางเลื่อน บานพับ สี และส่วนอื่นๆ ที่เกิดสนิม)</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="complain-form-block">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="title">กรณีดังต่อไปนี้ อยู่นอกเหนือการรับประกัน</p>
                                <div class="form">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="bullet-list">
                                                        <li>เกิดจากการต่อเติมอาคารภายหลังโอนกรรมสิทธิ์, การใช้งานที่ผิดไปจากปกติวิสัย หรือแก้ไขข้อบกพร่องที่มิได้ดำเนินการโดยช่างของบริษัทฯ</li>
                                                        <li>เกิดจากภัยธรรมชาติ หรืออุบัติเหตุ, การเสื่อมสภาพจากการใช้งาน</li>
                                                        <li>เกิดจากการติดตั้งอุปกรณ์เครื่องใช้ ที่ลูกค้าดำเนินการเอง</li>
                                                        <li>กรณีโอนกรรมสิทธิ์ให้กับบุคคลอื่น หรือไม่ได้เข้าพักอาศัยเกินกว่า 3 เดือน</li>
                                                        <li>การทรุดตัว การแตกร้าว ของพื้นจอดรถ พื้นลานซักล้าง พื้นดินทั่วไป</li>
                                                        <li>สวน การตกแต่งสวน องค์ประกอบของสวน</li>
                                                        <li>หลอดไฟ และอุปกรณ์</li>
                                                        <li>งานตกแต่งภายใน ภายนอก</li>
                                                    </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="complain-form-block">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="title">หมายเหตุ</p>
                                <div class="form">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="bullet-list">
                                                    <li>กรณีความชำรุดบกพร่อง มีผลทำให้ส่วนอื่นๆ ได้รับความเสียหาย บริษัทฯ ยินดีซ่อมแซมให้ เฉพาะส่วนที่เป็นวัสดุ อุปกรณ์ ตามที่บริษัทฯ ได้ส่งมอบให้กับลูกค้า</li>
                                                    <li>วัสดุ อุปกรณ์ เครื่องใช้ บางชนิด เป็นการรับประกันของเจ้าของผลิตภัณฑ์หรือตัวแทนจำหน่าย รายละเอียด และเงื่อนไขของระยะเวลาประกันจะแตกต่างกัน ท่านสามารถติดต่อขอรับบริการได้โดยตรงกับเจ้าของผลิตภัณฑ์หรือตัวแทนจำหน่าย</li>
                                                    <li>วิธีซ่อมแซมในความชำรุดบกพร่อง บริษัทฯสงวนสิทธิ์ที่จะดำเนินการแก้ไขตามมาตรฐานของบริษัทฯ</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br><br>
                    <div class="complain-form-block">
                        <div class="report-btns">
                            <a href="" class="btn btn-submit">Accept</a>
                            <a href="" class="btn btn-submit btn-deny">Deny</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>


<?php include('footer.php'); ?>
