<?php
header('Location: //'.$_SERVER['SERVER_NAME'].'/'.'en');
exit();

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';
require_once 'include/help/query_independent.php';
require_once 'include/help/Helper.php';

$living_tip_id = $_GET['living_tip_id'];

if(empty($living_tip_id) || getLivingTipDetail($living_tip_id) == false){
    echo 'not have a project for this project id';
}

$living_tip = getLivingTipDetail($living_tip_id);

$other_living_tips = getLivingTip_other($living_tip_id);

$title_page = $living_tip->living_tip_name_th;
$description_page = $living_tip->living_tip_content_th;

$image_page = str_replace('fileupload/images/living_tips/','https://www.lh.co.th/www_cn/Backend/fileupload/images/living_tips/Thumbnails_', $living_tip->living_tip_img);

$pages = 'LH_LIVING_TIPS';
$pages_id = $living_tip_id;

?>
<?php include('header.php'); ?>
    <!-- CSS -->
    <link rel="stylesheet" href="<?= file_path('css/review-details.css') ?>" type="text/css">
    <!-- JS -->
    <script src="<?= file_path('js/review-details.js') ?>"></script>


<div class="page-nav">
    <div class="container">
        <?php
        $SEO = getSEOUrl($actual_link);
        if($actual_link == @$SEO->url_page){
            echo '<h1 >'.$SEO->h1.'<h1>';
        }else{
            if(empty($living_tip->h1)){
               echo '<h1>LH LIVING TIP</h1>'; 
           }else{
            echo "<h1>".$living_tip->h1."</h1>";
        } 
        
    }
    ?>
    <br>
    <p class="title" style="margin: 0;"><?= $living_tip->living_tip_name_th ?></p>
</div>
</div>

    <div id="content" class="content">
        <div class="container">

            <div class="review-content">

                <div class="content-block">

                  <?= Helper::lang('th', str_replace('../build/images/file_content_web/', '/www_cn/build/images/file_content_web/',  $living_tip->living_tip_dis_content) ,
                        str_replace('../build/images/file_content_web/', '/www_cn/build/images/file_content_web/',  $living_tip->living_tip_dis_content_en) ) ?>
                </div>

            </div>

            <div class="">
                <p class="heading-title">เรื่องอื่นๆ ที่น่าสนใจ</p>
                <div class="row">
                    <div id="relatePost" class="col-slide">
                        <?php
                        foreach ($other_living_tips as $other_living_tip){
                            if(isset($other_living_tip->custom_url)){
                                $url_name = $other_living_tip->custom_url;
                            }else{
                                $url_name =$other_living_tip->living_tip_name_th;
                            }
                            ?>
                            <div class="item">
                                <div class="relate-post">
                                    <div class="review-img">
                                    <?php
                                         $living_tip_img = str_replace('fileupload/images/living_tips/','fileupload/images/living_tips/Thumbnails_',$other_living_tip->living_tip_img);
                                    ?>
                                        <img src="<?= backend_url('base',$living_tip_img) ?>" alt="">
                                    </div>
                                    <a href="<?= $router->generate('living-tips-detail',['living_tip_name' => str_replace(' ','-',$other_living_tip->living_tip_name_th) ]) ?>">
                                        <p class="title"><?= $other_living_tip->living_tip_name_th ?></p>
                                        <p><?= $other_living_tip->living_tip_content_th ?></p>
                                    </a>
                                    <a href="<?= $router->generate('living-tips-detail',['living_tip_name' => str_replace(' ','-',$url_name) ]) ?>" class="btn btn-seemore">See more</a>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>

            <a href="javascript:history.back()" class="btn-backtopage">< Back</a>
        </div>
    </div>

    <script type="text/javascript">
        $( document ).ready(function() {
            $('video').attr("poster","https://www.lh.co.th/www_cn/Frontend//images/logo/vdo_backgroup.png");
            // $('video').attr("muted","muted");
            // $('video').attr("playsinline","playsinline");
        });
    </script>

<?php include('footer.php'); ?>
