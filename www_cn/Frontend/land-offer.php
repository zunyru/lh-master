<?php
header('Location: //'.$_SERVER['SERVER_NAME'].'/'.'en');
exit();

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

$page = 'offer';
$page_index = 0 ;
$img = '';
?>
<?php include('header.php'); ?>
    <!-- CSS -->
    <link rel="stylesheet" href="<?= file_path('css/land-offer.css') ?>" type="text/css">

    <div class="page-banner-md banner-hide">
        <div id="bannerSlide">
            <div class="item">
                <div class="banner-parallax" style="background-image: url(<?= file_path('images/temp2/banner_land_offer.jpg') ?>);">
                    <img src="<?= file_path('images/temp2/banner_land_offer.jpg') ?>" alt="">
                </div>
            </div>
        </div>
    </div>

<div id="content" class="content rps-content-pd">
    <div class="container">
        
        <?php
        $SEO = getSEOUrl($actual_link);
        if($actual_link == @$SEO->url_page){
            echo '<h1 class="heading-title">'.$SEO->h1.'</h1>';
        }else{
           echo '<h1 class="heading-title">เสนอขายที่ดิน</h1>'; 
       }
       ?>
       <div class="land-offer-details">
        <div class="row">
            <div class="col-md-4 mbr-15">
                <p class="title green">เอกสารประกอบในการนำเสนอขายที่ดิน</p>
                <ol>
                    <li>แผนที่ตั้งของที่ดินอย่างชัดเจน พร้อมระบุ<br>
                    เนื้อที่รวมทั้งหมด และเราคาที่นำเสนอขาย</li>
                    <li>สำเนาโฉนดที่ดิน</li>
                    <li>ระวางที่ดิน หรือ รูปที่ดิน</li>
                    <li>สำเนาบัตรประชาชนของผู้นำเสนอขายที่ดิน</li>
                </ol>

                    </div>
                    <div class="col-md-4 mbr-15">
                        <p class="title green">รายชื่อเจ้าหน้าที่</p>
                        <ol>
                            <li>คุณภัครา เรืองกุลกวิน</li>
                            <li>คุณปริญญา กิจนะวันชัย</li>
                            <li>คุณศริวรรณ สินทรัพย์</li>
                            <li>คุณมงคล มั่นทองขาว</li>
                         </ol>
                    </div>
                    <div class="col-md-4 mbr-15">
                        <p class="title green">เวลาในการนำส่งเอกสาร</p>
                        จันทร์ - ศุกร์ เวลา 9.00น. ถึง 17.00น.<br>
                        ยกเว้นวันหยุดของบริษัท

                        <p class="title green mbr-15">สถานที่นำส่งเอกสาร</p>
                        บริษัท แลนด์ แอนด์ เฮ้าส์ จำกัด (มหาชน) สำนักงานใหญ่<br>
                        เลขที่ 1 ชั้น 38 อาคารคิวเฮ้าส์ ลุมพินี ถนนสาทรใต้<br>
                        แขวงทุ่งมหาเมฆ เขตสาทร กรุงเทพมหานคร<br>
                        โทร. 0-2343-8900 ต่อฝ่ายที่ดิน
                    </div>
                </div>
            </div>

            <div class="land-offer-map">
                <h1 class="heading-title">แผนที่ในการจัดส่งเอกสาร</h1>

                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <a href="#" data-featherlight="<?= file_path('images/temp2/land_offer_1.jpg') ?>">
                            <img src="<?= file_path('images/temp2/land_offer_1.jpg') ?>" alt="">
                        </a>

                    </div>
                    
                </div>
            </div>
        </div>
    </div>


<?php include('footer.php'); ?>
