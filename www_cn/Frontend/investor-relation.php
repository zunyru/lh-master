<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>

<?php include('header.php'); ?>
<!-- CSS -->
<link rel="stylesheet" href="css/investor.css" type="text/css">


<div id="content" class="content invest-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">

                <h1>นักลงทุนสัมพันธ์</h1>

                <div class="row">
                    <div class="col-md-5">
                        <ul class="invest-list">
                            <li><a href=""><i class="inv-budget"></i>งบการเงิน</a></li>
                            <li><a href=""><i class="inv-report"></i>รายงานประจำปี 2558</a></li>
                            <li><a href=""><i class="inv-calendar"></i>ปฎิทินนักลงทุน</a></li>
                        </ul>
                    </div>
                    <div class="col-md-offset-1 col-md-6">
                        <ul class="invest-list">
                            <li><a href=""><i class="inv-meeting"></i>ขอเชิญเสนอวาระการประชุมสามัญผู้ถือหุ้น <i class="download"></i></a></li>
                            <li><a href=""><i class="inv-claim"></i>ข้อกำหนดสิทธิ LH-W3<i class="download"></i></a></li>
                            <li><a href=""><i class="inv-form"></i>แบบแสดงความจำนง LH-W3<i class="download"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include('footer.php'); ?>
