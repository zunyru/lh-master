<?php
//get id from parent
$id=$_GET["id"];

//do something with this id for get the data
$job = getJobATTRIBUTE($id);

//
?>
<!-- Modal Header -->
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">
        <span aria-hidden="true">&times;</span>
        <span class="sr-only">Close</span>
    </button>
    <h4 class="modal-title" id="myModalLabel">
    	ตำแหน่งงาน : <span style="color: #6f9755;"><?=$job->TITLE;?></span>
    </h4>
</div>
<!-- Modal Body -->
<div class="modal-body">
	<!-- Content HERE -->
	<div class="row">
	  <div class="col-md-12 "><strong>คุณสมบัติ</strong></div>
	</div>		
	<br>
	<div class="row">
		<div class="col-md-11 col-md-offset-1">
		<ul class="desc_floor" >

		        <script type="text/javascript">
		            $('.desc_floor').html( '<li>'+`<?= $job->ATTRIBUTE?>`.replace(/\r?\n/g, '</li><li>') + '</li>')
		        </script>
			
		</ul>
	    </div>
    </div>
    <br>
    <div class="row">
	  <div class="col-md-12 "><strong>พื้นที่ทำงาน :</strong> </div>
    </div>
    <div class="row">
	  <div class="col-md-11 col-md-offset-1"><?=$job->LOCATION_NAME ." ".$job->PROVINCE_NAME;?></div>
    </div>

    <br>
    <div class="row">
	  <div class="col-md-12 "><strong>อัตราที่รับ :</strong> </div>
    </div>
    <div class="row">
	  <div class="col-md-11 col-md-offset-1"><?=$job->RATES ." อัตรา";?></div>
    </div>
</div>
<!-- Modal Footer -->
<div class="modal-footer">

</div>
