<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

$series_id = $_GET['series_id'];

if (empty($series_id) || getHomeSeriesByID($series_id) == false) {
    echo 'not have a home series for this series id';
}

function mssql_escape($str) {
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return str_replace("'", "''", $str);
}

$plan_series = getPlanSeries($series_id);

$series = getHomeSeriesByID($series_id);

//die(ddd($home_series));
//$title_page = "แบบบ้านซีรี่ย์ ".$series->series_name_th ." จาก Land & Houses";
//$description_page = "ชมภาพแบบบ้านซีรี่ย์ ".$series->series_name_th ." พร้อมโครงการที่เปิดขายจาก Land & Houses";
// $title_page = "Land & Houses Public Co.,Ltd";
// $description_page = "业务类型：房屋以及土地销售为主的房地产业务， 项目主要位于曼谷以及周边府和大的城市。 ";
$title_page = $series->series_name_th ;
$description_page = mssql_escape($series->series_des_th);
$image_page = isset($plan_series[0]->plan_img) ? str_replace('fileupload/images/galery_plan/', 'http://www.lh.co.th/www/Backend/fileupload/images/galery_plan/Thumbnails_',$plan_series[0]->plan_img) : '';
$keywords = $series->series_name_th ;
?>
<?php include('header.php'); ?>
    <!-- CSS -->
    <link rel="stylesheet" href="<?= file_path('css/split-home-series.css') ?>" type="text/css">
    <!--JS-->
    <script src="<?= file_path('js/split-home-series.js') ?>"></script>

<div class="container">
    <!-- H1 -->
    <?php 
    $SEO = getSEOUrl($actual_link);
    if($actual_link == @$SEO->url_page){
        echo '<h1 class="heading-title header-margin">'.$SEO->h1.'</h1>'; 
    }
    ?>
    <!-- End H1 -->
</div>    
<div id="content" class="content project-info-page">

        <h1 class="heading-logo"><img src="<?= backend_url('series_logo', $series->series_logo) ?>" alt=""></h1>

        <div class="container">

            <?php
            foreach ($plan_series as $i => $plan_item) {
                $planImg = getGalleryFirstPlan($plan_item->plan_id);
                $lhPlan = getPlanByPlanID($plan_item->plan_id);
                ?>
                <div class="block-margin">
                    <div class="item">
                        <div class="row">
                            <div class="col-md-8 <?= $i % 2 == 0 ? 'pull-left' : 'pull-right' ?>">
                                <img src="<?php if ($planImg != false) echo backend_url('base', $planImg->plan_img) ?>"
                                     alt="<?= $planImg->plan_seo ?>">
                            </div>
                            <div class="col-md-4 pull-left">
                                <div class="promo-descrp">
                                    <p class="title"><?= $lhPlan->plan_name_th ?></p>
                                    <p>编号 <?= $lhPlan->plan_code ?>
                                        <br>
                                        用地面积  <?= $lhPlan->plan_useful ?> 平方米以上 <br>

                                        <?php
                                        $planFunctions     = getPlanFunction($plan_item->plan_id);
                                        $countOtherList    = 0;
                                        foreach ($planFunctions as $index => $planFunction){
                                            ?>
                                            <?php
                                            if($index == 0 || $index == 1 ){
                                                ?>
                                                <?= $planFunction->function_plan_name_th ?>
                                                <?= $planFunction->function_plan_sub_plan_count_room ?>
                                                <?= $planFunction->function_plan_pronoun ?>
                                                <?php
                                            } else{
                                                if($index == 2){
                                                    ?>
                                                    <br>
                                                    <?= $planFunction->function_plan_name_th ?>
                                                    <?= $planFunction->function_plan_sub_plan_count_room ?>
                                                    个
                                                    <?php
                                                }else{
                                                    if($countOtherList%2 == 1){
                                                        echo '/';
                                                    }elseif($countOtherList%2 == 0){
                                                        echo '<br>';
                                                    }
                                                    $countOtherList++;
                                                    ?>
                                                    <?= $planFunction->function_plan_name_th ?>
                                                    <?= $planFunction->function_plan_sub_plan_count_room ?>
                                                    <?= $planFunction->function_plan_pronoun ?>
                                                    <?php
                                                }
                                                ?>

                                                <?php
                                            }
                                            ?>
                                            <?php if($index == 0) {
                                                ?>
                                                /
                                                <?php
                                            }?>
                                        <?php
                                        }
                                        ?>
                                    </p>
                                    <a href="<?= $router->generate('home-series-detail',[
                                        'plan_name'  =>  str_replace(' ','-',$lhPlan->plan_name_th),
                                        'series_name'  =>  str_replace(' ','-',$series->series_name_th),

                                    ]) ?>"
                                       class="btn btn-seemore">更多信息</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

        </div>

        <?php
        $serie = getSeries($series_id);
            ?>
            <div class="container">
                <div class="project-info-other-block block-margin">
                    <div class="row">
                        <?php
                        if (!empty($serie->series_360_thumbnail) && !empty($serie->series_360)) {
                            ?>
                            <div class="col-md-6">
                                <p class="heading-title">ภาพ 360 องศา</p>
                                <div class="project-info-other-img">
                                    <a href="#" data-featherlight="#lb360">
                                        <span class="i-view-360"></span>
                                        <img src="<?= backend_url('base', $serie->series_360_thumbnail) ?>"
                                             alt="" class="img100">
                                    </a>
                                    <iframe class="lightbox vdoview"
                                            src="<?= $serie->series_360 ?>"
                                            width="1000"
                                            height="560" id="lb360" style="border:none;" webkitallowfullscreen
                                            mozallowfullscreen allowfullscreen></iframe>
                                </div>
                            </div>
                        <?php
                        }
                        ?>

                        <?php
                        if (!empty($serie->series_youtube) && !empty($serie->series_youtube_thumbnail)) {
                            ?>
                            <div class="col-md-6">
                                <p class="heading-title">VDO</p>
                                <div class="project-info-other-img">
                                    <?php
                                    if($serie->series_youtube_type == 'youtube') {
                                        ?>
                                        <a href="#" data-featherlight="#lbVdo1">
                                            <span class="i-view-360"></span>
                                            <img src="<?= backend_url('base', $serie->series_360_thumbnail) ?>"
                                                 alt="" class="img100">
                                        </a>
                                        <iframe class="lightbox vdoview"
                                                src="<?= backend_url('base', $serie->series_youtube_thumbnail) ?>"
                                                width="1000"
                                                height="560" id="lbVdo1" style="border:none;" webkitallowfullscreen
                                                mozallowfullscreen allowfullscreen></iframe>
                                    <?php
                                    }elseif($serie->series_youtube_type == 'vdo'){
                                        ?>
                                        <a id="ban_vdo">
                                            <span class="i-view-vdo"></span>
                                            <img src="<?= backend_url('base', $serie->series_youtube_thumbnail) ?>"
                                                 alt="" class="hm-highl">
                                        </a>
                                        <script>
                                            $(document).ready(function () {
                                                $('#ban_vdo').click(function () {
                                                    $('#lbVdo').show();
                                                    $.featherlight($('#lbVdo'),{});
                                                    $('.featherlight-content #video_player')[0].play();
                                                    $('#lbVdo').hide();
                                                });
                                            })
                                        </script>
                                        <div id="lbVdo" style="position:relative;width: 100%;display: none;">
                                            <video id='video_player' preload='none' controls>
                                                <source src="<?= backend_url('base',$serie->series_youtube) ?>" type="video/mp4">
                                            </video>
                                        </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>

            <?php
            $gl_of_series = getGallerySeries($series_id);
            if($gl_of_series != false){
                ?>
                <div class="container">
                    <div class="project-info-detail-block block-margin">
                        <div class="gallery-block block-margin">
                            <p class="heading-title header-margin">แกลอรี</p>
                            <div class="tpl5-img">
                                <div id="tpl5-img">
                                    <?php
                                    foreach ($gl_of_series as $i => $gl_of_serie) {
                                        ?>
                                        <div class="item tpl5-img-<?= $i+1 ?>">
                                            <a class="gallery"
                                               href="<?= backend_url('base', $gl_of_serie->galery_img_path) ?>">
                                                <img src="<?= backend_url('base', $gl_of_serie->galery_img_path) ?>"
                                                     alt="<?= $gl_of_serie->alt_seo ?>">
                                                <p class="title-gallery"><?= $gl_of_serie->dis_th ?></p>
                                            </a>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>

    </div>

<?php include('footer.php'); ?>