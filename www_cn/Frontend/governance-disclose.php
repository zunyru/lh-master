<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>

<?php include('header.php'); ?>
<!-- CSS -->
<link rel="stylesheet" href="css/corporate-all.css" type="text/css">
<link rel="stylesheet" href="css/corporate-menu.css" type="text/css">
<link rel="stylesheet" href="css/governance.css" type="text/css">

<!-- JS -->
<script src="js/corporate-menu.js"></script>

<div id="content" class="content ethic-page corp-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">

                <h1>การเปิดเผยข้อมูลและความโปร่งใส</h1>
                <div id="corpMenu" class="corp-menu-tab">
                    <ul>
                        <li class=""><a href="#corpBlock1">งบการเงิน</a></li>
                        <li><a href="#corpBlock2">รายงานความรับผิดชอบต่อรายงานทางการเงิน</a></li>
                        <li><a href="#corpBlock3">รายงานนโยบายการกำกับดูแลกิจการ</a></li>
                        <li><a href="#corpBlock4">การเปิดเผยบทบาทและหน้าที่ของคณะกรรมการ</a></li>
                    </ul>
                </div>

                <div id="corpBlock" class="corp-block current">
                    <div>
                        <div class="govern-block">
                            <div class="col-govern-50">
                                <h2 class="title-gov">บรรษัทภิบาล : การเปิดเผยข้อมูลและความโปร่งใส</h2>
                                <p>บริษัทได้ตระหนักถึงความสำคัญของการเปิดเผยข้อมูล อย่างเพียงพอ ถูกต้อง ครบถ้วน รวดเร็ว และโปร่งใส บริษัทจึงได้เผยแพร่ข้อมูลที่สำคัญของบริษัททั้งข้อมูลทางการเงินและข้อมูลที่ไม่ใช่ทางการเงิน ผ่านทาง website ของตลาดหลักทรัพย์แห่งประเทศไทย www.set.or.th และ website ของสำนักงานคณะกรรมการกำกับหลักทรัพย์และ ตลาดหลักทรัพย์ www.sec.or.th ตามเกณฑ์ที่ตลาดหลักทรัพย์และสำนักงานคณะกรรมการกำกับหลักทรัพย์กำหนดไว้ นอกจากนี้ข้อมูลสารสนเทศต่างๆที่บริษัทได้เผยแพร่ต่อสาธารณชนและผู้มีส่วนได้เสียแล้ว จะนำมาเผยแพร่รวมไว้ใน website ของบริษัท www.lh.co.th ด้วย โดยมีการปรับปรุงข้อมูลอย่างสม่ำเสมอ เพื่อให้ผู้ใช้สามารถรับข้อมูลข่าวสารได้ทันต่อเหตุการณ์ เข้าถึงได้สะดวก และได้รับประโยชน์สูงสุด</p>
                            </div>
                            <div class="col-govern-50 col-govern-right">
                                <img src="images/governance/govern_disclose1.jpg" alt="" class="">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="">
                            <div class="col-govern-50">
                                <p>นอกจากนี้ บริษัทยังให้ความสำคัญในด้านความสัมพันธ์กับผู้ถือหุ้น ผู้ลงทุนทั่วไป และนักวิเคราะห์ บริษัทจึงกำหนดให้การเผยแพร่ข้อมูลของบริษัทแก่นักลงทุน ทั้งที่เป็นผู้ถือหุ้นและผู้ที่สนใจจะถือหุ้นในอนาคต เป็นอำนาจของกรรมการผู้จัดการที่ได้รับมอบหมายให้ทำหน้าที่เป็น Chief Investor Relations Officer พร้อมทั้งจัดให้มีส่วนงานนักลงทุนสัมพันธ์ เพื่อเป็นตัวแทนในการสื่อสารกับผู้ถือหุ้น นักลงทุน และนักวิเคราะห์หลักทรัพย์ โดยสามารถติดต่อส่วนงานนักลงทุนสัมพันธ์ได้ที่หมายเลขโทรศัพท์ 0-2230-8306 <br>หรือ e-mail address: investor@lh.co.th</p>
                            </div>
                            <div class="col-govern-50 col-govern-right">
                                <p>ทั้งนี้ในรอบปี 2556-2558 เจ้าหน้าที่ระดับสูงของบริษัทรวมถึงส่วนงานนักลงทุนสัมพันธ์ได้ให้ข้อมูล และแจ้งสารสนเทศของบริษัทต่อผู้ถือหุ้น นักลงทุนและนักวิเคราะห์หลักทรัพย์ สรุปเป็นจำนวนครั้ง/ปีได้ดังนี้</p>
                                <table class="tb-disclose" width="100%" border="0">
                                    <tbody>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td><strong>2556</strong></td>
                                            <td><strong>2557</strong></td>
                                            <td><strong>2558</strong></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Road show ในประเทศและต่างประเทศ</strong></td>
                                            <td class="txt-center">10</td>
                                            <td class="txt-center">9</td>
                                            <td class="txt-center">7</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Analyst Meeting </strong></td>
                                            <td class="txt-center">5</td>
                                            <td class="txt-center">5</td>
                                            <td class="txt-center">5</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Company Visit</strong></td>
                                            <td class="txt-center">107</td>
                                            <td class="txt-center">60</td>
                                            <td class="txt-center">57</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Conference Call</strong></td>
                                            <td class="txt-center">8</td>
                                            <td class="txt-center">7</td>
                                            <td class="txt-center">8</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="corpBlock1" class="corp-block">
                    <div>
                        <div class="">
                            <div class="col-govern-50">
                                <h2 class="title-gov title-gov-p2">งบการเงิน</h2>
                                <p>งบการเงินที่นำมาเปิดเผยต่อผู้มีส่วนได้เสียต้องต้องผ่านความ เห็นชอบจากคณะกรรมการตรวจสอบ และคณะกรรมการบริษัทก่อน</p>
                            </div>
                            <div class="col-govern-50 col-govern-right">
                                <img src="images/governance/govern_disclose2.jpg" alt="" class="">
                            </div>
                        </div>
                    </div>
                </div>

                <div id="corpBlock2" class="corp-block">
                    <div>
                        <div class="">
                            <div class="col-govern-50">
                                <h2 class="title-gov title-gov-p2">รายงานความรับผิดชอบต่อรายงานทางการเงิน</h2>
                                <p>คณะกรรมการบริษัทได้จัดให้มีรายงานความรับผิดชอบของคณะกรรมการต่อรายงานทางการเงิน แสดงควบคู่กับรายงานของผู้สอบบัญชีในรายงานประจำปี ตั้งแต่ปี 2549 เป็นต้นมา</p>
                            </div>
                            <div class="col-govern-50 col-govern-right">
                                <img src="images/governance/govern_disclose3.jpg" alt="" class="">
                            </div>
                        </div>
                    </div>
                </div>

                <div id="corpBlock3" class="corp-block">
                    <div>
                        <div class="">
                            <div class="col-govern-50">
                                <h2 class="title-gov title-gov-p2">รายงานนโยบายการกำกับดูแลกิจการ</h2>
                                <p>บริษัทได้รายงานนโยบายการกำกับดูแลกิจการที่ได้ให้ความเห็นชอบ โดยสรุป และผลการปฏิบัติตามนโยบายดังกล่าวไว้ในรายงานประจำปี และ website ของบริษัท โดยเริ่มตั้งแต่รายงานประจำปี 2550 เป็นต้นมา</p>
                            </div>
                            <div class="col-govern-50 col-govern-right">
                                <img src="images/governance/govern_disclose4.jpg" alt="" class="">
                            </div>
                        </div>
                    </div>
                </div>

                <div id="corpBlock4" class="corp-block">
                    <div>
                        <div class="">
                            <div class="col-govern-50">
                                <h2 class="title-gov title-gov-p2">การเปิดเผยบทบาทและหน้าที่ของคณะกรรมการ</h2>
                                <p>บริษัทได้เปิดเผยเกี่ยวกับบทบาทและหน้าที่ของคณะกรรมการและคณะกรรมการชุดย่อยต่างๆ และจำนวนครั้งที่ กรรมการแต่ละท่านเข้าร่วมประชุมไว้ภายใต้หัวข้อ โครงสร้างการจัดการแล้ว โดยมีรายงานของประธานคณะกรรมการแต่ละคณะเปิดเผยไว้ด้วย</p>
                            </div>
                            <div class="col-govern-50 col-govern-right">
                                <img src="images/governance/govern_disclose5.jpg" alt="" class="">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<?php include('footer.php'); ?>
