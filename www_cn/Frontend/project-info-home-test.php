<?php include('header.php'); ?>
    <!-- CSS -->
    <link rel="stylesheet" href="css/project-info-home.css" type="text/css">
    <link rel="stylesheet" href="bxslider/jquery.bxslider.css" type="text/css">
    <!-- JS -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC0Itod-fczPlmFpGySrnvOJ15khTzQTd4&callback=initMap"
            async defer></script>
    <script src="bxslider/jquery.bxslider.min.js"></script>
    <script src="js/project-info-home.js"></script>


    <div class="page-banner page-scroll slider">
        <div id="pjHomeBanner" class="owl-carousel owl-theme">
            <div class="item slide">
                <div class="banner-container banner-parallax background-show">
                    <img src="images/temp/pj_bannerhome_1.jpg" alt="">
                </div>

            </div>


        </div>
        <span id="scrollNextInfoHome" class="i-scroll-next"></span>
    </div>

    <!-- Responsive Submenu-->
    <div class="proj-rps-submenu-container">
        <p>ข้อมูลโครงการ</p>
        <div class="proj-rps-submenu">
            <?php include('snippet/snippet-menu-project-info-home-test.php'); ?>
        </div>
    </div>



    <div id="sec-information" class="content project-info-page">
        <div class="project-info-container">
            <div class="container">
                <div class="row">
                    <div class="info-logo-img">
                        <img src="images/logo/logo_indy.png" alt="" style="width: 100px;">
                    </div>
                </div>
                <div class="row mb15">
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                        <p class="subtitle green">ข้อมูลโครงการ</p>
                    </div>
                    <div class="col-md-6">
                        <p><b><span class="line-space ">โครงการทาวน์โฮม INDY บางใหญ่</span></b><br>
                            <span class="line-space">พื้นที่โครงการ 27.5 ไร่</span><br>
                            <span class="line-space">จำนวนแปลงขาย 300 แปลง</span><br>
                            <span class="line-space">ประเภทที่อยู่อาศัยในโครงการ</span><br>
                            <span class="line-space">ประเภท ทาวน์โฮม มีแบบบ้านให้เลือกทั้งหมด 1 แบบ</span><br>
                            <span class="line-space">พื้นที่ใช้สอยตั้งแต่ 89 ตร.ม. บนขนาดที่ดินตั้งแต่ 18 ตารางวา</span>
                        </p>
                        <p class="subtitle">ราคาเริ่มต้น 2.79 ล้านบาท</p>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <div class="row mb15">
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                        <p class="subtitle green">แนวคิดโครงการ</p>
                    </div>
                    <div class="col-md-6">
                        <p class="txt-slogan">"The new definition of London living"</p>
                        <p class="subtitle">"เริ่มต้นเรื่องราวของชีวิตในไลฟ์สไตล์ที่เป็นคุณ..."</p>
                    </div>
                    <div class="col-md-2"></div>
                </div>

                <div class="row mb15">
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                        <p class="subtitle green">จุดเด่นโครงการ</p>
                    </div>
                    <div class="col-md-6">
                        <ol>
                            <li>เชื่อมติดชีวิตเมือง ด้วยรถไฟฟ้าสายสีม่วง พร้อมอาคารจอดรถ เพียง 1.9 กม. </li>
                            <li>ใกล้แหล่ง Community Mall และ ห้างสรรพสินค้าแห่งใหม่ Central West Gate </li>
                            <li>เชื่อมต่อทุกการเดินทางด้วย ถนนกาญจนาภิเษก, ถนนรัตนาธิเบศร์ และอีกหลากหลายเส้นทาง </li>
                            <li>โอบล้อมด้วยสีสันของดอกไม้นานาพันธุ์ สไตล์ English Garden </li>
                            <li>พื้นที่สวนสาธารณะขนาดใหญ่ พร้อมบริการ Wifi</li>
                        </ol>
                    </div>
                    <div class="col-md-2"></div>
                </div>

                <div class="row mb15">
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                        <p class="subtitle green">สิ่งอำนวยความสะดวก</p>
                    </div>
                    <div class="col-md-6">
                        <p>โถงล็อบบี้, สระว่ายน้ำระบบเกลือ, ห้องฟิตเนส, ห้องอ่านหนังสือ<br>
                            ห้องนั่งเล่น, สนามเทนนิส, สวนกลางแจ้ง</p>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <div class="row mb15">
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                        <p class="subtitle green">ระบบรักษาความปลอดภัย</p>
                    </div>
                    <div class="col-md-6">
                        <p>CCTV, Key Card, Digital Door Lock</p>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <div class="row mb15">
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                        <p class="subtitle green">ที่จอดรถ</p>
                    </div>
                    <div class="col-md-6">
                        <p>จำนวน 80% โดยประมาณ</p>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>

        <div class="container">
            <div id="sec-promotion" class="block-margin">
                <p class="heading-title">โปรโมชั่น</p>
                <div class="row">
                    <div id="projectPromoSlide">
                        <div class="item">
                            <div class="">
                                <div class="col-md-8">
                                    <div class="review-img">
                                        <img src="images/temp/pj_home_promo.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="promo-descrp">
                                        <p class="title">Easy Smart Life</p>
                                        <p>พบทาวน์โฮมดีไซน์ใหม่ ในบรรยากาศที่สวยที่สุดสไตล์ London
                                            เริ่มต้นชีวิตด้วยบ้านคุณภาพได้ง่ายๆ บนทำเลสะดวกที่สุด
                                            ใกล้รถไฟฟ้าและทางด่วนใหม่ศรีรัช-วงแหวน อัตราผ่อนพิเศษ 9,300 บาท/เดือน เริ่ม
                                            2.79 ล้าน</p>
<!--                                        <a href="" class="btn btn-seemore" style="text-align:-->
<!--                                    center">See more</a>                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="sec-home-sale" class="block-margin">
                <p class="heading-title">บ้านพร้อมขาย</p>

                <div class="row">
                    <div id="projectModelSlide">
                        <div class="col-md-8">
                            <div id="projectSlide" class="owl-carousel owl-theme">
                                <div class="gal-img item"><img src="images/temp/pj_home_sale.jpg"/></div>
                                <div class="gal-img item"><img src="images/temp/pj_home_sale_a1.jpg"/></div>
                                <div class="gal-img item"><img src="images/temp/pj_home_sale_a2.jpg"/></div>
                                <div class="gal-img item"><img src="images/temp/pj_home_sale_a3.jpg"/></div>
                                <div class="gal-img item"><img src="images/temp/pj_home_sale_a4.jpg"/></div>
                                <div class="gal-img item"><img src="images/temp/pj_home_sale_a5.jpg"/></div>
                                <div class="gal-img item"><img src="images/temp/pj_home_sale_a2.jpg"/></div>
                                <div class="gal-img item"><img src="images/temp/pj_home_sale_a3.jpg"/></div>
                                <div class="gal-img item"><img src="images/temp/pj_home_sale_a4.jpg"/></div>
                                <div class="gal-img item"><img src="images/temp/pj_home_sale_a5.jpg"/></div>
                            </div>

                            <div id="imgThumb" class="slide-thumb owl-thumbs owl-carousel owl-theme">
                                <a class="item"><img src="images/temp/pj_home_sale.jpg" alt=""></a>
                                <a class="item"><img src="images/temp/pj_home_sale_a1.jpg" alt=""></a>
                                <a class="item"><img src="images/temp/pj_home_sale_a2.jpg" alt=""></a>
                                <a class="item"><img src="images/temp/pj_home_sale_a3.jpg" alt=""></a>
                                <a class="item"><img src="images/temp/pj_home_sale_a4.jpg" alt=""></a>
                                <a class="item"><img src="images/temp/pj_home_sale_a5.jpg" alt=""></a>
                                <a class="item"><img src="images/temp/pj_home_sale_a2.jpg" alt=""></a>
                                <a class="item"><img src="images/temp/pj_home_sale_a3.jpg" alt=""></a>
                                <a class="item"><img src="images/temp/pj_home_sale_a4.jpg" alt=""></a>
                                <a class="item"><img src="images/temp/pj_home_sale_a5.jpg" alt=""></a>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="promo-descrp" id="desc_home">
                                <p class="title green">แบบบ้าน WALNUT</p>
                                <p class="title dsktp">หมายเลขแปลง 01U02</p>
                                <p class="title dsktp">ลักษณะแปลงที่ดิน</p>
                                <p>ลักษณะเด่นของแปลง ราคาที่สุดพิเศษกับทำเลส่วนตัว
                                private zone</p>
                                <p class="title green">ราคา 5.49 ล้านบาท</p>
                                <a href="<?php url('/project-info-sale.php'); ?>" class="btn btn-seemore">See more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="other-home-sec" class="other-home-sec">
                <p class="heading-title">บ้านพร้อมขายทั้งหมด <span id="index_plan">1/5 แปลง</span></p>
                <input type="hidden" id="amount_value" value="5">
                <div class="row">
                    <div id="otherHome" class="col-slide">
                        <div class="item">
                            <div class="">
                                <div class="other-home-img">
                                    <img src="images/temp/pj_home_sale.jpg" alt="">
                                </div>
                                <a>
                                    <p class="title">แบบบ้าน CEDAR A</p>
                                    <p class="gray mb0">หมายเลขแปลง 01I07</p>
                                    <p class="title green"><b>ราคา 5.49 ล้านบาท</b></p>
                                    <div id="data-slider" style="display: none">
                                        <input type="hidden" id="index_value" value="1">
                                        <input type="hidden" id="title_home" value="CEDAR A">
                                        <input type="hidden" id="format_home" value="แบบ บ้านๆ">
                                        <input type="hidden" id="id_home" value="01I07">
                                        <input type="hidden" id="identity_home" value="เจ๋งเฟร่อ">
                                        <input type="hidden" id="price_home" value="6.88 ล้านบาทจร้า">
                                        <div id="img_list">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale_a3.jpg">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale_a4.jpg">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale_a5.jpg">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="">
                                <div class="other-home-img">
                                    <img src="images/temp/pj_home_sale.jpg" alt="">
                                </div>
                                <a>
                                    <p class="title">แบบบ้าน WALNUT</p>
                                    <p class="gray mb0">หมายเลขแปลง 01I07</p>
                                    <p class="title green"><b>ราคา 5.49 ล้านบาท</b></p>
                                    <div id="data-slider" style="display: none">
                                        <input type="hidden" id="index_value" value="2">
                                        <input type="hidden" id="title_home" value="WALNUT">
                                        <input type="hidden" id="format_home" value="WALNUT MODERN">
                                        <input type="hidden" id="id_home" value="01U07">
                                        <input type="hidden" id="identity_home" value="บ้านทรง โมเดิร์น">
                                        <input type="hidden" id="price_home" value="ราคา 7.99 ล้านบาท">
                                        <div id="img_list">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale_a3.jpg">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale_a4.jpg">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale_a5.jpg">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="">
                                <div class="other-home-img">
                                    <img src="images/temp/pj_home_sale.jpg" alt="">
                                </div>
                                <a>
                                    <p class="title">แบบบ้าน CEDAR B</p>
                                    <p class="gray mb0">หมายเลขแปลง 01I07</p>
                                    <p class="title green"><b>ราคา 5.49 ล้านบาท</b></p>
                                    <div id="data-slider" style="display: none">
                                        <input type="hidden" id="index_value" value="3">
                                        <input type="hidden" id="title_home" value="CEDAR B">
                                        <input type="hidden" id="format_home" value="CEDAR B">
                                        <input type="hidden" id="id_home" value="01H07">
                                        <input type="hidden" id="identity_home" value="Loof Of Title">
                                        <input type="hidden" id="price_home" value="ราคา 7.98 ล้านบาท">
                                        <div id="img_list">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale_a3.jpg">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale_a4.jpg">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale_a5.jpg">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale_a4.jpg">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale_a4.jpg">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="">
                                <div class="">
                                    <img src="images/temp/pj_home_sale.jpg" alt="">
                                </div>
                                <a>
                                    <p class="title green">แบบบ้าน CEDAR C</p>
                                    <p class="gray mb0">หมายเลขแปลง 01I07</p>
                                    <p class="title green"><b>ราคา 5.49 ล้านบาท</b></p>
                                    <div id="data-slider" style="display: none">
                                        <input type="hidden" id="index_value" value="4">
                                        <input type="hidden" id="title_home" value="CEDAR C">
                                        <input type="hidden" id="format_home" value="CEDAR C Custom">
                                        <input type="hidden" id="id_home" value="01I07">
                                        <input type="hidden" id="identity_home" value="CEDAR C Custom Private">
                                        <input type="hidden" id="price_home" value="ราคา 16.88 ล้านบาท">
                                        <div id="img_list">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale_a3.jpg">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale_a4.jpg">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale_a5.jpg">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale_a5.jpg">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale_a5.jpg">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale_a5.jpg">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="">
                                <div class="">
                                    <img src="images/temp/pj_home_sale.jpg" alt="">
                                </div>
                                <a>
                                    <p class="title green">แบบบ้าน CEDAR C</p>
                                    <p class="gray mb0">หมายเลขแปลง 01I07</p>
                                    <p class="title green"><b>ราคา 5.49 ล้านบาท</b></p>
                                    <div id="data-slider" style="display: none">
                                        <input type="hidden" id="index_value" value="5">
                                        <input type="hidden" id="title_home" value="CEDAR C">
                                        <input type="hidden" id="format_home" value="CEDAR C Custom">
                                        <input type="hidden" id="id_home" value="01I07">
                                        <input type="hidden" id="identity_home" value="CEDAR C Custom Private">
                                        <input type="hidden" id="price_home" value="ราคา 16.88 ล้านบาท">
                                        <div id="img_list">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale_a3.jpg">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale_a4.jpg">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale_a5.jpg">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale_a5.jpg">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale_a5.jpg">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale_a5.jpg">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale_a3.jpg">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale_a4.jpg">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="sec-gallery" class="gallery-block block-margin">
                <p class="heading-title">แกลอรี</p>
                <div class="tpl5-img">
                    <div id="tpl5-img">
                        <div class="item tpl5-img-1">
                            <a class="gallery" href="images/temp/pj_home_1.jpg">
                                <img src="images/temp/pj_home_1.jpg" alt="">
                                <p class="title-gallery">ห้องครัวขนาดใหญ่</p>
                            </a>
                        </div>
                        <div class="item tpl5-img-2">
                            <a class="gallery" href="images/temp/pj_home_2.jpg">
                                <img src="images/temp/pj_home_2.jpg" alt="">
                                <p class="title-gallery">Private Zone หน้าสวนสวย </p>
                            </a>
                        </div>
                        <div class="item tpl5-img-3">
                            <a class="gallery" href="images/temp/pj_home_4.jpg">
                                <img src="images/temp/pj_home_4.jpg" alt="">
                                <p class="title-gallery">สวนสาธารณะขนาดใหญ่ </p>
                            </a>
                        </div>
                        <div class="item tpl5-img-4">
                            <a class="gallery" href="images/temp/pj_home_3.jpg">
                                <img src="images/temp/pj_home_3.jpg" alt="">
                                <p class="title-gallery">ห้องนอนขนาดใหญ่</p>
                            </a>
                        </div>
                        <div class="item tpl5-img-5">
                            <a class="gallery" href="images/temp/pj_home_5.jpg">
                                <img src="images/temp/pj_home_5.jpg" alt="">
                                <p class="title-gallery">โซนรับประทานอาหาร</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="project-info-other-block block-margin">
                <div class="row">
                    <div id="sec-virtual" class="col-md-6">
                        <p class="heading-title">ภาพ 360 องศา</p>
                        <div class="project-info-other-img">
                            <a href="#" data-featherlight="#lb360">
                                <span class="i-view-360"></span>
                                <img src="images/temp/pj_home_360.jpg" alt="">
                            </a>
                            <iframe class="lightbox vdoview" src="http://www.lh.co.th/360file/VBA/" width="1000"
                                    height="560" id="lb360" style="border:none;" webkitallowfullscreen
                                    mozallowfullscreen allowfullscreen></iframe>
                        </div>
                        <p class="title">Good Moment</p>
                    </div>
                    <div id="sec-tvc" class="col-md-6">
                        <p class="heading-title">VDO</p>
                        <div class="project-info-other-img">
                            <a href="#" data-featherlight="#lbVdo1">
                                <span class="i-view-vdo"></span>
                                <img src="images/temp/pj_home_tvc.jpg" alt="">
                            </a>
                            <iframe class="lightbox" src="https://www.youtube.com/embed/88RWB4CobYc" width="1000"
                                    height="560" id="lbVdo1" style="border:none;" webkitallowfullscreen
                                    mozallowfullscreen allowfullscreen></iframe>

                        </div>
                        <p class="title">Good Moment</p>
                    </div>
                </div>
            </div>
        </div>

        <div id="sec-location" class="project-location-block block-margin">
            <div class="container">
                <div class="row">
                    <p class="heading-title">Location</p>
                </div>
            </div>

            <div class="map-container">
                <div id="map"></div>
                <span class="i-getdirection"></span>
            </div>
            <div id="info-window" style="display:none;">
                <div style="width: auto;">
                    <img src="images/logo/logo_indy.jpg" class="background-logo" style="float:left;"/>
                    <p style="float:left; padding-top: 20px;margin-left: 20px;">ราคาเริ่มต้น <br> <span>3.79 - 5 ล้านบาท</span></p>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p class="title">สิ่งอำนวยความสะดวกโดยรอบโครงการ</p>
                        <div class="content-style">
                            <div class="col-md-6">
                                <ul class="tbs1">
                                    <li>ใกล้ม.มหิดล ศาลายา 4.00 กม.</li>
                                    <li>ใกล้ Central ศาลายา 4.00 กม.</li>
                                    <li>ไป Central ปิ่นเกล้า 20.00 กม.จากเชิงสะพานปิ่นเกล้า</li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul class="tbs1">
                                    <li>ใกล้ม.มหิดล ศาลายา 4.00 กม.</li>
                                    <li>ใกล้ Central ศาลายา 4.00 กม.</li>
                                    <li>ไป Central ปิ่นเกล้า 20.00 กม.จากเชิงสะพานปิ่นเกล้า</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div id="sec-contact" class="block-margin">
                <p class="heading-title">Contact & Appointment</p>

                <div>
                    <form action="" class="project-form form-container">
                        <p class="title-label"></p>
                        <div class="">
                            <div class="form-group radio-checkmark">
                                <input type="radio" id="check-contact" name="check-type" checked />
                                <label for="check-contact" class="title-label"><span></span>ติดต่อสอบถาม</label>
                                <input type="radio" id="check-appoint" name="check-type" />
                                <label for="check-appoint" class="title-label"><span></span>นัดหมายเยี่ยมชมโครงการ</label>
                            </div>
                            <div class="form-group check-type-radio">
                                <input type="text" id="date_appointment" name="" class="form-control" placeholder="*วันที่ต้องการนัดหมาย">
                            </div>
                            <div class="form-group">
                                <input type="text" name="" class="form-control" placeholder="ccr@lh.co.th">
                            </div>
                            <div class="form-group">
                                <input type="text" name="" class="form-control" placeholder="*ระบุชื่อ - นามสกุล">
                            </div>
                            <div class="form-group">
                                <input type="text" name="" class="form-control" placeholder="*หมายเลขโทรศัพท์">
                            </div>
                            <div class="form-group">
                                <input type="text" name="" class="form-control" placeholder="*อีเมล์">
                            </div>
                            <div class="form-group">
                                <textarea name="" placeholder="*ระบุข้อความ" id="" class="form-control" cols="30" rows="10"></textarea>
                            </div>
                            <div class="title-block">
                                <p class="title">ต้องการให้ติดต่อกลับทาง</p>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" id="check-tel" name="check-tel" />
                                <label for="check-tel" class="title-label"><span></span>หมายเลขโทรศัพท์</label>
                                <input type="checkbox" id="check-mail" name="check-mail" />
                                <label for="check-mail" class="title-label"><span></span>อีเมล์</label>
                            </div>
                            <button class="btn btn-submit">ส่งข้อมูล</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>


<?php include('footer.php'); ?>