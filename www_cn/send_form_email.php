<?php

if(isset($_POST['email_cus'])) {

    // EDIT THE 2 LINES BELOW AS REQUIRED
  $email_to = "lhch@lh.co.th";
  $email_subject = "Your email subject line";

  function died($error) {
        // your error code can go here
    echo "We are very sorry, but there were error(s) found with the form you submitted. ";
    echo "These errors appear below.<br /><br />";
    echo $error."<br /><br />";
    echo "Please go back and fix these errors.<br /><br />";
    die();
  }

  /*'project_id' : project_id,
  'project_name' : project_name,
  'contact': 'contact',
  'email_lh': email_lh,
  'customer_name': customer_name,
  'tell': tell,
  'email_cus': email_cus,
  'content': content,
  'contact_us' : contact_us,*/

    // validation expected data exists
  if(!isset($_POST['project_id']) ||
    !isset($_POST['project_name']) ||
    !isset($_POST['contact']) ||
    !isset($_POST['email_lh']) ||
    !isset($_POST['customer_name']) ||
    !isset($_POST['email_cus']) ||
    !isset($_POST['content']) ||
    !isset($_POST['contact_us']) ||
    !isset($_POST['tell'])) {
    died('We are sorry, but there appears to be a problem with the form you submitted.');       
}



    $first_name = $_POST['customer_name']; // required
    $project_name = $_POST['project_name']; // required
    $email_from = $_POST['email_cus']; // required
    $telephone = $_POST['tell']; // not required
    $comments = $_POST['content']; // required

    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';

    if(!preg_match($email_exp,$email_from)) {
      $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
    }

    /*$string_exp = "/^[A-Za-z .'-]+$/";

    if(!preg_match($string_exp,$first_name)) {
      $error_message .= 'The First Name you entered does not appear to be valid.<br />';
    }

    if(!preg_match($string_exp,$project_name)) {
      $error_message .= 'The Last Name you entered does not appear to be valid.<br />';
    }

    if(strlen($comments) < 2) {
      $error_message .= 'The Comments you entered do not appear to be valid.<br />';
    }*/

    if(strlen($error_message) > 0) {
      died($error_message);
    }

    $email_message = "Form details below.\n\n";


    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }



    $email_message .= "Name: ".clean_string($first_name)."\n";
    $email_message .= "Project : ".clean_string($project_name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
    $email_message .= "Telephone: ".clean_string($telephone)."\n";
    $email_message .= "Comments: ".clean_string($comments)."\n";

// create email headers
    $headers = 'From: '.$email_from."\r\n".
    'Reply-To: '.$email_from."\r\n" .
    'X-Mailer: PHP/' . phpversion();
    @mail($email_to, $email_subject, $email_message, $headers); 

       echo json_encode('success'); 
    ?>


    <?php

  }
  ?>