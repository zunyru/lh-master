
<?php 
session_start();
$_SESSION['group_id'];
include_once('include/dbGroups.php');  

$_GET['page']='group';

    $funGroup = new dbGroups();  
    if(isset($_POST['groupsname'])){  
         $groupsname = $_POST['groupsname'];  
         $password = $_POST['password'];  
         $qr = $funGroup->add_group($groupsname, $password); 
        if ($qr) {  
            
           //header("location:group_add.php");
           $success ="success";  
        } else {  
            
          $error="error"; //ค่าซ้ำ
      
        }  
    }  
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
     <script>
      $(document).ready(function(){
        $("#alert").show();
            $("#alert").fadeTo(3000, 500).slideUp(500, function(){
              $("#alert").alert('close');
          });
      });
      </script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
           
           <!-- sidebar menu -->
            <?php include './master/navbar.php';?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
           
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
          <?php include './master/top_nav.php'; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><a href="group.php">ระบบจัดการข้อมูล Group</a> > <a href="group2.html">สร้าง Group</a></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                  <p class="font-gray-dark"> 
                  </p>
                  <br>
                <?php if(isset($success)){?>
                   <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-success col-md-4" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>สร้างข้อมูลเรียบร้อยแล้ว</strong> 
                      </div>
                    </div>
                    <div class="col-md-6"></div> 
                   </div> 
                  <?php }else if(isset($error)){?>
                    <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-4" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>มีชื่อนี้อยู่ในระบบแล้ว</strong>
  
                      </div>
                    </div>
                    <div class="col-md-6"></div> 
                   </div> 
                  <?php }?>
                  <form class="form-horizontal form-label-left" action="" method="post" id="commentForm">
                    <div class="form-group">
                      <label class="control-label col-md-2" for="first-name">ระบุชื่อ Group <span class="required">*</span>
                      </label>
                      <div class="col-md-4">
                        <input type="text"  required="required" name="groupsname" class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อ Group">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2"  for="first-name">Password <span class="required">*</span>
                      </label>
                      <div class="col-md-4">
                        <input type="password"  required="required" name="password" class="form-control col-md-7 col-xs-12" placeholder="รหัสผ่าน">
                      </div>
                    </div>  
                  <div class="col-md-6">
                    <center><br>
                    <button type="submit" class="btn btn-success">Submit</button>
                  </center>
                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        <!-- /page content -->

        <!-- footer content -->
        <!-- <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer> -->
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

          <!-- Validate -->
          <script src="../build/js/jquery.validate.js"></script>
          <script>
              $(document).ready(function() {

                  $("#commentForm").validate({
                      rules: {
                          groupsname: "required",
                          password: "required",

                      },
                      messages: {
                          groupsname: "กรุณากรอกชื่อ group !",
                          password : "กรุณากรอก password !",


                      }
                  });

                  $("#lead_img_file").rules("add", {
                      required:true,
                      'hobbies[]': "เลือกอย่างน้อย 1 รายการ"
                  });

              });
          </script>
        <!-- jQuery Tags Input -->
    <script>
      function onAddTag(tag) {
        alert("Added a tag: " + tag);
      }

      function onRemoveTag(tag) {
        alert("Removed a tag: " + tag);
      }

      function onChangeTag(input, tag) {
        alert("Changed a tag: " + tag);
      }

      $(document).ready(function() {
        $('#tags_1').tagsInput({
          width: 'auto'
        });
        $('#tags_2').tagsInput({
          width: 'auto'
        });
      });
    </script>
    <!-- /jQuery Tags Input -->

  </body>
</html>
