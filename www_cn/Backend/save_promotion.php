<?php

session_start();
include './include/dbCon_mssql.php';
date_default_timezone_set('Asia/Bangkok');
$_SESSION['group_id'];


$promotion_name_th = mssql_escape($_POST['promotion_name_th']);
$promotion_name_en = mssql_escape($_POST['promotion_name_en']);
$promotion_detail_th = $_POST['promotion_detail_th'];
$promotion_detail_en = $_POST['promotion_detail_en'];
$project_id = $_POST['project_id'];
$iCheck = $_POST['iCheck'];
$date_start = $_POST['date_start'];
$date = $_POST['date'];
$promotion_url = $_POST['promotion_url'];

//echo $test =str_replace(" ","-","Hello world!",$var);
//http://devwww2.lh.co.th/Land_and_house/Backend/tset-project
//Frontend/project-info-home.php



$dates = $date_start;
$year = substr($dates, 6, 4); //2017
$day = substr($dates, 0, 2); //21
$mont = substr($dates, 3, 2); //02

$date_end = $date;
$mont2 = substr($date_end, 3, 2); //02
$day2 = substr($date_end, 0, 2); //21
$year2 = substr($date_end, 6, 4); //2017

$start_date = $year . "-" . $mont . "-" . $day;
$end_date = $year2 . "-" . $mont2 . "-" . $day2;


$dates        = date("Y-m-d H:i:s");

function mssql_escape($str) {
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return str_replace("'", "''", $str);
}

if ($iCheck == "") {
    //มีชื่อ series นี้อยู่แล้ว
    $_SESSION['add']='-2';
                echo '<script>
                window.history.go(-1);
                </script>';
        exit();
}

$query = "SELECT COUNT(*) AS counts FROM LH_PROMOTION WHERE project_id = '" . $project_id . "'";

$query_result = mssql_query($query, $db_conn);
$check_id = mssql_fetch_row($query_result);
$check_id[0];
// project not duplicate
if ($check_id[0] == 0) {
    $sql = "SELECT project_name_en,galery_project_img_id,galery_project_img_name FROM LH_PROJECTS p
								LEFT JOIN LH_GALERY_PROJECT g_p ON g_p.project_id = p.project_id
								LEFT JOIN LH_GALERY_IMG_PROJECT g_img ON g_img.galery_project_id = g_p.galery_project_id
								WHERE  g_img.galery_project_img_id = '$iCheck' ";
    $query = mssql_query($sql, $db_conn);
    $result = mssql_fetch_array($query);
    $img = $result['galery_project_img_name'];
    $id_img = $result['galery_project_img_id'];


    $sql = "INSERT INTO LH_PROMOTION (project_id,promotion_name_th,promotion_name_en,promotion_detail_th,promotion_detail_en, "
            . "promotion_start_date,promotion_end_date,promotion_url,promotion_approve,promotion_highlights,creat_date,update_date) VALUES "
            . "('$project_id',N'$promotion_name_th','$promotion_name_en',N'" . mssql_escape($promotion_detail_th) . "',"
            . "'" . mssql_escape($promotion_detail_en) . "','$start_date','$end_date',N'" . mssql_escape($promotion_url) . "','N','','$dates','$dates')";
    $query = mssql_query($sql, $db_conn);

    $sql_id = "SELECT MAX(promotion_id) AS max FROM LH_PROMOTION ";
    $query_id = mssql_query($sql_id, $db_conn);
    $result_id = mssql_fetch_array($query_id);
    $promotion_id = $result_id['max'];

    $sql_img = "INSERT INTO LH_IMG_PROMOTION (promotion_id,img_promotion_name,img_id_promotion) VALUES ('$promotion_id','$img','$id_img')";
    $query_img = mssql_query($sql_img, $db_conn);

    if ($query_img) {
        $_SESSION['add']='1';
        header("location:promotion_add.php?");
    } else {
        $_SESSION['add']='-3';
        header("location:promotion_add.php?");
    }
// project duplicate
} else if ($check_id[0] != 0) {
    $query = "SELECT COUNT(*) AS counts FROM LH_PROMOTION WHERE project_id = '" . $project_id . "' AND (( promotion_start_date BETWEEN '$start_date'  AND '$end_date') OR (promotion_end_date BETWEEN '$start_date' AND '$end_date') OR ('$start_date' BETWEEN promotion_start_date AND promotion_end_date ) OR ('$end_date' BETWEEN promotion_start_date AND promotion_end_date )) AND project_id IN ($project_id) ";
    $query_result = mssql_query($query, $db_conn);
    $count_date = mssql_fetch_row($query_result);
    $count_date[0];
    if ($count_date[0] >= 1) {
         $_SESSION['add']='-1';
                echo '<script>
                window.history.go(-1);
                </script>';
        exit();
    } else {
        $sql = "SELECT project_name_en,galery_project_img_id,galery_project_img_name FROM LH_PROJECTS p
									LEFT JOIN LH_GALERY_PROJECT g_p ON g_p.project_id = p.project_id
									LEFT JOIN LH_GALERY_IMG_PROJECT g_img ON g_img.galery_project_id = g_p.galery_project_id
									WHERE  g_img.galery_project_img_id = '$iCheck' ";

        $query = mssql_query($sql, $db_conn);
        $result = mssql_fetch_array($query);
        $img = $result['galery_project_img_name'];
        $id_img = $result['galery_project_img_id'];
        if ($promotion_url == "") {
            $url = str_replace(" ", "-", $result['project_name_en'], $var);
            $url_status = ''; //ไม่ได้กำหนด
        } else {

            $url = $promotion_url;
            $url_status = '';
        }


        $sql = "INSERT INTO LH_PROMOTION (project_id,promotion_name_th,promotion_name_en,promotion_detail_th,promotion_detail_en, "
                . "promotion_start_date,promotion_end_date,promotion_url,promotion_approve,promotion_highlights,creat_date,update_date) VALUES "
                . "('$project_id',N'$promotion_name_th','$promotion_name_en',N'" . mssql_escape($promotion_detail_th) . "',"
                . "'" . mssql_escape($promotion_detail_en) . "','$start_date','$end_date',N'" . mssql_escape($promotion_url) . "','N','$url_status','$dates','$dates')";
        $query = mssql_query($sql, $db_conn);

        $sql_id = "SELECT MAX(promotion_id) AS max FROM LH_PROMOTION ";
        $query_id = mssql_query($sql_id, $db_conn);
        $result_id = mssql_fetch_array($query_id);
        $promotion_id = $result_id['max'];

        $sql_img = "INSERT INTO LH_IMG_PROMOTION (promotion_id,img_promotion_name,img_id_promotion) VALUES ('$promotion_id','$img','$id_img')";
        $query_img = mssql_query($sql_img, $db_conn);
    }
}
if ($query_img) {
      $_SESSION['add']='1';
    header("location:promotion_add.php?");
} else {
      $_SESSION['add']='-3';
    header("location:promotion_add.php?");
}
?>