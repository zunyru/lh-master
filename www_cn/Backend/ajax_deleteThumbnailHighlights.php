<?php
require_once 'include/dbConnect.php';

    try {
        $highlights_id = $_GET['highlights_id'];

        $conn = (new dbConnect())->getConn();
        $sql = "UPDATE LH_HIGHLIGHTS SET highlight_thumbnail = ''
                WHERE highlights_id =".$highlights_id;
        $result= $conn->query($sql);

        echo json_encode($result->fetchAll());

    } catch (\Exception $e) {
        return $e->getMessage();
    }
?>