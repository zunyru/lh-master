<?php

session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include './include/dbCon_mssql.php';

date_default_timezone_set('Asia/Bangkok');
$date       = date("Ymd");
$numrand_th = (mt_rand());
$numrand_en = (mt_rand());


function mssql_escape($str)
{
   if(get_magic_quotes_gpc())
   {
       $str= stripslashes($str);
   }
   return str_replace("'", "''", $str);
}

$name_tag    = mssql_escape($_POST['name']);
$url_page    = mssql_escape($_POST['url_page']);
$id = $_POST['id'];

$footer    = mssql_escape($_POST['footer']);


$dates        = date("Y-m-d H:i:s");
 $query        = "SELECT COUNT(*) AS counts FROM LH_FOOTERS WHERE name = N'$name_tag' AND NOT footer_id = '$id'";
$query_result = mssql_query($query, $db_conn);

$count = mssql_fetch_row($query_result);
$count[0];
if ($count[0] >= 1) {
    $_SESSION['add'] = '-1';
    echo '<script>window.history.go(-1);</script>';
    exit();
}

 $query   = "SELECT COUNT(*) AS counts FROM LH_FOOTERS WHERE url_page = N'$url_page' AND NOT footer_id = '$id'";
$query_result = mssql_query($query, $db_conn);

$count = mssql_fetch_row($query_result);
$count[0];
if ($count[0] >= 1) {
    $_SESSION['add'] = '-2';
    echo '<script>window.history.go(-1);</script>';
    exit();
}


$query_2 = "UPDATE LH_FOOTERS SET name = N'$name_tag',url_page = N'$url_page',content = N'$footer',update_date ='$dates' WHERE footer_id = '$id';";

$query_result2 = mssql_query($query_2, $db_conn);
if ($query_result2) {
    $_SESSION['add'] = '1';
    header("location:footer_update.php?id=$id");
}

