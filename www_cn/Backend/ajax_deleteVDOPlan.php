<?php
require_once 'include/dbConnect.php';

    try {
        $clip_plan_id = $_GET['clip_plan_id'];

        $conn = (new dbConnect())->getConn();
        $sql = "UPDATE LH_CLIP_FOR_PLAN SET clip_plan_url = ''
                WHERE clip_plan_id =".$clip_plan_id;
        $result= $conn->query($sql);

        echo json_encode($result->fetchAll());

    } catch (\Exception $e) {
        return $e->getMessage();
    }
?>