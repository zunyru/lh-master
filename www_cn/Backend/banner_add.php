<?php
session_start();
$_SESSION['group_id'];
include './include/dbCon_mssql.php';
header( 'Content-Type:text/html; charset=utf8');
$_GET['page']='lead_page';

if (isset($_GET['add'])) {
    if($_GET['add'] ==1){
        //header("location:product_add.php");
        $success ="success";
    } else if($_GET['add'] ==0){

        $error="error"; //ค่าซ้ำ

    } else if($_GET['add'] ==-1){
        $error="errors";
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>

    <!-- uploade img -->
    <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
    <!--uploade-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap-imgupload.min.css">
    <!-- Fancybox image popup -->
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />

    <!-- Include Editor style. -->
    <link href="editor/css/froala_editor.min.css" rel="stylesheet" type="text/css" />
    <link href="editor/css/froala_style.min.css" rel="stylesheet" type="text/css" />

    <!-- Include Editor style. -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.3.4/css/froala_editor.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.3.4/css/froala_style.min.css" rel="stylesheet" type="text/css" />

    <!-- Include JS file. -->
    <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.3.4/js/froala_editor.min.js"></script> -->

    <!-- Include Code Mirror style -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">

    <!-- Include Editor Plugins style. -->
    <link rel="stylesheet" href="editor/css/plugins/char_counter.css">
    <link rel="stylesheet" href="editor/css/plugins/code_view.css">
    <link rel="stylesheet" href="editor/css/plugins/colors.css">
    <link rel="stylesheet" href="editor/css/plugins/emoticons.css">
    <link rel="stylesheet" href="editor/css/plugins/file.css">
    <link rel="stylesheet" href="editor/css/plugins/fullscreen.css">
    <link rel="stylesheet" href="editor/css/plugins/image.css">
    <link rel="stylesheet" href="editor/css/plugins/image_manager.css">
    <link rel="stylesheet" href="editor/css/plugins/line_breaker.css">
    <link rel="stylesheet" href="editor/css/plugins/quick_insert.css">
    <link rel="stylesheet" href="editor/css/plugins/table.css">
    <link rel="stylesheet" href="editor/css/plugins/video.css">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <style>
        .file-caption-main .btn-file {
            overflow: visible;
        }

        .file-caption-main .btn-file .error {
            position: absolute;
            bottom: -32px;
            right: 30px;
        }
    </style>

    <style>
        #myProgress {
            position: fixed;
            bottom: 0;
            left: 0;
            right: 0;
            top: 0;
            background: rgba(0,0,0,0.5);
            z-index: 999;
        }

        #myCenter {
            margin-top: 230px;
            color: #fff;
        }
    </style>



</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="index.php" class="site_title"> <img src="./images/lh.jpg" alt="..." ><span></span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <?php include './master/navbar.php';?>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php include './master/top_nav.php'; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2><a href="banner.php">ระบบจัดการ Banner</a> > สร้าง Banner </h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <!-- <h4>Step1 : สร้างข้อมูล Series ได้ทั้งภาษาจีน และ ภาษาอังกฤษ(*ไม่บังคับ)</h4> -->
                                <p class="font-gray-dark">
                                </p><br>
                                <?php if(isset($success)){?>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="form-group">
                                            <div class="alert alert-success col-md-6" id="alert">
                                                <button type="button" class="close" data-dismiss="alert">x</button>
                                                <strong>สร้างข้อมูลเรียบร้อยแล้ว</strong>
                                            </div>
                                        </div>
                                        <div class="col-md-6"></div>
                                    </div>
                                <?php }else if(isset($error)){?>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="form-group">
                                            <div class="alert alert-danger col-md-6" id="alert">
                                                <button type="button" class="close" data-dismiss="alert">x</button>
                                                <strong>มีชื่อนี้อยู่ในระบบแล้ว</strong>
                                            </div>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                <?php }else if(isset($errors)){?>

                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="form-group">
                                            <div class="alert alert-danger col-md-6" id="alert">
                                                <button type="button" class="close" data-dismiss="alert">x</button>
                                                <strong>เกิดข้อผิดพลาด !</strong> ไม่สามารถบันทึกข้อมมูลได้
                                            </div>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                <?php }?>
                                <form  data-toggle="validator" class="form-horizontal form-label-left" action="save_banner.php" method="post" enctype="multipart/form-data" id="commentForm">
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">ชื่อ Banner <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6">
                                            <input type="text"   class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อ Banner " name="banner_name" value=""
                                                   data-rule-required="true">
                                        </div>
                                    </div>
                                    <?php
//                                    $page = array("home_page", "new_project", "country");
//                                    $mycheck =array();
//                                    for($i=0;$i<=8;$i++){
//                                        $sql="SELECT COUNT(banner_page) AS num1 FROM LH_BANNER_PAGE WHERE banner_page = '$page[$i]'";
//                                        $query = mssql_query($sql);
//                                        $mycheck=array_push($row = mssql_fetch_array($query));
//                                        print_r($mycheck);
//                                    }


                                    ?>

                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">เลือกหน้าที่แสดง   <span class="required">*</span> <br> (เลือกอย่างน้อย 1 หน้า)
                                        </label>
                                        <div class="col-md-2">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"  class="flat"  id="home"  name="spam[]" checked value="Home"  minlength="1" required> Home page
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="flat"  name="spam[]" value="ข่าวโครงการใหม่" > ข่าวโครงการใหม่
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="flat"  name="spam[]" value="โครงการในต่างจังหวัด"> โครงการในต่างจังหวัด
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="flat" id="project"  name="spam[]" value="project"> โครงการ
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="flat"  name="spam[]" value="บ้านเดี่ยว"> บ้านเดี่ยว
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="flat"  name="spam[]" value="ทาวน์โฮม"> ทาว์โฮม
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="flat"  name="spam[]" value="คอนโดมิเนียม"> คอนโดมิเนียม
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="flat"  name="spam[]" value="บ้านตกแต่งพร้อมขาย"> บ้านตกแต่งพร้อมขาย
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="flat"  name="spam[]" value="Ladawan"> Ladawan
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="flat"  name="spam[]" value="แบบบ้าน"> แบบบ้าน
                                                </label>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name"></label>
                                        <div class="col-md-6">
                                            <label for="spam[]"  class="error"></label>
                                        </div>
                                    </div>

                                    <div class="form-group" id="l5">
                                        <label class="control-label col-md-2" for="first-name">เลือกหน้าโครงการ <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6">
                                            <select  class="form-control" name="project" onchange="getval(this);">
                                                <option value="" selected>-เลือกโครงการ-</option>
                                            <?php
                                                $sql_p ="SELECT * FROM LH_PROJECTS ORDER  BY project_name_th asc";
                                                $query_p =mssql_query($sql_p);
                                               while ($row_p = mssql_fetch_array($query_p)){
                                            ?>
                                                <option value="<?=$row_p['project_id']?>" ><?=$row_p['project_name_th']." (".$row_p['project_name_en'].")";?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">ชื่อสไตล์บ้าน <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6">
                                            <select id="mySelect"  class="form-control" name="page" onchange="getval(this);">
                                                <option value="image" selected>Lead Image</option>
                                                <option value="banner">Banner Activity</option>
                                                <option value="youtube">VDO youtube</option>
                                                <option value="vdo">VDO file</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group" id="l1_2">
                                        <label class="control-label col-md-2" for="first-name">Lead Image Mobile <span class="required">*</span>  <br> สูงสุด 6 รูป <br> (รูปขนาด A x B )</label>

                                        </label>
                                        <div class="col-md-6">
                                            <input id="lead_img_file_mb" name="lead_img_file_mb[]" type="file" multiple class="file-loading" accept="image/*" >
                                        </div>
                                    </div>

                                    <script>
                                        $("#lead_img_file_mb").fileinput({
                                            uploadUrl: "upload.php", // server upload action
                                            maxFileCount: 6,
                                            allowedFileExtensions: ["jpg", "png", "jpeg"],
                                            browseLabel: 'เลือกรูป',
                                            removeLabel: 'ลบ',
                                            browseClass: 'btn btn-success',
                                            showUpload: false,
                                            showCaption: false,
                                            //msgFilesTooMany: 'ไฟล์ที่คุณเลือกมีจำนวน <b>({n})</b> ซึ่งเกินกว่าที่ระบบอนุญาตที่ <b>{m}</b>, กรุณาลองใหม่อีกครั้ง!',
                                            msgFilesTooMany: 'เลือกได้ สูงสุด <b>{m}</b> ไฟล์',
                                            msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                            xxx:'seo_lead_img_file',
                                            dropZoneTitle : 'รูป Lead Image',
                                            minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                            minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                            maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                            maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                            msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                            msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                            msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                                            msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                                        });
                                    </script>


                                    <!-- Lead Image -->
                                    <div id="id="l1">
                                    <div class="form-group" >
                                            <label class="control-label col-md-2" for="first-name">Lead Image <span class="required">*</span>  <br> สูงสุด 6 รูป <br> (รูปขนาด 1920 x 1080 )</label>

                                        </label>
                                        <div class="col-md-6">
                                            <input id="lead_img_file" name="lead_img_file[]" type="file" multiple class="file-loading" accept="image/*" >
                                        </div>
                                    </div>

                                    <script>
                                        $("#lead_img_file").fileinput({
                                            uploadUrl: "upload.php", // server upload action
                                            maxFileCount: 6,
                                            allowedFileExtensions: ["jpg", "png", "jpeg"],
                                            browseLabel: 'เลือกรูป',
                                            removeLabel: 'ลบ',
                                            browseClass: 'btn btn-success',
                                            showUpload: false,
                                            showCaption: false,
                                            //msgFilesTooMany: 'ไฟล์ที่คุณเลือกมีจำนวน <b>({n})</b> ซึ่งเกินกว่าที่ระบบอนุญาตที่ <b>{m}</b>, กรุณาลองใหม่อีกครั้ง!',
                                            msgFilesTooMany: 'เลือกได้ สูงสุด <b>{m}</b> ไฟล์',
                                            msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                            xxx:'seo_lead_img_file',
                                            dropZoneTitle : 'รูป Lead Image',
                                            minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                            minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                            maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                            maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                            msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                            msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                            msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                                            msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                                        });
                                    </script>

                                </div>
                                    <!-- url Banner -->
                                    <div id="l2">
                                        <div class="form-group" >
                                            <label class="control-label col-md-2" for="first-name">Banner Image <span class="required">*</span> <br> สูงสุด 1 รูป   <br> (รูปขนาด 1920 x 1080 ) </label>

                                            <div class="col-md-6">
                                                <input id="banner_image" name="banner_image" type="file" multiple class="file-loading" accept="image/*" >
                                            </div>
                                        </div>

                                        <script>
                                            $("#banner_image").fileinput({
                                                uploadUrl: "upload.php", // server upload action
                                                maxFileCount: 1,
                                                allowedFileExtensions: ["jpg", "png", "jpeg"],
                                                browseLabel: 'เลือกรูป',
                                                removeLabel: 'ลบ',
                                                browseClass: 'btn btn-success',
                                                showUpload: false,
                                                showCaption: false,
                                                msgFilesTooMany: 'เลือกได้ สูงสุด <b>{m}</b> ไฟล์',
                                                msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                xxx:'seo_lead_img_file',
                                                dropZoneTitle : 'รูป Banner',
                                                minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                                minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                                maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                                maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                                msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                                msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                                msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                                                msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                                            });
                                        </script>

                                        <div class="form-group" >
                                            <label class="control-label col-md-2" for="first-name">Banner content <span class="required">*</span></label>

                                            <div class="col-md-6">
                                                <textarea name="banner_text" placeholder="กรอกข้อความ banner" id="edit" required class="form-control" rows="3"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group" >
                                            <label class="control-label col-md-2" for="first-name">Backgroup color <br> (กำหนดสีพื้นหลัง Banner) <span class="required">*</span></label>

                                            <div class="col-md-1">
                                                <input type="color" class="form-control" name="favcolor" value="#f5f5f5">
                                            </div>
                                        </div>


                                    </div>

                                    <!-- url yputube -->
                                    <div id="l3">
                                        <div class="form-group" >
                                            <label class="control-label col-md-2" for="first-name"> Thumbnail youtube <span class="required">*</span> <br> สูงสุด 1 url   <br> (รูปขนาด 1920 x 1080 ) </label>

                                            <div class="col-md-6">
                                                <input id="img_youtube" name="img_youtube" type="file" multiple class="file-loading" accept="image/*" >
                                            </div>
                                        </div>

                                        <script>
                                            $("#img_youtube").fileinput({
                                                uploadUrl: "upload.php", // server upload action
                                                maxFileCount: 1,
                                                allowedFileExtensions: ["jpg", "png", "jpeg"],
                                                browseLabel: 'เลือกรูป',
                                                removeLabel: 'ลบ',
                                                browseClass: 'btn btn-success',
                                                showUpload: false,
                                                showCaption: false,
                                                msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                xxx:'seo_lead_img_file',
                                                dropZoneTitle : 'รูปภาพ Thumbnail Youtube',
                                                minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                                minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                                maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                                maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                                msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                                msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                                msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                                                msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                                            });
                                        </script>
                                        <div class="form-group" >
                                                <label class="control-label col-md-2" for="first-name">URL VDO Leade image <span class="required">*</span></label>

                                            <div class="col-md-6">
                                                <input id="url_youtube" class="form-control" name="url_youtube" type="text"  placeholder="URL Youtube" >
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Vdo file -->
                                   <div id="l4">
                                    <div class="form-group" >
                                        <label class="control-label col-md-2" for="first-name">Thumbnail VDO <span class="required">*</span> <br> สูงสุด 1 ไฟล์ <br> (รูปขนาด 1920 x 1080 ) </label>

                                        </label>
                                        <div class="col-md-6">
                                            <input id="vdo_img" name="vdo_img" type="file" multiple class="file-loading" accept="image/*" >
                                        </div>
                                    </div>

                                    <script>
                                        $("#vdo_img").fileinput({
                                            uploadUrl: "upload.php", // server upload action
                                            maxFileCount: 1,
                                            allowedFileExtensions: ["jpg", "png", "jpeg"],
                                            browseLabel: 'เลือกรูป',
                                            removeLabel: 'ลบ',
                                            browseClass: 'btn btn-success',
                                            showUpload: false,
                                            showCaption: false,
                                            msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                            msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                            xxx:'seo_lead_img_file',
                                            dropZoneTitle : 'รูปภาพ Thumbnail VDO',
                                            minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                            minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                            maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                            maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                            msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                            msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                            msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                                            msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                                        });
                                    </script>

                                       <div class="form-group" >
                                           <label class="control-label col-md-2" for="first-name">VDO File  <span class="required">*</span> สูงสุด 1 ไฟล์ <br> (ไฟล์ .mp4)</label>

                                           </label>
                                           <div class="col-md-6">
                                               <input id="vdo_file" name="vdo_file" type="file" multiple class="file-loading" accept="video/mp4" >
                                           </div>
                                       </div>

                                       <script>
                                           $("#vdo_file").fileinput({
                                               uploadUrl: "upload.php", // server upload action
                                               maxFileCount: 1,
                                               allowedFileExtensions: ["mp4"],
                                               browseLabel: 'เลือกไฟล์',
                                               removeLabel: 'ลบ',
                                               browseClass: 'btn btn-success',
                                               showUpload: false,
                                               showCaption: false,
                                               msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                               msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                               xxx:'seo_lead_img_file',
                                               dropZoneTitle : 'File VDO',

                                               msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                               msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                               msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                                               msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                                           });
                                       </script>

                                   </div>



                                    <div class="form-group" >
                                        <label class="control-label col-md-2" for="first-name">Link url <span class="required"></span></label>

                                        <div class="col-md-6">
                                            <input name="banner_url" placeholder="กรอก url link" class="form-control" rows="3">
                                        </div>
                                    </div>




                                    <center><br>
                                        <div class="col-md-6">
                                            <button type="submit" name="submit" class="btn btn-success" >Submit</button>
                                            <!--<button type="button" name="delete" class="btn btn-danger">Delete</button>-->
                                        </div>
                                    </center>
                                </form>

                                <div class="form-group">
                                    <label class="control-label col-md-4" > <span class="required"></span></label>
                                    <div class="col-md-3">
                                        <div id="myProgress">
                                            <!-- <div id="myBar"></div> -->
                                            <center id="myCenter"><img  src="fileupload/images/motivo/cloud_upload_256.gif" width="50" height="50">
                                                <p>กำลังโหลด...</p></center>
                                        </div>
                                    </div>
                                </div>



                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <!-- <footer>
                  <div class="pull-right">
                    Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
                  </div>
                  <div class="clearfix"></div>
                </footer> -->
                <!-- /footer content -->
            </div>
        </div>

        <!-- jQuery -->
        <script src="../vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../vendors/nprogress/nprogress.js"></script>
        <!-- iCheck -->
        <script src="../vendors/iCheck/icheck.min.js"></script>
        <!-- jQuery Tags Input -->
        <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
        <!-- Datatables -->
        <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
        <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
        <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
        <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
        <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
        <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
        <script src="../vendors/jszip/dist/jszip.min.js"></script>
        <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
        <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
        <script type="text/javascript" src="js/bootstrap-imgupload.js"></script>
        <!-- Fancybox image popup -->
        <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>

        <!--Editor_script-->

        <!-- Include JS files. -->
        <script type="text/javascript" src="editor/js/froala_editor.min.js"></script>
        <!-- Include Code Mirror. -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
        <!-- Include Plugins. -->
        <script type="text/javascript" src="editor/js/plugins/align.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/char_counter.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/code_beautifier.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/code_view.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/colors.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/emoticons.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/entities.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/file.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/font_family.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/font_size.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/fullscreen.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/image.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/image_manager.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/inline_style.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/line_breaker.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/link.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/lists.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/paragraph_format.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/paragraph_style.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/quick_insert.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/quote.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/table.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/save.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/url.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/video.min.js"></script>


        <!-- Include Language file if we want to u-->
        <script>
            $(function() {
                $('#edit').froalaEditor({
                    toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'fontFamily', 'fontSize', '|', 'specialCharacters', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', '-', '|', 'undo', 'redo', 'clearFormatting', 'selectAll', 'html', 'applyFormat', 'removeFormat', 'fullscreen', 'print'],
                    pluginsEnabled: null,
                    colorsText: [
                        '#ff00ff', '#1ABC9C', '#54ACD2', '#2C82C9', '#9365B8', '#475577', '#CCCCCC',
                        '#61BD6D', '#1ABC9C', '#54ACD2', '#2C82C9', '#9365B8', '#475577', '#CCCCCC',
                        '#41A85F', '#00A885', '#3D8EB9', '#2969B0', '#553982', '#28324E', '#000000',
                        '#F7DA64', '#FBA026', '#EB6B56', '#E25041', '#A38F84', '#EFEFEF', '#FFFFFF',
                        '#FAC51C', '#F37934', '#D14841', '#B8312F', '#7C706B', '#D1D5D8', 'REMOVE'
                    ],
                    height: 300,
                    imageUploadURL: 'uploade_highlights.php',
                    imageUploadParams: {
                        id: 'my_editor'
                    },

                    fileUploadURL: 'upload_file.php',
                    fileUploadParams: {
                        id: 'my_editor'
                    },
                    imagePaste: false,

                    imageManagerLoadURL: 'uploade_highlights.php',
                    imageManagerDeleteURL: "delete_image_highlights.php",
                    imageManagerDeleteMethod: "POST"
                })
                // Catch image removal from the editor.
                    .on('froalaEditor.image.removed', function (e, editor, $img) {
                        $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_image_highlights.php",

                            // Request params.
                            data: {
                                src: $img.attr('src')
                            }
                        })
                            .done (function (data) {
                                console.log ('image was deleted'+$img.attr('src'));
                            })
                            .fail (function (err) {
                                console.log ('image delete problem: ' + JSON.stringify(err));
                            })
                    })

                    // Catch image removal from the editor.
                    .on('froalaEditor.file.unlink', function (e, editor, link) {

                        $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_file.php",

                            // Request params.
                            data: {
                                src: link.getAttribute('href')
                            }
                        })
                            .done (function (data) {
                                console.log ('file was deleted');
                            })
                            .fail (function (err) {
                                console.log ('file delete problem: ' + JSON.stringify(err));
                            })
                    })
            });
        </script>



        <!-- Validate -->
        <script src="../build/js/jquery.validate.js"></script>
        <script>
            $(document).ready(function() {

                $("#commentForm").validate({
                    rules: {
                        banner_name: "required",
                        banner_image: "required",
                        banner_text : "required",
                        url_youtube : "required",
                        img_youtube : "required",
                        series_des_en: "required",
                        images : "required",
                        vdo_img : "required",
                        vdo_file : "required",
                        project : "required",
                    },
                    messages: {
                        banner_name: "กรุณากรอกชื่อ Banner !",
                        banner_text : "กรุณากรอกข้อความ Banner !",
                        url_youtube : "กรุณากรอก URL Youtube",
                        img_youtube  :" &nbsp; กรุณาเลือกรูป !",
                        banner_image :" &nbsp; กรุณาเลือกรูป !",
                        images : " &nbsp; กรุณาเลือกรูป !",
                        vdo_img : " &nbsp; กรุณาเลือกรูป",
                        vdo_file : "&nbsp; กรุณาเลือกไฟล์",
                        'spam[]': "เลือกอย่างน้อย 1 รายการ",
                        project : "กรุณาเลือกโครงการ",
                    }
                });

                $("#lead_img_file").rules("add", {
                    required:true,
                    messages: {
                        required: " &nbsp; ไม่มีรูป !"
                    }
                });

            });
        </script>
        <script type="text/javascript">

            function move() {

                    if (window.ActiveXObject) {
                        var fso = new ActiveXObject("Scripting.FileSystemObject");
                        var filepath = document.getElementById('vdo_file').value;
                        var thefile = fso.getFile(filepath);
                        var sizeinbytes = thefile.size;
                    } else {
                        var sizeinbytes = document.getElementById('vdo_file').files[0].size;

                    }
                    alert(sizeinbytes);

                    var fSExt = new Array('Bytes', 'KB', 'MB', 'GB');
                    fSize = sizeinbytes;
                    i = 0;
                    while (fSize > 900) {
                        fSize /= 1024;
                        i++;
                    }

                    var progess2 = (Math.round(fSize * 100) * 100) + ' ' + fSExt[i];

                    var progess = (Math.round(fSize * 100) / 100);
                    parseInt(progess);
                    //alert(progess);
                    var elem = document.getElementById("myBar");
                    var width = 1;
                    var id = setInterval(frame, 1000);

                    function frame() {
                        if (width >= 100) {
                            clearInterval(id);
                            //alert("OK");
                        } else {
                            width++;
                            //elem.style.width = width + '%';
                        }
                    }

                    $("#myProgress").show();

                }

        </script>

        <script type="text/javascript">
            $('.fancybox').fancybox();
        </script>

        <script type="text/javascript">
            $('.imgupload').imgupload();
        </script>
        <script type="text/javascript">
            $('#imgupload2').imgupload();
        </script>
        <script type="text/javascript">
            $('#imgupload1').imgupload();
        </script>




        <script>
            $(document).ready(function(){
                $("#alert").show();
                $("#alert").fadeTo(3000, 500).slideUp(500, function(){
                    $("#alert").alert('close');
                });
            });
        </script>


        <!-- jQuery Tags Input -->
        <script>
            function onAddTag(tag) {
                alert("Added a tag: " + tag);
            }

            function onRemoveTag(tag) {
                alert("Removed a tag: " + tag);
            }

            function onChangeTag(input, tag) {
                alert("Changed a tag: " + tag);
            }

            $(document).ready(function() {
                $('#tags_1').tagsInput({
                    width: 'auto'
                });
                $('#tags_2').tagsInput({
                    width: 'auto'
                });
            });
        </script>
        <!-- /jQuery Tags Input -->
        <script>
            $(document).ready(function(){
                $("#l1").show();
                $("#l2").hide();
                $("#l3").hide();
                $("#l4").hide();
                $("#l5").hide();
                $("#l1_2").show();
            });
        </script>

        <script>

            function getval(sel){
                $("#mySelect").val();
                if($("#mySelect").val() == "image"){
                    $("#l1").show();
                    $("#l2").hide();
                    $("#l3").hide();
                    $("#l4").hide();
                }else if($("#mySelect").val() == "banner"){
                    $("#l2").show();
                    $("#l3").hide();
                    $("#l1").hide();
                    $("#l4").hide();
                }else if($("#mySelect").val() == "youtube"){
                    $("#l3").show();
                    $("#l1").hide();
                    $("#l2").hide();
                    $("#l4").hide();
                }else if($("#mySelect").val() == "vdo"){
                    $("#l4").show();
                    $("#l1").hide();
                    $("#l2").hide();
                    $("#l3").hide();
                }
            }

            $('#project').click(function() {
                console.log($('#project').val());
            });

        </script>

        <script>
            function display_home_info() {
                var value = $("#home").attr("value");
                var isChecked = $("#home").prop("checked");
                if(isChecked == true){
                    $("#l1_2").show();
                }else{
                    $("#l1_2").hide();
                }
            }
            $(function() {
                display_cb_info();
                $("#home").change(display_home_info);
            });


            function display_cb_info() {
                var value = $("#project").attr("value");
                var isChecked = $("#project").prop("checked");
                if(isChecked == true){
                    $("#l5").show();
                }else{
                    $("#l5").hide();
                }
            }
            $(function() {
                display_cb_info();
                $("#project").change(display_cb_info);
            });
        </script>

        <script>
            $(document).ready(function(){
                $("#myProgress").hide();
            });
        </script>

</body>
</html>