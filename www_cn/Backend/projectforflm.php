<?php
session_start();
header( 'Content-Type:text/html; charset=utf8');

include("qc_project.php");
include "include/function_date.php";

error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once 'include/dbConnect.php';

$conn = (new dbConnect())->getConn();
$project_name_th = "";

if (isset($_GET['project_id'])) {

    $arrProjectName = getProjectsById($_GET['project_id']);
    $GLOBALS['project_name_th'] = $arrProjectName[0]['project_name_th'];
}

function getProjectsById($project_id) {
    try {
        $sql = 'select project_name_th from lh_projects where project_id='.$project_id;
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getBrand() {
    try {
        $sql = 'SELECT * FROM LH_BRANDS';
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}

function getZone() {
    try {
        $sql = 'SELECT * FROM LH_ZONES';
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}

function getProvince() {
    try {
        $sql = 'SELECT * FROM lh_provinces';
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}

function getAmphur() {
    try {
        $sql = 'SELECT * FROM lh_amphurs';
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}

function getDistrict() {
    try {
        $sql = 'SELECT * FROM lh_districts';
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}

function getTypeProjectImageByIdProject() {
    try {
        $sql = 'SELECT DISTINCT LEAD_IMAGE_PROJECT_FILE_TYPE
        FROM LH_LEAD_IMAGE_PROJECT_FILE
        WHERE PROJECT_ID = '.$_GET['project_id'];
        $result = $GLOBALS['conn']->query($sql);
        if($result !=null )
        {
            return $result->fetchAll();
        }

    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}

function getAllProjectImageByIdProject() {
    try {
        $sql = 'SELECT * FROM LH_LEAD_IMAGE_PROJECT_FILE
        WHERE PROJECT_ID = '.$_GET['project_id'];
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getGeleryByIdProject() {
    try {
        $sql = 'SELECT * FROM LH_GALERY_IMG_PROJECT
        WHERE GALERY_PROJECT_ID = '.$_GET['project_id'];
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getURL360ByIdProject() {
    try {
        $sql = 'SELECT * FROM LH_360_PROJECT
        WHERE project_id = '.$_GET['project_id'];
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getPositionGeleryByIdProject() {
    try {
        $sql = 'SELECT * FROM LH_GALERY_PROJECT
        WHERE PROJECT_ID = '.$_GET['project_id'];
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getAllCommunityFeature() {
    try {
        $sql = 'SELECT * FROM LH_COMMUNITY_FEATURES';
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getCommunityByIdProject() {
    try {
        $sql = 'SELECT COM.community_features_id, COM.project_sub_id, PS.product_id, PS.project_id
        FROM LH_COMMUNITY COM
        LEFT JOIN LH_PROJECT_SUB PS
        ON COM.project_sub_id = PS.project_sub_id
        LEFT JOIN LH_PROJECTS P
        ON PS.project_id = P.project_id
        WHERE PS.project_id = '.$_GET['project_id'];
        $sql .=' AND PS.product_id = 3';
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getAllRoomFeature() {
    try {
        $sql = 'SELECT * FROM LH_ROOM_FEATURES';
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getFeatureByIdProject() {
    try {
        $sql = 'SELECT FT.room_features_id, FT.project_sub_id, PS.product_id, PS.project_id
        FROM LH_FEATURES FT
        LEFT JOIN LH_PROJECT_SUB PS
        ON FT.project_sub_id = PS.project_sub_id
        LEFT JOIN LH_PROJECTS P
        ON PS.project_id = P.project_id
        WHERE PS.project_id = '.$_GET['project_id'];
        $sql .=' AND PS.product_id = 3';
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function checkTypeProductByIdProject() {
    try {
        $sql = 'SELECT PS.product_id FROM LH_PROJECT_SUB PS
        LEFT JOIN LH_PROJECTS P
        ON PS.project_id = P.project_id
        LEFT JOIN LH_PRODUCTS PD
        ON PS.product_id = PD.product_id
        WHERE PS.project_id = '.$_GET['project_id'];
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

$group_id=$_SESSION['group_id'];
$_SESSION['project_id'] = $_GET['project_id'];
$_GET['page']='form';

?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>
    <style>
    .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        width: 80px;
        top: -1px;
        right: 0;
        left: 0;
        min-width: 100%;
        min-height: 100%;
        padding: 6px 12px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }
    .thumb-image{float:left;width:100px;position:relative;padding:5px;}
    textarea {
       resize: none;
   }
   .file-drop-zone-title {
    padding: 0px 0px !important;
}
</style>


<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">

<!-- Bootstrap -->
<link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
<!-- iCheck -->
<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
<!-- Datatables -->
<link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="../build/css/custom.min.css" rel="stylesheet">

<link href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">

<link href="js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
<!-- Fancybox image popup -->
<link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
<style>
.fileinput-remove-button {
    font-size: 14px;
    background-color: #F44336;
}
div.btn.btn-primary.btn-file {
    font-size: 14px;
    margin-left: -5px;
    border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;
}
.kv-fileinput-caption {
    font-size: 16px;
}
</style>
</head>

<body class="nav-md">
	<div class="container body">
     <div class="main_container">
         <div class="col-md-3 left_col">
             <div class="left_col scroll-view">
                 <div class="navbar nav_title" style="border: 0;">
                     <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
                 </div>
                 <div class="clearfix"></div>
                 <?php include "master/navbar.php"; ?>
             </div>
         </div>
         <!-- top navigation -->
         <?php include './master/top_nav.php'; ?>
         <!-- /top navigation -->

         <!-- page content class=form-->
         <div class="right_col" role="main">
          <!-- class="" -->
          <div class="">
             <div class="clearfix"></div>
             <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><?php echo $GLOBALS['project_name_th'];?></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p class="font-gray-dark"></p>
                            <h4><u>สำหรับ ระบบ FLM</u></h4>
                            <form id="form-project" class="form-horizontal form-label-left">
                                <!-- class=form-group -->
                                <div class="form-group">
                                    <label class="control-label col-md-2">URL รายละเอียดโครงการ</label>
                                    <div class="col-md-8">
                                        <!--                                                <a href="--><?php //echo $project[0]['project_url'] ?><!--" class="form-control" target="_blank" style=" background-color: #ececec; ">--><?php //echo $project[0]['project_url'] ?><!--</a>-->
                                        <input class="form-control" value="<?php echo $project[0]['project_url'] ?>" disabled>
                                        <label class="control-label col-md-3 wdlabel"></label>
                                    </div>
                                </div>
                                <!-- ./ class=form-group -->
                                <div class="form-group">
                                    <label class="control-label col-md-2" >เบอร์โทรสายตรงโครงการ
                                        <span class="required"></span>
                                    </label>
                                    <div class="col-md-2">
                                        <input type="text" name="telephone_line" value="<?php echo trim($detailContact[0]['telephone_line']); ?>"
                                        class="form-control col-md-7 col-xs-12" disabled>
                                    </div>
                                </div>
                                <!-- class=form-group -->
                                <div class="form-group">
                                    <label class="control-label col-md-2">ชื่อ-นามสกุลเจ้าหน้าที่ฝ่ายขายประจำโครงการ
                                    </label><br>
                                    <div class="col-md-6">
                                        <input type="text" name="name_staff" value="<?php echo trim($detailContact[0]['name_staff']); ?>"
                                        class="form-control col-md-7 col-xs-12" disabled>
                                    </div>
                                </div>
                                <!-- ./ class=form-group -->
                                <?php $sql_zone = "SELECT z.zone_name_th,z.zone_name_en FROM LH_PROJECT_ZONE p LEFT JOIN LH_ZONES z 
                                ON p.zone_id_flm = z.zone_id WHERE p.project_id = '".$_GET['project_id']."'";
                                $query = mssql_query($sql_zone);
                                while ($row = mssql_fetch_array($query)){

                                    ?>
                                    <!-- class=form-group -->
                                    <div class="form-group" id="district">
                                       <label class="control-label col-md-2" for="zone">ทำเลในระบบ FLM</label>
                                       <div class="col-md-6 col-sm-9 col-xs-12">

                                        <input class="form-control" value="<?=$row['zone_name_th']; ?>" disabled>
                                    </div>
                                </div>
                                <?php }?>
                                <!-- ./ class=form-group -->
                                <div class="form-group">
                                    <label class="control-label col-md-2">รหัสบริษัท (DL200)
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text" id="company_no_DL200_id" name="company_no_DL200"
                                        value="<?php echo trim($project[0]['company_no_DL200']); ?>"
                                        class="form-control col-md-7 col-xs-12" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">รหัสโครงการ (DL200)
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text" id="project_no_DL200_id" name="project_no_DL200"
                                        value="<?php echo trim($project[0]['project_no_DL200']); ?>"
                                        class="form-control col-md-7 col-xs-12" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">ค่าส่วนกลาง
                                    </label>
                                    <div class="col-md-3">
                                        <input type="text" name="central_value" value="<?php echo trim($detailContact[0]['central_value']); ?>"
                                        class="form-control col-md-7 col-xs-12" disabled>
                                    </div>
                                    <?php if(isset($array_unit['unit_id'])){ ?>
                                    <div class="col-md-2 col-sm-9 col-xs-12">
                                        <select name="unit_id" class="form-control" style="width: 170px;">
                                            <option value = "" selected >หน่วย</option >
                                            <?php
                                            foreach ($detailUnit as $value)
                                            {
                                                if($value['unit_id'] == $detailContact[0]['unit_id'] ) {
                                                    echo '<option value = "'.$value['unit_id'].'" selected >'.$value['unit_name'].'</option >';
                                                } else {
                                                    echo '<option value = "'.$value['unit_id'].'" >'.$value['unit_name'].'</option >';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <?php }else  { ?>
                                    <div class="col-md-2 col-sm-9 col-xs-12">
                                        <select name="unit_id" class="form-control" style="width: 170px;" disabled>
                                            <option value = "" selected >หน่วย</option >
                                            <?php
                                            foreach ($detailUnit as $value)
                                            {
                                                echo '<option value = "'.$value['unit_id'].'"  >'.$value['unit_name'].'</option >';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <?php }?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">ชำระล่วงหน้าถึงวันที่
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control has-feedback-left"
                                        name="prepay"
                                        value="<?php echo DateThai($detailContact[0]['prepay']); ?>"
                                        placeholder="ระบุวันชำระล่วงหน้า" disabled>
                                        <span class="fa fa-calendar-o form-control-feedback left"
                                        aria-hidden="true"></span>
                                        <span id="inputSuccess2Status"
                                        class="sr-only">(success)</span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /page content -->
            </div>
        </div>

        <!-- jQuery -->
        <script src="../vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../vendors/nprogress/nprogress.js"></script>
        <!-- iCheck -->
        <script src="../vendors/iCheck/icheck.min.js"></script>
        <!-- jQuery Tags Input -->
        <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
        <!-- Datatables -->
        <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
        <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
        <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
        <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
        <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
        <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
        <script src="../vendors/jszip/dist/jszip.min.js"></script>
        <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
        <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
        <!-- bootstrap-daterangepicker -->
        <script src="js/moment/moment.min.js"></script>
        <script src="js/datepicker/daterangepicker.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
        <script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>

        <!-- Custom Theme Scripts -->
        <script src="../build/js/custom.min.js"></script>

        <script src="js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
        <!-- Fancybox image popup -->
        <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>
    </body>
    </html>
