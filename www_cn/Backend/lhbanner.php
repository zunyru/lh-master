<?php
//Set show any error
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>
    <!DOCTYPE html>
    <?php
    session_start();
    //include './connect_db.php';
    $group_id=$_SESSION['group_id'];

    $_GET['page']='banner';
    ?>
    <html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">

</head>
<?php
$_GET ['title_page'] = "LAND & HOUSES";
$_GET ['page'] = 'lead_page';
?>
<?php 
//include('header.php');
require_once 'include/function_query.php';
include 'include/function_date.php';
?>
<!-- <script src="js/lhbanner.js"></script> -->
<link href="css/lhbanner.css" rel="stylesheet">
<body class="nav-md">
<div class="container body">
    <div class="main_container">
    <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
                <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- sidebar menu -->
            <?php include 'master/navbar.php';?>
            <!-- /sidebar menu -->


            <!-- /menu footer buttons -->
        </div>
    </div>

    <!-- top navigation -->
    <?php include './master/top_nav.php'; ?>
    <!-- /top navigation -->

    <!-- page content -->
<!-- page content -->
<div class="right_col" role="main">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>ระบบจัดการข้อมูล Banner</h2>

					<div class="clearfix"></div>
				</div>
				<!-- start content -->
				<div class="x_content">
				    <?php if(isset($_GET['saveAdd'])){?>
                   <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>สร้างข้อมูลเรียบร้อยแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-6"></div>
                   </div>
                  <?php }else if(isset($_GET['saveEdit'])){?>
                   <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>แก้ไขข้อมูลเรียบร้อยแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-6"></div>
                   </div>
                    <?php }else if(isset($_GET['delData'])){?>
                   <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>ลบข้อมูลเรียบร้อยแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-6"></div>
                   </div>
                   <?php } ?>
                   <!-- end dialog save|update message -->
					<div class="" role="tabpanel" data-example-id="togglable-tabs">
						<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
							<li role="presentation" class="active"><a href="#tab_content1"
								id="Project" role="tab" data-toggle="tab" aria-expanded="true">Banner</a>
							</li>
							<li role="presentation" class=""><a href="#tab_content2"
								role="tab" id="Highlights" data-toggle="tab"
								aria-expanded="false">Highlights</a></li>

						</ul>
						<!-- render tab -->
						    <div id="myTabContent" class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="Project">
                                        <div class="form-group">
                                            <div class="col-md-10" >

                                            </div>
                                            <div class="col-md-2" >
                                                <a href="lhbanner_add.php?pagestate=Add"><button type="button" class="btn btn-success flright">สร้างหน้า Banner +</button></a>
                                            </div>
                                        </div>

                                        <!-- <p class="text-muted font-13 m-b-30">
                                          DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>
                                        </p> -->
                                        <table id="datatable" class="table table-striped table-bordered">
                                            <thead>
                                            <tr>
                                                <th>หน้าที่แสดง</th>
                                                <th>ประเภทชิ้นงาน</th>
                                                <th>วันที่เริ่ม - สิ้นสุด</th>
                                                <th >วันที่แก้ไขล่าสุด</th>

                                            </tr>
                                            </thead>

                                            <tbody>
                                               <?php
                                                 $listbanner=getBannerListAll();
                                                 //die(var_dump($listbanner));
                                                 if(isset($listbanner)){
                                                 	foreach($listbanner as $data){

                                                        $date=date_create($data->startdate);
                                                        $strDate =date_format($date,"Y-m-d H:i:s");

                                                        $date_update=date_create($data->updatedate);
                                                        $strDate_update =date_format($date_update,"Y-m-d H:i:s");
                                                ?>
                                                <tr>
                                                 <td>
                                                        <a href="lhbanner_update.php?bannerMainId=<?=$data->banner_main_id ?>">
                                                           <?=BannerHelper::getPageTypeById($data->type_page_id) ?>
                                                        </a></td>
                                                       <td>                                        
                                                        <a href="lhbanner_update.php?bannerMainId=<?=$data->banner_main_id?>">
                                                            <?=BannerHelper::getTypeShowById($data->type_show_id) ?>
                                                        </a></td>
                                                           <td><p style="display: none"><?=$strDate; ?>
                                                        <a href="lhbanner_update.php?bannerMainId=<?=$data->banner_main_id?>">
                                                            <p class="hidden"><?=$strDate?></p>
                                                            <?=$strDate != '1900-01-01 00:00:00' ? DateThai($data->startdate)." - ".DateThai($data->enddate) : 'ไม่มีกำหนด';?>
                                                        </a></td>
                                                           <td><p class="hidden"><?=$strDate_update; ?></p>
                                                        <a href="lhbanner_update.php?bannerMainId=<?=$data->banner_main_id?>">
                                                            <?=DateThai_time($data->updatedate) ?> 
                                                        </a>
                                                   </tr>
                                                <?php } }?>
                                            </tbody>
                                        </table>
                                        <br><br>
                                    </div>
                                      <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="Highlights">
                                                                                 <div class="form-group">
                                            <div class="col-md-9" >

                                            </div>
                                            <div class="col-md-3" >
                                                <a href="highlights_add.php"><button type="button" class="btn btn-success" >สร้างข้อมูล Highlight +</button></a>
                                            </div>
                                        </div>
                                        <table id="Project_tb" class="table table-striped table-bordered">
                                            <thead>
                                            <tr>
                                                <th>ชื่อ Highlights</th>
                                                <th >รายละเอียด</th>
                                                <th >วันที่เริ่ม - สิ้นสุด</th>
                                                <th >วันที่แก้ไขล่าสุด</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $sql_pr="SELECT highlights_id,highlights_name_th,highlights_name_en,highlights_dis_th 
		                                             ,start_date,end_date,highlights_update FROM LH_HIGHLIGHTS ";
                                            $query_pr = mssql_query($sql_pr);
                                            while ( $row_pr = mssql_fetch_array($query_pr)) {

                                                $date=date_create($row_pr['highlights_update']);
                                                $strDate =date_format($date,"Y-m-d H:i:s");

                                                $date2=date_create($row_pr['start_date']);
                                                $strDate2 =date_format($date2,"Y-m-d H:i:s");


                                                ?>
                                                <tr>
                                                    <td>
                                                        <a href="highlights_update.php?id=<?=$row_pr['highlights_id']?>">
                                                            <?php
                                                            echo $row_pr['highlights_name_th']; if($row_pr['highlights_name_en'] !=''){" (".$row_pr['highlights_name_en'].")";}
                                                            ?>
                                                        </a></td>
                                                    <td><a href="highlights_update.php?id=<?=$row_pr['highlights_id']?>">
                                                            <?php
                                                            $str = strlen($row_pr['highlights_dis_th']);
                                                            if($str >= 50){
                                                                echo iconv_substr($row_pr['highlights_dis_th'], 0,50, "UTF-8")."...";
                                                            }else{
                                                                echo $row_pr['highlights_dis_th'];
                                                            }
                                                            ?>
                                                        </a></td>
                                                    <td><p class="hidden"><?=$strDate2?></p>
                                                    <a href="highlights_update.php?id=<?=$row_pr['highlights_id']?>"><?=DateThai($row_pr['start_date'])." - ".DateThai($row_pr['end_date']);?></a></td>
                                                    <td align="center"><p class="hidden"><?=$strDate?></p>
                                                        <a href="highlights_update.php?id=<?=$row_pr['highlights_id']?>"><?=DateThai_time($row_pr['highlights_update']);?></a></td>
                                                </tr>
                                            <?php  }  ?>
                                            </tbody>
                                        </table>
                                      </div>
						  </div>
						<!-- end render tab -->
					</div>
				</div>
				<!-- end content -->
			</div>
		</div>
	</div>
</div><!-- end page content -->
    </div>
</div>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../vendors/jszip/dist/jszip.min.js"></script>
<script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

<!-- Custom Theme Scripts -->
<script src="../build/js/custom.min.js"></script>

<script>
    $(document).ready(function(){
        $("#alert").show();
        $("#alert").fadeTo(3000, 400).slideUp(500, function(){
            $("#alert").alert('close');
        });
    });
</script>

<!-- Datatables -->
<script>
    $(document).ready(function() {
        var handleDataTableButtons = function() {
            if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                    dom: "Bfrtip",
                    buttons: [
                        {
                            extend: "copy",
                            className: "btn-sm"
                        },
                        {
                            extend: "csv",
                            className: "btn-sm"
                        },
                        {
                            extend: "excel",
                            className: "btn-sm"
                        },
                        {
                            extend: "pdfHtml5",
                            className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                    ],
                    responsive: true
                });
            }
        };

        TableManageButtons = function() {
            "use strict";
            return {
                init: function() {
                    handleDataTableButtons();
                }
            };
        }();

        $('#datatable').dataTable(
            {
                "order": [[ 3, 'desc' ]],
                "bLengthChange": false,
                "pageLength": 50
            });
        $('#Project_tb').dataTable(
            {
                "order": [[ 3, 'desc' ]],
                "bLengthChange": false,
                "pageLength": 50
            });

        $('#datatable-keytable').DataTable({
            keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
            ajax: "js/datatables/json/scroller-demo.json",
            deferRender: true,
            scrollY: 380,
            scrollCollapse: true,
            scroller: true
        });

        $('#datatable-fixed-header').DataTable({
            fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');


        $datatable.dataTable({


        });
        $datatable.on('draw.dt', function() {
            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-green'
            });
        });

        TableManageButtons.init();
    });
</script>
<!-- /Datatables -->
</body>
</html>