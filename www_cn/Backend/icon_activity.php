<?php
session_start();
include './connect_db.php';
include './include/dbCon_mssql.php';
$group_id=$_SESSION['group_id'];
include_once('include/dbIcon_Activity.php');
require_once './include/function_date.php';
$_GET['page']='homemodel';
$funtion_icon = new dbIcon_Activity();
if(isset($_GET['id'])){
  $id=$_GET['id'];
  $de=$funtion_icon->delete_activity($id);
  if($de){
    include './include/dbCon_mssql.php';
    $query_3 ="SELECT icon_img FROM dbo.LH_ICON_ACTIVITY;";
    $query_result3= mssql_query($query_3 , $db_conn);
    $row = mssql_fetch_array($query_result3);

    $icon_img=$row['icon_img'];
    $path="fileupload/images/icon_img/"; ;
    @$flgDelete = unlink($path.$icon_img);

    $delete='';
  }
}

if (isset($_SESSION['add'])) {
  if($_SESSION['add'] ==1){
        //header("location:product_add.php");
    $success ="success";
    unset($_SESSION["add"]);
  } else if($_SESSION['add'] ==2){

        $yum="error"; //ค่าซ้ำ
        $_SESSION['add']='';
        unset($_SESSION["add"]);
      } else if($_SESSION['add'] ==0){

        $error="error"; //ค่าซ้ำ
        $_SESSION['add']='';
        unset($_SESSION["add"]);

      }
    }

    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <!-- Meta, title, CSS, favicons, etc. -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>LAND & HOUSES</title>

      <!-- Bootstrap -->
      <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome -->
      <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <!-- NProgress -->
      <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
      <!-- bootstrap-progressbar -->
      <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
      <!-- Custom Theme Style -->
      <link href="../build/css/custom.min.css" rel="stylesheet">
      <!-- Fancybox image popup -->
      <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
      <!-- jQuery -->
      <script src="../vendors/jquery/dist/jquery.min.js"></script>
      <!-- Fancybox image popup -->
      <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
      <!-- uploade img -->
      <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
      <!--uploade-->
      <!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
      <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>

      <!-- Datatables -->
      <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

      <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
      <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>
      <!-- confirm-->
      <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
      <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>

      <style type="text/css">

      th.next.available{
        background-color: #6f9755;
      }
      th.prev.available{
        background-color: #6f9755;
      }
      .file-caption-main .btn-file {
        overflow: visible;
      }

      .file-caption-main .btn-file .error {
        position: absolute;
        bottom: -32px;
        right: 30px;
      }
      .form-control[readonly] { /* For Firefox */
        background-color: white;
      }

      .form-control[readonly] {
        background-color: white;
      }
    </style>

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <?php include './master/navbar.php';?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include './master/top_nav.php'; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <a href="icon_activity.php"><h2>เพิ่มไอคอนกิจกรรม</h2></a>
                    <div class="clearfix"></div>
                  </div>
                  <?php if(isset($_GET['add'])){?>
                  <div class="row">
                    <div class="col-md-2"></div>
                    <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>สร้างข้อมูลเรียบร้อยแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-6"></div>
                  </div>
                  <?php }else if(isset($yum)){?>
                  <div class="row">
                    <div class="col-md-2"></div>
                    <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>มีชื่อนี้อยู่ในระบบแล้ว !</strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                  </div>
                  <?php }else if(isset($error)){?>
                  <div class="row">
                    <div class="col-md-2"></div>
                    <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>เกิดข้อผิดพลาดในการบันทึกข้อมูล !</strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                  </div>
                  <?php }?>
                  <?php if(isset($delete)){?>
                  <div class="row">
                    <div class="col-md-2"></div>
                    <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>ลบข้อมูลเรียบร้อย !</strong>
                      </div>
                    </div>
                    <div class="col-md-6"></div>
                  </div>
                  <?php }if(isset($_GET['update'])){?>
                  <div class="row">
                    <div class="col-md-2"></div>
                    <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>แก้ไขข้อมูลเรียบร้อยแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-6"></div>
                  </div>
                  <?php }?>
                  <div class="x_content">
                    <a href="series.php"><button type="button" class="btn btn-default">ข้อมูลสไตล์บ้าน (Series)</button></a>
                    <a href="homemodel.php"><button type="button" class="btn btn-default">ข้อมูลแบบบ้าน</button></a>
                    <a href="fucntion_home.php"><button type="button" class="btn btn-default">เพิ่มฟังก์ชั่นบ้าน</button></a>
                    <a href="fucntion_condo.php"><button type="button" class="btn btn-default">ฟังก์ชั่นคอนโด</button></a>
                    <a href="icon_activity.php"><button type="button" class="btn btn-success">ไอคอนกิจกรรม</button></a>
                    <a href="master_gallery_homesell.php"><button type="button" class="btn btn-default">ข้อมูลรูปบ้านตกแต่งพร้อมขาย</button></a>
                    <br>


                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th><center>ชื่อกิจกรรม</center></th>
                          <th><center>วันที่เริ่ม - วันที่สิ้นสุด</center></th>
                          <th><center>Delete</center></th>
                          <th><center>วันที่แก้ไขล่าสุด</center></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        require_once './include/function_date.php';
                        $query_3 ="SELECT * FROM LH_ICON_ACTIVITY;";
                        $stmt= mssql_query($query_3 , $db_conn);

                        if($stmt){
                          while ( $row = mssql_fetch_array($stmt)){
                              //print_r($row);
                           $date=$row['icon_date_start'];
                           $year = substr($date, 0, 4);
                           $mont = substr($date, 5, 2);
                           $day = substr($date, 8, 2);
                           $strDate1= $year."-".$mont."-".$day;

                           $date2=$row['icon_date_end'];
                           $year2 = substr($date2, 0, 4);
                           $mont2 = substr($date2, 5, 2);
                           $day2 = substr($date2, 8, 2);
                           $strDate2= $year2."-".$mont2."-".$day2;

                           $date3=date_create($row['update_date']);
                           $strDate =date_format($date3,"Y-m-d H:i:s");
                           ?>
                           <tr>
                            <td>
                              <p style="display: none"><?=$row['icon_id']?></p>
                              <a href="icon_activity.php?icon_id=<?=$row['icon_id']?>"><?=$row['icon_name']; ?></a></td>
                              <td><center><a href="icon_activity.php?icon_id=<?=$row['icon_id']?>"><?=DateThai($strDate1)." - ".DateThai($strDate2);?></a></center></td>
                              <td><center><u style="color:#FF0000">
                                <a class="confirms2" data-title="ยืนยันการลบข้อมูล" name="delete"  href="icon_activity.php?id=<?=$row['icon_id']?>">Delete</a>

                              </u></center></td>
                              <td><p class="hidden"><?=$strDate?></p>
                                <center><?=DateThai_time($row['update_date'])?></center></td>
                              </tr>
                              <?php }} ?>
                            </tbody>
                          </table><br>

                          <script type="text/javascript">
                            $('a.confirms2').confirm({
                              content: 'คุณแน่ใจที่จะลบข้อมูลนี้ !',
                              buttons: {
                                Yes: {
                                  text: 'Yes',
                                  btnClass: 'btn-danger',
                                  keys: ['enter', 'a'],
                                  action: function(){
                                    location.href = this.$target.attr('href');
                                  }
                                },
                                No: {
                                  text: 'No',
                                  btnClass: 'btn-default',
                                  keys: ['enter', 'a'],
                                  action: function(){
                                        // button action.
                                      }
                                    },

                                  }
                                });
                              </script>

                              <?php
                              if(!empty($_GET['icon_id'])){
                                $query_up ="SELECT * FROM LH_ICON_ACTIVITY WHERE icon_id = '".$_GET['icon_id']."' ";
                                $stmt= mssql_query($query_up , $db_conn);
                                $row_up = mssql_fetch_array($stmt);
                        //print_r($row_up);
                                $date=$row_up['icon_date_start'];
                                $year = substr($date, 0, 4);
                                $mont = substr($date, 5, 2);
                                $day = substr($date, 8, 2);
                                $strDate= $day."/".$mont."/".$year;

                                $date2=$row_up['icon_date_end'];
                                $year2 = substr($date2, 0, 4);
                                $mont2 = substr($date2, 5, 2);
                                $day2 = substr($date2, 8, 2);
                                $strDate2= $day2."/".$mont2."/".$year2;

                                $icon = 'แก้ไข ข้อมูล ICON '." : ".$row_up['icon_name'];
                                $action = 'update_icon_activity.php';

                              }else{
                                $icon = 'สร้างข้อมูล ICON';
                                $action = 'save_icon_activity.php';
                              }


                              ?>

                              <h4>ข้อมูล ICON กิจกรรม > <?=$icon; ?></h4>

                              <form class="form-horizontal form-label-left" action="<?=$action?>" method="post" enctype="multipart/form-data" id="commentForm">
                                <div class="form-group">
                                  <label class="control-label col-md-2" for="first-name">ชื่อ ICON กิจกรรม <span class="required">*</span>
                                  </label>
                                  <div class="col-md-6">
                                    <input type="text"  name="icon_name" class="form-control col-md-7 col-xs-12" required="required"
                                    value="<?php echo ''.(!empty($_GET['icon_id'])? $row_up['icon_name'] : "" ).'';?>">
                                  </div>
                                </div>

                                <div class="form-group">
                                 <label class="control-label col-md-2" for="first-name">รูป ไอคอน กิจกรรม<br>(.jpg , .png , .gif <br> ขนาด 150 x 85 px) <br>ภาพขนาดไม่เกิน 300 KB <span class="required">*</span>
                                 </label>
                                 <div class="col-md-6">
                                   <div id="map-image-holder-brochure"></div>
                                   <?php
                                   $required ='required';
                                   if(isset($row_up['icon_img']) && $row_up['icon_img']!= '') { $required ='';?>

                                   <div class="file-preview ">
                                     <div class="close fileinput-remove"></div>
                                     <div class="file-drop-disabled">
                                       <div class="file-preview-thumbnails">
                                         <div class="file-initial-thumbs">
                                           <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                             <div class="kv-file-content">
                                               <a class="fancybox" href="fileupload/images/icon_img/<?php echo $row_up['icon_img'];?>">
                                                 <img src="fileupload/images/icon_img/<?php echo $row_up['icon_img'];?>" class="kv-preview-data file-preview-image"  style="width:150px;height:auto;">
                                               </a>
                                             </div>
                                             <div class="file-thumbnail-footer">
                                               <div class="file-actions">
                                                <div class="file-footer-buttons">
                                                  <button type="button" id="<?php echo $row_up['icon_id']; ?>" onclick="deleteIconActivity(<?php echo $row_up['icon_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                  <a class="fancybox" href="fileupload/images/icon_img/<?php echo $row_up['icon_img'];?>">
                                                    <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                  </a>
                                                </div>
                                                <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                              </div>
                                            </div>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="clearfix"></div>
                                      <div class="file-preview-status text-center text-success"></div>
                                      <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                    </div>
                                  </div>
                                  <?php } ?>
                                  <input type="file" id="img_icon" name="fileupload_th" class="file-loading"  <?=$required; ?>>
                                </div>
                              </div>

                              <script>
                               $("#img_icon").fileinput({
                               uploadUrl: "upload.php", // server upload action
                               maxFileCount: 1,
                               allowedFileExtensions: ["jpg", "png", "jpeg" , "gif"],
                               browseLabel: 'เลือกรูป',
                               removeLabel: 'ลบ',
                               browseClass: 'btn btn-success',
                               showUpload: false,
                               showRemove:false,
                               showCaption: false,
                               msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                               msgZoomModalHeading: 'ตัวอย่างละเอียด',
                               xxx:'seo_lead_img_file',
                               dropZoneTitle : 'รูปไอคอนกิจกรรม',
                               minImageWidth: 150, //ขนาด กว้าง ต่ำสุด
                               minImageHeight: 85, //ขนาด ศุง ต่ำสุด
                               maxImageWidth: 150, //ขนาด กว้าง ต่ำสุด
                               maxImageHeight: 85, //ขนาด ศุง ต่ำสุด
                               maxFileSize :300 ,
                               msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                               msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                               msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                               msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                               msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                             });
                           </script>


                           <div class="form-group">
                             <label class="control-label col-md-2" for="first-name">ระบุวันเริ่ม <span class="required">*</span>
                             </label>
                             <div class="col-md-2">
                               <fieldset>
                                 <div class="input-prepend input-group">
                                   <input type="hidden" value="<?=$_GET['icon_id'];?>" name="id">
                                   <input type="text" style="width: 180px" name="date_start" required id="date_start" class="form-control" readonly value="<?php echo ''.(!empty($_GET['icon_id'])? $strDate : "" ).'';?>" />
                                 </div>
                               </fieldset>
                             </div>
                             <label class="control-label col-md-2" for="first-name" style="margin-left: -28px;">ระบุวันสิ้นสุด <span class="required" aria-required="true">*</span></label>
                             <div class="col-md-2">
                               <fieldset>
                                 <div class="input-prepend input-group">
                                   <input type="text" style="width: 180px;" name="date_end" required="" id="date" class="form-control " readonly  value="<?php echo ''.(!empty($_GET['icon_id'])? $strDate2 : "" ).'';?>" aria-required="true" />
                                 </div>
                               </fieldset>
                             </div>
                           </div>


                           <br>

                           <div class="form-group">
                            <label class="control-label col-md-3" for="first-name">
                            </label>
                            <div class="col-md-4">
                              <?php if(!empty($_GET['icon_id'])){ ?>
                              <button type="submit" name="save" class="btn btn-success">Update</button>
                              <?php }else{ ?>
                              <button type="submit" name="save" class="btn btn-success">Submit</button>
                              <?php } ?>
                            </div>
                          </div>

                        </form>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /page content -->

                <!-- footer content -->
        <!-- <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer> -->
        <!-- /footer content -->
      </div>
    </div>

    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- date -->
    <link rel="stylesheet" type="text/css" href="../build/css/jquery-ui.css"/>
    <script src="../build/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>



    <!-- Fancybox image popup -->
    <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>
    <script src="../build/js/jquery.validate.js"></script>
    <script src="../build/js/custom.min.js"></script>

    <script type="text/javascript">
      $('.fancybox').fancybox();
    </script>

    <script>
      $(document).ready(function(){
        $("#alert").show();
        $("#alert").fadeTo(3000, 500).slideUp(500, function(){
          $("#alert").alert('close');
        });
      });
    </script>

    <!-- Datatables -->
    <script>
      $(document).ready(function() {


        $('#datatable').dataTable({
          "bLengthChange": false,
          "pageLength": 50,
          "order": [[ 3, "DESC" ]]
        });


      });
    </script>

    <script type="text/javascript">

      $('#date_start').datepicker({
        dateFormat : "dd/mm/yy",
        autoclose: true,
        changeMonth: true,
        showDropdowns: true,
        changeYear: true,
        minDate:  new Date(),
        onSelect: function(dateText) {
          var d = new Date($(this).datepicker("getDate"));
          $("input#date").datepicker('option', 'minDate', dateText);
          $("input#date").prop('disabled', false);

              //$("#date").datepicker('option', 'minDate', min);
              //$scope.value.posted = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate();
            }
          });
      $('#date').datepicker({
        dateFormat : "dd/mm/yy",
        autoclose: true,
        changeMonth: true,
        showDropdowns: true,
        changeYear: true,
        minDate:  new Date(),
        onSelect: function(dateText) {
          var d = new Date($(this).datepicker("getDate"))
          $("input#date_start").datepicker('option', 'maxDate', dateText);
              //$("input#date_start").prop('disabled', false);
            }
          });
        </script>


        <!-- Validate -->

        <script>
          $(document).ready(function() {

            $("#commentForm").validate({
              rules: {
                icon_name: "required",
                          //fileupload_th: "required",
                          reservation : "required",
                          date_start : "required",
                          date_end: "required",
                        },
                        messages: {
                          icon_name: "กรุณากรอกชื่อ ไอคอนกิจกรรม !",
                          reservation : "กรุณา ระบุวันเริ่ม - สิ้นสุด !",
                          fileupload_th : " &nbsp; กรุณาเลือกรูป !",
                          date_start : "กรุณาระบุวันที่เริ่ม  !",
                          date_end: "กรุณาวันที่สิ้นสุด !",

                        }
                      });

            $("#lead_img_file").rules("add", {
              required:true,
              messages: {
                required: " &nbsp; ไม่มีรูป !"
              }
            });

          });
        </script>
        <script>
          function deleteIconActivity(id) {
            $.confirm({
              title: 'ลบรูปนี้ !',
              content: 'คุณแน่ใจที่จะลบรูปนี้!',
              buttons : {
                Yes: {
                  text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                              var self = this;
                              return $.ajax({
                                url: 'ajax_deleteIconActivity.php?icon_id=' + id,
                                dataType: 'json',
                                method: 'get'
                              }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                  }).fail(function () {
                                    self.setContent('Something went wrong.');
                                  });
                                }
                              },
                              No: function () {
                            //$.alert('ยกลเิก!');
                          },
                        }
                      });
          }
        </script>
        <script src="js/validate_file_300kb.js"></script>


      </body>
      </html>