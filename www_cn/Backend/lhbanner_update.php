<?php
session_start();
include './include/dbCon_mssql.php';
require_once 'include/function_query.php';
$group_id = $_SESSION['group_id'];

$_GET['page']='lead_image';

$bannerMainId=isset ( $_GET['bannerMainId'] ) ? $_GET['bannerMainId'] : '';
$bannerMain=getBannerMainById($bannerMainId);

if(empty($bannerMain))
    return;
$defaultBannerImg=getBannerMainDefaultLeadImage($bannerMain->type_page_id);
$startdate=$bannerMain->startdate ;
$enddate=$bannerMain->enddate ;
if (! empty ( $startdate ) && ! empty ( $enddate )) {
    $startdate = new DateTime($startdate);
    $enddate =  new DateTime($enddate);
    // startdate->format("Y-m-d")
    $startdate = $startdate->format('d/m/Y');
    $enddate = $enddate->format('d/m/Y');
}



if (isset($_SESSION['add'])) {
    if($_SESSION['add'] ==1){
//header("location:product_add.php");
        $success ="success";
        unset($_SESSION["add"]);
    } else if($_SESSION['add'] ==2){

$error_name="error_name"; //ค่าซ้ำ
unset($_SESSION["add"]);


} else if($_SESSION['add'] ==-1){
    $error_img="errors_img";
    unset($_SESSION["add"]);
} else if($_SESSION['add'] ==-2){
    $error_up="error_up";
    unset($_SESSION["add"]);
}
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <!-- Fancybox image popup -->
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
    <!-- bootstrap-progressbar -->
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>

    <!-- uploade img -->
    <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
    <!--uploade-->
    <!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
    <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
    <!-- confirm-->
    <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
    <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>

    <!-- Include Editor style. -->
    <link href="editor/css/froala_editor.min.css" rel="stylesheet" type="text/css" />
    <link href="editor/css/froala_style.min.css" rel="stylesheet" type="text/css" />

    <!--  froala  -->
    <link href="libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="libs/codemirror/5.3.0/codemirror.min.css">
    <link href="libs/froala-editor/2.6.2/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
    <link href="libs/froala-editor/2.6.2/css/froala_style.min.css" rel="stylesheet" type="text/css" />

    <!-- Include Editor Plugins style. -->
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/char_counter.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/code_view.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/colors.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/emoticons.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/file.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/fullscreen.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/image.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/image_manager.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/line_breaker.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/quick_insert.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/table.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/video.css">

    <script id="fr-fek">try{(function (k){localStorage.FEK=k;t=document.getElementById('fr-fek');t.parentNode.removeChild(t);})('inkH3fiA6B-13a==')}catch(e){}</script>

    <link rel="stylesheet" href="../build/css/huebee.css">
    <!-- or -->

    <script src="../build/js/huebee.pkgd.min.js"></script>

    <style type="text/css">
    .fr-toolbar.fr-desktop.fr-top.fr-basic.fr-sticky.fr-sticky-off {
        background: #f7f7f7  !important;
        border-top: 5px solid #f5f5f5  !important;
    }
    th.next.available {
        background: #6f9755;
    }
    th.next.available:hover {
        background: #9ab688;
    }
    th.prev.available {
        background: #6f9755;
    }
    th.prev.available:hover {
        background: #9ab688;
    }
    .thumbnail {
        height: 100px;
        margin: 5px;
    }
    .fileUpload {
        position: relative;
        overflow: hidden;
        //margin: 10px;
    }
    .fileUpload input.upload {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
    .file-drop-zone-title {
        padding: 0px 0px !important;
    }
    .form-control[readonly] { /* For Firefox */
        background-color: white;
    }

    .form-control[readonly] {
        background-color: white;
    }

    .file-caption-main .btn-file {
        overflow: visible;
    }

    .file-caption-main .btn-file .error {
        position: absolute;
        bottom: -32px;
        right: 30px;
    }
    .huebee__container {
        top: -300px; /* position */
        left: 100px;
    }
    .huebee__cursor {
        width: 30px;
        height: 30px;

    }
    input#seo{
        display: none;
    }

    .disable_page{
        cursor: not-allowed;
    }
    .disable_page select {
        pointer-events: none;
        color: #9e9c9c;
    }
</style>
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
                    </div>

                    <div class="clearfix"></div>

                    <!-- menu profile quick info -->
                    <?php include './master/navbar.php';?>
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <?php include './master/top_nav.php'; ?>
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2><a href="lhbanner.php">ระบบจัดการ Banner</a> > แก้ไข Banner</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <form class="form-horizontal form-label-left" method="POST" enctype="multipart/form-data" action="update_lhbanner.php" id="editBannerForm">

                                        <div class="form-group">
                                            <label class="control-label col-md-3" for="brand">หน้าจอที่แสดง <span
                                                class="required">*</span></label>
                                                <div class="col-md-4 disable_page">
                                                    <select class="form-control" name="PageTypeId" required id="page" >
                                                        <option value="">เลือกหน้าจอ</option>
                                                        <?php $arrayName = array('1' => 'หน้า Home','2'=>'บ้านเดี่ยว','3'=>'บ้านตกแต่งพร้อมขาย','4'=>'ข่าวโครงการใหม่','5'=>'ทาว์โฮม','6'=>'Ladawan','7'=>'โครงการในต่างจังหวัด','8'=>'คอนโดมิเนียม','9'=>'แบบบ้าน'); 
                                                        foreach ($arrayName as $key => $value) { 
                                                            $select ='';
                                                            if($key == $bannerMain->type_page_id){
                                                                $select = 'selected';
                                                            }

                                                            ?>
                                                            <option  value="<?=$key?>" <?=$select?>><?=$value?></option>
                                                            <?php       
                                                        } 
                                                        ?>
                                                        
                                                        
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3" for="brand">ตำแหน่งรูปภาพหลักของหน้า PC *
                                                    <br> (.jpg  ขนาด 1920 x 1080 px)
                                                    <br> ภาพขนาดไม่เกิน 300 KB
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="form-group bannerlead" id="bannerLead" >

                                                <div class="row">
                                                    <?php
                                                    $i=0;
                                                    $sql=" select  m.banner_main_id,l.* ";
                                                    $sql.=" from LH_BANNER_MAIN m left join LH_BANNER_LEAD l on m.banner_main_id = l.banner_main_id ";
                                                    $sql.=" where m.type_page_id= ".$bannerMain->type_page_id ." and l.lead_img_mobile = 'd' ";
                                                    $sql.=" order by l.seq_leade_img asc ";

                                                    $query_lead = mssql_query($sql);
                                                    while ($value_l = mssql_fetch_array($query_lead)) {
                                                        $i++;

                                                        ?>
                                                        <div class="col-sm-3">
                                                            <div class="form-group ">
                                                                <div class="col-md-12">
                                                                    <!--                                                            <div class="file-preview ">-->
                                                                        <div class="close fileinput-remove"></div>
                                                                        <div class="file-drop-disabled">
                                                                            <div class="file-preview-thumbnails">
                                                                                <div class="file-initial-thumbs">
                                                                                    <div class="file-preview-frame file-preview-initial" data-fileindex="init_0" data-template="image">
                                                                                        <div class="kv-file-content">
                                                                                            <a class="fancybox" rel="group_position_project" href="<?php echo $value_l['lead_path']; ?>">
                                                                                                <img src="<?php echo $value_l['lead_path']; ?>" class="kv-preview-data file-preview-image"  style="width:150px;max-height:150px;min-height:150px;">
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="file-thumbnail-footer">

                                                                                            <input type="text" name="banner_lead_seo_edit[]" class="form-control" placeholder="Alt Text" value="<?=$value_l['seo_lead']?>" >
                                                                                            <br>


                                                                                            <input type="hidden" name="banner_lead_id[]" value="<?=$value_l['banner_lead_id']?>">
                                                                                            <input type="file" id="lead_image_edit_<?=$i?>" name="lead_image_d_edit[]" accept="image/*"/>

                                                                                            <script>
                                                                                                $("#lead_image_edit_<?=$i?>").fileinput({
                                                    uploadUrl: "upload.php", // server upload action
                                                    maxFileCount: 1,
                                                    allowedFileExtensions: ["jpg", "png", "jpeg"],
                                                    browseLabel: ' เลือก รูปภาพหลัก PC',
                                                    browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                                                    removeLabel: 'ลบ',
                                                    browseClass: 'btn btn-success',
                                                    showUpload: false,
                                                    showRemove:false,
                                                    showCaption: false,
                                                    msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                    msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                                    msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                    xxx:'seo',
                                                    minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                                                    minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                                                    maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                                                    maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                                                    dropZoneTitle : 'รูปภาพหลัก PC',
                                                    maxFileSize :300 ,
                                                    msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                                    msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                                    msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                                    msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                                    msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.', 
                                                });
                                            </script>

                                            <span><?="ลำดับที่ ".$i;?></span>
                                            <div class="file-actions">
                                                <div class="file-footer-buttons">
                                                    <?php if($i != 1){?>
                                                    <button type="button" id="<?php echo $value_l['banner_lead_id']; ?>" onclick="deleteBanner_d(<?php echo $value_l['banner_lead_id'];?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                    <?php }?>
                                                    <a class="fancybox" href="<?php echo $value_l['lead_path']; ?>" target="_blank">
                                                        <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                    </a>
                                                </div>
                                                <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!--                                                                        </div>-->
                                </div>

                                <div class="clearfix"></div>
                                <div class="file-preview-status text-center text-success"></div>
                                <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="form-group ">
                    <div class="col-md-12">
                        <input type="text"  class="form-control" name="lead_image_d_url_edit[]" placeholder="EX : http://lh.co.th or https://lh.co.th" value="<?php echo $value_l['img_url'];?>" />
                    </div>
                </div>

            </div>
            <?php } ?>

        </div>

        <div class="row">
            <div id="lead_image_project_d"></div>
        </div>

        <br>
        <div class="form-group">
            <label class="control-label col-md-1">
            </label>
            <div class="col-md-4">
                <button id="lead_image_add" type="button" class="btn btn-round btn-success" onclick="add_lead_image_d()" >+
                    เพิ่มรูป รูปภาพหลัก PC
                </button>
            </div>
        </div>

    </div>

    <?php if($bannerMain->type_page_id == 1){ ?>
    <div id="mobile">
        <div class="form-group">
            <label class="control-label col-md-3" for="brand">ตำแหน่งรูปภาพหลักของหน้า Mobile *
                <br> (.jpg  ขนาด 1080 x 1920 px)
                <br> ภาพขนาดไม่เกิน 300 KB
            </label>
            <hr>
        </div>

        <div class="row">
            <?php
            $i=0;
            $sql=" select  m.banner_main_id,l.* ";
            $sql.=" from LH_BANNER_MAIN m left join LH_BANNER_LEAD l on m.banner_main_id = l.banner_main_id ";
            $sql.=" where  m.type_page_id= ".$bannerMain->type_page_id ." and l.lead_img_mobile = 'm' and m.lead_img_mobile = 'd' ";
            $sql.=" order by l.seq_leade_img asc ";


            $query_lead = mssql_query($sql);
            $num_mb_d = mssql_num_rows($query_lead);
            while ($value_l = mssql_fetch_array($query_lead)) {
                $i++;
//echo $value_l['lead_path'];
                ?>

                <div class="col-sm-3">
                    <div class="form-group ">
                        <div class="col-md-12">

                            <div class="close fileinput-remove"></div>
                            <div class="file-drop-disabled">
                                <div class="file-preview-thumbnails">
                                    <div class="file-initial-thumbs">
                                        <div class="file-preview-frame file-preview-initial" data-fileindex="init_0" data-template="image">
                                            <div class="kv-file-content">
                                                <a class="fancybox" rel="group_position_project" href="<?php echo $value_l['lead_path']; ?>">
                                                    <img src="<?php echo $value_l['lead_path']; ?>" class="kv-preview-data file-preview-image"  style="width:150px;max-height:150px;min-height:150px;">
                                                </a>
                                            </div>
                                            <div class="file-thumbnail-footer">

                                                <input type="text" name="banner_lead_d_m_seo_edit[]" class="form-control" placeholder="Alt Text" value="<?=$value_l['seo_lead']?>" >
                                                <br>
                                                <input type="hidden" name="lead_image_m_id[]" value="<?=$value_l['banner_lead_id']?>">
                                                <input type="file" id="lead_image_edit_M<?=$i?>" name="lead_image_m_edit[]" accept="image/*"/>

                                                <script>
                                                    $("#lead_image_edit_M<?=$i?>").fileinput({
                                uploadUrl: "upload.php", // server upload action
                                maxFileCount: 1,
                                allowedFileExtensions: ["jpg", "png", "jpeg"],
                                browseLabel: ' เลือก รูปภาพหลัก Mobile',
                                browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                                removeLabel: 'ลบ',
                                browseClass: 'btn btn-success',
                                showUpload: false,
                                showRemove:false,
                                showCaption: false,
                                msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                xxx:'seo',
                                minImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
                                minImageHeight: 1920, // ขนาด ศุง ต่ำสุด
                                maxImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
                                maxImageHeight: 1920, // ขนาด ศุง ต่ำสุด
                                dropZoneTitle : 'รูปภาพหลัก Mobile',
                                maxFileSize :300 ,
                                msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                            });
                        </script>

                        <span><?="ลำดับที่ ".$i;?></span>
                        <div class="file-actions">
                            <div class="file-footer-buttons">
                                <?php if($i != 1){?>
                                <button type="button" id="<?php echo $value_l['banner_lead_id']; ?>" onclick="deleteBanner_d(<?php echo $value_l['banner_lead_id'];?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                <?php }?>
                                <a class="fancybox" href="<?php echo $value_l['lead_path']; ?>" target="_blank">
                                    <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                </a>
                            </div>
                            <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                        </div>
                    </div>
                </div>
                <!--                                                                        </div>-->
            </div>

            <div class="clearfix"></div>
            <div class="file-preview-status text-center text-success"></div>
            <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
        </div>
    </div>
</div>
</div>

<div class="form-group ">
    <div class="col-md-12">
        <input type="text"  class="form-control" name="lead_image_d_m_url_edit[]" placeholder="EX : http://lh.co.th or https://lh.co.th" value="<?php echo $value_l['img_url'];?>" />
    </div>
</div>

</div>
<?php } ?>

</div>
<div class="row">
    <div class="form-group">

        <div id="lead_image_project_m"></div

        </div>
    </div>
    <?php if( $num_mb_d != 0){?>
    <br>
    <div class="form-group">
        <label class="control-label col-md-1">
        </label>
        <div class="col-md-4">
            <button id="lead_image_add" type="button" class="btn btn-round btn-success" onclick="add_lead_image()" >+
                เพิ่มรูป รูปภาพหลัก Mobile
            </button>
        </div>
    </div>
    <?php }?>
    <script>
        $("#lead_image0").fileinput({
uploadUrl: "upload.php", // server upload action
maxFileCount: 1,
allowedFileExtensions: ["jpg", "png", "jpeg"],
browseLabel: ' เลือก Lead image',
browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
removeLabel: 'ลบ',
browseClass: 'btn btn-success',
showUpload: false,
showRemove:false,
showCaption: false,
msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
msgZoomModalHeading: 'ตัวอย่างละเอียด',
xxx:'seo',
dropZoneTitle : 'Lead image',
maxFileSize :300 ,
msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
});
</script>
</div>
<?php } ?>

<hr>


<div class="form-group">

    <label class="control-label col-md-6" for="ch_banner">

        <?php 
        $banner_show =  getShowBanner_btn($bannerMainId);


        if(($banner_show->banner_lead_id == NULL ) && ($banner_show->type_show_id == 0)){
         $checked = '';
         $banner_leade_ch = 'hidden';
     }elseif(($banner_show->banner_lead_id != NULL ) && ($banner_show->type_show_id == 0)){
         $checked = 'checked'; 
         $banner_leade_ch = 'show';    
     }else{
         $checked = 'checked';
         $banner_leade_ch = 'show';   
     } 

     ?> 
     <input type="hidden" id="banner_chek" value="<?=$banner_leade_ch;?>">
     <input type="checkbox" name="banner" class="flat" id="ch_banner" <?=$checked?>>
     ถ้าต้องการสร้างชิ้นงาน Banner (กรุณาคลิกที่ปุ่มสีเขียว)
 </label>

</div>

<div  id="banner">
    <hr>
    <div class="form-group">
        <label class="control-label col-md-3" for="brand">เลือกประเภท
        Banner ที่ต้องการแสดงสำหรับหน้าจอที่เลือก </label>
        <div class="col-md-9">
            <label class="radio-inline control-label"> <input type="radio"
                name="bannerchoose" value="1" id="b1" class="flat"> รูปภาพ
            </label> <label class="radio-inline control-label"> <input
                type="radio" name="bannerchoose" value="2" id="b2" class="flat"> 
                รูปภาพและข้อความ
            </label> <label class="radio-inline control-label"> <input
                type="radio" name="bannerchoose" value="3" id="b3" class="flat"> 
                คลิปวีดีโอ
            </label>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-md-3" for="brand">ตำแหน่งรูปภาพหลักของหน้า PC
            <br> (.jpg  ขนาด 1920 x 1080 px)
            <br> ภาพขนาดไม่เกิน 300 KB
        </label>
        <hr/>
    </div>
    <div class="form-group bannerlead" id="bannerPC" >

        <div class="col-md-12">

            <div id="gallery_master"></div>

            <div id="galery_img"></div>

            <div class="row">
                <?php
                $i=0;
                $sql=" select DISTINCT m.banner_main_id,l.* ";
                $sql.=" from LH_BANNER_MAIN m left join LH_BANNER_LEAD l on m.banner_main_id = l.banner_main_id ";
                $sql.=" where m.type_page_id= ".$bannerMain->type_page_id ." and l.lead_img_mobile = 'p' ";
                $sql.=" order by l.seq_leade_img asc ";
//echo $sql;
                $query_lead = mssql_query($sql);
                $num = mssql_num_rows($query_lead);
                while ($value_l = mssql_fetch_array($query_lead)) {
                    $i++;
//echo $value_l['lead_path'];
                    ?>
                    <div class="col-sm-3">
                        <div class="form-group ">
                            <div class="col-md-12">
                                <!--                                                            <div class="file-preview ">-->
                                    <div class="close fileinput-remove"></div>
                                    <div class="file-drop-disabled">
                                        <div class="file-preview-thumbnails">
                                            <div class="file-initial-thumbs">
                                                <div class="file-preview-frame file-preview-initial" data-fileindex="init_0" data-template="image">
                                                    <div class="kv-file-content">
                                                        <a class="fancybox" rel="group_position_project" href="<?php echo $value_l['lead_path']; ?>">
                                                            <img src="<?php echo $value_l['lead_path']; ?>" class="kv-preview-data file-preview-image"  style="width:150px;max-height:150px;min-height:150px;">
                                                        </a>
                                                    </div>
                                                    <div class="file-thumbnail-footer">

                                                        <input type="text" name="lead_image_p_seo_edit[]" class="form-control" placeholder="Alt SEO" value="<?php echo $value_l['seo_lead']; ?>">
                                                        <br>

                                                        <input type="hidden" name="banner_lead_p_id[]" value="<?=$value_l['banner_lead_id']?>">
                                                        <input type="file" id="lead_image_p_edit<?=$i?>" name="lead_image_p_edit[]" accept="image/*"/>

                                                        <script>
                                                            $("#lead_image_p_edit<?=$i?>").fileinput({
uploadUrl: "upload.php", // server upload action
maxFileCount: 1,
allowedFileExtensions: ["jpg", "png", "jpeg"],
browseLabel: ' เลือก รูปภาพหลัก PC ',
browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
removeLabel: 'ลบ',
browseClass: 'btn btn-success',
showUpload: false,
showRemove:false,
showCaption: false,
msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
msgZoomModalHeading: 'ตัวอย่างละเอียด',
xxx:'seo',
minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
dropZoneTitle : 'รูปภาพหลัก PC',
maxFileSize :300 ,
msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
});
</script>

<span><?="ลำดับที่ ".$i;?></span>
<div class="file-actions">
    <div class="file-footer-buttons">
        <button type="button" id="<?php echo $value_l['banner_lead_id']; ?>" onclick="deleteBanner_d(<?php echo $value_l['banner_lead_id'];?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
        <a class="fancybox" href="<?php echo $value_l['lead_path']; ?>" target="_blank">
            <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
        </a>
    </div>
    <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
</div>
</div>
</div>
<!--                                                                        </div>-->
</div>

<div class="clearfix"></div>
<div class="file-preview-status text-center text-success"></div>
<div class="kv-fileinput-error file-error-message" style="display: none;"></div>
</div>
</div>
</div>
</div>

<div class="form-group ">
    <div class="col-md-12">
        <input type="text"  class="form-control" name="lead_image_p_url_edit[]" placeholder="EX : http://lh.co.th or https://lh.co.th" value="<?php echo $value_l['img_url'];?>" />
    </div>
</div>


</div>
<?php } ?>


</div>


<br>
<div class="row">

    <div class="form-group hidden">
        <div class="col-md-3">
            <input  id="bannerPc_0" name="bannerPc_0" type="file" accept="image/*" />
        </div>
    </div>
    <?php if($num == 0){?>
    <div class="col-sm-3">
        <div class="form-group setIndexProject_galery">
            <div class="col-md-12">
                <input type="file" id="lead_image_p0" class="lead_image" name="lead_image_p[]" accept="image/*" />
            </div>
        </div>
        <div class="form-group setIndexProject_galery">
            <div class="col-md-12">
                <input type="text"  class="form-control" name="lead_image_p_url[]"  placeholder="EX : http://lh.co.th or https://lh.co.th" />
            </div>
        </div>
    </div>

    <?php }?>

    <div id="lead_image_project_p"></div>
</div>

<br>
<div class="form-group">
    <label class="control-label col-md-1">
    </label>
    <div class="col-md-4">
        <button id="lead_image_add" type="button" class="btn btn-round btn-success" onclick="add_lead_image_p()" >+
            เพิ่มรูป รูปภาพหลัก PC
        </button>
    </div>
</div>
</div>
</div>

<div id="bannerMB">
    <div class="form-group">
        <label class="control-label col-md-3" for="brand">ตำแหน่งรูปภาพหลักของหน้า Mobile
            <br> (.jpg  ขนาด 1080 x 1920 px)
            <br> ภาพขนาดไม่เกิน 300 KB
        </label>
        <hr/>
    </div>
    <div class="form-group bannerlead" >

        <div class="col-md-12">


            <div id="bannerMb_img"></div>
            <div id="bannerMb_img2">

                <div class="row">
                    <?php
                    $i=0;
                    $sql=" select DISTINCT m.banner_main_id,l.* ";
                    $sql.=" from LH_BANNER_MAIN m left join LH_BANNER_LEAD l on m.banner_main_id = l.banner_main_id ";
                    $sql.=" where m.type_page_id= ".$bannerMain->type_page_id ." and l.lead_img_mobile = 'm' ";
                    $sql.=" and m.lead_img_mobile = 'h' order by l.seq_leade_img asc ";
//echo $sql;
                    $query_lead = mssql_query($sql);
                    $nums = mssql_num_rows($query_lead);
                    while ($value_l = mssql_fetch_array($query_lead)) {
                        $i++;
//echo $value_l['lead_path'];
                        ?>
                        <div class="col-sm-3">
                            <div class="form-group ">
                                <div class="col-md-12">
                                    <!--                                                            <div class="file-preview ">-->
                                        <div class="close fileinput-remove"></div>
                                        <div class="file-drop-disabled">
                                            <div class="file-preview-thumbnails">
                                                <div class="file-initial-thumbs">
                                                    <div class="file-preview-frame file-preview-initial" data-fileindex="init_0" data-template="image">
                                                        <div class="kv-file-content">
                                                            <a class="fancybox" rel="group_position_project" href="<?php echo $value_l['lead_path']; ?>">
                                                                <img src="<?php echo $value_l['lead_path']; ?>" class="kv-preview-data file-preview-image"  style="width:150px;max-height:150px;min-height:150px;">
                                                            </a>
                                                        </div>
                                                        <div class="file-thumbnail-footer">
                                                            <br>
                                                            <input type="text" class="form-control" name="banner_lead_p_m_seo_edit[]" placeholder="Alt seo" value="<?=$value_l['seo_lead']?>">
                                                            <br>

                                                            <input type="hidden" name="banner_lead_p_m_id[]" value="<?=$value_l['banner_lead_id']?>">
                                                            <input type="file" id="lead_image_m_edit_<?=$i?>" name="lead_image_p_m_edit[]" accept="image/*"/>

                                                            <script>
                                                                $("#lead_image_m_edit_<?=$i?>").fileinput({
                                                                uploadUrl: "upload.php", // server upload action
                                                                maxFileCount: 1,
                                                                allowedFileExtensions: ["jpg", "png", "jpeg"],
                                                                browseLabel: ' เลือกรูปภาพหลัก Mobile ',
                                                                browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                                                                removeLabel: 'ลบ',
                                                                browseClass: 'btn btn-success',
                                                                showUpload: false,
                                                                showRemove:false,
                                                                showCaption: false,
                                                                msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                                msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                                                msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                                xxx:'seo',
                                                                minImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
                                                                minImageHeight: 1920, // ขนาด ศุง ต่ำสุด
                                                                maxImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
                                                                maxImageHeight: 1920, // ขนาด ศุง ต่ำสุด
                                                                dropZoneTitle : 'รูปภาพหลัก Mobile',
                                                                maxFileSize :300 ,
                                                                msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                                                msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                                                msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                                                msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                                                msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                                            });
                                                        </script>

                                                        <span><?="ลำดับที่ ".$i;?></span>
                                                        <div class="file-actions">
                                                            <div class="file-footer-buttons">
                                                                <button type="button" id="<?php echo $value_l['banner_lead_id']; ?>" onclick="deleteBanner_d(<?php echo $value_l['banner_lead_id'];?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                                <a class="fancybox" href="<?php echo $value_l['lead_path']; ?>" target="_blank">
                                                                    <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                                </a>
                                                            </div>
                                                            <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                                                        </div>-->
                                            </div>

                                            <div class="clearfix"></div>
                                            <div class="file-preview-status text-center text-success"></div>
                                            <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group setIndexProject_galery">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name="lead_image_p_m_url_edit[]" placeholder="EX : http://lh.co.th or https://lh.co.th" value="<?=$value_l['img_url']?>" />
                                </div>
                            </div>

                        </div>
                        <?php } ?>


                    </div>
                    <br>
                    <div class="row">

                        <div class="form-group hidden">
                            <div class="col-md-3">
                                <input  id="bannerPc_0" name="bannerPc_0" type="file" accept="image/*" />
                            </div>
                        </div>
                        <?php if($nums == 0){?>
                        <div class="col-sm-3">
                            <div class="form-group setIndexProject_galery">
                                <div class="col-md-12">
                                    <input type="file" id="lead_image_p_m0" class="lead_image" name="lead_image_p_m[]" accept="image/*" />
                                </div>
                            </div>

                            <div class="form-group setIndexProject_galery">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name="lead_image_p_m_url[]" placeholder="EX : http://lh.co.th or https://lh.co.th" />
                                </div>
                            </div>

                        </div>

                        <?php }?>

                        <div id="lead_image_project_p_m"></div>
                    </div>

                    <br>
                    <div class="form-group">
                        <label class="control-label col-md-1">
                        </label>
                        <div class="col-md-4">
                            <button id="lead_image_add_m" type="button" class="btn btn-round btn-success" onclick="add_lead_image_p_m()" >+
                                เพิ่มรูปภาพหลัก Mobile
                            </button>
                        </div>
                    </div>
                </div>            
            </div>
        </div>
    </div>

    <div class="form-group upVideo" id="upVideo">      
        <label class="control-label col-md-3" for="first-name">เลือกชนิดการอัปไฟล์
            <span class="required"></span>
        </label>
        <div class="col-md-6">
            <div class="radio">
                <label><input type="radio" class="flat" name="upvido" value="youtube" id="youtube" checked> VDO Youtube</label>
            </div>
            <div class="radio">
                <label><input type="radio" class="flat" name="upvido" value="vdo" id="vdo"> VDO File</label>
            </div>
        </div>
    </div>
    <div class="form-group upVideo" id="upYoutube">

        <?php
        $sql_yu = "SELECT * FROM LH_BANNER_MAIN WHERE type_show_id  = 3 AND banner_main_id =  '$bannerMainId'";
        $query_yu = mssql_query($sql_yu);
        $num_yu = mssql_num_rows($query_yu);
        $value_yu  =mssql_fetch_array($query_yu);
        ?>

        <label class="control-label col-md-3" for="first-name">URL
            Youtube <span class="required"> *</span>
        </label>
        <div class="col-md-6">
            <div class="control-group">
                <input type="text" name="url_youtube" id="url_youtube"
                class="form-control col-md-7 col-xs-12"
                value="<?=$value_yu['url_vdo']?>"  placeholder="EX : http://lh.co.th or https://lh.co.th">
            </div>
        </div>
    </div>
    <div class="form-group upVideo" id="gthumbnailYoutube">
        <label class="control-label col-md-3" for="brand">Thumbnail Youtube <br> (.jpg ขนาด 1920 x 1080 px)</label>

        <?php

        if($num_yu == 0){
            ?>
            <div class="col-md-4">
                <input id="thumbnailYoutube" name="thumbnailYoutube" type="file">
            </div>
            <?php }else{?>
            <div class="col-sm-3">
                <div class="form-group ">
                    <div class="col-md-12">
                        <!--                                                            <div class="file-preview ">-->
                            <div class="close fileinput-remove"></div>
                            <div class="file-drop-disabled">
                                <div class="file-preview-thumbnails">
                                    <div class="file-initial-thumbs">
                                        <div class="file-preview-frame file-preview-initial" data-fileindex="init_0" data-template="image">
                                            <div class="kv-file-content">
                                                <a class="fancybox" rel="group_position_project" href="<?php echo $value_yu['thumnail_path']; ?>">
                                                    <img src="<?php echo $value_yu['thumnail_path']; ?>" class="kv-preview-data file-preview-image"  style="width:150px;max-height:150px;min-height:150px;">
                                                </a>
                                            </div>
                                            <div class="file-thumbnail-footer">

                                                <input type="text" name="youtube_img_seo[]" class="form-control" placeholder="Alt SEO" value="<?=$value_yu['seo_main']?>">
                                                <br>

                                                <input type="hidden" name="banner_lead_p_m_id[]" value="<?=$value_yu['banner_lead_id']?>">
                                                <input type="file" id="youtube_edit_<?=$value_yu['banner_lead_id']?>" name="thumbnailYoutube_edit" accept="image/*"/>

                                                <script>
                                                    $("#youtube_edit_<?=$value_yu['banner_lead_id']?>").fileinput({
                                                    uploadUrl: "upload.php", // server upload action
                                                    maxFileCount: 1,
                                                    allowedFileExtensions: ["jpg", "png", "jpeg"],
                                                    browseLabel: ' แก้ไข Thumbnail Youtube ',
                                                    browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                                                    removeLabel: 'ลบ',
                                                    browseClass: 'btn btn-success',
                                                    showUpload: false,
                                                    showRemove:false,
                                                    showCaption: false,
                                                    msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                    msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                                    msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                    xxx:'seo',
                                                    minImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
                                                    minImageHeight: 1920, // ขนาด ศุง ต่ำสุด
                                                    maxImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
                                                    maxImageHeight: 1920, // ขนาด ศุง ต่ำสุด
                                                    dropZoneTitle : 'Thumbnail Youtube',
                                                    maxFileSize :300 ,
                                                    msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                                    msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                                    msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                                    msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                                    msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                                });
                                            </script>

                                            <div class="file-actions">
                                                <div class="file-footer-buttons">

                                                    <a class="fancybox" href="<?php echo $value_yu['thumnail_path']; ?>" target="_blank">
                                                        <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                    </a>
                                                </div>
                                                <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!--                                                                        </div>-->
                                </div>

                                <div class="clearfix"></div>
                                <div class="file-preview-status text-center text-success"></div>
                                <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <?php }?>
        </div>
        <?php
        $sql_v = "SELECT * FROM LH_BANNER_MAIN WHERE type_show_id  = 2 AND banner_main_id =  '$bannerMainId'";
        $query_v = mssql_query($sql_v);
        $num_v = mssql_num_rows($query_v);
        $value_v  =mssql_fetch_array($query_v);
        ?>
        <div class="form-group upVideo" id="upVdo">
            <label class="control-label col-md-3" for="first-name">เลือก VDO <br>(.mp4 ไม่เกิน 200 MB)
            </label><span class="required"></span>
            <?php

            if($num_v == 0){
                ?>
                <div class="col-md-4">
                    <input type="file" id="vdofile" name="vdofile"
                    class="form-control col-md-7 col-xs-12"
                    accept="video/mp4">
                </div>
                <?php }else{?>
                <div class="col-sm-3">
                    <div class="form-group ">
                        <div class="col-md-12">
                            <!--                                                            <div class="file-preview ">-->
                                <div class="close fileinput-remove"></div>
                                <div class="file-drop-disabled">
                                    <div class="file-preview-thumbnails">
                                        <div class="file-initial-thumbs">
                                            <div class="file-preview-frame file-preview-initial" data-fileindex="init_0" data-template="image">
                                                <div class="kv-file-content">
                                                    <video width="250" controls>
                                                        <source src="<?= $value_v['url_vdo'] ?>" type="video/mp4">

                                                        </video>

                                                    </div>
                                                    <div class="file-thumbnail-footer">

                                                        <input type="hidden" name="banner_lead_p_m_id[]" value="<?=$value_v['banner_lead_id']?>">
                                                        <input type="file" id="vdo_edit_<?=$value_v['banner_lead_id']?>" name="vdofile_edit" accept="video/mp4"/>

                                                        <script>
                                                            $("#vdo_edit_<?=$value_v['banner_lead_id']?>").fileinput({
                                                    uploadUrl: "upload.php", // server upload action
                                                    maxFileCount: 1,
                                                    allowedFileExtensions: ["mp4"],
                                                    browseLabel: ' แก้ไข VDO File ',
                                                    browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                                                    removeLabel: 'ลบ',
                                                    browseClass: 'btn btn-success',
                                                    showUpload: false,
                                                    showRemove:false,
                                                    showCaption: false,
                                                    msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                    msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                                    msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                    xxx:'seo',
                                                    maxFileSize :51200 ,
                                                    maxFilePreviewSize : 51200,
                                                    minImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
                                                    minImageHeight: 1920, // ขนาด ศุง ต่ำสุด
                                                    maxImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
                                                    maxImageHeight: 1920, // ขนาด ศุง ต่ำสุด
                                                    dropZoneTitle : 'VDO File',

                                                });
                                            </script>

                                            <div class="file-actions">
                                                <div class="file-footer-buttons">

                                                </div>
                                                <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!--                                                                        </div>-->
                                </div>

                                <div class="clearfix"></div>
                                <div class="file-preview-status text-center text-success"></div>
                                <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <?php }?>
        </div>
        <div class="form-group upVideo" id="gthumbnailVDO">
            <label class="control-label col-md-3" for="brand">Thumbnail VDO <br> (.jpg ขนาด 1920 x 1080 px)</label>
            <?php

            if($num_v == 0){
                ?>
                <div class="col-md-4">
                    <input id="thumbnailVDO" name="thumbnailVDO" type="file">
                </div>
                <?php }else{?>
                <div class="col-sm-3">
                    <div class="form-group ">
                        <div class="col-md-12">
                            <!--                                                            <div class="file-preview ">-->
                                <div class="close fileinput-remove"></div>
                                <div class="file-drop-disabled">
                                    <div class="file-preview-thumbnails">
                                        <div class="file-initial-thumbs">
                                            <div class="file-preview-frame file-preview-initial" data-fileindex="init_0" data-template="image">
                                                <div class="kv-file-content">
                                                    <a class="fancybox" rel="group_position_project" href="<?php echo $value_v['thumnail_path']; ?>">
                                                        <img src="<?php echo $value_v['thumnail_path']; ?>" class="kv-preview-data file-preview-image"  style="width:150px;max-height:150px;min-height:150px;">
                                                    </a>
                                                </div>
                                                <div class="file-thumbnail-footer">

                                                    <input type="text" name="vdo_img_seo[]" class="form-control" placeholder="Alt SEO" value="<?=$value_v['seo_main']?>">
                                                    <br>

                                                    <input type="hidden" name="banner_lead_p_m_id[]" value="<?=$value_v['banner_lead_id']?>">
                                                    <input type="file" id="th_vdo_edit_<?=$value_v['banner_lead_id']?>" name="thumbnailVDO_edit[]" accept="image/*"/>

                                                    <script>
                                                        $("#th_vdo_edit_<?=$value_v['banner_lead_id']?>").fileinput({
uploadUrl: "upload.php", // server upload action
maxFileCount: 1,
allowedFileExtensions: ["jpg", "png", "jpeg"],
browseLabel: ' แก้ไข Thumbnail VDO ',
browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
removeLabel: 'ลบ',
browseClass: 'btn btn-success',
showUpload: false,
showRemove:false,
showCaption: false,
msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
msgZoomModalHeading: 'ตัวอย่างละเอียด',
xxx:'seo',
minImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
minImageHeight: 1920, // ขนาด ศุง ต่ำสุด
maxImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
maxImageHeight: 1920, // ขนาด ศุง ต่ำสุด
dropZoneTitle : 'Thumbnail VDO',
maxFileSize :300 ,
msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
});
</script>

<div class="file-actions">
    <div class="file-footer-buttons">

        <a class="fancybox" href="<?php echo $value_yu['thumnail_path']; ?>" target="_blank">
            <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
        </a>
    </div>
    <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
</div>
</div>
</div>
<!--                                                                        </div>-->
</div>

<div class="clearfix"></div>
<div class="file-preview-status text-center text-success"></div>
<div class="kv-fileinput-error file-error-message" style="display: none;"></div>
</div>
</div>
</div>
</div>

</div>
<?php }?>
</div>
<div class="form-group activityimg" id="ActivityColor">

    <?php
    $sql_ac = "SELECT * FROM LH_BANNER_MAIN WHERE type_show_id  = 1 AND banner_main_id = '$bannerMainId'";
    $query_ac = mssql_query($sql_ac);
    $num_ac = mssql_num_rows($query_ac);
    $value_ac  =mssql_fetch_array($query_ac);

    ?>

    <label class="control-label col-md-3" for="brand">สีพื้นหลัง</label>
    <div class="col-md-3 colortxt">
        <input id="activitycolor" name="activitycolor" type="text" placeholder="กรุณาเลือกสี" class="form-control color-input" readonly value="<?=$value_ac['color_code_activity']?>" >
    </div>
</div>
<div class="form-group activityimg" id="gActivityImg">
    <label class="control-label col-md-3" for="brand">รูปภาพ Activity</label>
    <?php

    if($num_ac == 0){
        ?>
        <div class="col-md-4">
            <input id="activityimg" name="activityimg" type="file">
        </div>
        <?php }else{ ?>
        <div class="col-sm-3">
            <div class="form-group ">
                <div class="col-md-12">
                    <!--                                                            <div class="file-preview ">-->
                        <div class="close fileinput-remove"></div>
                        <div class="file-drop-disabled">
                            <div class="file-preview-thumbnails">
                                <div class="file-initial-thumbs">
                                    <div class="file-preview-frame file-preview-initial" data-fileindex="init_0" data-template="image">
                                        <div class="kv-file-content">
                                            <a class="fancybox" rel="group_position_project" href="<?php echo $value_ac['pic_activity']; ?>">
                                                <img src="<?php echo $value_ac['pic_activity']; ?>" class="kv-preview-data file-preview-image"  style="width:150px;max-height:150px;min-height:150px;">
                                            </a>
                                        </div>
                                        <div class="file-thumbnail-footer">

                                            <input type="text" name="seo_lead_banner[]" placeholder="Alt SEO" class="form-control" value="<?=$value_ac['seo_main']?>">
                                            <br>

                                            <input type="hidden" name="banner_lead_p_id[]" value="<?=$value_ac['banner_main_id']?>">
                                            <input type="file" id="activity_edit_<?=$value_ac['banner_lead_id']?>" name="activityimg_edit" accept="image/*"/>

                                            <script>
                                                $("#activity_edit_<?=$value_ac['banner_lead_id']?>").fileinput({
uploadUrl: "upload.php", // server upload action
maxFileCount: 1,
allowedFileExtensions: ["jpg", "png", "jpeg"],
browseLabel: ' แก้ไข Banner Activity ',
browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
removeLabel: 'ลบ',
browseClass: 'btn btn-success',
showUpload: false,
showRemove:false,
showCaption: false,
msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
msgZoomModalHeading: 'ตัวอย่างละเอียด',
xxx:'seo',
minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
dropZoneTitle : 'Banner Activity',
maxFileSize :300 ,
msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
});
</script>

<div class="file-actions">
    <div class="file-footer-buttons">

        <a class="fancybox" href="<?php echo $value_ac['pic_activity']; ?>" target="_blank">
            <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
        </a>
    </div>
    <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
</div>
</div>
</div>
<!--                                                                        </div>-->
</div>

<div class="clearfix"></div>
<div class="file-preview-status text-center text-success"></div>
<div class="kv-fileinput-error file-error-message" style="display: none;"></div>
</div>
</div>
</div>
</div>

</div>
<?php }?>
</div>

<div class="form-group activityimg">
    <label class="control-label col-md-3" for="first-name">เนื้อหา Activity ภาษาจีน <span class="required">*</span>
    </label>

    <div class="col-md-9">
        <textarea placeholder="กรอกข้อความ banner"
        id="editTh" name="editTh"  class="form-control"
        rows="3" ><?=$value_ac['text_activity_th']?></textarea>
    </div>
</div>
<div class="form-group activityimg">
    <label class="control-label col-md-3" for="first-name">เนื้อหา Activity ภาษาอังกฤษ</label>

    <div class="col-md-9">
        <textarea  placeholder="กรอกข้อความ banner"
        id="editEn" name="editEn" class="form-control"
        rows="3" ><?=$value_ac['text_activity_en']?></textarea>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3" for="first-name">ระบุวันเริ่ม
        <span class="required">*</span>
    </label>
    <div class="col-md-2">
        <fieldset>
            <div class="input-prepend input-group">
                <input type="text" style="width: 180px" name="startdate" id="startdate" class="form-control"
                readonly  value="<?=$startdate=='01/01/1900' ? '' : $startdate ?>"/>
            </div>
        </fieldset>
    </div>
    <label class="control-label col-md-2" for="first-name">ระบุวันสิ้นสุด
        <span class="required" aria-required="true">*</span>
    </label>
    <div class="col-md-2">
        <fieldset>
            <div class="input-prepend input-group">
                <input type="text" style="width: 180px" name="enddate" id="enddate" class="form-control "
                aria-required="true" readonly value="<?=$enddate =='01/01/1900' ? '' : $enddate  ?>" />
            </div>
        </fieldset>
    </div>
</div>
</div>
<br/>
<div class="form-group">
    <div align="center">
        <input type="hidden" name="chageBannerChoose" />
        <input type="hidden" name="oldBannerImg" />
        <input type="hidden" name="oldBannerPC" />
        <input type="hidden" name="oldBannerMB" />
        <input type="hidden" name="defaultBannerMainId" />
        <input type="hidden" name="bannerMainId" value="<?=$_GET['bannerMainId']?>" />
        <input type="hidden" name="action" value="EDIT" />

        <button style="display: none;" type="button" class="btn btn-danger confirms" name="delBanner">Delete</button>
        <button type="submit"  name="save" value="" class="btn btn-success">Update</button>


    </div>
</div>

<div class="form-group">
    <U> หมายเเหตุ </U> :  หากไม่ต้องการใช้งานหรือเคลียร์ Banner ให้ กับการติกปุ่ม <B> (สีเขียว) </B> ออก
</div>




</form>
</div>
</div>
</div>
</div>

</div>
</div>

<!--   froala JS     -->

<!--        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>-->
<script type="text/javascript" src="libs/codemirror/5.3.0/codemirror.min.js"></script>
<script type="text/javascript" src="libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/froala_editor.pkgd.min.js"></script>

<!-- Include Plugins. -->
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/align.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/char_counter.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/code_beautifier.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/code_view.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/colors.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/emoticons.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/entities.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/file.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/font_family.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/font_size.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/fullscreen.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/image.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/image_manager.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/inline_style.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/line_breaker.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/link.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/lists.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/paragraph_format.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/paragraph_style.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/quick_insert.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/quote.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/table.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/save.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/url.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/video.min.js"></script>
<script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/word_paste.min.js"></script>


<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="js/bootstrap-imgupload.js"></script>

<script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>

<!-- bootstrap-progressbar -->
<script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- Custom Theme Scripts -->
<script src="../build/js/custom.min.js"></script>

<!-- date -->
<link rel="stylesheet" type="text/css" href="../build/css/jquery-ui.css"/>
<script src="../build/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

<!-- Validate -->
<script src="../build/js/jquery.validate.js"></script>

<!-- <script src="js/js_banner_update.js" type="text/javascript" charset="utf-8" async defer></script> -->

<script>

    $('.fancybox').fancybox();

    $(function() {
        $.FroalaEditor.DefineIcon("imageInfo", { NAME: "info" });
        $.FroalaEditor.RegisterCommand("imageInfo", {
            title: "Info",
            focus: false,
            undo: false,
            refreshAfterCallback: false,
            callback: function() {
                var $img = this.image.get();
                alert($img.attr("src"));
            }
        });

        $('#editTh').froalaEditor({
            toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'fontFamily', 'fontSize','insertLink', '|', 'specialCharacters', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', '-', '|', 'undo', 'redo', 'clearFormatting', 'selectAll', 'html', 'applyFormat', 'removeFormat', 'fullscreen', 'print'],    
            height: 200,
            imageUploadURL: 'uploade_highlights.php',
            imageUploadParams: {
                id: 'edit'
            },
            imageManagerLoadURL: 'images_load.php',

            fileUploadURL: 'upload_file.php',
            fileUploadParams: {
                id: 'edit'
            },
            fontFamily: {
                "LHfont": 'Kittithada',
                "Roboto,sans-serif": 'Roboto',
                "Oswald,sans-serif": 'Oswald',
                "Montserrat,sans-serif": 'Montserrat',
                "'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
            },

            imageManagerDeleteURL: "delete_image_highlights.php",
            imageManagerDeleteMethod: "POST"
        })
                // Catch image removal from the editor.
                .on('froalaEditor.image.removed', function (e, editor, $img) {
                    $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_image_highlights.php",

                            // Request params.
                            data: {
                                src: $img.attr('src')
                            }
                        })
                    .done (function (data) {
                        console.log ('image was deleted'+$img.attr('src'));
                    })
                    .fail (function (err) {
                        console.log ('image delete problem: ' + JSON.stringify(err));
                    })
                })

                    // Catch image removal from the editor.
                    .on('froalaEditor.file.unlink', function (e, editor, link) {

                        $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_file.php",

                            // Request params.
                            data: {
                                src: link.getAttribute('href')
                            }
                        })
                        .done (function (data) {
                            console.log ('file was deleted');
                        })
                        .fail (function (err) {
                            console.log ('file delete problem: ' + JSON.stringify(err));
                        })
                    });

                //
                $('#editEn').froalaEditor({
                    toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'fontFamily', 'fontSize','insertLink', '|', 'specialCharacters', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', '-', '|', 'undo', 'redo', 'clearFormatting', 'selectAll', 'html', 'applyFormat', 'removeFormat', 'fullscreen', 'print'],    
                    height: 200,
                    imageUploadURL: 'uploade_highlights.php',
                    imageUploadParams: {
                        id: 'edit'
                    },
                    imageManagerLoadURL: 'images_load.php',

                    fileUploadURL: 'upload_file.php',
                    fileUploadParams: {
                        id: 'edit'
                    },
                    fontFamily: {
                        "LHfont": 'Kittithada',
                        "Roboto,sans-serif": 'Roboto',
                        "Oswald,sans-serif": 'Oswald',
                        "Montserrat,sans-serif": 'Montserrat',
                        "'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
                    },

                    imageManagerDeleteURL: "delete_image_highlights.php",
                    imageManagerDeleteMethod: "POST"
                })
                // Catch image removal from the editor.
                .on('froalaEditor.image.removed', function (e, editor, $img) {
                    $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_image_highlights.php",

                            // Request params.
                            data: {
                                src: $img.attr('src')
                            }
                        })
                    .done (function (data) {
                        console.log ('image was deleted'+$img.attr('src'));
                    })
                    .fail (function (err) {
                        console.log ('image delete problem: ' + JSON.stringify(err));
                    })
                })

                    // Catch image removal from the editor.
                    .on('froalaEditor.file.unlink', function (e, editor, link) {

                        $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_file.php",

                            // Request params.
                            data: {
                                src: link.getAttribute('href')
                            }
                        })
                        .done (function (data) {
                            console.log ('file was deleted');
                        })
                        .fail (function (err) {
                            console.log ('file delete problem: ' + JSON.stringify(err));
                        })
                    });
                });

        //////////////////////////////

        $('#startdate').datepicker(
        {
            dateFormat : "dd/mm/yy",
            autoclose : true,
            changeMonth : true,
            showDropdowns : true,
            changeYear : true,
            minDate : new Date(),
            onSelect : function(dateText) {
                var d = new Date($(this).datepicker(
                    "getDate"));
                $("input#enddate").datepicker('option',
                    'minDate', dateText);
                $("input#enddate").prop('disabled', false);
            }
        });

        $('#enddate').datepicker(
        {
            dateFormat : "dd/mm/yy",
            autoclose : true,
            changeMonth : true,
            showDropdowns : true,
            changeYear : true,
            minDate : new Date(),
            onSelect : function(dateText) {
                var d = new Date($(this).datepicker(
                    "getDate"))
                $("input#startdate").datepicker('option',
                    'maxDate', dateText);
            }
        });

        ////
        var elem = document.querySelector('.color-input');
        var hueb = new Huebee( elem, {
           notation: 'hex'
            // options
        });

         /////////////////
         $("#activityimg").fileinput({
            uploadUrl : "save_lhbanner.php", // server
            // upload
            // action
            maxFileCount : 1,
            allowedFileExtensions : [ "jpg", "png","jpeg", "gif" ],
            browseLabel : 'เลือกรูป',
            removeLabel : 'ลบ',
            browseClass : 'btn btn-success',
            showUpload : false,
            showRemove : false,
            showCaption : false,
            autoReplace: false,
            msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
            msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
            msgZoomModalHeading : 'ตัวอย่างละเอียด',
            dropZoneTitle : 'Banner Activity Image',
            xxx:'seo_lead_banner',
            minImageWidth: 1920, // ขนาด กว้าง
            // ต่ำสุด
            minImageHeight: 1080, // ขนาด ศุง
            // ต่ำสุด
            maxImageWidth: 1920, // ขนาด กว้าง
            // ต่ำสุด
            maxImageHeight: 1080, // ขนาด ศุง
            // ต่ำสุด
            maxFileSize :300 ,
            msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
            msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
            msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
            msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
            msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
        });

         $("#thumbnailYoutube")
         .fileinput(
         {
                    uploadUrl : "save_lhbanner.php", // server
                    // upload
                    // action
                    maxFileCount : 1,
                    allowedFileExtensions : [ "jpg", "png","jpeg", "gif" ],
                    browseLabel : 'เลือกรูป',
                    removeLabel : 'ลบ',
                    browseClass : 'btn btn-success',
                    showUpload : false,
                    showRemove : false,
                    showCaption : false,
                    autoReplace: false,
                    msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                    msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                    msgZoomModalHeading : 'ตัวอย่างละเอียด',
                    dropZoneTitle : 'รูปภาพ Thumbnail Youtube',
                    xxx:'youtube_img_seo',
                    //maxFileSize : 500 ,
                    minImageWidth: 1920, // ขนาด กว้าง
                    // ต่ำสุด
                    minImageHeight: 1080, // ขนาด ศุง
                    // ต่ำสุด
                    maxImageWidth: 1920, // ขนาด กว้าง
                    // ต่ำสุด
                    maxImageHeight: 1080, // ขนาด ศุง
                    // ต่ำสุด
                    maxFileSize :300 ,
                    msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                    msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                    msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                    msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                    msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                });

         $("#vdofile")
         .fileinput(
         {
                    uploadUrl : "save_lhbanner.php", // server
                    // upload
                    // action
                    maxFileCount : 1,
                    allowedFileExtensions : [ "mp4" ],
                    browseLabel : 'เลือกไฟล์',
                    removeLabel : 'ลบ',
                    browseClass : 'btn btn-success',
                    showUpload : false,
                    showRemove : false,
                    showCaption : false,
                    autoReplace: false,
                    maxFileSize : 204800 , // 200 MB
                    msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                    msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                    msgZoomModalHeading : 'ตัวอย่างละเอียด',
                    xxx:'seo',
                    dropZoneTitle : 'File VDO',
                    //maxFileSize :300 ,
                    msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                    msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                    msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                    msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                    msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                });

         $("#thumbnailVDO")
         .fileinput(
         {
                    uploadUrl : "save_lhbanner.php", // server
                    // upload
                    // action
                    maxFileCount : 1,
                    allowedFileExtensions : [ "jpg", "png","jpeg", "gif" ],
                    browseLabel : 'เลือกรูป',
                    removeLabel : 'ลบ',
                    browseClass : 'btn btn-success',
                    showUpload : false,
                    showRemove : false,
                    showCaption : false,
                    autoReplace: false,
                    msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                    msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                    msgZoomModalHeading : 'ตัวอย่างละเอียด',
                    dropZoneTitle : 'รูปภาพ Thumbnail VDO',
                    xxx:'vdo_img_seo',
                    //maxFileSize : 500 ,
                    minImageWidth: 1920, // ขนาด กว้าง
                    // ต่ำสุด
                    minImageHeight: 1080, // ขนาด ศุง
                    // ต่ำสุด
                    maxImageWidth: 1920, // ขนาด กว้าง
                    // ต่ำสุด
                    maxImageHeight: 1080, // ขนาด ศุง
                    // ต่ำสุด
                    maxFileSize :300 ,
                    msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                    msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                    msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                    msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                    msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                });

        //add_lead_image mobile
        function add_lead_image()
        {

            var index = $('.setIndexProject_galery_galley').length;

            var form = `<div><div class="col-sm-3">
            <div class="form-group setIndexProject_galery">
            <div class="col-md-12">
            <input type="file" id="lead_image_m${index + 1}" class="lead_image" name="lead_image_m[]" accept="image/*" />
            </div>
            </div>

            <div class="form-group ">
            <div class="col-md-12">
            <input type="text"  class="form-control" name="lead_image_p_m_url[]" placeholder="EX : http://lh.co.th or https://lh.co.th" value="" />
            </div>
            </div>

            <button  type="button" class="btn btn-round btn-danger" style="margin-left: 112px;" onclick="removeLead_image(this)"> - ลบ Banner Mobile</button>

            </div>
            </div>`;

            $('#lead_image_project_m').append(form);

            $(".lead_image").fileinput({
                uploadUrl: "upload.php", // server upload action
                maxFileCount: 1,
                allowedFileExtensions: ["jpg", "png", "jpeg"],
                browseLabel: ' เลือก Banner Mobile',
                browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                removeLabel: 'ลบ',
                browseClass: 'btn btn-success',
                showUpload: false,
                showRemove:false,
                showCaption: false,
                msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                msgZoomModalHeading: 'ตัวอย่างละเอียด',
                xxx:'banner_lead_m_seo',
                minImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
                minImageHeight: 1920, // ขนาด ศุง ต่ำสุด
                maxImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
                maxImageHeight: 1920, // ขนาด ศุง ต่ำสุด
                dropZoneTitle : 'Banner Mobile',
                maxFileSize :300 ,
                msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
            });
        }

        function removeLead_image(element)
        {
            //$('#gallery_add').prop("disabled", false);
            $(element).parent().remove();
        }

        //
        //add_lead_image d
        function add_lead_image_d()
        {

            var index = $('.setIndexProject_galery_galley').length;

            var form = `<div><div class="col-sm-3">
            <div class="form-group setIndexProject_galery">
            <div class="col-md-12">
            <input type="file" id="lead_image${index + 1}" class="lead_image" name="lead_image_d[]" accept="image/*" />
            </div>
            </div>

            <div class="form-group ">
            <div class="col-md-12">
            <input type="text"  class="form-control" name="lead_image_p_url[]" placeholder="EX : http://lh.co.th or https://lh.co.th" value="" />
            </div>
            </div>

            <button  type="button" class="btn btn-round btn-danger" style="margin-left: 112px;" onclick="removeLead_image(this)"> - ลบ รูปภาพหลัก PC </button>

            </div>
            </div>`;

            $('#lead_image_project_d').append(form);

            $(".lead_image").fileinput({
                uploadUrl: "upload.php", // server upload action
                maxFileCount: 1,
                allowedFileExtensions: ["jpg", "png", "jpeg"],
                browseLabel: ' เลือก รูปภาพหลัก PC',
                browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                removeLabel: 'ลบ',
                browseClass: 'btn btn-success',
                showUpload: false,
                showRemove:false,
                showCaption: false,
                msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                msgZoomModalHeading: 'ตัวอย่างละเอียด',
                xxx:'banner_lead_seo',
                minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                dropZoneTitle : 'รูปภาพหลัก PC',
                maxFileSize :300 ,
                msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
            });
        }

        function removeLead_image(element)
        {
            //$('#gallery_add').prop("disabled", false);
            $(element).parent().remove();
        }

        //add_lead_image p
        function add_lead_image_p()
        {

            var index = $('.setIndexProject_galery_galley').length;

            var form = `<div><div class="col-sm-3">
            <div class="form-group setIndexProject_galery">
            <div class="col-md-12">
            <input type="file" id="lead_image_p${index + 1}" class="lead_image" name="lead_image_p[]" accept="image/*" />
            </div>
            </div>

            <div class="form-group setIndexProject_galery">
            <input type="text" name="lead_image_p_url[]" class="form-control" placeholder="EX : http://lh.co.th or https://lh.co.th">
            </div>

            <button  type="button" class="btn btn-round btn-danger" style="margin-left: 112px;" onclick="removeLead_image(this)"> - ลบ รูปภาพหลัก PC </button>

            </div>
            </div>`;

            $('#lead_image_project_p').append(form);

            $(".lead_image").fileinput({
                uploadUrl: "upload.php", // server upload action
                maxFileCount: 1,
                allowedFileExtensions: ["jpg", "png", "jpeg"],
                browseLabel: ' เลือกรูปภาพหลัก PC',
                browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                removeLabel: 'ลบ',
                browseClass: 'btn btn-success',
                showUpload: false,
                showRemove:false,
                showCaption: false,
                msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                msgZoomModalHeading: 'ตัวอย่างละเอียด',
                xxx:'lead_image_p_seo',
                minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                dropZoneTitle : 'รูปภาพหลัก PC',
                maxFileSize :300 ,
                msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
            });
        }

        function removeLead_image(element)
        {
            //$('#gallery_add').prop("disabled", false);
            $(element).parent().remove();
        }

        //add_lead_image p
        function add_lead_image_p_m()
        {

            var index = $('.setIndexProject_galery_galley').length;

            var form = `<div><div class="col-sm-3">
            <div class="form-group setIndexProject_galery">
            <div class="col-md-12">
            <input type="file" id="lead_image_p${index + 1}" class="lead_image" name="lead_image_p_m[]" accept="image/*" />
            </div>
            </div>

            <div class="form-group setIndexProject_galery">
            <div class="col-md-12">
            <input type="text"  class="form-control" name="lead_image_p_m_url[]" placeholder="EX : http://lh.co.th or https://lh.co.th" />
            </div>
            </div>

            <button  type="button" class="btn btn-round btn-danger" style="margin-left: 112px;" onclick="removeLead_image(this)"> - ลบ Banner MB </button>

            </div>
            </div>`;

            $('#lead_image_project_p_m').append(form);

            $(".lead_image").fileinput({
                uploadUrl: "upload.php", // server upload action
                maxFileCount: 1,
                allowedFileExtensions: ["jpg", "png", "jpeg"],
                browseLabel: ' เลือกรูปภาพหลัก Mobile',
                browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                removeLabel: 'ลบ',
                browseClass: 'btn btn-success',
                showUpload: false,
                showRemove:false,
                showCaption: false,
                msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                msgZoomModalHeading: 'ตัวอย่างละเอียด',
                xxx:'lead_image_p_m_seo',
                minImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
                minImageHeight: 1920, // ขนาด ศุง ต่ำสุด
                maxImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
                maxImageHeight: 1920, // ขนาด ศุง ต่ำสุด
                dropZoneTitle : 'รูปภาพหลัก Mobile',
                maxFileSize :300 ,
                msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
            });
        }

        function removeLead_image(element)
        {
            //$('#gallery_add').prop("disabled", false);
            $(element).parent().remove();
        }


        $("#lead_image_p0").fileinput({
            uploadUrl: "upload.php", // server upload action
            maxFileCount: 1,
            allowedFileExtensions: ["jpg", "png", "jpeg"],
            browseLabel: ' เลือกรูปภาพหลัก PC ',
            browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
            removeLabel: 'ลบ',
            browseClass: 'btn btn-success',
            showUpload: false,
            showRemove:false,
            showCaption: false,
            msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
            msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
            msgZoomModalHeading: 'ตัวอย่างละเอียด',
            xxx:'lead_image_p_seo',
            minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
            minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
            maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
            maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
            dropZoneTitle : 'รูปภาพหลัก PC',
            maxFileSize :300 ,
            msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
            msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
            msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
            msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
            msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
        });

        $("#lead_image_p_m0").fileinput({
            uploadUrl: "upload.php", // server upload action
            maxFileCount: 1,
            allowedFileExtensions: ["jpg", "png", "jpeg"],
            browseLabel: ' เลือก Banner MB',
            browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
            removeLabel: 'ลบ',
            browseClass: 'btn btn-success',
            showUpload: false,
            showRemove:false,
            showCaption: false,
            msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
            msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
            msgZoomModalHeading: 'ตัวอย่างละเอียด',
            xxx:'lead_image_p_m_seo',
            minImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
            minImageHeight: 1920, // ขนาด ศุง ต่ำสุด
            maxImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
            maxImageHeight: 1920, // ขนาด ศุง ต่ำสุด
            dropZoneTitle : 'Banner MB',
            maxFileSize :300 ,
            msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
            msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
            msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
            msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
            msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
        });


        $("button[name='delBanner']").confirm({
            title : 'ลบข้อมูล',
            content: 'คุณแน่ใจที่จะลบข้อมูลนี้ !',
            buttons: {
                Yes: {
                    text: 'Yes',
                    btnClass: 'btn-danger',
                    keys: ['enter', 'a'],
                    action: function(){
                        $.ajax({ 
                            url: 'ajax_deleteBanner_page.php?id=' + <?=$_GET['bannerMainId']?>,
                            dataType: 'json',
                            method: 'get',

                            success: function(e) {
                                //console.log(e);
                                window.location.href =  'lhbanner.php?delData=success';
                            }
                        });
                    }
                },
                No: {
                    text: 'No',
                    btnClass: 'btn-default',
                    keys: ['enter', 'a'],
                    action: function(){
                        // button action.
                    }
                },

            }
        });

        function deleteBanner_d(id) {
            $.confirm({
                title: 'ลบรูปนี้ !',
                content: 'คุณแน่ใจที่จะลบ รูป นี้!',
                buttons : {
                    Yes: {
                        text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                                var self = this;
                                return $.ajax({
                                    url: 'ajax_deleteBanner_d.php?id=' + id,
                                    dataType: 'json',
                                    method: 'get'
                                }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                }).fail(function () {
                                    self.setContent('Something went wrong.');
                                });
                            }
                        },
                        No: function () {
                            //$.alert('ยกลเิก!');
                        },
                    }
                });
        }



        /////////////////////
        if($('#page').val() == 1){
            $('#bannerMB').show();
        }else{
            $('#bannerMB').hide();
        }

        <?php
        $show_id = getType_show($bannerMainId);

        if($show_id->type_show_id == 0){
          $show_id = 1;
      }else if($show_id->type_show_id == 1){
          $show_id = 2;  
      }else if($show_id->type_show_id == 2){
          $show_id = 3; 
          ?>
          $('#vdo').prop( "checked", true );
          $('#gthumbnailYoutube').hide();
          $('#upYoutube').hide();

          <?php  
      }else if($show_id->type_show_id == 3){
          $show_id = 3;  
          ?>
          $('#youtube').prop( "checked", true );
          $('#gthumbnailVDO').hide();
          $('#upVdo').hide();
          <?php        
      }
      ?>


      $("input[name=bannerchoose][id=b<?=$show_id?>]").prop( "checked", true );

      if($('input[name=bannerchoose][id=b1]').is(':checked')){
       $('#bannerPC').show();
       $('#bannerMB').show();

       $('#ActivityColor').hide();
       $('#gActivityImg').hide(); 
       $('.activityimg').hide(); 

       $('#upVideo').hide();
       $('#upYoutube').hide();
       $('#gthumbnailYoutube').hide();
       $('#gthumbnailVDO').hide();
       $('#upVdo').hide();
   }else if($('input[name=bannerchoose][id=b2]').is(':checked')){
       $('#ActivityColor').show();
       $('#gActivityImg').show(); 
       $('.activityimg').show(); 

       $('#bannerPC').hide();
       $('#bannerMB').hide();

       $('#upVideo').hide();
       $('#upYoutube').hide();
       $('#gthumbnailYoutube').hide();
       $('#gthumbnailVDO').hide();
       $('#upVdo').hide();
   }else if($('input[name=bannerchoose][id=b3]').is(':checked')){

       $('#ActivityColor').hide();
       $('#gActivityImg').hide(); 
       $('.activityimg').hide(); 

       $('#bannerPC').hide();
       $('#bannerMB').hide();

   }

   $("#b1").on('ifChecked', function(event){
       $('#bannerPC').show();
       if($('#page').val() == 1){
        $('#bannerMB').show();
    }else{
        $('#bannerMB').hide();
    }

    $('#ActivityColor').hide();
    $('#gActivityImg').hide(); 
    $('.activityimg').hide(); 

    $('#upVideo').hide();
    $('#upYoutube').hide();
    $('#gthumbnailYoutube').hide();
    $('#gthumbnailVDO').hide();
    $('#upVdo').hide();
});

   $("#b2").on('ifChecked', function(event){
       $('#ActivityColor').show();
       $('#gActivityImg').show(); 
       $('.activityimg').show(); 

       $('#bannerPC').hide();
       $('#bannerMB').hide();

       $('#upVideo').hide();
       $('#upYoutube').hide();
       $('#gthumbnailYoutube').hide();
       $('#upVdo').hide();
       $('#gthumbnailVDO').hide();
   });

   $("#b3").on('ifChecked', function(event){
       $('#ActivityColor').hide();
       $('#gActivityImg').hide(); 
       $('.activityimg').hide(); 

       $('#bannerPC').hide();
       $('#bannerMB').hide();


       $('#upYoutube').show();
       $('#gthumbnailYoutube').show();
       $('#upVdo').hide();
       $('#upVideo').show();
       $('#gthumbnailVDO').hide();
   });

   $('#youtube').on('ifChecked', function(event){
      $('#upYoutube').show();
      $('#gthumbnailYoutube').show();

      $('#gthumbnailVDO').hide();
      $('#upVdo').hide();
  }); 

   $('#vdo').on('ifChecked', function(event){
       $('#upYoutube').hide();
       $('#gthumbnailYoutube').hide();

       $('#gthumbnailVDO').show();
       $('#upVdo').show();
   }); 


   if($('#banner_chek').val() == 'hidden' ){
       $('#banner').hide();

   }else{
    $('#banner').show();
            //Vakidate_();
        }
        
         //Vakidate_(true,false,false,false,false,false,false,false,false,false,false,false);
         $('#ch_banner').on('ifChecked', function(event){
            $('#banner').show();
            

            
        });
         $('#ch_banner').on('ifUnchecked', function (event) {
            $('#banner').hide();
        });

    //
    $(document).ready(function() {
        if($('#page').val() == 1){
            $('#bannerMB').show();
        }else{
            $('#bannerMB').hide();
        }
        $.validator.setDefaults({ ignore: "[contenteditable='true']" });

        $('#editBannerForm').validate({
            rules: {
                PageTypeId : {
                    required: true
                },
                startdate : {
                    required: function(){
                      if($('#ch_banner').is(':checked')){
                        return true;
                    } else {
                        return false;
                    }
                },
            },
            enddate : {
                required: function(){
                  if($('#ch_banner').is(':checked')){
                    return true;
                } else {
                    return false;
                }
            },
        },
        'lead_image_p[]': {
           required: function(){
              if($('#ch_banner').is(':checked')){
               if($('#b1').is(':checked')){
                return true;
            }else{
                return false;
            }
        } else {
            return false;
        }
    },
},
'lead_image_p_m[]' : {
    required: false
},
activityimg : {
    required: function(){
      if($('#ch_banner').is(':checked')){
       if($('#b2').is(':checked')){
        return true;
    }else{
        return false;
    }
} else {
    return false;
}
},
},
activitycolor : {
    required: function(){
      if($('#ch_banner').is(':checked')){
       if($('#b2').is(':checked')){
        return true;
    }else{
        return false;
    }
} else {
    return false;
}
},
},
thumbnailYoutube : {
    required: function(){
      if($('#ch_banner').is(':checked')){
       if($('#b3').is(':checked')){
        if($('#youtube').is(':checked')){
         return true;
     }
 }else{
    return false;
}
} else {
    return false;
}
},
},
url_youtube : {
 required: function(){
  if($('#ch_banner').is(':checked')){
   if($('#b3').is(':checked')){
    if($('#youtube').is(':checked')){
     return true;
 }
}else{
    return false;
}
} else {
    return false;
}
},
},
vdofile : {
    required: function(){
      if($('#ch_banner').is(':checked')){
       if($('#b3').is(':checked')){
        if($('#vdo').is(':checked')){
         return true;
     }
 }else{
    return false;
}
} else {
    return false;
}
},
},
thumbnailVDO : {
    required: function(){
      if($('#ch_banner').is(':checked')){
       if($('#b3').is(':checked')){
        if($('#vdo').is(':checked')){
         return true;
     }
 }else{
    return false;
}
} else {
    return false;
}
},
},
'lead_image_d[]' : {
    required: true
},
'lead_image_m[]'  : {
    required: true
},

},
messages: {
    PageTypeId : "กรุณาเลือกหน้าจอ",
    startdate : "กรุณาเลือกวันที่เริ่มต้น",
    enddate : "กรุณาเลือกวันที่สิ้นสุด",
    'lead_image_p[]': "กรุณาเลือกรูป",
    'lead_image_p_m[]': "กรุณาเลือกรูป",
    activityimg : "กรุณาเลือกรูป",
    thumbnailYoutube : "กรุณาเลือกรูป",
    url_youtube : "กรุณากรอก URL",
    vdofile : "กรุณาเลือก VDO",
    thumbnailVDO : "กรุณาเลือกรูป",
    'lead_image_d[]' : "กรุณาเลือกรูป",
    'lead_image_m[]'  : "กรุณาเลือกรูป",
    activitycolor : "กรุณาเลือกสี",


}
});

    });



</script>
<script src="js/validate_file_300kb.js"></script>

</body>
</html>
