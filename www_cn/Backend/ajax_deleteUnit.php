<?php
require_once 'include/dbConnect.php';

try {
    $id = $_GET['id'];

    $conn = (new dbConnect())->getConn();
    $sql = "DELETE u FROM LH_UNIT_PLAN_CONDO u LEFT JOIN LH_UNIT_PLAN_IMG m ON m.unit_plan_id = u.unit_plan_id
				WHERE u.unit_plan_id =".$id;
    $result= $conn->query($sql);


    echo json_encode($result->fetchAll());

} catch (\Exception $e) {
    return $e->getMessage();
}
?>