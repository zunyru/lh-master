<?php
session_start();

require_once 'include/dbConnect.php';
require_once 'include/dbCon_mssql.php'; // for insert utf8

	try {
		$plan_id = $_GET['plan_id'];

		$conn = (new dbConnect())->getConn();
		$sql_plan = "SELECT * FROM LH_PLANS
				WHERE plan_id =".$plan_id;
        $result['plan'] = $conn->query($sql_plan);
        $result['plan'] = $result['plan']->fetchAll();
        $conn = null;

		$conn = (new dbConnect())->getConn();
        $sql_plan_sub = "SELECT pf.function_plan_name_th, sp.function_plan_sub_plan_count_room,
        		pf.function_plan_pronoun
				FROM LH_FUNCTION_PLAN_SUB as sp
				LEFT JOIN LH_PLAN_FUNCTION as pf ON sp.function_plan_plan_id = pf.function_plan_id
				WHERE sp.plan_id =".$plan_id;
        $result['plan_sub'] = $conn->query($sql_plan_sub);
        if ($result['plan_sub']) {
        	$result['plan_sub'] = $result['plan_sub']->fetchAll();
        }
        $conn = null;

		$conn = (new dbConnect())->getConn();
        // $sql_galery_master = "	SELECT gps.img_plan_name FROM LH_GALERY_PLAN AS gp
								// LEFT JOIN LH_GALERY_PLAN_SUB AS gps ON gp.galery_plan_id = gps.galery_plan_id
								// WHERE gp.plan_id =".$plan_id;

        $sql_galery_master = "	SELECT gp.galery_plan_name, gp.galery_plan_img_seo FROM LH_GALERY_PLAN AS gp
								WHERE gp.plan_id = ".$plan_id;

		$result['gallery_master'] = $conn->query($sql_galery_master);
		if($result['gallery_master']) {
        	$result['gallery_master'] = $result['gallery_master']->fetchAll();
		}
        $conn = null;

		$conn = (new dbConnect())->getConn();
       //  $sql_galery_floor = "SELECT gfp.floor_plan_id, gfp.floor_plan_img, gfp.floor_plan_img_dis 
	      //   				FROM LH_GALERY_FLOOR_PLAN AS gfp
							// LEFT JOIN LH_PLANS AS p ON gfp.plan_id = p.plan_id
							// WHERE gfp.plan_id =".$plan_id;

        $sql_galery_floor = "SELECT gfp.floor_plan_img_id, gfp.floor_plan_img_name,p.plan_img,p.plan_seo,
							gfp.floor_plan_dis_th, gfp.floor_plan_dis_en, gfp.floor_plan_img_seo, gfp.number_floor_plan
							FROM LH_GALERY_FLOOR_PLAN_IMG AS gfp
							LEFT JOIN LH_PLANS AS p ON gfp.plan_id = p.plan_id
							WHERE gfp.plan_id =".$plan_id;


	  	$result_galery = mssql_query($sql_galery_floor);

		if ($result_galery) {
        	$results= array();
		    while ($result_fetch = mssql_fetch_array($result_galery)) {
		        $results[]= $result_fetch;
		    }
		    $result['gallery_floor'] = $results;
		}

		echo json_encode($result);

	} catch (\Exception $e) {
		return $e->getMessage();
	}
?>