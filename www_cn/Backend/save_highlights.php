<?php
session_start();
include './include/dbCon_mssql.php';
date_default_timezone_set('Asia/Bangkok');
$date = date("Y-m-d");

$highlights_name_th    = $_POST['highlights_name_th'];
$highlights_name_en    = $_POST['highlights_name_en'];
$highlights_dis_th     = $_POST['highlights_dis_th'];
$highlights_dis_en     = $_POST['highlights_dis_en'];
$page                  = $_POST['page'];
$highlights_url        = $_POST['highlights_url'];
$highlights_content    = $_POST['highlights_content'];
$highlights_content_en = $_POST['highlights_content_en'];
$date_start = $_POST['date_start'];
$date_end = $_POST['date'];

$year = substr($date_start, 6, 4);
$mont = substr($date_start, 3, 2);
$day = substr($date_start, 0, 2);
$date_start= $year."-".$mont."-".$day." 00:00:00.000";

$year2 = substr($date_end, 6, 4);
$mont2 = substr($date_end, 3, 2);
$day2 = substr($date_end, 0, 2);
$date_end= $year2."-".$mont2."-".$day2." 00:00:00.000";


$dates        = date("Y-m-d H:i:s");

function mssql_escape($str)
{
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return str_replace("'", "''", $str);
}

$query        = "SELECT COUNT(*) AS counts FROM LH_HIGHLIGHTS WHERE  highlights_name_th = N'$highlights_name_th'";
$query_result = mssql_query($query, $db_conn);
$count        = mssql_fetch_row($query_result);
$count[0];
if ($count[0] >= 1) {
    $_SESSION['add'] = '2';
    echo '<script>
                        window.history.go(-1);
                    </script>';
    exit();
}

$today = date("H:i:s");

if ($page == "image") {
    $seo_lead_img_file = $_POST['seo_lead_img_file'][0];
    $numrand           = (mt_rand());
    $images            = $_FILES['lead_img_file'];
    $file_image        = $images['name'];

// Loop through each file

    if (!empty($_FILES['lead_img_file'])) {
        $dates_file = date("Y-m-d");
        $path_img   = "fileupload/highlights_file/";

        $type_img = strrchr($file_image, ".");

        $newname_img   = $date . $numrand . $type_img;
        $path_copy_img = $path_img . $newname_img;

        if (move_uploaded_file($_FILES['lead_img_file']['tmp_name'], $path_copy_img)) {

            $sql_r = "INSERT INTO LH_HIGHLIGHTS (highlights_name_th,highlights_name_en,highlights_dis_th,highlights_dis_en,highlights_lead_img,highlights_content,highlights_lead_img_type,highlights_update,highlight_link,highlights_content_en,highlights_lead_img_seo,highlights_time_update,start_date,end_date) "
            . " VALUES (N'$highlights_name_th','$highlights_name_en',N'" . mssql_escape($highlights_dis_th) . "','" . mssql_escape($highlights_dis_en) . "','" . $path_img . $newname_img . "',N'" . mssql_escape($highlights_content) . "','$page','$dates',N'$highlights_url','" . mssql_escape($highlights_content_en) . "',N'$seo_lead_img_file','$today','$date_start','$date_end')";

            $query_r = mssql_query($sql_r, $db_conn);

            if ($query_r) {
                header("location:banner.php?add=1&tab=High");

            } else {
                $_SESSION['add'] = '-1';
                echo '<script>
                        window.history.go(-1);
                    </script>';
                exit();

            }
        }
    }

} else if ($page == "youtube") {
    $seo_lead_img_file = $_POST['seo_lead_img_file'][0];
    $url_youtube       = $_POST['url_youtube'];

    $numrand    = (mt_rand());
    $images     = $_FILES['img_youtube'];
    $file_image = $images['name'];

    $total = count($_FILES['img_youtube']['name']);

// Loop through each file

    if (!empty($_FILES['img_youtube'])) {
        $dates_file = date("Y-m-d");
        $path_img   = "fileupload/highlights_file/";

        $type_img = strrchr($file_image, ".");

        $newname_img   = $date . $numrand . $type_img;
        $path_copy_img = $path_img . $newname_img;

        if (move_uploaded_file($_FILES['img_youtube']['tmp_name'], $path_copy_img)) {

            $sql_r = "INSERT INTO LH_HIGHLIGHTS (highlights_name_th,highlights_name_en,highlights_dis_th,highlights_dis_en,highlights_lead_img,highlights_content,highlights_lead_img_type,highlights_update,highlight_link,highlight_thumbnail,highlights_content_en,highlights_thumbnail_seo,highlights_time_update,start_date,end_date) "
            . " VALUES (N'$highlights_name_th','$highlights_name_en',N'" . mssql_escape($highlights_dis_th) . "','" . mssql_escape($highlights_dis_en) . "','$url_youtube',N'" . mssql_escape($highlights_content) . "','$page','$dates',N'$highlights_url','$path_copy_img','" . mssql_escape($highlights_content_en) . "',N'$seo_lead_img_file','$today','$date_start','$date_end')";

            $query_r = mssql_query($sql_r, $db_conn);

            if ($query_r) {
                header("location:banner.php?add=1&tab=High");

            } else {
                $_SESSION['add'] = '-4';
                echo '<script>
                        window.history.go(-1);
                    </script>';
                exit();
            }
        }
    }

} elseif ($page == "vdo") {
    $seo_lead_img_file = $_POST['seo_lead_img_file'][0];
    $numrand           = (mt_rand());
    $images            = $_FILES['vdo_img'];
    $vdo               = $_FILES['vdo_file'];

    //print_r($vdo );
    $file_image = $images['name'];
    $file_vdo   = $vdo['name'];

    $total = count($_FILES['vdo_file']['name']);

    //for($i=0; $i<$total; $i++) {
    if (!empty($_FILES['vdo_file'])) {
        $dates_file = date("Y-m-d");
        $path_img   = "fileupload/highlights_file/";

        $type_img = strrchr($file_image, ".");
        $type_vdo = strrchr($file_vdo, ".");

        $newname_img = $date . $numrand . $type_img;
        $newname_vdo = $date . $numrand . "_vdo" . $type_vdo;

        $path_copy_img = $path_img . $newname_img;
        $path_copy_vdo = $path_img . $newname_vdo;

        if (move_uploaded_file($_FILES['vdo_img']['tmp_name'], $path_copy_img)) {
            if (move_uploaded_file($_FILES['vdo_file']['tmp_name'], $path_copy_vdo)) {
                //echo $newname_img;

               echo  $sql_r = "INSERT INTO LH_HIGHLIGHTS (highlights_name_th,highlights_name_en,highlights_dis_th,highlights_dis_en,highlights_lead_img,highlights_content,highlights_lead_img_type,highlights_update,highlight_link,highlight_thumbnail,highlights_content_en,highlights_thumbnail_seo,highlights_time_update,start_date,end_date) "
                . " VALUES (N'$highlights_name_th','$highlights_name_en',N'" . mssql_escape($highlights_dis_th) . "','" . mssql_escape($highlights_dis_en) . "','$path_copy_vdo',N'" . mssql_escape($highlights_content) . "','$page','$dates',N'$highlights_url','$path_copy_img','" . mssql_escape($highlights_content_en) . "',N'$seo_lead_img_file','$today','$date_start','$date_end')";

                $query_r = mssql_query($sql_r, $db_conn);

                if ($query_r) {
                    header("location:lhbanner.php?add=1&tab=High");

                } else {
                    $_SESSION['add'] = '-3';
                    echo '<script>
                        window.history.go(-1);
                    </script>';
                    exit();
                }
            }
        }
    }
}
