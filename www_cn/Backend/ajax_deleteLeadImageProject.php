<?php
require_once 'include/dbConnect.php';

	try {
		$LEAD_IMAGE_PROJECT_FILE_ID = $_GET['LEAD_IMAGE_PROJECT_FILE_ID'];

		$conn = (new dbConnect())->getConn();
		$sql = "DELETE FROM LH_LEAD_IMAGE_PROJECT_FILE
				WHERE LEAD_IMAGE_PROJECT_FILE_ID =".$LEAD_IMAGE_PROJECT_FILE_ID;
        $result= $conn->query($sql);

		echo json_encode($result->fetchAll());

	} catch (\Exception $e) {
		return $e->getMessage();
	}
?>