<?php
require_once 'include/dbConnect.php';

	try {
		$galery_project_id = $_GET['galery_project_id'];

		$conn = (new dbConnect())->getConn();
		$sql = "SELECT * FROM LH_GALERY_PROJECT gp
				LEFT JOIN LH_GALERY_IMG_PROJECT gip
				ON gp.galery_project_id = gip.galery_project_id
				WHERE gip.galery_project_id = ".$galery_project_id;
        $result= $conn->query($sql);

		echo json_encode($result->fetchAll());

	} catch (\Exception $e) {
		return $e->getMessage();
	}
?>