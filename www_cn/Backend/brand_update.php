<?php
session_start();
$_SESSION['group_id'];
include_once('include/dbBrands.php');
include './include/dbCon_mssql.php';
header('Content-Type:text/html; charset=utf8');
$_GET['page'] = 'brand';
$funBrabd = new dbBrands();
$id = $_GET['id'];

if (isset($_SESSION['add'])) {
	if ($_SESSION['add'] == 1) {
		//header("location:product_add.php");
		$success = "success";
		unset($_SESSION["add"]);
	} else if ($_SESSION['add'] == 2) {

		$error = "error"; //ค่าซ้ำ
		$_SESSION['add'] = '';

	} else if ($_SESSION['add'] == -1) {
		$errors = "errors";
		unset($_SESSION["add"]);
	} else if ($_SESSION['add'] == -2) {
		$errors2 = "errors";
		unset($_SESSION["add"]);
	}
}

?>
          <!DOCTYPE html>
          <html lang="en">
          <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <!-- Meta, title, CSS, favicons, etc. -->
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <title>LAND & HOUSES</title>

            <!-- Bootstrap -->
            <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
            <!-- Font Awesome -->
            <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
            <!-- NProgress -->
            <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
            <!-- Custom Theme Style -->
            <link href="../build/css/custom.min.css" rel="stylesheet">
            <!-- Fancybox image popup -->
            <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
            <!-- bootstrap-progressbar -->
            <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
            <!-- jQuery -->
            <script src="../vendors/jquery/dist/jquery.min.js"></script>

            <script src="../build/js/custom.min.js"></script>      <script src="../build/js/custom.min.js"></script>
            <!-- uploade img -->
            <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
            <!--uploade-->
            <!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
            <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
            <!-- confirm-->
            <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
            <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>
            <style>
            .file-caption-main .btn-file {
              overflow: visible;
            }

            .file-caption-main .btn-file .error {
              position: absolute;
              bottom: -32px;
              right: 30px;
            }
          </style>

        </head>

        <body class="nav-md">
          <div class="container body">
            <div class="main_container">
              <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                  <div class="navbar nav_title" style="border: 0;">
                    <a href="index.html" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
                  </div>

                  <div class="clearfix"></div>

                  <!-- menu profile quick info -->
                  <?php include './master/navbar.php';?>
                  <!-- /menu footer buttons -->
                </div>
              </div>

              <!-- top navigation -->
              <?php include './master/top_nav.php';?>
              <!-- /top navigation -->
              <?php
$sql = "SELECT * FROM LH_BRANDS WHERE brand_id ='" . $id . "'";
$stmt = mssql_query($sql, $db_conn);
$resulft = mssql_fetch_array($stmt);
// $qr_id = $funBrabd->list_brand($id );
// $resulft = $qr_id->fetch( PDO::FETCH_ASSOC );

?>
              <!-- page content -->
              <div class="right_col" role="main">
                <div class="">
                  <div class="clearfix"></div>
                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="x_panel">
                        <div class="x_title">
                          <h2><a href="brand.php">ระบบจัดการข้อมูล Brand</a> > แกไขข้อมูล Brand : <?php echo $resulft['brand_name_th'] ?></h2>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                          <p class="font-gray-dark">
                          </p><br>
                          <?php if (isset($success)) {?>
                          <div class="row">
                            <div class="col-md-2"></div>
                            <div class="form-group">
                              <div class="alert alert-success col-md-6" id="alert">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <strong>แก้ไขข้อมูลเรียบร้อยแล้ว</strong>
                              </div>
                            </div>
                            <div class="col-md-6"></div>
                          </div>
                          <?php } else if (isset($error)) {?>
                          <div class="row">
                            <div class="col-md-2"></div>
                            <div class="form-group">
                              <div class="alert alert-danger col-md-6" id="alert">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <strong>มีชื่อนี้อยู่ในระบบแล้ว</strong>
                              </div>
                            </div>
                            <div class="col-md-4"></div>
                          </div>
                          <?php } else if (isset($errors)) {?>

                          <div class="row">
                            <div class="col-md-2"></div>
                            <div class="form-group">
                              <div class="alert alert-danger col-md-6" id="alert">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <strong>เกิดข้อผิดพลาด !</strong> ไม่สามารถบันทึกข้อมมูลได้
                              </div>
                            </div>
                            <div class="col-md-4"></div>
                          </div>
                          <?php } else if (isset($errors2)) {?>
                          <div class="row">
                            <div class="col-md-2"></div>
                            <div class="form-group">
                              <div class="alert alert-danger col-md-6" id="alert">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <strong>ไม่สามารถลบข้อมมูลได้ !</strong> Brand นี้ถูกผูกกับโครงการอยู่
                              </div>
                            </div>
                            <div class="col-md-4"></div>
                          </div>
                          <?php }?>
                          <form class="form-horizontal form-label-left" action="update_brand.php" method="post" enctype="multipart/form-data" id="commentForm">

                            <div class="form-group">
                              <label class="control-label col-md-2" for="first-name">ระบุชื่อ Brand จีน <span class="required">*</span>
                              </label>
                              <div class="col-md-6">
                                <input type="text"  required class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อ Brand (จีน)" name="brand_name_th" value="<?=$resulft['brand_name_th']?>">
                                <input type="hidden" name="id" value="<?=$resulft['brand_id']?>">
                              </div>
                            </div>
                            <div class="form-group hide-for-th">
                              <label class="control-label col-md-2" for="first-name">ระบุชื่อ Brand อังกฤษ <span class="required">*</span>
                              </label>
                              <div class="col-md-6">
                                <input type="text"  required class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อ Brand (อังกฤษ)" name="brand_name_en" value="<?=$resulft['brand_name_en']?>">
                              </div>
                            </div>
                            <?php
$req = "*";
$required = "required";
?>

                            <?php if (isset($resulft['logo_brand_th']) && ($resulft['logo_brand_th'] != "")) {
	$req = "";
	$required = "";?>

                              <div class="form-group">
                                <label class="control-label col-md-2" for="first-name">ตัวอย่าง Brand Logo จีน<br>(.jpg , .png , .gif <br> ขนาด 150 x 150 px) <br> ภาพขนาดไม่เกิน 300 KB<span class="required"></span>
                                </label>
                                <div class="col-md-6">
                                  <div id="map-image-holder-brochure"></div>
                                  <div class="file-preview ">
                                    <div class="close fileinput-remove"></div>
                                    <div class="file-drop-disabled">
                                      <div class="file-preview-thumbnails">
                                        <div class="file-initial-thumbs">
                                          <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                            <div class="kv-file-content">
                                              <a class="fancybox" href="fileupload/images/brand_img/<?php echo $resulft['logo_brand_th']; ?>">
                                                <img src="fileupload/images/brand_img/<?php echo $resulft['logo_brand_th']; ?>" class="kv-preview-data file-preview-image"  style="max-height: 150px; width: auto">
                                              </a>
                                            </div>
                                            <div class="file-thumbnail-footer">
                                              <p>Alt text : </p>
                                              <input class="form-control" value="<?=$resulft['brand_img_seo_th']?>" name="brand_seo_th_edit" placeholder="Alt SEO">
                                              <div class="file-actions">
                                                <div class="file-footer-buttons">
                                                  <button type="button" id="<?php echo $resulft['brand_id']; ?>" onclick="deleteLogoBrandTH(<?php echo $resulft['brand_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                  <a class="fancybox" href="fileupload/images/brand_img/<?=$resulft['logo_brand_th']?>">
                                                    <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                  </a>
                                                </div>
                                                <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                              </div>
                                            </div>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="clearfix"></div>
                                      <div class="file-preview-status text-center text-success"></div>
                                      <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <?php }?>
                              <div class="form-group">
                                <label class="control-label col-md-2">เลือก Brand Logo จีน <?php echo $req ?> <br>(.jpg , .png , .gif <br> ขนาด 150 x 150 px) <br> ภาพขนาดไม่เกิน 300 KB</label><span class="required"></span>
                                <div class="col-md-6">
                                  <input type="file" id="img_plan" name="fileupload_th" <?php echo $required; ?> class="file-loading" >
                                </div>
                              </div>

                              <script>
                                $("#img_plan").fileinput({
                                  uploadUrl: "upload.php", // server upload action
                                  maxFileCount: 1,
                                  allowedFileExtensions: ["jpg", "png", "jpeg", "gif"],
                                  browseLabel: 'เลือกรูป',
                                  removeLabel: 'ลบ',
                                  browseClass: 'btn btn-success',
                                  showUpload: false,
                                  showRemove: false,
                                  showCaption: false, // ปิดช่องแสดงชื่อภาพ
                                  msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                  msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                                  msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                  msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                  xxx: 'brand_img_seo_th',
                                  dropZoneTitle: 'รูป Brand Logo จีน',
                                  deleteUrl: "/site/file-delete",
                                  // minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                  // minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                  // maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                  // maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                  maxFileSize :300 ,
                                  msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                  msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                  msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                  msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                  msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                  initialPreviewAsData: true,
                                  overwriteInitial: true,
                                });

                              </script>

                              <?php
$req = "*";
$required = "required";
?>

                              <?php if (isset($resulft['logo_brand_en']) && ($resulft['logo_brand_en'] != "")) {
	$req = "";
	$required = "";?>

                                <div class="form-group hide-for-th">
                                  <label class="control-label col-md-2" for="first-name">ตัวอย่าง Brand Logo อังกฤษ<br>(.jpg , .png ,.gif <br> ขนาด 150 x 150 px) <br> ภาพขนาดไม่เกิน 300 KB<span class="required"></span>
                                  </label>
                                  <div class="col-md-6">
                                    <div id="map-image-holder-brochure"></div>
                                    <div class="file-preview ">
                                      <div class="close fileinput-remove"></div>
                                      <div class="file-drop-disabled">
                                        <div class="file-preview-thumbnails">
                                          <div class="file-initial-thumbs">
                                            <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                              <div class="kv-file-content">
                                                <a class="fancybox" href="fileupload/images/brand_img/<?php echo $resulft['logo_brand_en']; ?>">
                                                  <img src="fileupload/images/brand_img/<?php echo $resulft['logo_brand_en']; ?>" class="kv-preview-data file-preview-image"  style="max-height: 150px; width: auto;">
                                                </a>
                                              </div>
                                              <div class="file-thumbnail-footer">
                                                <p>Alt text : </p>
                                                <input class="form-control" value="<?=$resulft['brand_img_seo_en']?>" name="brand_seo_en_edit" placeholder="Alt SEO">

                                                <div class="file-actions">
                                                  <div class="file-footer-buttons">
                                                    <button type="button" id="<?php echo $resulft['brand_id']; ?>" onclick="deleteLogoBrandEN(<?php echo $resulft['brand_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                    <a class="fancybox" href="fileupload/images/brand_img/<?=$resulft['logo_brand_en']?>">
                                                      <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                    </a>
                                                  </div>
                                                  <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                                </div>
                                              </div>

                                            </div>
                                          </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="file-preview-status text-center text-success"></div>
                                        <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <?php }?>
                                <div class="form-group hide-for-th">
                                  <label class="control-label col-md-2">เลือก Brand Logo อังกฤษ <?php echo $req ?> <br>(.jpg , .png ,.gif <br> ขนาด 150 x 150 px) <br> ภาพขนาดไม่เกิน 300 KB</label><span class="required"></span>
                                  <div class="col-md-6">
                                    <input type="file" id="img_plan2" name="fileupload_en" <?php echo $required; ?> class="file-loading" >
                                  </div>
                                </div>
                                <script>
                                  $("#img_plan2").fileinput({
                              uploadUrl: "upload.php", // server upload action
                              maxFileCount: 1,
                              allowedFileExtensions: ["jpg", "png", "jpeg" , "gif"],
                              browseLabel: 'เลือกรูป',
                              removeLabel: 'ลบ',
                              browseClass: 'btn btn-success',
                              showUpload: false,
                              showRemove:false,
                              showCaption: false, // ปิดช่องแสดงชื่อภาพ
                              msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                              msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                              msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                              msgZoomModalHeading: 'ตัวอย่างละเอียด',
                              xxx:'brand_img_seo_en',
                              dropZoneTitle : 'รูป Brand Logo อังกฤษ',
                             // minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                             // minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                             // maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                             // maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                             maxFileSize :300 ,
                             msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                             msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                             msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                             msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                             msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                           });
                         </script>

                         <div class="form-group">
                          <label class="control-label col-md-2" for="first-name">Brand Concept (จีน)<span class="required"></span>
                          </label>
                          <div class="col-md-6">
                            <textarea id="message"  class="form-control" name="concept_th" placeholder="กรอกBrand Concept (จีน)" rows="5" ><?php if (!empty($resulft['brand_concep_th'])) {echo $resulft['brand_concep_th'];}?></textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-2" for="first-name">รายละเอียด Brand Concept (จีน)<span class="required"></span>
                          </label>
                          <div class="col-md-6">
                            <textarea id="message" class="form-control" name="message_th"  placeholder="กรอกรายละเอียด Brand Concept (จีน)" rows="7" ><?php if (!empty($resulft['message_th'])) {echo $resulft['message_th'];} else {echo '';}?></textarea>
                          </div>
                        </div>
                        <div class="form-group hide-for-th">
                          <label class="control-label col-md-2" for="first-name">Brand Concept (อังกฤษ)<span class="required"></span>
                          </label>
                          <div class="col-md-6">
                            <textarea id="message"  class="form-control" name="concept_en" placeholder="กรอกรายละเอียด Brand Concept (อังกฤษ)" rows="5" ><?=$resulft['brand_concep_en']?></textarea>
                          </div>
                        </div>
                        <div class="form-group hide-for-th">
                          <label class="control-label col-md-2" for="first-name">รายละเอียด Brand Concept (อังกฤษ)<span class="required"></span>
                          </label>
                          <div class="col-md-6">
                            <textarea id="message"  class="form-control" name="message_en" placeholder="กรอกBrand Concept (อังกฤษ)" rows="7" ><?php if (!empty($resulft['message_en'])) {echo $resulft['message_en'];} else {echo '';}?></textarea>
                          </div>
                        </div>
                      </div>
                      <center><br>
                        <div class="col-md-8">

                         <!--  <a href="brand_update.php?delete=1&id="><button type="button" name="delete" class="btn btn-danger">Delete</button> -->
                          <?php if ($id != 23) {?>
                          <a class="confirms" data-title="ยืนยันการลบข้อมูล" name="delete" href="delete_brand.php?id=<?=$id?>">
                            <button type="button" class="btn btn-danger">Delete</button></a>
                            <?php }?>
                            <button type="submit" name="submit" class="btn btn-success">Update</button>
                          </div>
                        </center>
                      </form>
                    </div>
                  </div>
                </div>
              </div>


              <script type="text/javascript">
                $('a.confirms').confirm({
                  content: 'คุณแน่ใจที่จะลบข้อมูลนี้ !',
                  buttons: {
                    Yes: {
                      text: 'Yes',
                      btnClass: 'btn-danger',
                      keys: ['enter', 'a'],
                      action: function(){
                        location.href = this.$target.attr('href');
                      }
                    },
                    No: {
                      text: 'No',
                      btnClass: 'btn-default',
                      keys: ['enter', 'a'],
                      action: function(){
                          // button action.
                        }
                      },

                    }
                  });
                </script>

              </div>
            </div>

            <!-- Bootstrap -->
            <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
            <!-- FastClick -->
            <script src="../vendors/fastclick/lib/fastclick.js"></script>
            <!-- NProgress -->
            <script src="../vendors/nprogress/nprogress.js"></script>

            <script type="text/javascript" src="js/bootstrap-imgupload.js"></script>
            <!-- Validate -->
            <script src="../build/js/jquery.validate.js"></script>

            <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>

            <!-- bootstrap-progressbar -->
            <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>

            <!-- Custom Theme Scripts -->
            <script src="../build/js/custom.min.js"></script>


            <script>
              $(document).ready(function() {

                $("#commentForm").validate({
                  rules: {
                    brand_name_th: "required",
                    brand_name_en: "required",

                  },
                  messages: {
                    brand_name_th: "กรุณากรอกชื่อ Brand จีน !",
                    brand_name_en : "กรุณากรอกชื่อ Brand อังกฤษ !",
                    fileupload_th :" &nbsp; กรุณาเลือกรูป !",
                    fileupload_en : " &nbsp; กรุณาเลือกรูป !",

                  }
                });

                $("#lead_img_file").rules("add", {
                  required:true,
                  messages: {
                    required: " &nbsp; ไม่มีรูป !"
                  }
                });

              });
            </script>


            <script type="text/javascript">
              $('.fancybox').fancybox();
            </script>
            <script type="text/javascript">
              $('#imgupload1').imgupload();
            </script>
            <script type="text/javascript">
              $('#imgupload2').imgupload();
            </script>

            <!-- Custom Theme Scripts -->

            <script>
              $(document).ready(function(){
                $("#alert").show();
                $("#alert").fadeTo(6000, 500).slideUp(500, function(){
                  $("#alert").alert('close');
                });
              });
            </script>
            <script>
              function deleteLogoBrandTH(id) {
                $.confirm({
                  title: 'ลบรูปนี้ !',
                  content: 'คุณแน่ใจที่จะลบรูปนี้!',
                  buttons : {
                    Yes: {
                      text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                              var self = this;
                              return $.ajax({
                                url: 'ajax_deleteLogoBrandTH.php?brand_id=' + id,
                                dataType: 'json',
                                method: 'get'
                              }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                  }).fail(function () {
                                    self.setContent('Something went wrong.');
                                  });
                                }
                              },
                              No: function () {
                            //$.alert('ยกลเิก!');
                          },
                        }
                      });
              }

            </script>
            <script>
              function deleteLogoBrandEN(id) {
                $.confirm({
                  title: 'ลบรูปนี้ !',
                  content: 'คุณแน่ใจที่จะลบรูปนี้!',
                  buttons : {
                    Yes: {
                      text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                              var self = this;
                              return $.ajax({
                                url: 'ajax_deleteLogoBrandEN.php?brand_id=' + id,
                                dataType: 'json',
                                method: 'get'
                              }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                  }).fail(function () {
                                    self.setContent('Something went wrong.');
                                  });
                                }
                              },
                              No: function () {
                            //$.alert('ยกลเิก!');
                          },
                        }
                      });
              }

            </script>
            <script src="js/validate_file_300kb.js"></script>

          </body>
          </html>