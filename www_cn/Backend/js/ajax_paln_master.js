
$(document).ready(function () {
    var listpic=[]; //push order position gallery
    initGallery();

    $('#showPlan').change(function () {
        var params = {
            id_plan : $(this).val(),
            id_home_sell : $('#home_sell_id').val(),
        };
        //console.log($('#home_sell_id').val());
        showPaln(params);

    });

    $('#home').change(function () {
        var params = {
            id_plan : $('#showPlan').val()
        };
        if(this.checked) {
            params['check_homesell'] =  'homesell';
            setHtmlByCheckbOx(params);
        }else {
            showPaln(params);
        }
    });

    $('#showIcon').change(function () {
        var params = {
            id_icon : $(this).val()
        };

        showIcon(params);


    });

    $('#showgallery_master').change(function () {
        var params = {
            id_master : $(this).val()
        };
        //clear list gallery
        listpic=[];
        $('input[name="selectGallery"]').val('');

        showMaster(params);


    });

    $('#form_home_sell').on('change', 'input[type=checkbox]', function(e) {
        //console.log(this.name+' '+this.value+' '+this.checked);
        if(this.name=='gallpo'){
            if (this.checked) {
                listpic.push(this.value);
                $('div[name="dispos'+this.value+'"]').html(listpic.length);
            } else {
                //remove
                var index = listpic.indexOf(this.value);
                if(index!=-1){
                    listpic.splice(index, 1);
                }
                $('div[name="dispos'+this.value+'"]').html('');
                for (i = index; i < listpic.length; i++) {
                    var tmpval=$('div[name="dispos'+listpic[i]+'"]').html();
                    if(tmpval!=''){
                        $('div[name="dispos'+listpic[i]+'"]').html(parseInt(tmpval)-1);
                    }
                }
            }
            $('input[name="selectGallery"]').val(listpic.toString());
        }
        
    });

    function initGallery(){
        //set listpic[]
        $arrval=$('input[name="selectGallery"]').val();
        if($arrval!==''){
            listpic=$arrval.split(',');
        }
        for (i = 0; i < listpic.length; i++) {
            $('div[name="dispos'+listpic[i]+'"]').html(String(i+1));
            $("#form_home_sell input:checkbox").each(function() {
                if(this.name=='gallpo'){
                    if(this.value==listpic[i]){
                        //console.log(this.value);
                        this.setAttribute('checked', 'checked');
                    }
                }

            });
        }
        //console.log('curposition : '+listpic.toString());
        // console.log('listgallpo--->'+$('input[name="selectGallery"]').val());
    }

    //$('input[type=radio][name=optradio_vdo]').trigger('change');

});



function showPaln(params) {
    $.ajax({
        type: "POST",
        url: "ajex_getplan.php",
        data: params,
        async: false,
        cache: false
    }).success(function(data) {
        $('#id').val(data.q1.plan_id);
        $('#use_full').val(data.q1.plan_useful);
        $("#use_full").prop('disabled', true);

        $('#room').html('<br><h4>ระบุฟังก์ชั่นของบ้าน</h4>');
        $.each(data.q2,function (i,val) {
            $('#room').append('<div class="form-group">'+
                '<label class="control-label col-md-2" for="first-name">'+val.function_plan_name_th+'</label>'+
                '<div class="col-md-3">'+
                '<input type="text" id="rooms" class="form-control col-md-7 col-xs-12 wdpercent1 rooms" value="'+val.function_plan_sub_plan_count_room+'">'+
                '<label class="control-label col-md-3 wdlabel1" >'+val.function_plan_pronoun+
                '</label>'+
                '</div>'+
                '</div>');
            $(".rooms").prop('disabled', true);
        });

        // ima series
        $('#series').html('');
        $('#series').append('<div class="form-group">'+
            '<label class="control-label col-md-2" for="first-name">ภาพ ICON ซีรี่ส์แบบบ้าน'+
            '</label><div class="col-md-3"><div class="file-preview">'+
            '<div class="close fileinput-remove"></div>'+
            '<div class="file-drop-disabled">'+
            '<div class="file-preview-thumbnails">'+
            '<div class="file-initial-thumbs">'+
            '<div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">'+
            '<div class="kv-file-content">'+
            '<a class="fancybox" rel="group_galery_master" href="fileupload/images/series_img/series_img_logo/'+data.q3.series_logo+'">'+
            '<img src="fileupload/images/series_img/series_img_logo/'+data.q3.series_logo+'" class="kv-preview-data file-preview-image" style="width:150px;height:auto;">'+
            '</a>'+
            '</div>'+

            '<div class="file-thumbnail-footer">'+
            '<p>Alt text : '+data.q3.series_logo_seo+'</p>'+
            '<div class="file-actions">'+
            '<div class="file-footer-buttons">'+
            '<a class="fancybox" href="fileupload/images/series_img/series_img_logo/'+data.q3.series_logo+'">'+
            '<button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>'+
            '</a>'+
            '</div>'+
            '<span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>'+
            '</div>'+
            '</div>'+

            '</div>'+
            '</div>'+
            '</div>'+
            '<div class="clearfix"></div>'+
            '<div class="file-preview-status text-center text-success"></div>'+
            '<div class="kv-fileinput-error file-error-message" style="display: none;"></div>'+
            '</div>'+
            '</div></div>');

        //plan_model
        $('#plan_model').html('');
        var imgModelList = '';
        var ch_model = [];
        var chek_model ='';

        $.each(data.model,function (i,val) {
            // console.log(val.model_img_path);
            // console.log(data.ch_model.model_image);
            if(data.ch_model.model_image == val.model_img_path){
              chek_model = 'checked';
            }else{
              chek_model = '';
              if(i == 0){
                 chek_model = 'checked';
              }  
            }

            imgModelList += '<div class="file-preview-thumbnails">'+
                '<div class="file-initial-thumbs">'+
                '<div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">'+
                '<div class="kv-file-content">'+
                '<a class="fancybox" rel="group_galery_master" href="'+val.model_img_path+'">'+
                '<img src="'+val.model_img_path+'" class="kv-preview-data file-preview-image" style="width:150px;height:auto;">'+
                '</a>'+
                '<div class="form-group" style="margin-top: 25px;">'+
                '<div class="col-md-12">'+
                '<input name="list_model_image[]" id="'+i+'" value="'+val.model_img_path+'" type="radio" '+chek_model+' >'+
                '<label for="'+i+'">&nbsp;&nbsp;เลือกเป็นภาพหลัก</label>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>';   
        }); 

        var templateModel = 
            '<label class="control-label col-md-2" for="first-name">ภาพ แบบบ้าน *<br>(เลือกรูปแบบบ้านหลัก)<br>เลือกรูปอย่างน้อย 1 รูป'+
            '</label>'+
            '<div class="form-group">'+
            '<label class="control-label col-md-2" for="first-name">'+
            '</label>' +
            '<div class="col-md-10">' +
            '<div class="file-preview">'+
            '<div class="close fileinput-remove">' +
            '</div>'+
            '<div class="file-drop-disabled">'+

            imgModelList+

            '<div class="clearfix">' +
            '</div>'+
            '<div class="file-preview-status text-center text-success">' +
            '</div>'+
            '<div class="kv-fileinput-error file-error-message" style="display: none;">' +
            '</div>'+
            '</div>'+
            '</div>' +
            '</div>' +
            '</div>';

        $('#plan_model').append(templateModel);   


        // ima plan
        /*
        $('#plan').html('');
        $('#plan').append('<div class="form-group">'+
            '<label class="control-label col-md-2" for="first-name">ภาพ แบบบ้าน'+
            '</label><div class="col-md-6"><div class="file-preview">'+
            '<div class="close fileinput-remove"></div>'+
            '<div class="file-drop-disabled">'+
            '<div class="file-preview-thumbnails">'+
            '<div class="file-initial-thumbs">'+
            '<div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">'+
            '<div class="kv-file-content">'+
            '<a class="fancybox" rel="group_galery_master" href="'+data.q1.plan_img+'">'+
            '<img src="'+data.q1.plan_img+'" class="kv-preview-data file-preview-image" style="width:150px;height:auto;">'+
            '</a>'+
            '</div>'+

            '<div class="file-thumbnail-footer">'+
            '<p>Alt text : '+data.q1.plan_seo+'</p>'+
            '<div class="file-actions">'+
            '<div class="file-footer-buttons">'+
            '<a class="fancybox" href="'+data.q1.plan_img+'">'+
            '<button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>'+
            '</a>'+
            '</div>'+
            '<span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>'+
            '</div>'+
            '</div>'+

            '</div>'+
            '</div>'+
            '</div>'+
            '<div class="clearfix"></div>'+
            '<div class="file-preview-status text-center text-success"></div>'+
            '<div class="kv-fileinput-error file-error-message" style="display: none;"></div>'+
            '</div>'+
            '</div></div>');
           */ 

        $('#foot').html('<br>');
        $.each(data.q4,function (j,valu) {
            $('#foot').append('<div class="form-group">'+
                '<label class="control-label col-md-2" for="first-name"> ภาพ Floor Plan ชั้น '+ valu.number_floor_plan +
                '</label><div class="col-md-6"><div class="file-preview">'+
                '<div class="close fileinput-remove"></div>'+
                '<div class="file-drop-disabled">'+
                '<div class="file-preview-thumbnails">'+
                '<div class="file-initial-thumbs">'+
                '<div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">'+
                '<div class="kv-file-content">'+
                '<a class="fancybox" rel="group_galery_master" href="'+valu.floor_plan_img_name+'">'+
                '<img src="'+valu.floor_plan_img_name+'" class="kv-preview-data file-preview-image" style="width:150px;height:auto;">'+
                '</a>'+
                '</div>'+
                '<div class="file-thumbnail-footer">'+
                '<p>Alt text : '+valu.floor_plan_img_seo+'</p>'+
                '<div class="file-actions">'+
                '<div class="file-footer-buttons">'+
                '<a class="fancybox" href="'+valu.floor_plan_img_name+'">'+
                '<button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>'+
                '</a>'+
                '</div>'+
                '<span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '<div class="clearfix"></div>'+
                '<div class="file-preview-status text-center text-success"></div>'+
                '<div class="kv-fileinput-error file-error-message" style="display: none;"></div>'+
                '</div></div>'+
                '</div></div>'+
                '<div class="form-group">'+
                '<label class="control-label col-md-2" for="first-name">รายละเอียด ชั้น '+ valu.number_floor_plan +
                '</label>'+
                '<div class="col-md-6">'+
                '<textarea class="form-control foot" rows="5">'+valu.floor_plan_dis_th+'</textarea>'+
                '</div>'+
                '</div>'
            );
            $(".foot").prop('disabled', true);
        });


        $('#galery_img').html('<br><h4>ภาพ galery บ้านพร้อมขาย</h4>');
        var imgList = '';
        var chek;

        var ch = [];
        $.each(data.gallery_plan_ch,function (i,val) {
            ch.push(val.galery_plan_id);
        });
        // console.log(data.gallery_plan_ch);
        // console.log(ch);
        $.each(data.qalery,function (i,val) {
            if(jQuery.inArray( val.galery_plan_id, ch )>=0){
                chek = 'checked';
            }else{
                chek = '';
            }

            imgList += '<div class="file-preview-thumbnails">'+
                '<div class="file-initial-thumbs">'+
                '<div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">'+
                '<div class="kv-file-content">'+
                '<a class="fancybox" rel="group_galery_master" href="'+val.galery_plan_name+'">'+
                '<img src="'+val.galery_plan_name+'" class="kv-preview-data file-preview-image" style="width:150px;height:auto;">'+
                '</a>'+
                '<div class="col-md-3">'+
                '<input name="list_paln[]" value="'+val.galery_plan_id+'" type="checkbox" '+chek+' >'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>';
        });
        var templateGall = '<div class="form-group">'+
            '<label class="control-label col-md-2" for="first-name">'+
            '</label>' +
            '<div class="col-md-10">' +
            '<div class="file-preview">'+
            '<div class="close fileinput-remove">' +
            '</div>'+
            '<div class="file-drop-disabled">'+

            imgList+

            '<div class="clearfix">' +
            '</div>'+
            '<div class="file-preview-status text-center text-success">' +
            '</div>'+
            '<div class="kv-fileinput-error file-error-message" style="display: none;">' +
            '</div>'+
            '</div>'+
            '</div>' +
            '</div>' +
            '</div>';

        $('#galery_img').append(templateGall);


        var a = '';
    });
}

function setHtmlByCheckbOx(params) {
    $.ajax({
        type: "POST",
        url: "ajex_getplan.php",
        data: params,
        async: false,
        cache: false
    }).success(function(data) {
        $('#galery_img').html('<br><h4>ภาพ galery ภาพภายในบ้านตกแต่งพร้อมขาย</h4>');
        var imgList = '';
        $.each(data.qalery,function (i,val) {
            imgList += '<div class="file-preview-thumbnails" id="view1">'+
                '<div class="file-initial-thumbs">'+
                '<div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">'+
                '<div class="kv-file-content">'+
                '<a class="fancybox" rel="group_galery_master" href="'+val.galery_plan_name+'">'+
                '<img src="'+val.galery_plan_name+'" class="kv-preview-data file-preview-image" style="width:150px;height:auto;">'+
                '</a>'+
                '<div class="col-md-3">'+
                '<input name="list_plan[]" value="'+val.galery_plan_id+'" type="checkbox">'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>';
        });
        var templateGall = '<div class="form-group">'+
            '<label class="control-label col-md-2" for="first-name">'+
            '</label>' +
            '<div class="col-md-10">' +
            '<div class="file-preview">'+
            '<div class="close fileinput-remove">' +
            '</div>'+
            '<div class="file-drop-disabled">'+

            imgList+

            '<div class="clearfix">' +
            '</div>'+
            '<div class="file-preview-status text-center text-success">' +
            '</div>'+
            '<div class="kv-fileinput-error file-error-message" style="display: none;">' +
            '</div>'+
            '</div>'+
            '</div>' +
            '</div>' +
            '</div>';

        $('#galery_img').append(templateGall);
    });
}
///icon
function showIcon(params) {
    $.ajax({
        type: "POST",
        url: "ajex_getplan.php",
        data: params,
        async: false,
        cache: false
    }).success(function(data) {
        //icon
        if(data.icon.icon_img == 'undefined' || data.icon.icon_img == '' || data.icon.icon_img == null){
        $('#icon').html('');
        }else{
        $('#icon').html('');    
        $('#icon').append('<div class="form-group">'+
            '<label class="control-label col-md-2" for="first-name">ภาพ icon กิจกรรม'+
            '</label><div class="col-md-4"><div class="file-preview">'+
            '<div class="close fileinput-remove"></div>'+
            '<div class="file-drop-disabled">'+
            '<div class="file-preview-thumbnails">'+
            '<div class="file-initial-thumbs">'+
            '<div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">'+
            '<div class="kv-file-content">'+
            '<a class="fancybox" rel="group_galery_master" href="fileupload/images/icon_img/'+data.icon.icon_img+'">'+
            '<img src="fileupload/images/icon_img/'+data.icon.icon_img+'" class="kv-preview-data file-preview-image" style="width:150px;height:auto;">'+
            '</a>'+
            '</div>'+

            '<div class="file-thumbnail-footer">'+
            '<div class="file-actions">'+
            '<div class="file-footer-buttons">'+
            '<a class="fancybox" href="fileupload/images/icon_img/'+data.icon.icon_img+'">'+
            '<button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>'+
            '</a>'+
            '</div>'+
            '<span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>'+
            '</div>'+
            '</div>'+

            '</div>'+
            '</div>'+
            '</div>'+
            '<div class="clearfix"></div>'+
            '<div class="file-preview-status text-center text-success"></div>'+
            '<div class="kv-fileinput-error file-error-message" style="display: none;"></div>'+
            '</div>'+
            '</div></div>');
    }

    });
}

///master
function showMaster(params) {
    $.ajax({
        type: "POST",
        url: "ajex_getplan.php",
        data: params,
        async: false,
        cache: false
    }).success(function(data) {
        $('#gallery_master').html('');
        var imgList = '';
        //var data.master_count;
        $.each(data.master,function (i,val) {


            imgList += '<div class="file-preview-thumbnails">'+
                '<div class="file-initial-thumbs">'+
                '<div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">'+
                '<div class="kv-file-content">'+
                '<a class="fancybox" rel="group_galery_master" href="'+val.path_funished+'">'+
                '<img src="'+val.path_funished+'" class="kv-preview-data file-preview-image" style="width:150px;height:auto;">'+
                '</a>'+
                '<div class="col-md-3">'+

                '<input name="gallpo" value="'+val.galery_funished_sub_id+'" type="checkbox" >'+
                '<div name="dispos'+val.galery_funished_sub_id+'" class="showposition"></div>'+

                '</div>' +
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>';

        });


        var templateGall = '<br><h4>ภาพ galery บ้านพร้อมตกแต่ง</h4><div class="form-group">'+
            '<label class="control-label col-md-2" for="first-name">'+
            '</label>' +
            '<div class="col-md-10">' +
            '<div class="file-preview">'+
            '<div class="close fileinput-remove">' +
            '</div>'+
            '<div class="file-drop-disabled">'+

            imgList+

            '<div class="clearfix">' +
            '</div>'+
            '<div class="file-preview-status text-center text-success">' +
            '</div>'+
            '<div class="kv-fileinput-error file-error-message" style="display: none;">' +
            '</div>'+
            '</div>'+
            '</div>' +
            '</div>' +
            '</div>';



        $('#gallery_master').append(templateGall);
    });
}
