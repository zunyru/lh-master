var firstPicYoutube="";
var firstPicActivity="";
var firstPicVDO="";
var firstFileVDO="";
$(document)
		.ready(
				function() {
					$('#alert').fadeIn('fast').delay(2000).fadeOut('slow');
					// date picker
					 //declare global js
					$('#startdate').datepicker(
							{
								dateFormat : "dd/mm/yy",
								autoclose : true,
								changeMonth : true,
								showDropdowns : true,
								changeYear : true,
								minDate : new Date(),
								onSelect : function(dateText) {
									var d = new Date($(this).datepicker(
											"getDate"));
									$("input#enddate").datepicker('option',
											'minDate', dateText);
									$("input#enddate").prop('disabled', false);
								}
							});

					$('#enddate').datepicker(
							{
								dateFormat : "dd/mm/yy",
								autoclose : true,
								changeMonth : true,
								showDropdowns : true,
								changeYear : true,
								minDate : new Date(),
								onSelect : function(dateText) {
									var d = new Date($(this).datepicker(
											"getDate"))
									$("input#startdate").datepicker('option',
											'maxDate', dateText);
								}
							});

					/** initial tool * */
					var bannertable = $('#datatable').dataTable({
						"bLengthChange" : false,
						"pageLength" : 50,
						"order" : [ [ 4, "desc" ] ],
						"columnDefs" : [ {
							"targets" : [ 4 ],
							"visible" : false,
							"searchable" : false
						} ]
					});

					$('#Project_tb').dataTable({
						"bLengthChange" : false,
						"pageLength" : 50,
						"order" : [ [ 3, "desc" ] ]
					});
                 					// forlar editor
					$('#editTh').froalaEditor({
	                    toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'fontFamily', 'fontSize', '|', 'specialCharacters', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', '-', '|', 'undo', 'redo', 'clearFormatting', 'selectAll', 'html', 'applyFormat', 'removeFormat', 'fullscreen', 'print'],
	                    pluginsEnabled: null,
	                    colorsText: [
	                        '#ff00ff', '#1ABC9C', '#54ACD2', '#2C82C9', '#9365B8', '#475577', '#CCCCCC',
	                        '#61BD6D', '#1ABC9C', '#54ACD2', '#2C82C9', '#9365B8', '#475577', '#CCCCCC',
	                        '#41A85F', '#00A885', '#3D8EB9', '#2969B0', '#553982', '#28324E', '#000000',
	                        '#F7DA64', '#FBA026', '#EB6B56', '#E25041', '#A38F84', '#EFEFEF', '#FFFFFF',
	                        '#FAC51C', '#F37934', '#D14841', '#B8312F', '#7C706B', '#D1D5D8', 'REMOVE'
	                    ],
	                    fontFamily: {
	                          "LHfont": 'Kittithada',
	                          "Roboto,sans-serif": 'Roboto',
	                          "Oswald,sans-serif": 'Oswald',
	                          "Montserrat,sans-serif": 'Montserrat',
	                          "'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
	                      },
	                    height: 200
	                    });
					$('#editEn').froalaEditor({
	                    toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'fontFamily', 'fontSize', '|', 'specialCharacters', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', '-', '|', 'undo', 'redo', 'clearFormatting', 'selectAll', 'html', 'applyFormat', 'removeFormat', 'fullscreen', 'print'],
	                    pluginsEnabled: null,
	                    colorsText: [
	                        '#ff00ff', '#1ABC9C', '#54ACD2', '#2C82C9', '#9365B8', '#475577', '#CCCCCC',
	                        '#61BD6D', '#1ABC9C', '#54ACD2', '#2C82C9', '#9365B8', '#475577', '#CCCCCC',
	                        '#41A85F', '#00A885', '#3D8EB9', '#2969B0', '#553982', '#28324E', '#000000',
	                        '#F7DA64', '#FBA026', '#EB6B56', '#E25041', '#A38F84', '#EFEFEF', '#FFFFFF',
	                        '#FAC51C', '#F37934', '#D14841', '#B8312F', '#7C706B', '#D1D5D8', 'REMOVE'
	                    ],
	                    fontFamily: {
	                          "LHfont": 'Kittithada',
	                          "Roboto,sans-serif": 'Roboto',
	                          "Oswald,sans-serif": 'Oswald',
	                          "Montserrat,sans-serif": 'Montserrat',
	                          "'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
	                      },
	                    height: 200
	                    });
				
					/** end initial tool * */

					intBanner();

					$("select[name='PageTypeId']").change(
							function() {
								//debugger;
								var radioSelect = $(
										'input[name=bannerchoose]:checked')
										.val();
								if (this.value == 1 && radioSelect == 1) {
									// Home & Banner Image
									showBannerLead(true);
								} else {
									if (radioSelect == 1) {
										showBannerLead(false);
									} else if (radioSelect == 2) {
										showActivityBanner();
									} else if (radioSelect == 3) {
										showVideoBanner();
									}
								}

					});

					$('input[name="bannerchoose"]').change(function() {
				        	   $('input[name="chageBannerChoose"]').val("change");
								var pageTypeIdSelect = $(
										'select[name="PageTypeId"]').val();
								if (this.value == 1 && pageTypeIdSelect == 1) {
									// Home & Banner Image
									resetForm('Banner');
									showBannerLead(true);
								} else {
									if (this.value == 1) {
										resetForm('Banner');
										showBannerLead(false);
									} else if (this.value == 2) {
										resetForm('Activity');
										showActivityBanner();
									} else if (this.value == 3) {
										resetForm('Video');
										showVideoBanner();
									}
								}

							});
					
					$('input[name="upvido"]').change(function() {
						$('input[name="chageBannerChoose"]').val("change");
						resetForm('Video');
						upVideoShow(this.value);
					});
					
					function changeBannerChoose(selected){

						var pageTypeIdSelect = $('select[name="PageTypeId"]').val();
				         if (selected == 1 && pageTypeIdSelect == 1) {
					         // Home & Banner Image
					         showBannerLead(true);
				         } else {
					            if (selected  == 1) {
						            showBannerLead(false);
					           } else if (selected  == 2) {
						                showActivityBanner();
					          } else if (selected  == 3) {
						                showVideoBanner();
					                   }
				              }
					}

					function intBanner() {
						if (typeof (pagestate) != "undefined"
								&& pagestate != null && pagestate == 'Add') {
							// initial add
							$('input:radio[name=bannerchoose]')[0].checked = true;
							// init banerLead data
							$('div#bannerMB').hide();
							$('div.upVideo').hide();
							$('div.activityimg').hide();
						} else if (typeof (pagestate) != "undefined"
							&& pagestate != null && pagestate == 'Edit') {
							// initial edit
							 $('input[name="oldBannerImg"]').val(JSON.stringify(defaultbannerImg));
						     $('input[name="oldBannerPC"]').val(JSON.stringify(bannerPC));
						     $('input[name="oldBannerMB"]').val(JSON.stringify(bannerMB));
							 $('select[name="PageTypeId"]').val(pageTypeId);
							 // $('#startdate').datepicker('setDate',sdate );
							 // $('#enddate').datepicker('setDate', edate);
							 initbannerImg("bannerLead",defaultbannerImg);
							 $('input[name="defaultBannerMainId"]').val(defaultLeadImageMainId);
							 $('input[name="bannerMainId"]').val(bannerMainId);
							 switch (bannerChoose) {
							    case "0":{//leadimage
							    	     $('input:radio[name="bannerchoose"][value="1"]').prop('checked', true);
							    	      changeBannerChoose(1);
							    	      if (pageTypeId == 1) {
							    	    	  //case home
							    	    	  initbannerImg("bannerPc",bannerPC);
							    	    	  initbannerImg("bannerMb",bannerMB);
							    	      }else{
							    	    	  //case other
							    	    	  initbannerImg("bannerPc",bannerPC);
							    	      }
							             }
							             break;
							    case "1":{//activity
							    	       $('input:radio[name="bannerchoose"][value="2"]').prop('checked', true);
							    	       changeBannerChoose(2);
							    	       intEditActivityImg(dataBannerMain);
							               $('input[name="activitycolor"]').val(dataBannerMain.color_code_activity);
							               $("#editTh").froalaEditor('html.set', dataBannerMain.text_activity_th);
									       $("#editEn").froalaEditor('html.set', dataBannerMain.text_activity_en);
							               }
							               break;
							    case "2":{//video
							    	   $('input:radio[name="bannerchoose"][value="3"]').prop('checked', true);
							    	   $('input:radio[name="upvido"][value="vdo"]').prop('checked', true);
							    	   changeBannerChoose(3);
							    	   initEditFileVDO(dataBannerMain);
							    	   intEditThumbnailVDO(dataBannerMain);
							           }
							        break;
							    case "3"://youtube
							          {
							    	   $('input:radio[name="bannerchoose"][value="3"]').prop('checked', true);
							    	   $('input:radio[name="upvido"][value="youtube"]').prop('checked', true);
							    	   changeBannerChoose(3);
							    	   $("#url_youtube").val(dataBannerMain.url_vdo);
							    	   intEditThumbnailYoutube(dataBannerMain);
							           }
							        break;
							 }
							 initEdit=false;
						}
					}
					
					function upVideoShow(typeshow){
						$('div#upVideo').show();
						if(typeshow=='vdo'){
							$('div#upVdo').show();
							$('div#gthumbnailVDO').show();
							$('div#upYoutube').hide();
							$('div#gthumbnailYoutube').hide();
						}else{
							// youtube
							$('div#upYoutube').show();
							$('div#gthumbnailYoutube').show();
							$('div#upVdo').hide();
							$('div#gthumbnailVDO').hide();
						}
					}

					function showBannerLead(isHome) {
						if (isHome) {
							$('div#bannerPC').show();
							$('div#bannerMB').show();
						} else {
							$('div#bannerPC').show();
							$('div#bannerMB').hide();
						}
						$('div.upVideo').hide();
						$('div.activityimg').hide();
					}

					function showActivityBanner() {
						$('div#bannerPC').hide();
						$('div#bannerMB').hide();
						$('div.upVideo').hide();
						$('div.activityimg').show();
					}

					function showVideoBanner() {
						$('div#bannerPC').hide();
						$('div#bannerMB').hide();
						$('div.activityimg').hide();
						var showVideoType=$("input[name='upvido']:checked").val();
						if(typeof (showVideoType) == "undefined" || showVideoType==''){
							$('input:radio[name=upvido]')[0].checked = true;
							showVideoType=$("input[name='upvido']:checked").val();
						}
						upVideoShow(showVideoType);
					}
					
					function resetForm(resetType){
					    if(resetType=='Banner'){
					    	$("input[name^=bannerPc]").fileinput('clear');
					    	$("input[name^=bannerMb]").fileinput('clear');
			         		$('.setIndexBannerPC').each(function() {
			         			$(this).parent().parent().remove();
			         		});
			         		$('.setIndexBannerMB').each(function() {
			         			$(this).parent().parent().remove();
			         		});
			         		
						}else if(resetType=='Activity'){
							 $("#activityimg").fileinput('clear');
							 $("#editTh").froalaEditor('html.set', '');
							 $("#editEn").froalaEditor('html.set', '');
					         	}else if(resetType=='Video'){
					         		   $("#url_youtube").val('');
					         		   $("#thumbnailYoutube").fileinput('clear');
					         		   $("#vdofile").fileinput('clear');
					         		   $("#thumbnailVDO").fileinput('clear');
					         	}else{
					         		// reset form all type
					         		// $("#bannerForm").trigger('reset');
					         		$('.setIndexBannerPC').each(function() {
					         			$(this).parent().parent().remove();
					         		});
					         		$('.setIndexBannerMB').each(function() {
					         			$(this).parent().parent().remove();
					         		});
					         		$("input[name^=bannerPc]").fileinput('clear');
							    	$("input[name^=bannerMb]").fileinput('clear');
					         		$("#activityimg").fileinput('clear');
									$("#editTh").froalaEditor('html.set', '');
									$("#editEn").froalaEditor('html.set', '');
									$("#url_youtube").val('');
					         		$("#thumbnailYoutube").fileinput('clear');
					         		$("#vdofile").fileinput('clear');
					         		$("#thumbnailVDO").fileinput('clear');
					         	}
					}
					
					// banner galary
					$("#bannerLead_0").fileinput({
					    uploadUrl: "save_lhbanner.php", // server upload
															// action
					    maxFileCount: 1,
					    allowedFileExtensions : [ "jpg", "png","jpeg", "gif" ],
					    browseLabel: ' เพิ่ม Banner Default',
					    browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
						removeLabel : 'ลบ',
						browseClass : 'btn btn-success',
						showUpload : false,
						showRemove : false,
						showCaption : false,
						autoReplace: false,
						msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
						msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
						msgZoomModalHeading : 'ตัวอย่างละเอียด',
						 xxx:'altLead',
						dropZoneTitle : 'Banner Default',
						maxFileSize : 500 ,
						minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                        minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                        maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                        maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
						msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
						msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
						msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
						msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
					    
					});
					
					$("#bannerPc_0").fileinput({
					    uploadUrl: "save_lhbanner.php", // server upload
															// action
					    maxFileCount: 1,
					    allowedFileExtensions : [ "jpg", "png","jpeg", "gif" ],
					    browseLabel: ' เพิ่ม Banner PC',
					    browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
						removeLabel : 'ลบ',
						browseClass : 'btn btn-success',
						showUpload : false,
						showRemove : false,
						showCaption : false,
						autoReplace: false,
						msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
						msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
						msgZoomModalHeading : 'ตัวอย่างละเอียด',
						 xxx:'altPC',
						dropZoneTitle : 'Banner Lead',
						maxFileSize : 500 ,
						minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                        minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                        maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                        maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
						msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
						msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
						msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
						msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
					    
					});
					
					$("#bannerMb_0").fileinput({
					    uploadUrl: "save_lhbanner.php", // server upload
															// action
					    maxFileCount: 1,
					    allowedFileExtensions: ["jpg", "png", "jpeg"],
					    browseLabel: ' เพิ่ม Banner MB',
					    browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
					    removeLabel: 'ลบ',
					    browseClass: 'btn btn-success',
					    showUpload: false,
					    showRemove:false,
					    showCaption: false,
					    autoReplace: false,
					    msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
					    msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
					    msgZoomModalHeading: 'ตัวอย่างละเอียด',
					    xxx:'altMB',
					    dropZoneTitle : 'Banner Lead Mobile',
						maxFileSize : 500 ,
						minImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
			            minImageHeight: 1920, // ขนาด ศุง ต่ำสุด
			            maxImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
			            maxImageHeight: 1920, // ขนาด ศุง ต่ำสุด
						msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
						msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
						msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
						msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.'
					});
//////////////////////////////////////////////////////////////////////////////////////////////////////////
					
					/** validate method * */
                    $.validator.setDefaults({ ignore: "[contenteditable='true']" });
					$.validator.addMethod("valueNotMatch", function(value, element, arg){
						  return arg != value;
					}, "Value Not Match arg.");
					
					$.validator.addMethod("colorCode", function(value, element, regexpr) {          
					    return /^#([A-Fa-f0-9]){6}$/.test($('#activitycolor').val());
					}, "Please enter Color code.");
					
					$.validator.addMethod("editDefaultBannerLead", function(value, element, args) {
						 
							 if(args!='' && args[0] < defaultbannerImg.length){
								 if(defaultbannerImg[args[0]]['position'] == undefined)
										   return (!jQuery.isEmptyObject(defaultbannerImg[args[0]])||value!='');
									 else
										   return value!='';
								   
							 }else
								 return value!=''
					}, "&nbsp; กรุณาเลือกรูป!");
					
					$.validator.addMethod("editBannerPc", function(value, element, args) { 
						//debugger;
						 var flag=$('input[name="chageBannerChoose"]').val();
						 if(flag=='' && args!='' && args[0] < bannerPC.length){
							 if(bannerPC[args[0]]['position'] == undefined)
									   return (!jQuery.isEmptyObject(bannerPC[args[0]])||value!='');
								 else
									   return value!='';
							   
						 }else
							 return value!=''
					}, "&nbsp; กรุณาเลือกรูป!");
					
					$.validator.addMethod("editBannerMb", function(value, element, args) {   
						 var flag=$('input[name="chageBannerChoose"]').val();
						 if(flag==''&& args!='' && args[0] < bannerMB.length){
						        if(bannerMB[args[0]]['position'] == undefined)
									   return (!jQuery.isEmptyObject(bannerMB[args[0]])||value!='');
								 else
									   return value!='';
							   
						 }else
							 return value!=''
					}, "&nbsp; กรุณาเลือกรูป!");
					
					$.validator.addMethod("editPicYoutube", function(value, element, args) { 
						var flag=$('input[name="chageBannerChoose"]').val();
						if(flag=='')
					       return (value!=''||firstPicYoutube!='') ;
						else
							return value!='';
					}, "&nbsp; กรุณาเลือกรูป!");
					
					$.validator.addMethod("editPicActivity", function(value, element, args) {   
						var flag=$('input[name="chageBannerChoose"]').val();
						if(flag=='')
					       return (value!=''||firstPicActivity!='') ;
						else
							return value!='';
							
					}, "&nbsp; กรุณาเลือกรูป!");
					
					$.validator.addMethod("editPicVDO", function(value, element, args) {
						var flag=$('input[name="chageBannerChoose"]').val();
						if(flag=='')
					       return (value!=''||firstPicVDO!='') ;
						else
							return value!='';
					}, "&nbsp; กรุณาเลือกรูป!");
					
					$.validator.addMethod("editFileVDO", function(value, element, args) { 
						var flag=$('input[name="chageBannerChoose"]').val();
						if(flag=='')
					       return (value!=''||firstFileVDO!='') ;
						else
							return value!='';
					}, "&nbsp; กรุณาเลือกรูป!");

					
				    //for editForm
				    $('form#editBannerForm').validate({
				        rules: {
				        	  PageTypeId : {valueNotMatch:"0"},
				        	   startdate : "required",
				        	     enddate : "required",
				        	 activityimg : {editPicActivity : true },
				           activitycolor : { required:true ,colorCode : true},
				             url_youtube : "required",
				        thumbnailYoutube : {editPicYoutube : true },
				                 vdofile : {editFileVDO : true },
				            thumbnailVDO : {editPicVDO : true },
				        },
				        messages: {
				        	  PageTypeId : {valueNotMatch:"กรุณาเลือกหน้าจอ"},
				        	   startdate : "กรุณาเลือกวันที่เริ่มต้น",
				        	     enddate : "กรุณาเลือกวันที่สิ้นสุด",
				        	 activityimg : { editPicActivity : "&nbsp; กรุณาเลือกรูป!" },
					       activitycolor : {required:"กรุณาระบุสีพื้นหลัง" , colorCode : "กรุณาระบุสีให้ถูกรูปแบบ"},
					         url_youtube : "กรุณาใส่ url youtube",
					    thumbnailYoutube : {editPicYoutube : "&nbsp; กรุณาใส่ ภาพthumbnailYoutube!" },
					             vdofile : {editFileVDO : "&nbsp; กรุณาเลือก video!" },
					        thumbnailVDO : {editPicVDO : "&nbsp; กรุณาเลือก thumbnailVDO!" },
					       
				        }
				    });

				    $('form#bannerForm').validate({
				        rules: {
				        	  PageTypeId : {valueNotMatch:"0"},
				        	   startdate : "required",
				        	     enddate : "required",
				        	 activityimg : "required",
				           activitycolor : { required:true ,colorCode : true},
				             url_youtube : "required",
				        thumbnailYoutube : "required",
				                 vdofile : "required",
				            thumbnailVDO : "required"
				        },
				        messages: {
				        	  PageTypeId : {valueNotMatch:"กรุณาเลือกหน้าจอ"},
				        	   startdate : "กรุณาเลือกวันที่เริ่มต้น",
				        	     enddate : "กรุณาเลือกวันที่สิ้นสุด",
				        	 activityimg : "&nbsp; กรุณาเลือกรูป!",
					       activitycolor : {required:"กรุณาระบุสีพื้นหลัง" , colorCode : "กรุณาระบุสีให้ถูกรูปแบบ"},
					         url_youtube : "กรุณาใส่ url youtube",
					    thumbnailYoutube : "&nbsp; กรุณาใส่ ภาพthumbnailYoutube!",
					             vdofile : "&nbsp; กรุณาเลือก video!",
					        thumbnailVDO : "&nbsp; กรุณาเลือก thumbnailVDO!"
					       
				        }
				    });

				    /*
				    $('form#editBannerForm button[name=save]').click(function( e ) {
				    	console.log("Edit Handler for .submit() called." );
				    	e.preventDefault();
				    	//bannerLead
				    	var index=0;
					    $("input[name^=bannerLead]").each(function () {
					    	 $(this).rules( "add", {
					    		 editDefaultBannerLead: [index],
							  	  messages: {
							  	    required: " &nbsp; กรุณาเลือกรูป!",
							  	   } 
					    	    });
					    	 index++;
					    }); 
					    //bannerPC
					    index=0;
					    $("input[name^=bannerPc]").each(function () {
					    	 $(this).rules( "add", {
					    		 editBannerPc:  [index],
							  	  messages: {
							  	    required: " &nbsp; กรุณาเลือกรูป!",
							  	   } 
					    	    });
					    	 index++;
					    });
					    //bannerMB
					    index=0;
					    $("input[name^=bannerMb]").each(function () {
					    	 $(this).rules( "add", {
					    		 editBannerMb : [index],
							  	  messages: {
							  	    required: " &nbsp; กรุณาเลือกรูป!",
							  	   } 
					    	    });
					    	index++;
					    });
					    //validate form
					    if($('form#editBannerForm').valid()){
					    	$('form#editBannerForm').submit();
					    }else
					    	return false;
				    });
                    */

				    $('form#bannerForm button[name=save]').click(function( e ) {
				    	console.log("Add Handler for .submit() called." );
				    	e.preventDefault();
				    	//bannerLead
					    $("input[name^=bannerLead]").each(function () {
					    	 $(this).rules( "add", {
							  	  required: true,
							  	  messages: {
							  	    required: " &nbsp; กรุณาเลือกรูป!",
							  	   }
					    	    });
					    });
					   // console.log($("#ch_banner").val());

					  if($("#ch_banner").is(":checked")) {
                          //bannerPC
                          $("input[name^=bannerPc]").each(function () {
                              $(this).rules("add", {
                                  required: true,
                                  messages: {
                                      required: " &nbsp; กรุณาเลือกรูป!",
                                  }
                              });
                          });
                          //bannerMB
                          $("input[name^=bannerMb]").each(function () {
                              $(this).rules("add", {
                                  required: true,
                                  messages: {
                                      required: " &nbsp; กรุณาเลือกรูป!",
                                  }
                              });
                          });
                      }
					     
					    /**
					    var checkValidate=true;
					    $("div.kv-fileinput-error").each(function(){
					        if($(this).css("display")=="block"){
					        	checkValidate=false;
					        	return;
					        }
					     });
					     **/
					    //$('form#bannerForm').validate();
					    if($('form#bannerForm').valid())
					    	 $('form#bannerForm').submit();
					    else
					    	return false;
				    });

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

					$("#thumbnailYoutube")
							.fileinput(
									{
										uploadUrl : "save_lhbanner.php", // server
										// upload
										// action
										maxFileCount : 1,
										allowedFileExtensions : [ "jpg", "png","jpeg", "gif" ],
										browseLabel : 'เลือกรูป',
										removeLabel : 'ลบ',
										browseClass : 'btn btn-success',
										showUpload : false,
										showRemove : false,
										showCaption : false,
										autoReplace: false,
										msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
										msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
										msgZoomModalHeading : 'ตัวอย่างละเอียด',
										dropZoneTitle : 'รูปภาพ Thumbnail Youtube',
										xxx:'altYoutube',
										maxFileSize : 500 ,
										minImageWidth: 1920, // ขนาด กว้าง
																// ต่ำสุด
			                            minImageHeight: 1080, // ขนาด ศุง
																// ต่ำสุด
			                            maxImageWidth: 1920, // ขนาด กว้าง
																// ต่ำสุด
			                            maxImageHeight: 1080, // ขนาด ศุง
																// ต่ำสุด
										msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
										msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
										msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
										msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
									});

					$("#vdofile")
							.fileinput(
									{
										uploadUrl : "save_lhbanner.php", // server
										// upload
										// action
										maxFileCount : 1,
										allowedFileExtensions : [ "mp4" ],
										browseLabel : 'เลือกไฟล์',
										removeLabel : 'ลบ',
										browseClass : 'btn btn-success',
										showUpload : false,
										showRemove : false,
										showCaption : false,
										autoReplace: false,
										maxFileSize : 204800 , // 200 MB
										msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
										msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
										msgZoomModalHeading : 'ตัวอย่างละเอียด',
										 xxx:'altVideo',
										dropZoneTitle : 'File VDO',
										msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
										msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
										msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
										msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
									});

					$("#thumbnailVDO")
							.fileinput(
									{
										uploadUrl : "save_lhbanner.php", // server
										// upload
										// action
										maxFileCount : 1,
										allowedFileExtensions : [ "jpg", "png","jpeg", "gif" ],
										browseLabel : 'เลือกรูป',
										removeLabel : 'ลบ',
										browseClass : 'btn btn-success',
										showUpload : false,
										showRemove : false,
										showCaption : false,
										autoReplace: false,
										msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
										msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
										msgZoomModalHeading : 'ตัวอย่างละเอียด',
										dropZoneTitle : 'รูปภาพ Thumbnail VDO',
										 xxx:'altthumbVideo',
										maxFileSize : 500 ,
										minImageWidth: 1920, // ขนาด กว้าง
																// ต่ำสุด
			                            minImageHeight: 1080, // ขนาด ศุง
																// ต่ำสุด
			                            maxImageWidth: 1920, // ขนาด กว้าง
																// ต่ำสุด
			                            maxImageHeight: 1080, // ขนาด ศุง
																// ต่ำสุด
										msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
										msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
										msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
										msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
									});

					$("#activityimg").fileinput({
										uploadUrl : "save_lhbanner.php", // server
										// upload
										// action
										maxFileCount : 1,
										allowedFileExtensions : [ "jpg", "png","jpeg", "gif" ],
										browseLabel : 'เลือกรูป',
										removeLabel : 'ลบ',
										browseClass : 'btn btn-success',
										showUpload : false,
										showRemove : false,
										showCaption : false,
										autoReplace: false,
										msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
										msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
										msgZoomModalHeading : 'ตัวอย่างละเอียด',
										dropZoneTitle : 'Banner Activity Image',
										 xxx:'altthumbActivity',
										maxFileSize : 500 ,
										minImageWidth: 1920, // ขนาด กว้าง
																// ต่ำสุด
			                            minImageHeight: 1080, // ขนาด ศุง
																// ต่ำสุด
			                            maxImageWidth: 1920, // ขนาด กว้าง
																// ต่ำสุด
			                            maxImageHeight: 1080, // ขนาด ศุง
																// ต่ำสุด
										msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
										msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
										msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
										msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
									});
                 //deleteMainId

				 });
// document ready

function addBannerLead_galery(data) {
  
	var maxId=$("input[name^='bannerLead']:last").attr('id');
	var index=maxId.indexOf("_")==-1?0:parseInt(maxId.substr(maxId.indexOf("_")+1));
	
    var maxIndex = $('.setIndexBannerLead').length;
    /**
    if(maxIndex > 3){
    	alert('จำกัดไม่เกิน 5 รูป');
    	return;
    }
    **/
    var form = `<div><div class="col-sm-4"><div class="form-group setIndexBannerLead">
            <div class="col-md-12">
        <input type="file" id="bannerLead_${index + 1}" class="gallery_master" name="bannerLead_${index + 1}" accept="image/*" />
        </div>
        </div>`
    	if (typeof (pagestate) != "undefined"
    		&& pagestate != null && pagestate == 'Edit') {
    		form +=`<button  type="button" class="btn btn-round btn-danger" style="margin-left: 112px;" onclick="removeEditProject_galery(this,'`+ data.banner_lead_id +`','`+data.lead_path+`','defaultbannerImg')"> - ลบ Banner</button>`;
    	}else{
    		form +=`<button  type="button" class="btn btn-round btn-danger" style="margin-left: 112px;" onclick="removeProject_galery(this)"> - ลบ Banner</button>`;
    		}
        form+= `</div></div>`;

    $('#bannerLeadGallery').append(form);
    
  if (typeof (pagestate) != "undefined"
		&& pagestate != null && pagestate == 'Add') {
      $("#bannerLead_"+(index + 1)).fileinput({
        uploadUrl: "save_lhbanner.php", // server upload action
        uploadAsync : true,
		maxFileCount : 1,
		showBrowse : true,
		browseOnZoneClick : false,
		allowedFileExtensions : ['jpg', 'png','gif','jpeg'],
		browseLabel : ' เพิ่ม Banner Default',
		browseIcon : "<i class=\"glyphicon glyphicon-picture\"></i> ",
		removeLabel : 'ลบ',
		browseClass : 'btn btn-success',
		showUpload : false,
		showRemove : false,
		showCaption : false,
		autoReplace: false,
		msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
		msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
		msgZoomModalHeading : 'ตัวอย่างละเอียด',
		dropZoneTitle : 'Banner Default',
	    xxx:'altLead',
		maxFileSize : 500 ,
		minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
        minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
        maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
        maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
		msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
		msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
		msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
		msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.'
      });
   }else if (typeof (pagestate) != "undefined"
		&& pagestate != null && pagestate == 'Edit') {
	    $("#bannerLead_"+(index + 1)).fileinput({
		    uploadUrl: "save_lhbanner.php", // server upload
		    initialPreview: [data.lead_path],
		    initialPreviewAsData: true, // defaults markup
		    initialPreviewFileType: 'image', // image is the default and can be overridden in config below
		    initialPreviewConfig: [
		    	{caption: data.filename, size:data.filesize ,url: "lhbanner_upload.php?action=delBannerLead&" +
	    			"bannerLeadId="+data.banner_lead_id+"&imgPath="+data.lead_path,key:data.banner_lead_id}
		    ],
		    maxFileCount: 1,
		    allowedFileExtensions : [ "jpg", "png","jpeg", "gif" ],
		    browseLabel: ' เพิ่ม Banner Default',
		    browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
			removeLabel : 'ลบ',
			browseClass : 'btn btn-success',
			showUpload : false,
			showRemove : false,
			showCaption : false,
			autoReplace: false,
			msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
			msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
			msgZoomModalHeading : 'ตัวอย่างละเอียด',
			 xxx:'altLead',
			dropZoneTitle : 'Banner Default',
			maxFileSize : 500 ,
			minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
           minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
           maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
           maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
			msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
			msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
			msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
			msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
		    
		});
	   
	   $("#bannerLead_"+(index + 1)).on("filepredelete", function(jqXHR) {
	   	    var abort = true;
	   	    if (confirm("คุณต้องการลบรูปนี้?")) {
	   	    	var index=getBannerIndexById(this.id);
	   	        abort = false;
	   	        delPicData('banner_lead_id', defaultbannerImg[index].banner_lead_id, defaultbannerImg ,this.id);
	   	    }
	   	    return abort; // you can also send any data/object that you can receive on `filecustomerror` event

	     }); 
   }

}

 function addBannerPC_galery(data) {
     
        var maxId=$("input[name^='bannerPc']:last").attr('id');
    	var index=maxId.indexOf("_")==-1?0:parseInt(maxId.substr(maxId.indexOf("_")+1));
    	
        var maxIndex = $('.setIndexBannerPC').length;
        /**
        if(maxIndex > 3){
        	alert('จำกัดไม่เกิน 5 รูป');
        	return;
        }
        **/
        var form = `<div><div class="col-sm-4"><div class="form-group setIndexBannerPC">
                <div class="col-md-12">
            <input type="file" id="bannerPc_${index + 1}" class="gallery_master" name="bannerPc_${index + 1}" accept="image/*" />
            </div>
           </div>`
    	if (typeof (pagestate) != "undefined"
    		&& pagestate != null && pagestate == 'Edit') {
    		form +=`<button  type="button" class="btn btn-round btn-danger" style="margin-left: 112px;" onclick="removeEditProject_galery(this,'`+ data.banner_lead_id +`','`+data.lead_path+`','bannerPC')"> - ลบ Banner</button>`;
    	}else{
    		form +=`<button  type="button" class="btn btn-round btn-danger" style="margin-left: 112px;" onclick="removeProject_galery(this)"> - ลบ Banner</button>`;
    		}
        form+= `</div></div>`;


        $('#Project_galery').append(form);

   if (typeof (pagestate) != "undefined"
    		&& pagestate != null && pagestate == 'Add') {
        $("#bannerPc_"+(index + 1)).fileinput({
            uploadUrl: "save_lhbanner.php", // server upload action
            uploadAsync : true,
			maxFileCount : 1,
			showBrowse : true,
			browseOnZoneClick : false,
			allowedFileExtensions : ['jpg', 'png','gif','jpeg'],
			browseLabel : ' เพิ่ม Banner PC',
			browseIcon : "<i class=\"glyphicon glyphicon-picture\"></i> ",
			removeLabel : 'ลบ',
			browseClass : 'btn btn-success',
			showUpload : false,
			showRemove : false,
			showCaption : false,
			autoReplace: false,
			msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
			msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
			msgZoomModalHeading : 'ตัวอย่างละเอียด',
			dropZoneTitle : 'Banner Lead',
		    xxx:'altPC',
			maxFileSize : 500 ,
			minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
            minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
            maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
            maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
			msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
			msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
			msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
			msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.'
          });
       }else if (typeof (pagestate) != "undefined"
   		&& pagestate != null && pagestate == 'Edit') {
    	   $("#bannerPc_"+(index + 1)).fileinput({
    		    uploadUrl: "save_lhbanner.php", // server upload
    		    initialPreview: [data.lead_path],	
    		    initialPreviewAsData: true, // defaults markup
    		    initialPreviewFileType: 'image', // image is the default and can be overridden in config below
    		    initialPreviewConfig: [
    		    	{caption: data.filename, size:data.filesize ,url: "lhbanner_upload.php?action=delBannerLead&" +
    		    			"bannerLeadId="+data.banner_lead_id+"&imgPath="+data.lead_path,key:data.banner_lead_id}
    		    ],
    		    maxFileCount: 1,
    		    allowedFileExtensions : [ "jpg", "png","jpeg", "gif" ],
    		    browseLabel: ' เพิ่ม Banner PC',
    		    browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
    			removeLabel : 'ลบ',
    			browseClass : 'btn btn-success',
    			showUpload : false,
    			showRemove : false,
    			showCaption : false,
    			autoReplace: false,
    			msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
    			msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
    			msgZoomModalHeading : 'ตัวอย่างละเอียด',
    			 xxx:'altLead',
    			dropZoneTitle : 'Banner Lead',
    			maxFileSize : 500 ,
    			minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
               minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
               maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
               maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
    			msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
    			msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
    			msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
    			msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
    		    
    		});
    	   
    	   $("#bannerPc_"+(index + 1)).on("filepredelete", function(jqXHR) {
		   	    var abort = true;
		   	    if (confirm("คุณต้องการลบรูปนี้?")) {
		   	    	var index=getBannerIndexById(this.id);
		   	        abort = false;
		   	        delPicData('banner_lead_id', bannerPC[index].banner_lead_id, bannerPC ,this.id);
		   	    }
		   	    return abort; // you can also send any data/object that you can receive on `filecustomerror` event
		     });
       }

    }
 
 function addBannerMB_galery(data) {
     
    var maxId=$("input[name^='bannerMb']:last").attr('id');
 	var index=maxId.indexOf("_")==-1?0:parseInt(maxId.substr(maxId.indexOf("_")+1));
 	
     var maxIndex = $('.setIndexBannerMB').length;
     /**
     if(maxIndex > 3){
     	alert('จำกัดไม่เกิน 5 รูป');
     	return;
     }
     **/
     var form = `<div><div class="col-sm-4"><div class="form-group setIndexBannerMB">
             <div class="col-md-12">
         <input type="file" id="bannerMb_${index + 1}" class="gallery_master" name="bannerMb_${index + 1}" accept="image/*" />
         </div>
         </div>`
    	if (typeof (pagestate) != "undefined"
    		&& pagestate != null && pagestate == 'Edit') {
    		form +=`<button  type="button" class="btn btn-round btn-danger" style="margin-left: 112px;" onclick="removeEditProject_galery(this,'`+ data.banner_lead_id +`','`+data.lead_path+`','bannerMB')"> - ลบ Banner MB</button>`;
    	}else{
    		form +=`<button  type="button" class="btn btn-round btn-danger" style="margin-left: 112px;" onclick="removeProject_galery(this)"> - ลบ Banner MB</button>`;
    		}
        form+= `</div></div>`;


     $('#bannerMb_galery').append(form);

  if (typeof (pagestate) != "undefined"
 		&& pagestate != null && pagestate == 'Add') {
     $("#bannerMb_"+(index + 1)).fileinput({
         uploadUrl: "save_lhbanner.php", // server upload action
           uploadAsync : true,
			maxFileCount : 1,
			showBrowse : true,
			browseOnZoneClick : false,
			allowedFileExtensions : ['jpg', 'png','gif','jpeg'],
			browseLabel : ' เพิ่ม Banner Mobile',
			browseIcon : "<i class=\"glyphicon glyphicon-picture\"></i> ",
			removeLabel : 'ลบ',
			browseClass : 'btn btn-success',
			showUpload : false,
			showRemove : false,
			showCaption : false,
			autoReplace: false,
			msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
			msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
			msgZoomModalHeading : 'ตัวอย่างละเอียด',
			dropZoneTitle : 'Banner Lead Mobile',
			xxx:'altMB',
			maxFileSize : 500 ,
			minImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
            minImageHeight: 1920, // ขนาด ศุง ต่ำสุด
            maxImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
            maxImageHeight: 1920, // ขนาด ศุง ต่ำสุด
			msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
			msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
			msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
			msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.'
        });
    }else if (typeof (pagestate) != "undefined"
 		    && pagestate != null && pagestate == 'Edit') {
	       $("#bannerMb_"+(index + 1)).fileinput({
	           uploadUrl: "save_lhbanner.php", // server upload action
	           initialPreview: [data.lead_path],	
   		       initialPreviewAsData: true, // defaults markup
   		       initialPreviewFileType: 'image', // image is the default and can be overridden in config below
   		       initialPreviewConfig: [
   		    	{caption: data.filename, size:data.filesize ,url: "lhbanner_upload.php?action=delBannerLead&" +
	    			"bannerLeadId="+data.banner_lead_id+"&imgPath="+data.lead_path,key:data.banner_lead_id}
   		        ],
				maxFileCount : 1,
				showBrowse : true,
				browseOnZoneClick : false,
				allowedFileExtensions : ['jpg', 'png','gif','jpeg'],
				browseLabel : ' เพิ่ม Banner MB',
				browseIcon : "<i class=\"glyphicon glyphicon-picture\"></i> ",
				removeLabel : 'ลบ',
				browseClass : 'btn btn-success',
				showUpload : false,
				showRemove : false,
				showCaption : false,
				autoReplace: false,
				msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
				msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
				msgZoomModalHeading : 'ตัวอย่างละเอียด',
				dropZoneTitle : 'Banner Lead Mobile',
				xxx:'altMB',
				maxFileSize : 500 ,
				minImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
	            minImageHeight: 1920, // ขนาด ศุง ต่ำสุด
	            maxImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
	            maxImageHeight: 1920, // ขนาด ศุง ต่ำสุด
				msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
				msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
				msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
				msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.'
	       });
	       
	       $("#bannerMb_"+(index + 1)).on("filepredelete", function(jqXHR) {
		   	    var abort = true;
		   	    if (confirm("คุณต้องการลบรูปนี้?")) {
		   	    	var index=getBannerIndexById(this.id);
		   	        abort = false;
		   	        delPicData('banner_lead_id', bannerMB[index].banner_lead_id, bannerMB ,this.id);
		   	    }
		   	    return abort; // you can also send any data/object that you can receive on `filecustomerror` event
		     });
	       
       }
    
 }

 function removeProject_galery(element)
 {
    $(element).parent().parent().remove();
 }
 
 function removeEditProject_galery(element,bId,picPath,arr)
 {
     if(bId=='undefined' || picPath=='undefined' || bId=='' || picPath=='' )
	   $(element).parent().parent().remove();
     else{
    	  var conf = confirm('คุณต้องการลบรูปนี้?');
		    if(conf) {
		        // do stuff here if OK was clicked
		    	$.ajax({ url: 'lhbanner_upload.php',
		                data: {action: 'delBannerLead' ,
		                	  bannerLeadId : bId,
		                	  imgPath : picPath
		                },  
		                type: 'get',
		                success: function(e) {
		                	     if(e){
		                	    	 removeJsonData('banner_lead_id', bId, arr);
		                	     }
		                         $(element).parent().parent().remove();
		                 }
		             });
		     }
       }
 }
 
 function initbannerImg(name,datas){
	if(name!='' && datas!=''){
        for(var i = 0; i < datas.length; i++) {
		   var data= datas[i];
		   //console.log(data.lead_path); 
		   var bname="";
		   var msgtext="";
		   if(i==0){
			   bname=name+"_"+i;
			   switch(name){
			   case "bannerLead" :{ msgtext="เพิ่ม Banner Default" };
			             break;
			   case "bannerPc" : { msgtext="เพิ่ม Banner PC" };
	                     break;
			   case "bannerMb" :  { msgtext="เพิ่ม Banner MB" };
	                     break;
			   }
		 if(name=='bannerMb') {
		       $("#"+bname).fileinput({
			    uploadUrl: "save_lhbanner.php", // server upload
			    // initialPreview: [data.lead_path],
			    // initialPreviewAsData: true, // defaults markup
			    // initialPreviewFileType: 'image', // image is the default and can be overridden in config below
			    // initialPreviewConfig: [
			    // 	{caption: data.filename, size:data.filesize ,url: "lhbanner_upload.php?action=delBannerLead&" +
		    	// 		"bannerLeadId="+data.banner_lead_id+"&imgPath="+data.lead_path,key:data.banner_lead_id }
			    // ],
			    maxFileCount: 1,
			    allowedFileExtensions : [ "jpg", "png","jpeg", "gif" ],
			    browseLabel: msgtext,
			    browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
				removeLabel : 'ลบ',
				browseClass : 'btn btn-success',
				showUpload : false,
				showRemove : false,
				showCaption : false,
				autoReplace: false,
				msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
				msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
				msgZoomModalHeading : 'ตัวอย่างละเอียด',
				dropZoneTitle : 'Banner Default',
				xxx:'altMB',
				maxFileSize : 500 ,
				minImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
	            minImageHeight: 1920, // ขนาด ศุง ต่ำสุด
	            maxImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
	            maxImageHeight: 1920, // ขนาด ศุง ต่ำสุด
				msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
				msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
				msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
				msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.'   
			});
		 }else{
			 
			 $("#"+bname).fileinput({
				    uploadUrl: "save_lhbanner.php", // server upload
				    // initialPreview: [data.lead_path],
				    // initialPreviewAsData: true, // defaults markup
				    // initialPreviewFileType: 'image', // image is the default and can be overridden in config below
				    // initialPreviewConfig: [
				    // 	{caption: data.filename, size:data.filesize ,url: "lhbanner_upload.php?action=delBannerLead&" +
			    	// 		"bannerLeadId="+data.banner_lead_id+"&imgPath="+data.lead_path,key:data.banner_lead_id }
				    // ],
				    maxFileCount: 1,
				    allowedFileExtensions : [ "jpg", "png","jpeg", "gif" ],
				    browseLabel: msgtext,
				    browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
					removeLabel : 'ลบ',
					browseClass : 'btn btn-success',
					showUpload : false,
					showRemove : false,
					showCaption : false,
					autoReplace: false,
					msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
					msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
					msgZoomModalHeading : 'ตัวอย่างละเอียด',
					 xxx:'altLead',
				     dropZoneTitle : 'Banner Default',
		    	     maxFileSize : 500 ,
		    		 minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
		             minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
		             maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
		             maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
		    	     msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
		    		 msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
		    		 msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
		    		 msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
			  });
					   
		 }
		
		    $("#"+bname).on("filepredelete", function(jqXHR) {
		    	    var abort = true;
		    	    if (confirm("คุณต้องการลบรูปนี้?")) {
		    	    	var index=getBannerIndexById(this.id);
		    	        abort = false;
		    	        switch(name){
		 			      case "bannerLead" :{ delPicData('banner_lead_id', defaultbannerImg[index].banner_lead_id, defaultbannerImg, this.id );  }
		 			             break;
		 			      case "bannerPc" : { delPicData('banner_lead_id', bannerPC[index].banner_lead_id, bannerPC, this.id ); }
		 	                     break;
		 			       case "bannerMb" :{ delPicData('banner_lead_id', bannerMB[index].banner_lead_id, bannerMB ,this.id ); }
		 	                     break;
		 			    }
		    	    }
		    	    return abort; // you can also send any data/object that you can receive on `filecustomerror` event
		    });
		   }else{
			   switch(name){
			   case "bannerLead" : addBannerLead_galery(data);
			             break;
			   case "bannerPc" :   addBannerPC_galery(data);
	                     break;
			   case "bannerMb" :   addBannerMB_galery(data);
	                     break;
			   }
		   }
		 
		}
	 }
 }
 
function intEditThumbnailVDO(data){
	   firstPicVDO=data.thumnail_path;
	   $("#thumbnailVDO").fileinput({
		    uploadUrl: "save_lhbanner.php", // server upload
		    initialPreview: [data.thumnail_path],	
		    initialPreviewAsData: true, // defaults markup
		    initialPreviewFileType: 'image', // image is the default and can be overridden in config below
		    initialPreviewConfig: [
		    	{caption: data.pic_vdo_name, size:data.pic_vdo_size ,url: "lhbanner_upload.php?action=delPicVDO&" +
	    			"bannerMainId="+data.banner_main_id+"&imgPath="+data.thumnail_path,key:data.banner_main_id}
		    ],
		    maxFileCount: 1,
		    allowedFileExtensions : [ "jpg", "png","jpeg", "gif" ],
		    browseLabel: ' เพิ่ม Banner PC',
		    browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
			removeLabel : 'ลบ',
			browseClass : 'btn btn-success',
			showUpload : false,
			showRemove : false,
			showCaption : false,
			msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
			msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
			msgZoomModalHeading : 'ตัวอย่างละเอียด',
			 xxx:'altLead',
			dropZoneTitle : 'Banner Default',
			maxFileSize : 500 ,
			minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
            minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
            maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
            maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
			msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
			msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
			msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
			msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
		    
		});
	    $("#thumbnailVDO").on("filepredelete", function(jqXHR) {
	   	    var abort = true;
	   	    if (confirm("คุณต้องการลบรูปนี้?")) {
	   	        abort = false;
	   	        firstPicVDO='';
	   	    }
	   	    return abort; // you can also send any data/object that you can receive on `filecustomerror` event
	       });
}

function initEditFileVDO(data){
	firstFileVDO=data.url_vdo;
	$("#vdofile")
	      .fileinput(
			   {
				uploadUrl : "save_lhbanner.php", // server
				initialPreview: [data.url_vdo],	
			    initialPreviewAsData: true, // defaults markup
			    initialPreviewFileType: 'image', // image is the default and can be overridden in config below
			    initialPreviewConfig: [
			    	{caption: data.vdo_name, size:data.vdo_size ,filetype: "video/mp4",
			    		 type: "video",url: "lhbanner_upload.php?action=delVDOFile&" +
			            "bannerMainId="+data.banner_main_id+"&imgPath="+data.url_vdo,key:data.banner_main_id}
			    ],
				maxFileCount : 1,
				allowedFileExtensions : [ "mp4" ],
				browseLabel : 'เลือกไฟล์',
				removeLabel : 'ลบ',
				browseClass : 'btn btn-success',
				showUpload : false,
				showRemove : false,
				showCaption : false,
				maxFileSize : 204800 , // 200 MB
				msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
				msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
				msgZoomModalHeading : 'ตัวอย่างละเอียด',
				 xxx:'altVideo',
				dropZoneTitle : 'File VDO',
				msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
				msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
				msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
				msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
			});
	       $("#vdofile").on("filepredelete", function(jqXHR) {
               var abort = false;
               $.confirm({
                   title: 'ลบ VDO เรียบร้อย !',
                   content: 'กรูณา เพิ่ม VDO ด้วย',
                   buttons: {
                       confirm: {
                           text: 'OK',
                           btnClass: 'btn-danger',
                           keys: ['enter', 'shift'],
                           action: function(){
                               abort = false;
                               firstPicActivity='';

                           }
                       },
                   }
               });
	   	    return abort; // you can also send any data/object that you can receive on `filecustomerror` event
	       });

}

function intEditThumbnailYoutube(data){
	   firstPicYoutube=data.thumnail_path;
	   $("#thumbnailYoutube").fileinput({
		    uploadUrl: "save_lhbanner.php", // server upload
		    initialPreview: [data.thumnail_path],	
		    initialPreviewAsData: true, // defaults markup
		    initialPreviewFileType: 'image', // image is the default and can be overridden in config below
		    initialPreviewConfig: [
		    	{caption: data.pic_vdo_name, size:data.pic_vdo_size ,url: "lhbanner_upload.php?action=delPicYoutube&" +
	    			"bannerMainId="+data.banner_main_id+"&imgPath="+data.thumnail_path,key:data.banner_main_id}
		    ],
		    maxFileCount: 1,
		    allowedFileExtensions : [ "jpg", "png","jpeg", "gif" ],
		    browseLabel: ' เพิ่ม Banner PC',
		    browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
			removeLabel : 'ลบ',
			browseClass : 'btn btn-success',
			showUpload : false,
			showRemove : false,
			showCaption : false,
			msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
			msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
			msgZoomModalHeading : 'ตัวอย่างละเอียด',
			 xxx:'altLead',
			dropZoneTitle : 'Banner Default',
			maxFileSize : 500 ,
			minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
         minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
         maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
         maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
			msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
			msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
			msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
			msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
		    
		});
	    $("#thumbnailYoutube").on("filepredelete", function(jqXHR) {
   	    var abort = false;
            $.confirm({
                title: 'ลบรูปเรียบร้อย !',
                content: 'กรูณาเพิ่มรูป Thumbnail Youtube ด้วย',
                buttons: {
                    confirm: {
                        text: 'OK',
                        btnClass: 'btn-danger',
                        keys: ['enter', 'shift'],
                        action: function(){
                            abort = false;
                            firstPicActivity='';

                        }
                    },
                }
            });

   	    return abort; // you can also send any data/object that you can receive on `filecustomerror` event
       });
}

function intEditActivityImg(data){
	        firstPicActivity=data.pic_activity;
	       $("#activityimg").fileinput({
		    uploadUrl: "save_lhbanner.php", // server upload
		    initialPreview: [data.pic_activity],	
		    initialPreviewAsData: true, // defaults markup
		    initialPreviewFileType: 'image', // image is the default and can be overridden in config below
		    initialPreviewConfig: [
		    	{caption: data.pic_activity_name, size:data.pic_activity_size ,url: "lhbanner_upload.php?action=delPicActivity&" +
	    			"bannerMainId="+data.banner_main_id+"&imgPath="+data.pic_activity,key:data.banner_main_id}
		    ],
		    maxFileCount: 1,
		    allowedFileExtensions : [ "jpg", "png","jpeg", "gif" ],
		    browseLabel: ' เพิ่ม Banner PC',
		    browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
			removeLabel : 'ลบ',
			browseClass : 'btn btn-success',
			showUpload : false,
			showRemove : false,
			showCaption : false,
			msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
			msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
			msgZoomModalHeading : 'ตัวอย่างละเอียด',
			 xxx:'altLead',
			dropZoneTitle : 'Activity',
			maxFileSize : 500 ,
			minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
            minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
            maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
            maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
			msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
			msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
			msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
			msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
		    
		});
	    $("#activityimg").on("filepredelete", function(jqXHR) {
	    	    var abort = false;
	    	    // if (confirm("คุณต้องการลบรูปนี้?")) {
	    	    //     abort = false;
	    	    //     firstPicActivity='';
	    	    // }

            $.confirm({
                title: 'ลบรูปเรียบร้อย !',
                content: 'กรูณาเพิ่มรูป Banner Activity ด้วย',
                buttons: {
                    confirm: {
                        text: 'OK',
                        btnClass: 'btn-danger',
                        keys: ['enter', 'shift'],
                        action: function(){
                            abort = false;
							firstPicActivity='';

                        }
                    },
                    // cancel: function () {
                    //     //return abort;
                    // },
                }
            });

	    	    return abort; // you can also send any data/object that you can receive on `filecustomerror` event
	  });
}

function removeJsonData(property, bannerLeadId, arr) {

	if(typeof arr === 'string'){
		switch(arr){
		   case "defaultbannerImg" : arr=defaultbannerImg;
		             break;
		   case "bannerPC" :  arr=bannerPC;
                  break;
		   case "bannerMB" : arr=bannerMB;
                  break;
		   }
	}
    for (var i in arr) {
        if (arr[i][property] == bannerLeadId){
            arr.splice(i, 1);
        }
    }
    //set old value to editForm
    $('input[name="oldBannerImg"]').val(JSON.stringify(defaultbannerImg));
    $('input[name="oldBannerPC"]').val(JSON.stringify(bannerPC));
    $('input[name="oldBannerMB"]').val(JSON.stringify(bannerMB));
 }

function delPicData(property, bannerLeadId, arr ,indexId) {

    for (var i in arr) {
        if (arr[i][property] == bannerLeadId){
        	arr[i]={ position : indexId , banner_lead_id:bannerLeadId ,lead_path :'' };
        }
    }
    //set old value to editForm
    $('input[name="oldBannerImg"]').val(JSON.stringify(defaultbannerImg));
    $('input[name="oldBannerPC"]').val(JSON.stringify(bannerPC));
    $('input[name="oldBannerMB"]').val(JSON.stringify(bannerMB));
 }


function getBannerIndexById(id){
	var n = id.search("_");
	return  id.substring(n+1);
}

//

function deleteBanner_d(id){
    $.confirm({
        title: 'ลบรูปนี้ !',
        content: 'คุณแน่ใจที่จะลบ รูป นี้!',
        buttons : {
            Yes: {
                text: 'Yes',
                btnClass: 'btn-red any-other-class', // multiple classes.
                action: function () {
                    var self = this;
                    return $.ajax({
                        url: 'ajax_deleteBanner_d.php?id=' + id,
                        dataType: 'json',
                        method: 'get'
                    }).done(function () {
                        //$.alert('Confirmed!');
                        location.reload();
                    }).fail(function () {
                        self.setContent('Something went wrong.');
                    });
                }
            },
            No: function () {
                //$.alert('ยกลเิก!');
            },
        }
    });

}




 
 
 
 
 
 
 
 
 
	