$(document).ready(function() {
            //$.validator.setDefaults({ ignore: "[contenteditable='true']" });  
            
            $("input[name='email']").keypress(function(event){

                if (event.keyCode === 64 ) 
                     event.preventDefault();

            });
            $("#form-project").validate({
                rules: {
                    project_name_th: "required",
                    project_name_en: "required",
                    brand_id : "required",
                    zone_id : "required",
                    group_id  : "required",
                    province_id : "required",
                    'preview_page[]': "required",
                    'product_id[]' : "required",
                    title_th : "required",
                    desc_th : "required",
                    keyword_th : "required",
                    detailband : "required",
                    'detailZone_id[]' : "required",
                    company_no_DL200 : "required",
                    //area_land : "required",
                    open_time_start : "required",
                    open_time_end : "required",
                    email : "required",
                    telephone : "required",
                    telephone_line : "required",
                    name_staff : "required",
                    concept_th :  {
                        //required: true,
                        maxlength: 200
                    },
                    concept_en : {
                        required: false,
                        maxlength: 200
                    },
                    'lead_image[]' : "required",
                    company_no_DL200 : "required",
                    project_no_DL200 : "required",
                    /*area_land : "required",
                    area_farm : "required",
                    'homearea_m[]' : "required",
                    'homearea_w[]' : "required",
                    'num_plan_home[]': "required",*/
                    email : "required",
                    //location_th : "required",
                    //paking_th : "required",
                    'close-day[]' : {
                        required: "#open-day-sum:checked"
                    },
                    /*value_pire_mod_begine: {
                        required: "#ch2:checked"
                    },
                    value_pire_mod_end : {
                        required: "#ch2:checked"
                    },
                    distinctive_th : "required",
                    'value_pire_mod_condo[]' : {
                        required : ".v_price_condo:checked"
                    },*/
                    project_keyword_seo_th : "required",
                    project_title_seo_th : "required",
                    project_des_seo_th : "required",
                    //'flm[]' :  "required",
                    'lead_img_banner[]' :{
                        required: function(){
                            if($('#Banner_check').is(':checked')){
                                if($('#r_lead ').is(':checked')){
                                    if($('#banner_imade_sum').val() > 0 ){
                                       return false;
                                   }else{
                                       return true;   
                                   }    
                               }else{
                                return false;
                            }
                        } else {
                            return false;
                        }
                    },
                },
                date_start : {
                    required: function(){
                        if($('#Banner_check').is(':checked')){     
                            return true;  
                        } else {
                            return false;
                        }
                    },
                },
                date_end: {
                    required: function(){
                        if($('#Banner_check').is(':checked')){     
                            return true;  
                        } else {
                            return false;
                        }
                    },
                },
                favcolor : {
                    required: function(){
                        if($('#Banner_check').is(':checked')){
                            if($('#r_banner ').is(':checked')){
                                return true;
                            }else{
                                return false;
                            }
                        } else {
                            return false;
                        }
                    },
                },
                banner_image : {
                    required: function(){
                        if($('#Banner_check').is(':checked')){
                            if($('#r_banner ').is(':checked')){
                                return true;
                            }else{
                                return false;
                            }
                        } else {
                            return false;
                        }
                    },
                },
                img_youtube_banner :{
                    required: function(){
                        if($('#Banner_check').is(':checked')){
                            if($('#rdo_youtube  ').is(':checked')){
                                return true;
                            }else{
                                return false;
                            }
                        } else {
                            return false;
                        }
                    },
                },
                url_youtube2 : {
                    required: function(){
                        if($('#Banner_check').is(':checked')){
                            if($('#rdo_youtube  ').is(':checked')){
                                return true;
                            }else{
                                return false;
                            }
                        } else {
                            return false;
                        }
                    },
                },
                vdo_img :{
                 required: function(){
                    if($('#Banner_check').is(':checked')){
                        if($('#rdo_vdo  ').is(':checked')){
                            return true;
                        }else{
                            return false;
                        }
                    } else {
                        return false;
                    }
                },
            },
            vdo_file:{
                required: function(){
                    if($('#Banner_check').is(':checked')){
                        if($('#rdo_vdo  ').is(':checked')){
                            return true;
                        }else{
                            return false;
                        }
                    } else {
                        return false;
                    }
                },
            },
        },
        messages: {
            project_name_th: "กรุณากรอกชื่อ โครงการ จีน !",
            project_name_en : "กรุณากรอกชื่อ โครงการ อังกฤษ !",
            brand_id : "กรุณาเลือก Brand !",
            zone_id : "กรุณาเลือก Zone !",
            group_id  : "กรุณาเลือก กลุ่ม !",
            'product_id[]' : "เลือกอย่างน้อย 1 รายการ !",
            province_id : "กรุณาเลือกจังหวัด",
            title_th : "กรุณากรอก Title seo จีน !",
            desc_th : "กรุณากรอก Description จีน !",
            keyword_th : "กรุณากรอก keyword จีน !",
            series: "กรุณากรอกเลือก series!",
            plan_code : "ระบุ รหัสแบบบ้าน !",
            plan_name_th : " กรุณากรอกชื่อ แบบ้าน จีน !",
            plan_name_en : "กรุณากรอกชื่อ แบบ้าน อังกฤษ !",
            plan_useful : " กรุณากรอก พื้นที่ใช้สอย !",
            images_plan : " &nbsp; กรุณาเลือกไฟล์ !",
            'preview_page[]' : "เลือกอย่างน้อย 1 รายการ !",
            open_time_start : "ระบุ !",
            open_time_end : "ระบุ !",
            telephone : "ระบุเบอร์โทรศัพท์ !",

            name_staff : "ระบุชื่อ-นามสกุลเจ้าหน้าที่ !",
            concept_th : {
                required:  "กรุณากรอก แนวคิดโครงการ (จีน) !",
                maxlength:  "กรุณากรอก ไม่เกิน 200 ตัวอักษร !",

            },
            concept_en : {

                maxlength:  "กรุณากรอก ไม่เกิน 200 ตัวอักษร !",

            },
            //distinctive_th : "กรุณากรอก จุดเด่นโครงการ (ไทย) !",
            map_image_logo : "เลือกโลโก้ !",
            company_no_DL200 : "กรุณากรอก รหัสบริษัท(DL200) !",
            project_no_DL200 : "กรุณากรอก รหัสโครงการ(DL200) !",
            /*area_land : {
                required : "กรุณากรอกพื้นที่โครงการ !",
                min : "กรุณาระบุ ค่ามากกว่า 0 !",
            },
            area_farm : "ระบุ",
            'homearea_m[]' : {
                required: "ระบุ พื้นที่ใช้สอยตั้งแต่ !",
                min : "กรุณาระบุ ค่ามากกว่า 0 !",
            },
            'homearea_w[]' : {
                required: "ระบุ ขนาดที่ดิน !",
                min : "กรุณาระบุ ค่ามากกว่า 0 !",
            },
            'num_plan_home[]': {
                required: "ระบุ จำนวนแบบบ้าน !",
                min : "กรุณาระบุ ค่ามากกว่า 0 !",
            },*/
            email : "ระบุ email !",
            telephone_line : "ระบุ เบอร์โทร !",
            //location_th : "กรุณากรอก ทำเลที่ตั้ง !",
            //paking_th : "กรุณากรอก ที่จอดรถ !",
            /*'value_pire_mod[]' : "ระบุ ราคา  ",
            value_pire_mod_begine :  "ระบุ ราคาเริ่มต้น !",
            value_pire_mod_end :  "ระบุ ราคาสิ้นสุด !",*/
            'lead_image[]':'กรุณาเลือกรูป !',
            prepay : "กรุณาระบุ !",
            'close-day[]' : "เลือกอย่างน้อย 1 รายการ !",
            //'value_pire_mod_condo[]' : 'ระบุ ราคา',
            project_keyword_seo_th : "กรุณากรอก keyword จีน !",
            project_title_seo_th : "ระบุ Title seo !",
            project_des_seo_th : "ระบุ Description !",
            project_keyword_seo_th : "ระบุ keyword !",
            'building_num_layer[]' : {
                min : "1 ขึ้นไป !",
            },
            'building_unit[]' : {
                min : "1 ขึ้นไป !",
            },
            num_plan_condo : {
                min : "กรุณาระบุ ค่ามากกว่า 0 !",
            },
            building_uni : {
                min : "กรุณาระบุ ค่ามากกว่า 0 !",
            },
            area_condofrom : {
                min : "กรุณาระบุ ค่ามากกว่า 0 !",
            },
            area_condoto : {
                min : "กรุณาระบุ ค่ามากกว่า 0 !",
            },
            'progress_update_mount_add[]' : {
                min : "1 ขึ้นไป !",
                max : "ไม่เกิน 100 !",
            },
            'flm[]' : "กรุณาเลือก Zone FLM",
            'lead_img_banner[]' : "กรุณาเลือกรูป !",
            date_start :"กรุณาระบุวันที่เริ่มต้น !",
            date_end : "กรุณาระบุวันที่สิ้นสุด !",
            favcolor : "กรุณาเลือกสีพื้นหลัง",
            banner_image : "กรุณาเลือกรูป",
            img_youtube_banner : "กรุณาเลือกรูป",
            url_youtube2 : "กรุณาระบุ URL Youtube",
            vdo_img :"กรุณาเลือกรูป",
            vdo_file : "กรุณาเลือกไฟล์ VDO",
            'detailZone_id[]' : "กรุณาเลือก Zone", 

                }
            });


$( "#myform" ).validate({
    rules: {
        field: {
            required: true,
            min: 13
        }
    }
});


/*$("#price1").rules("add", {
    required:"#ch1:checked",
    messages: {
        required: "กรุณาระบุ ราคา !",
        min : "กรุณาระบุ ค่ามากกว่า 0 !",
    }
});*/

$( "#project_title_seo_th" ).rules( "add", {
    required: true,
    maxlength: 160,
    messages: {
        required: "กรุณากรอก Title seo จีน !",
        maxlength: jQuery.validator.format("กรอกตัวอักษรได้ไม่เกิน 160 ตัว !")
    }
});

$( "#project_des_seo_th" ).rules( "add", {
    required: true,
    maxlength: 300,
    messages: {
        required: "กรุณากรอก Description จีน !",
        maxlength: jQuery.validator.format("กรอกตัวอักษรได้ไม่เกิน 300 ตัว !")
    }
});


});

 $(document).on('change', "select.flm ", function() {
     $('#error-checkbox-flm').html("");
 });

 $(document).on('change', "select.zone ", function() {
     $('#error-checkbox-zone').html("");
 });



