﻿<?php
session_start();
include './include/dbCon_mssql.php';
include "include/function_date.php";
$group_id=$_SESSION['group_id'];

$_GET['page']='managment';


if (isset($_GET['add'])) {
           if($_GET['add'] ==1){
             //header("location:product_add.php");
            $success ="success";
          } else if($_GET['add'] ==0){

            $error="error"; //ค่าซ้ำ

          } else if($_GET['add'] ==-1){
            $success_d="success_d";
          }else if($_GET['add'] ==-2){
            $error_d="error_d";
          }elseif ($_GET['add'] == -3) {
            $update="update";
          }
      }
 if(isset($_SESSION['add'])) {
     if ($_SESSION['add'] == -1) {
         $success_d = 'delete';
     }
     unset($_SESSION["add"]);
 }


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>
      <!-- Bootstrap -->
      <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome -->
      <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <!-- NProgress -->
      <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
      <!-- iCheck -->
      <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
      <!-- Datatables -->
      <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

      <!-- Fancybox image popup -->
      <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />

      <!-- Custom Theme Style -->
      <link href="../build/css/custom.min.css" rel="stylesheet">

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <?php include './master/navbar.php' ?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
          <?php include './master/top_nav.php'; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                    <a href="managment_web.php"><h2>ระบบจัดการเว็บไซต์</h2></a>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <?php if(isset($success)){?>
                   <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>สร้างข้อมูลเรียบร้อยแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-6"></div>
                   </div>
                  <?php }else if(isset($error)){?>
                    <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>สร้างข้อมูลเรียบร้อยแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                  <?php }else if(isset($success_d)){?>
                    <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>ลบข้อมูลเรียบร้อยแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-6"></div>
                   </div>
                  <?php }else if(isset($error_d)){?>
                  <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>มีชื่อนี้อยู่ในระบบแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                  <?php }else if(isset($update)){?>
                  <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>แก้ไขข้อมูลเรียบร้อย</strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                  <?php }?>

                  <div class="" role="tabpanel" data-example-id="togglable-tabs">
                  <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <!-- <li role="presentation" class="active"><a href="#tab_content1" id="Highlights" role="tab" data-toggle="tab" aria-expanded="true">Highlights</a>
                        </li> -->
                        <li role="presentation" class="active"><a href="#tab_content1" role="tab" id="Project" data-toggle="tab" aria-expanded="false">Project review</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content2" id="Community"  data-toggle="tab" aria-expanded="true">Community</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content3" role="tab" id="Living" data-toggle="tab" aria-expanded="false">Living tips</a>
                        </li>
                      </ul>
                  <div id="myTabContent" class="tab-content">


                  <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="Project">
                  <div style="margin-top:35px;margin-bottom:20px;">
                  <a class="fancybox" href="images/project-revie-grid.jpg">
                    <h2> รูปแบบการจัดวางตำแหน่ง (คลิกเพื่อดูรูปใหญ่) กรุณากำหนดลำดับภาพ 1 - 6 รูป เพื่อแสดง หน้า Home</h2>
                    <img alt="" style="width: 150px" src="images/project-revie-grid.jpg">
                  </a>
                  </div>
                        <div class="form-group">
                        <div class="col-md-9" >

                        </div>
                        <div class="col-md-2" >
                          <a href="project_review_add.php"><button type="button" class="btn btn-success" >สร้างข้อมูล Project Review +</button></a>
                        </div>
                      </div>
                    <table id="Project_tb" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th><center>ชื่อ Project Review</center></th>
                          <th ><center>รายละเอียด</center></th>
                            <th ><center>วันที่เผยแพร่</center></th>
                          <th ><center>วันที่แก้ไขล่าสุด</center></th>
                          <th ><center>ลำดับรูปภาพ</center></th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                        $sql_pr="SELECT * FROM LH_PROJECT_REVIEW ORDER BY order_seq  ASC";
                        $query_pr = mssql_query($sql_pr);
                        while ( $row_pr = mssql_fetch_array($query_pr)) {
                            $date3=date_create($row_pr['project_update']);
                            $strDate_pr =date_format($date3,"Y-m-d H:i:s");
                            $date4=date_create($row_pr['update_date']);
                            $strDate_pr2 =date_format($date4,"Y-m-d H:i:s");
                      ?>
                        <tr>
                          <td>
                              <a href="project_review_update.php?id=<?=$row_pr['project_review_id']?>">
                                  <?php
                                  $str =strlen($row_pr['project_review_name_th']);
                                  if($str >= 66){
                                      echo iconv_substr($row_pr['project_review_name_th'], 0,66, "UTF-8")."...";
                                  }else{
                                      echo $row_pr['project_review_name_th'];
                                  }
                          ?>
                              </a></td>
                          <td><a href="project_review_update.php?id=<?=$row_pr['project_review_id']?>">
                                  <?php
                                  $str =strlen($row_pr['project_review_dis_th']);
                                  if($str >= 50){
                                     echo iconv_substr($row_pr['project_review_dis_th'], 0,50, "UTF-8")."...";
                                  }else{
                                      echo $row_pr['project_review_dis_th'];
                                  }
                                  ?>
                              </a></td>
                            <td><p style="display: none"><?=$strDate_pr?></p>
                                <center><a href="project_review_update.php?id=<?=$row_pr['project_review_id']?>"><?=DateThai($row_pr['project_update']);?></a></center></td>
                          <td><p style="display: none"><?=$strDate_pr2?></p>
                              <center><a href="project_review_update.php?id=<?=$row_pr['project_review_id']?>"><?=DateThai_time($row_pr['update_date']);?></a></center></td>
                              <td>
                              <center><a href="project_review_update.php?id=<?=$row_pr['project_review_id']?>">
                                        <?php
                                          if($row_pr['order_seq']==''){
                                            echo "<span style='display:none;'>BLANK</span>";
                                          }else{
                                            echo $row_pr['order_seq'];
                                          }
                                        ?>
                                      </a></center>
                              </td>
                        </tr>
                      <?php  }  ?>
                      </tbody>
                    </table>
                    <br><br>
                    </div>


                    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="Community">
                      <div class="form-group">
                        <div class="col-md-9" >

                        </div>
                        <div class="col-md-2" >
                          <a href="community_add.php"><button type="button" class="btn btn-success" >สร้างข้อมูล Community +</button></a>
                        </div>
                      </div>
                      <table id="Community_tb" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th><center>ชื่อ Community</center></th>
                          <th ><center>วันที่แก้ไขล่าสุด</center></th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                        $sql_cm="SELECT * FROM LH_COMMUNITY_PAGE";
                        $query_cm = mssql_query($sql_cm);
                        while ( $row_cm = mssql_fetch_array($query_cm)) {
                            $date3=date_create($row_cm['community_update']);
                            $strDate_cm =date_format($date3,"Y-m-d H:i:s");

                      ?>
                        <tr>
                          <td>
                              <a href="community_update.php?id=<?=$row_cm['community_id']?>">
                                  <?php
                                  $str =strlen($row_cm['community_name_th']);
                                  if($str >= 100){
                                      echo iconv_substr($row_cm['community_name_th'], 0,100, "UTF-8")."...";
                                  }else{
                                      echo $row_cm['community_name_th'];
                                  }
                                  //$row_cm['community_name_th']." (".$row_cm['community_name_en'].")";
                                  ?>
                              </a></td>
                          <td><p style="display: none"><?=$strDate_cm?></p>
                              <center><a href="community_update.php?id=<?=$row_cm['community_id']?>"><?=DateThai_time($row_cm['community_update']);?></a></center></td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table><br><br>
                    </div>

                     <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="Living">
                        <div class="form-group">
                        <div class="col-md-9" >

                        </div>
                        <div class="col-md-2" >
                          <a href="living_tips_add.php"><button type="button" class="btn btn-success" >สร้างข้อมูล Living Tip +</button></a>
                        </div>
                      </div>
                      <table id="Living_tb" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                           <th><center>ชื่อ Living Tip</center></th>
                          <th><center>ประเภท</center></th>
                          <th><center>วันที่เผยแพร่</center></th>
                            <th><center>วันที่แก้ไขล่าสุด</center></th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                        $sql_lt="SELECT * FROM LH_LIVING_TIPS";
                        $query_lt = mssql_query($sql_lt);
                        while ( $row_lt = mssql_fetch_array($query_lt)) {

                            $date3=date_create($row_lt['update_date']);
                            $strDate_lt =date_format($date3,"Y-m-d H:i:s");

                            $date4=date_create($row_lt['living_tip_post_date']);
                            $strDate_lt4 =date_format($date4,"Y-m-d H:i:s");


                      ?>
                        <tr>
                          <td><a href="living_tips_update.php?id=<?=$row_lt['living_tip_id']?>">
                                  <?php
                                  $row_lt['living_tip_name_th'];
                                  if($str >= 50){
                                      echo iconv_substr($row_lt['living_tip_name_th'], 0,50, "UTF-8")."...";
                                  }else{
                                      echo $row_lt['living_tip_name_th'];
                                  }
                                  ?>
                              </a></td>
                          <td><center><a href="living_tips_update.php?id=<?=$row_lt['living_tip_id']?>"><?=$row_lt['living_tip_category'];?></a></center></td>
                          <td><p style="display: none"><?=$strDate_lt4;?></p>
                              <center><a href="living_tips_update.php?id=<?=$row_lt['living_tip_id']?>"><?=DateThai($row_lt['living_tip_post_date']);?></a></center></td>
                            <td><p style="display: none"><?=$strDate_lt;?></p>
                                <center><a href="living_tips_update.php?id=<?=$row_lt['living_tip_id']?>"><?=DateThai_time($row_lt['update_date']);?></a></center></td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table><br><br>
                     </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
                  <br>
                   <br>
                   <br>
                   <br>
                   <br>
                   <br>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <script src="../build/js/custom.min.js"></script>
     <!-- Datatables -->
    <script>
      $(document).ready(function() {
        $("a.fancybox").fancybox();
        $('#Project_tb').dataTable({
          "bLengthChange": false,
          "pageLength": 50,
          "order": [[ 4, "asc" ],[ 3, "desc" ]],
        });
      });
    </script>
    <!-- /Datatables -->

    <!-- Datatables -->
    <script>
      $(document).ready(function() {
        $('#Community_tb').dataTable({
          "bLengthChange": false,
          "pageLength": 50,
          "order": [[ 1, "desc" ]]
        });
      });
    </script>
    <!-- /Datatables -->

     <!-- Datatables -->
    <script>
      $(document).ready(function() {
        $('#Living_tb').dataTable({
          "bLengthChange": false,
          "pageLength": 50,
          "order": [[ 3, "desc" ],[ 2, "desc" ]]
        });
      });
    </script>
    <!-- /Datatables -->
     <?php
          if(!empty($_GET['tab'])){
            if($_GET['tab']=="Highlights"){
      ?>
          <script type="text/javascript">
            $('.nav-tabs a[href="#tab_content1"]').tab('show')
        </script>
      <?php
          }else if($_GET['tab']=="Project"){
      ?>
           <script type="text/javascript">
            $('.nav-tabs a[href="#tab_content1"]').tab('show')
        </script>
      <?php
        }else if($_GET['tab']=="Community"){
      ?>
         <script type="text/javascript">
            $('.nav-tabs a[href="#tab_content2"]').tab('show')
        </script>
      <?php
         }else if($_GET['tab']=="Living"){
      ?>
        <script type="text/javascript">
            $('.nav-tabs a[href="#tab_content3"]').tab('show')
        </script>

    <?php }} ?>

    <script>
        $(document).ready(function(){
            $("#alert").show();
            $("#alert").fadeTo(3000, 500).slideUp(500, function(){
                $("#alert").alert('close');
            });
        });
    </script>
  </body>
</html>