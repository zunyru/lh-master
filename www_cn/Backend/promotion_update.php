<?php
session_start();
include './include/dbCon_mssql.php';
$group_id = $_SESSION['group_id'];
date_default_timezone_set('Asia/Bangkok');
$time = date("H:i:s");

$_GET['page'] = 'promotion';
$id = $_GET['id'];
if (isset($_SESSION['add'])) {
	if ($_SESSION['add'] == 1) {
		//header("location:product_add.php");
		$success = "success";
		$_SESSION['add'] = '';
	} else if ($_SESSION['add'] == 2) {

		$error_name = "error_name"; //ค่าซ้ำ
		$_SESSION['add'] = '';

	} else if ($_SESSION['add'] == -1) {
		$errors_date = "errors_date";
		$_SESSION['add'] = '';
	} else if ($_SESSION['add'] == -2) {
		$error_img = "error_img";
		$_SESSION['add'] = '';
	} else if ($_SESSION['add'] == -3) {
		$error_up = "error_up";
		$_SESSION['add'] = '';
	}
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>
      <!-- Bootstrap -->
      <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome -->
      <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <!-- NProgress -->
      <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
      <!-- iCheck -->
      <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
      <!-- Custom Theme Style -->
      <link href="../build/css/custom.min.css" rel="stylesheet">
      <!-- Fancybox image popup -->
      <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
      <!-- bootstrap-progressbar -->
      <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
      <!-- jQuery -->
      <script src="../vendors/jquery/dist/jquery.min.js"></script>

      <script src="../build/js/custom.min.js"></script>      <script src="../build/js/custom.min.js"></script>
      <!-- uploade img -->
      <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
      <!--uploade-->
      <!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
      <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
      <!-- confirm-->
      <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
      <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>

    <style type="text/css">

      th.next.available{
        background-color: #6f9755;
      }
      th.prev.available{
        background-color: #6f9755;
      }
    </style>
    <style>
          .file-caption-main .btn-file {
              overflow: visible;
          }

          .file-caption-main .btn-file .error {
              position: absolute;
              bottom: -32px;
              right: 30px;
          }
          .form-control[readonly] { /* For Firefox */
              background-color: white;
          }

          .form-control[readonly] {
              background-color: white;
          }
      </style>



  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <?php include './master/navbar.php';?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
          <?php include './master/top_nav.php';?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><a href="promotion.php">ระบบจัดการข้อมูลโปรโมชั่น : </a>  <?=$row['promotion_name_th'];?></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <p class="font-gray-dark">
                  </p><br>
                  <?php if (isset($success)) {?>
                   <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>แก้ไขข้อมูลเรียบร้อยแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-6"></div>
                   </div>
                  <?php } else if (isset($error_name)) {?>
                    <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>มีชื่อนี้อยู่ในระบบแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                   <?php } else if (isset($errors_date)) {?>

                  <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                         <strong>ไม่สามารถสร้างข้อมูลได้ !</strong> วันที่ดำเนินการ ทับซ้อนกัน
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                  <?php } else if (isset($error_img)) {?>
                  <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>คุณยังไม่ได้เลือกรูปในโครงการ !</strong> กรุณาเลือกรูปโปรโมชัน
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                   <?php } else if (isset($error_up)) {?>
                   <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>เกิดข้อผิดพลาดระหว่างบันทึกข้อมูล !</strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                   <?php }?>
                   <?php
$sql = "SELECT * FROM LH_PROMOTION pr LEFT JOIN LH_IMG_PROMOTION img ON pr.promotion_id = img.promotion_id LEFT join LH_PROJECTS j ON pr.project_id = j.project_id  WHERE pr.promotion_id = '$id'";
$query_result3 = mssql_query($sql, $db_conn);
$row = mssql_fetch_array($query_result3);
//                      if($_SESSION['group_account_name'] == 'Admin'){
//                        $read_only = "readonly";
//                      }else{
//                        $read_only = "";
//                      }
$read_only = "";

?>
                   <?php if (($row['promotion_approve'] == 'Y') && ($_SESSION['group_account_name'] != 'Admin')) {?>
                    <div class="form-group">
                        <label class="control-label col-md-2" for="first-name">
                        </label>
                        <div class="col-md-4">
                            <div class="alert alert-success">
                                <center>
                                    <strong>Approve !</strong>
                                </center>
                            </div>
                            <label style="color: red">หากต้องการแก้ไขข้อมูลกรุณาแจ้ง Admin</label>
                        </div>
                    </div>
                </div>

                  <?php }?>

                  <form class="form-horizontal form-label-left" action="update_promotion.php" method="post" id="commentForm">
                    <div class="form-group">
                      <label class="control-label col-md-2" for="first-name">ชื่อโปรโมชั่น (จีน)<span class="required">*</span></label>
                      <div class="col-md-6">
                        <input type="text" id="first-name2" class="form-control col-md-7 col-xs-12" name="promotion_name_th" value="<?=htmlentities($row['promotion_name_th']);?>" required    placeholder="กรอกชื่อโครงการ (จีน)" >
                      </div>
                    </div>
                    <div class="form-group hide-for-th">
                      <label class="control-label col-md-2" for="first-name">ชื่อโปรโมชั่น (อังกฤษ) <span class="required"></span>
                      </label>
                      <div class="col-md-6">
                        <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="promotion_name_en" value="<?=str_replace('"', '&quot;', $row['promotion_name_en']);?>" placeholder="กรอกชื่อโครงการ (อังกฤษ)" >
                      </div>
                    </div>
                     <div class="form-group">
                      <label class="control-label col-md-2" for="first-name">รายละเอียดโปรโมชั่น (จีน)
                      </label>
                      <div class="col-md-6">
                        <textarea id="message" class="form-control" name="promotion_detail_th" rows="7" placeholder="ระบุรายละเอียดโปรโมชั่น (จีน)" <?=$read_only?>><?php echo '' . (!empty($row['promotion_detail_th']) ? $row['promotion_detail_th'] : "") . ''; ?></textarea>
                      </div>
                    </div>
                    <div class="form-group hide-for-th">
                      <label class="control-label col-md-2" for="first-name">รายละเอียดโปรโมชั่น (อังกฤษ)
                      </label>
                      <div class="col-md-6">
                        <textarea id="message" class="form-control" name="promotion_detail_en" rows="7" placeholder="ระบุรายละเอียดโปรโมชั่น (อังกฤษ)" <?=$read_only?>><?php echo '' . (!empty($row['promotion_detail_en']) ? $row['promotion_detail_en'] : "") . ''; ?></textarea>
                      </div>
                    </div>

                    <?php
$sql = "SELECT project_id,project_name_th,project_name_en FROM LH_PROJECTS WHERE group_id = '" . $group_id . "'";
$query_result3 = mssql_query($sql, $db_conn);
?>
                    <div class="form-group">
                      <label class="control-label col-md-2" for="brand">เลือกรูปและโครงการ ใหม่ <span class="required">*</span>
                      </label>

                      <?php
$query_3 = "SELECT * FROM dbo.LH_PROJECTS order by project_name_th asc;";
$query_result3 = mssql_query($query_3, $db_conn);

?>

                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select id="select" class="form-control" name="project_id" required onchange="showUser(this.value)" <?=$read_only?>>
                            <option value="0">เลือกโครงการ</option>
                             <?php
while ($row1 = mssql_fetch_array($query_result3)) {
	if ($row1['project_id'] == $row['project_id']) {
		$selected = 'selected';
	} else {
		$selected = '';
	}
	?>
                             <option value="<?=$row1['project_id'];?>" <?=$selected?>><?=$row1['project_name_th'];
	echo '' . (!empty($row1['project_name_en']) ? ' (' . $row1['project_name_en'] . ")" : "") . ''?></option>
                            <?php }?>

                          </select>
                        </div>
                      </div>

                      <script>
                          $("#select").trigger('change');
                         function showUser(str) {
                            if (str=="") {
                              document.getElementById("txtHint").innerHTML="";
                              return;
                            }
                            if (window.XMLHttpRequest) {
                              // code for IE7+, Firefox, Chrome, Opera, Safari
                              xmlhttp=new XMLHttpRequest();
                            } else { // code for IE6, IE5
                              xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                            }
                            xmlhttp.onreadystatechange=function() {
                              if (this.readyState==4 && this.status==200) {
                                document.getElementById("img").innerHTML=this.responseText;
                              }
                            }
                            xmlhttp.open("GET","ajaxselect_img_galery_project.php?id="+str+"&edit=<?=$_GET['id']?>",true);
                            xmlhttp.send();
                          }
                    </script>
                    <?php //} ?>

                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-3 col-xs-12"></label>
                        <div class="col-md-8">
                          <label for="iCheck"  class="error"></label>
                      </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-3 col-xs-12"></label>
                        <div class="col-md-8">
                          <div id="map-image-holder-brochure"></div>
                          <div id="img"></div>
                      </div>
                    </div>

                    <?php
$date = $row['promotion_start_date'];
$year = substr($date, 0, 4);
$mont = substr($date, 5, 2);
$day = substr($date, 8, 2);
$strDate = $day . "/" . $mont . "/" . $year;

$date2 = $row['promotion_end_date'];
$year2 = substr($date2, 0, 4);
$mont2 = substr($date2, 5, 2);
$day2 = substr($date2, 8, 2);
$strDate2 = $day2 . "/" . $mont2 . "/" . $year2;
?>
                     <div class="form-group">
                      <label class="control-label col-md-2" for="first-name">ระบุวันเริ่ม <span class="required">*</span>
                      </label>
                      <div class="col-md-2">
                        <fieldset>
                        <div class="input-prepend input-group">
                           <input type="text" style="width: 180px" name="date_start" required id="date_start" class="form-control" value="<?php echo $strDate; ?>" readonly/>
                         </div>
                        </fieldset>
                    </div>
                    <label class="control-label col-md-2" for="first-name">ระบุวันสิ้นสุด <span class="required" aria-required="true">*</span></label>
                        <div class="col-md-2">
                        <fieldset>
                        <div class="input-prepend input-group">
                           <input type="text" style="width: 180px" name="date" required="" id="date" class="form-control " value="<?php echo $strDate2; ?>" aria-required="true" readonly />
                         </div>
                        </fieldset>
                    </div>
                  </div>

                 <?php $path = "http://devwww2.lh.co.th/Land_and_house/Frontend/";?>
                    <div class="form-group">
                      <label class="control-label col-md-2" for="first-name">กำหนด URL พิเศษ
                      </label>
                      <div class="col-md-6">
                        <input type="hidden" name="id" value="<?=$row['promotion_id']?>">
                        <input type="text" id="first-name2" name="promotion_url" placeholder="EX : http://lh.co.th or https://lh.co.th" value="<?php echo '' . (!empty($row['promotion_url']) ? $row['promotion_url'] : "") . ''; ?>" class="form-control col-md-7 col-xs-12" <?=$read_only?> >
                      </div>
                      <p style="color:#929292;">**ถ้าไม่กำหนดจะลิงค์ไปยังหน้าโครงการอัตโนมัติ</p>
                    </div>

                    <?php if ($_SESSION['group_account_name'] == 'Admin') {?>
                    <div class="form-group">
                      <label class="control-label col-md-2" >แอดมินอนุมัติ</label>
                      <div class="col-md-5">
                      <p style="padding: 5px;">
                        <input type="checkbox" name="approve1" id="status2" value="Y" class="flat" <?php if ($row['promotion_approve'] == 'Y') {echo "checked";}?>   />  อนุมัติ

                        <br />
                        <p>
                      </div>
                    </div>
                    <?php }?>
                     <br>

                     <div class="form-group">
                        <label class="control-label col-md-2 col-sm-3 col-xs-12"></label>
                        <div class="col-md-8">
                           <?php if ($_SESSION['group_account_name'] == 'Admin') {?>


                             <a class="confirms" data-title="ยืนยันการลบข้อมูล" name="delete" href="delete_promotion.php?id=<?=$id;?>">
                            <button type="button" class="btn btn-danger">Delete</button></a>
                            <button type="submit"  name="approve" value="" class="btn btn-success">Approve</button>
                            <?php } else if ($row['promotion_approve'] == 'N') {?>

                              <a class="confirms" data-title="ยืนยันการลบข้อมูล" name="delete" href="delete_promotion.php?id=<?=$id;?>">
                            <button type="button" class="btn btn-danger">Delete</button></a>
                              <button type="submit" name="update" class="btn btn-success">Update</button>
                            <?php }?>
                      </div>
                    </div>

                  </form>
                  <br>
                   <br>
                    <br>
                     <br>
                      <br>
                       <br>
                       <script type="text/javascript">
                        $('a.confirms').confirm({
                          content: 'คุณแน่ใจที่จะลบข้อมูลนี้ !',
                            buttons: {
                              Yes: {
                                  text: 'Yes',
                                  btnClass: 'btn-danger',
                                  keys: ['enter', 'a'],
                                  action: function(){
                                      location.href = this.$target.attr('href');
                                  }
                              },
                              No: {
                                  text: 'No',
                                  btnClass: 'btn-default',
                                  keys: ['enter', 'a'],
                                  action: function(){
                                      // button action.
                                  }
                              },

                            }
                        });
                      </script>
                </div>
              </div>
            </div>
          </div>
        <!-- /page content -->

        <!-- footer content -->
        <!-- <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer> -->
        <!-- /footer content -->
      </div>
    </div>

        <!-- jQuery -->
        <!-- <script src="../vendors/jquery/dist/jquery.min.js"></script> -->
        <!-- Bootstrap -->
        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../vendors/nprogress/nprogress.js"></script>
        <!-- iCheck -->
        <script src="../vendors/iCheck/icheck.min.js"></script>
        <!-- date -->
        <link rel="stylesheet" type="text/css" href="../build/css/jquery-ui.css"/>
        <script src="../build/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
        <!-- jQuery Tags Input -->
        <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
        <!-- Fancybox image popup -->
        <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>
        <script src="../build/js/jquery.validate.js"></script>
        <script src="../build/js/custom.min.js"></script>

        <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>

        <script type="text/javascript">
            $('.fancybox').fancybox();
        </script>


        <!-- jQuery Tags Input -->
    <script>
      function onAddTag(tag) {
        alert("Added a tag: " + tag);
      }

      function onRemoveTag(tag) {
        alert("Removed a tag: " + tag);
      }

      function onChangeTag(input, tag) {
        alert("Changed a tag: " + tag);
      }

      $(document).ready(function() {
        $('#tags_1').tagsInput({
          width: 'auto'
        });
        $('#tags_2').tagsInput({
          width: 'auto'
        });
      });
    </script>


    <script>
        $(document).ready(function() {

            $("#commentForm").validate({
                rules: {
                    promotion_name_th: "required",
                    project_id: "required",
                    date_start : "required",
                    date : "required",
                    iCheck : "required",
                    //promotion_name_en : "required",
                },
                messages: {
                    promotion_name_th: "กรุณากรอกชื่อโปรโมชั่น จีน !",
                    project_id : "กรุณาเลือกโครงการ !",
                    date_start : "กรุณาระบุวันที่เริ่มต้น !",
                    date : "กรุณาระบุวันที่สิ้นสุด !",
                    iCheck : "กรุณาเลือกรูปอย่างน้อย 1 รูป",
                    //promotion_name_en : "กรุณากรอกชื่อโปรโมชั่น อังกฤษ !",
                }
            });

        });
    </script>
        <script type="text/javascript">

            $('#date_start').datepicker({
                dateFormat : "dd/mm/yy",
                autoclose: true,
                changeMonth: true,
                showDropdowns: true,
                changeYear: true,
                minDate:  new Date(),
                onSelect: function(dateText) {
                    var d = new Date($(this).datepicker("getDate"));
                    $("input#date").datepicker('option', 'minDate', dateText);
                    $("input#date").prop('disabled', false);
                }
            });
            $('#date').datepicker({
                dateFormat : "dd/mm/yy",
                autoclose: true,
                changeMonth: true,
                showDropdowns: true,
                changeYear: true,
                minDate:  new Date(),
                onSelect: function(dateText) {
                    //edited by yothin -> not depend with start date
                    // var d = new Date($(this).datepicker("getDate"))
                    // $("input#date_start").datepicker('option', 'maxDate', dateText);
                }
            });
        </script>

    <script>
        $(document).ready(function(){
            $("#alert").show();
            $("#alert").fadeTo(6000, 500).slideUp(500, function(){
                $("#alert").alert('close');
            });
        });
    </script>
  </body>
</html>