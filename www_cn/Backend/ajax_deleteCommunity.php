<?php
require_once 'include/dbConnect.php';

try {
    $id = $_GET['id'];

    $conn = (new dbConnect())->getConn();
    $sql = "DELETE FROM LH_COMMUNITY
				WHERE community_features_id =".$id;
    $result= $conn->query($sql);

    echo json_encode($result->fetchAll());

} catch (\Exception $e) {
    return $e->getMessage();
}
?>