<?php
session_start();
include './connect_db.php';
include './include/dbCon_mssql.php';
require_once './include/function_date.php';
$group_id=$_SESSION['group_id'];


$_GET['page']='motivo';

if (isset($_SESSION['add'])) {
            if($_SESSION['add'] ==1){
                $success ="success";
                $_SESSION['add']='';
            }  else if($_SESSION['add']== -1){
                $delete="delete";
                $_SESSION['add']='';
            }else if($_SESSION['add']== -3){
                $error_up="error_up";
                $_SESSION['add']='';
            }
        }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>

      <!-- Bootstrap -->
      <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome -->
      <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <!-- NProgress -->
      <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
      <!-- iCheck -->
      <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
      <!-- Datatables -->
      <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

      <!-- Custom Theme Style -->
      <link href="../build/css/custom.min.css" rel="stylesheet">

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
           <?php include './master/navbar.php';?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
          <?php include './master/top_nav.php'; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>ระบบจัดการข้อมูลนิตยสาร Motivo</h2>

                    <div class="clearfix"></div>
                  </div>

                  <?php if(isset($delete)){?>
                      <div class="row">
                              <div class="col-md-2"></div>
                               <div class="form-group">
                                <div class="alert alert-success col-md-6" id="alert">
                                  <button type="button" class="close" data-dismiss="alert">x</button>
                                  <strong>ลบข้อมูลเรียบร้อยแล้ว</strong>
                                </div>
                              </div>
                              <div class="col-md-4"></div>
                             </div>
                  <?php }?>
                  <?php if(isset($success)){?>
                   <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>สร้างข้อมูลเรียบร้อยแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-6"></div>
                   </div>
                  <?php }?>
                  <div class="x_content">

                    <a href="motivo_add.php"><button type="button" class="btn btn-success flright">สร้างข้อมูลนิตยสาร Motivo +</button></a>
                    <!-- <p class="text-muted font-13 m-b-30">
                      DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>
                    </p> -->
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
<!--                            <th><center>#</center></th>-->
                          <th><center>ชื่อนิตยสาร</center></th>
                          <th><center>ช่วงเวลา</center></th>
                          <th><center>สถานะ</center></th>
                          <th><center>จำนวนการ<br>Download</center></th>
                            <th><center>วันที่สร้าง</center></th>
                        </tr>
                      </thead>


                      <tbody>
                         <?php
                              $sql ="SELECT * FROM LH_MOTIVO ORDER BY  creat_date DESC";
                             $query= mssql_query($sql , $db_conn);
                             $i=1;
                              while ($row=mssql_fetch_array($query)){
                             $dates=$row['motivo_date'];
                               $mount1 = substr($dates, 0, 2);
                               $year1 = substr($dates, 6, 4);
                               $mount2 = substr($dates, 13, 2);
                               $year2 = substr($dates, 19, 4);

                               $date_start = $row['motivo_date_start'];
                               $month_start =  substr($date_start,5,2);
                               $year_start = substr($date_start,0,4);

                               $date_end = $row['motivo_date_end'];
                               $month_end =  substr($date_end,5,2);
                               $year_end = substr($date_end,0,4);

                               if($mount1!="10"){
                                 $mount1 =str_replace("0","",$mount1,$var);

                               }
                               if($mount2!="10"){
                                 $mount2 =str_replace("0","",$mount2,$var);
                               }
                               //$arrayName = array('','January','February','March','April','May','June','July','August','September','October','November','December');
                               $arrayName = array('','ม.ค','ก.พ','มี.ค','เม.ย','พ.ค','มิ.ย','ก.ค','ส.ค','ก.ย','ต.ค','พ.ย','ธ.ค');
                               $arraymonth = array("01"=>"ม.ค","02"=>"ก.พ","03"=>"มี.ค","04"=>"เม.ย","05"=>"พ.ค","06"=>"มิ.ย","07"=>"ก.ค","08"=>"ส.ค","09"=>"ก.ย","10"=>"ต.ค","11"=>"พ.ย","12"=>"ธ.ค");

                                  $date3=date_create($row['creat_date']);
                                  $strDate3 =date_format($date3,"Y-m-d H:i:s");
                         ?>
                        <tr>


                          <td><p style="display: none;"><?=$row['motivo_date_start']?></p><a href="motivo_update.php?id=<?=$row['motivo_id'];?>"><?=$row['motivo_name']?></a></td>
                          <td>
                              <a href="motivo_update.php?id=<?=$row['motivo_id'];?>"><center>
                                      <?=$date_start !=''?$arraymonth[$month_start]." ".$year_start." - ".$arraymonth[$month_end]." ".$year_end: '';?>
                                  </center></a></td>
                          <td><a href="motivo_update.php?id=<?=$row['motivo_id'];?>"><center><?php if($row['motivo_status']=='New'){echo "NEW";}else{echo "";}?></a></td>
                          <td><a href="motivo_update.php?id=<?=$row['motivo_id'];?>"><center><?=$row['download_num'] ?></center></a></td>
                            <td><p class="hidden"><?=$strDate3;?></p>
                                <center><?=DateThai_time($row['creat_date']);?></center>
                            </td>
                        </tr>
                        <?php $i++;} ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Custom Theme Scripts -->

    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <script src="../build/js/custom.min.js"></script>

    <!-- Datatables -->
    <script>
      $(document).ready(function() {


        $('#datatable').dataTable({
          "bLengthChange": false,
          "pageLength": 50,
          "order": []
        });


      });
    </script>
    <script>
        $(document).ready(function(){
            $("#alert").show();
            $("#alert").fadeTo(3000, 400).slideUp(500, function(){
                $("#alert").alert('close');
            });
        });
    </script>
    <!-- /Datatables -->
  </body>
</html>