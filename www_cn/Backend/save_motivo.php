<?php
session_start();
include './include/dbCon_mssql.php';
date_default_timezone_set('Asia/Bangkok');
$date = date("Ymd");

$motivo_name   = $_POST['motivo_name'];
$motivo_detail = $_POST['motivo_detail'];
$status        = $_POST['status'];
// $reservation =  $_POST['reservation'];
$motivo_detail = $_POST['motivo_detail'];
$month_start   = $_POST['month_start'];
$year_start    = $_POST['year_start'];
$month_end     = $_POST['month_end'];
$year_end      = $_POST['year_end'];

$numrand_img = (mt_rand());
$numrand_pdf = (mt_rand());
$images      = $_FILES['images'];
//print_r($images);
$pdf        = $_FILES['pdf'];
$file_image = $images['name'];
$file_pdf   = $pdf['name'];

$dates        = date("Y-m-d H:i:s");

$check = getimagesize($_FILES["images"]["tmp_name"]);

$sql   = "SELECT COUNT(*) AS counts FROM LH_MOTIVO WHERE motivo_name = N'" . $_POST['motivo_name'] . "'";
$query = mssql_query($sql);
$nums  = mssql_fetch_array($query);

if ($nums['counts'] >= 1) {
    $_SESSION['add'] = '2';
    echo '<script>
                        window.history.go(-1);
                    </script>';
    exit();
}

function mssql_escape($str)
{
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return str_replace("'", "''", $str);
}


    $start_date = $year_start . "-" . $month_start . "-" . "01";
    $end_date = $year_end . "-" . $month_end . "-" . "01";


if ($check !== false) {

    if (!empty($_FILES['pdf'])) {
        $dates_file = date("Y-m-d");
        $path_img   = "fileupload/images/motivo/";

        $type_img = strrchr($file_image, ".");
        $type_pdf = strrchr($file_pdf, ".");

        $newname_img   = $date . $numrand_img . $type_img;
        $path_copy_img = $path_img . $newname_img;

        $newname_pdf   = $date . $numrand_pdf . $type_pdf;
        $path_copy_pdf = $path_img . $newname_pdf;

        $seo_img = $_POST['seo_img'];

        if ($status == "") {
            $status = "No";
        }

        if (move_uploaded_file($_FILES['images']['tmp_name'], $path_copy_img)) {

            if (move_uploaded_file($_FILES['pdf']['tmp_name'], $path_copy_pdf)) {

                $sql   = "SELECT * FROM LH_MOTIVO WHERE motivo_name = '$motivo_name'";
                $query = mssql_query($sql, $db_conn);
                $row   = mssql_num_rows($query);
                if ($row <= 0) {

                    if($month_start != '') {
                        $sql6 = "INSERT INTO LH_MOTIVO (motivo_name,motivo_img_master,motivo_file,motivo_status,download_num,motivo_detail,motivo_date_start,motivo_date_end,creat_date ,update_date) "
                        . " VALUES (N'" . mssql_escape($motivo_name) . "','$newname_img','$newname_pdf' ,'$status','0',N'" . mssql_escape($motivo_detail) . "','$start_date','$end_date','$dates','$dates')";
                        $query_r6        = mssql_query($sql6, $db_conn);
                        $_SESSION['add'] = '1';
                        header("location:motivo.php");
                    }else{
                        $sql6 = "INSERT INTO LH_MOTIVO (motivo_name,motivo_img_master,motivo_file,motivo_status,download_num,motivo_detail,motivo_date_start,motivo_date_end,creat_date ,update_date) "
                            . " VALUES (N'" . mssql_escape($motivo_name) . "','$newname_img','$newname_pdf' ,'$status','0',N'" . mssql_escape($motivo_detail) . "',NULL,NULL,'$dates','$dates')";
                        $query_r6        = mssql_query($sql6, $db_conn);
                        $_SESSION['add'] = '1';
                        header("location:motivo.php");
                    }

                } else {
                    $_SESSION['add'] = '-3';
                    header("location:motivo_add.php");
                }
            }
        }
    }
} else {
    $_SESSION['add'] = '-3';
    header("location:motivo_add.php");
}
