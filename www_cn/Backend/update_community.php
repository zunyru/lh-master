<?php
session_start();
include './include/dbCon_mssql.php';
date_default_timezone_set('Asia/Bangkok');
$date         = date("Ymd");
$dates = date("Y-m-d H:i:s");
$numrand_logo = (mt_rand());
$numrand_img  = (mt_rand());

$community_name_th = $_POST['community_name_th'];
$community_name_en = $_POST['community_name_en'];
$community_img     = $_FILES['community_img'];
$community_dis_th  = $_POST['community_dis_th'];
$community_dis_en  = $_POST['community_dis_en'];
$community_img_seo = $_POST['seo'][0];
$file_image        = $community_img['name'];
$check             = getimagesize($_FILES["community_img"]["tmp_name"]);
$community_con_th  = $_POST['community_con_th'];
$community_con_en  = $_POST['community_con_en'];

$id = $_POST['id'];

$sql          = "SELECT COUNT(*) AS num FROM LH_COMMUNITY_PAGE WHERE community_name_th = '$community_name_th ' AND NOT community_id = '$id' ";
$query_result = mssql_query($sql, $db_conn);
$count_date   = mssql_fetch_row($query_result);
$count_date[0];
if ($count_date[0] >= 1) {
    $_SESSION['add'] = '2';
    echo '<script>
            window.history.go(-1);
          </script>';
    exit();
}

$query_2 = "UPDATE LH_COMMUNITY_PAGE SET community_name_th = N'$community_name_th', "
. "community_name_en ='$community_name_en',"
. "community_dis_th =N'" . mssql_escape($community_dis_th) . "',"
. "community_dis_en ='" . mssql_escape($community_dis_en) . "',"
. "communuty_content_th = N'" . mssql_escape($community_con_th) . "' ,"
. "communuty_content_en = '" . mssql_escape($community_con_en) . "' ,"
    . "community_update = '$dates' "
    . "WHERE community_id = '$id'";

$query_result2 = mssql_query($query_2, $db_conn);

function mssql_escape($str)
{
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return str_replace("'", "''", $str);
}

if ($check !== false) {
    //รูป

    $dates_file = date("Y-m-d");
    $path_img   = "fileupload/images/community_page/";
    //print_r($file_image);
    $type_img = strrchr($file_image, ".");

    $newname_img   = $dates_file . $numrand_img . $type_img;
    $path_copy_img = $path_img . $newname_img;

    if (move_uploaded_file($_FILES['community_img']['tmp_name'], $path_copy_img)) {
        $sql   = "SELECT community_img FROM LH_COMMUNITY_PAGE WHERE community_id = '$id'";
        $query = mssql_query($sql, $db_conn);
        $row   = mssql_fetch_array($query);
        unlink($path_img . $row['community_img']);

        $sql = "UPDATE LH_COMMUNITY_PAGE SET community_img = '$path_copy_img' ,community_update = '$dates', community_img_seo = N'" . mssql_escape($community_img_seo) . "'"
            . " WHERE community_id = '$id'";
        $query = mssql_query($sql, $db_conn);

    } else {
        $_SESSION['add'] = '-2';
        header("location:community_update.php?id=$id&add=-2");
    }
}

if ($query_result2) {
    $_SESSION['add'] = '1';
    header("location:community_update.php?id=$id&add=1");
} else {
    $_SESSION['add'] = '-2';
    header("location:community_update.php?id=$id&add=-2");
}
