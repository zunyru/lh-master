<?php
session_start();
header('Content-Type:text/html; charset=utf8');

if (!$_SESSION['login']) {
	$link = "Location: login.php";
	header($link);
}
//check privilage
if ($_SESSION['group_id'] != 1) {
	$link = "Location: project.php";
	header($link);
}
require_once 'include/dbConnect.php';
require_once 'include/dbCon_mssql.php'; // for insert utf8

$conn = (new dbConnect())->getConn();

if (isset($_POST['project_name_th']) &&
	isset($_POST['project_name_en'])) {
	$createDate = date("Y-m-d H:i:s");
	setProject($_POST, $createDate);

	$link = "Location: project.php?add=y";
	header($link); /* Redirect browser */
}

function mssql_escape($str)
{
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return str_replace("'", "''", $str);
}

function setProject($request, $createDate) {
	try {
		//$URL_Project = base_url().str_replace(" ","-",$request['project_name_en']);
		$sql_yum = "SELECT COUNT (project_id) As num FROM LH_PROJECTS WHERE project_name_th = N'" . $request['project_name_th'] . "' ";
		$result_yum = mssql_query($sql_yum, $GLOBALS['db_conn']);
		$num = mssql_num_rows($result_yum);
		$row = mssql_fetch_array($result_yum);

		if ($row['num'] > 0) {

			$link = "Location: project.php?yum=0";
			header($link);
			exit();
		}

		//var_dump($request);

		$URL_Project = str_replace(" ", "-", $request['project_name_th']);
		$sql = "INSERT INTO LH_PROJECTS (brand_id, zone_id, group_id, project_name_th, project_name_en, project_url, project_status, project_data_status, updated_date)
    VALUES (" . $request['brand_id'] . ", " . $request['zone_id'][0] . ", " . $request['group_id'] . ",
    N'" . $request['project_name_th'] . "', '" . $request['project_name_en'] . "', '" . $URL_Project . "', 'NP', 'FM', '" . $createDate . "' )";
		$result = mssql_query($sql, $GLOBALS['db_conn']);
		$maxId = getMaxId('LH_PROJECTS', 'project_id');
		$lastId = $maxId[0][0];

		setProjectZone($request, $lastId);
		setSubProject($request, $lastId);
		setSEO($request, $lastId);
		setpreview($request, $lastId);

	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}

}

function setProjectZone($request, $project_id) {
	try {

		$zone_id = 0;
		$province_id = 0;
		$amphur_id = 0;
		$district_id = 0;

		$i = 0;
		foreach ($request['zone_id'] as $key => $value_z) {
			$sql1 = "SELECT * FROM LH_ZONES WHERE zone_id = $value_z";
			$result1 = mssql_query($sql1, $GLOBALS['db_conn']);
			$row = mssql_fetch_array($result1);

			if ($value_z == '') {
				$value_z = 0;
			}
			if ($request['amphur_id'][$i] == '') {
				$request['amphur_id'][$i] = 0;
			}
			if ($request['district_id'][$i] == '') {
				$request['district_id'][$i] = 0;
			}

			$sql = "INSERT INTO LH_PROJECT_ZONE (project_id, zone_id, province_id, amphur_id, district_id)
           VALUES (" . $project_id . ", " . $value_z . ", " . $row['province_id'] . ", " . $request['amphur_id'][$i] . ", " . $request['district_id'][$i] . ")";
			$result = mssql_query($sql, $GLOBALS['db_conn']);
			$i++;
		}

	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}

}

function setpreview($request, $project_id) {
	try {
		$size2 = count($request['preview']);
		for ($i = 0; $i < $size2; $i++) {
			$sql = "INSERT INTO LH_PROJECT_VIEW (project_id, product_view_id)
      VALUES ('" . $project_id . "', '" . $request['preview'][$i] . "')";
			$result = mssql_query($sql, $GLOBALS['db_conn']);
		}
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}

}

function setSubProject($request, $project_id) {
	try {
		$size = count($request['product_id']);
		for ($i = 0; $i < $size; $i++) {
			$sql = "INSERT INTO LH_PROJECT_SUB (project_id, product_id)
      VALUES (" . $project_id . ", " . $request['product_id'][$i] . ")";
			$result = mssql_query($sql, $GLOBALS['db_conn']);
		}
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}

}

function setSEO($request, $project_id) {
	try {
		$sql = "INSERT INTO LH_PROJECT_SEO (project_id, project_title_seo_th, project_title_seo_en,
    project_des_seo_th, project_des_seo_en, project_keyword_th, project_keyword_en)
    VALUES (" . $project_id . ", N'" . mssql_escape($request['title_th']) . "', '" . $request['title_en'] . "',
    N'" . mssql_escape($request['desc_th']) . "', '" . $request['desc_en'] . "', N'" . mssql_escape($request['keyword_th']) . "',
    '" . $request['keyword_en'] . "')";
		$result = mssql_query($sql, $GLOBALS['db_conn']);

	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}

}

function getMaxId($table, $column) {
	try {
		$sql = 'select max(' . $column . ') from ' . $table;
		$result = $GLOBALS['conn']->query($sql);
		return $result->fetchAll();
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}

}

function getProductById($projectId) {
	try {
		$sql = 'SELECT * FROM LH_PROJECT_SUB PS
    LEFT JOIN LH_PROJECTS P
    ON PS.project_id = P.project_id
    LEFT JOIN LH_PRODUCTS PD
    ON PS.product_id = PD.product_id
    WHERE PS.project_id =' . $projectId;
		$result = $GLOBALS['conn']->query($sql);
		return $result->fetchAll();
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}

}

function getBrand() {
	try {
		$sql = 'SELECT * FROM LH_BRANDS';
		$result = $GLOBALS['conn']->query($sql);
		return $result->fetchAll();
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}

}

function getZone() {
	try {
		$sql = "SELECT zone_id,zone_name_th,zone_name_en FROM LH_ZONES  WHERE zone_attribute = 'websiteflm' OR zone_attribute = 'website' ORDER BY zone_name_th ASC";
		$result = $GLOBALS['conn']->query($sql);
		return $result->fetchAll();
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}

}

function getProvince() {
	try {
		$sql = 'SELECT * FROM lh_provinces';
		$result = $GLOBALS['conn']->query($sql);
		return $result->fetchAll();
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}

}

function getAmphur() {
	try {
		$sql = 'SELECT * FROM lh_amphurs';
		$result = $GLOBALS['conn']->query($sql);
		return $result->fetchAll();
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}

}

function getDistrict() {
	try {
		$sql = 'SELECT * FROM lh_districts';
		$result = $GLOBALS['conn']->query($sql);
		return $result->fetchAll();
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}

}

function getGroup() {
	try {
		$sql = 'SELECT G.group_id, G.group_account_name
    FROM LH_GROUPS G
    WHERE G.group_id != 1
    ORDER BY G.group_account_name ASC';
		$result = $GLOBALS['conn']->query($sql);
		return $result->fetchAll();
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}

}

function getProduct() {
	try {
		$sql = 'SELECT * FROM LH_PRODUCTS';
		$result = $GLOBALS['conn']->query($sql);
		return $result->fetchAll();
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}

}

function base_url($atRoot = FALSE, $atCore = FALSE, $parse = FALSE) {
	if (isset($_SERVER['HTTP_HOST'])) {
		$http = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
		$hostname = $_SERVER['HTTP_HOST'];
		$dir = str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

		$core = preg_split('@/@', str_replace($_SERVER['DOCUMENT_ROOT'], '', realpath(dirname(__FILE__))), NULL, PREG_SPLIT_NO_EMPTY);
		$core = $core[0];

		$tmplt = $atRoot ? ($atCore ? "%s://%s/%s/" : "%s://%s/") : ($atCore ? "%s://%s/%s/" : "%s://%s%s");
		$end = $atRoot ? ($atCore ? $core : $hostname) : ($atCore ? $core : $dir);
		$base_url = sprintf($tmplt, $http, $hostname, $end);
	} else {
		$base_url = 'http://localhost/';
	}

	if ($parse) {
		$base_url = parse_url($base_url);
		if (isset($base_url['path'])) {
			if ($base_url['path'] == '/') {
				$base_url['path'] = '';
			}
		}

	}

	return $base_url;
}

$_GET['page'] = 'form';

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>LAND & HOUSES</title>

  <!-- Bootstrap -->
  <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- NProgress -->
  <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
  <!-- iCheck -->
  <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
  <!-- Datatables -->
  <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
  <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
  <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
  <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
  <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

  <!-- Custom Theme Style -->
  <link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;">
            <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
          </div>

          <div class="clearfix"></div>

          <!-- menu profile quick info -->

          <!-- sidebar menu -->
          <?php include './master/navbar.php';?>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
        </div>
      </div>

      <!-- top navigation -->
      <?php include './master/top_nav.php';?>
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><a href="project.php">ระบบจัดการข้อมูลโครงการ</a> > <a href="project_add.php">กำหนดข้อมูลโครงการ</a></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <h4>Step1 : แอดมินสร้างข้อมูลโครงการให้นักการตลาด</h4>
                  <p class="font-gray-dark">
                  </p><br>
                  <form class="form-horizontal form-label-left" action="" method="POST" id="commentForm">
                    <div class="form-group">
                      <label class="control-label col-md-2">ระบุชื่อโครงการ <span class="required">*</span>
                      </label>
                      <div class="col-md-6">
                        <input type="text" name="project_name_th" required class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อโครงการ (จีน)">
                      </div>
                    </div>
                    <div class="form-group hide-for-th">
                      <label class="control-label col-md-2">ระบุชื่อโครงการ <span class="required">*</span>
                      </label>
                      <div class="col-md-6">
                        <input type="text" name="project_name_en" required  class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อโครงการ (อังกฤษ)" >
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2" for="brand">ระบุ Brand <span class="required">*</span>
                      </label>
                      <div class="col-md-4 col-sm-9 col-xs-12">
                        <select name="brand_id" class="form-control" required>
                          <option value="">เลือก Brand</option>
                          <?php
$sql = "SELECT * FROM LH_BRANDS ORDER BY brand_name_th ASC";
$query = mssql_query($sql);
while ($value = mssql_fetch_array($query)) {?>
                          <option value="<?php echo $value['brand_id']; ?>">
                            <?php echo $value['brand_name_th']; ?>
                          </option>
                          <?php }?>
                        </select>
                      </div>
                    </div>
                    <!-- zone -->
                    <div class="form-group">
                      <label class="control-label col-md-2" for="zone">ระบุ Zone <span class="required">*</span>
                      </label>
                      <div class="col-md-4 col-sm-9 col-xs-12">
                        <select name="zone_id[]" class="form-control zone" id="zone_id1" required>
                          <option value="">เลือกโซน</option>
                          <?php 
                          $sql = "SELECT zone_id,zone_name_th,zone_name_en FROM LH_ZONES  WHERE zone_attribute = 'websiteflm' OR zone_attribute = 'website' ORDER BY zone_name_th ASC";
                          $query = mssql_query($sql);
                          //foreach (getZone() as $value) {
                          while ($value = mssql_fetch_array($query)){
                            ?>
                          <option value="<?php echo $value['zone_id']; ?>">
                            <?php echo $value['zone_name_th']; ?>
                          </option>
                          <?php }?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-2" for="zone">ระบุชื่อจังหวัด   <span class="required">*</span></label>
                      <div class="col-md-4 col-sm-9 col-xs-12">
                        <select name="province_id[]" class="form-control province_id" id="province_id1" required disabled onChange="changeProvince(value)">
                          <option value="">เลือกจังหวัด</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-2" for="zone">ระบุชื่ออำเภอ  <!-- <span class="required">*</span> --></label>
                      <div class="col-md-4 col-sm-9 col-xs-12">
                        <select name="amphur_id[]" id="amphur_id1" class="form-control amphur_id" onChange="changeAmpur(value)">
                          <option value="">เลือกอำเภอ</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-2" for="zone">ระบุชื่อตำบล  <!-- <span class="required">*</span> --></label>
                      <div class="col-md-4 col-sm-9 col-xs-12">
                        <select name="district_id[]" id="district_id1" class="form-control">
                          <option value="">เลือกตำบล</option>
                        </select>
                      </div>
                    </div>

                    <!-- end zone -->

                    <hr>
                    <div id="divzone"></div>

                    <div class="form-group">
                      <label class="control-label col-md-5"></label>
                      <div class="col-md-4 col-sm-9 col-xs-12">
                        <button type="button" class="btn btn-success" onclick="addZone()">+ เพิ่มทำเล</button>
                      </div>
                    </div>


                    <div class="form-group">
                      <label class="control-label col-md-2" for="group">ระบุกลุ่ม <span class="required">*</span>
                      </label>
                      <div class="col-md-4 col-sm-9 col-xs-12">
                        <select name="group_id" class="form-control" required>
                          <option value="">เลือกกลุ่ม</option>
                          <?php foreach (getGroup() as $value) {?>
                          <option  value="<?php echo $value['group_id']; ?>" >
                            <?php echo $value['group_account_name']; ?>
                          </option>
                          <?php }?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2" > <span class="required">*</span> ประเภทที่อยู่อาศัย<br>(ระบุได้มากกว่า 1 รายการ)</label>
                      <p id="error-checkbox" style="color: #F44300;"></p>
                      <div class="col-md-3">
                        <div class="checkbox-group required">
                          <?php
                          $sql = mssql_query('SELECT * FROM LH_PRODUCTS');
                           while($value = mssql_fetch_array($sql)) {?>
                          <input type="checkbox" name="product_id[]" value="<?php echo $value['product_id']; ?>" data-parsley-mincheck="1"  class="flat" />  <?php echo $value['product_name_en']; ?>
                          <br />
                          <?php }?>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-2" for="first-name"></label>
                      <div class="col-md-6">
                        <label for="product_id[]"  class="error"></label>
                      </div>
                    </div>

                    <br>
                    <div class="form-group">
                      <label class="control-label col-md-2" for="group">เลือกหน้าที่จะแสดง <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-9 col-xs-12">
                        <input type="checkbox" name="preview[]" value="1" data-parsley-mincheck="1"  class="flat" />
                        บ้านเดี่ยว
                        <br />
                        <input type="checkbox" name="preview[]" value="2"   class="flat" />
                        ทาวน์โฮม
                        <br />
                        <input type="checkbox" name="preview[]" value="3"   class="flat" />
                        คอนโดมิเนียม
                        <br>
                        <input type="checkbox" name="preview[]" value="4"  class="flat" />
                        โครงการในต่างจังหวัด
                        <br>
                        <input type="checkbox" name="preview[]" value="5"   class="flat" />
                        Ladawan
                        <br />
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2" for="first-name"></label>
                      <div class="col-md-6">
                        <label for="preview[]"  class="error"></label>
                      </div>
                    </div>

                    <br>
                    <h4>Step2 : แอดมินสร้างข้อมูล Support SEO</h4>
                    <p class="font-gray-dark">
                    </p><br>
                    <div class="form-group">
                      <label class="control-label col-md-2">Title <span class="required"> *</span>
                        <br> (กรอกได้ไม่เกิน 70 คำ)
                      </label>
                      <div class="col-md-6">
                        <input type="text" name="title_th" id="title_th" required maxlength="70"  class="form-control col-md-7 col-xs-12" placeholder="Title SEO (จีน) กรอกได้ไม่เกิน 70 คำ" >
                      </div>
                    </div>
                    <div class="form-group hide-for-th">
                      <label class="control-label col-md-2">Title <span class="required"></span>
                        <br> (กรอกได้ไม่เกิน 70 คำ)
                      </label>
                      <div class="col-md-6">
                        <input type="text" name="title_en"  maxlength="70" class="form-control col-md-7 col-xs-12" placeholder="Title SEO (อังกฤษ) กรอกได้ไม่เกิน 70 คำ" >
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2">Description <span class="required"> *</span>
                        <br>(กรอกได้ไม่เกิน 160 คำ)
                      </label>

                      <div class="col-md-6">
                        <textarea name="desc_th" id="desc_th" required class="form-control" name="message" rows="4" maxlength="160" placeholder="Description (จีน) กรอกได้ไม่เกิน 160 คำ"></textarea>
                      </div>
                    </div>
                    <div class="form-group hide-for-th">
                      <label class="control-label col-md-2">Description <span class="required"></span>
                        <br>(กรอกได้ไม่เกิน 160 คำ)
                      </label>

                      <div class="col-md-6">
                        <textarea name="desc_en"  class="form-control" name="message" rows="4" maxlength="160" placeholder="Description (อังกฤษ) กรอกได้ไม่เกิน 160 คำ"></textarea>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-2">Keyword <span class="required"> *</span>
                      </label>
                      <div class="col-md-6">
                        <textarea name="keyword_th" required class="form-control" name="message" rows="4" placeholder="ตัวอย่าง : บ้านเดี่ยว , บ้านใหม่ ,ทาว์โฮม"></textarea>
                      </div>
                    </div>
                    <div class="form-group hide-for-th">
                      <label class="control-label col-md-2">Keyword <span class="required"> </span>
                      </label>
                      <div class="col-md-6">
                        <textarea name="keyword_en"  class="form-control" name="message" rows="4" placeholder="EX : Single house , Townhome "></textarea>
                      </div>
                    </div>

                    <br>

                    <center>
                      <div class="form-group">
                        <label class="control-label col-md-2"><span class="required"></span>
                        </label>
                        <div class="col-md-6">
                          <button type="submit" class="btn btn-success" id="realSubmit" >Submit</button></textarea>
                        </div>
                      </div>


                    </center>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <!-- <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer> -->
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>


    <!-- Validate -->
    <script src="../build/js/jquery.validate.js"></script>

    <script type="text/javascript">

      function addZone(){
        var index = $('.zone').length;

        var zone =`<!-- zone -->
        <div>
        <div class="form-group">
        <label class="control-label col-md-2" for="zone">ระบุ Zone <span class="required">*</span>
        </label>
        <div class="col-md-4 col-sm-9 col-xs-12">
        <select name="zone_id[]" class="form-control zone" id="zone_id${index + 1}" required >
        <option value="">เลือกโซน</option>`;
        <?php foreach (getZone() as $value): ?>

        zone+='<option value="<?php echo $value['zone_id']; ?>"><?=$value['zone_name_th'];?></option>';

          <?php endforeach;?>

         zone+=`</select>
          </div>
          </div>

          <div class="form-group">
          <label class="control-label col-md-2" for="zone">ระบุชื่อจังหวัด   <span class="required">*</span></label>
          <div class="col-md-4 col-sm-9 col-xs-12">
          <select name="province_id[]" id="province_id${index + 1}" class="form-control province_id" required disabled onChange="changeProvince(value,${index + 1})">
          <option value="">เลือกจังหวัด</option></select>
          </div>
          </div>

          <div class="form-group">
          <label class="control-label col-md-2" for="zone">ระบุชื่ออำเภอ  </label>
          <div class="col-md-4 col-sm-9 col-xs-12">
          <select name="amphur_id[]" id="amphur_id${index + 1}" class="form-control" onChange="changeAmpur(value,${index + 1})">
          <option value="">เลือกอำเภอ</option>
          </select>
          </div>
          </div>

          <div class="form-group">
          <label class="control-label col-md-2" for="zone">ระบุชื่อตำบล  </label>
          <div class="col-md-4 col-sm-9 col-xs-12">
          <select name="district_id[]" id="district_id${index + 1}" class="form-control district_id">
          <option value="">เลือกตำบล</option>
          </select>
          </div>
          </div>

          <div class="form-group">
          <label class="control-label col-md-2" ></label>
          <div class="col-md-4 col-sm-9 col-xs-12">
          <button class="btn btn-danger" onclick="RemoveZone(this);">- ลบทำเล</button>
          </div>
          </div>
          <hr>
          </div>

          <!-- end zone -->`;

          $('#divzone').append(zone);


        }


        function RemoveZone(element)
        {
          $(element).parent().parent().parent().remove();
        }
      </script>
      <script>
        $(document).ready(function() {

          $("#commentForm").validate({
            rules: {
              project_name_th: "required",
              project_name_en: "required",
              brand_id : "required",
                'zone_id[]' : "required",
                group_id  : "required",
                province_id : "required",
                'preview[]': "required",
                'product_id[]' : "required",
                        //title_th : "required",
                        //desc_th : "required",
                        keyword_th : "required",
                      },
                      messages: {
                        project_name_th: "กรุณากรอกชื่อ โครงการ จีน !",
                        project_name_en : "กรุณากรอกชื่อ โครงการ อังกฤษ !",
                        brand_id : "กรุณาเลือก Brand !",
                        zone_id : "กรุณาเลือก Zone !",
                        group_id  : "กรุณาเลือก กลุ่ม !",
                        'preview[]': "เลือกอย่างน้อย 1 รายการ",
                        'product_id[]' : "เลือกอย่างน้อย 1 รายการ",
                        province_id : "กรุณาเลือกจังหวัด",
                        //title_th : "กรุณากรอก Title seo จีน !",
                        //desc_th : "กรุณากรอก Description จีน !",
                        keyword_th : "กรุณากรอก keyword จีน !",
                      }
                    });

          $("#lead_img_file").rules("add", {
            required:true,
            messages: {
              required: " &nbsp; ไม่มีรูป !"
            }
          });

          $( "#title_th" ).rules( "add", {
            required: true,
            maxlength: 60,
            messages: {
              required: "กรุณากรอก Title seo จีน !",
              maxlength: jQuery.validator.format("กรอกตัวอักษรได้ไม่เกิน 60 ตัว !")
            }
          });

          $( "#desc_th" ).rules( "add", {
            required: true,
            maxlength: 160,
            messages: {
              required: "กรุณากรอก Description จีน !",
              maxlength: jQuery.validator.format("กรอกตัวอักษรได้ไม่เกิน 160 ตัว !")
            }
          });



        });
      </script>

      <!-- Datatables -->
      <script>
       $(document).ready(function(){

        // if($(".province_id").val()){
        //   changeProvince($(this).val());
        // }
        // if($(".amphur_id").val()){
        //   changeAmpur($(this).val());
        // }

        $(document).on('change', "select.zone", function() {

         var str = $(this).attr('id');
         var id_zone = str.replace("zone_id", "");
         console.log(id_zone);

         var params  = {
          zone_id: $('#zone_id'+id_zone).val()
        };
        getData(params,id_zone);
        console.log($("#province_id"+id_zone).val());
        changeProvince($("#province_id"+id_zone).val(),id_zone);
        changeAmpur($("#amphur_id"+id_zone).val(),id_zone);
      });

      });


       function getData(params,id) {
        var id_zone = id;
        console.log(id_zone);
        $.ajax({
          type: "POST",
          url: "search_zone.php",
          data: params,
          async: false,
          cache: false
        }).success(function (data) {
          console.log(data);
          if(data.provinces){
                // $('#province_id'+id_zone).empty();
                console.log(id_zone);
                $('#province_id'+id_zone).append('<option value="'+data.provinces.province_id+'" selected >'+data.provinces.province_name+'('+data.provinces.province_name_eng+')'+'</option>')
              }
            });
      }

      function changeProvince(id,index){
        console.log(id);
        if(id != 0) {
          var url = "ajax_getDataRefProvince.php";
          var link = url+'?province_id='+id;
          $.ajax( link )
          .done(function(response) {
            if (response != 0) {
              //console.log(response)
              if(response != ""){
                if(typeof response === "object"){
                  var json_obj = response;
                }else{
                              var json_obj =  $.parseJSON(response);//parse JSON
                            }
                          }

                      //Clear Elemant <Option>
                      $('#amphur_id'+index).empty();
                      $('#district_id'+index).empty();
                      $('#amphur_id'+index).val(json_obj.result_data[0].PROVINCE_NAME);
                      var htmlOption = '<option value="">เลือกอำเภอ</option>';
                      var htmlOption2 = '<option value="">เลือกตำบล</option>';
                      for (var i = 0; i < json_obj.result_data.length; i++) {
                        var toStr = json_obj.result_data[i].AMPHUR_NAME + '/ ' + json_obj.result_data[i].AMPHUR_NAME_ENG;
                        htmlOption += '<option value="'+json_obj.result_data[i].AMPHUR_ID+'">'+toStr+'</option>';
                      }
                      $('#amphur_id'+index).append(htmlOption);
                      $('#district_id'+index).append(htmlOption2);
                    }else{

                    }
                  })
          .fail(function() {

                    //Clear Elemant <Option>
                    $('#amphur_id'+index).empty();
                    $('#district_id'+index).empty();
                    console.error('Backend error');
                  })
          .always(function() {
            console.info( "complete" );
          });
        }else {

          //Clear Elemant <Option>
          $('#amphur_id').empty();
          $('#district_id').empty();
        }
      }

      function changeAmpur(id,index){
        if(id != 0) {
          var url = "ajax_getDataRefAmpur.php";
          var link = url+'?ampur_id='+id;
          $.ajax( link )
          .done(function(response) {
            if (response != 0) {
              console.log(response)
              if(response != ""){
                if(typeof response === "object"){
                  var json_obj = response;
                }else{
                              var json_obj =  $.parseJSON(response);//parse JSON
                            }
                          }

                      //Clear Elemant <Option>
                      $('#district_id'+index).empty();
                      $('#district_id'+index).val(json_obj.result_data[0].PROVINCE_NAME);
                      var htmlOption = '<option value="">เลือกตำบล</option>';
                      for (var i = 0; i < json_obj.result_data.length; i++) {
                        var toStr = json_obj.result_data[i].DISTRICT_NAME + '/ ' + json_obj.result_data[i].DISTRICT_NAME_ENG;
                        htmlOption += '<option value="'+json_obj.result_data[i].DISTRICT_ID+'">'+toStr+'</option>';
                      }
                      $('#district_id'+index).append(htmlOption);
                    }else{

                    }
                  })
          .fail(function() {

                    //Clear Elemant <Option>
                    $('#district_id'+index).empty();
                    console.error('Backend error');
                  })
          .always(function() {
            console.info( "complete" );
          });
        }else {

          //Clear Elemant <Option>
          $('#district_id'+index).empty();
        }
      }

    </script>
    <!-- /Datatables -->
    <!-- jQuery Tags Input -->
    <script>
      function onAddTag(tag) {
        alert("Added a tag: " + tag);
      }

      function onRemoveTag(tag) {
        alert("Removed a tag: " + tag);
      }

      function onChangeTag(input, tag) {
        alert("Changed a tag: " + tag);
      }

      $(document).ready(function() {
        $('#tags_1').tagsInput({
          width: 'auto'
        });
        $('#tags_2').tagsInput({
          width: 'auto'
        });
      });
    </script>
    <!-- /jQuery Tags Input -->

  </body>
  </html>