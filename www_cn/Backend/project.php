<?php
  session_start();
  header( 'Content-Type:text/html; charset=utf8');

  if (!$_SESSION['login']) {
        $link = "Location: login.php";
        header($link);
  }

  require_once 'include/dbConnect.php';
  require_once 'include/dbCon_mssql.php'; // for insert utf8

//FM เพิ่มข้อมูลแล้ว แต่ ยังไม่ เผยแหร่
//FLM ข้อมูลสำหรับ FLM และยังไม่ออนไลน์
//Published : ข้อมูลออนไลน์บนเว็บไซต์เรียบร้อยแล้ว

$sql_project ="SELECT project_id,public_date FROM LH_PROJECTS";
$query_project = mssql_query($sql_project);

while ($row = mssql_fetch_array($query_project)) {
    if($row['public_date']=='1900-01-01') {
        //เปลี่ยน status เป็น public
        $sql1 = "UPDATE LH_PROJECTS SET project_data_status  = 'FM' WHERE DATEDIFF(day,public_date,GETDATE())>=1 AND project_id = '".$row['project_id']."' ";
        $query1 = mssql_query($sql1);
    }else{
        $sql1 = "UPDATE LH_PROJECTS SET project_data_status  = 'PB' WHERE DATEDIFF(day,public_date,GETDATE())>=0 AND project_id = '".$row['project_id']."' ";
        $query1 = mssql_query($sql1);
    }

    $sql3 = "UPDATE LH_PROJECTS SET project_data_status  = 'FLM' WHERE DATEDIFF(day,public_date,GETDATE())<= -1  AND NOT project_status = 'SO'";
    $query3 = mssql_query($sql3);

    $sql2 = "UPDATE LH_PROJECTS SET project_status  = '' WHERE DATEDIFF(day,public_date,GETDATE())>= 180 AND NOT project_status = 'SO'";
    $query2 = mssql_query($sql2);
}

  $conn = (new dbConnect())->getConn();

  if (isset($_POST['group']) || isset($_POST['status']) || isset($_POST['status_project'])) {
  	$arrProject = searchProjects($_POST);
  }else{
  	$arrProject = getProjectsById($_SESSION['group_id']);

  }

function searchProjects($request) {
    try {
    	$_SESSION['idUser'] = '0';
    	$_SESSION['status'] = '0';
    	$_SESSION['status_project'] = '0';

    	//validate for sales
    	if ($_SESSION['group_id'] != 1) {
    		$request['group'] = $_SESSION['group_id'];
    	}
      	$sql = 'SELECT P.project_id,P.project_name_th,P.project_name_en,G.group_name,Z.zone_name_th,P.project_status
                ,P.project_data_status,P.updated_date,P.public_date
                FROM LH_PROJECTS P
				LEFT JOIN LH_GROUPS G
				ON P.group_id = G.group_id
				LEFT JOIN LH_ZONES Z
				ON P.zone_id = Z.zone_id

';
        if ($request['group'] != '0') {
    	 	$_SESSION['idUser'] = $request['group'];
          $sql = 'SELECT P.project_id,P.project_name_th,P.project_name_en,G.group_name,Z.zone_name_th,P.project_status
                ,P.project_data_status,P.updated_date,P.public_date
                    FROM LH_PROJECTS P
					LEFT JOIN LH_GROUPS G
					ON P.group_id = G.group_id
					LEFT JOIN LH_ZONES Z
					ON P.zone_id = Z.zone_id
					where G.group_id='.$request['group'];
        }
        if($request['status'] != '0'){
        	$_SESSION['status'] = $request['status'];
          $sql = "SELECT P.project_id,P.project_name_th,P.project_name_en,G.group_name,Z.zone_name_th,P.project_status
                ,P.project_data_status,P.updated_date,P.public_date
                    FROM LH_PROJECTS P
					LEFT JOIN LH_GROUPS G
					ON P.group_id = G.group_id
					LEFT JOIN LH_ZONES Z
					ON P.zone_id = Z.zone_id
					where P.project_data_status="."'".$request['status']."'";
        }
        if($request['status_project'] != '0'){
        	$_SESSION['status_project'] = $request['status_project'];
          	$sql = "SELECT P.project_id,P.project_name_th,P.project_name_en,G.group_name,Z.zone_name_th,P.project_status
                ,P.project_data_status,P.updated_date,P.public_date
                    FROM LH_PROJECTS P
					LEFT JOIN LH_GROUPS G
					ON P.group_id = G.group_id
					LEFT JOIN LH_ZONES Z
					ON P.zone_id = Z.zone_id
					where P.project_status="."'".$request['status_project']."'";
			//only sales
    		if ($_SESSION['group_id'] != 1) {
				$sql .="AND P.group_id=".$request['group'];
			}
        }
        if ($request['group'] != '0' && $request['status'] != '0') {
    	 	$_SESSION['idUser'] = $request['group'];
        	$_SESSION['status'] = $request['status'];
        	$_SESSION['status_project'] = $request['status_project'];

          	$sql = "SELECT P.project_id,P.project_name_th,P.project_name_en,G.group_name,Z.zone_name_th,P.project_status
                ,P.project_data_status,P.updated_date,P.public_date
                    FROM LH_PROJECTS P
					LEFT JOIN LH_GROUPS G
					ON P.group_id = G.group_id
					LEFT JOIN LH_ZONES Z
					ON P.zone_id = Z.zone_id
					where P.group_id=".$request['group']." AND P.project_data_status="."'".$request['status']."'";
        }
        if ($request['group'] != '0' && $request['status_project'] != '0') {
    	 	$_SESSION['idUser'] = $request['group'];
        	$_SESSION['status_project'] = $request['status_project'];

          	$sql = "SELECT P.project_id,P.project_name_th,P.project_name_en,G.group_name,Z.zone_name_th,P.project_status
                ,P.project_data_status,P.updated_date,P.public_date
                    FROM LH_PROJECTS P
					LEFT JOIN LH_GROUPS G
					ON P.group_id = G.group_id
					LEFT JOIN LH_ZONES Z
					ON P.zone_id = Z.zone_id
					where P.group_id=".$request['group'];
			$sql .=" AND P.project_status="."'".$request['status_project']."'";

        }
        if ($request['status'] != '0' && $request['status_project'] != '0') {
        	$_SESSION['status'] = $request['status'];
        	$_SESSION['status_project'] = $request['status_project'];

          	$sql = "SELECT P.project_id,P.project_name_th,P.project_name_en,G.group_name,Z.zone_name_th,P.project_status
                ,P.project_data_status,P.updated_date,P.public_date
                    FROM LH_PROJECTS P
					LEFT JOIN LH_GROUPS G
					ON P.group_id = G.group_id
					LEFT JOIN LH_ZONES Z
					ON P.zone_id = Z.zone_id";
			$sql .=" where P.project_data_status="."'".$request['status']."'";
			$sql .=" AND P.project_status="."'".$request['status_project']."'";

        }
        if ($request['group'] != '0' && $request['status'] != '0' && $request['status_project'] != '0') {
    	 	$_SESSION['idUser'] = $request['group'];
        	$_SESSION['status'] = $request['status'];
        	$_SESSION['status_project'] = $request['status_project'];

          	 $sql = "SELECT P.project_id,P.project_name_th,P.project_name_en,G.group_name,Z.zone_name_th,P.project_status
                ,P.project_data_status,P.updated_date,P.public_date
                    FROM LH_PROJECTS P
					LEFT JOIN LH_GROUPS G
					ON P.group_id = G.group_id
					LEFT JOIN LH_ZONES Z
					ON P.zone_id = Z.zone_id
					where P.group_id=".$request['group'];
			 $sql .=" AND P.project_data_status="."'".$request['status']."'";
			 $sql .=" AND P.project_status="."'".$request['status_project']."'";

        }

        $result = mssql_query($sql);
        return $result;
//        $result = $GLOBALS['conn']->query($sql);
//        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}

function getProjectsById($userId) {
    try {
	 	$_SESSION['idUser'] = '0';
    	$_SESSION['status'] = '0';
    	$_SESSION['status_project'] = '0';
        if ($userId == 1) {
          $sql = 'SELECT project_id,project_name_th,project_name_en,zone_name_th,group_name,project_status,public_date,project_data_status,updated_date FROM LH_PROJECTS P
        					LEFT JOIN LH_GROUPS G
        					ON P.group_id = G.group_id
        					LEFT JOIN LH_ZONES Z
        					ON P.zone_id = Z.zone_id
                  ORDER BY P.updated_date DESC';
        }else{
          $sql = 'SELECT project_id,project_name_th,project_name_en,zone_name_th,group_name,project_status,public_date,project_data_status,updated_date FROM LH_PROJECTS P
        					LEFT JOIN LH_GROUPS G
        					ON P.group_id = G.group_id
        					LEFT JOIN LH_ZONES Z
        					ON P.zone_id = Z.zone_id
        					where P.group_id='.$userId;
          $sql .=' ORDER BY P.updated_date DESC';
        }
        $result = mssql_query($sql);
        return $result;
//        $result = $GLOBALS['conn']->query($sql);
//        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}

//function getProductById($projectId) {
//    try {
//          $sql = 'SELECT * FROM LH_PROJECT_SUB PS
//			LEFT JOIN LH_PROJECTS P
//			ON PS.project_id = P.project_id
//			LEFT JOIN LH_PRODUCTS PD
//			ON PS.product_id = PD.product_id
//			WHERE PS.project_id ='.$projectId;
//        $result = $GLOBALS['conn']->query($sql);
//        return $result->fetchAll();
//    }
//    catch(PDOException $e) {
//        echo $sql . "<br>" . $e->getMessage();
//    }
//
//}

function getGroup() {
    try {
          $sql = 'SELECT G.group_id, G.group_account_name
        					FROM LH_GROUPS G
        					WHERE G.group_id != 1
                  ORDER BY G.group_account_name ASC';
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}
$_GET['page']='form';

if (isset($_SESSION['add'])) {
    if($_SESSION['add'] ==1){
        //header("location:product_add.php");
        $success ="success";
    } else if($_SESSION['add'] ==0){

        $error="error"; //ค่าซ้ำ
        unset($_SESSION["add"]);

    } else if($_SESSION['add'] ==-1){
        $delete="errors";
    }
}

unset($_SESSION["add"]);

?>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>

      <!-- Bootstrap -->
      <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome -->
      <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <!-- NProgress -->
      <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
      <!-- iCheck -->
      <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
      <!-- Datatables -->
      <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

      <!-- Custom Theme Style -->
      <link href="../build/css/custom.min.css" rel="stylesheet">


  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="project.php" class="site_title"> <img style="padding-top: 10px;" src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

           <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <?php include './master/navbar.php';?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include './master/top_nav.php'; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row">
            	<?php if($_SESSION['group_status']!=="Admin"): ?>
            	<div class="col-md-12 col-sm-12 col-xs-12">
            		<div class='alert alert-info' style="font-size: 25px; font-weight: 500; letter-spacing: 1px; color: white; padding: 10px;">  หากต้องการสร้างข้อมูลโครงการใหม่กรุณาติดต่อฝ่าย Coco </div>
            	</div>
            	<?php endif;?>
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>ระบบจัดการข้อมูลโครงการ</h2>

                    <div class="clearfix"></div>
                  </div>
                   <?php if(isset($_GET['add'])){?>
                      <div class="row">
                        <div class="col-md-2"></div>
                         <div class="form-group">
                          <div class="alert alert-success col-md-6" id="alert">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <strong>สร้างข้อมูล ! </strong> เรียบร้อยแล้ว
                          </div>
                        </div>
                        <div class="col-md-4"></div>
                       </div>
                  <?php }?>
                   <?php if(isset($delete)){?>
                      <div class="row">
                        <div class="col-md-2"></div>
                         <div class="form-group">
                          <div class="alert alert-success col-md-6" id="alert">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <strong>ลบข้อมูล!</strong> เรียบร้อย
                          </div>
                        </div>
                        <div class="col-md-4"></div>
                       </div>
                  <?php }?>
                    <?php if(isset($error)){?>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="form-group">
                                <div class="alert alert-danger col-md-6" id="alert">
                                    <button type="button" class="close" data-dismiss="alert">x</button>
                                    <strong>ไม่สามารถลบข้อมูลได้</strong> เนื่องจากมีบ้านพร้อมขายผูกกับโครงการนี้อยู่
                                </div>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                    <?php }?>
                    <?php if(isset($_GET['yum'])){?>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="form-group">
                                <div class="alert alert-danger col-md-6" id="alert">
                                    <button type="button" class="close" data-dismiss="alert">x</button>
                                    <strong>มีชื่อโครงการนี้อยู่ในระบบแล้ว !</strong>
                                </div>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                    <?php }?>

                  <div class="x_content">
	                    <form id="form-project" action="" method="POST">
                  		<?php if($_SESSION['group_id'] == 1) { ?>
	                    <div class="col-md-2 col-sm-9 col-xs-12">
	                      <select name="group" onchange="search()" class="form-control">
	                        <option value="0">เลือกกลุ่ม</option>
	                          <?php
                              foreach (getGroup() as $value) {

		                  			if ($value['group_id'] == $_SESSION['idUser']) {
		                  		?>
	                  				<option value="<?php echo $value['group_id']; ?>" selected>
	                  					<?php echo $value['group_account_name']; ?>
	                  				</option>
	                  			<?php }else{ ?>
		                            <option value="<?php echo $value['group_id']; ?>">
		                                <?php echo $value['group_account_name']; ?>
		                            </option>
		                    	<?php }} ?>
	                      </select>
	                    </div>
                   		<?php } ?>
	                    <div class="col-md-3 col-sm-9 col-xs-12">
	                      <select name="status_project" onchange="search()" class="form-control">
	                          <?php if( $_SESSION['status_project'] == '0' ){?>
	                            <option value="0" selected>เลือกดูตามสถานะโครงการ</option>
	                            <option value="NP" >New Project</option>
	                            <option value="SO" >Sold Out</option>
	                          <?php }elseif( $_SESSION['status_project'] == 'NP' ){?>
	                            <option value="0">เลือกดูตามสถานะโครงการ</option>
	                            <option value="NP" selected>New Project</option>
	                            <option value="SO" >Sold Out</option>
	                          <?php }elseif( $_SESSION['status_project'] == 'SO' ) {?>
	                            <option value="0">เลือกดูตามสถานะโครงการ</option>
	                            <option value="NP" >New Project</option>
	                            <option value="SO" selected>Sold Out</option>
	                          <?php }else{ ?>
	                            <option value="0">เลือกดูตามสถานะโครงการ</option>
	                            <option value="PB" >Published</option>
	                            <option value="NP" >New Project</option>
	                            <option value="SO" >Sold Out</option>
	                    	<?php } ?>
	                      </select>
	                    </div>

	                    <div class="col-md-3 col-sm-9 col-xs-12">
	                      <select name="status" onchange="search()" class="form-control">
	                          <?php if( $_SESSION['status'] == '0' ){?>
	                            <option value="0" selected>เลือกดูตามสถานะข้อมูล</option>
	                            <option value="PB" >Published</option>
	                            <option value="FM" >For Approve</option>
	                            <option value="FLM" >For FLM</option>
	                          <?php }elseif( $_SESSION['status'] == 'PB' ){?>
	                            <option value="0">เลือกดูตามสถานะข้อมูล</option>
	                            <option value="PB" selected>Published</option>
	                            <option value="FM" >For Approve</option>
	                            <option value="FLM" >For FLM</option>
	                          <?php }elseif( $_SESSION['status'] == 'FM' ) {?>
	                            <option value="0">เลือกดูตามสถานะข้อมูล</option>
	                            <option value="PB" >Published</option>
	                            <option value="FM" selected>For Approve</option>
	                            <option value="FLM" >For FLM</option>
	                          <?php }elseif( $_SESSION['status'] == 'FLM' ) {?>
	                            <option value="0">เลือกดูตามสถานะข้อมูล</option>
	                            <option value="PB" >Published</option>
	                            <option value="FM" >For Approve</option>
	                            <option value="FLM" selected>For FLM</option>
	                          <?php }else{ ?>
	                            <option value="0">เลือกดูตามสถานะข้อมูล</option>
	                            <option value="PB" >Published</option>
	                            <option value="FM" >For Approve</option>
	                            <option value="FLM" >For FLM</option>
	                    	<?php } ?>
	                      </select>
	                    </div>
	                    <button type="submit" id="realSubmit" class="hide">Submit-Hidden</button>

                    <?php if($_SESSION['group_id'] == 1) { ?>
                    <a href="project_add.php"><button type="button" class="btn btn-success flright">กำหนดข้อมูลโครงการ +</button></a>
                    <?php } ?>
                    <table id="datatable" class="table table-striped table-bordered" style="font-size: 20px;">
                      <thead>
                        <tr>
                          <th ><center>ชื่อโครงการ</center></th>
                          <th><center>กลุ่ม</center></th>
                          <th width="15%"><center>ทำเล</center></th>
                          <!-- <th><center>ประเภทที่อยู่อาศัยในโครงการ</center></th> -->
                          <th width="10%"><center>สถานะโครงการ</center></th>
                          <th width="10%"><center>สถานะข้อมูล</center></th>
                          <!-- <th><center>URL โครงการ</center></th> -->
                            <th width="10%"><center>ออนไลน์ข้อมูลวันที่</center></th>
                          <th width="12%"><center>แก้ไขข้อมูลล่าสุดวันที่</center></th>


                          <th width="10%"><center>ข้อมูลสำหรับ FLM</center></th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php
                            include "include/function_date.php";
                            while ($value = mssql_fetch_array($arrProject)){
                            //foreach ($arrProject as $value) {
                             $url = 'qc_projectview.php?project_id='.$value['project_id'];
                             $urlForFLM = 'projectforflm.php?project_id='.$value['project_id'];
                             $productName = "";

                                $date=date_create($value['updated_date']);
                                $strDate =date_format($date,"Y-m-d H:i:s");
                          ?>
	                        <tr>
	                          	<td ></p><a href="<?php echo $url ?>"><?php echo $value['project_name_th'] ?></a>
	                          	</td>

	                          	<td><a href="<?php echo $url ?>"><?php echo $value['group_name']; ?></a></td>

	                          	<td><a href="<?php echo $url ?>"><?php 

                               $sqlzone = "SELECT * FROM LH_PROJECT_ZONE WHERE project_id = '".$value['project_id']."' ";
                               $queryzone = mssql_query($sqlzone);

                               while ($row = mssql_fetch_array($queryzone)) {

                                  $sqlz = "SELECT zone_name_th FROM LH_ZONES WHERE zone_id = '".$row['zone_id']."'";
                                  $queryz = mssql_query($sqlz);
                                  $rowz = mssql_fetch_array($queryz);
                                  echo $rowz['zone_name_th']." ,"; 
                               }

                               ?></a></td>


								<?php if($value['project_status'] == 'NP') { ?>
	                          		<td><a href="<?php echo $url ?>">New Project</a></td>

	                          	<?php }else if($value['project_status'] == 'SO') { ?>
	                          		<td><a href="<?php echo $url ?>">Sold Out</a></td>
                                <?php }else { ?>
                                <td><a href="<?php echo $url ?>"></a></td>
                                <?php } ?>
								<?php if($value['project_data_status'] == 'FM') { ?>
	                          		<td><a href="<?php echo $url ?>">For Approve</a></td>
	                          	<?php }else if ($value['project_data_status'] == 'FLM') { ?>
	                          		<td><a href="<?php echo $url ?>">For FLM</a></td>
	                          	<?php }else { ?>
	                          		<td><a href="<?php echo $url ?>">Published</a></td>
                          		<?php } ?>

	                          	<td>
                                    <a href="<?php echo $url ?>"><?php if(($value['public_date']=="")||($value['public_date']=="1900-01-01")){echo "";}else{echo DateThai($value['public_date']);}  ?></a></td>
                                <td>
                                    <p style="display: none"><?=$strDate;?></p>
                                    <a href="<?php echo $url ?>"><?php echo DateThai_time($value['updated_date']) ?></a>
                                </td>
                                <td><a href="<?php echo $urlForFLM ?>">ดูรายละเอียด</a></td>
	                        </tr>
                          <?php } ?>

                      </tbody>
                    </table>
                    <br>
                    <div class="row">
                    	<div class="col-md-5">
		                    หมายเหตุ : สถานะข้อมูล
		                    <br>- For Approve : รอการใส่ข้อมูล และส่งแอดมินตรวจสอบ
		                    <br>- For FLM : ข้อมูลสำหรับ FLM และยังไม่ออนไลน์
		                    <br>- Published : ข้อมูลออนไลน์บนเว็บไซต์เรียบร้อยแล้ว
		                </div>
                    	<div class="col-md-5">
		                    หมายเหตุ : สถานะโครงการ
		                    <br>- New Project : ข้อมูลโครงการใหม่
		                    <br>- Sold Out : ปิดการขายโครงการ
		                </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Custom Theme Scripts -->

    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <script src="../build/js/custom.min.js"></script>

    <!--      <link href="../build/css/custom_copy.min.css" rel="stylesheet">-->
    <!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
    <script>
        $(document).ready(function(){
            $("#alert").show();
            $("#alert").fadeTo(3000, 400).slideUp(500, function(){
                $("#alert").alert('close');
            });
        });
    </script>


    <script type="text/javascript">
      function search(){
        $("#realSubmit").click();
      }
    </script>
    <!-- Datatables -->
    <script>
        $(document).ready(function() {


            $('#datatable').dataTable({
                "bLengthChange": false,
                "pageLength": 50,
                "order": [[ 6, "desc" ],[ 5, "desc" ]]
            });


        });
    </script>
    <!-- /Datatables -->
  </body>
</html>