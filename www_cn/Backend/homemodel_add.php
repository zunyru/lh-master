<?php
session_start();
include './include/dbCon_mssql.php';
$group_id = $_SESSION['group_id'];

$_GET['page'] = 'homemodel';
include './include/dbFucntionPlan.php';

$dbFucntionPlan = new dbFucntionPlan();

if (isset($_SESSION['add'])) {
	if ($_SESSION['add'] == 1) {
		//header("location:product_add.php");
		$success = "success";
		unset($_SESSION["add"]);
	} else if ($_SESSION['add'] == 0) {

		$error = "error"; //ค่าซ้ำ
		$_SESSION['add'] = '';

	} else if ($_SESSION['add'] == -1) {
		$errors = "errors";
		unset($_SESSION["add"]);
	}
}

?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <!-- Meta, title, CSS, favicons, etc. -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>LAND & HOUSES</title>

      <!-- Bootstrap -->
      <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome -->
      <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <!-- NProgress -->
      <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
      <!-- iCheck -->
      <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
      <!-- bootstrap-progressbar -->
      <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
      <!-- Custom Theme Style -->
      <link href="../build/css/custom.min.css" rel="stylesheet">

      <!-- jQuery -->
      <script src="../vendors/jquery/dist/jquery.min.js"></script>

      <!-- uploade img -->
      <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
      <!--uploade-->
      <!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
      <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
      <!-- confirm-->
      <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
      <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>

      <script>
        $(document).ready(function(){
          $("#alert").show();
          $("#alert").fadeTo(6000, 400).slideUp(500, function(){
            $("#alert").alert('close');
          });
        });
      </script>
      <style type="text/css">
      .thumbnail {
        height: 100px;
        margin: 5px;
      }
      .fileUpload {
        position: relative;
        overflow: hidden;
        //margin: 10px;
      }
      .fileUpload input.upload {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
      }
      .file-drop-zone-title {
        padding: 0px 0px !important;
      }

      .file-caption-main .btn-file {
       overflow: visible;
     }

     .file-caption-main .btn-file .error {
      position: absolute;
      bottom: -32px;
      right: 30px;
    }
    input#seo_img_360 {
     display: none;
   }
   .btn-switch {
     font-size: 14px;
     position: relative;
     display: inline-block;
     -webkit-user-select: none;
     -moz-user-select: none;
     -ms-user-select: none;
     user-select: none;
   }
   .btn-switch__radio {
    display: none;
  }
  .btn-switch__label {
    display: inline-block;
    padding: .75em .5em .75em .75em;
    vertical-align: top;
    font-size: 1em;
    font-weight: 700;
    line-height: 1.5;
    color: #666;
    cursor: pointer;
    transition: color .2s ease-in-out;
  }
  .btn-switch__label + .btn-switch__label {
   padding-right: .75em;
   padding-left: 0;
 }
 .btn-switch__txt {
  font-size: 19px;
  position: relative;
  z-index: 2;
  display: inline-block;
  min-width: 1.5em;
  opacity: 1;
  pointer-events: none;
  transition: opacity .2s ease-in-out;
}
.btn-switch__radio_no:checked ~ .btn-switch__label_yes .btn-switch__txt,
.btn-switch__radio_yes:checked ~ .btn-switch__label_no .btn-switch__txt {
  opacity: 0;
}
.btn-switch__label:before {
  content: "";
  position: absolute;
  z-index: -1;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background: #f0f0f0;
  border-radius: 1.5em;
  box-shadow: inset 0 .0715em .3572em rgba(43,43,43,.05);
  transition: background .2s ease-in-out;
}
.btn-switch__radio_yes:checked ~ .btn-switch__label:before {
  background: #1abc9c;
}
.btn-switch__label_no:after {
  content: "";
  position: absolute;
  z-index: 2;
  top: .5em;
  bottom: .5em;
  left: .5em;
  width: 2em;
  background: #fff;
  border-radius: 1em;
  pointer-events: none;
  box-shadow: 0 .1429em .2143em rgba(43,43,43,.2), 0 .3572em .3572em rgba(43,43,43,.1);
  transition: left .2s ease-in-out, background .2s ease-in-out;
}
.btn-switch__radio_yes:checked ~ .btn-switch__label_no:after {
  left: calc(100% - 2.5em);
  background: #fff;
}
.btn-switch__radio_no:checked ~ .btn-switch__label_yes:before,
.btn-switch__radio_yes:checked ~ .btn-switch__label_no:before {
  z-index: 1;
}
.btn-switch__radio_yes:checked ~ .btn-switch__label_yes {
  color: #fff;
}


</style>

</head>


<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;">
            <a href="index.html" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
          </div>

          <div class="clearfix"></div>

          <!-- menu profile quick info -->
          <?php include './master/navbar.php';?>
          <!-- /menu footer buttons -->
        </div>
      </div>

      <!-- top navigation -->
      <?php include './master/top_nav.php';?>
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><a href="homemodel.php">ข้อมูลแบบบ้าน</a> > <a href="homemodel.php">สร้างข้อมูลแบบบ้าน</a></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <p class="font-gray-dark">
                  </p><br>

                  <?php if (isset($success)) {?>
                  <div class="row">
                    <div class="col-md-2"></div>
                    <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>สร้างข้อมูลเรียบร้อยแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-6"></div>
                  </div>
                  <?php } else if (isset($error)) {?>
                  <div class="row">
                    <div class="col-md-2"></div>
                    <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>มีชื่อ หรือ รหัส อยู่ในระบบแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                  </div>
                  <?php } else if (isset($errors)) {?>

                  <div class="row">
                    <div class="col-md-2"></div>
                    <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>เกิดข้อผิดพลาด !</strong> ไม่สามารถบันทึกข้อมูลได้
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                  </div>
                  <?php }?>

                  <form class="form-horizontal form-label-left" action="save_plan_master.php" method="post" enctype="multipart/form-data"
                  id="commentForm"  novalidate="novalidate">
                  <div class="form-group">
                    <label class="control-label col-md-4" for="brand">เลือกสไตล์บ้าน <span class="required">*</span>
                    </label>
                    <?php
$query_3 = "SELECT * FROM LH_SERIES ORDER BY series_name_th ASC;";
$query_result3 = mssql_query($query_3, $db_conn);

?>
                    <div class="col-md-6 col-sm-9 col-xs-12">
                      <select class="form-control" name="series" required>
                        <option value="">เลือกสไตล์บ้าน</option>
                        <?php while ($row = mssql_fetch_array($query_result3)) {?>
                        <option value="<?=$row['series_id'];?>"><?=$row['series_name_th'];?></option>
                        <?php }?>

                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-4" for="first-name">รหัสแบบบ้าน <span class="required">*</span>
                    </label>
                    <div class="col-md-4">
                      <input type="text"  value="" name="plan_code" required class="form-control col-md-7 col-xs-12" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-4" for="first-name">ระบุชื่อแบบบ้าน (จีน) <span class="required">*</span>
                    </label>
                    <div class="col-md-6">
                      <input type="text"  value="" name="plan_name_th" required class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อแบบบ้าน (จีน)">
                    </div>
                  </div>
                  <div class="form-group hide-for-th">
                    <label class="control-label col-md-4" for="first-name">ระบุชื่อแบบบ้าน (อังกฤษ) <span class="required">*</span>
                    </label>
                    <div class="col-md-6">
                      <input type="text" id="first-name2" value="" name="plan_name_en" required  class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อแบบบ้าน (อังกฤษ)">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-4" for="first-name">พื้นที่ใช้สอย <span class="required">*</span>
                    </label>
                    <div class="col-md-4"  id="staticParent">
                      <input type="text" id="child" value=""  name="plan_useful"  class="form-control col-md-4 col-xs-12" placeholder="" required>
                    </div><label class="control-label col-md-2 textleft" >ตารางเมตร
                    </label>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-4" for="first-name">ประเภทบ้าน <span class="required">*</span>
                    </label>
                    <div class="col-md-4"  id="staticParent">
                      <select name="product_id" class="form-control">
                        <option value="">-เลือกประเภทบ้าน-</option>
                        <?php
$sql = "SELECT  * FROM LH_PRODUCTS ORDER BY product_name_th  ASC";
$q = mssql_query($sql);
while ($row = mssql_fetch_array($q)) {
	?>
                          <option value="<?=$row['product_id']?>"><?=$row['product_name_th'] ?></option>
                          <?php }?>
                        </select>
                      </div>
                    </label>
                  </div>


                  <br>
                  <div class="form-group">
                    <label class="control-label col-md-4" for="first-name">ระบุฟังก์ชั่นของบ้าน (ถ้าไม่มีไม่ต้องระบุ, ห้ามใส่ 0)
                    </label>
                  </div>
                  <?php

$spm = $dbFucntionPlan->show_FucntionPlan($group_id);

while ($row = $spm->fetch(PDO::FETCH_ASSOC)) {

	if ($row['function_plan_name_th'] == 'ที่จอดรถ') {
		$span = '<span class="required"> *</span>';
		$required = 'required';
		$_id = 'id="f_id1"';
		$name = 'name="fucn[]1"';
	} elseif ($row['function_plan_name_th'] == 'ห้องนอน') {
		$span = '<span class="required"> *</span>';
		$required = 'required';
		$_id = 'id="f_id2"';
		$name = 'name="fucn[]2"';
	} elseif ($row['function_plan_name_th'] == 'ห้องน้ำ') {
		$span = '<span class="required"> *</span>';
		$required = 'required';
		$_id = 'id="f_id3"';
		$name = 'name="fucn[]3"';
	} else {
		$_id = 'id=""';
		$span = '';
		$required = '';
		$name = 'name="fucn[]"';
	}

	?>

                  <div class="form-group" id="staticParent_<?=$row['function_plan_id']?>">
                    <label class="control-label col-md-4" for="first-name"><?=$row['function_plan_name_th'];
	echo $span;?>
                    </label>
                    <div class="col-md-2">
                      <input type="hidden" name="name_f[]" value="<?=$row['function_plan_id'];?>">
                      <input type="text" class="form-control col-md-7 col-xs-12 wdpercent1" <?=$name;?> <?=$_id;?> <?=$required;?> onkeypress="return isNumber(event)">
                    </div>
                    <div class="col-md-3" style="margin-left: -100px;">
                      <label class="control-label col-md-5" style="text-align: left;" ><?=$row['function_plan_pronoun'];?>
                      </label>
                    </div>
                  </div>

                  <?php }?>

                  <div class="form-group">
                    <label class="control-label col-md-10"><u>ภาพแบบบ้าน (.JPG , .PNG , .GIF )</u>
                      กรุณาเลือกภาพที่ต้องการเป็นภาพหลัก 1 ภาพ (ขนาดรูป 1920 x 1080 px) ภาพขนาดไม่เกิน 300 KB

                    </label>

                  </div>

                  <label id="model_galery_radio[]-error" class="error" for="model_galery_radio[]"></label>

                  <div class="row">
                    <div class="col-sm-3">
                      <div class="form-group">
                        <label class="control-label col-md-4">
                        </label>
                        <div class="col-md-12">
                          <input type="file" id="galery_model0" name="model_galery[]" />
                        </div>
                      </div>

                      <div class="form-group">

                        <div class="col-md-2">
                          <input type="radio" class="" value="0" name="model_galery_radio[]" >
                        </div>
                        <label >
                          เลือกเป็นภาพหลัก
                        </label>
                      </div>

                      <script>
                        $("#galery_model0").fileinput({
                                    uploadUrl: "upload.php", // server upload action
                                    maxFileCount: 1,
                                    allowedFileExtensions: ["jpg", "png", "jpeg"],
                                    browseLabel: 'เลือกรูป',
                                    removeLabel: 'ลบ',
                                    browseClass: 'btn btn-success',
                                    showUpload: false,
                                    showRemove:false,
                                    showCaption: false,
                                    xxx:'upload_model_galery_seo',
                                    dropZoneTitle : 'รูปภาพแบบบ้าน',
                                    minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                    minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                    maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                    maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด

                                    maxFileSize :300 ,
                                    msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                    msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                    msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                    msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                    msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                  });
                                </script>

                              </div>

                              <div id="Model_galery"></div>
                            </div>

                            <br>
                            <div class="form-group">
                              <label class="control-label col-md-3">
                              </label>
                              <div class="col-md-4">
                                <button type="button" class="btn btn-round btn-success" onclick="addModel_galery()" >+
                                  เพิ่มรูปภาพแบบบ้าน
                                </button>
                              </div>
                            </div>

                            <br>
                            <hr>

                            <div class="form-group">
                              <label class="control-label col-md-4" for="first-name">ภาพ Floor Plan ชั้น 1 <br> ภาพขนาดไม่เกิน 300 KB
                              </label>
                              <div class="col-md-6">
                                <input id="img_plan_1" name="images_plan_1[]" type="file" multiple class="file-loading" accept="image/*" >
                              </div>
                            </div>

                            <script>
                              $("#img_plan_1").fileinput(
                            //'enable',
                            {
                            uploadUrl: "upload.php", // server upload action

                            maxFileCount: 1,
                            allowedFileExtensions: ["jpg", "gif", "png", "jpeg"],
                            browseLabel: 'เลือกภาพ Floor Plan',
                            removeLabel: 'ลบ',
                            browseClass: 'btn btn-success',
                            showUpload: false,
                            showRemove: false,
                            showCaption: false,// ปิดช่องแสดงชื่อรูป
                            msgFilesTooMany: 'ไฟล์ที่คุณเลือกมีจำนวน <b>({n})</b> ซึ่งเกินกว่าที่ระบบอนุญาตที่ <b>{m}</b>, กรุณาลองใหม่อีกครั้ง!',
                            msgZoomModalHeading: 'ตัวอย่างละเอียด',
                            xxx:'seo_img_floor_paln1',
                            dropZoneTitle : 'รูปภาพ Floor Plan ชั้น 1',
                            maxFileSize :300 ,
                            msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',

                          });
                        </script>

                        <div class="form-group">
                          <label class="control-label col-md-4" for="first-name">บรรยาย Floor Plan ชั้น 1
                          </label>
                          <div class="col-md-6">
                           <textarea id="message"  class="form-control" name="message1_th" rows="7" placeholder="กรอกคำบรรยาย (จีน) Floor Plan ชั้น 1"></textarea>
                         </div>
                       </div>

                       <div class="form-group hide-for-th">
                        <label class="control-label col-md-4" for="first-name">บรรยาย Floor Plan ชั้น 1
                        </label>
                        <div class="col-md-6">
                         <textarea id="message"  class="form-control" name="message1_en" rows="7" placeholder="กรอกคำบรรยาย (อังกฤษ) Floor Plan ชั้น 1"></textarea>
                       </div>
                     </div>

                     <div class="form-group">
                      <label class="control-label col-md-4" for="first-name">ภาพ Floor Plan ชั้น 2 <br> ภาพขนาดไม่เกิน 300 KB
                      </label>
                      <div class="col-md-6">
                        <input id="img_plan_2" name="images_plan_2[]" type="file" multiple class="file-loading" accept="image/*" >
                      </div>
                    </div>

                    <script>
                      $("#img_plan_2").fileinput(
                            //'enable',
                            {
                            uploadUrl: "upload.php", // server upload action
                            maxFileCount: 1,
                            allowedFileExtensions: ["jpg", "gif", "png", "jpeg"],
                            browseLabel: 'เลือกภาพ Floor Plan',
                            removeLabel: 'ลบ',
                            browseClass: 'btn btn-success',
                            showUpload: false,
                            showRemove: false,
                            showCaption: false,//ปิดช่องแสดงชื่อรูป
                            msgFilesTooMany: 'ไฟล์ที่คุณเลือกมีจำนวน <b>({n})</b> ซึ่งเกินกว่าที่ระบบอนุญาตที่ <b>{m}</b>, กรุณาลองใหม่อีกครั้ง!',
                            msgZoomModalHeading: 'ตัวอย่างละเอียด',
                            xxx:'seo_img_floor_paln2',
                            dropZoneTitle : 'รูปภาพ Floor Plan ชั้น 2',
                            maxFileSize :300 ,
                            msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',

                          });
                        </script>

                        <div class="form-group">
                          <label class="control-label col-md-4" for="first-name">บรรยาย Floor Plan ชั้น 2
                          </label>
                          <div class="col-md-6">
                           <textarea id="message"  class="form-control" name="message2_th" rows="7" placeholder="กรอกคำบรรยาย Floor Plan (จีน) ชั้น 2"></textarea>
                         </div>
                       </div>

                       <div class="form-group hide-for-th">
                        <label class="control-label col-md-4" for="first-name">บรรยาย Floor Plan ชั้น 2
                        </label>
                        <div class="col-md-6">
                         <textarea id="message"  class="form-control" name="message2_en" rows="7" placeholder="กรอกคำบรรยาย Floor Plan (อังกฤษ) ชั้น 2"></textarea>
                       </div>
                     </div>
                     <div class="form-group">
                      <label class="control-label col-md-4" for="first-name">ภาพ Floor Plan ชั้น 3 <br> ภาพขนาดไม่เกิน 300 KB
                      </label>
                      <div class="col-md-6">
                        <input id="img_plan_3" name="images_plan_3[]" type="file" multiple class="file-loading" accept="image/*" >
                      </div>
                    </div>

                    <script>
                      $("#img_plan_3").fileinput(
                            //'enable',
                            {
                            uploadUrl: "upload.php", // server upload action
                            maxFileCount: 1,
                            allowedFileExtensions: ["jpg", "gif", "png", "jpeg"],
                            browseLabel: 'เลือกภาพ Floor Plan',
                            removeLabel: 'ลบ',
                            browseClass: 'btn btn-success',
                            showUpload: false,
                            showRemove: false,
                               showCaption: false, // ปิดช่องแสดงชื่อรูป
                               msgFilesTooMany: 'ไฟล์ที่คุณเลือกมีจำนวน <b>({n})</b> ซึ่งเกินกว่าที่ระบบอนุญาตที่ <b>{m}</b>, กรุณาลองใหม่อีกครั้ง!',
                               msgZoomModalHeading: 'ตัวอย่างละเอียด',
                               xxx:'seo_img_floor_paln2',
                               dropZoneTitle : 'รูปภาพ Floor Plan ชั้น 3',
                               maxFileSize :300 ,
                               msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',

                             });
                           </script>

                           <div class="form-group">
                            <label class="control-label col-md-4" for="first-name">บรรยาย Floor Plan ชั้น 3
                            </label>
                            <div class="col-md-6">
                             <textarea id="message"  class="form-control" name="message3_th" rows="7" placeholder="กรอกคำบรรยาย Floor Plan (จีน) ชั้น 3"></textarea>
                           </div>
                         </div>

                         <div class="form-group hide-for-th">
                          <label class="control-label col-md-4" for="first-name">บรรยาย Floor Plan ชั้น 3
                          </label>
                          <div class="col-md-6">
                           <textarea id="message"  class="form-control" name="message3_en" rows="7" placeholder="กรอกคำบรรยาย Floor Plan (อังกฤษ) ชั้น 3"></textarea>
                         </div>
                       </div>

                       <div class="form-group">
                        <label class="control-label col-md-4" for="first-name">URL 360 Virtual Tour
                        </label>
                        <div class="col-md-6">
                          <input type="text" id="c_360" name="c_360" class="form-control col-md-7 col-xs-12" placeholder="Url 360 Virtual Tour">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-4" for="first-name">360 Cover image <br>(ขนาดรูป 1920 x 1080) <br> ภาพขนาดไม่เกิน 300 KB
                        </label>
                        <div class="col-md-6">
                          <input type="file" id="img" name="c_360_img[]" class="form-control col-md-7 col-xs-12" accept="image/*">
                        </div>
                      </div>

                      <script>
                        $("#img").fileinput(
                              //'enable',
                              {
                                  uploadUrl: "upload.php", // server upload action
                                  maxFileCount: 1,
                                  allowedFileExtensions: ["jpg", "gif", "png", "jpeg"],
                                  browseLabel: 'เลือกภาพ Cover image',
                                  removeLabel: 'ลบ',
                                  browseClass: 'btn btn-success',
                                  showUpload: false,
                                  showRemove: false,
                                  showCaption: false, // ปิดช่องแสดงชื่อรูป
                                  msgFilesTooMany: 'ไฟล์ที่คุณเลือกมีจำนวน <b>({n})</b> ซึ่งเกินกว่าที่ระบบอนุญาตที่ <b>{m}</b>, กรุณาลองใหม่อีกครั้ง!',
                                  msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                  xxx:'seo_img_360',
                                  dropZoneTitle : 'รูปภาพ Cover image 360',
                                  minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                  minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                  maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                  maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                  maxFileSize :300 ,
                                  msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                  msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                  msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                  msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                  msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                });
                              </script>


                              <div class="form-group" id="inline_content">
                                <label class="control-label col-md-4" name="tvc_detail" for="first-name">เลือกชนิดการอัปไฟล์ <span class="required"></span>
                                </label>
                                <div class="col-md-6">
                                  <div class="radio">
                                    <label><input type="radio" id="youtube" name="optradio" checked value="youtube" class="flat"> VDO Youtube</label>
                                  </div>
                                  <div class="radio">
                                    <label><input type="radio" id="file" name="optradio" value="vdo" class="flat"> VDO File</label>
                                  </div>
                                </div>
                              </div>

                              <div id="l1">
                                <div class="form-group">
                                  <label class="control-label col-md-4" for="first-name">URL Youtube <span class="required"> </span>
                                  </label>
                                  <div class="col-md-6">
                                    <div class="control-group">
                                      <input type="text" name="youtube_url" id="youtube_url" class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube" value="">
                                    </div>
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label class="control-label col-md-4" for="first-name">Thumbnail Youtube <br> (ขนาดรูป 1920 x 1080) <br> ภาพขนาดไม่เกิน 300 KB<span class="required"> </span>
                                  </label>
                                  <div class="col-md-6">
                                    <div class="control-group">
                                      <input type="file" name="img_youtube" id="img_youtube" class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube" value="" accept="image/*">
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <script>
                                $("#img_youtube").fileinput({
                              uploadUrl: "upload.php", // server upload action
                              maxFileCount: 1,
                              allowedFileExtensions: ["jpg", "png", "jpeg"],
                              browseLabel: 'เลือกรูป',
                              removeLabel: 'ลบ',
                              browseClass: 'btn btn-success',
                              showUpload: false,
                              showRemove:false,
                              showCaption: false, // ปิดช่องแสดงชื่อรูป
                              msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                              msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                              msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                              msgZoomModalHeading: 'ตัวอย่างละเอียด',
                              xxx:'seo_lead_img_file',
                              dropZoneTitle : 'รูป Thumbnail Youtube',
                              minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                              minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                              maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                              maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                              maxFileSize :300 ,
                              msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                              msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                              msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                              msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                              msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                            });
                          </script>

                          <div id="l2">
                            <div class="form-group">
                              <label class="control-label col-md-4" for="first-name">Thumbnail VDO <br>(.jpg , .png ,.gif <br> ขนาด 1920 x 1080 px) <br> ภาพขนาดไม่เกิน 300 KB<span class="required"></span>
                              </label>
                              <div class="col-md-6">
                                <div class="control-group">
                                  <input type="file" name="thumbnail_vdo" id="thumbnail_vdo" class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube" value="" accept="image/*">
                                </div>
                              </div>
                            </div>

                            <script>
                              $("#thumbnail_vdo").fileinput({
                                  uploadUrl: "upload.php", // server upload action
                                  maxFileCount: 1,
                                  allowedFileExtensions: ["jpg", "png", "jpeg"],
                                  browseLabel: 'เลือกรูป',
                                  removeLabel: 'ลบ',
                                  browseClass: 'btn btn-success',
                                  showUpload: false,
                                  showRemove:false,
                                  showCaption: false, // ปิดช่องแสดงชื่อรูป
                                  msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                  msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                                  msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                  msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                  xxx:'seo_lead_img_file',
                                  dropZoneTitle : 'รูป Thumbnail VDO',
                                  minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                  minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                  maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                  maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                  maxFileSize :300 ,
                                  msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                  msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                  msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                  msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                  msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                });
                              </script>

                              <div class="form-group">
                                <label class="control-label col-md-4" for="first-name"> VDO<span class="required"> </span>
                                </label>
                                <div class="col-md-6">
                                  <div class="control-group">
                                    <input type="file" name="vdo_file" id="vdo_file" class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube" value="" accept="video/mp4">
                                  </div>
                                </div>
                              </div>

                              <script>
                                $("#vdo_file").fileinput({
                                  uploadUrl: "upload.php", // server upload action
                                  maxFileCount: 1,
                                  allowedFileExtensions: ["mp4"],
                                  browseLabel: 'เลือกไฟล์',
                                  removeLabel: 'ลบ',
                                  browseClass: 'btn btn-success',
                                  showUpload: false,
                                  showRemove:false,
                                  showCaption: false,
                                  msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                  msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                                  msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                  msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                  xxx:'seo_lead_img_file',
                                  dropZoneTitle : 'VDO File',

                                  msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                  msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                  msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                                  msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                                });
                              </script>
                            </div>

                            <br>

                            <div class="form-group">
                              <label class="control-label col-md-8"><u>Gallery แบบบ้าน (.JPG , .PNG , .GIF )
                              ใส่ภาพน้อยสุด 5 ภาพ และสูงสุด 15 ภาพ  ภาพขนาดไม่เกิน 300 KB</u>
                            </label>

                          </div>

                          <div class="row">
                            <div class="col-sm-3">
                              <div class="form-group">
                                <label class="control-label col-md-4">
                                </label>
                                <div class="col-md-12">
                                  <input type="file" id="galery0" name="project_galery[]" />
                                </div>
                              </div>
                              <div class="form-group">

                                <div class="col-md-12">
                                  <input type="text"  class="form-control"  name="project_galery_text[]" placeholder="กรอกรายละเอียด Gallery จีน "/>
                                </div>
                              </div>

                              <div class="form-group hide-for-th">

                                <div class="col-md-12">
                                  <input type="text"  class="form-control"  name="project_galery_text_en[]" placeholder="กรอกรายละเอียด Gallery อังกฤษ "/>
                                </div>
                              </div>
                            </div>

                            <div id="Project_galery"></div>

                          </div>

                          <br>
                          <div class="form-group">
                            <label class="control-label col-md-3">
                            </label>
                            <div class="col-md-4">
                              <button type="button" class="btn btn-round btn-success" onclick="addProject_galery()" >+
                                เพิ่มรูป Gallery แบบบ้าน
                              </button>
                            </div>
                          </div>

                          <script>
                            $("#galery0").fileinput({
                              uploadUrl: "upload.php", // server upload action
                              maxFileCount: 1,
                              allowedFileExtensions: ["jpg", "png", "jpeg"],
                              browseLabel: 'เลือกรูป',
                              removeLabel: 'ลบ',
                              browseClass: 'btn btn-success',
                              showUpload: false,
                              showRemove:false,
                              showCaption: false,
                              msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                              msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                              msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                              msgZoomModalHeading: 'ตัวอย่างละเอียด',
                              xxx:'upload_project_galery_seo',
                              dropZoneTitle : 'รูป Gallery แบบบ้าน',
                              // minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                              // minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                              // maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                              // maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                              maxFileSize :300 ,
                              msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                              msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                              msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                              msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                              msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                            });
                          </script>

                          <hr>
                          <br>

                          <div class="form-group">
                            <label class="control-label col-md-3">แสดงบนเว็บ
                            </label>
                            <div class="col-md-4">
                             <p class="btn-switch">
                               <input type="radio" id="yes" name="switch" value="on" class="btn-switch__radio btn-switch__radio_yes" />
                               <input type="radio" checked id="no" value="off" name="switch" class="btn-switch__radio btn-switch__radio_no" />
                               <label for="yes" class="btn-switch__label btn-switch__label_yes"><span class="btn-switch__txt">เปิด</span></label>                  <label for="no" class="btn-switch__label btn-switch__label_no"><span class="btn-switch__txt">ปิด</span></label>
                             </p>
                           </div>
                         </div>

                         <br>
                         <br>

                         <div class="col-md-10">
                          <center>
                            <button type="submit" class="btn btn-success">Submit</button>
                          </center>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /page content -->

              <!-- footer content -->
        <!-- <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer> -->
        <!-- /footer content -->
      </div>
    </div>

    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="js/bootstrap-imgupload.js"></script>
    <!-- Validate -->
    <script src="../build/js/jquery.validate.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

    <script>
      $(function() {
        $('#staticParent').on('keydown', '#child', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
      });
      $(function() {
        $('#staticParent2').on('keydown', '#child2', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
      });
      $(function() {
        $('#staticParent3').on('keydown', '#child3', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
      });
    </script>

    <script>
      $(document).ready(function(){
        $("#l1").show();
        $("#l2").hide();
      });
//              $('input:radio[name="optradio"]').change(function() {
//                  if ($(this).val() == 'youtube') {
//
//                  } else {
//                      $("#l1").hide();
//                      $("#l2").show();
//                  }
//              });

$("#youtube").on('ifChecked', function(event){
  $("#l1").show();
  $("#l2").hide();
});
$("#file").on('ifChecked', function(event){
  $("#l1").hide();
  $("#l2").show();
});


</script>

<!-- Validate -->
<script src="../build/js/jquery.validate.js"></script>
<script>
  $(document).ready(function() {

    $("#commentForm").validate({
      rules: {
        series: "required",
        plan_code: "required",
        plan_name_th : "required",
        plan_name_en : "required",
        plan_useful : "required",
        images_plan : "required",
        product_id : "required",
        'model_galery[]' : "required",
        'model_galery_radio[]' : "required",
      },
      messages: {
        series: "กรุณากรอกเลือก series !",
        plan_code : "กรุณากรอกรหัสแบบบ้าน !",
        plan_name_th : " กรุณากรอกชื่อ แบบบ้าน จีน !",
        plan_name_en : "กรุณากรอกชื่อ แบบบ้าน อังกฤษ !",
        plan_useful : " กรุณากรอก พื้นที่ใช้สอย !",
        images_plan : " &nbsp; กรุณาเลือกไฟล์",
        product_id : "กรุณากรอกเลือก ประเภทบ้าน !",
        'model_galery[]' : "กรุณาเลือกรูป !",
        'c_360_img[]' : "กรุณาเลือกรูป !",
        c_360 : "กรุณากรอก URL 360 Virtual Tour !",
        youtube_url : "กรุณากรอก URL Youtube !",
        img_youtube : "กรุณาเลือกรูป  !",
        thumbnail_vdo : "กรุณาเลือกรูป !",
        vdo_file : "กรุณาเลือกไฟล์ VDO",
        'model_galery_radio[]' : "กรุณาเลือกรูปภาพแบบ้านหลักจำนวน 1 รูปภาพ",


      }
    });


    $("#f_id1").rules("add", {
      required:true,
      messages: {
        required: "กรุณากรอกข้อมูล !"
      }
    });
    $("#f_id2").rules("add", {
      required:true,
      messages: {
        required: "กรุณากรอกข้อมูล !"
      }
    });
    $("#f_id3").rules("add", {
      required:true,
      messages: {
        required: "กรุณากรอกข้อมูล !"
      }
    });

                  // $("#lead_img_file").rules("add", {
                  //     required:true,
                  //     messages: {
                  //         required: " &nbsp; ไม่มีรูป !"
                  //     }
                  // });

                });
              </script>
              <script type="text/javascript">
                $("#commentForm").submit(function() {
                  var youtube_url = $("#youtube_url").val();
                  var img_youtube = $("#img_youtube").val();
                  if(youtube_url != "" && img_youtube == "" ){
                    $("#img_youtube").attr('required','true');

                  }else if(img_youtube != "" && youtube_url =="" ){
                    $("#youtube_url").attr('required','true');

                  }else if(youtube_url == "" && img_youtube == ""){
                    $("#img_youtube").removeAttr('required','true');
                    $("#youtube_url").removeAttr('required','true');
                  }
                });
              </script>
              <script type="text/javascript">
                $("#commentForm").submit(function() {
                  var thumbnail_vdo = $("#thumbnail_vdo").val();
                  var vdo_file = $("#vdo_file").val();
                  if(vdo_file != "" && thumbnail_vdo == "" ){
                    $("#thumbnail_vdo").attr('required','true');

                  }else if(thumbnail_vdo != "" && vdo_file =="" ){
                    $("#vdo_file").attr('required','true');

                  }else if(vdo_file == "" && thumbnail_vdo == ""){
                    $("#thumbnail_vdo").removeAttr('required','true');
                    $("#vdo_file").removeAttr('required','true');
                  }
                });
              </script>
              <script type="text/javascript">
                $("#commentForm").submit(function() {
                  var c_360 = $("#c_360").val();
                  var img = $("#img").val();
                  if(c_360 != "" && img == "" ){
                    $("#img").attr('required','true');

                  }else if(img != "" && c_360 =="" ){
                    $("#c_360").attr('required','true');

                  }else if(c_360 == "" && img == ""){
                    $("#img").removeAttr('required','true');
                    $("#c_360").removeAttr('required','true');
                  }
                });
              </script>

              <script>
                function isNumber(evt) {
                  evt = (evt) ? evt : window.event;
                  var charCode = (evt.which) ? evt.which : evt.keyCode;
                  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                  }
                  return true;
                }
              </script>


              <script>
              //galery
              function addProject_galery()
              {
                var index = $('.setIndexProject_galery').length;

                var form = `<div class="col-sm-3">
                <div class="form-group setIndexProject_galery">
                <label class="control-label col-md-4">
                </label>
                <div class="col-md-12">
                <input type="file" id="galery${index + 1}" name="project_galery[]" />
                </div>
                </div>
                <div class="form-group setIndexProject_galery">

                <div class="col-md-12">
                <input type="text"  class="form-control"  name="project_galery_text[]" placeholder="กรอกรายละเอียด Gallery จีน "/>
                </div>
                </div>

                <div class="form-group setIndexProject_galery hide-for-th">

                <div class="col-md-12">
                <input type="text"  class="form-control"  name="project_galery_text_en[]" placeholder="กรอกรายละเอียด Gallery อังกฤษ "/>
                </div>
                </div>
                <div class="form-group setIndexProject_galery">
                <button type="button" class="btn btn-round btn-danger" onclick="removeProject_galery(this)">
                - ลบ Gallery
                </button>
                </div>
                </div>
                <div>`;


                $('#Project_galery').append(form);

                $("#galery"+(index + 1)).fileinput({
                      uploadUrl: "upload.php", // server upload action
                      maxFileCount: 1,
                      allowedFileExtensions: ["jpg", "png", "jpeg"],
                      browseLabel: 'เลือกรูป',
                      removeLabel: 'ลบ',
                      browseClass: 'btn btn-success',
                      showUpload: false,
                      showRemove:false,
                      showCaption: false,
                      msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                      msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                      msgZoomModalHeading: 'ตัวอย่างละเอียด',
                      xxx:'upload_project_galery_seo',
                      dropZoneTitle : 'รูป Gallery แบบบ้าน',
                      maxFileSize :300 ,
                      msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                      msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                      msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                      msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                      msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                    });
              }

              function removeProject_galery(element)
              {
                $(element).parent().parent().remove();
              }

              //Model
              function addModel_galery()
              {
                var index = $('.setIndexProject_galery').length;
                var num = $('.num_model').length;

                var form = `<div class="col-sm-3">
                <div class="form-group setIndexProject_galery">
                <label class="control-label col-md-4">
                </label>
                <div class="col-md-12">
                <input type="file" id="galery_model${index + 1}" name="model_galery[]" />
                </div>
                </div>
                <div class="form-group num_model">
                <div class="col-md-2">
                <input type="radio" class="flat" value="${num + 1}" name="model_galery_radio[]" >
                </div>
                <label >
                เลือกเป็นภาพหลัก
                </label>
                </div>

                <div class="form-group setIndexProject_galery">
                <button type="button" class="btn btn-round btn-danger" onclick="removeModel_galery(this)">
                - ลบรูปภาพ
                </button>
                </div>
                </div>
                <div>`;


                $('#Model_galery').append(form);

                $("#galery_model"+(index + 1)).fileinput({
                          uploadUrl: "upload.php", // server upload action
                          maxFileCount: 1,
                          allowedFileExtensions: ["jpg", "png", "jpeg"],
                          browseLabel: 'เลือกรูป',
                          removeLabel: 'ลบ',
                          browseClass: 'btn btn-success',
                          showUpload: false,
                          showRemove:false,
                          showCaption: false,
                           minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                           minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                           maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                           maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                           msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                           msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                           msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                           msgZoomModalHeading: 'ตัวอย่างละเอียด',
                           xxx:'upload_model_galery_seo',
                           dropZoneTitle : 'รูปภาพแบบบ้าน',
                           maxFileSize :300 ,
                           msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                           msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                           msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                           msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                           msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                         });
              }

              function removeModel_galery(element)
              {
                $(element).parent().parent().remove();
              }

            </script>
            <script src="js/validate_file_300kb.js"></script>



          </body>
          </html>