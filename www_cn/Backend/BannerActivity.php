

<!--  Banner Activity-->
<h4><u>กำหนด Banner กิจกรรม</u> </h4> <span>** หากไม่ต้องการ Banner กิจกรรมพิเศษ ให้ ติกปุ่ม สร้างกิจกรรม ออก</span><br>
<div class="form-group">
    <label class="control-label col-md-2"> สร้าง กิจกรรม <span
        class="required"></span>
    </label>
    <?php 

    $check_banner = getBanner_lead_Project($_GET['project_id']);

    $checked_radio_lead_image ='';
    $checked_radio_lead_banner ='';
    $checked_radio_lead_vedio ='';

    //var_dump($check_banner);

    $banner_vdo = getBanner_vdo($_GET['project_id']);
    $banner_youtube = getBanner_youtube($_GET['project_id']);
    $banner_vdo_youtube = getBanner_vdo_youtube($_GET['project_id']);

    if($check_banner != false){
        $checked = 'checked';


        if($check_banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'image_leade_banner'){
            $checked_radio_lead_image = 'checked';
            $banner_leads = getBanner_lead_image($_GET['project_id']);
            //var_dump($banner_leads);

        }elseif ($check_banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'banner') {
            $checked_radio_lead_banner ='checked';
            $banner_activitys = getBanner_activity($_GET['project_id']);
            //var_dump($banner_activitys);

        }elseif (($check_banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'vdo') OR 
            ($check_banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'youtube')) {
            $checked_radio_lead_vedio ='checked';
            $banner_vdo = getBanner_vdo($_GET['project_id']);
            $banner_youtube = getBanner_youtube($_GET['project_id']);
            $banner_vdo_youtube = getBanner_vdo_youtube($_GET['project_id']);
        }


    }else{
        $checked = '';
        $checked_radio_lead_image ='checked';
        //var_dump($check_banner);
    }
    ?>

    <div class="col-md-6">
        <input type="checkbox" name="banner" class="flat" id="Banner_check" 
        <?=$checked;?>>
    </div>
</div>

<div class="form-group" id="type_b">
    <label class="control-label col-md-3">เลือกประเเภทของชิ้นงาน กิจกรรม<span
        class="required"></span>
    </label>
    <div class="col-md-6">
        <input type="radio" name="banner_type" id="r_lead" value="image_lead" class="flat" <?=$checked_radio_lead_image ?>>
        ภาพ &nbsp;&nbsp;
        <input type="radio" name="banner_type" id="r_banner" value="activity" class="flat"  <?=$checked_radio_lead_banner ?>>
        ภาพและข้อความ &nbsp;&nbsp;
        <input type="radio" name="banner_type" id="r_vdo" value="vdo" class="flat"  <?=$checked_radio_lead_vedio ?>>
        วีดีโอกิจจกรม 

    </div>
</div>
<br>

<div id="banner_main">
    <!--################## BANNNER LEAD ###############-->
    <!-- Banner Lead-->
    <div id="banner_lead">

        <div class="form-group" >

            <div class="row">
                <label class="control-label col-md-7" for="first-name">ตำแหน่งรูปภาพหลักของหน้า PC   สูงสุด 5 รูป  (รูปขนาด 1920 x 1080 ) ภาพขนาดไม่เกิน 300 KB<span class="required"></span></label>
            </label>
        </div>
        <br>

        <?php 
        if(isset($banner_leads)){
         if($banner_leads != false){ ?>
         <div class="row">
             <?php for($i=0; count($banner_leads) > $i; $i++){?>
             <input type="hidden" name="banner_imade_sum" id="banner_imade_sum" value="<?=count($banner_leads)?>">
             <div class="col-sm-3">
                <div class="form-group ">
                    <div class="col-md-12">
                        <div class="close fileinput-remove"></div>
                        <div class="file-drop-disabled">
                            <div class="file-preview-thumbnails">
                                <div class="file-initial-thumbs">
                                    <div class="file-preview-frame file-preview-initial" data-fileindex="init_0" data-template="image">
                                        <div class="kv-file-content">
                                            <a class="fancybox" rel="group_position_project" href="<?php echo $banner_leads[$i]->name; ?>">
                                                <img src="<?php echo $banner_leads[$i]->name; ?>" class="kv-preview-data file-preview-image"  style="width:150px;max-height:150px;min-height:150px;">
                                            </a>
                                        </div>
                                        <div class="file-thumbnail-footer">
                                            <input type="text" name="seo_lead_banner_edit[]" class="form-control" value="<?=$banner_leads[$i]->seo_lead;?>" placeholder="Alt text">
                                            <br>

                                            <input type="hidden" name="lead_image_banner_id_edit[]" value="<?=$banner_leads[$i]->id;?>">
                                            <input type="file" id="lead_img_banner_edit<?=$i;?>" name="lead_img_banner_edit[]" accept="image/*"/>

                                            <script>
                                                $("#lead_img_banner_edit<?=$i?>").fileinput({
                                                        uploadUrl: "upload.php", // server upload action
                                                        maxFileCount: 1,
                                                        allowedFileExtensions: ["jpg", "png", "jpeg"],
                                                        browseLabel: ' เลือก รูป',
                                                        browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                                                        removeLabel: 'ลบ',
                                                        browseClass: 'btn btn-success',
                                                        showUpload: false,
                                                        showRemove:false,
                                                        showCaption: false,
                                                        msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                        msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                                        msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                        xxx:'seo',
                                                        dropZoneTitle : 'รูปกิจกรรม',
                                                         minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                                         minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                                         maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                                          maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                                          maxFileSize :300 ,
                                                          msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                                          msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                                          msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                                          msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                                          msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.', 
                                                      });
                                                  </script>

                                                  <div class="file-actions">
                                                    <div class="file-footer-buttons">
                                                        <button type="button" id="<?php echo $banner_leads[$i]->id; ?>" onclick="deleteLeadImageBanner(<?php echo $banner_leads[$i]->id;?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                        <a class="fancybox" href="<?php echo $banner_leads[$i]->name; ?>" target="_blank">
                                                            <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                        </a>
                                                    </div>
                                                    <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="file-preview-status text-center text-success"></div>
                                    <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                       <div class="col-md-12">
                           <input type="text" class="form-control" name="lead_img_banner_url_edit[]" placeholder="EX : http://lh.co.th or https://lh.co.th" value="<?=$banner_leads[$i]->img_url;?>">
                       </div>
                   </div>

               </div>
               <?php }?>
           </div> 
           <br>     

           <?php
       }
   }
   ?>

   
   <div class="row">
    <div class="form-group">
        <div class="col-sm-3">
            <div class="form-group">
                <div class="col-md-12">
                    <input type="file" id="lead_img_banner0" name="lead_img_banner[]" accept="image/*"/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <input type="text" class="form-control" name="lead_img_banner_url_add[]" placeholder="EX : http://lh.co.th or https://lh.co.th" value="">
                </div>
            </div>
        </div>

        
        <div id="lead_image_banner"></div>

    </div>

</div>

<br>
<div class="form-group">
    <label class="control-label col-md-2">
    </label>
    <div class="col-md-4">
        <button id="lead_image_banner_add" type="button" class="btn btn-round btn-success" onclick="add_lead_image_banner()" >+
            เพิ่มรูปภาพหลักของ PC 
        </button>
    </div>
</div>

<script>
    $("#lead_img_banner0").fileinput({
uploadUrl: "upload.php", // server upload action
maxFileCount: 1,
allowedFileExtensions: ["jpg", "png", "jpeg"],
browseLabel: 'เลือกรูปภาพหลัก PC',
removeLabel: 'ลบ',
browseClass: 'btn btn-success',
showUpload: false,
showRemove:false,
showCaption: false,
msgFilesTooMany: 'เลือกได้ สูงสุด <b>{m}</b> ไฟล์',
msgZoomModalHeading: 'ตัวอย่างละเอียด',
xxx:'seo_lead_banner_add',
dropZoneTitle : 'รูปภาพหลัก PC',
minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
maxFileSize :300 ,
msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
});
</script>
</div>

</div>
<!--END Banner Lead -->

<!--################## BANNNER ACTIVITY ###############-->
<!-- Banner Activiti text-->

<div id="banner_text">
    <div class="form-group" >
        <label class="control-label col-md-3" for="first-name">พื้นหลัง <br> (กำหนดสีพื้นหลัง Banner) <span class="required">*</span></label>

        <div class="col-md-3">
            <input id="activitycolor" name="favcolor" type="text" class="form-control color-input" readonly p placeholder="กรุณาลือกสีพื้นหลัง" value="<?php 
            if(isset($banner_activitys->backgroup_color)){ echo $banner_activitys->backgroup_color; }else{ echo  ''; }?>">
        </div>
    </div>
    <?php 
    if(isset($banner_activitys)){
      if($banner_activitys != false){ ?>
      <div class="form-group ">
        <label class="control-label col-md-3" for="first-name">รูปภาพ Activity<span class="required">*</span> <br> สูงสุด 1 รูป   <br> (รูปขนาด 1920 x 1080 ) <br> ภาพขนาดไม่เกิน 300 KB</label>
        <div class="col-md-3">
            <div class="close fileinput-remove"></div>
            <div class="file-drop-disabled">
                <div class="file-preview-thumbnails">
                    <div class="file-initial-thumbs">
                        <div class="file-preview-frame file-preview-initial" data-fileindex="init_0" data-template="image">
                            <div class="kv-file-content">
                                <a class="fancybox" rel="group_position_project" href="<?php echo $banner_activitys->name; ?>">
                                    <img src="<?php echo $banner_activitys->name; ?>" class="kv-preview-data file-preview-image"  style="width:150px;max-height:150px;min-height:150px;">
                                </a>
                            </div>
                            <div class="file-thumbnail-footer">

                                <input type="hidden" name="banner_activity_id_edit" value="<?=$banner_activitys->id;?>">
                                <input type="file" id="banner_activity_edit<?=$i;?>" name="banner_activity_edit" accept="image/*"/>

                                <script>
                                    $("#banner_activity_edit<?=$i?>").fileinput({
                                                uploadUrl: "upload.php", // server upload action
                                                maxFileCount: 1,
                                                allowedFileExtensions: ["jpg", "png", "jpeg"],
                                                browseLabel: ' รูป Banner Activity',
                                                browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                                                removeLabel: 'ลบ',
                                                browseClass: 'btn btn-success',
                                                showUpload: false,
                                                showRemove:false,
                                                showCaption: false,
                                                msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                                msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                xxx:'seo',
                                                dropZoneTitle : 'รูปกิจกรรม',
                                                minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                                minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                                maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                                maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                                maxFileSize :300 ,
                                                msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                                msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                                msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                                msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                                msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                            });
                                        </script>

                                        <div class="file-actions">
                                            <div class="file-footer-buttons">
                                                <button type="button" id="<?php echo $banner_activitys->id; ?>" onclick="deleteLeadImageBanner(<?php echo $banner_activitys->id;?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                <a class="fancybox" href="<?php echo $banner_activitys->name; ?>" target="_blank">
                                                    <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                </a>
                                            </div>
                                            <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="file-preview-status text-center text-success"></div>
                            <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                        </div>
                    </div>
                </div>
            </div>


            <?php } }else{?>
            <div class="form-group" >
                <label class="control-label col-md-3" for="first-name">รูปภาพ Activity<span class="required">*</span> <br> สูงสุด 1 รูป   <br> (รูปขนาด 1920 x 1080 ) <br> ภาพขนาดไม่เกิน 300 KB</label>

                <div class="col-md-4">
                    <input id="banner_image" name="banner_image" type="file" multiple class="file-loading" accept="image/*" >
                </div>
            </div>

            <script>
                $("#banner_image").fileinput({
uploadUrl: "upload.php", // server upload action
maxFileCount: 1,
allowedFileExtensions: ["jpg", "png", "jpeg"],
browseLabel: 'เลือกรูป',
removeLabel: 'ลบ',
browseClass: 'btn btn-success',
showUpload: false,
showCaption: false,
showRemove: false,
msgFilesTooMany: 'เลือกได้ สูงสุด <b>{m}</b> ไฟล์',
msgZoomModalHeading: 'ตัวอย่างละเอียด',
xxx:'seo',
dropZoneTitle : 'รูป Banner Activity',
minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
maxFileSize :300 ,
msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
});
</script>

<?php } ?>

<div class="form-group" >
    <label class="control-label col-md-3" for="first-name">เนื้อหา Activity <span class="required">*</span></label>

    <div class="col-md-6">
        <textarea name="banner_text" placeholder="กรอกข้อความ banner" id="edit" class="form-control" rows="3"><?php if(!isset($check_banner->banner_text)){ echo ''; }else{  echo $check_banner->banner_text;}?></textarea>
    </div>
</div>


</div>


<!--################## BANNNER VDO ###############-->
<!-- url yputube -->
<div id="banner_vdo">
   <?php 
   $checked_youtube ='checked';
   $checked_vdo = '';
   $url_youtube = '';
   $vdo_file    = '';
   if(isset($banner_vdo_youtube)){
    
     if($banner_vdo_youtube != false){ 
        if($banner_vdo_youtube->type == 'youtube'){
         $checked_youtube = 'checked';
         $url_youtube     = $banner_vdo_youtube->name;
     }else{
         $checked_vdo = 'checked';  
         $vdo_file    = $banner_vdo_youtube->name; 

     }
 }
}
?>    

<div class="form-group upVideo" id="upVideo">      
    <label class="control-label col-md-3" for="first-name">เลือกชนิดการอัปไฟล์
        <span class="required"></span>
    </label>
    <div class="col-md-6">
        <div class="radio">
            <label><input type="radio" class="flat" name="upvido" value="youtube" id="rdo_youtube" <?=$checked_youtube ?>> VDO Youtube</label>
        </div>
        <div class="radio">
            <label><input type="radio" class="flat" name="upvido" value="vdo" id="rdo_vdo" <?=$checked_vdo ?>> VDO File</label>
        </div>
    </div>
</div>


<!--youtube -->
<div id="banner_vdo_youtube">
    <div class="form-group" >
        <label class="control-label col-md-3" for="first-name">URL VDO  <span class="required">*</span></label>

        <div class="col-md-6">
            <input id="url_youtube2" class="form-control" name="url_youtube2" type="text"  placeholder="EX : http://lh.co.th or https://lh.co.th" value="<?=$url_youtube;?>">
        </div>
    </div>

    <?php 
    if(isset($banner_youtube)){
        if( $banner_youtube != false){
            if( $banner_youtube->type == 'youtube'){
                ?> 


                <div class="form-group ">
                    <label class="control-label col-md-3" for="first-name"> Thumbnail youtube <span class="required">*</span> <br> สูงสุด 1 url   <br> (รูปขนาด 1920 x 1080 ) <br> ภาพขนาดไม่เกิน 300 KB</label>
                    <div class="col-md-3">
                        <div class="close fileinput-remove"></div>
                        <div class="file-drop-disabled">
                            <div class="file-preview-thumbnails">
                                <div class="file-initial-thumbs">
                                    <div class="file-preview-frame file-preview-initial" data-fileindex="init_0" data-template="image">
                                        <div class="kv-file-content">
                                            <a class="fancybox" rel="group_position_project" href="<?php echo  $banner_youtube->banner_img_thum; ?>">
                                                <img src="<?php echo  $banner_youtube->banner_img_thum; ?>" class="kv-preview-data file-preview-image"  style="width:150px;max-height:150px;min-height:150px;">
                                            </a>
                                        </div>
                                        <div class="file-thumbnail-footer">

                                            <input type="hidden" name="banner_youtube_id" value="<?= $banner_youtube->id;?>">
                                            <input type="file" id="banner_youtube_id_edit_<?=$i;?>" name="banner_youtube_edit" accept="image/*"/>

                                            <script>
                                                $("#banner_youtube_id_edit_<?=$i;?>").fileinput({
                                                    uploadUrl: "upload.php", // server upload action
                                                    maxFileCount: 1,
                                                    allowedFileExtensions: ["jpg", "png", "jpeg"],
                                                    browseLabel: ' เลือกรูป',
                                                    browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                                                    removeLabel: 'ลบ',
                                                    browseClass: 'btn btn-success',
                                                    showUpload: false,
                                                    showRemove:false,
                                                    showCaption: false,
                                                    msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                    msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                                    msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                    xxx:'seo',
                                                    dropZoneTitle : 'Thumbnail Youtube',
                                                    minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                                    minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                                    maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                                    maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                                    maxFileSize :300 ,
                                                    msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                                    msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                                    msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                                    msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                                    msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                                });
                                            </script>

                                            <div class="file-actions">
                                                <div class="file-footer-buttons">
                                                    <a class="fancybox" href="<?php echo  $banner_youtube->banner_img_thum; ?>" target="_blank">
                                                        <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                    </a>
                                                </div>
                                                <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="file-preview-status text-center text-success"></div>
                                <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <?php 
            }
        }

    }  
    ?>  


    <?php 

    if($banner_youtube == false){
        ?>
        <div class="form-group" >
            <label class="control-label col-md-3" for="first-name"> Thumbnail youtube <span class="required">*</span> <br> สูงสุด 1 url   <br> (รูปขนาด 1920 x 1080 ) <br> ภาพขนาดไม่เกิน 300 KB</label>

            <div class="col-md-4">
                <input id="img_youtube_banner" name="img_youtube_banner" type="file" multiple class="file-loading" accept="image/*" >
            </div>
        </div>

        <script>
            $("#img_youtube_banner").fileinput({
uploadUrl: "upload.php", // server upload action
maxFileCount: 1,
allowedFileExtensions: ["jpg", "png", "jpeg"],
browseLabel: 'เลือกรูป',
removeLabel: 'ลบ',
browseClass: 'btn btn-success',
showUpload: false,
showCaption: false,
showRemove:false,
msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
msgZoomModalHeading: 'ตัวอย่างละเอียด',
xxx:'seo',
dropZoneTitle : 'รูปภาพ Thumbnail Youtube',
minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
maxFileSize :300 ,
msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
});
</script>
<?php }?>

</div>
<!--END youtube -->

<!-- Vdo file -->
<div id="banner_vdo_file">
    <?php 
    if(isset($banner_vdo)){
      if($banner_vdo != false){
        if($banner_vdo->type == 'vdo'){
            ?> 

            <div class="form-group ">
             <label class="control-label col-md-3" for="first-name">Thumbnail VDO <span class="required">*</span> <br> สูงสุด 1 ไฟล์ <br> (รูปขนาด 1920 x 1080 ) <br> ภาพขนาดไม่เกิน 300 KB</label>
             <div class="col-md-3">
              <div class="close fileinput-remove"></div>
              <div class="file-drop-disabled">
                  <div class="file-preview-thumbnails">
                      <div class="file-initial-thumbs">
                          <div class="file-preview-frame file-preview-initial" data-fileindex="init_0" data-template="image">
                              <div class="kv-file-content">
                                  <a class="fancybox" rel="group_position_project" href="<?php echo $banner_vdo->banner_img_thum; ?>">
                                      <img src="<?php echo $banner_vdo->banner_img_thum; ?>" class="kv-preview-data file-preview-image"  style="width:150px;max-height:150px;min-height:150px;">
                                  </a>
                              </div>
                              <div class="file-thumbnail-footer">

                                  <input type="hidden" name="banner_img_vdo_id_edit" value="<?=$banner_vdo->id;?>">
                                  <input type="file" id="banner_vdo_id_edit_<?=$i;?>" name="banner_img_vdo_edit" accept="image/*"/>

                                  <script>
                                      $("#banner_vdo_id_edit_<?=$i;?>").fileinput({
                                                      uploadUrl: "upload.php", // server upload action
                                                      maxFileCount: 1,
                                                      allowedFileExtensions: ["jpg", "png", "jpeg"],
                                                      browseLabel: ' เลือกรูป',
                                                      browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                                                      removeLabel: 'ลบ',
                                                      browseClass: 'btn btn-success',
                                                      showUpload: false,
                                                      showRemove:false,
                                                      showCaption: false,
                                                      msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                      msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                                      msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                      xxx:'seo',
                                                      dropZoneTitle : 'Thumbnail VDO',
                                                      minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                                      minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                                      maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                                      maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                                      maxFileSize :300 ,
                                                      msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                                      msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                                      msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                                      msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                                      msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                                  });
                                              </script>

                                              <div class="file-actions">
                                                  <div class="file-footer-buttons">
                                                      
                                                      <a class="fancybox" href="<?php echo $banner_vdo->banner_img_thum; ?>" target="_blank">
                                                          <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                      </a>
                                                  </div>
                                                  <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                              </div>
                                          </div>
                                      </div>
                                  </div>

                                  <div class="clearfix"></div>
                                  <div class="file-preview-status text-center text-success"></div>
                                  <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                              </div>
                          </div>
                      </div>
                  </div>

                  <?php } } } ?>

                  <?php 
                  if($banner_vdo == false){
                    ?>
                    <div class="form-group" >
                        <label class="control-label col-md-3" for="first-name">Thumbnail VDO <span class="required">*</span> <br> สูงสุด 1 ไฟล์ <br> (รูปขนาด 1920 x 1080 ) <br> ภาพขนาดไม่เกิน 300 KB</label>

                        <div class="col-md-4">
                            <input id="vdo_img" name="vdo_img" type="file" multiple class="file-loading" accept="image/*" >
                        </div>
                    </div>

                    <script>
                        $("#vdo_img").fileinput({
uploadUrl: "upload.php", // server upload action
maxFileCount: 1,
allowedFileExtensions: ["jpg", "png", "jpeg"],
browseLabel: 'เลือกรูป',
removeLabel: 'ลบ',
browseClass: 'btn btn-success',
showUpload: false,
showCaption: false,
showRemove:false,
msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
msgZoomModalHeading: 'ตัวอย่างละเอียด',
xxx:'seo',
dropZoneTitle : 'รูปภาพ Thumbnail VDO',
minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
maxFileSize :300 ,
msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
});
</script>

<?php } ?>

<?php 
if(isset($banner_vdo)){
  if($banner_vdo != false){
    if($banner_vdo->type == 'vdo'){
        ?> 

        <div class="form-group ">
         <label class="control-label col-md-3" for="first-name">VDO File  <span class="required">*</span> สูงสุด 1 ไฟล์ <br> (ไฟล์ .mp4 ไม่เกิน 200 MB)</label>
         <div class="col-md-3">
          <div class="close fileinput-remove"></div>
          <div class="file-drop-disabled">
              <div class="file-preview-thumbnails">
                  <div class="file-initial-thumbs">
                      <div class="file-preview-frame file-preview-initial" data-fileindex="init_0" data-template="image">
                          <div class="kv-file-content">
                              <video width="275" controls>
                                  <source src="<?=$banner_vdo->name?>" type="video/mp4">
                                  </video>
                              </div>
                              <div class="file-thumbnail-footer">

                                  <input type="hidden" name="banner_vdo_file_id_edit" value="<?=$banner_vdo->id;?>">
                                  <input type="file" id="vdo_file_edit" name="banner_vdo_file_edit" accept="vdo/mp4"/>

                                  <script>
                                   $("#vdo_file_edit").fileinput({
                                             uploadUrl: "upload.php", // server upload action
                                             maxFileCount: 1,
                                             allowedFileExtensions: ["mp4"],
                                             browseLabel: 'เลือกไฟล์',
                                             removeLabel: 'ลบ',
                                             browseClass: 'btn btn-success',
                                             showUpload: false,
                                             showCaption: false,
                                             showRemove:false,
                                             msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                             msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                             xxx:'seo',
                                             dropZoneTitle : 'File VDO ',
                                             maxFileSize :51200 ,

                                           // minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                           // minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                           // maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                           // maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                           
                                           msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                           msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                           msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                           msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                           msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.', 
                                       });
                                   </script>

                                   <div class="file-actions">
                                      <div class="file-footer-buttons">
                                          
                                          
                                      </div>
                                      <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div class="clearfix"></div>
                      <div class="file-preview-status text-center text-success"></div>
                      <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                  </div>
              </div>
          </div>
      </div>  

      <?php } } } ?>

      <?php 
      if($banner_vdo == false){
        ?>
        <div class="form-group" >
            <label class="control-label col-md-3" for="first-name">VDO File  <span class="required">*</span> สูงสุด 1 ไฟล์ <br> (ไฟล์ .mp4 ไม่เกิน 50 MB)</label>

        </label>
        <div class="col-md-4">
            <input id="vdo_file" name="vdo_file" type="file" multiple class="file-loading" accept="video/mp4" >
        </div>
    </div>

    <script>
        $("#vdo_file").fileinput({
uploadUrl: "upload.php", // server upload action
maxFileCount: 1,
allowedFileExtensions: ["mp4"],
browseLabel: 'เลือกไฟล์',
removeLabel: 'ลบ',
browseClass: 'btn btn-success',
showUpload: false,
showCaption: false,
showRemove:false,
msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
msgZoomModalHeading: 'ตัวอย่างละเอียด',
xxx:'seo',
dropZoneTitle : 'File VDO ',
maxFileSize :51200 ,
maxFilePreviewSize : 51200,
msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.', 
});
</script>

</div>
<?php }?>
</div>

<?php 
if(isset($check_banner)){
    if($check_banner != false){
       if($check_banner->date_start){
         
         $banner_date_strat=date_create($check_banner->date_start);
         $banner_date_end=date_create($check_banner->date_end);
         
     }
 }
}
?>

<div class="form-group">
    <label class="control-label col-md-3" for="first-name">ระบุวันเริ่ม <span class="required">*</span>
    </label>
    <div class="col-md-2">
        <fieldset>
            <div class="input-prepend input-group">
                <input type="text" style="width: 180px" name="date_start"  id="date_start" class="form-control" readonly value="<?php if($check_banner != false){ echo date_format($banner_date_strat,"d/m/Y"); }else{ echo '';}?>" />

            </div>
        </fieldset>
    </div>
    <label class="control-label col-md-2" for="first-name" style="margin-left: -28px;">ระบุวันสิ้นสุด <span class="required" >*</span></label>
    <div class="col-md-2">
        <fieldset>
            <div class="input-prepend input-group">
                <input type="text" style="width: 180px;" name="date_end"  id="date_end" class="form-control " readonly  value="<?php if($check_banner != false){ echo date_format($banner_date_end,"d/m/Y"); }else{ echo '';}?>"  />
            </div>
        </fieldset>
    </div>
</div>

<!--END vod file -->



</div>








