<?php
session_start();
include './include/dbCon_mssql.php';
$_SESSION['group_id'];

$_GET['page'] = 'homemodel';
$gallery_id = $_GET['id'];

if (isset($_SESSION['add'])) {
	if ($_SESSION['add'] == 1) {
		//header("location:product_add.php");
		$success = "success";
		unset($_SESSION["add"]);
	} else if ($_SESSION['add'] == 0) {

		$error = "error"; //ค่าซ้ำ
		$_SESSION['add'] = '';

	} else if ($_SESSION['add'] == -1) {
		$errors = "errors";
		unset($_SESSION["add"]);
	} else if ($_SESSION['add'] == -2) {
		$error2 = "errors";
		unset($_SESSION["add"]);
	}
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>
      <!-- Bootstrap -->
      <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome -->
      <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <!-- NProgress -->
      <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
      <!-- iCheck -->
      <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
      <!-- bootstrap-progressbar -->
      <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
      <!-- Custom Theme Style -->
      <link href="../build/css/custom.min.css" rel="stylesheet">

      <!-- jQuery -->
      <script src="../vendors/jquery/dist/jquery.min.js"></script>

      <!-- uploade img -->
      <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
      <!--uploade-->
      <!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
      <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
       <!-- Fancybox image popup -->
      <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
      <!-- confirm-->
      <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
      <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>
      <style type="text/css">
        #seo_edit{
          display: none;
        }
      </style>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
           <?php include './master/navbar.php'?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
          <?php include './master/top_nav.php';?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><a href="master_gallery_homesell.php">ระบบจัดการรูปบ้านตกแต่งพร้อมขาย</a> > สร้าง Gallery รูปบ้านตกแต่งพร้อมขาย</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <p class="font-gray-dark">
                  </p><br>
                   <?php if (isset($success)) {?>
                   <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>สร้างข้อมูลเรียบร้อยแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                  <?php } else if (isset($error)) {?>
                    <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>มีชื่อ Folder นี้อยู่ในระบบแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                   <?php } else if (isset($error2)) {?>
                       <div class="row">
                           <div class="col-md-2"></div>
                           <div class="form-group">
                               <div class="alert alert-danger col-md-6" id="alert">
                                   <button type="button" class="close" data-dismiss="alert">x</button>
                                   <strong>ไม่สามารถลบได้ ! </strong>มีโครงการที่ผูกกับประเภทนี้อยู่
                               </div>
                           </div>
                           <div class="col-md-4"></div>
                       </div>
                  <?php }?>
                  <form class="form-horizontal form-label-left" action="save_gallery_homsell.php" method="post" id="commentForm" enctype="multipart/form-data">

                    <div class="form-group">
                      <label class="control-label col-md-2" for="first-name">เลือกโครงการ <span class="required">*</span>
                      </label>
                      <div class="col-md-6">
                       <?php
                       $sql_project = "SELECT project_name_th,project_name_en,project_id FROM LH_PROJECTS ORDER BY project_name_th asc";
                       $query_project = mssql_query($sql_project);
                       ?>
                         <select class="form-control" name="project_name_id" required>
                           <option value="">กรุณาลือกโครการ</option>
                           <?php while ($row_project= mssql_fetch_array($query_project)) { ?>
                           <option  value="<?=$row_project['project_id']?>" <?=$row_project['project_id']== $resulft['project_id'] ? 'selected' : '' ?> >
                           <?=$row_project['project_name_th'] ?></option>
                           <?php }?>
                         </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-2" for="first-name">ระบุชื่อ Folder <span class="required">*</span>
                      </label>
                      <div class="col-md-6">
                        <input type="hidden" name="galery" value="<?=$gallery_id?>">
                        <input type="hidden" name="folder_name_old" value="<?=$resulft['folder_name']?>">
                        <input type="text"  name="folder_name" required="required" class="form-control col-md-7 col-xs-12" value="<?=$resulft['folder_name']?>" placeholder="กรอกชื่อ Folder แนะนำระบุเป็นชื่ออังกฤษ">
                      </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                             <label class="control-label col-md-8">ภาพ Gallery (ขนาดรูป 1920 x 1080 px, type : .JPG , .PNG , .GIF ) ภาพขนาดไม่เกิน 300 KB </label>
                          </div>
                      </div>
                      <br>


                              <div class="row">

                              <div class="col-sm-3">
                                  <div class="form-group">
                                      <div class="col-md-12">
                                         <input type="file" id="galery0" name="project_galery[]" accept="image/*"/>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <div class="col-md-12">
                                          <input type="text"  class="form-control"  name="project_galery_text[]" placeholder="กรอกรายละเอียด Gallery (จีน)"/>
                                      </div>
                                  </div>
                                  <div class="form-group hide-for-th">
                                      <div class="col-md-12">
                                          <input type="text"  class="form-control"  name="project_galery_text_en[]" placeholder="กรอกรายละเอียด Gallery (อังกฤษ)"/>
                                      </div>
                                  </div>
                              </div>


                                   <div id="Project_galery"></div>

                          </div>

                              <br>
                              <div class="form-group">
                                  <label class="control-label col-md-1">
                                  </label>
                                  <div class="col-md-4">
                                      <input type="hidden" value="<?=$i?>" id="i" >
                                      <button id="gallery_add" type="button" class="btn btn-round btn-success" onclick="addProject_galery()" >+
                                          เพิ่มรูป Gallery
                                      </button>
                                  </div>
                              </div>

                              <script>
                                  $("#galery0").fileinput({
                                      uploadUrl: "upload.php", // server upload action
                                      maxFileCount: 1,
                                      allowedFileExtensions: ["jpg", "png", "jpeg"],
                                      browseLabel: ' Gallery บ้านตกแต่งพร้อมขาย',
                                      browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                                      removeLabel: 'ลบ',
                                      browseClass: 'btn btn-success',
                                      showUpload: false,
                                      showRemove:false,
                                      showCaption: false,
                                      msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                      msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                      msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                      xxx:'upload-project-galery-seo',
                                      dropZoneTitle : 'Gallery บ้านตกแต่งพร้อมขาย',
                                      minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                      minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                      maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                      maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                      maxFileSize :300 ,
                                      msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                      msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                      msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                      msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                      msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                  });
                              </script>



                    <center><br>
                     <div class="col-md-8">
                    <button type="submit" name="submit" class="btn btn-success">Submit</button>


                    </div>
                  </center>
                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <!-- /page content -->
            <script type="text/javascript">

            $('a.confirms').confirm({
              content: 'คุณแน่ใจที่จะลบข้อมูลนี้ !',
                buttons: {
                  Yes: {
                      text: 'Yes',
                      btnClass: 'btn-danger',
                      keys: ['enter', 'a'],
                      action: function(){
                          location.href = this.$target.attr('href');
                      }
                  },
                  No: {
                      text: 'No',
                      btnClass: 'btn-default',
                      keys: ['enter', 'a'],
                      action: function(){
                          // button action.
                      }
                  },

                }
            });
          </script>
        <!-- footer content -->
        <!-- <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer> -->
        <!-- /footer content -->
      </div>
    </div>

      <!-- Bootstrap -->
      <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
      <!-- FastClick -->
      <script src="../vendors/fastclick/lib/fastclick.js"></script>
      <!-- NProgress -->
      <script src="../vendors/nprogress/nprogress.js"></script>
      <!-- iCheck -->
      <script src="../vendors/iCheck/icheck.min.js"></script>
      <script type="text/javascript" src="js/bootstrap-imgupload.js"></script>
      <!-- Validate -->
      <script src="../build/js/jquery.validate.js"></script>

      <!-- Custom Theme Scripts -->
      <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>
      <!-- bootstrap-progressbar -->
      <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
      <!-- Custom Theme Scripts -->
      <script src="../build/js/custom.min.js"></script>
    <script>
        $(document).ready(function() {

            $("#commentForm").validate({
                rules: {
                    project_name_id: "required",
                    folder_name: "required",

                },
                messages: {
                    project_name_id: "กรุณาเลือกโครงการ!",
                    folder_name : "กรุณาระบุชื่อ Folder !",

                }
            });

            $("#lead_img_file").rules("add", {
                required:true,
                messages: {
                    required: " &nbsp; ไม่มีรูป !"
                }
            });

        });
    </script>

      <script>
      $(document).ready(function(){
        $("#alert").show();
            $("#alert").fadeTo(3000, 500).slideUp(500, function(){
              $("#alert").alert('close');
          });
      });
      </script>

    <!-- /Datatables -->
        <!-- jQuery Tags Input -->
    <script>
      //galery
                function addProject_galery()
                {


                    var index = $('.setIndexProject_galery_galley').length;

                    var form = `<div><div class="col-sm-3"><div class="form-group setIndexProject_galery">
                        <div class="col-md-12">
                    <input type="file" id="galery_${index + 1}" class="gallery_master" name="project_galery[]" accept="image/*" />
                    </div>
                    </div>

                    <div class="form-group setIndexProject_galery_galley">
                    <div class="col-md-12">
                    <input type="text"  class="form-control"  name="project_galery_text[]" placeholder="กรอกรายละเอียด Gallery (จีน)" value=""/>
                    </div>
                    </div>

                    <div class="form-group setIndexProject_galery_galley hide-for-th">
                    <div class="col-md-12">
                    <input type="text"  class="form-control"  name="project_galery_text_en[]" placeholder="กรอกรายละเอียด Gallery (อังกฤษ)" value=""/>
                    </div>
                    </div>


                    <button  type="button" class="btn btn-round btn-danger" style="margin-left: 112px;" onclick="removeProject_galery(this)"> - ลบ Gallery</button>

                    </div>
                    </div>`;



                    $('#Project_galery').append(form);

                    $(".gallery_master").fileinput({
                        uploadUrl: "upload.php", // server upload action
                        maxFileCount: 1,
                        allowedFileExtensions: ["jpg", "png", "jpeg"],
                        browseLabel: '  Gallery บ้านตกแต่งพร้อมขาย',
                        browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                        removeLabel: 'ลบ',
                        browseClass: 'btn btn-success',
                        showUpload: false,
                        showRemove:false,
                        showCaption: false,
                        msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                        msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                        msgZoomModalHeading: 'ตัวอย่างละเอียด',
                        xxx:'upload-project-galery-seo',
                        dropZoneTitle : 'Gallery บ้านพร้อมตกแต่ง',
                        minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                        minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                        maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                        maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                        maxFileSize :300 ,
                        msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                        msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                        msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                        msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                        msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                    });
                }

                function removeProject_galery(element)
                {

                    //$('#gallery_add').prop("disabled", false);
                    $(element).parent().remove();
                }

                //delete gallery
                function deleteGaleryProject(id) {
                    $.confirm({
                        title: 'ลบรูปนี้ !',
                        content: 'คุณแน่ใจที่จะลบ รูป นี้!',
                        buttons : {
                            Yes: {
                                text: 'Yes',
                                btnClass: 'btn-red any-other-class', // multiple classes.
                                action: function () {
                                    var self = this;
                                    return $.ajax({
                                        url: 'ajax_deleteImageGaleryHomesell.php?id=' + id,
                                        dataType: 'json',
                                        method: 'get'
                                    }).done(function () {
                                        //$.alert('Confirmed!');
                                        location.reload();
                                    }).fail(function () {
                                        self.setContent('Something went wrong.');
                                    });
                                }
                            },
                            No: function () {
                                //$.alert('ยกลเิก!');
                            },
                        }
                    });
                }

                function delete_gallery(id){
                   $.confirm({
                        title: 'ลบรูปนี้ !',
                        content: 'คุณแน่ใจที่จะลบ รูป Gallery นี้!',
                        buttons : {
                            Yes: {
                                text: 'Yes',
                                btnClass: 'btn-red any-other-class', // multiple classes.
                                action: function () {
                                    var self = this;
                                    return $.ajax({
                                        url: 'ajax_deleteGaleryHomesell_finish.php?id=' + id,
                                        dataType: 'json',
                                        method: 'get'
                                    }).done(function () {
                                        //$.alert('Confirmed!');
                                        location.reload();
                                    }).fail(function () {
                                        self.setContent('Something went wrong.');
                                    });
                                }
                            },
                            No: function () {
                                //$.alert('ยกลเิก!');
                            },
                        }
                    });
                }



    </script>
    <!-- /jQuery Tags Input -->
    <script type="text/javascript">
      $('.fancybox').fancybox();
    </script>
    <script src="js/validate_file_300kb.js"></script>

  </body>
</html>