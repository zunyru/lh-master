<?php
session_start();
header( 'Content-Type:text/html; charset=utf8');
if(empty($_SESSION['group_status'])){
    header("location:login.php?errors=e");
}
$url_master ="";
include './include/dbCon_mssql.php';

$_GET['page']='form';
$selectGallery='';
$_SESSION['group_id'];
if (isset($_GET['project_id'])) {
    $sql="SELECT * FROM LH_PROJECTS WHERE project_id ='".$_GET['project_id']."' ";
    $query = mssql_query($sql);
    $row = mssql_fetch_assoc($query);
}

if (isset($_GET['home_sell_id'])) {
    $sql_home_sell="SELECT h.home_sell_id,h.built_on,h.creat_date,h.features_convert,h.home_sell_status,h.house,h.image_in_home_sell_id,h.number_converter,h.plan_id,h.project_sub_id,h.soldout_update,h.update_date,p.plan_name_th,i.icon_id
,c.land_size,c.total_price,c.down,c.money,c.payments,c.contract,c.transfer_money,c.starting_pay,c.interest,c.repayment_period
FROM LH_HOME_SELL h LEFT JOIN LH_CONDITION_HOME_SELL d
                    ON h.home_sell_id = d.home_sell_id
                    LEFT JOIN LH_PLANS p ON p.plan_id = h.plan_id
                    LEFT JOIN LH_ICON_ACTIVITY i ON i.icon_id = h.icon_id
                                        LEFT JOIN LH_CONDITION_HOME_SELL c ON c.home_sell_id = h.home_sell_id
                    WHERE h.home_sell_id =   '".$_GET['home_sell_id']."' ";
    $query_home_sell = mssql_query($sql_home_sell);
    $row_home_sell = mssql_fetch_assoc($query_home_sell);
}


if (isset($_SESSION['add'])) {
    if($_SESSION['add'] ==1){

        $update="update";
        $_SESSION['add']='';

    }else{
        $error="error"; //ค่าซ้ำ
        $_SESSION['add']='';
    }
    unset($_SESSION["add"]);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>


    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <!-- Fancybox image popup -->
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
    <!-- bootstrap-progressbar -->
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <script type="text/javascript" src="js/jquery.number.js"></script>

    <script src="../build/js/custom.min.js"></script>      <script src="../build/js/custom.min.js"></script>
    <!-- uploade img -->
    <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
    <!--uploade-->
    <!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
    <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
    <!-- confirm-->
    <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
    <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>
    <?php include_once ("homefurnis/header_upload_image.php"); ?>

    <script type="text/javascript">

        $( document ).ready(function() {
           var  id_home_sell = '<?php echo $_GET['home_sell_id'];?>'
            console.log(id_home_sell);
            <?php  if($row_home_sell['house'] == 'Y'){ ?>
                $("#home").prop('checked', true);
            <?php    } else{ ?>
                $("#home").prop('checked', false);

            <?php  } ?>

            //$("#home").prop('checked', '<?php echo $row_home_sell['home_sell_status'];?>')

            $("#showPlan").val('<?php echo $row_home_sell['plan_id'];?>').change();
            var params = {
                id_plan :'<?php echo $row_home_sell['plan_id'];?>',
                id_home_sell : id_home_sell,
            };
            showPaln(params);

            $("#showIcon").val('<?php echo $row_home_sell['icon_id'];?>').change();
            <?php if(!empty($row_home_sell['icon_id'])){?>
            var params_icon = {
                id_icon :'<?php echo $row_home_sell['icon_id'];?>'
            };
            showIcon(params_icon);

            <?php } ?>

        });
    </script>
    <style>
        #upload-project-galery-seo_edit{
            display: none;
        }
    </style>

</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
                </div>

                <div class="clearfix"></div>

                <?php include "master/navbar.php"; ?>

            </div>
        </div>

        <!-- top navigation -->
        <?php include './master/top_nav.php'; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <a href="homesales.php?project_id=<?=$_GET['project_id'];?>"><h2>แก้ไขข้อมูลบ้านพร้อมขาย โครงการ : </a><?=$row['project_name_th']." (".$row['project_name_en'].")"; ?></h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <p class="font-gray-dark">
                                </p><br>


                                <?php if(isset($update)){?>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="form-group">
                                            <div class="alert alert-success col-md-6" id="alert">
                                                <button type="button" class="close" data-dismiss="alert">x</button>
                                                <strong>แก้ไขข้อมูลเรียบร้อยแล้ว</strong>
                                            </div>
                                        </div>
                                        <div class="col-md-6"></div>
                                    </div>
                                <?php } ?>
                                <?php
                                $sql_plan = "SELECT * FROM LH_PLANS ORDER BY plan_name_th ASC";
                                $query_paln = mssql_query($sql_plan);

                                ?>
                                <form class="form-horizontal form-label-left" action="update_home_sell.php" method="POST" enctype="multipart/form-data" id="form_home_sell" onsubmit="return validateForm()">
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="brand">เลือกชื่อแบบบ้าน <span class="required">*</span>
                                        </label>
                                        <div class="col-md-4 col-sm-9 col-xs-12">
                                            <select class="form-control" id="showPlan" name="plan_id">
                                                <option value="">เลือกแบบบ้าน</option>
                                                <?php while ($row_paln = mssql_fetch_assoc($query_paln)){?>
                                                    <option value="<?=$row_paln['plan_id']?>"><?=$row_paln['plan_name_th']." (".$row_paln['plan_name_en'].")";?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">หมายเลขแปลง <span class="required"></span>
                                        </label>
                                        <div class="col-md-3">
                                            <input type="text" name="number_converter" value="<?php echo $row_home_sell['number_converter'];?>"  class="form-control col-md-7 col-xs-12" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">ระบุลักษณะเด่นของแปลง <span class="required">*</span><br>
                                        <span style="font-size: 15px;">(ไม่เกิน 250 ตัวอักษร)</span>
                                        </label>
                                        <div class="col-md-4">
                                            <textarea maxlength="250"  rows="3" name="features_convert"  required class="form-control col-md-7 col-xs-12" ><?php echo $row_home_sell['features_convert'];?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">สร้างบนพื้นที่ตั้งแต่ <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3">
                                            <input type="text" name="built_on" value="<?php echo $row_home_sell['built_on'];?>"   required class="form-control col-md-7 col-xs-12"  >
                                        </div><label class="control-label col-md-1" for="first-name">ตารางวา</label>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="brand">พื้นที่ใช้สอย <span class="required">*</span>
                                        </label>
                                        <div class="col-md-2 col-sm-9 col-xs-12">
                                            <input id="use_full" class="form-control" value="">
                                        </div><label class="control-label col-md-1" for="first-name">ตารางเมตร</label>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="brand">ระบุไอคอนกิจกรรม (ถ้ามี)
                                        </label>
                                        <div class="col-md-3 col-sm-9 col-xs-12">
                                            <?php
                                            $sql="SELECT * FROM LH_ICON_ACTIVITY WHERE  CONVERT(date, getdate()) < icon_date_end ORDER BY icon_id ASC ";
                                            $query =mssql_query($sql);

                                            ?>
                                            <select class="form-control" name="icon" id="showIcon">
                                                <option value="">เลือกไอคอน</option>
                                                <?php  while ($row=mssql_fetch_array($query)){ ?>
                                                    <option value="<?=$row['icon_id']?>"><?=$row['icon_name']?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>

                                    <div id="icon"></div>
                                    <div id="room"></div>

                                    <br>

                                    <div id="func"></div>
                                    <h4>ระบุรายละเอียดเพิ่มเติม (ถ้ามี) </h4>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">
                                        </label>
                                        <div class="col-md-6">
                                            <p style="padding: 5px;">
                                                <?php
                                                $sql_p = "SELECT * FROM LH_PECULIARITY ORDER BY peculiarity_name_th ASC";
                                                $query_p = mssql_query($sql_p);
                                                while ($row_p=mssql_fetch_array($query_p)){
                                                    ?>
                                                    <input type="checkbox" name="pecul[]"  value="<?=$row_p['peculiarity_id'] ?>" class="flat checkbox_more" /> <?=$row_p['peculiarity_name_th'] ?>
                                                    <br />
                                                <?php } ?>
                                            <p>
                                        </div>

                                        <?php
                                                $sql_check = "SELECT * FROM LH_PECULIARITY_SUB_HOME_SELL WHERE home_sell_id = '".$row_home_sell['home_sell_id']."' AND plan_id = '".$row_home_sell['plan_id']."' ";
                                                $query_check = mssql_query($sql_check);
                                                while ($row_check=mssql_fetch_array($query_check)){ ?>
                                                    <script type="text/javascript">
                                                    $( document ).ready(function() {
                                                        $('input[value="<?php echo $row_check['peculiarity_id']; ?>"][type="checkbox"]').iCheck('check');
                                                    });
                                                    </script>
                                        <?php   } ?>

                                    </div>
                                    <br>

                                    <div id="series"></div>

                                    <div id="plan"></div>
                                    <!-- <label id="list_model_image-error" class="error" for="list_model_image">กรุณาเลือกแบบบ้าน !</label> -->
                                    <div id="plan_model"></div>


                                    <div id="foot"></div>

                                    <hr>

                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">ถ้าเป็นบ้านพร้อมตกแต่ง  (หากไม่มีรุปกรุณา ติกออก)
                                        </label>
                                        <div class="col-md-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input id="id" name="home" value="" type="hidden" class="" >
                                                    <?php
                                                    $ch = '';
                                                    if($row_home_sell['house'] == 'Y'){

                                                        // <!-- <script type="text/javascript">
                                                        //     $( document ).ready(function() {
                                                        //         $('#home').iCheck('check');
                                                        //     });
                                                        // </script> -->
                                                        $ch = 'checked';

                                                   }?>
                                                    <input id="home" name="home_sell" value="Y"  type="checkbox" class="" <?=$ch?>> Yes
                                                </label></div>
                                        </div>
                                    </div>
                                    <br>

                                    <?php
                                    $sql_c = "SELECT m.type_galery FROM LH_GALERY_HOME_SELL_MAIN m
                                                        LEFT JOIN LH_GALERY_FURNISHED_SUB s ON m.galery_funished_sub_id = s.galery_funished_sub_id
                                                        WHERE m.home_sell_id = '".$_GET['home_sell_id']."'";
                                    $query_c = mssql_query($sql_c);
                                    $row_c = mssql_fetch_array($query_c);
                                    //die(var_dump($row_c));
                                    $checked_2= '';
                                    $checked = '';
                                    if($row_c['type_galery']==1){
                                        $checked_2 = 'checked';
                                    }else if($row_c['type_galery']==2){
                                        $checked = 'checked';
                                    }else{

                                    }
                                    ?>

                                    <div id="show">
                                        <div class="form-group" id="inline_content">
                                            <label class="control-label col-md-2" name="tvc_detail" for="first-name">Gallery บ้านพร้อมขาย <span class="required"></span>
                                            </label>
                                            <div class="col-md-6">

                                                <div class="radio">
                                                    <label><input type="radio" id="vdo" name="optradio_vdo" <?=$checked_2;?>   value="vdo" class=""> อัปโหลดรูปเอง</label>
                                                </div>
                                                <div class="radio">
                                                    <label><input type="radio" id="youtube" name="optradio_vdo" <?=$checked;?>  value="youtube" class=""> รูปจากส่วนกลาง (Gallery แบบบ้าน)</label>
                                                </div>


                                            </div>
                                        </div>
                                    </div>

                                    <div id="master">

                                        <div class="form-group">
                                            <label class="control-label col-md-2" for="brand">เลือกรูปจากส่วนกลาง <span class="required"></span>
                                            </label>
                                            <?php
                                            $sql_gallery = "SELECT f.galery_funished_main_id,f.folder_name FROM LH_GALERY_FURNISHED_MAIN f
                                                                WHERE f.project_id =  '".$_GET['project_id']."'";
                                            $query_gallery = mssql_query($sql_gallery);

                                            ?>
                                            <div class="col-md-4 col-sm-9 col-xs-12">
                                                <select name="gallery_master" id="showgallery_master" class="form-control" >
                                                    <option value="">เลือก folder</option>
                                                    <?php
                                                    $sql_g ="SELECT * FROM LH_GALERY_FURNISHED_MAIN m
                                                            LEFT JOIN LH_GALERY_FURNISHED_SUB s
                                                            ON m.galery_funished_main_id = s.galery_funished_main_id
                                                            LEFT JOIN LH_GALERY_HOME_SELL_MAIN h
                                                            ON s.galery_funished_sub_id = h.galery_funished_sub_id
                                                            WHERE h.home_sell_id = '".$_GET['home_sell_id']."'";
                                                    $que = mssql_query($sql_g);
                                                    $roews3 = mssql_fetch_array($que);
                                                    $id_folder = '';
                                                    while ($row_gallery = mssql_fetch_array($query_gallery)){

                                                        ?>
                                                        <option  value="<?=$row_gallery['galery_funished_main_id']?>"
                                                            <?php if($roews3['galery_funished_main_id'] == $row_gallery['galery_funished_main_id']){ echo "selected"; $id_folder=$row_gallery['galery_funished_main_id']; } ?> >
                                                            <?=$row_gallery['folder_name'];?></option>
                                                    <?php }  ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div id="gallery_master"></div>


                                <?php if($row_c['type_galery']!='1'){?>
                                    <div id="galery_img2">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <input type="file" id="galery0" name="project_galery_up[]" accept="image/*"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <input type="text"  class="form-control"  name="project_galery_text[]" placeholder="กรอกรายละเอียด Gallery (จีน)"/>
                                                    </div>
                                                </div>
                                                <div class="form-group hide-for-th">
                                                    <div class="col-md-12">
                                                        <input type="text"  class="form-control"  name="project_galery_text_en[]" placeholder="กรอกรายละเอียด Gallery (อังกฤษ)"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="Project_galery"></div>
                                        </div>

                                        <br>
                                        <div class="form-group">
                                            <label class="control-label col-md-2">
                                            </label>
                                            <div class="col-md-4">
                                                <button type="button" class="btn btn-round btn-success" onclick="addProject_galery()" >+
                                                    เพิ่มรูป Galery
                                                </button>
                                            </div>
                                        </div>

                                        <script>
                                            $("#galery0").fileinput({
                                                uploadUrl: "upload.php", // server upload action
                                                maxFileCount: 1,
                                                allowedFileExtensions: ["jpg", "png", "jpeg"],
                                                browseLabel: 'เลือกรูป Gallery',
                                                removeLabel: 'ลบ',
                                                browseClass: 'btn btn-success',
                                                showUpload: false,
                                                showRemove:false,
                                                showCaption: false,
                                                msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                                                msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                                msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                xxx:'upload-project-galery-seo',
                                                dropZoneTitle : 'Gallery',
                                                maxFileSize :300 ,
                                              msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                              msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                              msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                              msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                              msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                            });
                                        </script>
                                    </div>
                                <?php }?>

                                    <div id="galery_img"></div>

                                    <?php

                                    if($row_c['type_galery']=='3'){
                                      $sql_gallery_img2 = "SELECT * FROM LH_GALERY_HOME_SELL_MAIN m
                                                        LEFT JOIN LH_GALERY_FURNISHED_SUB s ON m.galery_funished_sub_id = s.galery_funished_sub_id
                                                        WHERE m.home_sell_id = '".$_GET['home_sell_id']."'";
                                      $query_img2 = mssql_query($sql_gallery_img2);
                                       $num = mssql_num_rows($query_img2);
                                       if($num > 0){
                                    ?>
                                      <div id="glr">
                                          <hr>
                                        <div class="form-group ">
                                            <label class="control-label col-md-2" for="brand">เลือกรูปจากส่วนกลาง <span class="required"></span>
                                            </label>
                                            <div class="col-md-10">
                                                <div class="file-preview ">

                                                    <div class="close fileinput-remove"></div>
                                                    <div class="file-drop-disabled">
                                                        <div class="file-preview-thumbnails">
                                                            <div class="file-initial-thumbs">
                                                                <?php
                                                                $sql_gallery_img = "SELECT * FROM LH_GALERY_HOME_SELL_MAIN m
                                                                            LEFT JOIN LH_GALERY_FURNISHED_SUB s ON m.galery_funished_sub_id = s.galery_funished_sub_id
                                                                            WHERE m.home_sell_id = '".$_GET['home_sell_id']."' order by m.order_list ASC ";
                                                                $query_img = mssql_query($sql_gallery_img);
                                                                $img=array();
                                                                $selectGallery='';
                                                                while ($row_g = mssql_fetch_array($query_img)){
                                                                	$object = new stdClass();
                                                                	$object->hmsId =$row_g['galery_funished_sub_id'];
                                                                	$object->hmsOrder=$row_g['order_list'];

                                                                	if(!empty($selectGallery)){
                                                                		$selectGallery.=','.$row_g['galery_funished_sub_id'];
                                                                	}else
                                                                		$selectGallery=$row_g['galery_funished_sub_id'];
                                                                    array_push($img,$object);
                                                                }
                                                                //die(var_dump($img));
                                                                $sql_loop ="SELECT * FROM LH_GALERY_FURNISHED_MAIN m
                                                                            LEFT JOIN LH_GALERY_FURNISHED_SUB s
                                                                            ON m.galery_funished_main_id = s.galery_funished_main_id
                                                                            WHERE m.galery_funished_main_id = '".$id_folder."' ";
                                                                $query_img_loop = mssql_query($sql_loop);
                                                                 $i=0;
                                                                while ($row_gallery = mssql_fetch_array($query_img_loop)){
                                                                	//die(var_dump($row_gallery));
                                                                ?>

                                                                <div class="file-preview-frame file-preview-initial" data-fileindex="init_0" data-template="image">
                                                                    <div class="kv-file-content">
                                                                        <a class="fancybox" rel="group_position_project" href="<?php echo $row_gallery['path_funished']; ?>">
                                                                            <img src="<?php echo $row_gallery['path_funished']; ?>" class="kv-preview-data file-preview-image"  style="width:150px;max-height:150px;">
                                                                        </a>
                                                                    </div>
                                                                    <div class="file-thumbnail-footer">

                                                                        <p>Alt Text : <?php $row_gallery['alt_seo']?> </p>

                                                                            <input type="checkbox" name="gallpo" value="<?=$row_gallery['galery_funished_sub_id']?>" >

                                                                            <div name="dispos<?=$row_gallery['galery_funished_sub_id']?>" class="showposition"></div>
                                                                            <div class="file-actions">
                                                                                <div class="file-footer-buttons">

                                                                                    <a class="fancybox" href="<?php echo $row_gallery['path_funished']; ?>" target="_blank">
                                                                                        <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                                                    </a>
                                                                                </div>
                                                                                <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                                                            </div>

                                                                    </div>
                                                                </div>
                                                                <?php $i++; }?>
                                                            </div>
                                                        </div>

                                                        <div class="clearfix"></div>
                                                        <div class="file-preview-status text-center text-success"></div>
                                                        <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                      </div>
                                       <?php }
                                     }elseif($row_c['type_galery']=='1'){ ?>
                                   <div id="gallery_up">
                                       <div class="row">

                                           <h4><u>ตำแหน่งภาพ Gallery</u></h4>
                                           <label class="control-label col-md-2" for="first-name"></label>
                                       </div>
                                    <div class="row">

                                        <?php
                                        $i=0;
                                        $sql_lead = "SELECT u.path_upload_gallery,u.dis_th,u.dis_en,m.galery_home_sell_id,u.uploade_galery_id,u.alt_seo FROM LH_GALERY_HOME_SELL_MAIN m
                                                    RIGHT JOIN LH_UPLOAD_GALERY_HOME_SELL u ON m.galery_home_sell_id = u.galery_home_sell_id
                                                    WHERE m.type_galery = 1 AND m.home_sell_id = '".$_GET['home_sell_id']."'";
                                        $query_lead = mssql_query($sql_lead);
                                        while ($value_l = mssql_fetch_array($query_lead)) {
                                            $i++;
                                            ?>

                                            <div class="col-sm-3">
                                                <div class="form-group ">
                                                    <div class="col-md-12">
<!--                                                        <div class="file-preview ">-->
                                                            <div class="close fileinput-remove"></div>
                                                            <div class="file-drop-disabled">
                                                                <div class="file-preview-thumbnails">
                                                                    <div class="file-initial-thumbs">
                                                                        <div class="file-preview-frame file-preview-initial" data-fileindex="init_0" data-template="image">
                                                                            <div class="kv-file-content">
                                                                                <a class="fancybox" rel="group_position_project" href="<?php echo $value_l['path_upload_gallery']; ?>">
                                                                                    <img src="<?php echo $value_l['path_upload_gallery']; ?>" class="kv-preview-data file-preview-image"  style="width:150px;max-height:150px;min-height:150px;">
                                                                                </a>
                                                                            </div>
                                                                            <div class="file-thumbnail-footer">
                                                                                <input type="hidden" name="uploade_galery_id[]" value="<?php echo $value_l['uploade_galery_id']; ?>">
                                                                                <input type="file" id="galery_<?php echo $value_l['uploade_galery_id']; ?>" name="project_galery_edit[]" accept="image/*"/>

                                                                                <script>
                                                                                    $("#galery_<?php echo $value_l['uploade_galery_id']; ?>").fileinput({
                                                                                        uploadUrl: "upload.php", // server upload action
                                                                                        maxFileCount: 1,
                                                                                        allowedFileExtensions: ["jpg", "png", "jpeg"],
                                                                                        browseLabel: 'เลือกรูป Gallery',
                                                                                        removeLabel: 'ลบ',
                                                                                        browseClass: 'btn btn-success',
                                                                                        showUpload: false,
                                                                                        showRemove:false,
                                                                                        showCaption: false,
                                                                                        msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                                                        msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                                                                                        msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                                                                        msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                                                        xxx:'upload-project-galery-seo_edit',
                                                                                        dropZoneTitle : 'Gallery ',
                                                                                        maxFileSize :300 ,
                                                                                      msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                                                                      msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                                                                      msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                                                                      msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                                                                      msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                                                                    });
                                                                                </script>

                                                                                <span>Alt Text :</span>
                                                                                <input type="text" name="upload_plan_galery_seo_edit[]" class="form-control" value="<?=$value_l['alt_seo']?>" placeholder="ALT">
                                                                                <span><?="ลำดับที่ ".$i;?></span>
                                                                                <div class="file-actions">
                                                                                    <div class="file-footer-buttons">

                                                                                        <a class="fancybox" href="<?php echo $value_l['path_upload_gallery']; ?>" target="_blank">
                                                                                            <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                                                        </a>
                                                                                    </div>
                                                                                    <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
<!--                                                                </div>-->

                                                                <div class="clearfix"></div>
                                                                <div class="file-preview-status text-center text-success"></div>
                                                                <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <input type="hidden" name="gallery_id[]" value="<?php echo $value_l['uploade_galery_id']; ?>">
                                                        <input type="text"  class="form-control"  name="project_galery_text_edit[]" placeholder="กรอกรายละเอียด Gallery (จีน)"
                                                               value="<?=$value_l['dis_th'] == ' '? '' : $value_l['dis_th'] ?>"/>
                                                    </div>
                                                </div>
                                                <div class="form-group hide-for-th">
                                                    <div class="col-md-12">
                                                        <input type="text"  class="form-control"  name="project_galery_text_en_edit[]" placeholder="กรอกรายละเอียด Gallery (อังกฤษ)"
                                                               value="<?=$value_l['dis_en']==' '? '' : $value_l['dis_en'];?>"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                       <button type="button" id="<?php echo $value_l['uploade_galery_id']; ?>" onclick="deleteLeadImageProject(<?php echo $value_l['uploade_galery_id'];?>)" class="btn btn-round btn-danger" title="Remove file" data-url="" data-key="1">ลบ Gallery</button>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                        <div id="galery_img2">
                                            <br>
                                          <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <input type="file" id="galery0" name="project_galery[]" accept="image/*"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <input type="text"  class="form-control"  name="project_galery_text[]" placeholder="กรอกรายละเอียด Gallery (จีน)"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <input type="text"  class="form-control"  name="project_galery_text_en[]" placeholder="กรอกรายละเอียด Gallery (อังกฤษ)"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="Project_galery"></div>
                                          </div>

                                            <br>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">
                                                </label>
                                                <div class="col-md-4">
                                                    <button type="button" class="btn btn-round btn-success" onclick="addProject_galery()" >+
                                                        เพิ่มรูป Galery
                                                    </button>
                                                </div>
                                            </div>

                                            <script>
                                                $("#galery0").fileinput({
                                                    uploadUrl: "upload.php", // server upload action
                                                    maxFileCount: 1,
                                                    allowedFileExtensions: ["jpg", "png", "jpeg"],
                                                    browseLabel: 'เลือกรูป Gallery',
                                                    removeLabel: 'ลบ',
                                                    browseClass: 'btn btn-success',
                                                    showUpload: false,
                                                    showRemove:false,
                                                    showCaption: false,
                                                    msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                    msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                                                    msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                                    msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                    xxx:'upload_plan_galery_seo',
                                                    dropZoneTitle : 'รูป Gallery',
                                                    maxFileSize :300 ,
                                                  msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                                  msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                                  msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                                  msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                                  msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                                });
                                            </script>
                                        </div>
                                   </div>
                                  <?php  } ?>


                                    <hr>
                                    <br>
                                    <h4>ตารางราคาและเงื่อนไขการผ่อน</h4>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">ขนาดที่ดิน <span class="required">*</span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" id="land_size" name="land_size"  required class="form-control col-md-7 col-xs-12"  value="<?php echo $row_home_sell['land_size'];?>">
                                        </div><label class="control-label col-md-1 textleft" for="first-name">ตารางวา
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">ราคารวมสุทธิ <span class="required">*</span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" required id="total_price" name="total_price" class="form-control col-md-7 col-xs-12" value="<?php echo $row_home_sell['total_price'];?>">
                                        </div><label class="control-label col-md-1 textleft" for="first-name">บาท </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">ดาวน์
                                        </label> <div class="col-md-2">
                                            <input type="text"  id="down"  name="down" class="form-control col-md-7 col-xs-12" placeholder="" value="<?php echo $row_home_sell['down'];?>" onkeypress="return isNumber(event)">
                                        </div>
                                        <label class="control-label col-md-1 textleft" for="first-name"> % เป็นเงิน
                                        </label>
                                        <div class="col-md-2">
                                            <input type="text"  id="money" name="money"   class="form-control col-md-7 col-xs-12" placeholder="" value="<?php echo $row_home_sell['money'];?>">
                                        </div><label class="control-label col-md-1 textleft" for="first-name"> บาท<span class="required"></span>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">เงินจอง
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text"  id="payments" name="payments"   class="form-control col-md-7 col-xs-12" value="<?php echo $row_home_sell['payments'];?>" >
                                        </div><label class="control-label col-md-1 textleft" for="first-name">บาท </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">ทำสัญญา
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text"   id="contract" name="contract"  class="form-control col-md-7 col-xs-12"  value="<?php echo $row_home_sell['contract'];?>">
                                        </div><label class="control-label col-md-1 textleft" for="first-name">บาท </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">เงินโอน * <span class="required"></span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" required id="transfer_money" name="transfer_money"  class="form-control col-md-7 col-xs-12"  value="<?php echo $row_home_sell['transfer_money'];?>">
                                        </div><label class="control-label col-md-1 textleft" for="first-name">บาท </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">ผ่อนเริ่มต้น *
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" required id="starting_pay" name="starting_pay"  class="form-control col-md-7 col-xs-12"  value="<?php echo $row_home_sell['starting_pay'];?>">
                                        </div><label class="control-label col-md-1 textleft" for="first-name">ต่อเดือน </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">ดอกเบี้ย *
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text"  required id="interest" name="interest"  class="form-control col-md-7 col-xs-12"  value="<?php echo $row_home_sell['interest'];?>" onkeypress="return isNumber(event)">
                                        </div><label class="control-label col-md-1 textleft" for="first-name">% </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">ระยะเวลาผ่อนชำระ *
                                        </label>
                                        <div class="col-md-2">
                                            <input name="id" type="hidden" value="<?=$_GET['project_id']?>">
                                            <input id="home_sell_id" name="home_sell_id" type="hidden" value="<?=$_GET['home_sell_id']?>">
                                            <input type="text" required name="repayment_period" maxlength="2"  class="form-control col-md-7 col-xs-12"  value="<?php echo $row_home_sell['repayment_period'];?>">
                                        </div><label class="control-label col-md-1 textleft" for="first-name">ปี </label>
                                    </div>
                                    <br><br>

                                    <div align="center">
                                        <div class="col-md-8">
                                        <?php
                                          $sql_url ="SELECT * FROM LH_PROJECTS p
                                                        LEFT JOIN LH_PROJECT_SUB s ON p.project_id = s.project_id
                                                        WHERE p.project_id = '".$_GET['project_id']."'";
                                          $q_url = mssql_query($sql_url);
                                          $url_array = mssql_fetch_array($q_url);
                                          if( $url_array['product_id']==2){
                                            $type = 'townhome/project/';
                                          }else{
                                            $type = 'singlehome/project/';
                                          }

                                        ?>
                                        <!-- th/singlehome/project/นันทวัน-ปิ่นเกล้า-ราชพฤกษ์/1/homesell/Flourish/126 -->
                                            <?php  $url=$url_master.'th/'.$type.str_replace(" ", "-", $url_array['project_name_th']).'/'.$url_array['product_id'].'/homesell/'.str_replace(" ", "-", $row_home_sell['plan_name_th'])."/".$_GET['home_sell_id'];?>
                                          <input type="hidden" name="selectGallery" value="<?php echo $selectGallery; ?>">
                                        <a href="<?=$url?>" target="_blank"><button type="button" class="btn btn-warning">Preview</button></a>
                                        <a class="confirms" data-title="ยืนยันการลบข้อมูล" name="delete" href="delete_homesale.php?id=<?=$_GET['home_sell_id']?>&project_id=<?=$_GET['project_id']?>">
                                            <button type="button"  class="btn btn-danger">Delete</button></a>
                                            <button type="submit" class="btn btn-success">Update</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <?php if(isset($error)){?>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="form-group">
                                            <div class="alert alert-danger col-md-6" id="alert">
                                                <button type="button" class="close" data-dismiss="alert">x</button>
                                               <strong> ไม่สามารถบันทึกข้อมูลได้ ถ้าระบุเป็นบ้านพร้อมตกแต่ง กรุณาเลือกรูปภาพในกล่องโฟลเดอร์รูป <u>Gallery</u> ถ้าไม่มีกรุณาเอาเครื่องหมายถูกออกค่ะ (แจ้งเพิ่มรูปได้ที่ฝ่ายโคโค่)</strong>
                                            </div>
                                        </div>
                                        <div class="col-md-6"></div>
                                    </div>
                                <?php } ?>

                </div>
                <!-- /page content -->

                <!-- footer content -->
                <!-- <footer>
                  <div class="pull-right">
                    Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
                  </div>
                  <div class="clearfix"></div>
                </footer> -->
                <!-- /footer content -->
            </div>
        </div>


        <!-- Bootstrap -->
        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../vendors/nprogress/nprogress.js"></script>

        <script type="text/javascript" src="js/bootstrap-imgupload.js"></script>
        <!-- Validate -->
        <script src="../build/js/jquery.validate.js"></script>
        <!-- iCheck -->
        <script src="../vendors/iCheck/icheck.min.js"></script>

        <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>

        <script src="js/ajax_paln_master.js"></script>

        <!-- bootstrap-progressbar -->
        <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>

        <script type="text/javascript" src="js/jquery.number.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="../build/js/custom.min.js"></script>
        <!-- Fancybox image popup -->
        <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>

        <script type="text/javascript" src="js/jquery.number.js"></script>

        <script type="text/javascript">
            $('.fancybox').fancybox();
        </script>

        <!-- Validate -->
        <script src="../build/js/jquery.validate.js"></script>

        <script>
            $(function() {
                // Set up the number formatting.
                $('#total_price').number(true, 0);
                //$('#down').number(true, 0);
                $('#money').number(true, 0);
                $('#payments').number(true, 0);
                $('#contract').number(true, 0);
                $('#transfer_money').number(true, 0);
                $('#starting_pay').number(true, 0);
                //$('#interest').number(true, 0);
                $('#repayment_period').number(true, 0);
                $('#land_size').number(true, 0);

            });
        </script>

        <script type="text/javascript">

            $('a.confirms').confirm({
                content: 'คุณแน่ใจที่จะลบข้อมูลนี้ !',
                buttons: {
                    Yes: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        keys: ['enter', 'a'],
                        action: function(){
                            location.href = this.$target.attr('href');
                        }
                    },
                    No: {
                        text: 'No',
                        btnClass: 'btn-default',
                        keys: ['enter', 'a'],
                        action: function(){
                            // button action.
                        }
                    },

                }
            });
        </script>

        <script>
            $(document).ready(function() {

                $("#form_home_sell").validate({
                    rules: {
                        plan_id: "required",
                        plan_1: "required",
                        plan_2 : "required",
                        //number_converter : "required",
                        features_convert : "required",
                        built_on : "required",
                        land_size : "required",
                        total_price : "required",
                        //down :"required",
                        //money : "required",
                        //payments : "required",
                        "list_model_image[]" : "required",
                        starting_pay : "required",
                        interest : "required",
                        repayment_period : "required",
                        transfer_money: "required",
                    },
                    messages: {
                        plan_id : "เลือกอย่างน้อย 1 รายการ !",
                        plan_1 : "ระบุ หมายเลขแปลง !",
                        "list_model_image[]" : "กรุณาเลือกรูปแแบบบ้าน !",
                        //number_converter : "ระบุ หมายเลขแปลง !",
                        features_convert : "ระบุ ลักษณะเด่นของแปลง !",
                        built_on : "ระบุ ขนาดสร้างบนพื้นที่ !",
                        land_size : "ระบุ ขนาดที่ดิน !",
                        total_price : "ระบุ ราคารวมสุทธิ !",
                        down : {
                            required : "ระบุ ดาวน์ ! ",
                            min : "กรุณากรอกค่า มากว่า 0 !",
                            max : "กรุณากรอกค่า ไม่เกิน 100 ! ",
                        },
                        money : "ระบุยอดเงิน !",
                        payments : "ระบุ เงินจอง !",
                        contract : "ระบุ ทำสัญญา !",
                        transfer_money : "ระบุ เงินโอน !",
                        starting_pay : "ระบุ ผ่อนเริ่มต้น ! ",
                        interest : {
                            required : "ระบุ ดอกเบี้ย ! ",
                            min : "กรุณากรอกค่า มากว่า 0 !",
                            max : "กรุณากรอกค่า ไม่เกิน 100 ! ",
                        },
                        repayment_period : "ระบุระยะเวลาผ่อนชำระ !",

                    }
                });

            });
        </script>
        <script>
            //galery
            function addProject_galery() {
                var index = $('.setIndexProject_galery_galley').length;

                var form = `<div><div class="col-sm-3"><div class="form-group setIndexProject_galery">
                        <div class="col-md-12">
                    <input type="file" id="galery_${index + 1}" class="gallery_master" name="project_galery_up[]" accept="image/*" />
                    </div>
                    </div>

                    <div class="form-group setIndexProject_galery_galley">
                    <div class="col-md-12">
                    <input type="text"  class="form-control"  name="project_galery_text[]" placeholder="กรอกรายละเอียด Gallery (จีน)" value=""/>
                    </div>
                    </div>

                    <div class="form-group setIndexProject_galery_galley">
                    <div class="col-md-12">
                    <input type="text"  class="form-control"  name="project_galery_text_en[]" placeholder="กรอกรายละเอียด Gallery (อังกฤษ)" value=""/>
                    </div>
                    </div>


                    <button  type="button" class="btn btn-round btn-danger" style="margin-left: 112px;" onclick="removeProject_galery(this)"> - ลบ Gallery</button>

                    </div>
                    </div>`;


                $('#Project_galery').append(form);

                $("#galery_"+(index + 1)).fileinput({
                    uploadUrl: "upload.php", // server upload action
                    maxFileCount: 1,
                    allowedFileExtensions: ["jpg", "png", "jpeg"],
                    browseLabel: 'เลือกรูป Gallery',
                    removeLabel: 'ลบ',
                    browseClass: 'btn btn-success',
                    showUpload: false,
                    showRemove:false,
                    showCaption: false,
                    msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                    msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                    msgZoomModalHeading: 'ตัวอย่างละเอียด',
                    xxx:'upload_plan_galery_seo',
                    dropZoneTitle : 'Gallery',
                    msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                    msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                    msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                    msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                });


            }


            function removeProject_galery(element)
            {
                $(element).parent().parent().remove();
            }
        </script>

        <script>
            $(document).ready(function(){
                //$("#galery_img").show();
                $("#galery_img2").show();
                <?php if($row_home_sell['house']== 'Y'){ ?>
                    $("#galery_img").hide();
                    $("#galery_img2").hide();
                    $('#check_home').hide();
                    $("#galery_img2").hide();
                    $("#show").hide();
                    $("#glr").show();
                    $('#master').show();
                    $("#gallery_master").show();
                <?php }else{?>

                    $('#master').hide();
                    $("#galery_img").show();
                    $("#galery_img2").show();
                    $('#check_home').show();
                    $("#galery_img2").show();
                    $("#glr").hide();

                <?php }?>

                <?php
                  if($row_c['type_galery']=='2'){ ?>
                      //$("#galery_img").hide();
                      $("#galery_img2").hide();
                 <?php
                  }else{ ?>
                     $("#galery_img").hide();
                     $("#gallery_up").show();
                     $("#vdo").prop("checked", true);
          <?php   }
                ?>
            });

            $('input:radio[name="optradio_vdo"]').change(function() {
                if ($(this).val() == 'youtube') {
                    $("#galery_img").show();
                    $("#gallery_up").hide();
                    $("#galery_img2").hide();
                    $("#showPlan").trigger('change');
                    $("#gallery_up").hide();

                } else {
                    $("#galery_img").hide();
                    $("#galery_img2").show();
                    $("#gallery_up").show();


                }
            });
            $('input:radio[name="optradio_vdo"]').trigger('change');

            $('#home').change(function () {
                if(this.checked) {
                    $("#galery_img").hide();
                    $("#galery_img2").hide();
                    $('#check_home').hide();
                    $("#show").hide();
                    $('#master').show();
                    $("#glr").show();
                    $("#gallery_master").show();
                }else{
                    var opradio = $('input:radio[name="optradio_vdo"]').val();
                    if(opradio =='vdo'){
                        $("#vdo").prop("checked", true);
                    }else{
                        $("#youtube").prop("checked", true);
                    }
                    $("#showPlan").trigger('change');
                    $("#show").show();
                    $('#check_home').show();
                    $('#master').hide();
                    $("#glr").hide();
                    $("#gallery_master").hide();
                    $("#galery_img2").show();
                    $("galery_img").show();

                }
            });
        </script>

        <script>
            $(document).ready(function(){
                $("#alert").show();
                $("#alert").fadeTo(6000, 500).slideUp(500, function(){
                    $("#alert").alert('close');
                });
            });
        </script>

        <script>
            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 46 || charCode > 58)|| (charCode == 47)) {
                    return false;
                }
                return true;
            }

             function validateForm() {

                   var selectGallery = $("input[name=selectGallery]");
                   var ch = $('#home').prop('checked');

                    if (($('#home').prop('checked') == true) && ($("#showgallery_master").val()=='')) {
                         $.alert({
                            title: '**หมายเหตุ',
                            content: 'ถ้าระบุเป็นบ้านพร้อมตกแต่ง กรุณาเลือกรูปภาพในกล่องโฟลเดอร์รูป <u>Gallery</u> ถ้าไม่มีกรุณาเอาเครื่องหมายถูกออกค่ะ (แจ้งเพิ่มรูปได้ที่ฝ่ายโคโค่)',
                        });
                         return false;

                    }else{

                        if( (selectGallery.val().length < 1)&&($('#home').prop('checked') == true)){
                        $.alert({
                            title: '**หมายเหตุ',
                            content: 'ถ้าระบุเป็นบ้านพร้อมขาย กรุณาเลือกรูปภาพในรูป <u>Gallery</u> ถ้าไม่มีกรุณาเอาเครื่องหมายถูกออกค่ะ (แจ้งเพิ่มรูปได้ที่ฝ่ายโคโค่',
                        });
                         return false;
                     }
                    }
                }
        </script>

        <script>
            function deleteLeadImageProject($id) {
                $.confirm({
                    title: 'ลบรายการนี้ !',
                    content: 'คุณแน่ใจที่จะรูปนี้!',
                    buttons : {
                        Yes: {
                            text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                                var self = this;
                                return $.ajax({
                                    url: 'ajax_deleteGalleyHomesell.php?id=' + $id,
                                    dataType: 'json',
                                    method: 'get'
                                }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                }).fail(function () {
                                    self.setContent('Something went wrong.');
                                });
                            }
                        },
                        No: function () {
                            //$.alert('ยกลเิก!');
                        },
                    }
                });

            }
        </script>

</body>
</html>