<?php
require_once 'include/dbConnect.php';

    try {
        $icon_id = $_GET['icon_id'];

        $conn = (new dbConnect())->getConn();
        $sql = "UPDATE LH_ICON_ACTIVITY SET icon_img = ''
                WHERE icon_id =".$icon_id;
        $result= $conn->query($sql);

        echo json_encode($result->fetchAll());

    } catch (\Exception $e) {
        return $e->getMessage();
    }
?>