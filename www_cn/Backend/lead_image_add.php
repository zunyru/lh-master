<?php 
session_start();
include './include/dbCon_mssql.php';
$group_id=$_SESSION['group_id'];
$zone_id=$_GET['id'];

$_GET['page']='lead_image';

if(isset($_GET['error'])){
    $error2="0";
}
if (isset($_GET['add'])) {  
           if($_GET['add'] ==1){   
             //header("location:product_add.php");
            $success ="success";  
          } else if($_GET['add'] ==0){  
              
            $error="error"; //ค่าซ้ำ
        
          } else if($_GET['add'] ==-1){ 
            $error="errors";
          }
      } 
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>

    <!-- uploade img -->
    <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
     <!--uploade-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
   

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    
    <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap-imgupload.min.css">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
   <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> -->
     <script>
      $(document).ready(function(){
        $("#alert").show();
            $("#alert").fadeTo(3000, 400).slideUp(500, function(){
              $("#alert").alert('close');
          });
      });
      </script>
    <style type="text/css">
      .thumbnail {
          height: 100px;
          margin: 5px;
      }
      .fileUpload {
        position: relative;
        overflow: hidden;
        //margin: 10px;
      }
      .fileUpload input.upload {
          position: absolute;
          top: 0;
          right: 0;
          margin: 0;
          padding: 0;
          font-size: 20px;
          cursor: pointer;
          opacity: 0;
          filter: alpha(opacity=0);
      }
      .file-drop-zone-title {
            padding: 0px 0px !important;
        }
    </style>
    
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
           <?php include './master/navbar.php'; ?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
          <?php include './master/top_nav.php'; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><a href="lead_image.php">ระบบจัดการข้อมูล Banner</a> > สร้างข้อมูล Banner</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <p class="font-gray-dark"> 
                  </p><br>
                   <?php if(isset($success)){?>
                   <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>สร้างข้อมูลเรียบร้อย</strong>  
                      </div>
                    </div>
                    <div class="col-md-4"></div> 
                   </div> 
                  <?php }else if(isset($error)){?>
                    <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>มีชื่อนี้อยู่ในระบบแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div> 
                   </div> 
                  <?php }?>
                  <?php if(isset($error2)){?>
                    <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>ผิดพลาด !</strong> กรุณาใส่ชิ้นงาน
                      </div>
                    </div>
                    <div class="col-md-4"></div> 
                   </div> 
                  <?php }?>
                  <form class="form-horizontal form-label-left" action="save_lead_img.php" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                      <label class="control-label col-md-2" for="first-name">ระบุชื่อชิ้นงาน <span class="required">*</span>
                      </label>
                      <div class="col-md-6">
                        <input type="text" id="first-name2" name="lead_img_zero_name" required="required" class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อชิ้นงาน" value=""> 
                      </div>
                    </div>

                    <script>
                      $(document).ready(function(){
                          $("#l1").show();
                          $("#l2").hide();
                          $("#l3").hide();
                      });
                    </script>

                    <script>

                          function getval(sel){
                             $("#mySelect").val();
                              if($("#mySelect").val() == "image"){
                                $("#l1").show();
                                $("#l2").hide();
                                $("#l3").hide();
                              }else if($("#mySelect").val() == "banner"){
                                $("#l2").show();
                                $("#l3").hide();
                                $("#l1").hide();
                              }else if($("#mySelect").val() == "vdo"){
                                $("#l3").show();
                                $("#l1").hide();
                                $("#l2").hide();
                              }       
                          }
                      </script>

                    <div class="form-group">
                      <label class="control-label col-md-2" for="first-name">ระบุชิ้นงาน(Banner) <span class="required">*</span>
                      </label>
                      <div class="col-md-6">
                        <select id="mySelect" name="category_file" class="form-control" name="peview_type" onchange="getval(this);">
                          <option value="image" selected>รูป Banner </option>
                          <option value="banner">Banner Text</option>
                          <option value="vdo">VDO Banner </option>
                        </select>
                      </div>
                    </div>

                    <!-- url yputube -->
                    <div class="form-group" id="l1">
                      <div class="row">
                          <label class="control-label col-md-2" for="first-name"> </label>
                          <label class="control-label col-md-4" for="first-name">เลือกรูป Banner </label>
                      </div>
                      <label class="control-label col-md-2" for="first-name">
                      </label>
                      <div class="col-md-6"> 
                          <input id="lead_img_file" name="lead_img_file" type="file" multiple class="file-loading" accept="image/*" >
                      </div>
                    </div>

                     <script>
                        $("#lead_img_file").fileinput({
                            uploadUrl: "upload.php", // server upload action
                            maxFileCount: 1,
                            allowedFileExtensions: ["jpg", "gif", "png", "jpeg","mp4"],
                            browseLabel: 'เลือกรูป',
                            removeLabel: 'ลบ',
                            browseClass: 'btn btn-success',
                            showUpload: false,
                            msgFilesTooMany: 'ไฟล์ที่คุณเลือกมีจำนวน <b>({n})</b> ซึ่งเกินกว่าที่ระบบอนุญาตที่ <b>{m}</b>, กรุณาลองใหม่อีกครั้ง!',
                            msgZoomModalHeading: 'ตัวอย่างละเอียด',
                            xxx:'seo_lead_img_file',
                            dropZoneTitle : 'ลากและวางรูป',
                            minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                            minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                            maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                            maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                            msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                            msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                            msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                            msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                            });
                        </script>
                        <!-- Banner -->
                      <div id="l2" >
                        <div class="form-group" >
                          <div class="row">
                            <label class="control-label col-md-2" for="first-name"> </label>
                            <label class="control-label col-md-4" for="first-name">เลือก Banner Leade Image </label>
                          </div>
                          <label class="control-label col-md-2" for="first-name"></label>
                           
                          <div class="col-md-6" >
                              <input id="banner" name="lead_baner_file" type="file" multiple class="file-loading" accept="image/*" >
                          </div>
                        </div>

                        <div class="form-group" >
                          <label class="control-label col-md-2" for="first-name"></label>
                           
                          <div class="col-md-6" >
                              <input id="banner" name="content_banner" type="text" class="form-control" value="" placeholder="ใส่ข้อความ Banner">
                          </div>

                        </div>
                      </div>

                         <script>
                            $("#banner").fileinput({
                                uploadUrl: "upload.php", // server upload action
                                maxFileCount: 1,
                                allowedFileExtensions: ["jpg", "gif", "png", "jpeg"],
                                browseLabel: 'เลือก Banner',
                                removeLabel: 'ลบ',
                                browseClass: 'btn btn-success',
                                showUpload: false,
                                msgFilesTooMany: 'ไฟล์ที่คุณเลือกมีจำนวน <b>({n})</b> ซึ่งเกินกว่าที่ระบบอนุญาตที่ <b>{m}</b>, กรุณาลองใหม่อีกครั้ง!',
                                msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                xxx:'seo_lead_baner_file',
                                dropZoneTitle : 'ลากและวาง Banner',
                                minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                                msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                                });
                            </script>

                    <!-- url yputube -->
                    <div class="form-group" id="l3">
                      <div class="row">
                            <label class="control-label col-md-2" for="first-name"> </label>
                            <label class="control-label col-md-3" for="first-name">URL VDO Banner</label>
                          </div>
                      <label class="control-label col-md-2" for="first-name">
                      </label>
                      
                      <div class="col-md-6">
                          <input id="url_youtube" class="form-control" name="url_youtube" type="text"  placeholder="URL Youtube" >
                      </div>
                    </div>

                    <div id="myContainer">
                    <div class="form-group">
                      <label class="control-label col-md-2" for="first-name">เลือกตำแหน่งที่แสดง <span class="required">*</span>
                      </label>
                      <div class="radio">
                            <label>
                                <input type="radio" id="check1"  checked="checked"  value="project"  name="optionsRadios">หน้าโครงการ
                            </label>
                        </div>
                      <div class="col-md-6">
                         <select class="form-control" id="page1" name="project">
                           <option value="">--เลือกโครงการ--</option>
                           <?php 
                                $sql="SELECT project_id,project_name_th,project_name_en FROM LH_PROJECTS";
                                $query=mssql_query($sql);
                                while ($rows=mssql_fetch_array($query)) {
                           ?>
                           <option value="<?=$rows['project_id']?>"><?=$rows['project_name_th']." (".$rows['project_name_en'].")";?></option>
                           <?php } ?>
                         </select>
                        
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2" for="first-name">&nbsp;<span class="required"></span>
                      </label>
                      <div class="radio">
                            <label>
                                <input type="radio" id="check2"  value="page"  name="optionsRadios">หน้า page
                            </label>
                        </div>
                      <div class="col-md-6">
                         <select class="form-control" id="page2" name="page">
                          <option value="">--เลือกหน้า Page--</option>
                           <option value="Home">Home</option>
                         </select>
                        
                      </div>
                    </div>
                    </div>

                    <script>
                    $(document).ready(function() {
                        //$("div.desc").hide();
                        $("#page2").attr("disabled", 'disabled');
                        $("#check1").click(function() {
                            var test = $(this).val();
                            //$("div.desc").hide();
                            if(test = "project"){
                              $("#page1").removeAttr('disabled');
                              $("#page1").attr("required", true);
                              $("#page2").attr("disabled", 'disabled');
                              $("#page2").val("");
                              //alert(test);
                            }
                            //$("#" + test).show();
                        });

                        $("#check2").click(function() {
                            var test = $(this).val();
                            //$("div.desc").hide();
                            if(test = "page"){
                              $("#page2").removeAttr('disabled');
                              $("#page2").attr("required", true);
                              $("#page1").attr("disabled", 'disabled');
                              $("#page1").val("");


                              //alert(test);
                            }
                            //$("#" + test).show();
                        });
                    });
                    </script>

                     <div class="form-group">
                      <label class="control-label col-md-2" for="first-name">URL พิเศษ <span class="required"></span>
                      </label>
                      <div class="col-md-6">
                         <input type="text"  class="form-control"  name="lead_img_url" placeholder="ใส่ URL พิเศษ (Landding page)" value="" />
                        
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-2" for="first-name">วันเริ่ม - สิ้นสุด <span class="required">*</span>
                      </label>
                      <div class="col-md-6">
                         <input type="text" required class="form-control has-feedback-left active" id="reservation" name="reservation" placeholder="ระบุช่วงเวลา" value="<?php echo ''.(!empty($_GET['icon_id'])? $strDate." - ".$strDate2 : "" ).'';?>" />
                          <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        
                      </div>
                    </div>

                    
                    <br>

                    <div class="form-group">
                      <label class="control-label col-md-2" for="first-name"><span class="required"></span>
                      </label>
                      <div class="col-md-6">
                        <button type="submit" name="submit" class="btn btn-success" >Submit</button>
                        
                      </div>
                    </div>
                  </form>
                 </div>
              </div>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              </div>
              </div>
              </div>
              </div>
              </div>
              </div>

             
          
        <!-- /page content -->


        <!-- footer content -->
        <!-- <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer> -->
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <!-- <script src="../vendors/jquery/dist/jquery.min.js"></script> -->
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
        <!-- bootstrap-daterangepicker -->
    <script src="js/moment/moment.min.js"></script>
    <script src="js/datepicker/daterangepicker.js"></script>
     <script type="text/javascript" src="js/bootstrap-imgupload.js"></script>
     <script>
      $(document).ready(function() {
        $('#reservation').daterangepicker(null, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });
      });
    </script>

    
     

  </body>
</html>