<?php
session_start();
$_SESSION['group_id'];
include_once('include/dbBrands.php');
header( 'Content-Type:text/html; charset=utf8');
$_GET['page']='homemodel';

if (isset($_SESSION['add'])) {
	if ($_SESSION['add'] == 1) {
		//header("location:product_add.php");
		$success = "success";
		unset($_SESSION["add"]);
	} else if ($_SESSION['add'] == 2) {

		$error = "error"; //ค่าซ้ำ
		$_SESSION['add'] = '';

	} else if ($_SESSION['add'] == -1) {
		$errors = "errors";
		unset($_SESSION["add"]);
	} else if ($_SESSION['add'] == -2) {
		$error_360 = "error_360";
		$_SESSION['add'] = '';
	}
}

?>
        <!DOCTYPE html>
        <html lang="en">
        <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <!-- Meta, title, CSS, favicons, etc. -->
          <meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1">

          <title>LAND & HOUSES</title>

          <!-- Bootstrap -->
          <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
          <!-- Font Awesome -->
          <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
          <!-- NProgress -->
          <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
          <!-- iCheck -->
          <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
          <!-- bootstrap-progressbar -->
          <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
          <!-- Custom Theme Style -->
          <link href="../build/css/custom.min.css" rel="stylesheet">

          <!-- jQuery -->
          <script src="../vendors/jquery/dist/jquery.min.js"></script>

          <!-- uploade img -->
          <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
          <!--uploade-->
          <!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
          <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>

          <!-- confirm-->
          <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
          <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>

          <style>
          .file-caption-main .btn-file {
            overflow: visible;
          }

          .file-caption-main .btn-file .error {
            position: absolute;
            bottom: -32px;
            right: 30px;
            }#seo_img_360{
              display: none;
            }
            #seo_lead_img_file{
              display: none;
            }

            /* Upload*/
            div.RTU-uploadContainer{
              width: 100% !important;
              font-size: 15px;
            }
            .lhDescRadio{
              display: none;
            }
            .RTU-uploadItemControls{
              margin: 10px;
            }
            .RTU-uploadItem{
              line-height: 0em;
            }
            div.lhDesc{
              margin-top: 15px !important;
            }
            div.lhDesc >input{
              margin-right: 10px;
              border: 1px solid #ccc;
              height: 35px;
              font-size: 18px;
              border-radius: 4px;
              text-align: center;
            }
            .btn-switch {
             font-size: 14px;
             position: relative;
             display: inline-block;
             -webkit-user-select: none;
             -moz-user-select: none;
             -ms-user-select: none;
             user-select: none;
           }
           .btn-switch__radio {
            display: none;
          }
          .btn-switch__label {
            display: inline-block;
            padding: .75em .5em .75em .75em;
            vertical-align: top;
            font-size: 1em;
            font-weight: 700;
            line-height: 1.5;
            color: #666;
            cursor: pointer;
            transition: color .2s ease-in-out;
          }
          .btn-switch__label + .btn-switch__label {
           padding-right: .75em;
           padding-left: 0;
         }
         .btn-switch__txt {
          font-size: 19px;
          position: relative;
          z-index: 2;
          display: inline-block;
          min-width: 1.5em;
          opacity: 1;
          pointer-events: none;
          transition: opacity .2s ease-in-out;
        }
        .btn-switch__radio_no:checked ~ .btn-switch__label_yes .btn-switch__txt,
        .btn-switch__radio_yes:checked ~ .btn-switch__label_no .btn-switch__txt {
          opacity: 0;
        }
        .btn-switch__label:before {
          content: "";
          position: absolute;
          z-index: -1;
          top: 0;
          right: 0;
          bottom: 0;
          left: 0;
          background: #f0f0f0;
          border-radius: 1.5em;
          box-shadow: inset 0 .0715em .3572em rgba(43,43,43,.05);
          transition: background .2s ease-in-out;
        }
        .btn-switch__radio_yes:checked ~ .btn-switch__label:before {
          background: #1abc9c;
        }
        .btn-switch__label_no:after {
          content: "";
          position: absolute;
          z-index: 2;
          top: .5em;
          bottom: .5em;
          left: .5em;
          width: 2em;
          background: #fff;
          border-radius: 1em;
          pointer-events: none;
          box-shadow: 0 .1429em .2143em rgba(43,43,43,.2), 0 .3572em .3572em rgba(43,43,43,.1);
          transition: left .2s ease-in-out, background .2s ease-in-out;
        }
        .btn-switch__radio_yes:checked ~ .btn-switch__label_no:after {
          left: calc(100% - 2.5em);
          background: #fff;
        }
        .btn-switch__radio_no:checked ~ .btn-switch__label_yes:before,
        .btn-switch__radio_yes:checked ~ .btn-switch__label_no:before {
          z-index: 1;
        }
        .btn-switch__radio_yes:checked ~ .btn-switch__label_yes {
          color: #fff;
        }

        /*-----*/
      </style>

      <link href="homefurnis/css/homefurnis.css" rel="stylesheet">
      <?php include_once ("homefurnis/header_upload_image.php"); ?>

    </head>

    <body class="nav-md">
      <div class="container body">
        <div class="main_container">
          <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
              <div class="navbar nav_title" style="border: 0;">
                <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
              </div>

              <div class="clearfix"></div>

              <!-- menu profile quick info -->
              <?php include './master/navbar.php';?>
              <!-- /menu footer buttons -->
            </div>
          </div>

          <!-- top navigation -->
          <?php include './master/top_nav.php';?>
          <!-- /top navigation -->

          <!-- page content -->
          <div class="right_col" role="main">
            <div class="">
              <div class="clearfix"></div>
              <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2><a href="series.php">ระบบจัดการข้อมูลสไตล์บ้าน (Series)</a> > สร้าง Series </h2>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      <!-- <h4>Step1 : สร้างข้อมูล Series ได้ทั้งภาษาจีน และ ภาษาอังกฤษ(*ไม่บังคับ)</h4> -->
                      <p class="font-gray-dark">
                      </p><br>
                      <?php if (isset($success)) {?>
                      <div class="row">
                        <div class="col-md-2"></div>
                        <div class="form-group">
                          <div class="alert alert-success col-md-6" id="alert">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <strong>สร้างข้อมูลเรียบร้อยแล้ว</strong>
                          </div>
                        </div>
                        <div class="col-md-6"></div>
                      </div>
                      <?php } else if (isset($error)) {?>
                      <div class="row">
                        <div class="col-md-2"></div>
                        <div class="form-group">
                          <div class="alert alert-danger col-md-6" id="alert">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <strong>มีชื่อนี้อยู่ในระบบแล้ว</strong>
                          </div>
                        </div>
                        <div class="col-md-4"></div>
                      </div>
                      <?php } else if (isset($errors)) {?>

                      <div class="row">
                        <div class="col-md-2"></div>
                        <div class="form-group">
                          <div class="alert alert-danger col-md-6" id="alert">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <strong>เกิดข้อผิดพลาด !</strong> ไม่สามารถบันทึกข้อมูลได้
                          </div>
                        </div>
                        <div class="col-md-4"></div>
                      </div>
                      <?php } else if (isset($error_360)) {?>
                      <div class="row">
                        <div class="col-md-2"></div>
                        <div class="form-group">
                          <div class="alert alert-danger col-md-6" id="alert">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <strong>เกิดข้อผิดพลาดเนื่องจากใส่ข้อมูล 360 Virtual Tour ไม่ครบ !</strong>
                          </div>
                        </div>
                        <div class="col-md-4"></div>
                      </div>
                      <?php }?>
                      <form  data-toggle="validator" class="form-horizontal form-label-left" action="save_series.php" method="post" enctype="multipart/form-data" id="commentForm">
                        <div class="form-group">
                          <label class="control-label col-md-2" for="first-name">ชื่อสไตล์บ้าน <span class="required">*</span>
                          </label>
                          <div class="col-md-6">
                            <input type="text"   class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อ Series (จีน)" name="series_name_th" value=""
                            data-rule-required="true">
                          </div>
                        </div>
                        <div class="form-group hide-for-th">
                          <label class="control-label col-md-2" for="first-name">ชื่อสไตล์บ้าน <span class="required">*</span>
                          </label>
                          <div class="col-md-6">
                            <input type="text"   class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อ Series (อังกฤษ)" name="series_name_en" value=""
                            data-rule-required="true">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-2" for="first-name">รายละเอียดสไตล์บ้าน <span class="required">*</span>
                          </label>
                          <div class="col-md-6">
                            <textarea name="series_des_th" rows="7" required class="form-control" placeholder="รายละเอียดสไตล์บ้าน (จีน)" ></textarea>
                          </div>
                        </div>
                        <div class="form-group hide-for-th">
                          <label class="control-label col-md-2" for="first-name">รายละเอียดสไตล์บ้าน <span class="required">*</span>
                          </label>
                          <div class="col-md-6">
                            <textarea name="series_des_en" rows="7" required class="form-control" placeholder="รายละเอียดสไตล์บ้าน (อังกฤษ)" ></textarea>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-md-2" for="first-name">โลโก้สไตล์บ้าน *<br>(.png ) <br> ขนาด 150 x 150 px <br> ภาพขนาดไม่เกิน 300 KB <span class="required"></span>
                          </label>
                          <div class="col-md-6">
                            <input id="logo_series" name="logo_series" type="file" multiple class="file-loading" accept="image/png" >
                          </div>
                        </div>

                        <script>
                          $("#logo_series").fileinput({
                              uploadUrl: "upload.php", // server upload action
                              maxFileCount: 1,
                              allowedFileExtensions: ["png"],
                              browseLabel: 'เลือกโลโก้สไตล์บ้าน',
                              removeLabel: 'ลบ',
                              browseClass: 'btn btn-success',
                              showUpload: false,
                              showRemove:false,
                              showCaption: false,//ปิดช่องแสดงชื่อรูป
                              msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                              msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                              msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                              msgZoomModalHeading: 'ตัวอย่างละเอียด',
                              xxx:'seo_logo_series',
                              dropZoneTitle : 'รูปโลโก้สไตล์บ้าน',
//                              minImageWidth: 150, //ขนาด กว้าง ต่ำสุด
//                              minImageHeight: 150, //ขนาด ศุง ต่ำสุด
//                              maxImageWidth: 150, //ขนาด กว้าง ต่ำสุด
//                              maxImageHeight: 150, //ขนาด ศุง ต่ำสุด
maxFileSize :300 ,
msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
});
</script>


<div class="form-group">
  <label class="control-label col-md-2" for="first-name">ภาพตัวแทนสไตล์บ้าน *<br>(.jpg , .png ) <br> ขนาด 1920 x 1080 px <br> ภาพขนาดไม่เกิน 300 KB<span class="required"></span>
  </label>
  <div class="col-md-6">
    <input id="images" name="images" type="file" multiple class="file-loading" accept="image/*" >
  </div>
</div>

<script>
  $("#images").fileinput({
                              uploadUrl: "upload.php", // server upload action
                              maxFileCount: 1,
                              allowedFileExtensions: ["png","jpg"],
                              browseLabel: 'เลือกภาพตัวแทนสไตล์บ้าน',
                              removeLabel: 'ลบ',
                              browseClass: 'btn btn-success',
                              showUpload: false,
                              showRemove:false,
                              showCaption: false,//ปิดช่องแสดงชื่อรูป
                              msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                              msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                              msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                              msgZoomModalHeading: 'ตัวอย่างละเอียด',
                              xxx:'seo_img_series',
                              dropZoneTitle : 'รูปภาพตัวแทนสไตล์บ้าน',
                              minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                              minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                              maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                              maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                              maxFileSize :300 ,
                              msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                              msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                              msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                              msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                              msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                            });
                          </script>


                          <div class="form-group">
                            <label class="control-label col-md-2" for="first-name">URL 360 Virtual Tour
                            </label>
                            <div class="col-md-6">
                              <input type="text" id="c_360" name="c_360" class="form-control col-md-7 col-xs-12" placeholder="Url 360 Virtual Tour">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-2" for="first-name">360 Cover image <br>(1920 x 1080) <br> ภาพขนาดไม่เกิน 300 KB
                            </label>
                            <div class="col-md-6">
                              <input type="file" id="img" name="c_360_img[]" class="form-control col-md-7 col-xs-12" accept="image/*">
                            </div>
                          </div>

                          <script>
                            $("#img").fileinput(
                              //'enable',
                              {
                                  uploadUrl: "upload.php", // server upload action
                                  maxFileCount: 1,
                                  allowedFileExtensions: ["jpg", "gif", "png", "jpeg"],
                                  browseLabel: 'เลือกภาพ Cover image',
                                  removeLabel: 'ลบ',
                                  browseClass: 'btn btn-success',
                                  showUpload: false,
                                  showRemove:false,
                                  showCaption: false,//ปิดช่องแสดงชื่อรูป
                                  minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                  minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                  maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                  maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                  msgFilesTooMany: 'ไฟล์ที่คุณเลือกมีจำนวน <b>({n})</b> ซึ่งเกินกว่าที่ระบบอนุญาตที่ <b>{m}</b>, กรุณาลองใหม่อีกครั้ง!',
                                  msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                  xxx:'seo_img_360',
                                  dropZoneTitle : 'รูปภาพ Cover image 360',
                                  maxFileSize :300 ,
                                  msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                  msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                  msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                  msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                  msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                });
                              </script>


                              <div class="form-group" id="inline_content">
                                <label class="control-label col-md-2" name="tvc_detail" for="first-name">เลือกชนิดการอัปไฟล์ <span class="required"></span>
                                </label>
                                <div class="col-md-6">
                                  <div class="radio">
                                    <label><input type="radio" id="youtube_" name="optradio_vdo" checked  value="youtube" class="flat"> VDO Youtube</label>
                                  </div>
                                  <div class="radio">
                                    <label><input type="radio" id="file_" name="optradio_vdo" value="vdo" class="flat"> VDO File</label>
                                  </div>
                                </div>
                              </div>

                              <div id="l1">
                                <div class="form-group">
                                  <label class="control-label col-md-2" for="first-name">URL Youtube <span class="required"> </span>
                                  </label>
                                  <div class="col-md-6">
                                    <div class="control-group">
                                      <input type="text" id="youtube" name="youtube" class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube" value="">
                                    </div>
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label class="control-label col-md-2" for="first-name">Thumbnail Youtube <br>(.jpg , .png , .gif <br> ขนาด 1920 x 1080 px) <br> ภาพขนาดไม่เกิน 300 KB<span class="required"> </span>
                                  </label>
                                  <div class="col-md-6">
                                    <div class="control-group">
                                      <input type="file" name="img_youtube" id="img_youtube" class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube" value="" accept="image/*">
                                    </div>
                                  </div>
                                </div>


                                <script>
                                  $("#img_youtube").fileinput({
                              uploadUrl: "upload.php", // server upload action
                              maxFileCount: 1,
                              allowedFileExtensions: ["jpg", "png", "jpeg"],
                              browseLabel: 'เลือกรูป',
                              removeLabel: 'ลบ',
                              browseClass: 'btn btn-success',
                              showUpload: false,
                              showRemove:false,
                              showCaption: false,//ปิดช่องแสดงชื่อรูป
                              msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                              msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                              msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                              msgZoomModalHeading: 'ตัวอย่างละเอียด',
                              xxx:'seo_lead_img_file',
                              dropZoneTitle : 'รูปภาพ Thumbnail Youtube',
                              minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                              minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                              maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                              maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                              maxFileSize :300 ,
                              msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                              msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                              msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                              msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                              msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                            });
                          </script>
                        </div>

                        <div id="l2">
                          <div class="form-group">
                            <label class="control-label col-md-2" for="first-name">Thumbnail VDO <br>(.jpg , .png ,.gif <br> ขนาด 1920 x 1080 px) <br> ภาพขนาดไม่เกิน 300 KB<span class="required"></span>
                            </label>
                            <div class="col-md-6">
                              <div class="control-group">
                                <input type="file" name="thumbnail_vdo" id="thumbnail_vdo" class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube" value="" accept="image/*">
                              </div>
                            </div>
                          </div>

                          <script>
                            $("#thumbnail_vdo").fileinput({
                                  uploadUrl: "upload.php", // server upload action
                                  maxFileCount: 1,
                                  allowedFileExtensions: ["jpg", "png", "jpeg"],
                                  browseLabel: 'เลือกรูป',
                                  removeLabel: 'ลบ',
                                  browseClass: 'btn btn-success',
                                  showUpload: false,
                                  showRemove:false,
                                  showCaption: false,//ปิดช่องแสดงชื่อรูป
                                  msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                  msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                                  msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                  msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                  xxx:'seo_lead_img_file',
                                  dropZoneTitle : 'รูปภาพ Thumbnail VDO',
                                  minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                  minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                  maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                  maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                  maxFileSize :300 ,
                                  msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                  msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                  msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                  msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                  msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                });
                              </script>

                              <div class="form-group">
                                <label class="control-label col-md-2" for="first-name"> VDO<span class="required"> </span>
                                </label>
                                <div class="col-md-6">
                                  <div class="control-group">
                                    <input type="file" name="vdo_file" id="vdo_file" class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube" value="" accept="video/mp4">
                                  </div>
                                </div>
                              </div>

                              <script>
                                $("#vdo_file").fileinput({
                                  uploadUrl: "upload.php", // server upload action
                                  maxFileCount: 1,
                                  allowedFileExtensions: ["mp4"],
                                  browseLabel: 'เลือกไฟล์',
                                  removeLabel: 'ลบ',
                                  browseClass: 'btn btn-success',
                                  showUpload: false,
                                  showRemove:false,
                                  showCaption: false,//ปิดช่องแสดงชื่อรูป
                                  msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                  msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                                  msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                  msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                  xxx:'seo_lead_img_file',
                                  dropZoneTitle : 'VDO File',

                                  //maxFileSize :300 ,
                                  msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                  msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                  msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                  msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                  msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                });
                              </script>
                            </div>
<!--
                            <div id="l2">
                              <div class="form-group">
                                <label class="control-label col-md-2" for="first-name">Gallery</span>
                                </label>
                                <div class="col-md-10">
                                  <div id="dropzone">
                                    <input type="file" id="uploader" multiple accept="image/*" />
                                  </div>
                                </div>
                              </div>
                            </div> -->

                            <br>
                            <div class="form-group">
                              <label class="control-label col-md-2">แสดงบนเว็บ
                              </label>
                              <div class="col-md-4">
                               <p class="btn-switch">
                                 <input type="radio" id="yes" name="switch" value="on" class="btn-switch__radio btn-switch__radio_yes" />
                                 <input type="radio" checked id="no" value="off" name="switch" class="btn-switch__radio btn-switch__radio_no" />
                                 <label for="yes" class="btn-switch__label btn-switch__label_yes"><span class="btn-switch__txt">เปิด</span></label>                  <label for="no" class="btn-switch__label btn-switch__label_no"><span class="btn-switch__txt">ปิด</span></label>
                               </p>
                             </div>
                           </div>


                           <center><br>
                            <div class="col-md-6">
                              <button type="submit" name="submit" class="btn btn-success" >Submit</button>
                              <!--<button type="button" name="delete" class="btn btn-danger">Delete</button>-->
                              <!--                        <a href="www.mypage.com" onclick="window.history.go(-1); return false;"> Link </a>-->
                            </div>
                          </center>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>

            <!-- Bootstrap -->
            <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
            <!-- FastClick -->
            <script src="../vendors/fastclick/lib/fastclick.js"></script>
            <!-- NProgress -->
            <script src="../vendors/nprogress/nprogress.js"></script>
            <!-- iCheck -->
            <script src="../vendors/iCheck/icheck.min.js"></script>
            <script type="text/javascript" src="js/bootstrap-imgupload.js"></script>
            <!-- Validate -->
            <script src="../build/js/jquery.validate.js"></script>

            <!-- Custom Theme Scripts -->
            <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>
            <!-- bootstrap-progressbar -->
            <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
            <!-- Custom Theme Scripts -->
            <script src="../build/js/custom.min.js"></script>
            <script>
              $(document).ready(function() {
                $("#commentForm").validate({
                  rules: {
                    series_name_th: "required",
                    series_name_en: "required",
                    series_des_th: "required",
                    series_des_en: "required",
                    logo_series : "required",
                    images : "required",
                  },
                  messages: {
                    series_name_th: "กรอกชื่อ series จีน !",
                    series_name_en: "กรอกชื่อ series อังกฤษ !",
                    series_des_th: "กรอกรายละเอียดสไตล์บ้าน จีน !",
                    series_des_en: "กรอกรายละเอียดสไตล์บ้าน อังกฤษ !",
                    logo_series :" &nbsp; ไม่มีรูป !",
                    images : " &nbsp; ไม่มีรูป !",
                    img_youtube : " &nbsp; ไม่มีรูป !",
                    youtube : "กรอก URL Youtube !",
                    thumbnail_vdo : " &nbsp; ไม่มีรูป !",
                    vdo_file :" &nbsp; ไม่มี VDO !",
                    'c_360_img[]' : " &nbsp; ไม่มีรูป !",
                    c_360 : "กรอก Url 360 Virtual Tour !",

                  }
                });

              });
            </script>

            <script type="text/javascript">
              $('.fancybox').fancybox();
            </script>



            <script type="text/javascript">
              $(document).ready(function(){
                $("#l1").show();
                $("#l2").hide();
              });
            </script>
            <script type="text/javascript">
              $("#commentForm").submit(function() {
                var youtube = $("#youtube").val();
                var img_youtube = $("#img_youtube").val();
                if(youtube != "" && img_youtube == "" ){
                  $("#img_youtube").attr('required','true');

                }else if(img_youtube != "" && youtube =="" ){
                  $("#youtube").attr('required','true');

                }else if(youtube == "" && img_youtube == ""){
                  $("#img_youtube").removeAttr('required','true');
                  $("#youtube").removeAttr('required','true');
                }
              });
            </script>
            <script type="text/javascript">
              $("#commentForm").submit(function() {
                var thumbnail_vdo = $("#thumbnail_vdo").val();
                var vdo_file = $("#vdo_file").val();
                if(vdo_file != "" && thumbnail_vdo == "" ){
                  $("#thumbnail_vdo").attr('required','true');

                }else if(thumbnail_vdo != "" && vdo_file =="" ){
                  $("#vdo_file").attr('required','true');

                }else if(vdo_file == "" && thumbnail_vdo == ""){
                  $("#thumbnail_vdo").removeAttr('required','true');
                  $("#vdo_file").removeAttr('required','true');
                }
              });
            </script>
            <script type="text/javascript">
              $("#commentForm").submit(function() {
                var c_360 = $("#c_360").val();
                var img = $("#img").val();
                if(c_360 != "" && img == "" ){
                  $("#img").attr('required','true');

                }else if(img != "" && c_360 =="" ){
                  $("#c_360").attr('required','true');

                }else if(c_360 == "" && img == ""){
                  $("#img").removeAttr('required','true');
                  $("#c_360").removeAttr('required','true');
                }
              });
            </script>




            <script>
              $(document).ready(function(){
                $("#alert").show();
                $("#alert").fadeTo(6000, 500).slideUp(500, function(){
                  $("#alert").alert('close');
                });
              });
            </script>


            <script>

              $("#youtube_").on('ifChecked', function(event){
                $("#l1").show();
                $("#l2").hide();
                var value = $(this).val();
                alert("You clicked " + value);
              });
              $("#file_").on('ifChecked', function(event){
                $("#l1").hide();
                $("#l2").show();
              });

            </script>
            <!-- /jQuery Tags Input -->
            <script src="js/validate_file_300kb.js"></script>

          </body>
          </html>