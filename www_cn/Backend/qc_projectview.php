<?php
session_start();
header('Content-Type:text/html; charset=utf8');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (empty($_SESSION['group_status'])) {
	header("location:login.php?errors=e");
}
include("qc_project.php");
$url_master ="";
include './include/dbCon_mssql.php';

error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once 'include/dbConnect.php';
require_once 'include/function_query.php';

$conn = (new dbConnect())->getConn();
$project_name_th = "";

if (isset($_SESSION['add'])) {
	if ($_SESSION['add'] == 0) {
		$yum = 'yum';
		unset($_SESSION["add"]);

	} else if ($_SESSION['add'] == -1) {
		$dont = 'not';
		unset($_SESSION["add"]);

	}
}

if (isset($_GET['project_id'])) {

	$arrProjectName = getProjectsById($_GET['project_id']);
	$GLOBALS['project_name_th'] = $arrProjectName[0]['project_name_th'];
}

function getProjectsById($project_id) {
	try {
		$sql = 'select project_name_th from lh_projects where project_id=' . $project_id;
		$result = $GLOBALS['conn']->query($sql);
		return $result->fetchAll();
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}
}

function getBrand() {
	try {
//        $sql = 'SELECT brand_id,brand_name_th,brand_name_en FROM LH_BRANDS';
		//        $result = $GLOBALS['conn']->query($sql);
		//        return $result->fetchAll();
		$sql = 'SELECT brand_id,brand_name_th,brand_name_en FROM LH_BRANDS';
		$query = mssql_query($sql);
		$result = mssql_fetch_array($query);
		return $result;
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}

}

function getZone() {
	try {
		$sql = 'SELECT * FROM LH_ZONES ORDER BY zone_name_th ASC';
		$result = $GLOBALS['conn']->query($sql);
		return $result->fetchAll();
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}

}

function getProvince() {
	try {
		$sql = 'SELECT * FROM lh_provinces ORDER BY PROVINCE_NAME ASC';
		$result = $GLOBALS['conn']->query($sql);
		return $result->fetchAll();
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}

}

function getAmphur($id) {
	try {
		$sql = "SELECT * FROM lh_amphurs WHERE PROVINCE_ID = '$id' ORDER BY AMPHUR_NAME ASC";
		$result = $GLOBALS['conn']->query($sql);
		return $result->fetchAll();
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}

}

function getDistrict($id) {
	try {
		$sql = "SELECT * FROM lh_districts WHERE AMPHUR_ID = '$id'  ORDER BY DISTRICT_NAME ASC";
		$result = $GLOBALS['conn']->query($sql);
		return $result->fetchAll();
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}

}

function getGroup() {
	try {
		$sql = 'SELECT G.group_id, G.group_account_name
        FROM LH_GROUPS G
        WHERE G.group_id != 1
        ORDER BY G.group_account_name ASC';
		$result = $GLOBALS['conn']->query($sql);
		return $result->fetchAll();
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}

}

function getTypeProjectImageByIdProject() {
	try {
		$sql = 'SELECT DISTINCT LEAD_IMAGE_PROJECT_FILE_TYPE
        FROM LH_LEAD_IMAGE_PROJECT_FILE
        WHERE PROJECT_ID = ' . $_GET['project_id'];
		$result = $GLOBALS['conn']->query($sql);
		if ($result != null) {
			return $result->fetchAll();
		}

	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}

}

function getAllProjectImageByIdProject() {
	try {
		$sql = 'SELECT * FROM LH_LEAD_IMAGE_PROJECT_FILE
        WHERE PROJECT_ID = ' . $_GET['project_id'];
		$result = $GLOBALS['conn']->query($sql);
		return $result->fetchAll();
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}
}

function getGeleryByIdProject() {
	try {
		$sql = 'SELECT * FROM LH_GALERY_PROJECT gp
        RIGHT JOIN LH_GALERY_IMG_PROJECT gip
        ON gp.galery_project_id = gip.galery_project_id
        WHERE gp.project_id = ' . $_GET['project_id'];
		//$result = $GLOBALS['conn']->query($sql);
        $result = mssql_query ( $sql, $GLOBALS ['db_conn'] );  
          $results = [];
          while ($row = mssql_fetch_assoc($result)) {
              $results[] = $row;
          }
		return $results;
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}
}

function getURL360ByIdProject() {
	try {
		$sql = 'SELECT * FROM LH_360_PROJECT
        WHERE project_id = ' . $_GET['project_id'];
		$result = $GLOBALS['conn']->query($sql);
		return $result->fetchAll();
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}
}

function getPositionGeleryByIdProject() {
	try {
		$sql = 'SELECT * FROM LH_GALERY_PROJECT
        WHERE PROJECT_ID = ' . $_GET['project_id'];
		$result = $GLOBALS['conn']->query($sql);
		return $result->fetchAll();
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}
}

function getAllCommunityFeature() {
	try {
		$sql = 'SELECT * FROM LH_COMMUNITY_FEATURES';
		$result = $GLOBALS['conn']->query($sql);
		return $result->fetchAll();
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}
}

function getFLmZone() {
	try {
		$sql = "SELECT * FROM LH_ZONES  WHERE zone_attribute = 'websiteflm' OR zone_attribute = 'flm' ORDER BY zone_name_th ASC";
		$result = $GLOBALS['conn']->query($sql);
		return $result->fetchAll();
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}
}

function getCommunityByIdProject() {
	try {
		$sql = 'SELECT COM.*, PS.product_id, PS.project_id
        FROM LH_COMMUNITY COM
        LEFT JOIN LH_PROJECT_SUB PS
        ON COM.project_sub_id = PS.project_sub_id
        LEFT JOIN LH_PROJECTS P
        ON PS.project_id = P.project_id
        WHERE PS.project_id = ' . $_GET['project_id'];
		$sql .= ' AND PS.product_id = 3';
		$result = $GLOBALS['conn']->query($sql);
		return $result->fetchAll();
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}
}

function getAllRoomFeature() {
	try {
		$sql = 'SELECT * FROM LH_ROOM_FEATURES';
		$result = $GLOBALS['conn']->query($sql);
		return $result->fetchAll();
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}
}

function getFeatureByIdProject() {
	try {
		$sql = 'SELECT FT.*, PS.product_id, PS.project_id
        FROM LH_FEATURES FT
        LEFT JOIN LH_PROJECT_SUB PS
        ON FT.project_sub_id = PS.project_sub_id
        LEFT JOIN LH_PROJECTS P
        ON PS.project_id = P.project_id
        WHERE PS.project_id = ' . $_GET['project_id'];
		$sql .= ' AND PS.product_id = 3';
		$result = $GLOBALS['conn']->query($sql);
		return $result->fetchAll();
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}
}

function checkTypeProductByIdProject() {
	try {
		$sql = 'SELECT PS.product_id FROM LH_PROJECT_SUB PS
        LEFT JOIN LH_PROJECTS P
        ON PS.project_id = P.project_id
        LEFT JOIN LH_PRODUCTS PD
        ON PS.product_id = PD.product_id
        WHERE PS.project_id = ' . $_GET['project_id'];
		$result = $GLOBALS['conn']->query($sql);
		return $result->fetchAll();
	} catch (PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}
}


$group_id = $_SESSION['group_id'];
$_SESSION['project_id'] = $_GET['project_id'];
$_GET['page'] = 'form';

?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
    <!-- comfirm -->
    <link rel="stylesheet" href="../build/libs/bundled.css">
    <!-- uploade img -->
    <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">

    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <script src="../build/js/custom.min.js"></script>
    <!--uploade-->
    <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
    <!-- Fancybox image popup -->
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />

    <!-- Include JS file. -->
    <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.3.4/js/froala_editor.min.js"></script> -->

    <!-- Include Code Mirror style -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">

    <!-- Include Editor style. -->
    <link href="editor/css/froala_editor.min.css" rel="stylesheet" type="text/css" />
    <link href="editor/css/froala_style.min.css" rel="stylesheet" type="text/css" />

    <!--  froala  -->
    <link href="libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="libs/codemirror/5.3.0/codemirror.min.css">
    <link href="libs/froala-editor/2.6.2/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
    <link href="libs/froala-editor/2.6.2/css/froala_style.min.css" rel="stylesheet" type="text/css" />

    <!-- Include Editor Plugins style. -->
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/char_counter.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/code_view.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/colors.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/emoticons.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/file.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/fullscreen.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/image.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/image_manager.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/line_breaker.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/quick_insert.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/table.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/video.css">

    <script id="fr-fek">try{(function (k){localStorage.FEK=k;t=document.getElementById('fr-fek');t.parentNode.removeChild(t);})('inkH3fiA6B-13a==')}catch(e){}</script>
    <link rel="stylesheet" href="../build/css/huebee.css">
    <!-- or -->

    <script src="../build/js/huebee.pkgd.min.js"></script>
    <link rel="stylesheet" href="css/css_qcproject_view.css">

    <!-- confirm-->
    <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
    <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
                    </div>
                    <div class="clearfix"></div>
                    <?php include "master/navbar.php";?>
                </div>
            </div>
            <!-- top navigation -->
            <?php include './master/top_nav.php';?>
            <!-- /top navigation -->
            <?php
             $sql_pn = "SELECT project_name_en,project_name_th,project_url,lacation_yan FROM LH_PROJECTS WHERE project_id  = '" . $_GET['project_id'] . "'";
             $query_pn = mssql_query($sql_pn);
            $row_pn = mssql_fetch_array($query_pn);
            ?>
            <!-- page content class=form-->
            <div class="right_col" role="main">
                <!-- class="" -->
                <div class="">
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2><a href="<?php echo 'project.php'; ?>">ข้อมูลโครงการ | Project Information : </a><?php echo $row_pn['project_name_th']; ?></h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <p class="font-gray-dark"></p>
                                    <?php if (isset($yum)) {?>
                                    <!-- class=row -->
                                    <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="form-group">
                                            <div class="alert alert-danger col-md-6" id="alert">
                                                <button type="button" class="close" data-dismiss="alert">x</button>
                                                <strong>มีชื่อนี้อยู่ในระบบแล้ว !</strong>
                                            </div>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                    <!-- ./ class=row -->
                                    <?php } else
if (isset($_GET['de'])) {?>
                                    <!-- class=row -->
                                    <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="form-group">
                                            <div class="alert alert-success col-md-6" id="alert">
                                                <button type="button" class="close" data-dismiss="alert">x</button>
                                                <strong>แก้ไขข้อมูลเรียบร้อยแล้ว</strong>
                                            </div>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                    <!-- ./ class=row -->
                                    <?php } else if (isset($don)) {?>
                                    <!-- class=row -->
                                    <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="form-group">
                                            <div class="alert alert-danger col-md-6" id="alert">
                                                <button type="button" class="close" data-dismiss="alert">x</button>
                                                <strong>ไม่สามารถลบข้อมูลได้ </strong> เนื่องจากมีบ้านพร้อมขายที่ผูกกับโครงการนี้อยู่
                                            </div>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                    <!-- ./ class=row -->
                                    <?php }?>
                                    <?php $sizeProduct = count(checkTypeProductByIdProject());
$resultTypeProduct = checkTypeProductByIdProject()[0];
if ($sizeProduct >= 2) {
	?>
                                        <a  href="#<?php //echo 'homesales.php?project_id=' . $_GET['project_id']; ?>" >
                                            <button type="button" class="btn btn-success flright">ข้อมูลบ้านพร้อมขาย</button>
                                        </a>
                                        <?php } else {
	if ($resultTypeProduct['product_id'] != 3) {
		?>
                                                <a  href="#<?php //echo 'homesales.php?project_id=' . $_GET['project_id']; ?>">
                                                    <button type="button" class="btn btn-success flright">ข้อมูลบ้านพร้อมขาย</button>
                                                </a>
                                                <?php }
}?>
                                            <br><br>
                                            <?php
$sql_pn = "SELECT project_name_en,project_name_th,project_url,lacation_yan FROM LH_PROJECTS WHERE project_id  = '" . $_GET['project_id'] . "'";
$query_pn = mssql_query($sql_pn);
$row_pn = mssql_fetch_array($query_pn);
?>
                                            <form id="form-project" class="form-horizontal form-label-left" action="save_project.php" enctype="multipart/form-data" method="POST">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> <span class="required"></span></label>
                                                    <div class="col-md-6">
                                                       <span style="color: red">* นักการตลาด กรอกข้อมูลสำหรับ FLM เฉพาะที่มี ดอกจัน * เท่านั้น</span>
                                                    </div>
                                                </div>
                                                <!-- class=form-group -->
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">ระบุชื่อโครงการ  (จีน)<span class="required">*</span></label>
                                                    <div class="col-md-6">
                                                        <input type="text" id="project_name_th"
                                                        name="project_name_th"
                                                        value="<?php echo htmlspecialchars($row_pn['project_name_th']) ?>"
                                                        class="form-control col-md-7 col-xs-12"
                                                        placeholder="กรอกชื่อโครงการ (จีน)" disabled="disabled">
                                                    </div>
                                                </div>
                                                <!-- ./ class=form-group -->
                                                <!-- class=form-group -->
                                                <div class="form-group ">
                                                    <label class="control-label col-md-3">ระบุชื่อโครงการ (ไทย)<span class="required">*</span></label>
                                                    <div class="col-md-6">
                                                        <input type="text" id="project_name_en"
                                                        value="<?php echo $row_pn['project_name_en'] ?>"
                                                        name="project_name_en"
                                                        class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อโครงการ (ไทย)"
                                                        disabled="disabled">
                                                    </div>
                                                </div>
                                                <!-- ./ class=form-group -->
                                                <!--  class=form-group -->
                                                <div class="form-group">
                                                    <label class="control-label col-md-3" for="brand">ระบุ Brand <span class="required">*</span></label>
                                                    <div class="col-md-4 col-sm-9 col-xs-12">
                                                        <select name="detailband" id="detailband" class="form-control" required disabled="disabled">
                                                            <option value="">เลือก Brand</option>
                                                            <?php
$sql = "SELECT brand_id,brand_name_th,brand_name_en FROM LH_BRANDS ORDER BY brand_name_th ASC ";
$query = mssql_query($sql);
while ($value = mssql_fetch_array($query)) {?>
                                                            <?php if ($value['brand_id'] == $detailband[0]['brand_id']) {?>
                                                            <option value="<?php echo $value['brand_id']; ?>" selected>
                                                                <?php echo $value['brand_name_th']; ?>
                                                            </option>
                                                            <?php } else {?>
                                                            <option value="<?php echo $value['brand_id']; ?>">
                                                                <?php echo $value['brand_name_th']; ?>
                                                            </option>
                                                            <?php }?>
                                                            <?php }?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!-- ./ class=form-group -->
                                                <!-- class=form-group -->
                                                <?php if ($_SESSION['group_status'] == 'sell') {
	$readonly = 'disabled';
	$display = 'style="display: none"';
} else {
	$readonly = '';
	$display = '';
}?>

                                                <!-- project zone -->
                                                <hr>

                                                <?php include "project_zone.php";?>
                                                <!-- end  -->

                                                <!-- class=form-group -->
                                                <div class="form-group" >
                                                    <label class="control-label col-md-3" for="group">ระบุกลุ่ม <span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-4 col-sm-9 col-xs-12">
                                                        <select name="group_id" class="form-control" required <?=$readonly;?>>
                                                            <option value="">เลือกกลุ่ม</option>
                                                            <?php foreach (getGroup() as $value) {?>
                                                            <?php if ($value['group_id'] == $project[0]['group_id']) {?>
                                                            <option value="<?php echo $value['group_id']; ?>" selected>
                                                                <?php echo $value['group_account_name']; ?>
                                                            </option>
                                                            <?php } else {?>
                                                            <option value="<?php echo $value['group_id']; ?>">
                                                                <?php echo $value['group_account_name']; ?>
                                                            </option>
                                                            <?php }?>
                                                            <?php }?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!-- ./ class=form-group -->

                                                <?php
foreach ($mapProducts as $itemProduct) {
	if ($itemProduct['checked'] == 1) {
		if ($itemProduct['product_home_id'] == 3) {
			?>
                                                        <!-- class=form-group -->
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">URL โครงการ <span
                                                                class="required"></span></label>
                                                                <div class="col-md-6">
                                                                    <input class="form-control"
                                                                    value="<?php $url_path = 'cn/condominium/';
			echo $url_project = $url_master . $url_path . str_replace(' ', '-', $row_pn['project_name_th']);?>" disabled>
             <input type="hidden" value="<?=$url_project;?>" name="url_web[]">


                                                                    <label class="control-label col-md-3 wdlabel"></label>
                                                                </div>
                                                            </div>
       
                                                            <?php
} else if ($itemProduct['product_home_id'] == 2) {
			?>
                                                        <!-- class=form-group -->
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">URL โครงการ <span
                                                                class="required"></span></label>
                                                                <div class="col-md-6">
                                                                    <input class="form-control"
                                                                    value="<?php $url_path = 'cn/townhome/project/';
			echo $url_project = $url_master . $url_path . str_replace(' ', '-', $row_pn['project_name_th']) . '/' . $itemProduct['product_home_id'];
			?>" disabled>
            <input type="hidden" value="<?=$url_project;?>" name="url_web[]">

                                                                    <label class="control-label col-md-3 wdlabel"></label>
                                                                </div>
                                                            </div>
            
                                                            <?php } else {
			?>
                                                            <!-- class=form-group -->
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">URL โครงการ <span
                                                                    class="required"></span></label>
                                                                    <div class="col-md-6">
                                                                        <input class="form-control"
                                                                        value="<?php $url_path = 'cn/singlehome/project/';
			echo $url_project = $url_master . $url_path . str_replace(' ', '-', $row_pn['project_name_th']) . '/' . $itemProduct['product_home_id'];
			?>" disabled>
            <input type="hidden" value="<?=$url_project;?>" name="url_web[]">

                                                                        <label class="control-label col-md-3 wdlabel"></label>
                                                                    </div>
                                                                </div>
             
                                                                <?php
}
	}
}
?>


                                                    <?php
if ($_SESSION['group_status'] != "Admin") {
	foreach ($mapProducts as $itemProduct) {
		if ($itemProduct['checked'] == 1) {
			if ($itemProduct['product_home_id'] == 3) {?>
                                                                <input type="hidden" name="productcondo_id"
                                                                value="<?php echo $itemProduct['product_home_id']; ?>">
                                                                <?php
} else {?>
                                                            <input type="hidden" name="product_home_id[]"
                                                            value="<?php echo $itemProduct['product_home_id']; ?>">

                                                            <?php }
		}
	}
}
?>




                                                <?php if ($_SESSION['group_status'] == "Admin") {
	?>
                                                <br>
                                                <!-- ./ class=form-group -->
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">ประเภทที่อยู่อาศัยที่เปิดขาย<br>(ระบุได้มากกว่า 1 รายการ) <span class="required">*</span></label>
                                                    <label id="error-checkbox" style="color: #F44300;"></label>
                                                    <!-- class=col-md-3 -->
                                                    <div class="col-md-3">
                                                        <p style="padding: 5px;" id="product_chkb">
                                                            <?php
foreach ($mapProducts as $itemProduct) {
		$type_id = $itemProduct['product_home_id'];
		if ($itemProduct['checked'] == 1) {
			if ($itemProduct['product_home_id'] == 3) {
				?>
                                                                        <input type="checkbox" name="productcondo_id"
                                                                        value="<?php echo $itemProduct['product_home_id']; ?>" checked="checked"
                                                                        onclick="detailProduct(<?php echo $itemProduct["product_home_id"] ?>, '<?php echo $itemProduct["product_home_name_en"] ?>' )"
                                                                        class="flat product_checkboxt"/> <?php echo $itemProduct["product_home_name_en"]; ?>
                                                                        <?php } else {?>
                                                                        <input type="checkbox" name="product_home_id[]"
                                                                        value="<?php echo $itemProduct['product_home_id']; ?>" checked="checked"
                                                                        onclick="detailProduct(<?php echo $itemProduct["product_home_id"] ?>, '<?php echo $itemProduct["product_home_name_en"] ?>' )"
                                                                        class="flat product_checkboxt"/> <?php echo $itemProduct["product_home_name_en"]; ?>
                                                                        <?php }?>
                                                                        <br/>
                                                                        <?php } else {
			if ($itemProduct['product_home_id'] == 3) {
				?>
                                                                                <input type="checkbox" name="productcondo_id"
                                                                                value="<?php echo $itemProduct['product_home_id']; ?>"
                                                                                onclick="detailProduct(<?php echo $itemProduct["product_home_id"] ?>, '<?php echo $itemProduct["product_home_name_en"] ?>' )"
                                                                                class="flat product_checkboxt"/> <?php echo $itemProduct["product_home_name_en"]; ?>
                                                                                <?php } else {?>
                                                                                <input type = "checkbox" name="product_home_id[]"
                                                                                value="<?php echo $itemProduct['product_home_id']; ?>"
                                                                                onclick="detailProduct(<?php echo $itemProduct["product_home_id"] ?>, '<?php echo $itemProduct["product_home_name_en"] ?>' )"
                                                                                class="flat product_checkboxt"/> <?php echo $itemProduct["product_home_name_en"]; ?>

                                                                                <?php }?>
                                                                                <br/>
                                                                                <?php }?>
                                                                                <?php }?>
                                                                            </p>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3" for="first-name"></label>
                                                                            <div class="col-md-6">
                                                                                <label for="product_home_id[]"  class="error"></label>
                                                                            </div>
                                                                        </div>
                                                                        <!-- ./ class=col-md-3 -->
                                                                    </div>
                                                                    <!-- ./ class=form-group -->

                                                                    <?php

	$sql_pv = "SELECT product_view_id FROM LH_PROJECT_VIEW WHERE project_id = '" . $_GET['project_id'] . "'";
	$query_pv = mssql_query($sql_pv);

	$result_pd = [];
	while ($row = mssql_fetch_array($query_pv)) {
		$result_pd[] = $row['product_view_id'];
	}
	?>
                                                                    <br>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3" for="group">เลือกหน้าที่จะแสดง <span class="required">*</span>
                                                                        </label>
                                                                        <div class="col-md-6 col-sm-9 col-xs-12">
                                                                            <input type="checkbox" name="preview_page[]"  <?=in_array(1, $result_pd) ? 'checked' : '';?> value="1" data-parsley-mincheck="1"  class="flat" />
                                                                            บ้านเดี่ยว
                                                                            <br />
                                                                            <input type="checkbox" name="preview_page[]" <?=in_array(2, $result_pd) ? 'checked' : '';?>   value="2" data-parsley-mincheck="1"  class="flat" />
                                                                            ทาวน์โฮม
                                                                            <br />
                                                                            <input type="checkbox" name="preview_page[]" <?=in_array(3, $result_pd) ? 'checked' : '';?>   value="3" data-parsley-mincheck="1"  class="flat" />
                                                                            คอนโดมิเนียม
                                                                            <br />
                                                                            <input type="checkbox" name="preview_page[]" <?=in_array(4, $result_pd) ? 'checked' : '';?>   value="4" data-parsley-mincheck="1"  class="flat" />
                                                                            โครงการในต่างจังหวัด
                                                                            <br />
                                                                            <input type="checkbox" name="preview_page[]" <?=in_array(5, $result_pd) ? 'checked' : '';?>   value="5" data-parsley-mincheck="1"  class="flat" />
                                                                            Ladawan
                                                                            <br />
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3" for="first-name"></label>
                                                                        <div class="col-md-6">
                                                                            <label for="preview_page[]"  class="error"></label>
                                                                        </div>
                                                                    </div>
                                                                    <?php } //admin save ?>
                                                                    <hr>
                                                                    <!-- class=form-group id=detail_div_checkbox -->
                                                                    <div class="form-group" id="detail_div_checkbox">
                                                                        <?php

foreach ($detailSubproject as $itemdetailSubproject) {
	if ($itemdetailSubproject['product_home_id'] != 3) {
		echo '<div id = "detail_checkbox' . $itemdetailSubproject["product_home_id"] . '">' .
			'<h4 ><u >' . $itemdetailSubproject["product_home_name_th"] . '</u ></h4>' .
			'<div class="form-group" >
                                                                                <label class="control-label col-md-3" > พื้นที่ใช้สอยตั้งแต่ 
                                                                                </label >
                                                                                <div class="col-md-2" >
                                                                                <input type="text" id = "homearea_m[]" name = "homearea_m[]"
                                                                                class="form-control"
                                                                                value ="'.trim($itemdetailSubproject["area_home"]).'"
                                                                                placeholder = "" onkeypress="return isNumber(event)" >

                                                                                </div >
                                                                                <div class="col-md-3" >
                                                                                <label class="control-label col-md-3 wdlabel" > ตารางเมตร
                                                                                </label >
                                                                                </div>
                                                                                </div >
                                                                                <div class="form-group" >
                                                                                <label class="control-label col-md-3" > สร้างบนขนาดที่ดินตั้งแต่ 
                                                                                </label >
                                                                                <div class="col-md-2" >
                                                                                <input type="text" id = "homearea_w[]" name = "homearea_w[]"
                                                                                class="form-control"
                                                                                value = "'.trim($itemdetailSubproject["area_square_w_home"]).'"
                                                                                placeholder = "" onkeypress="return isNumber(event)"  >

                                                                                </div >
                                                                                <div class="col-md-3" >
                                                                                <label class="control-label col-md-3 wdlabel" > ตารางวา
                                                                                </label >
                                                                                </div>
                                                                                </div >
                                                                                <div class="form-group" >
                                                                                <label class="control-label col-md-3" > มีแบบบ้านให้เลือก 
                                                                                </label >
                                                                                <div class="col-md-2" >
                                                                                <input type="text" id = "num_plan_home[]" name = "num_plan_home[]"
                                                                                class="form-control"
                                                                                value = "'.trim($itemdetailSubproject["num_plan_home"]).'"
                                                                                placeholder = "" onkeypress="return isNumber(event)"  >

                                                                                </div >
                                                                                <div class="col-md-3" >
                                                                                <label class="control-label col-md-3 wdlabel" > แบบ
                                                                                </label >
                                                                                </div>
                                                                                </div >
                                                                                <!--บ้านเดี่ยว-->
                                                                                </div >';
	} else {
        echo '<div class="form-group ">
          <label class="control-label col-md-3">Location "ย่าน"" หรือ "ทำเล"</label>
           <div class="col-md-6">
           <input type="text" value="'.htmlspecialchars($row_pn['lacation_yan']).'" name="yan" class="form-control col-md-7 col-xs-12" placeholder="กรอก ย่าน หรือ ทำเล">
           </div>
         </div>
         <hr>';
         
		echo '<div id="detail_checkbox' . $itemdetailSubproject["product_home_id"] . '">
                                                                                <!-- condo -->
                                                                                <h4><u>'.trim($itemdetailSubproject["product_home_name_th"]).'</u></h4>

                                                                                <div class="form-group">
                                                                                <label class="control-label col-md-3">พื้นที่ใช้สอยตั้งแต่ </label>
                                                                                <div class="col-md-2">
                                                                                <input type="text" id="area_condofrom"
                                                                                name="area_condofrom"
                                                                                value="'.trim($itemdetailSubproject["detailcondo"][0]["area_condofrom"]).'"
                                                                                class="form-control col-md-7 col-xs-12"
                                                                                onkeypress="return isNumber(event)" min="1">
                                                                                </div>
                                                                                <div class="col-md-1 col-sm-9 col-xs-12">
                                                                                <label class="control-label col-md-3 ">&nbsp&nbsp&nbspถึง </label>
                                                                                </div>
                                                                                <div class="col-md-2">
                                                                                <input type="text" id="area_condoto" name="area_condoto"
                                                                                value="'.trim($itemdetailSubproject["detailcondo"][0]["area_condoto"]).'"
                                                                                class="form-control col-md-7 col-xs-12" onkeypress="return isNumber(event)"  min="1">
                                                                                </div>
                                                                                <div class="col-md-3">
                                                                                <label class="control-label col-md-5"> ตารางเมตร
                                                                                </label>
                                                                                </div>
                                                                                </div>



                                                                                <div class="form-group">
                                                                                <label class="control-label col-md-3">มีแบบห้องให้เลือก</label>
                                                                                <div class="col-md-2">
                                                                                <input type="text" id="num_plan_condo"
                                                                                name="num_plan_condo"
                                                                                value="'.trim($itemdetailSubproject["detailcondo"][0]["num_plan_condo"]).'"
                                                                                class="form-control"
                                                                                onkeypress="return isNumber(event)"  min="1"/>
                                                                                </div>
                                                                                <div class="col-md-3">
                                                                                <label class="control-label col-md-3 wdlabel">แบบ
                                                                                </label>
                                                                                </div>
                                                                                </div>

                      <!-- unit new -->';
                      echo '<div id="unit_new">';
                      $countItemUnit = 0 ;

                      foreach ($itemdetailSubproject["detailUnitCondo_new"] as $itemUnit)
                      {
                        echo '<div class="form-group">
                        <label class="control-label col-md-3">ชื่อ Unit </label>
                        <div class="col-md-2">
                        <input type="text" id="unit_condo_name"
                        name="unit_condo_name[]"
                        value="'.$itemUnit['unit_condo_name'].'"
                        class="form-control col-md-7 col-xs-12">
                        </div>
                        <div class="col-md-1 col-sm-9 col-xs-12">
                        <label class="control-label col-md-3 ">&nbsp&nbspขนาด </label>
                        </div>
                        <div class="col-md-2">
                        <input type="text" id="size_unit_codo" name="size_unit_codo[]"
                        value="'.$itemUnit['size_unit_codo'].'"
                        class="form-control col-md-7 col-xs-12" >
                        </div>
                        <div class="col-md-3" style="width:90px">
                        <label class="control-label"> ตารางเมตร
                        </label>
                        </div>';

                        if ( $countItemUnit == 0 )
                        {

                            echo '';
                        }else {
                            echo  '<button style="margin-top: 37px;" type = "button" onclick = "removeUnitNew(this)" class="btn btn-round btn-danger" >- ลบข้อมูล Unit </button >';
                        }
                        $countItemUnit = $countItemUnit + 1 ;

                        //echo '</div>';

                    }
                   // echo '</div>';
                    echo '</div></div>';

                    echo '<div class="form-group">
                    <label class="control-label col-md-3" for="first-name"><span class="required" aria-required="true"></span>
                    </label>
                    <div class="col-md-2">
                    <button type = "button" onclick = "add_UnitNew()" class="btn btn-round btn-success" >
                    + เพิ่ม Unit
                    </button>
                    </div>
                    </div>


                                                                                <div class="form-group">
                                                                                <label class="control-label col-md-3">จำนวนอาคาร</label>
                                                                                <div class="col-md-2">
                                                                                <input type="text" id="building_uni" name="building_uni"
                                                                                value="' . $itemdetailSubproject["detailcondo"][0]["building_uni"] . '"
                                                                                class="form-control"
                                                                                onkeypress="return isNumber(event)"  >
                                                                                </div>
                                                                                <div class="col-md-3">
                                                                                <label class="control-label col-md-3 wdlabel">อาคาร
                    </label>
                    </div>
                    </div>';
                    echo '<div class="form-group">
                    <label class="control-label col-md-3">จำนวนชั้น</label>
                    <div class="col-md-2">
                    <input type="text" id="floor"
                    name="s_floor"
                    value="'.$itemdetailSubproject["detailcondo"][0]["floor"].'"
                    class="form-control"
                    onkeypress="return isNumber(event)" />
                    </div>
                    <div class="col-md-3">
                    <label class="control-label col-md-3 wdlabel">ชั้น
                                                                                </label>
                                                                                </div>
                                                                                </div>';
                                                                                echo '<div id="subdetailCondoOptionBuilding">';
                                                                                $countItemBuildin = 0 ;
                                                                                foreach ($itemdetailSubproject["optionbuilding"] as $itembuilding)
                                                                                {
                                                                                    echo '<div class="form-group">
                                                                                    <label class="control-label col-md-3" for="group">อาคาร </label>
                                                                                    <div class="col-md-2 col-sm-9 col-xs-12">

                                                                                    <select class="form-control" name="building_name[]"
                                                                                    id="building_name[]">';

                                                                                    foreach ($fixvaluebuild as $itemfixvaluebuild) {
                                                                                        if ($itembuilding["building_name"] == $itemfixvaluebuild[1])
                                                                                        {
                                                                                            echo  '<option value ="'.$itemfixvaluebuild[1].'" selected >'.$itemfixvaluebuild[1].'</option >';
                                                                                        } else
                                                                                        {
                                                                                            echo  '<option value ="'.$itemfixvaluebuild[1].'" >'.$itemfixvaluebuild[1].'</option >';
                                                                                        }
                                                                                    }
                                                                                    echo '</select>';
                                                                                    echo  '</div>';
                                                                                    echo '<label class="control-label col-md-1 textleft"
                                                                                    >จำนวนชั้น<span
                                                                                    class="required"></span>
                                                                                    </label>';
			echo '<div class="col-md-1">
                                                                                    <input type="text" name="building_num_layer[]"
                                                                                    value="' . $itembuilding["building_num_layer"] . '"' .
				'class="form-control col-md-7 col-xs-12"  onkeypress="return isNumber(event)">
                                                                                    </div>
                                                                                    <label class="control-label col-md-1 textleft">
                                                                                    ชั้น<span class="required"></span>
                                                                                    </label>
                                                                                    <div class="col-md-1">
                                                                                    <input type="text"
                                                                                    value="' . $itembuilding["building_unit"] . '"' .
				'name="building_unit[]"
                                                                                    class="form-control col-md-7 col-xs-12"  onkeypress="return isNumber(event)">
                                                                                    </div>
                                                                                    <label class="control-label col-md-1 textleft">
                                                                                    ยูนิต<span
                                                                                    class="required"></span>
                                                                                    </label>';

                                                                                    if ( $countItemBuildin == 0 )
                                                                                    {

				echo '';
			} else {
				echo '<button type = "button" onclick = "removeDetailBuildCondo(this)" class="btn btn-round btn-danger" >- ลบข้อมูลอาคาร </button >';
			}
			$countItemBuildin = $countItemBuildin + 1;
			echo '</div>';
		}
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group">
                                                                                <label class="control-label col-md-3" for="first-name"><span class="required"></span>
                                                                                </label>
                                                                                <div class="col-md-6">
                                                                                <button type = "button" onclick = "add_DetailBuildCondo()" class="btn btn-round btn-success" >
                                                                                + เพิ่มข้อมูลอาคาร
                                                                                </button>
                                                                                </div>
                                                                                </div>';
	}
}
?>
                                                                    </div>
                                                                    <!-- ./ class=form-group id=detail_div_checkbox -->
                                                                    <br>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">รหัสบริษัท(DL200) <span
                                                                            class="required">*</span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <input type="text" id="company_no_DL200_id" name="company_no_DL200"
                                                                            value="<?php echo trim($project[0]['company_no_DL200']); ?>"
                                                                            class="form-control col-md-7 col-xs-12" required >
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">รหัสโครงการ(DL200) <span
                                                                            class="required">*</span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <input type="text" id="project_no_DL200_id" name="project_no_DL200"
                                                                            value="<?php echo trim($project[0]['project_no_DL200']); ?>"
                                                                            class="form-control col-md-7 col-xs-12" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">พื้นที่โครงการ <span
                                                                            class="required"></span>
                                                                        </label>
                                                                        <div class="col-md-1">
                                                                            <input type="text" id="area_farm"
                                                                            name="area_farm"
                                                                            onkeypress="return isNumber(event)"
                                                                            value="<?php echo trim($project[0]['area_farm']) ?>"
                                                                            class="form-control col-md-7 col-xs-12" >
                                                                        </div>
                                                                        <label class="control-label col-md-1 textleft"> ไร่<span
                                                                            class="required"></span>
                                                                        </label>
                                                                        <div class="col-md-1">
                                                                            <input type="text" id="area_square_m" name="area_square_m"
                                                                            value="<?php echo trim($project[0]['area_square_m']) ?>"
                                                                            onkeypress="return isNumber(event)"
                                                                            class="form-control col-md-7 col-xs-12">
                                                                        </div>
                                                                        <label class="control-label col-md-1 textleft"> งาน<span
                                                                            class="required"></span>
                                                                        </label>
                                                                        <div class="col-md-1">
                                                                            <input type="text" id="area_square_w"
                                                                            value="<?php echo trim($project[0]['area_square_w']) ?>"
                                                                            name="area_square_w"
                                                                            class="form-control col-md-7 col-xs-12"
                                                                            onkeypress="return isNumber(event)">
                                                                        </div>
                                                                        <label class="control-label col-md-1 textleft"> ตารางวา<span
                                                                            class="required"></span>
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">จำนวนห้อง <span
                                                                            class="required"></span>
                                                                        </label>
                                                                        <div class="col-md-2">
                                                                            <input type="text" id="area_land"
                                                                            onkeypress="return isNumber(event)"
                                                                            name="area_land"
                                                                            value="<?php echo trim($project[0]['area_land']); ?>"
                                                                            class="form-control col-md-4" >

                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <label class="control-label col-md-3 wdlabel">ยูนิต
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <br>
                                                                    <hr>
                                                                    <h4><u>ระบุราคาขายโครงการ (เลือกรูปแบบอย่างใดอย่างหนึ่ง) </u></h4>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-2"><span
                                                                            class="required"></span>
                                                                        </label>
                                                                        <div class="col-md-10">
                                                                            <div class="radio">
                                                                                <label>
                                                                                    <?php

                                                                                    if( $detailPrice[0]['price_mode'] == "1")
                                                                                    {
                                                                                        $price_c =  $detailPrice[0]['project_price'];
                                                                                        if($price_c == ' '){
                                                                                            $price_c ='';
                                                                                        }
                                                                                        echo '<input type = "radio" checked = "checked" value = "1" id="ch1" class="v_price flat"
                                                                                        name = "pire_mode[]" /> ราคาเริ่มต้น
                                                                                        <input class="from-prire inputprice "
                                                                                        type = "text" id="price1"
                                                                                        name = "value_pire_mod[]"  value ="' . $price_c . '"  min="1"/>
                                                                                        บาท<span style = "color:#929292;" > (เช่น 3,000,000 บาท เป็นต้น)</span >';
} else {
	echo '<input type = "radio" id="ch1" checked = "" value = "1" class="v_price flat"
                                                                                        name = "pire_mode[]" /> ราคาเริ่มต้น
                                                                                        <input type = "text" class="from-prire inputprice "
                                                                                        name = "value_pire_mod[]" id="price1" value ="" min="1"/>
                                                                                        บาท <span  style = "color:#929292;" > (เช่น 3,000,000 บาท เป็นต้น)</span >';
}
?>

                                                                                </label>
                                                                            </div>
                                                                            <div class="radio">
                                                                                <label>
                                                                                    <?php if ($detailPrice[0]['price_mode'] == "2") {
	echo '<input type = "radio" checked = "checked" value = "2" name = "pire_mode[]" class="v_price2 flat" id="ch2"/> ราคา
                                                                                        <input type = "text"
                                                                                        name = "value_pire_mod_begine" id="price2"
                                                                                        value = "' . $detailPrice[1]['value_pire_mod_begine'] . '"
                                                                                        placeholder = "" class="from-prire inputprice" min="1"/> -
                                                                                        <input class="from-prire inputprice"
                                                                                        type = "text" name = "value_pire_mod_end" value = "' . $detailPrice[2]["value_pire_mod_end"] . '" placeholder = ""  id="price3"/> บาท<span
                                                                                        style = "color:#929292;" > (เช่น 3,000,000 - 6,000,000 บาท เป็นต้น)</span >';
} else {
	echo '<input type = "radio"  value = "2" name = "pire_mode[]" class="v_price2 flat" id="ch2"/> ราคา
                                                                                        <input type = "text" class="from-prire-two inputprice"
                                                                                        name = "value_pire_mod_begine"
                                                                                        value = "" id="price2"
                                                                                        placeholder = "" min="1"/> -
                                                                                        <input class="from-prire-two inputprice" type = "text" name = "value_pire_mod_end" value = "" placeholder = "" id="price3"/> บาท<span
                                                                                        style = "color:#929292;" > (เช่น 3,000,000 - 6,000,000 บาท เป็นต้น)</span >';

}
?>
                                                                                </label>
                                                                            </div>

                                                                            <div class="radio">
                                                                                <?php

if ($detailPrice[0]['price_mode'] == "3") {
	$price = $detailPrice[0]['project_price'];
//
	echo '<label>' .
		'<input type = "radio" checked = "checked" value = "3" id="ch4"
                                                                                    name = "pire_mode[]" class="v_price_condo flat" disabled/> ราคา
                                                                                    <input type = "text" class="from-prire inputprice"
                                                                                    value = "' . $price . '"
                                                                                    name = "value_pire_mod_condo[]" id="price4"
                                                                                    style="margin-left: 35px;background-color: #e7e7e7 !important;"
                                                                                    placeholder = "" min="1" disabled/>
                                                                                    บาท / ตารางเมตร
                                                                                    <span style = "color:#929292;" > (เช่น 120,000 บาท / ตารางเมตร เป็นต้น)</span >
                                                                                    </label >';
                                                                                }
                                                                                else
                                                                                    {  echo '<label>
                                                                                <input type="radio" value="3" id="ch4"
                                                                                name="pire_mode[]" class="v_price_condo flat" disabled/> ราคา
                                                                                <input type="text" class="from-prire inputprice"
                                                                                name="value_pire_mod_condo[]" id="price4"
                                                                                style="margin-left: 35px;background-color: #e7e7e7 !important;" min="1"  disabled/>
                                                                                บาท/ตารางเมตร
                                                                                <span style="color:#929292;"> (เช่น 120,000 บาท/ตารางเมตร เป็นต้น)</span>
                                                                                </label>';

}

?>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <?php

$classHiddenMasterPlanCondo = "hidden";
$issetDataMasterPlan = isset($detailSubproject);
$find_index_masterplan = null;

//check isset data detail of all product
if ($issetDataMasterPlan) {

	//recheck isset data detail of all product
	if (count($detailSubproject) > 0) {
		for ($i = 0; $i < sizeof($detailSubproject); $i++) {
			if (array_key_exists("dataMasterPlanCondo", $detailSubproject[$i])) {
				$find_index_masterplan = $i;
			}
		}

		if (!is_null($find_index_masterplan)) {
			$classHiddenMasterPlanCondo = array_key_exists("dataMasterPlanCondo", $detailSubproject[$find_index_masterplan]) ? "" : "hidden";
		}
	}

}
?>

                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3" for="first-name"></label>
                                                                    <div class="col-md-6">

                                                                        <label for="price1"  class="error"></label>
                                                                        <label for="price3"  class="error"></label>
                                                                        <label for="price2"  class="error"></label>
                                                                        <label for="price4"  class="error"></label>
                                                                    </div>
                                                                </div>

                                                                <br>
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">แนวคิดโครงการ <br> (กรอกข้อมูลไม่เกิน 200 ตัวอักษร)

                                                                    </label>
                                                                    <div class="col-md-6">
                                                                        <textarea id="message1" class="form-control" name="concept_th" maxlength="200"
                                                                        rows="7" placeholder="กรอกแนวคิดโครงการ (จีน)"><?php echo trim($detailConcept[0]['concept_th']) ?></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group hide-for-th">
                                                                    <label class="control-label col-md-3">แนวคิดโครงการ <br> (กรอกข้อมูลไม่เกิน 200 ตัวอักษร)

                                                                    </label>
                                                                    <div class="col-md-6">
                                                                        <textarea id="message" class="form-control" name="concept_en" maxlength="200"
                                                                        rows="7" placeholder="กรอกแนวคิดโครงการ (อังกฤษ)"><?php echo trim($detailConcept[0]['concept_en']) ?></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">จุดเด่นของโครงการ <br>
                                                                    </label>
                                                                    <div class="col-md-6">
                                                                        <textarea  class="form-control" name="distinctive_th"
                                                                        rows="7" placeholder="กรอกจุดเด่นโครงการ (จีน)"><?php echo trim($detailConcept[0]['distinctive_th']) ?></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group hide-for-th">
                                                                    <label class="control-label col-md-3">จุดเด่นของโครงการ <br>
                                                                    </label>
                                                                    <div class="col-md-6">
                                                                        <textarea id="message" class="form-control" name="distinctive_en"
                                                                        rows="7" placeholder="กรอกจุดเด่นโครงการ (อังกฤษ)"><?php echo trim($detailConcept[0]['distinctive_en']) ?></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">สิ่งอำนวยความสะดวก
                                                                        <br>
                                                                    </label><br>
                                                                    <div class="col-md-6">
                                                                        <textarea id="message" class="form-control" name="facilities_th"
                                                                        rows="4" placeholder="กรอกสิ่งอำนวยความสะดวก (จีน)"><?php echo trim($detailConcept[0]['facilities_th']) ?></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group hide-for-th">
                                                                    <label class="control-label col-md-3">สิ่งอำนวยความสะดวก
                                                                        <br>
                                                                    </label><br>
                                                                    <div class="col-md-6">
                                                                        <textarea id="message" class="form-control" name="facilities_en"
                                                                        rows="4" placeholder="กรอกสิ่งอำนวยความสะดวก (อังกฤษ)"><?php echo trim($detailConcept[0]['facilities_en']) ?></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">ระบบรักษาความปลอดภัย <br>
                                                                    </label>
                                                                    <div class="col-md-6">
                                                                        <textarea id="message" class="form-control" name="security_th"
                                                                        rows="4" placeholder="กรอกระบบรักษาความปลอดภัย (จีน)"><?php echo trim($detailConcept[0]['security_th']) ?></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group hide-for-th">
                                                                    <label class="control-label col-md-3">ระบบรักษาความปลอดภัย <br>
                                                                    </label>
                                                                    <div class="col-md-6">
                                                                        <textarea id="message" class="form-control" name="security_en"
                                                                        rows="4" placeholder="กรอกระบบรักษาความปลอดภัย (อังกฤษ)"><?php echo trim($detailConcept[0]['security_en']) ?></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">ข้อมูล สคบ. <br>
                                                                    </label>
                                                                    <div class="col-md-6">
                                                                        <textarea id="message" class="form-control" name="information_th"
                                                                        rows="7" placeholder="กรอกข้อมูล สคบ. (จีน)"><?php echo trim($detailConcept[0]['information_th']) ?></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group hide-for-th">
                                                                    <label class="control-label col-md-3">ข้อมูล สคบ. <br>
                                                                    </label>
                                                                    <div class="col-md-6">
                                                                        <textarea id="message" class="form-control" name="information_en"
                                                                        rows="7" placeholder="กรอกข้อมูล สคบ. (อังกฤษ)"><?php echo trim($detailConcept[0]['information_en']) ?></textarea>

                                                                    </div>
                                                                </div>

                                                                <?php
$sql_p = "select paking ,paking_en ,location,location_en from lh_projects where project_id = '" . $_GET['project_id'] . "'";
$query_p = mssql_query($sql_p);
$data_paking = mssql_fetch_array($query_p);

?>

                                                                <div id="divPraking" class="<?php echo $classHiddenMasterPlanCondo ?>">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">ข้อมูล สถานที่ตั้ง*<br>
                                                                        </label>
                                                                        <div class="col-md-6">
                                                                            <textarea id="message" class="form-control" name="location_th"
                                                                            rows="4" placeholder="กรอกข้อมูล สถานที่ตั้ง (จีน)"><?php echo $data_paking['location'] ?></textarea>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group hide-for-th">
                                                                        <label class="control-label col-md-3">ข้อมูล สถานที่ตั้ง <br>
                                                                        </label>
                                                                        <div class="col-md-6">
                                                                            <textarea  class="form-control" name="location_en"
                                                                            rows="4" placeholder="กรอก้อมูล สถานที่ตั้ง (อังกฤษ)"><?php echo $data_paking['location_en'] ?></textarea>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">ข้อมูลที่จอดรถ *<br>
                                                                        </label>
                                                                        <div class="col-md-6">
                                                                            <textarea  class="form-control" name="paking_th"
                                                                            rows="4" placeholder="กรอกข้อมูลที่จอดรถ (จีน)"><?php echo $data_paking['paking']; ?></textarea>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group hide-for-th">
                                                                        <label class="control-label col-md-3">ข้อมูลที่จอดรถ <br>
                                                                        </label>
                                                                        <div class="col-md-6">
                                                                            <textarea  class="form-control" name="paking_en"
                                                                            rows="4" placeholder="กรอกข้อมูล ที่จอดรถ (อังกฤษ)"><?php echo $data_paking['paking_en'] ?></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <br>
                                                                <hr>
                                                                <h4><u>ข้อมูลทำเลที่ตั้งและสถานที่ใกล้เคียงโครงการ</u></h4>
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3" for="latitude">Latitude <span
                                                                        class="required"></span>
                                                                    </label>
                                                                    <div class="col-md-2">
                                                                        <input type="text" id="latitude" name="latitude"
                                                                        value="<?php echo trim($project[0]['latitude']); ?>"
                                                                        class="form-control col-md-7 col-xs-12" onkeypress="return isNumber(event)" placeholder="กรอกละติจูด">
                                                                    </div>
                                                                    <label class="control-label col-md-1 textleft"> Longtitude<span
                                                                        class="required"></span>
                                                                    </label>
                                                                    <div class="col-md-2">
                                                                        <input type="text" id="longtitude"
                                                                        name="longtitude"
                                                                        value="<?php echo trim($project[0]['longtitude']); ?>"
                                                                        class="form-control col-md-7 col-xs-12" onkeypress="return isNumber(event)" placeholder="กรอกลองติจูด">
                                                                    </div>

                                                                    <div class="col-md-4">
                                                                        <a href="fileupload/Howto_LatLong.pdf" target="_blank"><u><h4>วิธีการหาค่า Latitude,Longtitude คลิก</h4></u></a>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3" for="latitude"> ระบุ URL (จาก Google Local Business) <span
                                                                        class="required"></span>
                                                                    </label>
                                                                    <div class="col-md-7">
                                                                        <input type="text"  name="map_link"
                                                                        value="<?php echo trim($project[0]['map_link']); ?>"
                                                                        class="form-control col-md-10 col-xs-12"  placeholder=" ระบุ URL (จาก Google Local Business) ">
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">สิ่งอำนวยความสะดวกรอบโครงการ <span
                                                                        class="required"></span>
                                                                    </label>
                                                                </div>

                                                                <div id="project_nearby">
                                                                    <?php
                                                                    $index_project_nearby = 1 ;
                                                                    ?>
                                                                    <?php foreach($detailNearBy as $itemDetailNearBy ) { ?>
                                                                    <div class="group-nearby">
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3 setIndexProjectNear" ><?php echo ($index_project_nearby) ?>)</label>
                                                                            <div class="col-md-6">
                                                                                <input type="text" name="nearby_name_th[]" value="<?php echo trim($itemDetailNearBy['nearby_name_th']); ?>"
                                                                                class="form-control col-md-7 col-xs-12 wdpercent3"
                                                                                placeholder="กรอกสิ่งอำนวยความสะดวกรอบโครงการ (จีน)">
                                                                                <label class="control-label col-md-1 wdlabel1 textright"> ระยะห่าง</label>
                                                                            </div>
                                                                            <div class="col-md-2" style=" width: 10%;margin-left: -70px; ">
                                                                                <input type="text" name="interval[]"
                                                                                value="<?php echo trim($itemDetailNearBy['interval']); ?>"
                                                                                class="form-control col-md-7 col-xs-12 wdpercent2" style="width: 70%;"
                                                                                onkeypress="return isNumber(event)">
</div>
                <div class="col-mg-4">
                    <select class="form-control col-md-2 wdpercent2" name="nearby_unit[]" style="width: 90px;">
                        <?php 
                        $selec1 = '';
                        $selec2 = '';
                        if($itemDetailNearBy['nearby_unit'] == 'กม.'){
                            $selec1 = 'selected';
                        }else if($itemDetailNearBy['nearby_unit'] == 'เมตร'){
                            $selec2 = 'selected';
                        }
                        ?>
                        <option value="กม." <?=$selec1?>>กม.</option>
                        <option value="เมตร" <?=$selec2?>>เมตร</option>
                    </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3"></label>
                                                                            <div class="col-md-6">
                                                                                <input type="text" name="nearby_name_en[]" value="<?php echo trim($itemDetailNearBy['nearby_name_en']); ?>"
                                                                                placeholder="กรอกสิ่งอำนวยความสะดวกรอบโครงการ (อังกฤษ)"
                                                                                class="form-control col-md-7 col-xs-12 wdpercent3 hide-for-th">
                                                                            </div>
                                                                            <?php if ($index_project_nearby > 0) {?>
                                                                            <button type="button" class="btn btn-round btn-danger" onclick="removeProject_nearby(this)" >-
                                                                                ลบสิ่งอำนวยความสะดวก
                                                                            </button>
                                                                            <?php } $index_project_nearby++; ?>
                                                                        </div>
                                                                    </div> <!-- end group-nearby -->
                                                                    <?php } //end foreach ?>
                                                                </div> <!-- end project_nearby -->

                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">
                                                                    </label>
                                                                    <div class="col-md-3">
                                                                        <button style="margin-left: -10px;" type="button" class="btn btn-round btn-success" onclick="addProject_nearby()" >
                                                                            + เพิ่มสิ่งอำนวยความสะดวก
                                                                        </button>
                                                                    </div>
                                                                </div>

                                                                <br>
                                                                <hr>
                                                                <h4><u>ข้อมูลติดต่อโครงการ</u> </h4>

                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">เปิดทำการ <span
                                                                        class="required">*</span>
                                                                    </label>
                                                                    <div class="col-md-6 col-sm-9 col-xs-12">
                                                                        <div class="radio">
                                                                            <label>
                                                                                <input type="radio" onclick="changOpenDay(value)" id="open-day-all" value="all" class="flat"
                                                                                <?php if ($detailContact[0]["open_day"] == 'all') {
	echo "checked";
}?>
                                                                                name="open-day" /> เปิดทำการทุกวัน
                                                                            </label>
                                                                        </div>

                                                                        <div class="radio">
                                                                            <label>
                                                                                <input type="radio" onclick="changOpenDay(value)" id="open-day-sum" value="sum-day" class="flat"
                                                                                <?php if ($detailContact[0]["open_day"] != 'all') {
	echo "checked";
}?>
                                                                                name="open-day" /> เปิดทำการทุกวันยกเว้นวัน
                                                                            </label>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-1" for="first-name"></label>
                                                                            <div class="col-md-6">

                                                                                <label for="close-day[]"  class="error"></label>

                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-4">
                                                                            <div class="checkbox-group required" id="close-day">
                                                                                <?php
$valueAllOneDay = array(
	"0" => 'วันหยุดนักขัตฤกษ์',
	"1" => 'วันจันทร์',
	"2" => 'วันอังคาร',
	"3" => 'วันพุธ',
	"4" => 'วันพฤหัสบดี',
	"5" => 'วันศุกร์',
	"6" => 'วันเสาร์',
	"7" => 'วันอาทิตย์',
);

$arrCloseDay = array();
$status = true;
$i = 0;
if ($detailContact[0]["open_day"] != 'all') {
	$arrCloseDay = explode(",", $detailContact[0]["open_day"]);
}
foreach ($valueAllOneDay as $key => $value) {
	$isData = strval(array_search(strval($key), $arrCloseDay));
	if ($isData != "") {
		?>
                                                                                        <input type="checkbox" name="close-day[]" value="<?php echo $key; ?>" data-parsley-mincheck="1"  class="flat" checked> <?php echo $value; ?>
                                                                                        <br>
                                                                                        <?php } else {?>
                                                                                        <input type="checkbox" name="close-day[]" value="<?php echo $key; ?>" data-parsley-mincheck="1"  class="flat" /> <?php echo $value; ?>
                                                                                        <br>
                                                                                        <?php }?>
                                                                                        <?php $i++;}?>
                                                                                        <br>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3">เปิดทำการ <span
                                                                                class="required">*</span>
                                                                            </label>
                                                                            <label class="control-label col-md-1">ตั้งแต่เวลา <span
                                                                                class="required"></span>
                                                                            </label>
                                                                            <div class="col-md-1">
                                                                                <input type="text" name="open_time_start" value="<?php if ($detailContact[0]['open_time_start'] == "") {echo "09.00";} else {echo trim($detailContact[0]['open_time_start']);}?>"
                                                                                class="form-control col-md-7 col-xs-12" required onkeypress="return isNumber(event)" maxlength="5">
                                                                            </div>
                                                                            <label class="control-label col-md-1 textleft"> ถึงเวลา<span
                                                                                class="required"></span>
                                                                            </label>
                                                                            <div class="col-md-1">
                                                                                <input type="text" name="open_time_end" value="<?php if ($detailContact[0]['open_time_end'] == "") {echo "17.30";} else {echo trim($detailContact[0]['open_time_end']);}?>"
                                                                                class="form-control col-md-7 col-xs-12" required onkeypress="return isNumber(event)" maxlength="5">
                                                                            </div>
                                                                            <span style="color:#929292;"> (เช่น 09.00 ถึง 17.00)</span>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3">เบอร์โทรศัพท์ <span
                                                                                class="required">*</span>
                                                                            </label>
                                                                            <div class="col-md-2">
                                                                                <input type="text" name="telephone" value="<?php if ($detailContact[0]['open_time_end'] == "") {echo "1198";} else {echo trim($detailContact[0]['telephone']);}?>"
                                                                                class="form-control col-md-7 col-xs-12" required onkeypress="return isNumber(event)">
                                                                            </div>
                                                                            <span style="color:#929292;"> *ระบุ 1198</span>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3">E-mail Address <span
                                                                                class="required">*</span>
                                                                            </label>
                                                                            <div class="col-md-2">
                                                                                <input type="text" name="email" value="<?php echo str_replace('@lh.co.th', '', $detailContact[0]['email']); ?>"
                                                                                class="form-control" required>

                                                                            </div>
                                                                            <label class="control-label col-md-1 textleft"> @lh.co.th<span
                                                                                class="required"></span>
                                                                            </label>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3" >เบอร์โทรสายตรงโครงการ
                                                                                <span class="required">*</span>
                                                                            </label>
                                                                            <div class="col-md-2">
                                                                                <input type="text" name="telephone_line" value="<?php echo trim($detailContact[0]['telephone_line']); ?>"
                                                                                class="form-control col-md-7 col-xs-12" required>
                                                                            </div>
                                                                        </div>

                                                                        <?php $sqlzone_flm = "SELECT * FROM LH_PROJECT_ZONE WHERE project_id = '" . $_GET['project_id'] . "' ORDER BY zone_id";
$queryzoneflm = mssql_query($sqlzone_flm);

$i = 0;
$j = 1;
while ($zonedataflm = mssql_fetch_array($queryzoneflm)) {

	?>

                                                                            <div class="form-group" id="flm<?=$i?>">
                                                                                <label class="control-label col-md-3" >ทำเลที่ตั้งสำหรับ FLM
                                                                                    <span class="required">*</span>
                                                                                </label>
                                                                                <div class="col-md-4">
                                                                                    <select name="flm[]" class="form-control flm" required id="flm<?=$i?>">
                                                                                        <option  value="">กรุณาเลือก</option>
                                                                                        <?php
$sql = "SELECT * FROM LH_ZONES  WHERE zone_attribute = 'websiteflm' OR zone_attribute = 'flm' ORDER BY zone_name_th ASC";
	$query = mssql_query($sql);
	while ($value = mssql_fetch_array($query)) {
		?>
                                                                                            <?php if ($value['zone_id'] == $detailzone[$i]['zone_id_flm']) {?>
                                                                                            <option value="<?php echo $value['zone_id']; ?>" selected>
                                                                                                <?php echo $value['zone_name_th']; ?>
                                                                                            </option>
                                                                                            <?php } else {?>
                                                                                            <option value="<?php echo $value['zone_id']; ?>">
                                                                                                <?php echo $value['zone_name_th']; ?>
                                                                                            </option>
                                                                                            <?php }?>
                                                                                            <?php }?>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>

                                                                                <?php $i++; $j++; }?>

                                                                                <div id="flmform"></div>

                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3"><span
                                                                                        class="required"> </span>
                                                                                    </label>
                                                                                    <label id="error-checkbox-flm" class="col-md-3" style="color: #F44300;"></label>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">ชื่อ-นามสกุลเจ้าหน้าที่ฝ่ายขายประจำโครงการ<span
                                                                                        class="required"> *</span>
                                                                                    </label><br>
                                                                                    <div class="col-md-4">
                                                                                        <input type="text" name="name_staff" value="<?php echo trim($detailContact[0]['name_staff']); ?>"
                                                                                        class="form-control col-md-7 col-xs-12" required>
                                                                                    </div>
                                                                                </div>
                                                                                <?php
$sql_unit = "select
                                                                                lh_project_contacts.unit_id,
                                                                                lh_units.unit_name
                                                                                from lh_project_contacts
                                                                                INNER JOIN lh_units ON lh_project_contacts.unit_id = lh_units.unit_id
                                                                                where lh_project_contacts.project_id = '" . $_GET['project_id'] . "' ";
$query_unit = mssql_query($sql_unit);
$array_unit = mssql_fetch_array($query_unit);
?>
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">ระบุค่าส่วนกลาง <span
                                                                                        class="required"></span>
                                                                                    </label>
                                                                                    <div class="col-md-2">
                                                                                        <input id="central_value" type="text" name="central_value" value="<?php echo trim($detailContact[0]['central_value']); ?>"
                                                                                        class="form-control col-md-7 col-xs-12" placeholder="ระบุค่าส่วนกลาง" >
                                                                                    </div>
                                                                                    <?php if(isset($array_unit['unit_id'])){ ?>
                                                                                    <div class="col-md-2 col-sm-9 col-xs-12">
                                                                                        <select name="unit_id" class="form-control" style="width: 170px;">
                                                                                            <option value = "" selected >หน่วย</option >
                                                                                            <?php
                                                                                            foreach ($detailUnit as $value)
                                                                                            {
                                                                                                if($value['unit_id'] == $detailContact[0]['unit_id'] ) {
                                                                                                    echo '<option value = "'.$value['unit_id'].'" selected >'.$value['unit_name'].'</option >';
                                                                                                } else {
                                                                                                    echo '<option value = "'.$value['unit_id'].'" >'.$value['unit_name'].'</option >';
                                                                                                }
                                                                                            }
                                                                                            ?>
                                                                                        </select>
                                                                                    </div>
                                                                                    <?php }else  { ?>
                                                                                    <div class="col-md-2 col-sm-9 col-xs-12">
                                                                                        <select name="unit_id" class="form-control" style="width: 170px;">
                                                                                            <option value = "" selected >หน่วย</option >
                                                                                            <?php
                                                                                            foreach ($detailUnit as $value)
                                                                                            {
                                                                                                echo '<option value = "'.$value['unit_id'].'"  >'.$value['unit_name'].'</option >';
                                                                                            }
                                                                                            ?>
                                                                                        </select>
                                                                                    </div>
                                                                                    <?php }?>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">ชำระล่วงหน้าถึงวันที่
                                                                                    </label>
                                                                                    <?php
$date_start2 = $detailContact[0]['prepay'];
$dates2 = $date_start2;
$year2 = substr($dates2, 0, 4);
$day2 = substr($dates2, 8, 2);
$mont2 = substr($dates2, 5, 2);
$start_date2 = $day2 . "/" . $mont2 . "/" . $year2;
?>
                                                                                    <div class="col-md-3">
                                                                                        <fieldset>
                                                                                            <div class="control-group">
                                                                                                <div class="controls">
                                                                                                    <div class="form-group">
                                                                                                        <input type="text" class="form-control has-feedback-left"
                                                                                                        id="prepaiddate"
                                                                                                        name="prepay"
                                                                                                        value="<?php if ($detailContact[0]['prepay'] == "") {echo "";} else {echo $start_date2;}?>"
                                                                                                        placeholder="ระบุวันชำระล่วงหน้า" readonly>
                                                                                                        <span class="fa fa-calendar-o form-control-feedback left"
                                                                                                        aria-hidden="true"></span>
                                                                                                        <span id="inputSuccess2Status"
                                                                                                        class="sr-only">(success)</span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </fieldset>
                                                                                    </div>
                                                                                </div>



                                                                                <!--                                    Lead image-->



                                                                                <?php if($_SESSION['group_status']== 'Admin'){ ?>
                                                                                <br>
                                                                                <hr>
                                                                                <h4><u>ข้อมูลภาพ/แผนที่/ไฟล์ดาวน์โหลด (Admin ดำเนินการ)</u></h4>


                                                                                <div class="form-group" >
                                                                                    <label class="control-label col-md-3">แผนที่โครงการ <br> (.jpg,.png,.gif ขนาด 1920 x 1080 px) <br> ภาพขนาดไม่เกิน 300 KB
                                                                                    </label><br>
                                                                                    <div class="col-md-3">
                                                                                        <div id="map-image-holder"></div>
                                                                                        <?php if (isset($projectMap[0]['map_img']) && ($projectMap[0]['map_img'] != " ")) {?>
                                                                                        <div class="file-preview ">
                                                                                            <div class="close fileinput-remove"></div>
                                                                                            <div class="file-drop-disabled">
                                                                                                <div class="file-preview-thumbnails">
                                                                                                    <div class="file-initial-thumbs">
                                                                                                        <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                                                                                            <div class="kv-file-content">
                                                                                                                <a class="fancybox" href="<?php echo $projectMap[0]['map_img']; ?>" target="_blank">
                                                                                                                    <img src="<?php echo $projectMap[0]['map_img']; ?>" class="kv-preview-data file-preview-image"  style="width:150px;height:auto;">
                                                                                                                </a>
                                                                                                            </div>
                                                                                                            <div class="file-thumbnail-footer">
                                                                                                                <p>Alt Text : <?php echo $projectMap[0]['map_img_seo']; ?>
                                                                                                                </p>
                                                                                                                <div class="file-actions">
                                                                                                                    <div class="file-footer-buttons">
                                                                                                                        <button type="button" id="<?php echo $projectMap[0]['project_id']; ?>" onclick="deleteJpgpdf(<?php echo $_GET['project_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                                                                                        <a class="fancybox" href="<?php echo $projectMap[0]['map_img']; ?>" target="_blank">
                                                                                                                            <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                                                                                        </a>
                                                                                                                    </div>
                                                                                                                    <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="clearfix"></div>
                                                                                                <div class="file-preview-status text-center text-success"></div>
                                                                                                <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <?php } else {?>
                                                                                        <input type="file" id="map-image" name="map-image" class="file-loading" accept="image/*">
                                                                                        <?php }?>

                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">แผนที่โครงการ <br>(.PDF) </label>
                                                                                    <div class="col-md-3">
                                                                                        <div id="map-image-holder-brochure"></div>
                                                                                        <?php if (isset($projectMap[0]['map_pdf']) && ($projectMap[0]['map_pdf'] != " ")) {
		?>
                                                                                            <div class="file-preview ">
                                                                                                <div class="close fileinput-remove"></div>
                                                                                                <div class="file-drop-disabled">
                                                                                                    <div class="file-preview-thumbnails">
                                                                                                        <div class="file-initial-thumbs">
                                                                                                            <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                                                                                                <div class="kv-file-content">
                                                                                                                    <a class="fancybox" href="<?php echo $projectMap[0]['map_pdf']; ?>" target="_blank">
                                                                                                                        <i class="fa fa-file-pdf-o" aria-hidden="true" style="font-size: 100px; margin-bottom: 10px;"></i>
                                                                                                                    </a>
                                                                                                                </div>
                                                                                                                <div class="file-actions">
                                                                                                                    <div class="file-footer-buttons">
                                                                                                                        <button type="button" id="<?php echo $projectMap[0]['project_id']; ?>" onclick="deleteMappdf(<?php echo $_GET['project_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                                                                                        <a class="fancybox" href="<?php echo $projectMap[0]['map_pdf']; ?>" target="_blank">
                                                                                                                            <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                                                                                        </a>
                                                                                                                    </div>
                                                                                                                    <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="clearfix"></div>
                                                                                                    <div class="file-preview-status text-center text-success"></div>
                                                                                                    <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <?php } else {?>
                                                                                            <input type="file" id="map-pdf" name="map-pdf"  class="file-loading" accept="application/pdf">
                                                                                            <?php }?>

                                                                                        </div>
                                                                                    </div>

                                                                                    <?php $requiredlogo = "required";?>
                                                                                    <div class="form-group">
                                                                                        <label class="control-label col-md-3">LOGO โครงการ <span class="required">*</span><br>(.PNG
                                                                                            ขนาด 150 x 150 px) <br> ภาพขนาดไม่เกิน 300 KB
                                                                                        </label><br>
                                                                                        <div class="col-md-3">
                                                                                            <div id="map-image-holder-logo"></div>
                                                                                            <?php if (isset($projectMap[0]['project_logo']) && $projectMap[0]['project_logo'] != " ") {
		$requiredlogo = '';?>
                                                                                                <div class="file-preview ">
                                                                                                    <div class="close fileinput-remove"></div>
                                                                                                    <div class="file-drop-disabled">
                                                                                                        <div class="file-preview-thumbnails">
                                                                                                            <div class="file-initial-thumbs">
                                                                                                                <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                                                                                                    <div class="kv-file-content">
                                                                                                                        <a class="fancybox" href="<?php echo $projectMap[0]['project_logo']; ?>">
                                                                                                                            <img src="<?php echo $projectMap[0]['project_logo']; ?>" class="kv-preview-data file-preview-image"  style="height: auto; max-width: 150px;">
                                                                                                                        </a>
                                                                                                                    </div>
                                                                                                                    <div class="file-thumbnail-footer">
                                                                                                                        <p>Alt Text :<?php echo $projectMap[0]['project_logo_seo']; ?>
                                                                                                                        </p>
                                                                                                                        <div class="file-actions">
                                                                                                                            <div class="file-footer-buttons">
                                                                                                                                <button type="button" id="<?php echo $projectMap[0]['project_id']; ?>" onclick="deleteLogo(<?php echo $_GET['project_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                                                                                                <a class="fancybox" href="<?php echo $projectMap[0]['project_logo']; ?>" target="_blank">
                                                                                                                                    <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                                                                                                </a>
                                                                                                                            </div>
                                                                                                                            <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                                                                                                        </div>

                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="clearfix"></div>
                                                                                                        <div class="file-preview-status text-center text-success"></div>
                                                                                                        <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <?php } else {?>
                                                                                                <input type="file" id="map-image-logo" name="map_image_logo" class="file-loading map-image-logo" accept="image/png" <?php echo $requiredlogo; ?> >
                                                                                                <?php }?>

                                                                                            </div>
                                                                                        </div>

                                                                                        <br>
                                                                                        <div class="form-group">
                                                                                            <label class="control-label col-md-3">Download Brochure <br>(.pdf)</label>
                                                                                            <div class="col-md-3">
                                                                                                <div id="map-image-holder-brochure"></div>
                                                                                                <?php if (isset($projectMap[0]['brochure']) && $projectMap[0]['brochure'] != " ") {?>
                                                                                                <div class="file-preview ">
                                                                                                    <div class="close fileinput-remove"></div>
                                                                                                    <div class="file-drop-disabled">
                                                                                                        <div class="file-preview-thumbnails">
                                                                                                            <div class="file-initial-thumbs">
                                                                                                                <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                                                                                                    <div class="kv-file-content">
                                                                                                                        <a class="fancybox" href="<?php echo $projectMap[0]['brochure']; ?>" target="_blank">
                                                                                                                            <i class="fa fa-file-pdf-o" aria-hidden="true" style="font-size: 100px; margin-bottom: 10px;"></i>
                                                                                                                        </a>
                                                                                                                    </div>
                                                                                                                    <div class="file-actions">
                                                                                                                        <div class="file-footer-buttons">
                                                                                                                            <button type="button" id="<?php echo $projectMap[0]['project_id']; ?>" onclick="deleteBrochure(<?php echo $_GET['project_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                                                                                            <a class="fancybox" href="<?php echo $projectMap[0]['brochure']; ?>" target="_blank">
                                                                                                                                <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                                                                                            </a>
                                                                                                                        </div>
                                                                                                                        <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="clearfix"></div>
                                                                                                        <div class="file-preview-status text-center text-success"></div>
                                                                                                        <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <?php } else {?>
                                                                                                <input type="file" id="map-image-brochure" name="map-brochure" class="file-loading">
                                                                                                <?php }?>

                                                                                            </div>
                                                                                        </div>

                                                                                        <br>
                                                                                        <!--Lead image-->
                                                                                        <hr>
                                                                                        <h4><u>Lead image โครงการ *</u></h4>
                                                                                        <div class="row">
                                                                                            <label class="control-label col-md-8">ตำแหน่งภาพ Lead image (.JPG , .PNG 1920 x 1080 px) สูงสุดไม่เกิน 5 รูป  ภาพขนาดไม่เกิน 300 KB
                                                                                            </label>
                                                                                        </div>
                                                                                        <br>
                                                                                        <div class="row">
                                                                                            <?php
$i = 0;
	$sql_lead = "SELECT * FROM LH_LEAD_IMAGE_PROJECT_FILE WHERE PROJECT_ID = '" . $_GET['project_id'] . "' AND LEAD_IMAGE_PROJECT_FILE_TYPE = 'image' ";
	$query_lead = mssql_query($sql_lead);
	while ($value_l = mssql_fetch_array($query_lead)) {
		$i++;
		?>
                                                                                                <div class="col-sm-3">
                                                                                                    <div class="form-group ">
                                                                                                        <div class="col-md-12">
                                                                                                            <!--                                                            <div class="file-preview ">-->
                                                                                                                <div class="close fileinput-remove"></div>
                                                                                                                <div class="file-drop-disabled">
                                                                                                                    <div class="file-preview-thumbnails">
                                                                                                                        <div class="file-initial-thumbs">
                                                                                                                            <div class="file-preview-frame file-preview-initial" data-fileindex="init_0" data-template="image">
                                                                                                                                <div class="kv-file-content">
                                                                                                                                    <a class="fancybox" rel="group_position_project" href="<?php echo $value_l['LEAD_IMAGE_PROJECT_FILE_NAME']; ?>">
                                                                                                                                        <img src="<?php echo $value_l['LEAD_IMAGE_PROJECT_FILE_NAME']; ?>" class="kv-preview-data file-preview-image"  style="width:150px;max-height:150px;min-height:150px;">
                                                                                                                                    </a>
                                                                                                                                </div>
                                                                                                                                <div class="file-thumbnail-footer">

                                                                                                                                    <input type="text" name="seo_lead_edit[]" class="form-control" placeholder="Alt text" value="<?=$value_l['seo_lead']?>" >
                                                                                                                                    <br>
                                                                                                                                    <input type="hidden" name="lead_image_id[]" value="<?=$value_l['LEAD_IMAGE_PROJECT_FILE_ID']?>">
                                                                                                                                    <input type="file" id="lead_image_edit_x<?=$i?>" name="lead_image_edit[]" accept="image/*"/ class="lead_image_edit">

                                                                                                                                    <script>
                                                                                                                                        $("#lead_image_edit_x<?=$i?>").fileinput({
                                                                                            uploadUrl: "upload.php", // server upload action
                                                                                            maxFileCount: 1,
                                                                                            allowedFileExtensions: ["jpg", "png", "jpeg"],
                                                                                            browseLabel: ' เลือก Lead image',
                                                                                            browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                                                                                            removeLabel: 'ลบ',
                                                                                            browseClass: 'btn btn-success',
                                                                                            showUpload: false,
                                                                                            showRemove:false,
                                                                                            showCaption: false,
							                                    minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
							                                    minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
							                                    maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
							                                    maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
							                                    msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                                msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                                                msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                                xxx:'lead_image_edit_seo',
                                                                dropZoneTitle : 'Lead image',
                                                                maxFileSize :300 ,
                                                                msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                                                msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                                                msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                                                msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                                                msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                                            });
                                                        </script>

                                                        <span><?="ลำดับที่ " . $i;?></span>
                                                        <div class="file-actions">
                                                            <div class="file-footer-buttons">
                                                                <?php if ($i > 1) {?>
                                                                <button type="button" id="<?php echo $value_l['LEAD_IMAGE_PROJECT_FILE_ID']; ?>" onclick="deleteLeadImageProject(<?php echo $value_l['LEAD_IMAGE_PROJECT_FILE_ID']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                                <?php }?>
                                                                <a class="fancybox" href="<?php echo $value_l['LEAD_IMAGE_PROJECT_FILE_NAME']; ?>" target="_blank">
                                                                    <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                                </a>
                                                            </div>
                                                            <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                                                        </div>-->
                                            </div>

                                            <div class="clearfix"></div>
                                            <div class="file-preview-status text-center text-success"></div>
                                            <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                              <div class="col-md-12">
                                  <input type="text"  class="form-control" placeholder="EX : http://lh.co.th or https://lh.co.th" name="lead_image_edit[]" value="<?=$value_l['img_url'];?>" />
                              </div>
                          </div>
                      </div>


                      <?php }?>

                  </div>
                  <div class="row">
                    <div class="form-group">
                        <?php
$sql_lead = "SELECT * FROM LH_LEAD_IMAGE_PROJECT_FILE WHERE PROJECT_ID = '" . $_GET['project_id'] . "'";
	$query_lead = mssql_query($sql_lead);
	$num = mssql_num_rows($query_lead);
	if ($num <= 0) {
		?>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <input type="file" id="lead_image0" name="lead_image[]" accept="image/*"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <input type="text"  class="form-control" placeholder="URL" name="lead_image_add[]"/>
                                    </div>
                                </div>
                            </div>
                            <?php }?>

                            <div id="lead_image_project"></div>

                            </div>


                        </div>

                        <br>
                        <div class="form-group">
                            <label class="control-label col-md-1">
                            </label>
                            <div class="col-md-4">
                                <button id="lead_image_add" type="button" class="btn btn-round btn-success" onclick="add_lead_image()" >+
                                    เพิ่มรูป Lead image
                                </button>
                            </div>
                        </div>


                        <script>
                            $("#lead_image0").fileinput({
                                                uploadUrl: "upload.php", // server upload action
                                                maxFileCount: 1,
                                                allowedFileExtensions: ["jpg", "png", "jpeg"],
                                                browseLabel: ' เลือก Lead image',
                                                browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                                                removeLabel: 'ลบ',
                                                browseClass: 'btn btn-success',
                                                showUpload: false,
                                                showRemove:false,
                                                showCaption: false,
                                                minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                                minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                                maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                                maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                                msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                                msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                xxx:'seo_lead_add',
                                                dropZoneTitle : 'Lead image',
                                                maxFileSize :300 ,
                                                msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                                msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                                msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                                msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                                msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                            });
                                        </script>
                                        <br>
                                        <hr>
                                        <h4><u>ภาพ Gallery </u> <span style="color: red">(ภาพ Gallery จะถูกนำไปให้เซลเลือกใช้ในข้อมูล Special Promotion ด้วย)</span></h4>

                                        <!-- แสดง gallery -->

                                        <?php if (isset(getGeleryByIdProject()[0])) {
		?>
                                        <div class="row">
                                            <label class="control-label col-md-3">ตำแหน่งภาพ Gallery (.JPG , .PNG , .GIF )  ภาพขนาดไม่เกิน 300 KB
                                            </label>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <?php
$i = 0;
		foreach (getGeleryByIdProject() as $value) {
			$i++;
			?>
                                                <div class="col-sm-3">
                                                    <div class="form-group ">
                                                        <div class="col-md-12">
                                                            <!--                                                            <div class="file-preview ">-->
                                                                <div class="close fileinput-remove"></div>
                                                                <div class="file-drop-disabled">
                                                                    <div class="file-preview-thumbnails">
                                                                        <div class="file-initial-thumbs">
                                                                            <div class="file-preview-frame file-preview-initial" data-fileindex="init_0" data-template="image">
                                                                                <div class="kv-file-content">
                                                                                    <a class="fancybox" rel="group_position_project" href="<?php echo $value['galery_project_img_name']; ?>">
                                                                                        <img src="<?php echo $value['galery_project_img_name']; ?>" class="kv-preview-data file-preview-image"  style="width:150px;max-height:150px;min-height:150px;">
                                                                                    </a>

                                                                                </div>
                                                                                <div class="file-thumbnail-footer">

                                                                                    <input type="hidden" name="galery_project_img_id[]" value="<?=$value['galery_project_img_id']?>">
                                                                                    <input type="file" class="galery_edit" id="galery_edit<?=$i?>" name="project_galery_edit[]" accept="image/*"/>


                                                                                    <script>
                                                                                        $("#galery_edit<?=$i?>").fileinput({
                                                                                            uploadUrl: "upload.php", // server upload action
                                                                                            maxFileCount: 1,
                                                                                            allowedFileExtensions: ["jpg", "png", "jpeg"],
                                                                                            browseLabel: ' เลือก Gallery ',
//                                                                                            browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
removeLabel: 'ลบ',
browseClass: 'btn btn-success',
showUpload: false,
showRemove:false,
showCaption: false,
msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
msgZoomModalHeading: 'ตัวอย่างละเอียด',
xxx:'upload-project-galery-seo_edit',
dropZoneTitle : 'Gallery',
maxFileSize :300 ,
msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
});
</script>

<p>Alt Text : </p> <input type="text" class="form-control" name="alt_gallery[]" placeholder="Alt SEO"
value="<?=$value['galery_project_img_seo'] == ' ' ? '' : $value['galery_project_img_seo'];?>" >

<span><?="ลำดับที่ " . $i;?></span>
<div class="file-actions">
    <div class="file-footer-buttons">
        <button type="button" id="<?php echo $value['galery_project_img_id']; ?>" onclick="deleteGaleryProject(<?php echo $value['galery_project_img_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
        <a class="fancybox" href="<?php echo $value['galery_project_img_name']; ?>" target="_blank">
            <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
        </a>
    </div>
    <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
</div>
</div>
</div>
</div>
</div>

<div class="clearfix"></div>
<div class="file-preview-status text-center text-success"></div>
<div class="kv-fileinput-error file-error-message" style="display: none;"></div>
</div>
<!--                                                            </div>-->
</div>
</div>
<div class="form-group">
    <div class="col-md-12">
        <input type="hidden" name="gallery_id[]" value="<?php echo $value['galery_project_img_id']; ?>">
        <input type="text"  class="form-control"  name="project_galery_text_edit[]" placeholder="กรอกรายละเอียด Gallery (จีน)"
        value="<?=$value['galery_project_img_desc'] == ' ' ? '' : $value['galery_project_img_desc']?>"/>
    </div>
</div>
<div class="form-group hide-for-th">
    <div class="col-md-12">
        <input type="text"  class="form-control"  name="project_galery_text_en_edit[]" placeholder="กรอกรายละเอียด Gallery (อังกฤษ)"
        value="<?=$value['galery_project_img_desc_en'] == ' ' ? '' : $value['galery_project_img_desc_en'];?>"/>
    </div>
</div>
</div>

<?php }?>

</div>
<br>
<?php }?>

<div class="row">
    <?php if (!isset(getGeleryByIdProject()[0])) {?>
    <div class="col-sm-3">
        <div class="form-group">
            <div class="col-md-12">
                <input type="file" id="galery0" name="project_galery[]" accept="image/*"/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <input type="text"  class="form-control"  name="project_galery_text[]" placeholder="กรอกรายละเอียด Gallery (จีน)"/>
            </div>
        </div>
        <div class="form-group hide-for-th">
            <div class="col-md-12">
                <input type="text"  class="form-control"  name="project_galery_text_en[]" placeholder="กรอกรายละเอียด Gallery (อังกฤษ)"/>
            </div>
        </div>
    </div>
    <?php }?>


    <div id="Project_galery"></div>

</div>

<br>
<div class="form-group">
    <label class="control-label col-md-1">
    </label>
    <div class="col-md-4">
        <input type="hidden" value="<?=$i?>" id="i" >
        <button id="gallery_add" type="button" class="btn btn-round btn-success" onclick="addProject_galery()" >+
            เพิ่มรูป Gallery
        </button>
    </div>
</div>

<script>
    $("#galery0").fileinput({
                                                uploadUrl: "upload.php", // server upload action
                                                maxFileCount: 1,
                                                allowedFileExtensions: ["jpg", "png", "jpeg"],
                                                browseLabel: ' เลือก Gallery โครงการ',
                                                browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                                                removeLabel: 'ลบ',
                                                browseClass: 'btn btn-success',
                                                showUpload: false,
                                                showRemove:false,
                                                showCaption: false,
                                                msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                                msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                xxx:'upload-project-galery-seo',
                                                dropZoneTitle : 'Gallery โครงการ',
                                                maxFileSize :300 ,
                                                msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                                msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                                msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                                msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                                msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                            });
                                        </script>


                                        <hr>
                                        <h4><u>ระบุข้อมูล VDO และ ภาพ 360 องศา</u></h4>

                                        <?php
$sql_c = "SELECT * FROM LH_PROJECT_CLIPS c WHERE c.project_id = '" . $_GET['project_id'] . "'";
	$query_c = mssql_query($sql_c);
	$row_c = mssql_fetch_assoc($query_c);
	$checked_vdo = '';
	$checked_vdo2 = '';
	//echo $row_c['tvc_type'];

	if ($row_c['tvc_type'] == "youtube") {
		$checked_vdo = 'checked';
		$type_vdo = 'show();';
		$type_vdo2 = 'hide();';
	} else if ($row_c['tvc_type'] == "vdo") {
		$checked_vdo2 = 'checked';
		$type_vdo = 'hide();';
		$type_vdo2 = 'show();';
	} else {
		$checked_vdo = 'checked';
		$type_vdo = 'show();';
		$type_vdo2 = 'hide();';
	}
	?>

                                        <div class="form-group" id="inline_content">
                                            <label class="control-label col-md-3" name="tvc_detail" for="first-name">เลือกชนิดการอัปไฟล์ <span class="required"></span>
                                            </label>
                                            <div class="col-md-6">
                                                <div class="radio">
                                                    <label><input type="radio" id="youtube2" class="flat" name="optradio_vdo" <?=$checked_vdo;?> value="youtube"> VDO Youtube</label>
                                                </div>
                                                <div class="radio">
                                                    <label><input type="radio" id="file2" class="flat" name="optradio_vdo" <?=$checked_vdo2;?> value="vdo"> VDO File</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="v1">
                                            <div class="form-group">
                                                <label class="control-label col-md-3" for="first-name">URL Youtube <span class="required"> </span></label>
                                                <div class="col-md-6">
                                                    <div class="control-group">
                                                        <input type="text"  id="tvc_youtube" name="url_youtube" class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube"
                                                        value="<?=$row_c['tvc_type'] == 'youtube' ? $row_c['clip_project_url'] : '';?>">
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3" for="first-name">Thumnal Youtube <br>(.jpg , .png ,.gif <br> ขนาด 1920 x 1080 px) <br> ภาพขนาดไม่เกิน 300 KB <span class="required"></span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div id="map-image-holder-brochure"></div>
                                                    <?php
if ($row_c['tvc_type'] == 'youtube') {
		if ($row_c['thumnail'] != "") {
			?>
                                                            <div class="file-preview ">
                                                                <div class="close fileinput-remove"></div>
                                                                <div class="file-drop-disabled">
                                                                    <div class="file-preview-thumbnails">
                                                                        <div class="file-initial-thumbs">
                                                                            <div class="file-preview-frame file-preview-initial" id="logo"
                                                                            data-fileindex="init_0" data-template="image">
                                                                            <div class="kv-file-content">
                                                                                <a class="fancybox"
                                                                                href="<?=$row_c['thumnail']?>">
                                                                                <img src="<?=$row_c['thumnail']?>"
                                                                                class="kv-preview-data file-preview-image"
                                                                                style="width:150px;height:auto;">
                                                                            </a>
                                                                        </div>
                                                                        <div class="file-actions">
                                                                            <div class="file-footer-buttons">
                                                                                <button type="button" id="<?php echo $row_c['clip_project_id']; ?>" onclick="deleteYoutubeProject(<?php echo $row_c['clip_project_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                                                <a class="fancybox" href="<?php echo $row_c['thumnail']; ?>" >
                                                                                    <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                                                </a>
                                                                            </div>
                                                                            <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="file-preview-status text-center text-success"></div>
                                                            <div class="kv-fileinput-error file-error-message"
                                                            style="display: none;"></div>
                                                        </div>
                                                    </div>
                                                    <?php } else {?>
                                                    <input type="file" name="img_youtube" id="img_youtube" class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube" value="" accept="image/*">

                                                    <?php }?>

                                                    <?php
} else {?>
                                                <input type="file" name="img_youtube" id="img_youtube" class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube" value="" accept="image/*">
                                                <?php }?>

                                            </div>
                                        </div>

                                    </div>

                                    <script>
                                        $("#img_youtube").fileinput({
                                                uploadUrl: "upload.php", // server upload action
                                                maxFileCount: 1,
                                                allowedFileExtensions: ["jpg", "png", "jpeg"],
                                                browseLabel: 'เลือก thumnail',
                                                removeLabel: 'ลบ',
                                                browseClass: 'btn btn-success',
                                                showUpload: false,
                                                showRemove:false,
                                                showCaption: false,
                                                msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                                msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                xxx:'seo_lead_img_file',
                                                dropZoneTitle : 'รูป thumnail TVC',
                                                minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                                minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                                maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                                maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                                maxFileSize :300 ,
                                                msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                                msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                                msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                                msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                                msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                            });
                                        </script>


                                        <div id="v2">

                                            <div class="form-group">
                                                <label class="control-label col-md-3" for="first-name">Thumnail TVC <br>(.jpg , .png ,.gif <br> ขนาด 1920 x 1080 px) <br> ภาพขนาดไม่เกิน 300 KB<span class="required"> </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div id="map-image-holder-brochure"></div>
                                                    <?php
if ($row_c['tvc_type'] == 'vdo') {
		if (($row_c['thumnail'] != '') && ($row_c['tvc_type'] == 'vdo')) {

			?>
                                                            <div class="file-preview ">
                                                                <div class="close fileinput-remove"></div>
                                                                <div class="file-drop-disabled">
                                                                    <div class="file-preview-thumbnails">
                                                                        <div class="file-initial-thumbs">
                                                                            <div class="file-preview-frame file-preview-initial" id="logo"
                                                                            data-fileindex="init_0" data-template="image">
                                                                            <div class="kv-file-content">
                                                                                <a class="fancybox"
                                                                                href="<?=$row_c['thumnail']?>">
                                                                                <img src="<?=$row_c['thumnail']?>"
                                                                                class="kv-preview-data file-preview-image"
                                                                                style="width:150px;height:auto;">
                                                                            </a>
                                                                        </div>
                                                                        <div class="file-actions">
                                                                            <div class="file-footer-buttons">
                                                                                <button type="button" id="<?php echo $row_c['clip_project_id']; ?>" onclick="deleteYoutubeProject(<?php echo $row_c['clip_project_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                                                <a class="fancybox" href="<?php echo $row_c['thumnail']; ?>" >
                                                                                    <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                                                </a>
                                                                            </div>
                                                                            <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="file-preview-status text-center text-success"></div>
                                                            <div class="kv-fileinput-error file-error-message"
                                                            style="display: none;"></div>
                                                        </div>
                                                    </div>
                                                    <?php } else {?>
                                                    <input type="file" name="thumbnail_vdo" id="thumbnail_vdo" class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube" value="" accept="image/*">
                                                    <?php }
	}?>

                                                <input type="file" name="thumbnail_vdo" id="thumbnail_vdo" class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube" value="" accept="image/*">

                                            </div>
                                        </div>



                                        <script>
                                            $("#thumbnail_vdo").fileinput({
                                                    uploadUrl: "upload.php", // server upload action
                                                    maxFileCount: 1,
                                                    allowedFileExtensions: ["jpg", "png", "jpeg"],
                                                    browseLabel: 'เลือก Thumnail TVC',
                                                    removeLabel: 'ลบ',
                                                    browseClass: 'btn btn-success',
                                                    showUpload: false,
                                                    showRemove:false,
                                                    showCaption: false,
                                                    msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                                    msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                    msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                    xxx:'seo_lead_img_file',
                                                    dropZoneTitle : 'รูป Thumnail TVC',
                                                    minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                                    minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                                    maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                                    maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                                    maxFileSize :300 ,
                                                    msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                                    msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                                    msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                                    msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                                    msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                                });
                                            </script>

                                            <div class="form-group">
                                                <label class="control-label col-md-3" for="first-name"> VDO TVC<br> (เฉพาะไฟล์ .mp4 ) <span class="required"> </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div id="map-image-holder-brochure"></div>
                                                    <?php
if ($row_c['tvc_type'] == 'vdo') {
		if ($row_c['clip_project_url'] != '') {

			?>
                                                            <div class="file-preview ">
                                                                <div class="close fileinput-remove"></div>
                                                                <div class="file-drop-disabled">
                                                                    <div class="file-preview-thumbnails">
                                                                        <div class="file-initial-thumbs">
                                                                            <div class="file-preview-frame file-preview-initial" id="logo"
                                                                            data-fileindex="init_0" data-template="image">
                                                                            <div class="kv-file-content">
                                                                                <video width="275" controls>
                                                                                    <source src="<?=$row_c['clip_project_url']?>" type="video/mp4">
                                                                                    </video>

                                                                                </div>
                                                                                <div class="file-actions">
                                                                                    <div class="file-footer-buttons">
                                                                                        <button type="button" id="<?php echo $row_c['clip_project_id']; ?>" onclick="deleteVDOFileProject(<?php echo $row_c['clip_project_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                                                    </div>
                                                                                    <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                    <div class="file-preview-status text-center text-success"></div>
                                                                    <div class="kv-fileinput-error file-error-message"
                                                                    style="display: none;"></div>
                                                                </div>
                                                            </div>
                                                            <?php } else {?>
                                                            <input type="file" name="vdo" id="vdo_file2" class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube" value="" accept="application/mp4">
                                                            <?php }
	}?>
                                                        <input type="file" name="vdo" id="vdo_file2" class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube" value="" accept="application/mp4">

                                                    </div>
                                                </div>

                                                <script>
                                                    $("#vdo_file2").fileinput({
                                                    uploadUrl: "upload.php", // server upload action
                                                    maxFileCount: 1,
                                                    allowedFileExtensions: ["mp4"],
                                                    browseLabel: 'เลือกไฟล์ TVC',
                                                    removeLabel: 'ลบ',
                                                    browseClass: 'btn btn-success',
                                                    showUpload: false,
                                                    showRemove:false,
                                                    showCaption: false,
                                                    msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                                    msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                    msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                    xxx:'seo_lead_img_file',
                                                    dropZoneTitle : 'TVC File',

                                                    msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                                    msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                                    msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                                                    msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                                                });
                                            </script>
                                        </div>

                                        <br>


                                        <?php
$sql_360 = "SELECT * FROM LH_360_PROJECT WHERE project_id = '" . $_GET['project_id'] . "'";
	$query_360 = mssql_query($sql_360);
	$row_360 = mssql_fetch_array($query_360);
	?>


                                        <div class="form-group">
                                            <label class="control-label col-md-3" for="first-name">360 Url
                                            </label>
                                            <div class="col-md-6">
                                                <input type="text" id="first-name2" name="project_360_url" class="form-control col-md-7 col-xs-12" placeholder="Url 360 Virtual Tour"
                                                value="<?php if (isset($row_360['c360_project_url'])) {echo trim($row_360['c360_project_url']);}?>">
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-md-3">360 Virtual Tour <br>(.jpg , .png ,.gif
                                            ขนาด 1920 x 1080 px) <br> ภาพขนาดไม่เกิน 300 KB</label>
                                            <div class="col-md-6">
                                                <div id="map-image-holder-brochure"></div>
                                                <?php if ($row_360['thumnail'] != '') {?>
                                                <div class="file-preview ">
                                                    <div class="close fileinput-remove"></div>
                                                    <div class="file-drop-disabled">
                                                        <div class="file-preview-thumbnails">
                                                            <div class="file-initial-thumbs">
                                                                <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                                                    <div class="kv-file-content">
                                                                        <a class="fancybox" href="<?=trim($row_360['thumnail']);?>">
                                                                            <img src="<?=trim($row_360['thumnail']);?>" class="kv-preview-data file-preview-image"  style="width:auto;height:160px;">
                                                                        </a>
                                                                    </div>
                                                                    <div class="file-actions">
                                                                        <div class="file-footer-buttons">
                                                                            <button type="button" id="<?php echo $row_360['c360_project_id']; ?>" onclick="delete360Project(<?php echo $row_360['c360_project_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                                            <a class="fancybox" href="<?php echo $row_360['thumnail']; ?>" >
                                                                                <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                                            </a>
                                                                        </div>
                                                                        <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="file-preview-status text-center text-success"></div>
                                                        <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                                    </div>
                                                </div>
                                                <?php } else {?>
                                                <input type="file" id="img" name="c_360_img" class="file-loading">
                                                <?php }?>

                                            </div>
                                        </div>

                                        <?php }?>


                                        <?php if($_SESSION['group_status']== 'Admin'){ ?>
                                        <div id="unitMasterPlanCondo" class="<?php echo $classHiddenMasterPlanCondo; ?>">
                                            <?php
$sql_master = "SELECT * FROM LH_MASTER_PLAN_CONDO m LEFT JOIN LH_PROJECTS j
                                            ON m.project_id = j.project_id WHERE j.project_id = '" . $_GET['project_id'] . "'";
	$quert_master = mssql_query($sql_master);
	$row_master = mssql_num_rows($quert_master);

	if ($row_master > 0) {
		?>
                                                <hr>
                                                <!--                                        <figure class="highlight">-->
                                                    <h4><u>ระบุข้อมูล Master Plan และ Floor Plan (สำหรับคอนโด)</u></h4>

                                                    <?php
$sql_master = "SELECT * FROM LH_MASTER_PLAN_CONDO m LEFT JOIN LH_PROJECTS j
                                                    ON m.project_id = j.project_id WHERE j.project_id = '" . $_GET['project_id'] . "'";
		$quert_master = mssql_query($sql_master);
		$i = 1;
		while ($vale_master = mssql_fetch_array($quert_master)) {
			if ($i != 1) {
				echo ' <hr>';
			}
			$i++;
			?>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Master Plan <br>
                                                                (.JPG ขนาด 1920 x 1080 px) <br> ภาพขนาดไม่เกิน 300 KB
                                                            </label>
                                                            <div class="col-md-6">
                                                                <div class="file-preview ">
                                                                    <div class="close fileinput-remove"></div>
                                                                    <div class="file-drop-disabled">
                                                                        <div class="file-preview-thumbnails">
                                                                            <div class="file-initial-thumbs">
                                                                                <div class="file-preview-frame file-preview-initial" data-fileindex="init_0" data-template="image">
                                                                                    <div class="kv-file-content">
                                                                                        <a class="fancybox" rel="group_position_project" href="<?php echo $vale_master['master_plan_img_name']; ?>">
                                                                                            <img src="<?php echo $vale_master['master_plan_img_name']; ?>" class="kv-preview-data file-preview-image"  style="width:150px;height:auto;">
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="file-thumbnail-footer">
                                                                                        <p>Alt Text : </p><input class="form-control" placeholder="Alt SEO" name="master_seo_edit[]" value="<?php echo $vale_master['master_plan_img_seo']; ?>" >

                                                                                        <div class="file-actions">
                                                                                            <div class="file-footer-buttons">
                                                                                        <!--
                                                                                            <button type="button" id="<?php //echo $value['community_features_id']; ?>" onclick="deleteImageCommunity_project(<?php //echo $value['community_features_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                                                        -->
                                                                                        <a class="fancybox" href="<?php echo $vale_master['master_plan_img_name']; ?>" >
                                                                                            <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                                                        </a>
                                                                                    </div>
                                                                                    <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <div class="file-preview-status text-center text-success"></div>
                                                                <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">คำอธิบาย (จีน)</label>
                                                    <div class="col-md-6">
                                                        <textarea type="text" name="master_plan_condo_dis_th_edit[]"
                                                        rows="3"
                                                        class="form-control col-md-7 col-xs-12"
                                                        placeholder="กรอกคำอธิบาย (จีน)"><?=$vale_master['master_plan_dis_th']?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group hide-for-th">
                                                    <label class="control-label col-md-3">คำอธิบาย (อังกฤษ)</label>
                                                    <div class="col-md-6">
                                                        <textarea type="text" name="master_plan_condo_dis_en_edit[]"
                                                        rows="3"
                                                        class="form-control col-md-7 col-xs-12"
                                                        placeholder="กรอกคำอธิบาย (อังกฤษ)"><?=$vale_master['master_plan_dis_en']?></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-7">
                                                    </label>
                                                    <div class="col-md-5">
                                                        <button type="button" id="<?php echo $vale_master['master_plan_id']; ?>" onclick="My_deleteMaster(<?php echo $vale_master['master_plan_id']; ?>)" class="btn btn-danger" title="Remove file" data-url="" data-key="1">
                                                            - ลบ Master Plan
                                                        </button>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="master_plan_id[]" value="<?=$vale_master['master_plan_id'];?>">

                                                <?php }?>
                                                <?php } else {?>

                                                <hr>
                                                <h4><u>ระบุข้อมูล Master Plan และ Floor Plan (สำหรับคอนโด)</u></h4>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Master Plan <br>(.JPG ขนาด
                                                    1920 x 1080 px) <br> ภาพขนาดไม่เกิน 300 KB</label>
                                                    <br>
                                                    <div class="col-md-6">
                                                        <input id="master_plan_img_name" type="file" name="master_plan_img_name[]"
                                                        class="master_plan_img_name"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">คำอธิบาย (จีน)</label>
                                                    <div class="col-md-6">
                                                        <textarea type="text" name="master_plan_condo_dis_th[]"
                                                        rows="3"
                                                        class="form-control col-md-7 col-xs-12"
                                                        placeholder="กรอกคำอธิบาย (จีน)"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group hide-for-th">
                                                    <label class="control-label col-md-3">คำอธิบาย (อังกฤษ)</label>
                                                    <div class="col-md-6">
                                                        <textarea type="text" name="master_plan_condo_dis_en[]"
                                                        rows="3"
                                                        class="form-control col-md-7 col-xs-12"
                                                        placeholder="กรอกคำอธิบาย (อังกฤษ)"></textarea>
                                                    </div>
                                                </div>

                                                <?php }?>


                                                <div id="Project_Master"></div>
                                                <!--                                        </figure>-->
                                                <!--                                         เพิ่ม master plan-->
                                                <br>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">
                                                    </label>
                                                    <div class="col-md-4">
                                                        <button type="button" class="btn btn-round btn-success" onclick="addProject_master()" >
                                                            + Master Plan
                                                        </button>
                                                    </div>
                                                </div>

                                            </div>

                                            <br>
                                            <?php
$classHiddenUnitPlanCondo = "hidden";
	$issetDataUnitPlan = isset($detailSubproject);
	$find_index_plan = 0;
	if ($issetDataUnitPlan) {
		if (count($detailSubproject) > 0) {

			for ($i = $find_index_plan; $i < sizeof($detailSubproject); $i++) {
				if (array_key_exists("dataUnitPlanCondo", $detailSubproject[$i])) {
					$find_index_plan = $i;
				}
			}

			$classHiddenUnitPlanCondo = array_key_exists("dataUnitPlanCondo", $detailSubproject[$find_index_plan]) ? "" : "hidden";
		}
	}
	?>


                                            <div id="unitPlanCondo2" class="<?php echo $classHiddenUnitPlanCondo; ?>" >
                                                <!--                                        <figure class="highlight">-->
                                                    <?php
$sql_unit = "select lh_unit_plan_condo.*,lh_unit_plan_img.*
                                                    from lh_unit_plan_condo
                                                    INNER JOIN lh_unit_plan_img ON lh_unit_plan_condo.unit_plan_id = lh_unit_plan_img.unit_plan_id
                                                    where lh_unit_plan_condo.project_id = '" . $_GET['project_id'] . "'";
	$query_unit = mssql_query($sql_unit);
	$num = mssql_num_rows($query_unit);
	if ($num > 0) {
		$countdataUnitPlanCondo = 0;
		$sql_unit = "select lh_unit_plan_condo.*,lh_unit_plan_img.*
                                                        from lh_unit_plan_condo
                                                        INNER JOIN lh_unit_plan_img ON lh_unit_plan_condo.unit_plan_id = lh_unit_plan_img.unit_plan_id
                                                        where lh_unit_plan_condo.project_id = '" . $_GET['project_id'] . "'";
		$query_unit = mssql_query($sql_unit);
		$i = 1;
		while ($itemDataUnitPlan = mssql_fetch_array($query_unit)) {
			?>
                                                        <div>
                                                            <hr>
                                                            <h4><u>ระบุข้อมูล Unit Plan (สำหรับคอนโด)</u></h4>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">ชื่อ Unit </label>
                                                                <div class="col-md-6">
                                                                    <input type="text"
                                                                    value="<?php if ($itemDataUnitPlan['unit_plan_name_th'] == " ") {echo "";} else {echo $itemDataUnitPlan['unit_plan_name_th'];}?>"
                                                                    name="unit_plan_name_th_condo_update[]"
                                                                    class="form-control col-md-7 col-xs-12"
                                                                    placeholder="กรอกชื่อ Unit (จีน)">
                                                                </div>
                                                            </div>
                                                            <div class="form-group hide-for-th">
                                                                <label class="control-label col-md-3">ชื่อ Unit </label>
                                                                <div class="col-md-6">
                                                                    <input type="text"
                                                                    value="<?php if ($itemDataUnitPlan['unit_plan_name_en'] == " ") {echo "";} else {echo $itemDataUnitPlan['unit_plan_name_en'];}?>"
                                                                    name="unit_plan_name_en_condo_update[]"
                                                                    class="form-control col-md-7 col-xs-12"
                                                                    placeholder="กรอกชื่อ Unit (อังกฤษ)">
                                                                </div>
                                                                <input type="hidden" name="unit_plan_img_id[]" value="<?php echo $itemDataUnitPlan['unit_plan_img_id']; ?>">
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-md-3"></div>
                                                                <div class="col-md-6">
                                                                    <div name="unit_plan_img_pervie">
                                                                        <?php
if (trim($itemDataUnitPlan['unit_plan_img_name'], " ") != "") {
				echo '<div class="file-preview ">
                                                                            <div class="close fileinput-remove"></div>
                                                                            <div class="file-drop-disabled">
                                                                            <div class="file-preview-thumbnails">
                                                                            <div class="file-initial-thumbs">
                                                                            <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                                                            <div class="kv-file-content">';
				echo '<a class="fancybox" href="' . $itemDataUnitPlan['unit_plan_img_name'] . '">' .
					'<img src="' . $itemDataUnitPlan['unit_plan_img_name'] . '" class="kv-preview-data file-preview-image"  style="width:150px;height:auto;">' .
					'</a>' .
					'</div>' .
					'<div class="file-thumbnail-footer">' .
					'<p>' . $itemDataUnitPlan['unit_plan_img_name_seo'] . '</p>' .
					'</div>' .
					'</div>' .
					'</div>' .
					'</div>' .
					'<div class="clearfix"></div>' .
					'<div class="file-preview-status text-center text-success"></div>' .
					'<div class="kv-fileinput-error file-error-message" style="display: none;"></div>' .
					'</div>' .
					'</div>';

			}
			echo '<input type="hidden" name="unit_plan_img_name_seo_temp' . $itemDataUnitPlan['unit_plan_img_id'] . '" value="' . $itemDataUnitPlan['unit_plan_img_name_seo'] . '" >';
			echo '<input type="hidden" name="unit_plan_img_name_temp' . $itemDataUnitPlan['unit_plan_img_id'] . '" value="' . $itemDataUnitPlan['unit_plan_img_name'] . '" >';

			?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">ภาพขนาดไม่เกิน 300 KB</label>
                                                                <div class="col-md-6">
                                                                    <input type="file" id="unit_plan_img<?php echo $i ?>" class="class_unit_plan_img" name="unit_plan_img<?php echo $itemDataUnitPlan['unit_plan_img_id'] ?>" />
                                                                </div>
                                                            </div>

                                                            <script>
                                                                $("#unit_plan_img<?php echo $i ?>").fileinput({
                                                                    uploadUrl: "upload.php",
                                                                    dropZoneTitle : 'Unit Plan',
                                                                    browseLabel: 'เลือกภาพ',
                                                                    removeLabel: 'ลบ',
                                                                    browseClass: 'btn btn-success',
                                                                    showUpload: false,
                                                                    showRemove: false,
                                                                    showCaption: false,
                                                                    maxFileCount: 1,
                                                                    mainClass: "input-group-md",
                                                                    xxx:'unit_plan_img_name_seo',
                                                                    allowedFileTypes: ["image"],
                                                                    maxFileSize :300 ,
                                                                    msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                                                });
                                                            </script>
                                                            <?php $i++;?>

                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">ขนาด
                                                                </label>
                                                                <div class="col-md-2">
                                                                    <input type="text" name="unit_plan_size_condo_update[]"
                                                                    value="<?php echo $itemDataUnitPlan['unit_plan_size']; ?>"
                                                                    class="form-control col-md-7 col-xs-12 unit_condo" >
                                                                </div>
                                                                <label class="control-label col-md-1" style="text-align:left;">ตารางเมตร</label>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">รายละเอียด
                                                                </label>
                                                                <div class="col-md-6">
                                                                    <textarea type="text" name="unit_plan_dis_th_condo_update[]" class="form-control col-md-7 col-xs-12" rows="3" placeholder="กรอกรายละเอียด (จีน)"><?php echo $itemDataUnitPlan['unit_plan_dis_th']; ?></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group hide-for-th">
                                                                <label class="control-label col-md-3">รายละเอียด
                                                                </label>
                                                                <div class="col-md-6">
                                                                    <textarea type="text" name="unit_plan_dis_en_condo_update[]"
                                                                    class="form-control col-md-7 col-xs-12"
                                                                    rows="3"
                                                                    placeholder="กรอกรายละเอียด (อังกฤษ)"><?php echo $itemDataUnitPlan['unit_plan_dis_en']; ?></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">ราคาเริ่มต้น
                                                                </label>
                                                                <div class="col-md-2">
                                                                    <input type="text" name="unit_plan_price_condo_update[]"
                                                                    value="<?php echo $itemDataUnitPlan['unit_plan_price']; ?>"
                                                                    class="form-control col-md-7 col-xs-12"
                                                                    onkeypress="return isNumber(event)"/>
                                                                </div>
                                                                <label class="control-label col-md-1 tabs-right">ต่อยูนิต</label>
                                                            </div>
                                                        </br>
                                                        <input type="hidden" name="unit_plan_id[]" value="<?php echo $itemDataUnitPlan['unit_plan_id']; ?>">

                                                        <div class="form-group">
                                                            <label class="control-label col-md-7">
                                                            </label>
                                                            <div class="col-md-5">
                                                                <button type="button" id="<?php echo $itemDataUnitPlan['unit_plan_id']; ?>" onclick="My_deleteUnit(<?php echo $itemDataUnitPlan['unit_plan_id']; ?>)" class="btn btn-danger" title="Remove file" data-url="" data-key="1">
                                                                    - ลบ Unit Plan
                                                                </button>
                                                            </div>
                                                            <?php
if ($countdataUnitPlanCondo == 0) {?>


                                                            <?php } else {?>
                                                            <!--                                                        <div class="form-group" >-->
                                                                <!--                                                            <label class="control-label col-md-7" ></label >-->
                                                                <!--                                                            <div class="col-md-2" >-->
                                                                    <!--                                                                <button type = "button" onclick = "DeleteUnitPlanCondo(this)" class="btn btn-round btn-danger" > -Delete Unit Plan-->
                                                                        <!--                                                                </button >-->
                                                                        <!--                                                            </div >-->
                                                                        <!--                                                        </div >-->
                                                                        <?php }?>
                                                                        <?php $countdataUnitPlanCondo = $countdataUnitPlanCondo + 1;?>
                                                                    </div>
                                                                    <?php }?>


                                                                    <?php } else {?>
                                                                    <hr>
                                                                    <h4><u>ระบุข้อมูล Unit Plan (สำหรับคอนโด)</u></h4>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">ชื่อ Unit
                                                                        </label>
                                                                        <div class="col-md-6">
                                                                            <input type="text"
                                                                            value=""
                                                                            name="unit_plan_name_th_condo[]"
                                                                            class="form-control col-md-7 col-xs-12"
                                                                            placeholder="กรอกชื่อ Unit (จีน)">
                                                                            <div name="unit_plan_img_pervie"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group hide-for-th">
                                                                        <label class="control-label col-md-3">ชื่อ Unit
                                                                        </label>
                                                                        <div class="col-md-6">
                                                                            <input type="text"
                                                                            name="unit_plan_name_en_condo[]"
                                                                            class="form-control col-md-7 col-xs-12"
                                                                            placeholder="กรอกชื่อ Unit (อังกฤษ)">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-3"></div>
                                                                        <div class="col-md-6">
                                                                            <input type="file" name="unit_plan_img_name[]" class="unit_plan_img_name" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">ขนาด
                                                                        </label>
                                                                        <div class="col-md-2">
                                                                            <input type="text" name="unit_plan_size_condo[]"
                                                                            class="form-control col-md-7 col-xs-12" 
                                                                            onkeypress="return isNumber(event)" placeholder="ขนาด">
                                                                        </div>
                                                                        <label class="control-label col-md-2" >ตารางเมตร</label>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">รายละเอียด
                                                                        </label>
                                                                        <div class="col-md-6">
                                                                            <textarea type="text" name="unit_plan_dis_th_condo[]"
                                                                            rows="3"
                                                                            class="form-control col-md-7 col-xs-12"
                                                                            placeholder="กรอกรายละเอียด (จีน)"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group hide-for-th">
                                                                        <label class="control-label col-md-3">รายละเอียด
                                                                        </label>
                                                                        <div class="col-md-6">
                                                                            <textarea type="text" name="unit_plan_dis_en_condo[]"
                                                                            class="form-control col-md-7 col-xs-12"
                                                                            rows="3"
                                                                            placeholder="กรอกรายละเอียด (อังกฤษ)"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">ราคาเริ่มต้น
                                                                        </label>
                                                                        <div class="col-md-2">
                                                                            <input type="text" name="unit_plan_price_condo[]"
                                                                            class="form-control col-md-7 col-xs-12" min="1"
                                                                            onkeypress="return isNumber(event)" placeholder="กรอกราคาเริ่มต้น"/>
                                                                        </div>
                                                                        <label class="control-label col-md-1 tabs-right">ต่อยูนิต</label>
                                                                    </div>
                                                                    <br>
                                                                    <!--                                        </figure>-->
                                                                    <!--                                            <div class="form-group">-->
                                                                        <!--                                                <label class="control-label col-md-1"></label>-->
                                                                        <!--                                                <div class="col-md-2">-->
                                                                            <!--                                                    <button type="button" onclick="addUnitPlanForCondo();"  class="btn btn-round btn-default">+ Add Unit Plan-->
                                                                                <!--                                                    </button>-->
                                                                                <!--                                                </div>-->
                                                                                <!--                                            </div>-->

                                                                                <?php }?>
                                                                                <div id="unitPlanCondo"></div>
                                                                            </div>



                                                                            <div class="form-group <?php echo $classHiddenUnitPlanCondo; ?>" >
                                                                                <label class="control-label col-md-3" ></label >
                                                                                <div class="col-md-3">
                                                                                    <button type = "button" onclick = "addUnitPlanForCondo()" class="btn btn-round btn-success" > + Add Unit Plan
                                                                                    </button >
                                                                                </div>
                                                                            </div >

                                                                            <br>

                                                                            <?php

	$classHiddenProgressCondo = "hidden";
	$issetDataProgressCondo = isset($detailSubproject);
	$find_index_plan = 0;
	$sizeProgressCondo = 0;
	if ($issetDataProgressCondo) {
		if (count($detailSubproject) > 0) {

			for ($i = $find_index_plan; $i < sizeof($detailSubproject); $i++) {
				if (array_key_exists("dataProgressCondo", $detailSubproject[$i])) {
					$find_index_plan = $i;
					$dataProgressCondo = $detailSubproject[$find_index_plan]["dataProgressCondo"];
					$sizeProgressCondo = sizeof($dataProgressCondo);

				}
			}

			$classHiddenProgressCondo = array_key_exists("dataProgressCondo", $detailSubproject[$find_index_plan]) ? "" : "hidden";
		}
	}
	//print_r($dataProgressCondo);
	?>
                                                                            <!--                                    <figure class="highlight --><?php //echo $classHiddenProgressCondo ?><!--">-->
                                                                                <div id="progressCondo"  class="<?php echo $classHiddenProgressCondo ?>" >
                                                                                    <hr>
                                                                                    <h4><u>ระบุข้อมูลความคืบหน้าโครงการ Progress Update (สำหรับคอนโด)</u></h4>

                                                                                    <?php
if ($sizeProgressCondo > 0) {
		for ($i = 0; $i < $sizeProgressCondo; $i++) {
			$date = $dataProgressCondo[$i]['progress_update'];
			$day = substr($date, 8, 2);
			$mount = substr($date, 5, 2);
			$year = substr($date, 0, 4);
			if ($date != '') {
				$progress_update_new = $day . "/" . $mount . "/" . $year;
			} else {
				$progress_update_new = "";
			}

			$key_progress = $dataProgressCondo[$i]['progress_update_id'];
			echo '<div>' .
				'<hr style="border-top: 2px solid #eee;">' .
				'<input name="progress_update_id[]" type="hidden" value = "' . $dataProgressCondo[$i]['progress_update_id'] . '" >' . '</input>' .
				'<input name = "progressSection[]" type="hidden" value = "' . $i . '" > </input>
                                                                                            <div class="form-group" >
                                                                                            <label class="control-label col-md-3" > อัพเดต % ความคืบหน้า' . '<BR>' . '[ 0 ถึง 100 ] </label><br>
                                                                                            <div class="col-md-2" >
                                                                                            <input  type = "text" name = "progress_update_mount[]" value="' . $dataProgressCondo[$i]['progress_update_mount'] . '"
                                                                                            class="form-control col-md-7 col-xs-12 progress2" placeholder = "กรอกอัพเดท " min="1" max="100" >
                                                                                            </div >
                                                                                            </div >
                                                                                            <div class="form-group" >
                                                                                            <label class="control-label col-md-3" > ระบุเดือน / ปี
                                                                                            </label >
                                                                                            <div class="col-md-2" >
                                                                                            <input type = "text" name = "progress_update_date[]" id="progress_update_date" value="' . $progress_update_new . '"
                                                                                            class="form-control col-md-7 col-xs-12 datepicker-progress-date" placeholder = "" >
                                                                                            </div >
                                                                                            <label class="control-label col-md-3" style=" text-align: left; "> ของภาพความคืบหน้าโครงการที่จะใส่
                                                                                            </label >
                                                                                            </div >';

			$dataProgressImg = $dataProgressCondo[$i]["progressImg"];
			$sizeDataProgressImg = sizeof($dataProgressImg);

			echo '<div id = "progressSecImg_part_' . $key_progress . '" >';
			if ($sizeDataProgressImg == 0) {
				echo '<div><hr class="progress_line">' .
					'<input name="progress_update_img_id_' . $key_progress . '[]" type="hidden" value = "" />
                                                                                                <div class="form-group" >
                                                                                                <label class="control-label col-md-3" > ระบุภาพ <br> ภาพขนาดไม่เกิน 300 KB</label >
                                                                                                <div class="col-md-4" >
                                                                                                <input type="file" class="progress_img progress-img-js file-loading" id="progress_img_part_seo_' . $key_progress . "_" . "0" . '" name="progress_img_part_' . $key_progress . '[]" >
                                                                                                <div class="previewImgProgress" >

                                                                                                </div>
                                                                                                </div >
                                                                                                </div >
                                                                                                <div class="form-group" >
                                                                                                <label class="control-label col-md-3" > คำอธิบาย  </label >
                                                                                                <div class="col-md-4" >
                                                                                                <input type = "text" name = "progress_update_dis_th_part_' . $key_progress . '[]"
                                                                                                class="form-control col-md-7 col-xs-12"
                                                                                                value=""
                                                                                                placeholder = "กรอกคำอธิบาย (จีน)" />
                                                                                                </div >
                                                                                                </div>
                                                                                                <div class="form-group hide-for-th" >
                                                                                                <label class="control-label col-md-3" > คำอธิบาย  </label >
                                                                                                <div class="col-md-4" >
                                                                                                <input type = "text" name= "progress_update_dis_en_part_' . $key_progress . '[]"
                                                                                                class="form-control col-md-7 col-xs-12"
                                                                                                value=""
                                                                                                placeholder = "กรอกคำอธิบาย (อังกฤษ)" />
                                                                                                </div >
                                                                                                </div>';

				echo '</div >';

			} else {
				for ($j = 0; $j < $sizeDataProgressImg; $j++) {

					$imageprogress = '';
					$image_seo_progress = '';

					if ($dataProgressImg[$j]['progress_update_img_name'] != "") {
						$imageprogress = $dataProgressImg[$j]['progress_update_img_name'];
					}

					if ($dataProgressImg[$j]['progress_update_img_seo'] != null ||
						$dataProgressImg[$j]['progress_update_img_seo'] != ""
					) {
						$image_seo_progress = $dataProgressImg[$j]['progress_update_img_seo'];
					}

					echo '<div>' .
						'<hr class="progress_line">' .
						'<input name="progress_update_img_id_' . $key_progress . '[]" type="hidden" value = "' . $dataProgressImg[$j]['progress_update_img_id'] . '" />
                                                                                                    <div class="form-group" >
                                                                                                    <label class="control-label col-md-3" > ระบุภาพ <br> ภาพขนาดไม่เกิน 300 KB</label >
                                                                                                    <div class="col-md-4" >';

					echo '<div class="previewImgProgress" >';
					if ($imageprogress != "" && $imageprogress != null
						&& $imageprogress != " " && $imageprogress != "_") {
						echo '<div class="file-preview ">
                                                                                                    <div class="close fileinput-remove"></div>
                                                                                                    <div class="file-drop-disabled">
                                                                                                    <div class="file-preview-thumbnails">
                                                                                                    <div class="file-initial-thumbs">
                                                                                                    <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                                                                                    <div class="kv-file-content">' .
							'<a class="fancybox" href="' . $imageprogress . '">
                                                                                                    <img src="' . $imageprogress . '" class="kv-preview-data file-preview-image"  style="width:150px;height:auto;">' .
							'</a>
                                                                                                    </div>
                                                                                                    <div class="file-thumbnail-footer">
                                                                                                    <p>' . $image_seo_progress . '
                                                                                                    </p>
                                                                                                    </div>
                                                                                                    </div>
                                                                                                    </div>
                                                                                                    </div>
                                                                                                    <div class="clearfix"></div>
                                                                                                    <div class="file-preview-status text-center text-success"></div>
                                                                                                    <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                                                                                    </div>
                                                                                                    </div>';
					}
					echo '</div>';

					echo '<input type="file" class="progress_img progress-img-js file-loading" id="progress_img_part_seo_' . $key_progress . "_" . $j . '"  name="progress_img_part_' . $key_progress . '[]" >';
					echo '<input type="hidden" name="progress_img_temp' . $dataProgressImg[$j]['progress_update_img_id'] . '" ' . 'value="' . $imageprogress . '"' . ">";
					echo '<input type="hidden" name="progress_seo_temp' . $dataProgressImg[$j]['progress_update_img_id'] . '" ' . 'value="' . $image_seo_progress . '"' . ">";
					echo '</div >
                                                                                                </div >
                                                                                                <div class="form-group" >
                                                                                                <label class="control-label col-md-3" > คำอธิบาย  </label >
                                                                                                <div class="col-md-4" >
                                                                                                <input type = "text" name = "progress_update_dis_th_part_' . $key_progress . '[]"
                                                                                                class="form-control col-md-7 col-xs-12"
                                                                                                value="' . $dataProgressImg[$j]['progress_update_dis_th'] . '"
                                                                                                placeholder = "กรอกคำอธิบาย (จีน)" />
                                                                                                </div >
                                                                                                </div>
                                                                                                <div class="form-group hide-for-th" >
                                                                                                <label class="control-label col-md-3" > คำอธิบาย  </label >
                                                                                                <div class="col-md-4" >
                                                                                                <input type = "text" name= "progress_update_dis_en_part_' . $key_progress . '[]"
                                                                                                class="form-control col-md-7 col-xs-12"
                                                                                                value="' . $dataProgressImg[$j]['progress_update_dis_en'] . '"
                                                                                                placeholder = "กรอกคำอธิบาย (อังกฤษ)" />
                                                                                                </div >
                                                                                                </div>';

					if ($j > 0) {
						echo '<div class="form-group" >
                                                                                                    <label class="control-label col-md-5" ></label >
                                                                                                    <div class="col-md-6 col-md-offset-1" style="margin-left: 33px;">
                                                                                                    <button type = "button" onclick="deleteProgreeImg(' . 'this' . ')" class="btn btn-round btn-danger" > -Delete ภาพ
                                                                                                    </button >
                                                                                                    </div >
                                                                                                    </div >';
					} else {
//                                                            echo '<div class="form-group" >
						//                                                                    <div class="col-md-2 col-md-offset-1" >
						//                                                                        <button type="button" onclick="addProgreeImg(' . $key_progress . ')" class="btn btn-round btn-default" > +Add ภาพเพิ่ม
						//                                                                 </button >
						//                                                                    </div >
						//                                                                </div >';
					}
					echo '</div >';
				}
			}

			echo '</div >';

			echo '<div class="form-group" >
                                                                                        <label class="control-label col-md-2"></label>
                                                                                        <div class="col-md-4 col-md-offset-1" >
                                                                                        <button type="button" onclick="addProgreeImg(' . $key_progress . ')" class="btn btn-round btn-success my_progress" > +Add ภาพเพิ่ม
                                                                                        </button >
                                                                                        </div >
                                                                                        </div >';

			if ($i == 0) {
				//echo  '<button type="button" id="btnaddProgreesPart" onclick="addProgreesPart()" class="btn btn-round btn-default" > + Add Progrees </button>';

			} else {
				echo '<div class="form-group">' .
					'<label class="control-label col-md-5"></label>' .
					'<div class="col-md-2 col-md-offset-1">' .
					'<button type="button"  onclick="deleteProgreesPart(this)" class="btn btn-round btn-danger" > - Delete Progrees </button>' .
					'</div></div>';
			}

			echo '</div >';
		}
	} else {
		echo '<div>' .
			'<hr style="border-top: 2px solid #eee;">' .
			'<input name ="progressSection_add[]" type="hidden" value = "' . "1" . '" > </input>
                                                                                    <div class="form-group" >
                                                                                    <label class="control-label col-md-3" > อัพเดต % ความคืบหน้า' . '<br>.' . '[
                                                                                     0 ถึง 100
                                                                                 ]
                                                                                 </label ><br>
                                                                                 <div class="col-md-2" >
                                                                                 <input type = "text" name = "progress_update_mount_add[]" value=""
                                                                                 class="form-control col-md-7 col-xs-12 progress2" placeholder = "กรอกอัพเดท" min="1" max="100">
                                                                                 </div >
                                                                                 </div >
                                                                                 <div class="form-group" >
                                                                                 <label class="control-label col-md-3" > ระบุเดือน / ปี
                                                                                 </label >
                                                                                 <div class="col-md-2" >
                                                                                 <input type = "text" name="progress_update_date_add[]" id="progress_update_date" value=""
                                                                                 class="form-control col-md-7 col-xs-12 datepicker-progress-date" placeholder = "" >
                                                                                 </div >
                                                                                 <label class="control-label col-md-3" style=" text-align: left; "> ของภาพความคืบหน้าโครงการที่จะใส่
                                                                                 </label >
                                                                                 </div >';

		echo '<div id = "progressSecImg_part_' . "1" . '" >';

		echo '<div>
                                                                                 <div class="form-group" >
                                                                                 <label class="control-label col-md-3" > ระบุภาพ <br> ภาพขนาดไม่เกิน 300 KB</label >
                                                                                 <div class="col-md-4" >
                                                                                 <input type="file" id="progress_img_part_add1" class="progress_img progress-img-js file-loading" name="progress_img_part_add1[]" >
                                                                                 <div class="previewImgProgress" >
                                                                                 </div>
                                                                                 </div >
                                                                                 </div >
                                                                                 <div class="form-group" >
                                                                                 <label class="control-label col-md-3" > คำอธิบาย  </label >
                                                                                 <div class="col-md-4" >
                                                                                 <input type = "text" name = "progress_update_dis_th_part_add' . "1" . '[]"
                                                                                 class="form-control col-md-7 col-xs-12"
                                                                                 value=""
                                                                                 placeholder = "กรอกคำอธิบาย (จีน)" >
                                                                                 </div >
                                                                                 </div>
                                                                                 <div class="form-grou hide-for-th" >
                                                                                 <label class="control-label col-md-3" > คำอธิบาย  </label >
                                                                                 <div class="col-md-4" >
                                                                                 <input type = "text" name = "progress_update_dis_en_part_add' . "1" . '[]"
                                                                                 class="form-control col-md-7 col-xs-12"
                                                                                 value=""
                                                                                 placeholder = "กรอกคำอธิบาย (อังกฤษ)" >
                                                                                 </div >
                                                                                 </div>';

		echo '</div >';
		echo '</div >';

		//echo '';
		echo '</div >';
		echo '<div class="form-group" >
                                                                                 <label class="control-label col-md-2"></label>
                                                                                 <div class="col-md-4 col-md-offset-1" >
                                                                                 <button type="button" onclick="addProgreeImg(' . "1" . ')" class="btn btn-round btn-success" > +Add ภาพเพิ่ม</button>
                                                                                 </div >
                                                                                 </div >';

	}
	?>




                                                                         </div>
                                                                         <div id="progess2" class="hide">
                                                                            <br>
                                                                            <div class="form-group">
                                                                                <label class="control-label col-md-2"></label>
                                                                                <div class="col-md-4" >
                                                                                    <button type="button" id="btnaddProgreesPart" onclick="addProgreesPart()" class="btn btn-round btn-success" > + Add Progrees </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--                                    </figure >-->

                                                                        <?php //}?>
                                                                        <!-- condo -->
                                                                        <!-- community-and-room-feature -->
                                                                        <?php //if($_SESSION['group_status']== 'Admin'){?>
                                                                        <div id="community-and-room-feature" class="<?php echo $classHiddenMasterPlanCondo ?>">
                                                                            <br>
                                                                            <hr>
                                                                            <!--                                        <figure class="highlight">-->
                                                                                <h4><u>สิ่งอำนวยความสะดวกของโครงการ</u></h4>

                                                                                <?php

	$sql = "SELECT * FROM LH_COMMUNITY WHERE project_id ='" . $_GET['project_id'] . "'";
	$query_l2 = mssql_query($sql);
	$row = mssql_num_rows($query_l2);
	if ($row > 0) {

		$sql_c = "SELECT * FROM LH_COMMUNITY WHERE project_id ='" . $_GET['project_id'] . "'";
		$query_c = mssql_query($sql_c, $GLOBALS['db_conn']);
		$i = 1;
		while ($value = mssql_fetch_array($query_c)) {
			if ($i != 1) {
				echo ' <hr>';
			}
			?>

                                                                                        <div class="form-group">
                                                                                            <label class="control-label col-md-3">สิ่งอำนวยความสะดวกของโครงการ
                                                                                            </label>
                                                                                            <div class="col-md-6">
                                                                                                <input type="text"  class="form-control"  name="pacu_text_th_edit[]" placeholder="กรอกชื่อสิ่งอำนวยความสะดวกภายในโครงการ (จีน)" value="<?php echo $value['community_name']; ?>">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group hide-for-th">
                                                                                            <label class="control-label col-md-3">สิ่งอำนวยความสะดวกของโครงการ
                                                                                            </label>
                                                                                            <div class="col-md-6">
                                                                                                <input type="text"  class="form-control"  name="pacu_text_en_edit[]" placeholder="กรอกชื่อสิ่งอำนวยความสะดวกภายในโครงการ (อังกฤษ)" value="<?php echo $value['community_name_en']; ?>">
                                                                                            </div>
                                                                                        </div>
                                                                                        <?php if ($value['community_features_img'] != '') {?>
                                                                                        <div class="form-group">
                                                                                            <label class="control-label col-md-3">สิ่งอำนวยความสะดวกของโครงการ <br> ขนาด 1920 x 1080 px <br>
                                                                                                (.JPG , .PNG , .GIF ) <br> ภาพขนาดไม่เกิน 300 KB
                                                                                            </label>
                                                                                            <div class="col-md-6">
                                                                                                <div class="file-preview ">
                                                                                                    <div class="close fileinput-remove"></div>
                                                                                                    <div class="file-drop-disabled">
                                                                                                        <div class="file-preview-thumbnails">
                                                                                                            <div class="file-initial-thumbs">
                                                                                                                <div class="file-preview-frame file-preview-initial" data-fileindex="init_0" data-template="image">
                                                                                                                    <div class="kv-file-content">
                                                                                                                        <a class="fancybox" rel="group_position_project" href="<?php echo $value['community_features_img']; ?>">
                                                                                                                            <img src="<?php echo $value['community_features_img']; ?>" class="kv-preview-data file-preview-image"  style="width:150px;height:auto;">
                                                                                                                        </a>
                                                                                                                    </div>
                                                                                                                    <div class="file-thumbnail-footer">
                                                                                                                        <p>Alt Text : <?php echo $value['community_features_img_seo']; ?>
                                                                                                                        </p>
                                                                                                                        <div class="file-actions">
                                                                                                                            <div class="file-footer-buttons">

                                                                                                                                <a class="fancybox" href="<?php echo $value['community_features_img']; ?>" >
                                                                                                                                    <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                                                                                                </a>
                                                                                                                            </div>
                                                                                                                            <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="clearfix"></div>
                                                                                                        <div class="file-preview-status text-center text-success"></div>
                                                                                                        <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <?php } else {?>
                                                                                        <div class="form-group">

                                                                                            <label class="control-label col-md-3">สิ่งอำนวยความสะดวกของโครงการ ขนาด 1920 x 1080 px <br>(.JPG , .PNG , .GIF )<br>
                                                                                            </label>
                                                                                            <div class="col-md-6">

                                                                                                <input type="file" id="pacus<?=$value['community_features_id'];?>" name="project_pacu_edit[]" accept="image/*"/>
                                                                                            </div>
                                                                                        </div>
                                                                                        <script>
                                                                                            var index = <?=$value['community_features_id']?>;
                                                                                            $("#pacus"+(index)).fileinput({
                                                        uploadUrl: "upload.php", // server upload action
                                                        maxFileCount: 1,
                                                        allowedFileExtensions: ["jpg", "png", "jpeg"],
                                                        browseLabel: 'เลือกรูป',
                                                        removeLabel: 'ลบ',
                                                        browseClass: 'btn btn-success',
                                                        showUpload: false,
                                                        showRemove:false,
                                                        showCaption: false,
                                                        msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                        msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                                        msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                        xxx:'upload_project_pacu_seo_edit',
                                                        dropZoneTitle : 'รูป สิ่งอำนวยความสะดวก',
                                                        minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                                                        minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                                                        maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                                                        maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                                                        maxFileSize :300 ,
                                                        msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                                        msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                                        msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                                        msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                                        msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                                    });
                                                </script>
                                                <?php }?>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">รายละเอียดสิ่งอำนวยความสะดวก (จีน)
                                                    </label>
                                                    <div class="col-md-6">
                                                        <textarea type="text" rows="3"  class="form-control"  name="project_pacu_text_edit[]" placeholder="กรอกรายละเอียด Gallery"><?php echo $value['community_features_desc']; ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group hide-for-th">
                                                    <label class="control-label col-md-3">รายละเอียดสิ่งอำนวยความสะดวก (อังกฤษ)
                                                    </label>
                                                    <div class="col-md-6">
                                                        <textarea type="text" rows="3"  class="form-control"  name="project_pacu_text_en_edit[]" placeholder="กรอกรายละเอียด Gallery"><?php echo $value['community_features_desc_en']; ?></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-6">
                                                    </label>
                                                    <div class="col-md-6">
                                                        <button type="button" id="<?php echo $value['community_features_id']; ?>" onclick="My_deleteCommunity(<?php echo $value['community_features_id']; ?>)" class="btn btn-danger" title="Remove file" data-url="" data-key="1">
                                                            - ลบ สิ่งอำนวยความสะดวกภายในโครงการ
                                                        </button>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="community_features_id[]" value="<?=$value['community_features_id'];?>">
                                                <br>
                                                <?php $i++;}} else {?>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">ชื่อสิ่งอำนวยความสะดวกภายในโครงการ
                                                    </label>
                                                    <div class="col-md-6">
                                                        <input type="text"  class="form-control"  name="pacu_text[]" placeholder="กรอกชื่อสิ่งอำนวยความสะดวกภายในโครงการ (จีน)">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">ชื่อสิ่งอำนวยความสะดวกภายในโครงการ
                                                    </label>
                                                    <div class="col-md-6">
                                                        <input type="text"  class="form-control"  name="pacu_text_en[]" placeholder="กรอกชื่อสิ่งอำนวยความสะดวกภายในโครงการ (อังกฤษ)">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">สิ่งอำนวยความสะดวกของโครงการ ขนาด 1920 x 1080 px <br>(.JPG , .PNG , .GIF )<br> ภาพขนาดไม่เกิน 300 KB
                                                    </label>
                                                    <div class="col-md-6">
                                                        <input type="file" id="pacu0" name="project_pacu[]" accept="image/*"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">รายละเอียดสิ่งอำนวยตวามสะดวก (จีน)
                                                    </label>
                                                    <div class="col-md-6">
                                                        <textarea type="text" rows="3"  class="form-control"  name="project_pacu_text[]" placeholder="กรอกรายละเอียด Gallery"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group hide-for-th">
                                                    <label class="control-label col-md-3">รายละเอียดสิ่งอำนวยตวามสะดวก (อังกฤษ)
                                                    </label>
                                                    <div class="col-md-6">
                                                        <textarea type="text" rows="3"  class="form-control"  name="project_pacu_text_en[]" placeholder="กรอกรายละเอียด Gallery"></textarea>
                                                    </div>
                                                </div>
                                                <?php }?>

                                                <div id="Project_pacu"></div>


                                                <div class="form-group">
                                                    <label class="control-label col-md-3">
                                                    </label>
                                                    <div class="col-md-4">
                                                        <button type="button" class="btn btn-round btn-success" onclick="addProject_pacu()" >+
                                                            เพิ่มสิ่งอำนวยความสะดวก
                                                        </button>
                                                    </div>
                                                </div>

                                                <script>
                                                    $("#pacu0").fileinput({
                                                    uploadUrl: "upload.php", // server upload action
                                                    maxFileCount: 1,
                                                    allowedFileExtensions: ["jpg", "png", "jpeg"],
                                                    browseLabel: 'สิ่งอำนวยความสะดวก',
                                                    removeLabel: 'ลบ',
                                                    browseClass: 'btn btn-success',
                                                    showUpload: false,
                                                    showRemove:false,
                                                    showCaption: false,
                                                    msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                    msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                                                    msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                                    msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                    xxx:'upload_project_pacu_seo',
                                                    dropZoneTitle : 'รูปสิ่งอำนวยความสะดวก',
                                                    minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                                                    minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                                                    maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                                                    maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                                                    maxFileSize :300 ,
                                                    msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                                    msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                                    msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                                    msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                                    msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                                });
                                            </script>

                                            <br>
                                            <?php }?>
                                            <!--                                       </figure >-->
                                        </div>
                                        <!-- community-and-room-feature -->

                                        <!-- Only Admin -->
                                        <?php if ($_SESSION['group_id'] == 1) {?>
                                        <div class="privilage">
                                            <?php } else {?>
                                            <div class="privilage hide">
                                                <?php }?>

                                                <!--                                            <figure class="highlight">-->

                                                    <hr>
                                                    <h4><u>ข้อมูล SEO (ห้ามแก้ไขโดยไม่แจ้ง webmaster / <span style="color: red;">หากต้องการแก้ไข หรือขอข้อมูลแจ้ง Panatson@lh.co.th</span> )</u></h4>
                                                    <p class="font-gray-dark">
                                                    </p>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Title *<span
                                                            class="required"></span>
                                                             <br> (กรอกได้ไม่เกิน 160 คำ)
                                                        </label>
                                                        <div class="col-md-6">
                                                            <input type="text" id="project_title_seo_th"
                                                            required
                                                            maxlength="160"
                                                            name="project_title_seo_th"
                                                            value="<?php echo htmlspecialchars($dataseo['project_title_seo_th']); ?>"
                                                            class="form-control col-md-7 col-xs-12"
                                                            placeholder="Title SEO (จีน) กรอกได้ไม่เกิน 160 คำ"
                                                            >
                                                        </div>
                                                    </div>
                                                    <div class="form-group hide-for-th">
                                                        <label class="control-label col-md-3">Title <span
                                                            class="required"></span>
                                                             <br> (กรอกได้ไม่เกิน 160 คำ)
                                                        </label>
                                                        <div class="col-md-6">
                                                            <input type="text"
                                                            maxlength="160"
                                                            name="project_title_seo_en"
                                                            id="project_title_seo_en"
                                                            value="<?php echo htmlspecialchars($dataseo['project_title_seo_en']); ?>"
                                                            class="form-control col-md-7 col-xs-12"
                                                            placeholder="Title SEO (อังกฤษ) กรอกได้ไม่เกิน 160 คำ">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Description *
                                                            <br>(กรอกได้ไม่เกิน 300 คำ)
                                                            <span
                                                            class="required">
                                                            </span>
                                                        </label>
                                                        <div class="col-md-6">
                                                            <textarea id="project_des_seo_th" class="form-control" required
                                                            maxlength="300"
                                                            name="project_des_seo_th" rows="7"
                                                            placeholder="Description (จีน) กรอกได้ไม่เกิน 300 คำ"><?php echo htmlspecialchars($dataseo['project_des_seo_th']); ?></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group hide-for-th">
                                                        <label class="control-label col-md-3">Description  <br>(กรอกได้ไม่เกิน 300 คำ)<span
                                                            class="required">

                                                            </span>

                                                        </label>
                                                        <div class="col-md-6">
                                                            <textarea id="project_des_seo_en" class="form-control"
                                                            maxlength="300"
                                                            name="project_des_seo_en" rows="7"
                                                            placeholder="Description (อังกฤษ) กรอกได้ไม่เกิน 300 คำ"><?php echo htmlspecialchars($dataseo['project_des_seo_en']); ?></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Keyword <span
                                                            class="required"></span>
                                                        </label>
                                                        <div class="col-md-6">
                                                         <textarea id="project_keyword_seo_th" class="form-control" required
                                                         name="project_keyword_seo_th" rows="7"
                                                         placeholder="ตัวอย่าง : บ้านเดี่ยว , บ้านใหม่ ,ทาว์โฮม"/><?php echo htmlspecialchars($dataseo['project_keyword_th']); ?></textarea>
                                                     </div>
                                                 </div>

                                                 <div class="form-group hide-for-th">
                                                    <label class="control-label col-md-3">Keyword  <span
                                                        class="required"></span>
                                                    </label>
                                                    <div class="col-md-6">
                                                     <textarea id="project_keyword_seo_en" class="tags form-control"
                                                     name="project_keyword_seo_en" rows="7"
                                                     placeholder="EX : Single house , Townhome "/><?php echo htmlspecialchars($dataseo['project_keyword_en']); ?></textarea>
                                                 </div>
                                             </div>
                                             <hr>

                                             <!-- Banner Activity -- >
                                                <?php include 'BannerActivity.php';?>
                                                <!-- END Banner -->

                                                <hr>
                                                <span class="section">Admin for Approve</span>
                                                <h4><u>กำหนดสถานะข้อมูล</u></h4><br>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">กำหนดสถานะโครงการ</label>
                                                    <div class="col-md-5">
                                                        <p style="padding: 5px;">
                                                            <?php if ($project[0]['project_status'] == 'NP') {?>
                                                            <input type="radio" name="project_status" id="status_on" checked value="NP" class="flat"/>
                                                            โครงการใหม่ (6เดือนนับจากเปิดข้อมูลบนเว็บไซต์)
                                                            <br/>
                                                            <input type="radio" name="project_status" id="status_off" value="SO" class="flat"/>
                                                            ปิดการขาย
                                                            <?php } else if ($project[0]['project_status'] == 'SO') {?>
                                                            <input type="radio" name="project_status" value="NP" id="status_on" class="flat"/>
                                                            โครงการใหม่ (6เดือนนับจากเปิดข้อมูลบนเว็บไซต์)
                                                            <br/>
                                                            <input type="radio" name="project_status" checked value="SO" id="status_off" class="flat"/>
                                                            ปิดการขาย
                                                            <?php } else {?>
                                                            <input type="radio" name="project_status" value="NP" id="status_on" class="flat"/>
                                                            โครงการใหม่ (6เดือนนับจากเปิดข้อมูลบนเว็บไซต์)
                                                            <br/>
                                                            <input type="radio" name="project_status"  value="SO" id="status_off" class="flat"/>
                                                            ปิดการขาย
                                                            <?php }?>
                                                            <p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"></label>
                                                            <div class="col-md-5">
                                                                <input type="button" value="Clear" onclick="Clear();" class="btn btn-default"/>
                                                            </div>
                                                        </div>
                                                        <script>
                                                            function Clear()
                                                            {
                                                                $('#status_on').iCheck('uncheck');
                                                                $('#status_off').iCheck('uncheck');

                                                        //$('input[name=project_status]').attr('checked', false);
                                                    }

                                                </script>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">URL Register</label>
                                                    <div class="col-md-5">
                                                        <input type="text" disabled name="project_url_register" value="<?=$project[0]['project_url_register'] == ' ' ? '' : $project[0]['project_url_register'];?>" class="form-control" placeholder="EX : http://lh.co.th or https://lh.co.th"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">กำหนดวันแสดงผลงานบนเว็บไซต์
                                                    </label>
                                                    <?php

$date_start = $project[0]['public_date'];
$dates = $date_start;
$year = substr($dates, 0, 4);
$day = substr($dates, 8, 2);
$mont = substr($dates, 5, 2);
$start_date = $day . "/" . $mont . "/" . $year;

?>
                                                    <div class="col-md-4">
                                                        <fieldset>
                                                            <div class="control-group">
                                                                <div class="controls" >
                                                                    <div class="form-group" >
                                                                        <input type="text" class="form-control has-feedback-left"
                                                                        value="<?php if (($project[0]['public_date'] != "1900-01-01") && ($project[0]['public_date'] != null)) {echo $start_date;} else {echo "";}?>"
                                                                        name="public_date"
                                                                        id="public_date" placeholder="ระบุวันแสดงผลงาน"
                                                                        aria-describedby="inputSuccess2Status" readonly>
                                                                        <span class="fa fa-calendar-o form-control-feedback left"
                                                                        aria-hidden="true"></span>
                                                                        <span id="inputSuccess2Status"
                                                                        class="sr-only">(success)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                <br>
                                                <!--                                            </figure>-->
                                            </div>

                                            <center>
                                                <?php
$sql_project = "SELECT s.product_id FROM LH_PROJECTS j LEFT JOIN LH_PROJECT_SUB s ON j.project_id = s.project_id WHERE j.project_id = '" . $_GET['project_id'] . "' ";
$query_project = mssql_query($sql_project);

while ($row_project = mssql_fetch_array($query_project)) {
	if ($row_project['product_id'] == 3) {
		$url = 'project-info-condominium.php';
		break;
	} else {
		$url = 'project-info-home.php';
	}
}

?>

                                                <?php
//http://uat.lh.co.th/th/singlehome/project/นันทวัน-บางนา-กม.7/1
$sql_preview = "SELECT PS.product_id,p.project_name_th,product_name_en FROM LH_PROJECT_SUB PS
                                                LEFT JOIN LH_PROJECTS P
                                                ON PS.project_id = P.project_id
                                                LEFT JOIN LH_PRODUCTS PD
                                                ON PS.product_id = PD.product_id
                                                WHERE PS.project_id = '" . $_GET['project_id'] . "'";
$query_preview = mssql_query($sql_preview);
$row_preview = mssql_fetch_array($query_preview);

if ($row_preview['product_id'] == '2') {
	$url_path = 'cn/townhome/project/';
	$url_view = $url_master . $url_path . str_replace(' ', '-', $row_preview['project_name_th']) . '/' . $row_preview['product_id'];

} elseif ($row_preview['product_id'] == '3') {
	$url_path = 'cn/condominium/';
	$url_view = $url_master . $url_path . str_replace(' ', '-', $row_preview['project_name_th']);

} else {
	$url_path = 'cn/singlehome/project/';
	$url_view = $url_master . $url_path . str_replace(' ', '-', $row_preview['project_name_th']) . '/' . $row_preview['product_id'];

}
?>

                                                <a href="<?=$url_view?>" target="_blank">
                                                    <button type="button" name="updatefrom" value="preview" class="btn btn-warning">
                                                        Preview
                                                    </button>
                                                </a>
                                                <button type="submit" name="updatefrom" value="update" class="btn btn-success">
                                                    Update
                                                </button>

                                                <a class="confirms" data-title="ยืนยันการลบข้อมูล" name="delete" href="delete_project.php?project_id=<?=$_GET['project_id'];?>">
                                                    <button type="button"  class="btn btn-danger">Delete</button>
                                                </a>
                                            </center>
                                        </div>

                                        <!-- ./ privilage -->

                                        <!-- Only Sales -->
                                        <?php if ($_SESSION['group_id'] != 1) {?>
                                        <div class="privilage-sales">
                                            <?php } else {?>
                                            <div class="privilage-sales hide">
                                                <?php }?>
                                                <!--                                                <center>-->
                                                    <!--                                                    <button type="submit" name="updatefrom" value="update" class="btn btn-success">-->
                                                        <!--                                                        Update-->
                                                        <!--                                                    </button>-->
                                                        <!--                                                </center>-->
                                                    </div>
                                                    <input type="hidden" name="project_id" value="<?php echo $projectId; ?>">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /page content -->
                            </div>
                        </div>

                        <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top"><span class="glyphicon glyphicon-chevron-up"></span></a>

                        <!-- date -->
                        <link rel="stylesheet" type="text/css" href="../build/css/jquery-ui.css"/>
                        <script src="../build/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
                        <!-- Bootstrap -->
                        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
                        <!-- FastClick -->
                        <script src="../vendors/fastclick/lib/fastclick.js"></script>
                        <!-- NProgress -->
                        <script src="../vendors/nprogress/nprogress.js"></script>
                        <!-- iCheck -->
                        <script src="../vendors/iCheck/icheck.min.js"></script>
                        <!-- jQuery Tags Input -->
                        <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>

                        <!-- bootstrap-daterangepicker -->
                        <!--            <script src="js/moment/moment.min.js"></script>-->
                        <!--            <script src="js/datepicker/daterangepicker.js"></script>-->
                        <!--            -->
                        <!--            <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>-->
                        <!--            <script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>-->

                        <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
                        <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>
                        <script type="text/javascript" src="js/jquery.number.js"></script>

                        <!--            Edit-->
                        <!--   froala JS     -->
                        <script type="text/javascript" src="libs/codemirror/5.3.0/codemirror.min.js"></script>
                        <script type="text/javascript" src="libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/froala_editor.pkgd.min.js"></script>
                        <!-- Include Plugins. -->
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/align.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/char_counter.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/code_beautifier.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/code_view.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/colors.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/emoticons.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/entities.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/file.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/font_family.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/font_size.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/fullscreen.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/image.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/image_manager.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/inline_style.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/line_breaker.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/link.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/lists.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/paragraph_format.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/paragraph_style.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/quick_insert.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/quote.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/table.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/save.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/url.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/video.min.js"></script>
                        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/word_paste.min.js"></script>
                        <!--            end edit-->



                        <script src="../build/js/jquery.validate.js"></script>
                        <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>
                        <!-- Validate -->
                        <script src="js/js_qc_projectview.js" type="text/javascript"></script>

                        <script>
                            $(function() {
                // Set up the number formatting.
                $('#price1').number(true, '');
                $('#price2').number(true, '');
                $('#price3').number(true, '');
                $('#price4').number(true, '');
                $('#price5').number(true, '');
                $('#central_value').number(true, '');
                $('input.progress2').number(true, 2);


            });
        </script>

        <script type="text/javascript">
            $('a.confirms').confirm({
                content: 'คุณแน่ใจที่จะลบข้อมูลนี้ !',
                buttons: {
                    Yes: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        keys: ['enter', 'a'],
                        action: function(){
                            location.href = this.$target.attr('href');
                        }
                    },
                    No: {
                        text: 'No',
                        btnClass: 'btn-default',
                        keys: ['enter', 'a'],
                        action: function(){
                            // button action.
                        }
                    },

                }
            });

            var elem = document.querySelector('.color-input');
            var hueb = new Huebee( elem, {
               notation: 'hex'
                // options
            });
        </script>


        <script>

            $(document).ready(function () {

                var userId = <?php echo $_SESSION['group_id']; ?>;
                if (userId == 1 || userId == '1') {
                    $("#project_name_th").removeAttr('disabled');
                    $("#project_name_en").removeAttr('disabled');
                    $("#detailband").removeAttr('disabled');
                    //$(".detailZone_id").prop('readonly', false);
                    $(".detailZone_id").removeAttr('readonly');

                    $("#project_name_th").prop('required',true);
                    $("#project_name_en").prop('required',true);
                    $("#detailband").prop('required',true);
                    $(".detailZone_id").prop('required',true);
                }else{
                    $(".detailZone_id").prop('readonly', true);
                }

                <?php foreach (getAllCommunityFeature() as $value) {?>
                    $("#community-img-<?php echo ($value['community_features_id']) ?>").fileinput({
                        uploadUrl: "upload.php",
                        dropZoneTitle : 'ลากและวางภาพสิ่งอานวยความสะดวก',
                        browseLabel: 'เลือกภาพสิ่งอานวยความสะดวก',
                        removeLabel: 'ลบ',
                        browseClass: 'btn btn-success',
                        showUpload: false,
                        maxFileCount: 1,
                        mainClass: "input-group-md",
                        xxx:'community-img-seo-<?php echo ($value['community_features_id']) ?>',
                        allowedFileTypes: ["image"]
                    });
                    <?php }?>

                    $(".community_features").on("ifChanged", function (event) {
                        var id = $(this).val();
                        if ($(this).prop('checked')) {
                            $('#community-'+id).removeClass('hide');
                        } else {
                            $('#community-'+id).addClass('hide');
                        }
                    });

                    <?php foreach (getAllRoomFeature() as $value) {?>
                        $("#room-img-<?php echo ($value['room_features_id']) ?>").fileinput({
                            uploadUrl: "upload.php",
                            dropZoneTitle : 'ลากและวางภาพสิ่งอานวยความสะดวก',
                            browseLabel: 'เลือกภาพสิ่งอานวยความสะดวก',
                            removeLabel: 'ลบ',
                            browseClass: 'btn btn-success',
                            showUpload: false,
                            maxFileCount: 1,
                            mainClass: "input-group-md",
                            xxx:'room-img-seo-<?php echo ($value['room_features_id']) ?>',
                            allowedFileTypes: ["image"]
                        });
                        <?php }?>

                        $(".room_features").on("ifChanged", function (event) {
                            var id = $(this).val();
                            if ($(this).prop('checked')) {
                                $('#room-'+id).removeClass('hide');
                            } else {
                                $('#room-'+id).addClass('hide');
                            }
                        });


                        $("#map-image").fileinput({
                            uploadUrl: "upload.php",
                            dropZoneTitle : 'รูปแผนที่โครงการ',
                            browseLabel: 'เลือกภาพแผนที่โครงการ',
                            removeLabel: 'ลบ',
                            browseClass: 'btn btn-success',
                            showUpload: false,
                            showRemove: false,
                            showCaption: false,
                            msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                            msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                            msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                            maxFileCount: 1,
                            mainClass: "input-group-md",
                            xxx:'map-image-seo',
                            allowedFileTypes: ["image"],
                            minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                            minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                            maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                            maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                            maxFileSize :300 ,
                            msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                            msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                            msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                            msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                            msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                        });

                        $("#map-pdf").fileinput({
                            uploadUrl: "upload.php",
                            dropZoneTitle : 'แผนที่โครงการ PDF ',
                            browseLabel: 'เลือกแผนที่โครงการ',
                            removeLabel: 'ลบ',
                            browseClass: 'btn btn-success',
                            showUpload: false,
                            showRemove:false,
                            showCaption: false,
                            msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                            msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                            msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                            maxFileCount: 1,
                            mainClass: "input-group-md",
                            allowedFileTypes: ["pdf"]
                        });

                        $("#map-image-logo").fileinput({
                            uploadUrl: "upload.php",
                            dropZoneTitle : 'Logo โครงการ',
                            browseLabel: 'เลือก Logo โครงการ',
                            removeLabel: 'ลบ',
                            browseClass: 'btn btn-success',
                            showUpload: false,
                            showRemove: false,
                            showCaption: false,
                            maxFileSize :300 ,
                            msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                            msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                            msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                            msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                            msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                    //minImageWidth: 150, //ขนาด กว้าง ต่ำสุด
                    //minImageHeight: 150, //ขนาด ศุง ต่ำสุด
                    //maxImageWidth: 150, //ขนาด กว้าง ต่ำสุด
                    //maxImageHeight: 150, //ขนาด ศุง ต่ำสุด
                    msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                    msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                    maxFileSize :300 ,
                    msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                    msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                    msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                    msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                    msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                    maxFileCount: 1,
                    mainClass: "input-group-md",
                    xxx:'map-image-logo-seo',
                    allowedFileExtensions: ["jpg", "png", "jpeg" , "gif"],
                });

                        $("#map-image-brochure").fileinput({
                            uploadUrl: "upload.php",
                            dropZoneTitle : 'Brochure โครงการ',
                            browseLabel: 'เลือก Brochure',
                            removeLabel: 'ลบ',
                            browseClass: 'btn btn-success',
                            showUpload: false,
                            showRemove: false,
                            showCaption: false,
                            maxFileSize :300 ,
                            msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                            msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                            msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                            msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                            msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                            msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                            msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                            maxFileCount: 1,
                            mainClass: "input-group-md",
                            xxx:'map-image-brochure-seo',
                            allowedFileExtensions: ["pdf"]
                        });



                        $("#upload-project-img").fileinput({
                            uploadUrl: "upload.php",
                            browseLabel: 'เลือก Lead Image',
                            dropZoneTitle : 'ลากและวาง Lead Image',
                            removeLabel: 'ลบ',
                            browseClass: 'btn btn-success',
                            showUpload: false,
                            showRemove: false,
                            msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                            msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                            maxFileCount: 6,
                    minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                    minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                    maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                    maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                    maxFileSize :300 ,
                    msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                    msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                    msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                    msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                    msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                    mainClass: "input-group-md",
                    xxx:'upload-project-leadImg-seo',
                    allowedFileExtensions: ["jpg", "png", "jpeg" , "gif"],
                });

                        $("#upload-project-vdo").fileinput({
                            uploadUrl: "upload.php",
                            dropZoneTitle : 'ลากและวาง VDO',
                            browseLabel: 'เลือก VDO',
                            removeLabel: 'ลบ',
                            browseClass: 'btn btn-success',
                            showUpload: false,
                            showRemove: false,
                            msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                            msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                            maxFileCount: 1,
                            mainClass: "input-group-md",
                            xxx:'upload-project-leadVdo-seo',
                            allowedFileExtensions: ["mp4"]
                        });

                        $("#upload-project-galery").fileinput({
                            uploadUrl: "upload.php",
                            dropZoneTitle : 'ลากและวาง Gallery Image',
                            browseLabel: 'เลือก Gallery',
                            removeLabel: 'ลบ',
                            browseClass: 'btn btn-success',
                            showUpload: false,
                            showRemove: false,
                            msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                            msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                            maxFileSize :300 ,
                            msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                            msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                            msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                            msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                            msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                            mainClass: "input-group-md",
                            xxx:'upload-project-galery-seo',
                            allowedFileExtensions: ["jpg", "png", "jpeg" , "gif"],
                        });

                        $("#img").fileinput(
                    //'enable',
                    {
                        uploadUrl: "upload.php", // server upload action
                        maxFileCount: 1,
                        allowedFileExtensions: ["jpg", "gif", "png", "jpeg"],
                        browseLabel: 'เลือกภาพ Cover 360',
                        removeLabel: 'ลบ',
                        browseClass: 'btn btn-success',
                        showUpload: false,
                        showRemove: false,
                        showCaption: false,
                        maxFileSize :300 ,
                        minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                        minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                        maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                        maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                        msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                        msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                        msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                        msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                        msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                        msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                        msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                        msgZoomModalHeading: 'ตัวอย่างละเอียด',
                        xxx:'seo_img_360',
                        dropZoneTitle : 'Cover image 360',


                    });


                        $("#upload-project-position").fileinput({
                            uploadUrl: "upload.php",
                            dropZoneTitle : 'Gallery Image',
                            showUpload: false,
                            showRemove: false,
                            showCaption: false,
                            maxFileSize :300 ,
                            msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                            msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                            msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                            msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                            msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                            msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                            msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                            mainClass: "input-group-md",
                            allowedFileTypes: ["image","video"]
                        });

                        $(document).ready(function(){
                            $('#open-day-all').iCheck('check');

                            var openDay = '<?php echo $detailContact[0]["open_day"]; ?>';

                            $("#open-day-all").on('ifChecked', function(event){
                                $('#close-day').addClass('hide');
                            });

                            $("#open-day-sum").on('ifChecked', function(event){
                                $('#close-day').removeClass('hide');
                            });

                            if ((openDay == 'all') ||(openDay == '0') ) {
                                $('#open-day-all').iCheck('check');
                                $('#close-day').addClass('hide');
                            }else{
                                $('#open-day-sum').iCheck('check');
                                $('#close-day').removeClass('hide');
                            }


                        });

                //check checked product id == 3
                $( ".product_checkboxt" ).each(function( index ) {
                    if( $(this).val() == 3 ){
                        if ( $(this).is(':checked') ) {
                            $('#community-and-room-feature').removeClass('hide');
                            $('#progess2').removeClass('hide');
                        }
                    }
                });



                var isCheckTypeProject = '<?php if (getTypeProjectImageByIdProject() != null && getTypeProjectImageByIdProject() != "") {
	echo getTypeProjectImageByIdProject()[0]['LEAD_IMAGE_PROJECT_FILE_TYPE'];
}?>';
                if (isCheckTypeProject == 'image') {
                    $("#optionsRadios-img").prop( "checked", true );
                    $("#upload-project-img").prop('disabled',false);
                    $("#upload-project-img").fileinput('enable');
                    $("#upload-project-vdo").fileinput('disable');

                }else{
                    $("#optionsRadios-vdo").prop( "checked", true );
                    $("#upload-project-img").fileinput('disable');
                    $("#upload-project-img").prop('disabled',true);
                }


                $('#prepaiddate').datepicker({
                    autoclose: true,
                    changeMonth: true,
                    showDropdowns: true,
                    changeYear: true,
                    minDate:  new Date(),
                    onSelect: function(dateText) {
                        var d = new Date($(this).datepicker("getDate"))
                        //$scope.value.posted = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate();
                    }
                });

                $('#public_date').datepicker({
                    autoclose: true,
                    changeMonth: true,
                    showDropdowns: true,
                    changeYear: true,
                    minDate:  new Date(),
                    onSelect: function(dateText) {
                        var d = new Date($(this).datepicker("getDate"))
                        //$scope.value.posted = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate();
                    }
                });

//                    $('#public_date').datetimepicker({
//                        locale: 'th',
//                        format: 'YYYY-MM-DD'
//                    });

$(".product_checkboxt").on("ifChanged", function (event) {

    var value = $(this).val();
    var text = $(this).text();
    var productId = "detail_checkbox" + value;

    if ($(this).prop('checked')) {
        addTagCheck(value);
                        // add trigger  Call Function detailProduct()
                        $(this).click();
                    } else {
                        // remove
                        removeTagCheck(value);
                        // check remove condo or house
                        if (value != "3") {
                            //alert(value);
                            removeHouse(productId);
                        }
                        else {
                            //alert("no "+value);
                            removeCondo(productId);
                            removeUnitPlanCondo();
                            removeMasterPlanCondo();
                            removeProgress();
                            removePraking();

                            $('#community-and-room-feature').addClass('hide');
                            $('#progess2').addClass('hide');
                        }
                    }

                });

$('.fancybox').fancybox();


initProgress();


                // Init JS Image Master Plan

                // Case Add
                $("#master_plan_img_name").fileinput({
                    uploadUrl: "upload.php",
                    dropZoneTitle : 'Master Plan',
                    browseLabel: 'เลือกภาพ',
                    removeLabel: 'ลบ',
                    browseClass: 'btn btn-success',
                    showUpload: false,
                    showRemove: false,
                    showCaption: false,
                    xxx : 'master_seo',
                    minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                    minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                    maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                    maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                    maxFileSize :300 ,
                    msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                    msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                    msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                    msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                    msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                    msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                    msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                    maxFileCount: 1,
                    mainClass: "input-group-md",
                    allowedFileTypes: ["image"]
                });


                //End Init JS Image Master Plan


                // Init JS Image Floor Plan
                // Case Add
                $(".floorr_plan_img_name").fileinput({
                    uploadUrl: "upload.php",
                    dropZoneTitle : 'ลากและวางภาพ',
                    browseLabel: 'เลือกภาพ',
                    removeLabel: 'ลบ',
                    browseClass: 'btn btn-success',
                    showUpload: false,
                    showRemove: false,
                    showCaption: false,
                    maxFileSize :300 ,
                    msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                    msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                    msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                    msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                    msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                    msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                    msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                    maxFileCount: 1,
                    mainClass: "input-group-md",
                    xxx:'floor_plan_img_seo',
                    allowedFileTypes: ["image"]
                });

                // Case Update
                // End  JS Image Floor Plan


                // Init JS Image UnitPlan
                // Case Add
                $(".unit_plan_img_name").fileinput({
                    uploadUrl: "upload.php",
                    dropZoneTitle : 'Unit Plan',
                    browseLabel: 'เลือกภาพ',
                    removeLabel: 'ลบ',
                    browseClass: 'btn btn-success',
                    showUpload: false,
                    showRemove: false,
                    showCaption: false,
                    maxFileCount: 1,
                    maxFileSize :300 ,
                    msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                    msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                    msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                    msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                    msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                    mainClass: "input-group-md",
                    xxx:'unit_plan_img_name_seo',
                    allowedFileTypes: ["image"]
                });

                //Case Edit
                <?php

if (@count($detailSubproject) > 0
	&& @array_key_exists("dataUnitPlanCondo", $detailSubproject[$find_index_plan])
	&& @sizeof($detailSubproject[$find_index_plan]["dataUnitPlanCondo"]) > 0) {
	$sql_unit = "select lh_unit_plan_condo.*,lh_unit_plan_img.*
                    from lh_unit_plan_condo
                    INNER JOIN lh_unit_plan_img ON lh_unit_plan_condo.unit_plan_id = lh_unit_plan_img.unit_plan_id
                    where lh_unit_plan_condo.project_id = '" . $_GET['project_id'] . "'";
	$query_unit = mssql_query($sql_unit);
	//mssql_fetch_array($query_unit)
	while ($itemDataUnitPlan = mssql_fetch_array($query_unit))
	//foreach ($detailSubproject[$find_index_plan]["dataUnitPlanCondo"] as $itemDataUnitPlan )
	{
		$key = "unit_plan_img" . $itemDataUnitPlan['unit_plan_img_id'];
		echo '$' . "(" . '"' . '#' . $key . '"' . ").fileinput({
                            uploadUrl: 'upload.php',
                            dropZoneTitle : 'Unit Plan',
                            browseLabel: 'เลือกภาพ',
                            removeLabel: 'ลบ',
                            browseClass: 'btn btn-success',
                            showUpload: false,
                            showRemove: false,
                            showCaption: false,
                            maxFileCount: 1,
                            mainClass: 'input-group-md',
                            xxx:'unit_plan_img_seo" . $itemDataUnitPlan['unit_plan_img_id'] . "',
                            allowedFileTypes: ['image'],
                            maxFileSize :300 ,
                            msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                            msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                            msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                            msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                            msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                        });";
	}
}
?>

                // End Init Js Image UnitPlan

                // Init Js Image Progress
                // Case Add
                $("#progress_img_part_add1").fileinput({
                    uploadUrl: "upload.php",
                    dropZoneTitle : 'รูป Progress',
                    browseLabel: 'เลือกภาพ Progress',
                    removeLabel: 'ลบ',
                    browseClass: 'btn btn-success',
                    showUpload: false,
                    showRemove: false,
                    showCaption: false,
                    maxFileCount: 1,
                    mainClass: "input-group-md",
                    xxx:'progress_img_seo_part_add1',
                    allowedFileTypes: ["image"],
                    maxFileSize :300 ,
                    msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                    msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                    msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                    msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                    msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                });

                // Case Update
                <?php

for ($i = 0; $i < @$sizeProgressCondo; $i++) {
	$key_progress = $dataProgressCondo[$i]['progress_update_id'];

	$dataProgressImg = $dataProgressCondo[$i]["progressImg"];
	$sizeDataProgressImg = sizeof($dataProgressImg);

	if ($sizeDataProgressImg == 0) {
		$initImgprogressSec = "progress_img_part_seo_" . $key_progress . "_" . "0";
		echo '$' . "(" . '"' . '#' . $initImgprogressSec . '"' . ").fileinput({
                            uploadUrl: 'upload.php',
                            dropZoneTitle : 'แก้รูป progress',
                            browseLabel: 'เลือกภาพ',
                            removeLabel: 'ลบ',
                            browseClass: 'btn btn-success',
                            showUpload: false,
                            showRemove: false,
                            showCaption: false,
                            maxFileCount: 1,
                            mainClass: 'input-group-md',
                            xxx:'progress_img_seo_part_" . $key_progress . "',
                            allowedFileTypes: ['image'],
                            maxFileSize :300 ,
                            msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                            msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                            msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                            msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                            msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                        });";

	} else {
		for ($j = 0; $j < $sizeDataProgressImg; $j++) {

			$initImgprogressSec = "progress_img_part_seo_" . $key_progress . "_" . $j;
			$progressimgeSec = $dataProgressImg[$j]['progress_update_img_id'];

			echo '$' . "(" . '"' . '#' . $initImgprogressSec . '"' . ").fileinput({
                                uploadUrl: 'upload.php',
                                dropZoneTitle : 'แก้รูป progress',
                                browseLabel: 'เลือกภาพ',
                                removeLabel: 'ลบ',
                                browseClass: 'btn btn-success',
                                showUpload: false,
                                showRemove: false,
                                showCaption: false,
                                maxFileCount: 1,
                                mainClass: 'input-group-md',
                                xxx:'progress_img_seo_part_" . $progressimgeSec . "',
                                allowedFileTypes: ['image'],
                                maxFileSize :300 ,
                                msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                            });";

		}
	}
}

?>

                // End Init Js Image Progress

                //scrope back to top
                $(window).scroll(function () {
                    if ($(this).scrollTop() > 50) {
                        $('#back-to-top').fadeIn();
                    } else {
                        $('#back-to-top').fadeOut();
                    }
                });
                // scroll body to 0px on click
                $('#back-to-top').click(function () {
                    $('#back-to-top').tooltip('hide');
                    $('body,html').animate({
                        scrollTop: 0
                    }, 800);
                    return false;
                });

                $('#back-to-top').tooltip('show');
                //end scrope back to top
            });
            //end document ready
            $("#alert").fadeTo(3000, 500).slideUp(500, function(){
                $("#alert").slideUp(500);
            });


            function removeiIndexSeo(valImgFloorId)
            {
                var ele = "floor-removeindex-seo"+valImgFloorId ;

                console.log(ele);

                $('#'+ele).remove();
            }


            $(document).on('change', '.progress_img' , function(){

                var countFiles = $(this)[0].files.length;
                var imgPath = $(this)[0].value;

                var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
                var image_holder = $($(this)["0"].parentNode).next();



                image_holder.empty();
                if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
                    if (typeof(FileReader) != "undefined") {
                        //loop for each file selected for uploaded.
                        for (var i = 0; i < countFiles; i++)
                        {
                            var reader = new FileReader();
                            reader.onload = function(e) {
                                $("<img />", {
                                    "src": e.target.result,
                                    "class": "img-thumbnail"
                                }).appendTo(image_holder);
                            }
                            image_holder.show();
                            reader.readAsDataURL($(this)[0].files[i]);
                        }
                    } else {
                        alert("This browser does not support FileReader.");
                    }
                } else {
                    alert("Pls select only images");
                }

            });


            function preSubmitValidateCheckbox(){
                var checked = $('p#product_chkb :checkbox:checked').length;
                if(!checked) {
                    $('#error-checkbox').html("กรุณาเลือกอย่างน้อย 1 รายการ.");
                    $('html, body').animate({
                        scrollTop: $("#error-checkbox").offset().top
                    }, 500);
                    return false;
                }else{
                    $('#error-checkbox').html("");
                    return true;
                }
            }



            function addProgressBlank()
            {

            }

            function detailProduct(productId, productName) {


                if (!checkdetailCheckBox(productId)) {
                    // Detail Condo
                    if (productId == "3") {
                        addCondo(productId, productName);

                        showDivUnitPlan();
                        showDivMasterPlan();
                        showDivProgress();
                        showDivPraking();

                    }
                    else {
                        addHouse(productId, productName);
                    }
                }
                else {
                    $("#detail_checkbox" + productId).removeClass('hidden');
                }

//                    $('.product_checkboxt').each(function () {
//                        if(!this.checked){
//                            $('detail_checkbox'+$(this).val()).hide();
//                        }
//                    });


$('#community-and-room-feature').removeClass('hide');
$('#progess2').removeClass('hide');

}

function addHouse(productId, productName) {


    var headTitle = "<h4><u>" + productName + "</u></h4>";


    var area_m = '<div class="form-group">' +
    '<label class="control-label col-md-3" >พื้นที่ใช้สอยตั้งแต่ </label>' +
    '<div class="col-md-2">' +
    '<input type="text" id="homearea_m[]" name="homearea_m[]" class="form-control" onkeypress="return isNumber(event)" >' +
    '</div >'+
    '<div class="col-md-3" >'+
    '<label class="control-label col-md-3 wdlabel">ตารางเมตร' +
    '</label>' +
    '</div>' +
    '</div>';

    var area_w = '<div class="form-group">' +
    '<label class="control-label col-md-3">สร้างบนขนาดที่ดินตั้งแต่ </label>' +
    '<div class="col-md-2">' +
    '<input type="text"  id="homearea_w[]" name="homearea_w[]" class="form-control " onkeypress="return isNumber(event)" >' +
    '</div >'+
    '<div class="col-md-3" >'+
    '<label class="control-label col-md-3 wdlabel">ตารางวา</label>' +
    '</div>' +
    '</div>';

    var area_num_plan_home = '<div class="form-group">' +
    '<label class="control-label col-md-3">มีแบบบ้านให้เลือก</label>' +
    '<div class="col-md-2">' +
    '<input type="text" id="num_plan_home[]" name="num_plan_home[]" class="form-control " onkeypress="return isNumber(event)"  >' +
    '</div >'+
    '<div class="col-md-3" >'+
    '<label class="control-label col-md-3 wdlabel">แบบ</label>' +
    '</div>' +
    '</div>';

    var form = '<div id="detail_checkbox' + productId + '"' + 'class="form-group">' +
    headTitle +
    area_m +
    area_w +
    area_num_plan_home
    + '</div>';

    var detailElement = findPositionInsertDetailcheckbox(productId);
    if (detailElement != null) {
        console.log("insertBefore");
        $(form).insertBefore("#" + detailElement);
    }
    else {
        console.log("append");
        $("#detail_div_checkbox").append(form);
    }



}


function findPositionInsertDetailcheckbox(productId) {
    console.log("findPositionInsertDetailcheckbox");

    var prosition = null;

    $('#detail_div_checkbox').children('div').each(function (index, value) {

        var elementDivDetailCheckBox = $(this);
        var divId = elementDivDetailCheckBox.attr('id');


        if (divId != null && divId != "undefined" && divId.indexOf("detail_checkbox") != -1) {
                        // Check Number
                        var numberinDiv = divId.replace(/^\D+/g, '');

                        if (productId < numberinDiv) {
                            console.log(productId + "<" + numberinDiv);
                            prosition = divId;
                            return false;
                        }

                    }

                });


    return prosition;
}


function addCondo(productId, productName) {
    var headTitle = "<h4><u>" + productName + "</u></h4>";


    var area_condo = '<div class="form-group">' +
    '<label class="control-label col-md-3">พื้นที่ใช้สอยตั้งแต่ </label>' +
    '<div class="col-md-2">' +
    '<input type="text" id="area_condofrom" name="area_condofrom"  value="" class="form-control col-md-7 col-xs-12 " onkeypress="return isNumber(event)" >' +
    '</div>' +
    '<div class="col-md-1 col-sm-9 col-xs-12">' +
    '<label class="control-label col-md-3 ">&nbsp&nbsp&nbspถึง </label>' +
    '</div>'+
    '<div class="col-md-2">' +
    '<input type="text" id="area_condoto" name="area_condoto" value="" class="form-control col-md-7 col-xs-12 ">' +
    '</div>' +
    '<div class="col-md-3">' +
    '<label class="control-label col-md-2 wdlabel"> ตารางเมตร </label>' +
    '</div>' +
    '</div>';

    var num_plan_condo = '<div class="form-group">' +
    '<label class="control-label col-md-3">มีแบบห้องให้เลือก</label>' +
    '<div class="col-md-2">' +
    '<input type="text" id="num_plan_condo"  name="num_plan_condo" value="" class="form-control" onkeypress="return isNumber(event)"/>' +
    '</div>' +
    '<div class="col-md-3">' +
    '<label class="control-label col-md-3 wdlabel">ห้อง</label>' +
    '</div>' +
    '</div>';

    var area_condo_unit = '<div class="form-group">' +
    '<label class="control-label col-md-3">ชื่อ Unit </label>' +
    '<div class="col-md-2">' +
    '<input type="text" id="unit_condo_name" name="unit_condo_name[]"  value="" class="form-control col-md-7 col-xs-12 " >' +
    '</div>' +
    '<div class="col-md-1 col-sm-9 col-xs-12">' +
    '<label class="control-label col-md-3 ">&nbsp&nbsp&nbspขนาด </label>' +
    '</div>'+
    '<div class="col-md-2">' +
    '<input type="text" id="size_unit_codo" name="size_unit_codo[]" value="" class="form-control col-md-7 col-xs-12 ">' +
    '</div>' +
    '<div class="col-md-3">' +
    '<label class="control-label col-md-2 wdlabel"> ตารางเมตร </label>' +
    '</div>' +
    '</div>';

    var building_uni = '<div class="form-group">' +
    '<label class="control-label col-md-3">จำนวนอาคาร</label>' +
    '<div class="col-md-2">' +
    '<input type="text" id="building_uni" name="building_uni" value="" class="form-control" onkeypress="return isNumber(event)">' +
    '</div>' +
    '<div class="col-md-3">' +
    '<label class="control-label col-md-3 wdlabel">อาคาร</label>' +
    '</div>' +
    '</div>';

    var subdetailCondoOptionBuilding =
    '<div id="subdetailCondoOptionBuilding">' +
    '<div class="form-group">' +
    '<label class="control-label col-md-3" for="group">อาคาร </label>' +
    '<div class="col-md-2 col-sm-9 col-xs-12">' +
    '<select class="form-control" id="building_name[]" name="building_name[]" >' +
    '<option value="">เลือกอาคาร</option>' +
    '<option value="A">A</option>' +
    '<option value="B">B</option>' +
    '<option value="C">C</option>' +
    '<option value="D">D</option>' +
    '<option value="E">E</option>' +
    '<option value="F">F</option>' +
    '<option value="G">G</option>' +
    '<option value="H">H</option>' +
    '<option value="I">I</option>' +
    '<option value="J">J</option>' +
    '<option value="K">K</option>' +
    '<option value="L">L</option>' +
    '<option value="M">M</option>' +
    '<option value="N">N</option>' +
    '<option value="O">O</option>' +
    '<option value="P">P</option>' +
    '<option value="Q">Q</option>' +
    '<option value="R">R</option>' +
    '<option value="S">S</option>' +
    '<option value="T">T</option>' +
    '<option value="U">U</option>' +
    '<option value="V">V</option>' +
    '<option value="W">W</option>' +
    '<option value="X">X</option>' +
    '<option value="Y">Y</option>' +
    '<option value="Z">Z</option>' +
    '</select>' +
    '</div>' +
    '<label class="control-label col-md-1 textleft">จำนวนชั้น' +
    '<span class="required"></span>' +
    '</label>' +
    '<div class="col-md-1">' +
    '<input type="text" name="building_num_layer[]"  value="" class="form-control col-md-7 col-xs-12"  onkeypress="return isNumber(event)">' +
    '</div>' +
    '<label class="control-label col-md-1 textleft">ชั้น<span class="required"></span></label>' +
    '<div class="col-md-1">' +
    '<input type="text" value=""  name="building_unit[]"  class="form-control col-md-7 col-xs-12" onkeypress="return isNumber(event)">' +
    '</div>' +
    '<label class="control-label col-md-1 textleft"> ยูนิต<span class="required"></span> </label>' +
    '</div>' +
    '</div>'+
    '<div class="form-group">'+
    '<label class="control-label col-md-3">'+
    '</label>'+
    '<div class="col-md-3">'+
    '<button type="button" class="btn btn-round btn-success" onclick="add_DetailBuildCondo()" > + เพิ่มข้อมูลอาคาร </button>' +
    '</div>'+
    '</div>';

    var form = '<div id="detail_checkbox' + productId + '"' + 'class="form-group">' +
    headTitle +
    area_condo +
    area_condo_unit +
    num_plan_condo +
    building_uni +
    subdetailCondoOptionBuilding
    + '</div>';

    console.log("findPositionInsertDetailcheckbox" + productId);

    var detailElement = findPositionInsertDetailcheckbox(productId);
    if (detailElement != null) {

        $(form).insertBefore("#" + detailElement);
    }
    else {
        $("#detail_div_checkbox").append(form);

    }

}


function addProject_nearby()
{
    var index = $('.setIndexProjectNear').length;

    var form = `<div class="group-nearby">
    <div  class="form-group">
    <label class="control-label col-md-3 setIndexProjectNear"> ${index + 1})</label>
    <div class="col-md-6">
    <input type="text" name="nearby_name_th[]"  value=""
    class="form-control col-md-7 col-xs-12 wdpercent3"
    placeholder="กรอกสิ่งอำนวยความสะดวกรอบโครงการ (จีน)" />
    <label class="control-label col-md-1 wdlabel1 textright"> ระยะห่าง</label>
    </div>
    <div class="col-md-2" style="width:10%;margin-left: -70px;">
    <input type="text" name="interval[]"
    class="form-control col-md-7 col-xs-12 wdpercent2" style="width: 70%;" onkeypress="return isNumber(event)">
    </div>

    <div class="col-mg-4">
    <select class="form-control col-md-2 wdpercent2" name="nearby_unit[]" style="width: 90px;">
    <option value="กม.">กม.</option>
    <option value="เมตร">เมตร</option>
    </select>
    </div>
    </div>
    <div class="form-group">
    <label class="control-label col-md-3"></label>
    <div class="col-md-6">
    <input type="text" name="nearby_name_en[]"
    placeholder="กรอกสิ่งอำนวยความสะดวกรอบโครงการ (อังกฤษ)"
    class="form-control col-md-7 col-xs-12 wdpercent3 hide-for-th" />
    </div>
    <button type="button" class="btn btn-round btn-danger" onclick="removeProject_nearby(this)">
    - ลบสิ่งอำนวยความสะดวก
    </button>
    </div>
    </div>`;


    $('#project_nearby').append(form);
}




            //galery
            function addProject_galery()
            {


                var index = $('.setIndexProject_galery_galley').length;

                var form = `<div><div class="col-sm-3"><div class="form-group setIndexProject_galery">
                <div class="col-md-12">
                <input type="file" id="galery_${index + 1}" class="gallery_master" name="project_galery[]" accept="image/*" />
                </div>
                </div>

                <div class="form-group setIndexProject_galery_galley">
                <div class="col-md-12">
                <input type="text"  class="form-control"  name="project_galery_text[]" placeholder="กรอกรายละเอียด Gallery (จีน)" value=""/>
                </div>
                </div>

                <div class="form-group setIndexProject_galery_galley hide-for-th">
                <div class="col-md-12">
                <input type="text"  class="form-control"  name="project_galery_text_en[]" placeholder="กรอกรายละเอียด Gallery (อังกฤษ)" value=""/>
                </div>
                </div>


                <button  type="button" class="btn btn-round btn-danger" style="margin-left: 112px;" onclick="removeProject_galery(this)"> - ลบ Gallery</button>

                </div>
                </div>`;



                $('#Project_galery').append(form);

                $(".gallery_master").fileinput({
                    uploadUrl: "upload.php", // server upload action
                    maxFileCount: 1,
                    allowedFileExtensions: ["jpg", "png", "jpeg"],
                    browseLabel: ' เลือก Gallery โครงการ',
                    browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                    removeLabel: 'ลบ',
                    browseClass: 'btn btn-success',
                    showUpload: false,
                    showRemove:false,
                    showCaption: false,
                    msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                    msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                    msgZoomModalHeading: 'ตัวอย่างละเอียด',
                    xxx:'upload-project-galery-seo',
                    dropZoneTitle : 'Gallery โครงการ',
                    maxFileSize :300 ,
                    msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                    msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                    msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                    msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                    msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                });
            }

            function removeProject_galery(element)
            {

                //$('#gallery_add').prop("disabled", false);
                $(element).parent().remove();
            }

            //add_lead_image
            function add_lead_image()
            {

                var index = $('.setIndexProject_galery_galley').length;

                var form = `<div><div class="col-sm-3">
                <div class="form-group setIndexProject_galery">
                <div class="col-md-12">
                <input type="file" id="lead_image${index + 1}" class="lead_image" name="lead_image[]" accept="image/*" />
                </div>
                </div>

                <div class="form-group">
                <div class="col-md-12">
                <input type="text" class="form-control" name="lead_image_url_add[]" placeholder="URL"  value="" />
                </div>
                </div>
                <button  type="button" class="btn btn-round btn-danger" style="margin-left: 112px;" onclick="removeLead_image(this)"> - ลบ Lead Image</button>

                </div>
                </div>`;

                $('#lead_image_project').append(form);

                $(".lead_image").fileinput({
                    uploadUrl: "upload.php", // server upload action
                    maxFileCount: 1,
                    allowedFileExtensions: ["jpg", "png", "jpeg"],
                    browseLabel: ' เลือก Lead image',
                    browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                    removeLabel: 'ลบ',
                    browseClass: 'btn btn-success',
                    showUpload: false,
                    showRemove:false,
                    showCaption: false,
                    msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                    msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                    msgZoomModalHeading: 'ตัวอย่างละเอียด',
                    xxx:'seo_lead_add',
                    dropZoneTitle : 'Lead image',
                    minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                    minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                    maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                    maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                    maxFileSize :300 ,
                    msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                    msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                    msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                    msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                    msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                });
            }

            function removeLead_image(element)
            {
                //$('#gallery_add').prop("disabled", false);
                $(element).parent().remove();
            }

            //
            //add_lead_image_banner
            function add_lead_image_banner()
            {

                var index = $('.setIndexProject_galery_galley').length;

                var form = `<div><div class="col-sm-3">
                <div class="form-group setIndexProject_galery">
                <div class="col-md-12">
                <input type="file" id="lead_image${index + 1}" class="lead_image_banner" name="lead_img_banner[]" accept="image/*" />
                </div>
                </div>

                <div class="form-group">
                <div class="col-md-12">
                <input type="text"  class="form-control" name="lead_img_banner_url_add[]" placeholder="EX : http://lh.co.th or https://lh.co.th" />
                </div>
                </div>
                <button  type="button" class="btn btn-round btn-danger" style="margin-left: 112px;" onclick="removeLead_image(this)"> - ลบรูปภาพ</button>

                </div>
                </div>`;

                $('#lead_image_banner').append(form);

                $(".lead_image_banner").fileinput({
                    uploadUrl: "upload.php", // server upload action
                    maxFileCount: 1,
                    allowedFileExtensions: ["jpg", "png", "jpeg"],
                    browseLabel: ' เลือกรูปภาพหลัก PC',
                    browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                    removeLabel: 'ลบ',
                    browseClass: 'btn btn-success',
                    showUpload: false,
                    showRemove:false,
                    showCaption: false,
                    msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                    msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                    msgZoomModalHeading: 'ตัวอย่างละเอียด',
                    xxx:'seo_lead_banner_add',
                    dropZoneTitle : 'รูปภาพหลัก PC',
                    minImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
                    minImageHeight: 1920, // ขนาด ศุง ต่ำสุด
                    maxImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
                    maxImageHeight: 1920, // ขนาด ศุง ต่ำสุด
                    maxFileSize :300 ,
                    msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                    msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                    msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                    msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                    msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                });
            }

            function removeLead_image(element)
            {
                //$('#gallery_add').prop("disabled", false);
                $(element).parent().remove();
            }


            //
            //pacu
            function addProject_pacu()
            {
                var index = $('.setIndexProject_galery').length;

                var form = `<div>
                <hr>
                <br>
                <div class="form-group">
                <label class="control-label col-md-3">ชื่อสิ่งอำนวยความสะดวกภายในโครงการ
                </label>
                <div class="col-md-6">
                <input type="text"  class="form-control"  name="pacu_text[]" placeholder="กรอกชื่อสิ่งอำนวยความสะดวกภายในโครงการ"></input>
                </div>
                </div>
                <div class="form-group hide-for-th">
                <label class="control-label col-md-3">ชื่อสิ่งอำนวยความสะดวกภายในโครงการ
                </label>
                <div class="col-md-6">
                <input type="text"  class="form-control"  name="pacu_text_en[]" placeholder="กรอกชื่อสิ่งอำนวยความสะดวกภายในโครงการ (อังกฤษ)"></input>
                </div>
                </div>
                <div class="form-group setIndexProject_galery">
                <label class="control-label col-md-3">สิ่งอำนวยความสะดวกของโครงการ ขนาด 1920 x 1080 px <br> (.JPG , .PNG , .GIF )<br> ภาพขนาดไม่เกิน 300 KB
                </label>
                <div class="col-md-6">
                <input type="file" id="pacu${index + 1}" name="project_pacu[]" accept="image/*"/>
                </div>
                </div>
                <div class="form-group setIndexProject_galery">
                <label class="control-label col-md-3">รายละเอียดสิ่งอำนวยความสะดวก (จีน)
                </label>
                <div class="col-md-6">
                <textarea type="text" rows="3"  class="form-control"  name="project_pacu_text[]" placeholder="กรอกรายละเอียด Gallery"></textarea>
                </div>

                </div>

                <div class="form-group setIndexProject_galery hide-for-th">
                <label class="control-label col-md-3">รายละเอียดสิ่งอำนวยความสะดวก (อังกฤษ)
                </label>
                <div class="col-md-6">
                <textarea type="text" rows="3"  class="form-control"  name="project_pacu_text_en[]" placeholder="กรอกรายละเอียด Gallery"></textarea>
                </div>

                </div>

                <div class="form-group setIndexProject_galery">
                <label class="control-label col-md-7">
                </label>
                <div class="col-md-5">
                <button type="button" class="btn btn-round btn-danger" onclick="removeproject_pacu(this)">
                - ลบ สิ่งอำนวยความสะดวก
                </button>
                </div>

                </div>`;


                $('#Project_pacu').append(form);

                $("#pacu"+(index + 1)).fileinput({
                    uploadUrl: "upload.php", // server upload action
                    maxFileCount: 1,
                    allowedFileExtensions: ["jpg", "png", "jpeg"],
                    browseLabel: 'เลือกรูป',
                    removeLabel: 'ลบ',
                    browseClass: 'btn btn-success',
                    showUpload: false,
                    showRemove:false,
                    showCaption: false,
                    msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                    msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                    msgZoomModalHeading: 'ตัวอย่างละเอียด',
                    xxx:'upload_project_pacu_seo',
                    dropZoneTitle : 'รูป สิ่งอำนวยความสะดวก',
                    minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                    minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                    maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                    maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                    maxFileSize :300 ,
                    msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                    msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                    msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                    msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                    msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                });
            }

            function removeproject_pacu(element) {
                $(element).parent().parent().parent().remove();
            }

            //add master paln

            //pacu
            function addProject_master()
            {
                var index = $('.setIndexlead').length;

                var form = `<div><hr>
                <div class="form-group setIndexlead">
                <label class="control-label col-md-3">Master Plan <br>(.JPG ขนาด
                1920 x 1080 px) <br> ภาพขนาดไม่เกิน 300 KB </label>
                <br>
                <div class="col-md-6">
                <input id="master${index + 1}" type="file" name="master_plan_img_name[]"
                class="master_plan_img_name"/>
                </div>
                </div>
                <div class="form-group setIndexlead">
                <label class="control-label col-md-3">คำอธิบาย (จีน)</label>
                <div class="col-md-6">
                <textarea  type="text" name="master_plan_condo_dis_th[]"
                rows="3"
                class="form-control col-md-7 col-xs-12"
                placeholder="กรอกคำอธิบาย (จีน)"></textarea>
                </div>
                </div>
                <div class="form-group setIndexlead hide-for-th">
                <label class="control-label col-md-3">คำอธิบาย (อังกฤษ)</label>
                <div class="col-md-6">
                <textarea  type="text" name="master_plan_condo_dis_en[]"
                rows="3"
                class="form-control col-md-7 col-xs-12"
                placeholder="กรอกคำอธิบาย (อังกฤษ)"></textarea>
                </div>
                </div>
                <div class="form-group">
                <label class="control-label col-md-6"></label>
                <div class="col-md-2 col-md-offset-1">
                <button type="button"  onclick="deleteMaster(this)" class="btn btn-round btn-danger" > - ลบ Master Plan </button>
                </div>
                </div>
                </div>`;


                $('#Project_Master').append(form);

                $("#master"+(index + 1)).fileinput({
                    uploadUrl: "upload.php", // server upload action
                    maxFileCount: 1,
                    allowedFileExtensions: ["jpg", "png", "jpeg"],
                    browseLabel: 'เลือกรูป',
                    removeLabel: 'ลบ',
                    browseClass: 'btn btn-success',
                    showUpload: false,
                    showRemove:false,
                    showCaption: false,
                    xxx : 'master_seo',
                    msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                    msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                    msgZoomModalHeading: 'ตัวอย่างละเอียด',
                    xxx:'upload_project_pacu_seo',
                    dropZoneTitle : 'Master Plan',
                    minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                    minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                    maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                    maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                    maxFileSize :300 ,
                    msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                    msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                    msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                    msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                    msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                });
            }

            function deleteMaster(element) {
                $(element).parent().parent().parent().remove();
            }

            //leade
            function add_leade()
            {
                var index = $('.setIndexlead').length;

                var form = `<div>
                <div class="form-group setIndexlead">
                <label class="control-label col-md-3">Leade image <br>(.JPG , .PNG , .GIF )<br> 1920 x 1080 px
                </label>
                <div class="col-md-6">
                <input type="file" id="leade${index + 1}" name="lead_img_file[]" accept="image/*"/>
                </div>
                <button type="button" class="btn btn-round btn-default" onclick="removeProject_galery2(this)">
                - ลบ Lead image
                </button>
                </div>
                </div>`;


                $('#leade_galery').append(form);

                $("#leade"+(index + 1)).fileinput({
                    uploadUrl: "upload.php", // server upload action
                    maxFileCount: 1,
                    allowedFileExtensions: ["jpg", "png", "jpeg"],
                    browseLabel: 'เลือกรูป',
                    removeLabel: 'ลบ',
                    browseClass: 'btn btn-success',
                    showUpload: false,
                    showRemove:false,
                    msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                    msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                    msgZoomModalHeading: 'ตัวอย่างละเอียด',
                    xxx:'seo_lead_img_file',
                    dropZoneTitle : 'ลากและวางรูป',
                    maxFileSize :300 ,
                    msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                    msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                    msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                    msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                    msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                });
            }


            function addFloorPlan()
            {


                var datenow = new Date ;
                var   dformat = [ datenow.getFullYear(),
                (datenow.getMonth()+1).padLeft(),
                datenow.getDate().padLeft()].join('')+
                '' +
                [ datenow.getHours().padLeft(),
                datenow.getMinutes().padLeft(),
                datenow.getSeconds().padLeft()].join('');

                var initImgeFloorPlan = "floorImgPlan_"+dformat+"";


                var form = '<div>'+
                '<div class="form-group">'+
                '<label class="control-label col-md-2">Floor Plan <br>(.JPG ขนาด AxB) </label><br>'+
                '<div class="col-md-4">'+
                '<input type="file" id="'+initImgeFloorPlan+'" name="floorr_plan_img_name[]" class="floorr_plan_img_name"/>'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<label class="control-label col-md-2">คำอธิบาย </label>'+
                '<div class="col-md-4">'+
                '<input type="text"  name="master_plan_floor_plan_dis_th[]" class="form-control col-md-7 col-xs-12"  placeholder="กรอกคำอธิบาย (จีน)">'+
                '</div>'+
                '<div class="col-md-4">'+
                '<input type="text" name="master_plan_floor_plan_dis_en[]" class="form-control col-md-7 col-xs-12" placeholder="กรอกคำอธิบาย (อังกฤษ)">'+
                '</div>'+
                '</div>'+
                '<br>'+
                '<div class="form-group">'+
                '<label class="control-label col-md-1">'+ '</label>'+
                '<div class="col-md-2">'+
                '<button type="button" onclick="removeFloorPlan(this)" class="btn btn-round btn-default">+ Delete Floor Plan </button>'+
                '</div>'+
                '</div>'+
                '</div>';

                $('#floorplan').append(form);


                $("#"+initImgeFloorPlan).fileinput({
                    uploadUrl: "upload.php",
                    dropZoneTitle : 'ลากและวางภาพ',
                    browseLabel: 'เลือกภาพ',
                    removeLabel: 'ลบ',
                    browseClass: 'btn btn-success',
                    showUpload: false,
                    maxFileCount: 1,
                    mainClass: "input-group-md",
                    xxx:'floor_plan_img_seo',
                    allowedFileTypes: ["image"]
                });

            }

            function removeFloorPlan(element)
            {
                $(element).parent().parent().parent().remove();
            }



            function removeProject_nearby(element)
            {

                $(element).parent().parent().remove();
                resetNoProject_NearBy();
            }

            function add_UnitNew()
            {
                var form = '<div class="form-group">'+
                '<label class="control-label col-md-3">ชื่อ Unit *</label>'+
                '<div class="col-md-2">'+
                '<input type="text" id="unit_condo_name[]" name="unit_condo_name[]" value="" class="form-control col-md-7 col-xs-12">'+
                '</div>'+
                '<div class="col-md-1 col-sm-9 col-xs-12">'+
                '<label class="control-label col-md-3 ">&nbsp;&nbsp;ขนาด </label>'+
                '</div>'+
                '<div class="col-md-2">'+
                '<input type="text" id="size_unit_codo" name="size_unit_codo[]" value="" class="form-control col-md-7 col-xs-12" >'+
                '</div>'+
                '<div class="col-md-3" style="width:90px">'+
                '<label class="control-label"> ตารางเมตร'+
                '</label>'+
                '</div>'+

                '<button type="button" onclick="removeUnitNew(this)" class="btn btn-round btn-danger">- ลบข้อมูล Unit </button>'+
                '</div>';

                $("#unit_new").append(form);
            }

            function add_DetailBuildCondo()
            {
                var form = '<div class="form-group">'+
                '<label class="control-label col-md-3" for="group">อาคาร</label>'+
                '<div class="col-md-2 col-sm-9 col-xs-12">'+
                '<select class="form-control" name="building_name[]" id="building_name[]">'+
                '<option value="" selected="">เลือกอาคาร</option>'+
                '<option value="A">A</option>'+
                '<option value="B">B</option>'+
                '<option value="C">C</option>'+
                '<option value="D">D</option>'+
                '<option value="E">E</option>'+
                '<option value="F">F</option>'+
                '<option value="G">G</option>'+
                '<option value="H">H</option>'+
                '<option value="I">I</option>'+
                '<option value="J">J</option>'+
                '<option value="K">K</option>'+
                '<option value="L">L</option>'+
                '<option value="M">M</option>'+
                '<option value="N">N</option>'+
                '<option value="O">O</option>'+
                '<option value="P">P</option>'+
                '<option value="Q">Q</option>'+
                '<option value="R">R</option>'+
                '<option value="S">S</option>'+
                '<option value="T">T</option>'+
                '<option value="U">U</option>'+
                '<option value="V">V</option>'+
                '<option value="W">W</option>'+
                '<option value="X">X</option>'+
                '<option value="Y">Y</option>'+
                '<option value="Z">Z</option>'+
                '</select>'+
                '</div>'+
                '<label class="control-label col-md-1 textleft">จำนวนชั้น<span class="required"></span></label>'+
                '<div class="col-md-1">'+
                '<input type="text" name="building_num_layer[]"  class="form-control col-md-7 col-xs-12"  onkeypress="return isNumber(event)">'+
                '</div>'+
                '<label class="control-label col-md-1 textleft">ชั้น<span class="required"></span> </label>'+
                '<div class="col-md-1">'+
                '<input type="text"  name="building_unit[]" class="form-control col-md-7 col-xs-12"  onkeypress="return isNumber(event)">'+
                '</div>'+
                '<label class="control-label col-md-1 textleft">ยูนิต<span class="required"></span> </label>'+
                '<button type="button" onclick="removeDetailBuildCondo(this)" class="btn btn-round btn-danger"> - ลบข้อมูลอาคาร</button>'+
                '</div>'+
                '</div>';

                $("#subdetailCondoOptionBuilding").append(form);
            }

            function showDivUnitPlan()
            {
                $('#unitPlanCondo').removeClass('hidden');
            }

            function showDivMasterPlan()
            {

                $('#unitMasterPlanCondo').removeClass('hidden');
            }

            function  showDivProgress()
            {
                $('#progressCondo').removeClass('hidden');
            }

            function  showDivPraking()
            {
                $('#divPraking').removeClass('hidden');
                $('#community-and-room-feature').removeClass('hidden');
            }



            function  addUnitPlanForCondo()
            {
                var datenow = new Date ;
                var   dformat = [ datenow.getFullYear(),
                (datenow.getMonth()+1).padLeft(),
                datenow.getDate().padLeft()].join('')+
                '' +
                [ datenow.getHours().padLeft(),
                datenow.getMinutes().padLeft(),
                datenow.getSeconds().padLeft()].join('');

                var index = $('.initImgeUnitPlan').length;


                var initImgeUnitPlan = "unit_plan_img_name_"+dformat+"";
                var initImgeUnitPlan2 = "unit_plan_img_name_"+index+"";

                var form = '<div>\
                <hr>\
                <div class="form-group">\
                <label class="control-label col-md-3">ชื่อ Unit</label>\
                <div name="unit_plan_img_pervie"></div>\
                <div class="col-md-6">\
                <input type="text" name="unit_plan_name_th_condo[]" class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อ Unit (จีน)">\
                </div>\
                </div>\
                <div class="form-group hide-for-th">\
                <label class="control-label col-md-3">ชื่อ Unit</label>\
                <div class="col-md-6">\
                <input type="text" name="unit_plan_name_en_condo[]" class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อ Unit (อังกฤษ)">\
                </div>\
                <input type="hidden" >\
                </div>\
                <div class="form-group">\
                <div class="col-md-3"></div>\
                <div class="col-md-4">\
                <div name="unit_plan_img_pervie"></div>\
                </div>\
                </div>\
                <div class="form-group initImgeUnitPlan">\
                <label class="control-label col-md-3">ภาพขนาดไม่เกิน 300 KB</label>\
                <div class="col-md-6">\
                <input type="file" name="unit_plan_img_name[]" id="'+initImgeUnitPlan2+'" class="unit_plan_img_name" />'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<label class="control-label col-md-3">ขนาด</label>'+
                '<div class="col-md-2">'+
                '<input type="text" name="unit_plan_size_condo[]" class="form-control col-md-7 col-xs-12"  placeholder="ขนาด">'+
                '</div>'+
                '<label class="control-label col-md-1" >ตารางเมตร</label>'+
                '</div>'+
                '<div class="form-group">'+
                '<label class="control-label col-md-3">รายละเอียด</label>'+
                '<div class="col-md-6">'+
                '<input type="text" name="unit_plan_dis_th_condo[]" class="form-control col-md-7 col-xs-12" placeholder="กรอกรายละเอียด (จีน)">'+
                '</div>'+
                '</div>'+
                '<div class="form-group hide-for-th">'+
                '<label class="control-label col-md-3">รายละเอียด</label>'+
                '<div class="col-md-6">'+
                '<input type="text" name="unit_plan_dis_en_condo[]" class="form-control col-md-7 col-xs-12" placeholder="กรอกรายละเอียด (อังกฤษ)">'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<label class="control-label col-md-3" >ราคาเริ่มต้น</label>'+
                '<div class="col-md-2">'+
                '<input type="text" name="unit_plan_price_condo[]" class="form-control col-md-7 col-xs-12" onkeypress="return isNumber(event)" placeholder="กรอกราคาเริ่มต้น">'+
                '</div>'+
                '<label class="control-label col-md-1">ต่อยูนิต</label>'+
                '</div>'+
                '<br>'+
                '<div class="form-group">'+
                '<label class="control-label col-md-6" ></label>'+
                '<div class="col-md-2">'+
                '<button type="button" onclick="DeleteUnitPlanCondo(this)" class="btn btn-round btn-danger">- ลบ Unit Plan </button>'+
                '</div>'+
                '</div>'+
                '</div>';


                $('#unitPlanCondo').append(form);


                $("#"+initImgeUnitPlan2).fileinput({
                    uploadUrl: "upload.php",
                    dropZoneTitle : 'Unit Plan',
                    browseLabel: 'เลือกภาพ',
                    removeLabel: 'ลบ',
                    browseClass: 'btn btn-success',
                    showUpload: false,
                    showRemove: false,
                    showCaption: false,
                    maxFileCount: 1,
                    mainClass: "input-group-md",
                    xxx:'unit_plan_img_name_seo',
                    allowedFileTypes: ["image"],
                    maxFileSize :300 ,
                    msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                });



            }

            function deleteProgreesPart(element)
            {
                $(element).parent().parent().parent().remove();
            }


            function DeleteUnitPlanCondo(element)
            {
                $(element).parent().parent().parent().remove();
            }
            function removeDetailBuildCondo(element)
            {
                $(element).parent().remove();
            }
            function removeUnitNew(element)
            {
                $(element).parent().remove();
            }

            Number.prototype.padLeft = function(base,chr){
                var  len = (String(base || 10).length - String(this).length)+1;
                return len > 0? new Array(len).join(chr || '0')+this : this;
            }

            function addProgreesPart()
            {

                var datenow = new Date ;
                var   dformat = [ datenow.getFullYear(),
                (datenow.getMonth()+1).padLeft(),
                datenow.getDate().padLeft()].join('')+
                '' +
                [ datenow.getHours().padLeft(),
                datenow.getMinutes().padLeft(),
                datenow.getSeconds().padLeft()].join('');

                var valInitPorgressInitName = "progress-img-js-init-"+dformat;
                var form = '<div>' +
                '<hr style="border-top: 2px solid #eee;">'+
                '<input name="progressSection_add[]" type="hidden" value="'+dformat+'">'+
                '<div class="form-group">'+
                '<label class="control-label col-md-3"> อัพเดต % ความคืบหน้า<br>.[  0 ถึง 100  ] </label>' +
                '<br>'+
                '<div class="col-md-2">'+
                '<input type="text" name="progress_update_mount_add[]" value="" class="form-control col-md-7 col-xs-12 progress2" min="1" max="100">'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<label class="control-label col-md-3"> ระบุเดือน / ปี  </label>'+
                '<div class="col-md-2">'+
                '<input type="text" name="progress_update_date_add[]" id="progress_update_date'+dformat+'" value="" class="form-control col-md-7 col-xs-12 datepicker-progress-date">'+
                '</div>'+
                '<label class="control-label col-md-3" style=" text-align: left; "> ของภาพความคืบหน้าโครงการที่จะใส่</label>'+
                '</div>'+
                '<div id="progressSecImg_part_'+dformat+'">'+
                '<div>'+
                '<div class="form-group">'+
                '<label class="control-label col-md-3"> ระบุภาพ <br> ภาพขนาดไม่เกิน 300 KB</label>'+
                '<div class="col-md-4">'+
                '<input type="file" class="progress_img progress-img-js" id="'+valInitPorgressInitName+'" name="progress_img_part_add'+dformat+'[]" >'+
                '<div class="previewImgProgress" >'+
                '</div>'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<label class="control-label col-md-3"> คำอธิบาย  </label>'+
                '<div class="col-md-4">'+
                '<input type="text" name="progress_update_dis_th_part_add'+dformat+'[]" class="form-control col-md-7 col-xs-12" value="" placeholder="กรอกคำอธิบาย (จีน)">'+
                '</div>'+
                '</div>'+
                '<div class="form-group hide-for-th">'+
                '<label class="control-label col-md-3"> คำอธิบาย  </label>'+
                '<div class="col-md-4">'+
                '<input type="text" name="progress_update_dis_en_part_add'+dformat+'[]" class="form-control col-md-7 col-xs-12" value="" placeholder="กรอกคำอธิบาย (อังกฤษ)">'+
                '</div>'+
                '</div>'+


                '</div>' +
                '</div>' +



                '<div class="form-group">'+
                '<label class="control-label col-md-2"></label>'+
                '<div class="col-md-2 col-md-offset-1">'+
                '<button type="button" onclick="addProgreeImg('+dformat+')" class="btn btn-round btn-success "> +Add ภาพเพิ่ม</button>'+
                '</div>'+
                '</div>' +


                '<div class="form-group">'+
                '<label class="control-label col-md-5"></label>'+
                '<div class="col-md-2 col-md-offset-1">'+
                '<button type="button"  onclick="deleteProgreesPart(this)" class="btn btn-round btn-danger" > -Delete Progrees </button>'+
                '</div>'+
                '</div>'

                '</div>';

                // initProgress_date(dformat);


                $('#progressCondo').append(form);

                initProgress();

                $("#progress_update_date"+dformat).datepicker({
                    autoclose: true,
                    changeMonth: true,
                    showDropdowns: true,
                    changeYear: true,
                    //minDate:  new Date(),
                    onSelect: function(dateText) {
                        //var d = new Date($(this).datepicker("getDate"))
                        //$scope.value.posted = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate();
                    }
                });

                $("#"+valInitPorgressInitName).fileinput({
                    uploadUrl: "upload.php",
                    dropZoneTitle : 'รูป Progress',
                    browseLabel: 'เลือกภาพ Progress',
                    removeLabel: 'ลบ',
                    browseClass: 'btn btn-success',
                    showUpload: false,
                    showRemove: false,
                    showCaption: false,
                    maxFileCount: 1,
                    mainClass: "input-group-md",
                    xxx:'progress_img_seo_part_add'+dformat,
                    allowedFileTypes: ["image"]
                });

            }

            function addProgreeImg(partProgress)
            {
                var datenow = new Date ;
                var   dformat = [ datenow.getFullYear(),
                (datenow.getMonth()+1).padLeft(),
                datenow.getDate().padLeft()].join('')+
                '' +
                [ datenow.getHours().padLeft(),
                datenow.getMinutes().padLeft(),
                datenow.getSeconds().padLeft()].join('');

                var valImageProgressSubId =  "progress-sub-img-"+dformat ;
                var form = '<div><hr class="progress_line">'+
                '<div class="form-group" >'+
                '<label class="control-label col-md-3">ระบุภาพ</label>'+
                '<div class="col-md-4">'+
                '<input type="file"  class="progress_img_add" id="'+valImageProgressSubId+'" name="progress_img_part_add'+partProgress+'[]"  >'+
                '<div class="previewImgProgress" >'+
                '</div>'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<label class="control-label col-md-3">คำอธิบาย</label>'+
                '<div class="col-md-4">'+
                '<input type="text" '+'name="progress_update_dis_th_part_add'+partProgress+'[]"'+' class="form-control col-md-7 col-xs-12" placeholder="กรอกคำอธิบาย (จีน)">'+
                '</div>'+
                '</div>' +
                '<div class="form-group hide-for-th">'+
                '<label class="control-label col-md-3">คำอธิบาย</label>'+
                '<div class="col-md-4">'+
                '<input type="text" name="'+"progress_update_dis_en_part_add"+partProgress+'[]" class="form-control col-md-7 col-xs-12" placeholder="กรอกคำอธิบาย (อังกฤษ)">'+
                '</div>'+
                '</div>'+
                '<div class="form-group" >'+
                '<label class="control-label col-md-4"></label>'+
                '<div class="col-md-4 col-md-offset-1" >'+
                '<button type = "button" onclick="deleteProgreeImg(this)" class="btn btn-round btn-danger" style="margin-left: 33px;"> -Delete ภาพเพิ่ม</button>'+
                '</div>'+
                '</div >'+
                '</div>';

                $('#progressSecImg_part_'+partProgress).append(form);


                $("#"+valImageProgressSubId).fileinput({
                    uploadUrl: "upload.php",
                    dropZoneTitle : 'รูป Progress',
                    browseLabel: 'เลือกภาพ Progress',
                    removeLabel: 'ลบ',
                    browseClass: 'btn btn-success',
                    showUpload: false,
                    showRemove: false,
                    showCaption: false,
                    msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                    maxFileCount: 1,
                    mainClass: "input-group-md",
                    xxx:'progress_img_seo_part_add'+partProgress,
                    allowedFileTypes: ["image"]
                });


            }

            function deleteProgreeImg(element)
            {
                $(element).parent().parent().parent().remove();
            }

            function checkdetailCheckBox(productId) {

                if ($("#detail_checkbox" + productId).length > 0) {

                    return true;
                }

                return false;

            }

            function disabledetailCheckBox() {


                $('.product_checkboxt').each(function (index, value) {
                    // alert($(this));
                    //  alert($(this).attr('id'));
                    var attribute_checked = $(this).attr('checked');
                    var attribute_value = $(this).attr('value');
                    console.log(attribute_checked);
                    console.log(attribute_value);
                    if (attribute_checked == "undefined" || attribute_checked == null) {
                        //alert(attribute_value);
                        $("#detail_checkbox" + attribute_value).remove();

                    }

                });
            }

            function removeHouse(productId) {
                $('#' + productId).addClass('hidden');
            }
            function removeCondo(productId) {
                $('#' + productId).addClass('hidden');
            }
            function removeUnitPlanCondo()
            {
                $('#unitPlanCondo').addClass('hidden');
            }
            function removeMasterPlanCondo()
            {
                $('#unitMasterPlanCondo').addClass('hidden');
            }

            function removeProgress()
            {
                $('#progressCondo').addClass('hidden');
            }

            function removePraking()
            {
                $('#divPraking').addClass('hidden');
                $('#community-and-room-feature').addClass('hidden');
            }

            function addTagCheck(productId) {
                $('.product_checkboxt').each(function (index, value) {
                    // alert($(this));
                    //  alert($(this).attr('id'));
                    var attribute_value = $(this).attr('value');
                    if (attribute_value == productId) {
                        $(this).attr('checked', true);
                        console.log("add attriblute checked on checkbox values " + productId);
                    }

                });
            }

            function removeTagCheck(productId) {
                $('.product_checkboxt').each(function (index, value) {
                    // alert($(this));
                    //  alert($(this).attr('id'));

                    var attribute_value = $(this).attr('value');

                    if (attribute_value == productId) {
                        $(this).removeAttr('checked');
                        console.log("remove  attriblute checked on checkbox value " + productId);
                    }

                });
            }

            function removeElementHtml(objectDom) {
                $(objectDom).remove;
            }




            function checkRadioProject(value){
                if(value == 'optionsRadios-img') {
                    $("#upload-project-img").prop('required',true);
                    $("#upload-project-img").prop('disabled',false);
                    $("#upload-project-img").fileinput('enable');
                    $("#upload-project-img").show('slow');
                    $("#upload-project-vdo").prop('required',false);
                    $("#upload-project-vdo").fileinput('disable');

                    // if(!tempClickRadioDuplicate) {
                    //   $('#images_in_homesales').empty();
                    //   tempClickRadioDuplicate = 1;
                    // }
                }else{

                    //tempClickRadioDuplicate = 0;
                    $("#upload-project-vdo").prop('disabled',false);
                    $("#upload-project-vdo").prop('required',true);
                    //$("#upload-project-vdo").fileinput('enable');
                    $("#upload-project-img").prop('required',false);
                    $("#upload-project-img").prop('disabled',true);
                    $("#upload-project-img").fileinput('disable');
                    $("#upload-project-vdo").fileinput('enable');
                    $("#upload-project-img").hide('slow');

                }
            }

            function deleteVDOFileProject(id) {
                $.confirm({
                    title: 'ลบ VDO นี้ !',
                    content: 'คุณแน่ใจที่จะลบ VDO นี้!',
                    buttons : {
                        Yes: {
                            text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                                var self = this;
                                return $.ajax({
                                    url: 'ajax_deleteVDOFileProject.php?id=' + id,
                                    dataType: 'json',
                                    method: 'get'
                                }).done(function () {
                                    $.alert('Confirmed!');
                                    location.reload();
                                }).fail(function () {
                                    self.setContent('Something went wrong.');
                                });
                            }
                        },
                        No: function () {
                            //$.alert('ยกลเิก!');
                        },
                    }
                });
            }

            function deleteLeadImageProject(id){
                $.confirm({
                    title: 'ลบรูปนี้ !',
                    content: 'คุณแน่ใจที่จะลบ รูป นี้!',
                    buttons : {
                        Yes: {
                            text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                                var self = this;
                                return $.ajax({
                                    url: 'ajax_deleteLeadImageProject.php?LEAD_IMAGE_PROJECT_FILE_ID=' + id,
                                    dataType: 'json',
                                    method: 'get'
                                }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                }).fail(function () {
                                    self.setContent('Something went wrong.');
                                });
                            }
                        },
                        No: function () {
                            //$.alert('ยกลเิก!');
                        },
                    }
                });

            }

            function deleteLeadImageBanner(id){
                $.confirm({
                    title: 'ลบรูปนี้ !',
                    content: 'คุณแน่ใจที่จะลบ รูป นี้!',
                    buttons : {
                        Yes: {
                            text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                                var self = this;
                                return $.ajax({
                                    url: 'ajax_deleteLeadImageProject.php?LEAD_IMAGE_PROJECT_FILE_ID=' + id,
                                    dataType: 'json',
                                    method: 'get'
                                }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                }).fail(function () {
                                    self.setContent('Something went wrong.');
                                });
                            }
                        },
                        No: function () {
                            //$.alert('ยกลเิก!');
                        },
                    }
                });

            }

            //delete gallery
            function deleteGaleryProject(id) {
                $.confirm({
                    title: 'ลบรูปนี้ !',
                    content: 'คุณแน่ใจที่จะลบ รูป นี้!',
                    buttons : {
                        Yes: {
                            text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                                var self = this;
                                return $.ajax({
                                    url: 'ajax_deleteImageGaleryProject.php?galery_project_img_id=' + id,
                                    dataType: 'json',
                                    method: 'get'
                                }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                }).fail(function () {
                                    self.setContent('Something went wrong.');
                                });
                            }
                        },
                        No: function () {
                            //$.alert('ยกลเิก!');
                        },
                    }
                });
            }

            //delete clip
            function deleteYoutubeProject(id) {
                $.confirm({
                    title: 'ลบรูปนี้ !',
                    content: 'คุณแน่ใจที่จะลบ รูป นี้!',
                    buttons : {
                        Yes: {
                            text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                                var self = this;
                                return $.ajax({
                                    url: 'ajax_deleteYoutube_project.php?id=' + id,
                                    dataType: 'json',
                                    method: 'get'
                                }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                }).fail(function () {
                                    self.setContent('Something went wrong.');
                                });
                            }
                        },
                        No: function () {
                            //$.alert('ยกลเิก!');
                        },
                    }
                });
            }

            function My_deleteCommunity(id) {
                $.confirm({
                    title: 'ลบรายการนี้ !',
                    content: 'คุณแน่ใจที่จะรายการนี้!',
                    buttons : {
                        Yes: {
                            text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                                var self = this;
                                return $.ajax({
                                    url: 'ajax_deleteCommunity.php?id=' + id,
                                    dataType: 'json',
                                    method: 'get'
                                }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                }).fail(function () {
                                    self.setContent('Something went wrong.');
                                });
                            }
                        },
                        No: function () {
                            //$.alert('ยกลเิก!');
                        },
                    }
                });
            }

            function My_deleteMaster(id) {
                $.confirm({
                    title: 'ลบรายการนี้ !',
                    content: 'คุณแน่ใจที่จะรายการนี้!',
                    buttons : {
                        Yes: {
                            text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                                var self = this;
                                return $.ajax({
                                    url: 'ajax_deleteMaster.php?id=' + id,
                                    dataType: 'json',
                                    method: 'get'
                                }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                }).fail(function () {
                                    self.setContent('Something went wrong.');
                                });
                            }
                        },
                        No: function () {
                            //$.alert('ยกลเิก!');
                        },
                    }
                });
            }

            //delete360Project
            function delete360Project(id) {
                $.confirm({
                    title: 'ลบรายการนี้ !',
                    content: 'คุณแน่ใจที่จะรูปนี้!',
                    buttons : {
                        Yes: {
                            text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                                var self = this;
                                return $.ajax({
                                    url: 'ajax_delete360.php?id=' + id,
                                    dataType: 'json',
                                    method: 'get'
                                }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                }).fail(function () {
                                    self.setContent('Something went wrong.');
                                });
                            }
                        },
                        No: function () {
                            //$.alert('ยกลเิก!');
                        },
                    }
                });
            }

            //delete Unit paln
            function My_deleteUnit(id) {
                $.confirm({
                    title: 'ลบรายการนี้ !',
                    content: 'คุณแน่ใจที่จะรายการนี้!',
                    buttons : {
                        Yes: {
                            text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                                var self = this;
                                return $.ajax({
                                    url: 'ajax_deleteUnit.php?id=' + id,
                                    dataType: 'json',
                                    method: 'get'
                                }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                }).fail(function () {
                                    self.setContent('Something went wrong.');
                                });
                            }
                        },
                        No: function () {
                            //$.alert('ยกลเิก!');
                        },
                    }
                });
            }

            //delete Community
            function deleteImageCommunity_project(id) {
                $.confirm({
                    title: 'ลบรูปนี้ !',
                    content: 'คุณแน่ใจที่จะลบ รูป นี้!',
                    buttons : {
                        Yes: {
                            text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                                var self = this;
                                return $.ajax({
                                    url: 'ajax_deleteImageCommunity_project.php?id=' + id,
                                    dataType: 'json',
                                    method: 'get'
                                }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                }).fail(function () {
                                    self.setContent('Something went wrong.');
                                });
                            }
                        },
                        No: function () {
                            //$.alert('ยกลเิก!');
                        },
                    }
                });
            }


            var url = "ajax_getDataRefZone.php";
            function changeZone(values,id){
                if(values != 0) {

                    var link = url+'?zone_id='+values;
                    $.ajax( link )
                    .done(function(response) {
                        if (response != 0) {

                            if(response != ""){
                                if(typeof response === "object"){
                                    var json_obj = response;
                                }else{
                                        var json_obj =  $.parseJSON(response);//parse JSON
                                    }
                                }

                                //Clear Elemant <Option>
                                $('#province-name'+id).empty();
                                $('#province-name'+id).val(json_obj.result_data[0].PROVINCE_NAME);
                                $('#province_id'+id).append('<option value="' + json_obj.result_data[0].PROVINCE_ID + '" selected>' + json_obj.result_data[0].PROVINCE_NAME + '</option>');
                                $('#amphur-name'+id).empty();
                                $('#amphur-name'+id).val(json_obj.result_data[0].AMPHUR_NAME);
                                $('#district-name'+id).empty();
                                $('#district-name'+id).val(json_obj.result_data[0].DISTRICT_NAME);
                                ///
                                $('#amphur_id'+id).empty();
                                $('#district_id'+id).empty();
                                $('#amphur_id'+id).val(json_obj.result_data[0].PROVINCE_NAME);
                                var htmlOption = '<option value="">เลือกอำเภอ</option>';
                                var htmlOption2 = '<option value="">เลือกตำบล</option>';

                                for (var i = 0; i < json_obj.result_data.length; i++) {

                                    var amphur_int = parseInt($('#amphur_choose'+id).val());
                                    var text_selected = amphur_int==json_obj.result_data[i].AMPHUR_ID ? 'selected' : '';

                                    var toStr = json_obj.result_data[i].AMPHUR_NAME + ' (' + json_obj.result_data[i].AMPHUR_NAME_ENG + ')';
                                    htmlOption += '<option value="'+json_obj.result_data[i].AMPHUR_ID+'" '+text_selected+'>'+toStr+'</option>';
                                }
                                $('#amphur_id'+id).append(htmlOption);
                                $('#district_id'+id).append(htmlOption2);

                            }else{

                            }
                        })
                    .fail(function() {

                            //Clear Elemant <Option>
                            $('#province-name'+id).empty();
                            $('#amphur-name'+id).empty();
                            $('#district-name'+id).empty();
                            console.error('Backend error');
                        })
                    .always(function() {
                        console.info( "complete" );
                    });
                }else {

                    //Clear Elemant <Option>
                    $('#zone').empty();
                }
            }


            function initProgress()
            {
//                    $(".datepicker-progress-date").datetimepicker({
//                        locale: 'th',
//                        format: 'YYYY-MM-DD'
//                    });
$('.datepicker-progress-date').datepicker({
    autoclose: true,
    changeMonth: true,
    showDropdowns: true,
    changeYear: true,
                    //minDate:  new Date(),
                    onSelect: function(dateText) {
                        //var d = new Date($(this).datepicker("getDate"))
                        //$scope.value.posted = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate();
                    }
                });
}

function initProgress_date(id)
{

    $("#progress_update_date"+id).datepicker({
        autoclose: true,
        changeMonth: true,
        showDropdowns: true,
        changeYear: true,
                    //minDate:  new Date(),
                    onSelect: function(dateText) {
                        //var d = new Date($(this).datepicker("getDate"))
                        //$scope.value.posted = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate();
                    }
                });
}

function changOpenDay(value)
{
    if (value == 'all') {
        $('#close-day').addClass('hide');
    } else {
        $('#close-day').removeClass('hide');
    }
}


function getData(params) {
    $.ajax({
        type: "POST",
        url: "search_zone.php",
        data: params,
        async: false,
        cache: false
    }).success(function (data) {
        if(data.provinces){
            var provinces = data.provinces;
            $('select[name="province_id"]').empty();
            $.each(provinces,function (i,val) {
                $('select[name="province_id"]').append('<option value="'+val.province_id+'">'+val.province_name+'</option>')
            });
        }
    });
}

function changeProvince(id){
    if(id != 0) {
        var url = "ajax_getDataRefProvince.php";
        var link = url+'?province_id='+id;
        $.ajax( link )
        .done(function(response) {
            if (response != 0) {
                console.log(response)
                if(response != ""){
                    if(typeof response === "object"){
                        var json_obj = response;
                    }else{
                                        var json_obj =  $.parseJSON(response);//parse JSON
                                    }
                                }

                                //Clear Elemant <Option>
                                $('#amphur_id').empty();
                                $('#district_id').empty();
                                $('#amphur_id').val(json_obj.result_data[0].PROVINCE_NAME);
                                var htmlOption = '<option value="">เลือกอำเภอ</option>';
                                var htmlOption2 = '<option value="">เลือกตำบล</option>';

                                for (var i = 0; i < json_obj.result_data.length; i++) {

                                    var amphur_int = parseInt($('#amphur_choose').val());
                                    var text_selected = amphur_int==json_obj.result_data[i].AMPHUR_ID ? 'selected' : '';

                                    var toStr = json_obj.result_data[i].AMPHUR_NAME + ' (' + json_obj.result_data[i].AMPHUR_NAME_ENG + ')';
                                    htmlOption += '<option value="'+json_obj.result_data[i].AMPHUR_ID+'" '+text_selected+'>'+toStr+'</option>';
                                }
                                $('#amphur_id').append(htmlOption);
                                $('#district_id').append(htmlOption2);
                            }else{

                            }
                        })
        .fail(function() {

                            //Clear Elemant <Option>
                            $('#amphur_id').empty();
                            $('#district_id').empty();
                            console.error('Backend error');
                        })
        .always(function() {
            console.info( "complete" );
        });
    }else {

                    //Clear Elemant <Option>
                    $('#amphur_id').empty();
                    $('#district_id').empty();
                }
            }

            function changeAmpur(values,id){
                if(values != 0) {
                    var url = "ajax_getDataRefAmpur.php";
                    var link = url+'?ampur_id='+values;
                    $.ajax( link )
                    .done(function(response) {
                        if (response != 0) {
                            console.log(response)
                            if(response != ""){
                                if(typeof response === "object"){
                                    var json_obj = response;
                                }else{
                                        var json_obj =  $.parseJSON(response);//parse JSON
                                    }
                                }

                                //Clear Elemant <Option>
                                $('#district_id'+id).empty();
                                $('#district_id'+id).val(json_obj.result_data[0].PROVINCE_NAME);
                                var htmlOption = '<option value="">เลือกตำบล</option>';

                                for (var i = 0; i < json_obj.result_data.length; i++) {
                                    var district_int = parseInt($('#district_choose'+id).val());
                                    var text_selected = district_int==json_obj.result_data[i].DISTRICT_ID ? 'selected' : '';

                                    var toStr = json_obj.result_data[i].DISTRICT_NAME + ' (' + json_obj.result_data[i].DISTRICT_NAME_ENG +')';
                                    htmlOption += '<option value="'+json_obj.result_data[i].DISTRICT_ID+'" '+text_selected+'>'+toStr+'</option>';
                                }
                                $('#district_id'+id).append(htmlOption);
                            }else{

                            }
                        })
                    .fail(function() {

                            //Clear Elemant <Option>
                            $('#district_id'+id).empty();
                            console.error('Backend error');
                        })
                    .always(function() {
                        console.info( "complete" );
                    });
                }else {

                    //Clear Elemant <Option>
                    $('#district_id').empty();
                }
            }

        </script>

        <script>
            $(function() {
                $('#staticParent').on('keydown', '#child', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
            });
        </script>

        <script>
            $(document).ready(function(){
                $("#l1").show();
                $("#l2").hide();
            });
            $('input:radio[name="optradio"]').change(function() {
                if ($(this).val() == 'jpg') {
                    $("#l1").show();
                    $("#l2").hide();
                } else {
                    $("#l1").hide();
                    $("#l2").show();
                }
            });

        </script>

        <script>
            $(document).ready(function(){
                $("#v1").<?=$type_vdo?>
                $("#v2").<?=$type_vdo2?>
            });
            $("#youtube2").on('ifChecked', function(event){
                $("#v1").show();
                $("#v2").hide();
            });
            $("#file2").on('ifChecked', function(event){
                $("#v1").hide();
                $("#v2").show();
            });

        </script>

        <script>
            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 46 || charCode > 58)|| (charCode == 47)) {
                    return false;
                }
                return true;
            }
        </script>

        <script>
            $(document).ready(function(){
                $("#p1").show();
                $("#p2").hide();
                $("#p3").hide();
                $("#p4").hide();
                $("#p5").hide();

            });
        </script>

        <script>

            function getval(){
                $("#mySelect").val();
                if($("#mySelect").val() == "image"){
                    $("#p1").show();
                    $("#p2").hide();
                    $("#p3").hide();
                    $("#p4").hide();
                }else if($("#mySelect").val() == "banner"){
                    $("#p2").show();
                    $("#p3").hide();
                    $("#p1").hide();
                    $("#p4").hide();
                }else if($("#mySelect").val() == "youtube"){
                    $("#p3").show();
                    $("#p1").hide();
                    $("#p2").hide();
                    $("#p4").hide();
                }else if($("#mySelect").val() == "vdo"){
                    $("#p4").show();
                    $("#p1").hide();
                    $("#p2").hide();
                    $("#p3").hide();
                }
            }
        </script>

        <script>
            <?php
$sql_l2 = "SELECT * FROM LH_LEAD_IMAGE_PROJECT_FILE WHERE PROJECT_ID = '" . $_GET['project_id'] . "'";
$query_l2 = mssql_query($sql_l2);
$row_l2 = mssql_fetch_array($query_l2);
$type = $row_l2['LEAD_IMAGE_PROJECT_FILE_TYPE'];

$h1 = "hide();";
$h2 = "hide();";
$h3 = "hide();";
$h4 = "hide();";
if ($type == "image") {
	$h1 = "show();";
} else if ($type == "banner") {
	$h2 = "show();";
} else if ($type == "youtube") {
	$h3 = "show();";
} else if ($type == "vdo") {
	$h4 = "show();";
}
?>
            $(document).ready(function(){
                $("#h1").<?=$h1;?>
                $("#h2").<?=$h2;?>
                $("#h3").<?=$h3;?>
                $("#h4").<?=$h4;?>

            });
        </script>

        <script>

            function getval2(sel){
                $("#mySelect").val();
                if($("#mySelect").val() == "image"){
                    $("#h1").show();
                    $("#h2").hide();
                    $("#h3").hide();
                    $("#h4").hide();
                }else if($("#mySelect").val() == "banner"){
                    $("#h2").show();
                    $("#h3").hide();
                    $("#h1").hide();
                    $("#h4").hide();
                }else if($("#mySelect").val() == "youtube"){
                    $("#h3").show();
                    $("#h1").hide();
                    $("#h2").hide();
                    $("#h4").hide();
                }else if($("#mySelect").val() == "vdo"){
                    $("#h4").show();
                    $("#h1").hide();
                    $("#h2").hide();
                    $("#h3").hide();
                }
            }

            $('#project').click(function() {
                console.log($('#project').val());
            });


        </script>
        <script>
            function deleteMappdf(id) {
                $.confirm({
                    title: 'ลบรูปนี้ !',
                    content: 'คุณแน่ใจที่จะลบรูปนี้!',
                    buttons : {
                        Yes: {
                            text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                                var self = this;
                                return $.ajax({
                                    url: 'ajax_deleteMapPDF.php?project_id=' + id,
                                    dataType: 'json',
                                    method: 'get'
                                }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                }).fail(function () {
                                    self.setContent('Something went wrong.');
                                });
                            }
                        },
                        No: function () {
                            //$.alert('ยกลเิก!');
                        },
                    }
                });
            }
        </script>
        <script>
            function deleteLogo(id) {
                $.confirm({
                    title: 'ลบรูปนี้ !',
                    content: 'คุณแน่ใจที่จะลบรูปนี้!',
                    buttons : {
                        Yes: {
                            text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                                var self = this;
                                return $.ajax({
                                    url: 'ajax_deleteLogo.php?project_id=' + id,
                                    dataType: 'json',
                                    method: 'get'
                                }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                }).fail(function () {
                                    self.setContent('Something went wrong.');
                                });
                            }
                        },
                        No: function () {
                            //$.alert('ยกลเิก!');
                        },
                    }
                });
            }
        </script>
        <script>
            function deleteBrochure(id) {
                $.confirm({
                    title: 'ลบรูปนี้ !',
                    content: 'คุณแน่ใจที่จะลบ Brochure นี้!',
                    buttons : {
                        Yes: {
                            text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                                var self = this;
                                return $.ajax({
                                    url: 'ajax_deleteBrochure.php?project_id=' + id,
                                    dataType: 'json',
                                    method: 'get'
                                }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                }).fail(function () {
                                    self.setContent('Something went wrong.');
                                });
                            }
                        },
                        No: function () {
                            //$.alert('ยกลเิก!');
                        },
                    }
                });
            }
        </script>
        <script>
            function deleteJpgpdf(id) {
                $.confirm({
                    title: 'ลบรูปนี้ !',
                    content: 'คุณแน่ใจที่จะลบรูปนี้!',
                    buttons : {
                        Yes: {
                            text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                                var self = this;
                                return $.ajax({
                                    url: 'ajax_deleteJpgpdf.php?project_id=' + id,
                                    dataType: 'json',
                                    method: 'get'
                                }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                }).fail(function () {
                                    self.setContent('Something went wrong.');
                                });
                            }
                        },
                        No: function () {
                            //$.alert('ยกลเิก!');
                        },
                    }
                });
            }
        </script>

        <!-- Include Language file if we want to u-->
        <script>
            $(function() {
                $('#edit').froalaEditor({
                    toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'fontFamily', 'fontSize','insertLink', '|', 'specialCharacters', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', '-', '|', 'undo', 'redo', 'clearFormatting', 'selectAll', 'applyFormat', 'removeFormat', 'fullscreen'],
                    pluginsEnabled: null,
                    colorsText: [
                    '#ff00ff', '#1ABC9C', '#54ACD2', '#2C82C9', '#9365B8', '#475577', '#CCCCCC',
                    '#61BD6D', '#1ABC9C', '#54ACD2', '#2C82C9', '#9365B8', '#475577', '#CCCCCC',
                    '#41A85F', '#00A885', '#3D8EB9', '#2969B0', '#553982', '#28324E', '#000000',
                    '#F7DA64', '#FBA026', '#EB6B56', '#E25041', '#A38F84', '#EFEFEF', '#FFFFFF',
                    '#FAC51C', '#F37934', '#D14841', '#B8312F', '#7C706B', '#D1D5D8', 'REMOVE'
                    ],
                    height: 300,
                    imageUploadURL: 'uploade_highlights.php',
                    imageUploadParams: {
                        id: 'my_editor'
                    },

                    fileUploadURL: 'upload_file.php',
                    fileUploadParams: {
                        id: 'my_editor'
                    },
                    imagePaste: false,

                    imageManagerLoadURL: 'uploade_highlights.php',
                    imageManagerDeleteURL: "delete_image_highlights.php",
                    imageManagerDeleteMethod: "POST"
                })
                // Catch image removal from the editor.
                .on('froalaEditor.image.removed', function (e, editor, $img) {
                    $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_image_highlights.php",

                            // Request params.
                            data: {
                                src: $img.attr('src')
                            }
                        })
                    .done (function (data) {
                        console.log ('image was deleted'+$img.attr('src'));
                    })
                    .fail (function (err) {
                        console.log ('image delete problem: ' + JSON.stringify(err));
                    })
                })

                    // Catch image removal from the editor.
                    .on('froalaEditor.file.unlink', function (e, editor, link) {

                        $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_file.php",

                            // Request params.
                            data: {
                                src: link.getAttribute('href')
                            }
                        })
                        .done (function (data) {
                            console.log ('file was deleted');
                        })
                        .fail (function (err) {
                            console.log ('file delete problem: ' + JSON.stringify(err));
                        })
                    })
                });
            </script>

            <script type="text/javascript">

                $('#date_start').datepicker({
                    dateFormat : "dd/mm/yy",
                    autoclose: true,
                    changeMonth: true,
                    showDropdowns: true,
                    changeYear: true,
                    minDate:  new Date(),
                    onSelect: function(dateText) {
                        var d = new Date($(this).datepicker("getDate"));
                        $("input#date_end").datepicker('option', 'minDate', dateText);
                        $("input#date_end").prop('disabled', false);

                    //$("#date").datepicker('option', 'minDate', min);
                    //$scope.value.posted = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate();
                }
            });
                $('#date_end').datepicker({
                    dateFormat : "dd/mm/yy",
                    autoclose: true,
                    changeMonth: true,
                    showDropdowns: true,
                    changeYear: true,
                    minDate:  new Date(),
                    onSelect: function(dateText) {
                        var d = new Date($(this).datepicker("getDate"))
                        $("input#date_start").datepicker('option', 'maxDate', dateText);
                    //$("input#date_start").prop('disabled', false);
                }
            });
        </script>

        <!--       End Banner       -->

        <script>
            // $(document).ready(function() {
            //     $('#type_b').hide();
            //     $("#banner_main").hide();
            //     $("#banner_text").hide();
            //     $("#banner_vdo").hide();

            // });


            //Banner_check click
            $('#Banner_check').on('ifChecked', function(event){
                $('#type_b').show();
                $("#banner_main").show();
            });
            $('#Banner_check').on('ifUnchecked', function (event) {
                $('#type_b').hide();
                $("#banner_main").hide();
                //$("#aa").hide();
            });

            //type
            $("#r_lead").on('ifChecked', function(event){

                $("#banner_lead").show();
                $("#banner_text").hide();
                $("#banner_vdo").hide();
            });
            $("#r_vdo").on('ifChecked', function(event){

                $("#banner_lead").hide();
                $("#banner_text").hide();
                $("#banner_vdo").show();

            });
            $("#r_banner").on('ifChecked', function(event){

                $("#banner_lead").hide();
                $("#banner_text").show();
                $("#banner_vdo").hide();
            });

            //banner_vdo_youtube

            $('#banner_vdo_file').hide();

            $('#rdo_youtube').on('ifChecked', function(event){
              $('#banner_vdo_youtube ').show();

              $('#banner_vdo_file').hide();
          });

            $('#rdo_vdo').on('ifChecked', function(event){
              $('#banner_vdo_youtube ').hide();

              $('#banner_vdo_file').show();
          });

          //Banner_check if
          if($('#Banner_check').is(':checked')){
            $('#type_b').show();
            $("#banner_main").show();

            if($('#r_banner').is(':checked')){
                $("#banner_lead").hide();
                $("#banner_text").show();
                $("#banner_vdo").hide();
            }else if($('#r_lead').is(':checked')){
                $("#banner_lead").show();
                $("#banner_text").hide();
                $("#banner_vdo").hide();
            }else if($('#r_vdo').is(':checked')){
                $("#banner_lead").hide();
                $("#banner_text").hide();
                $("#banner_vdo").show();


                if($('#rdo_youtube').is(':checked')){
                 $('#banner_vdo_youtube').show();
                 $('#banner_vdo_file').hide();
                   //console.log("youtube");
               }else{
                $('#banner_vdo_youtube').hide();
                $('#banner_vdo_file').show();
                    //console.log("vdo");
                }
            }
        }else{
            $('#type_b').hide();
            $("#banner_main").hide();
            $("#banner_text").hide();
            $("#banner_vdo").hide();
        }

    </script>

    <script src="js/validate_file_project.js"></script>



</body>
</html>
