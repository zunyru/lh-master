﻿<?php
session_start();
include './include/dbCon_mssql.php';

$project_review_name_th = $_POST['project_review_name_th'];
$project_review_name_en = $_POST['project_review_name_en'];
$project_review_dis_th = $_POST['project_review_dis_th'];
$project_review_dis_en = $_POST['project_review_dis_en'];
$project_review_dis_content = $_POST['project_review_dis_content'];
$project_review_dis_content_en = $_POST['project_review_dis_content_en'];
$project_id = $_POST['project'];
$date_start = $_POST['date_start'];
$order_seq = $_POST["order_seq"];
$custom_url = trim($_POST['custom_url']);
$h1 = $_POST["h1"];

$day =substr($date_start, 0, 2);
$mount =substr($date_start, 3, 2);
$year =substr($date_start, 6, 4);

$date_start = $year."-".$mount."-".$day;
$time=date("Y-m-d H:i:s");

$fileupload_th = $_FILES['fileupload_th']['name'];
$title_seo_th = $_POST['landing_page_title_seo_th'];
$description = $_POST['landing_page_description_th'];
$keyword_seo = $_POST['keyword_seo'];

$dates=date("Y-m-d H:i:s");
$date = date("YmdHis");


function mssql_escape($str)
{
	if(get_magic_quotes_gpc())
	{
		$str= stripslashes($str);
	}
	return str_replace("'", "''", $str);
}

if($order_seq  != ''){	
	$sql_old_val = "SELECT COUNT(*) AS counts FROM LH_PROJECT_REVIEW WHERE project_update = '$date_start'  AND order_seq = $order_seq ";
	$query_result2 = mssql_query($sql_old_val, $db_conn);
	$count2 = mssql_fetch_row($query_result2);
	$count2[0];
	if($count2[0] >= 1){
	    //มีชื่อ series นี้อยู่แล้ว
		$_SESSION['add']='3';
		echo '<script>
		window.history.go(-1);
		</script>';
		exit();
	}
}	



$query = "SELECT COUNT(*) AS counts FROM LH_PROJECT_REVIEW WHERE project_review_name_th = N'$project_review_name_th' ";
$query_result= mssql_query($query , $db_conn);

$count = mssql_fetch_row($query_result);
$count[0];
if($count[0] >= 1){
    //มีชื่อ series นี้อยู่แล้ว
	$_SESSION['add']='2';
	echo '<script>
	window.history.go(-1);
	</script>';
	exit();
}


$numrand_img= (mt_rand());
$lead_img_file = $_FILES['project_review_img'];
   //print_r($lead_img_file);
$file_image = $lead_img_file['name'];

if (!empty($_FILES['project_review_img'])) {
	$dates_file =date("Y-m-d");
	$path_img="fileupload/images/project_review/";


	$type_img = strrchr($file_image,".");

	$newname_img = $date.$numrand_img.$type_img;
	$path_copy_img=$path_img.$newname_img;
	$seo_img = $_POST['seo_lead_img_file'][0];

		          //resize
	$images = $_FILES["project_review_img"]["tmp_name"];
	$new_images = "Thumbnails_".$newname_img;
		          //copy($_FILES["lead_image"]["tmp_name"][0],"fileupload/images/project_img/".$_FILES["lead_image"]["name"][0]);
		          $width=600; //*** Fix Width & Heigh (Autu caculate) ***//
		          $size=GetimageSize($images);
		          $height=round($width*$size[1]/$size[0]);
		          $images_orig = ImageCreateFromJPEG($images);
		          $photoX = ImagesX($images_orig);
		          $photoY = ImagesY($images_orig);
		          $images_fin = ImageCreateTrueColor($width, $height);
		          ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
		          ImageJPEG($images_fin,"fileupload/images/project_review/".$new_images,93);
		          ImageDestroy($images_orig);
		          ImageDestroy($images_fin); 

		          //seo
		          $path = "fileupload/images/landingpage/";
		          $type_th = strrchr($_FILES['fileupload_th']['name'], ".");
		          $newname_th = $date . $numrand_th . $type_th;
		          $path_seo = $path . $newname_th;

		          if (move_uploaded_file($_FILES['fileupload_th']['tmp_name'], $path_seo)) {

		          	if(move_uploaded_file($_FILES['project_review_img']['tmp_name'],$path_copy_img)){

		          		$sql6="INSERT INTO LH_PROJECT_REVIEW "
		          		."(project_review_name_th,project_review_name_en,project_review_dis_th,project_review_dis_en,project_review_dis_content,project_review_img,project_update,project_review_img_seo,project_id,project_review_dis_content_en,update_date,order_seq,title_seo,description_seo,thumnail_seo,keyword_seo,custom_url,h1) "
		          		." VALUES (N'$project_review_name_th','$project_review_name_en',N'".mssql_escape($project_review_dis_th)."','".mssql_escape($project_review_dis_en)."',N'".mssql_escape($project_review_dis_content)."',"
		          		." '".$path_img.$newname_img."','$date_start',N'$seo_img','$project_id','".mssql_escape($project_review_dis_content_en)."','$time','$order_seq',N'$title_seo_th',N'$description','$path_seo',N'$keyword_seo',N'$custom_url',N'$h1')";
		          		$query_r6= mssql_query($sql6 , $db_conn);
		          		$success_img = true;
		          		$_SESSION['add']='1';
		          		header("location:project_review_add.php");
		          	}else {
		          		$success = false;

		          	}
		          }
		      }
