<?php
session_start();
include './include/dbCon_mssql.php';
date_default_timezone_set('Asia/Bangkok');


$id = $_POST['id'];

$tvc_name   = $_POST['tvc_name'];
$tvc_detail = $_POST['tvc_detail'];
$tvc_url    = $_POST['tvc_url'];

$sql   = "SELECT COUNT(*) AS counts FROM LH_TVC WHERE tvc_name = N'" . $_POST['tvc_name'] . "' AND NOT tvc_id = '$id'";
$query = mssql_query($sql);
$nums  = mssql_fetch_array($query);

if ($nums['counts'] >= 1) {
    $_SESSION['add'] = '2';
    echo '<script>
                        window.history.go(-1);
                    </script>';
    exit();
}

function mssql_escape($str)
{
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return str_replace("'", "''", $str);
}

// $query = mssql_query($sql);

$dates = $_POST['tvc_public_date'];

$year = substr($dates, 6, 4);
$day  = substr($dates, 0, 2);
$mont = substr($dates, 3, 2);

$date = $year . "/" . $mont . "/" . $day;
$page = $_POST['optradio'];

$dates=date("Y-m-d H:i:s");

$sql = "UPDATE LH_TVC SET tvc_name = N'" . mssql_escape($tvc_name) . "',"
. "tvc_detail = N'" . mssql_escape($tvc_detail) . "',"
    . "tvc_url = '$tvc_url',"
    . "update_date = '$dates',"
    . "tvc_public_date='$date' "
    . "WHERE tvc_id ='" . $_POST['id'] . "'";
$query = mssql_query($sql);

//tvc

if ($_POST['optradio'] == 'youtube') {
    $url_youtube = $_POST['url_youtube'];

    $seo_img_youtube = $_POST['seo_img_youtube'][0];
    $numrand_img3    = (mt_rand());
    if ($_FILES["img_youtube"]["name"] != "" && $_FILES["img_youtube"]["name"] != null && $url_youtube != "" && $url_youtube != null) {
        $path_copy_img = "fileupload/images/tvc/" . $numrand_img3 . '_' . $_FILES["img_youtube"]["name"];
        if (move_uploaded_file($_FILES['img_youtube']['tmp_name'], $path_copy_img)) {

            $url_youtube = $_POST['url_youtube'];
             $sql7        = "UPDATE LH_TVC SET tvc_youtube_link = '" . $url_youtube . "',update_date = '$dates' ,tvc_thumbnail= '" . $path_copy_img . "',tvc_type = 'youtube',tvc_thumbnail_youtube_seo = '$seo_img_youtube' WHERE tvc_id = '" . $id . "'";
            $query_r7    = mssql_query($sql7, $db_conn);
            $query       = mssql_query($sql);
            if ($query_r7) {
                $_SESSION['add'] = '1';
                header("location:tvc_update.php?id=$id");
            } else {
                $_SESSION['add'] = '-2';
                echo '<script>
                                    window.history.go(-1)+"?add=-1";
                          </script>';
            }

        }
    } else if ($_FILES["img_youtube"]["name"] != "" && $_FILES["img_youtube"]["name"] != null && $url_youtube == "" && $url_youtube == null) {
        $_SESSION['add'] = '-2';
        echo '<script>
                                    window.history.go(-1)+"?add=-1";
                          </script>';
    } else if ($_FILES["img_youtube"]["name"] == "" && $_FILES["img_youtube"]["name"] == null && $url_youtube != "" && $url_youtube != null) {
        $url_youtube = $_POST['url_youtube'];
        $sql7        = "UPDATE LH_TVC SET tvc_youtube_link = '" . $url_youtube . "' ,update_date = '$dates'   WHERE tvc_id = '" . $id . "'";
        $query_r7    = mssql_query($sql7, $db_conn);
        $query       = mssql_query($sql);

        if ($query_r7) {
            $_SESSION['add'] = '1';
            header("location:tvc_update.php?id=$id");
        } else {
            $_SESSION['add'] = '-2';
            echo '<script>
                                    window.history.go(-1)+"?add=-1";
                          </script>';
        }

    } else if ($_FILES["img_youtube"]["name"] == "" && $_FILES["img_youtube"]["name"] == null && $url_youtube == "" && $url_youtube == null) {
        $sql2 = "SELECT tvc_youtube_link FROM dbo.LH_TVC WHERE tvc_id = '" . $_POST['id'] . "' ";

        $query_result2 = mssql_query($sql2, $db_conn);
        $series_vdo    = mssql_fetch_row($query_result2);
        $series_vdo[0];
        if ($series_vdo[0] != "") {
            $sql2 = "SELECT tvc_thumbnail FROM dbo.LH_TVC WHERE tvc_id = '" . $_POST['id'] . "' ";

            $query_result2 = mssql_query($sql2, $db_conn);
            $tvc_thumbnail = mssql_fetch_row($query_result2);
            $tvc_thumbnail[0];
            if ($tvc_thumbnail[0] != "") {
                $_SESSION['add'] = '1';
                header("location:tvc_update.php?id=$id");
            } else {
                $_SESSION['add'] = '-2';

                echo '<script>
                                      window.history.go(-1);
                                  </script>';
                exit();
            }

        }
    }
} else if ($_POST['optradio'] == 'vdo') {

    $sql = "UPDATE LH_TVC SET tvc_name = N'" . mssql_escape($_POST['tvc_name']) . "',"
    . "tvc_detail = N'" . mssql_escape($_POST['tvc_detail']) . "',"
        . "tvc_url ='" . $_POST['tvc_url'] . "',"
        . "update_date = '$dates' ,"
        . "tvc_public_date='" . $date . "' "
        . "WHERE tvc_id ='" . $_POST['id'] . "'";
    $query = mssql_query($sql, $dbcon);

    $seo_thumbnail_vdo = $_POST['seo_thumbnail_vdo'][0];
    $numrand_img3      = (mt_rand());
    $numrand_vdo       = (mt_rand());

    $images = $_FILES['thumbnail_vdo'];
    $vdo    = $_FILES['vdo_file'];

    $file_image = $images['name'];
    $file_vdo   = $vdo['name'];
    if ($_FILES["thumbnail_vdo"]["name"] != "" && $_FILES["thumbnail_vdo"]["name"] != null && $_FILES["vdo_file"]["name"] != "" && $_FILES["vdo_file"]["name"] != null) {

        $date     = date("Y-m-d");
        $path_img = "fileupload/images/tvc/";

        $type_img = strrchr($file_image, ".");
        $type_vdo = strrchr($file_vdo, ".");

        $newname_img = $date . $numrand_img3 . $type_img;
        $newname_vdo = $date . $numrand_vdo . "_vdo" . $type_vdo;

        $path_copy_img = $path_img . $newname_img;
        $path_copy_vdo = $path_img . $newname_vdo;

        if (move_uploaded_file($_FILES['thumbnail_vdo']['tmp_name'], $path_copy_img)) {
            if (move_uploaded_file($_FILES['vdo_file']['tmp_name'], $path_copy_vdo)) {

                $sql7     = "UPDATE LH_TVC SET tvc_youtube_link = '" . $path_copy_vdo . "',update_date = '$dates' ,tvc_thumbnail= '" . $path_copy_img . "',tvc_type = 'vdo' ,tvc_thumbnail_vdo_seo = '$seo_thumbnail_vdo' WHERE tvc_id = '" . $id . "'";
                $query_r7 = mssql_query($sql7, $db_conn);
                $query    = mssql_query($sql);

                if ($query_r7) {
                    $_SESSION['add'] = '1';
                    header("location:tvc_update.php?id=$id");
                } else {
                    $_SESSION['add'] = '-1';
                    echo '<script>
                                        window.history.go(-1)+"?add=-1";
                              </script>';
                }

            }
        }
    } else if ($_FILES["thumbnail_vdo"]["name"] == "" && $_FILES["thumbnail_vdo"]["name"] == null && $_FILES["vdo_file"]["name"] != "" && $_FILES["vdo_file"]["name"] != null) {

        $date     = date("Y-m-d");
        $path_img = "fileupload/images/tvc/";

        $type_vdo = strrchr($file_vdo, ".");

        $newname_vdo = $date . $numrand_vdo . "_vdo" . $type_vdo;

        $path_copy_vdo = $path_img . $newname_vdo;
        if (move_uploaded_file($_FILES['vdo_file']['tmp_name'], $path_copy_vdo)) {
            $sql7     = "UPDATE LH_TVC SET tvc_youtube_link = '" . $path_copy_vdo . "' ,update_date = '$dates' ,tvc_type = 'vdo' WHERE tvc_id = '" . $id . "'";
            $query_r7 = mssql_query($sql7, $db_conn);
            $query    = mssql_query($sql);

            if ($query_r7) {
                $_SESSION['add'] = '1';
                header("location:tvc_update.php?id=$id");
            } else {
                $_SESSION['add'] = '-1';
                echo '<script>
                                    window.history.go(-1)+"?add=-1";
                          </script>';
            }

        }
    } else if ($_FILES["thumbnail_vdo"]["name"] != "" && $_FILES["thumbnail_vdo"]["name"] != null && $_FILES["vdo_file"]["name"] == "" && $_FILES["vdo_file"]["name"] == null) {

        $date     = date("Y-m-d");
        $path_img = "fileupload/images/tvc/";

        $type_img = strrchr($file_image, ".");

        $newname_img = $date . $numrand_img3 . $type_img;

        $path_copy_img = $path_img . $newname_img;
        if (move_uploaded_file($_FILES['thumbnail_vdo']['tmp_name'], $path_copy_img)) {
            $sql7     = "UPDATE LH_TVC SET tvc_thumbnail= '" . $path_copy_img . "',update_date = '$dates' ,tvc_type = 'vdo' ,tvc_thumbnail_vdo_seo = '$seo_thumbnail_vdo' WHERE tvc_id = '" . $id . "'";
            $query_r7 = mssql_query($sql7, $db_conn);
            $query    = mssql_query($sql);

            if ($query_r7) {
                $_SESSION['add'] = '1';
                header("location:tvc_update.php?id=$id");
            } else {
                $_SESSION['add'] = '-1';
                echo '<script>
                                    window.history.go(-1)+"?add=-1";
                          </script>';
            }

        }
    } else if ($_FILES["thumbnail_vdo"]["name"] == "" && $_FILES["thumbnail_vdo"]["name"] == null && $_FILES["vdo_file"]["name"] == "" && $_FILES["vdo_file"]["name"] == null) {
        $sql2 = "SELECT tvc_youtube_link FROM dbo.LH_TVC WHERE tvc_id = '" . $_POST['id'] . "' ";

        $query_result2 = mssql_query($sql2, $db_conn);
        $series_vdo    = mssql_fetch_row($query_result2);
        $series_vdo[0];
        if ($series_vdo[0] != "") {
            $sql2 = "SELECT tvc_thumbnail FROM dbo.LH_TVC WHERE tvc_id = '" . $_POST['id'] . "' ";

            $query_result2 = mssql_query($sql2, $db_conn);
            $tvc_thumbnail = mssql_fetch_row($query_result2);
            $tvc_thumbnail[0];
            if ($tvc_thumbnail[0] != "") {
                $_SESSION['add'] = '1';
                header("location:tvc_update.php?id=$id");
            } else {
                $_SESSION['add'] = '-1';

                echo '<script>
                                      window.history.go(-1);
                                  </script>';
                exit();
            }

        }
    }
}
