<?php
session_start();
include './include/dbCon_mssql.php';

date_default_timezone_set('Asia/Bangkok');
$date           = date("Ymd");
$year           = date("m");
$numrand_img  = (mt_rand());
$numrand_1      = (mt_rand());
$numrand_2      = (mt_rand());
$numrand_3      = (mt_rand());
$numrand_galery = (mt_rand());

$dates=date("Y-m-d H:i:s");

function mssql_escape($str)
{
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return str_replace("'", "''", $str);
}
$id              = $_POST['id'];
$series_id       = $_POST['series'];
$plan_code       = $_POST['plan_code'];
$plan_name_th    = mssql_escape($_POST['plan_name_th']);
$plan_name_en    = $_POST['plan_name_en'];
$plan_useful     = $_POST['plan_useful'];
$plan_feature_th = $_POST['plan_feature_th'];
$plan_feature_en = $_POST['plan_feature_en'];
$plan_land_size  = $_POST['plan_land_size'];

$message1_th = mssql_escape($_POST['message1_th']);
$message1_en = mssql_escape($_POST['message1_en']);

$message2_th = mssql_escape($_POST['message2_th']);
$message2_en = mssql_escape($_POST['message2_en']);

$message3_th = mssql_escape($_POST['message3_th']);
$message3_en = mssql_escape($_POST['message3_en']);
$switch = $_POST['switch'];

$c_360 = $_POST['c_360'];
//c_360_img
//$youtube    = $_POST['youtube'];
$url_youtube = $_POST['url_youtube'];
$product_id  = $_POST['product_id'];



//เช็คชื่อซ้ำ
$sql="SELECT COUNT(*) AS counts FROM LH_PLANS
WHERE plan_name_th = '$plan_name_th'  AND plan_id !=" . $id . ";";
$query_result2= mssql_query($sql , $db_conn);
$count = mssql_fetch_array($query_result2);
$count['counts'];
if($count['counts'] >= 1){
    //มีชื่อ series นี้อยู่แล้วs
    $_SESSION['add']='0';
    echo '<script>window.history.go(-1);</script>';
    exit();
}



//แก้ไข plan
$sql = "UPDATE LH_PLANS SET series_id='$series_id',"
    . "plan_name_th = N'" . $plan_name_th . "', "
    . "plan_name_en = '" . $plan_name_en . "', "
    . "plan_useful = '" . $plan_useful . "', "
    . "plan_feature_th = N'" . $plan_feature_th . "', "
    . "plan_feature_en = '" . $plan_feature_en . "', "
    . "plan_land_size = '" . $plan_land_size . "', "
    . "update_date = '" . $dates . "', "
    . "product_id = '" . $product_id . "',"
    . "plan_code = '" . $plan_code . "' ,"
    . "views = '" . $switch . "' "
    . " WHERE plan_id ='" . $id . "'";

$query_result2 = mssql_query($sql, $db_conn);



//เพิ่มฟังก์ชัน

if ($_POST['name_f'] != '') {
    $i = 1;
    foreach ($_POST['name_f'] as $key_n => $fucn_n) {

        foreach ($_POST['fucn'] as $key => $fucn_room) {
            if ($key == $key_n) {
                if ($fucn_room != '') {
                    //echo $i;
                    $fucn_n . "ขื่อ <br>";
                    $fucn_room . " ค่า <br>";
                    $sql_n   = "SELECT count(plan_id) AS counts FROM LH_FUNCTION_PLAN_SUB WHERE plan_id = '" . $id . "'";
                    $query_n = mssql_query($sql_n, $db_conn);
                    $row_num = mssql_fetch_array($query_n);
                    $row_num['counts'];
                    if ($row_num['counts'] > 0) {
                        $sql     = "SELECT * FROM LH_FUNCTION_PLAN_SUB WHERE plan_id = '" . $id . "'";
                        $query_r = mssql_query($sql, $db_conn);
                        while ($sql_plan_f_sub = mssql_fetch_array($query_r)) {
                            // echo $sql_plan_f_sub['function_plan_plan_id']."id_plan <br>";
                            // echo $fucn_n."ค่า index <br>";
                            if ($fucn_n == $sql_plan_f_sub['function_plan_plan_id']) {
                                if ($fucn_room != $sql_plan_f_sub['function_plan_sub_plan_count_room']) {
                                    $sql4     = "UPDATE LH_FUNCTION_PLAN_SUB  SET function_plan_sub_plan_count_room = '" . $fucn_room . "' WHERE plan_id = '" . $id . "' AND function_plan_plan_id = '" . $sql_plan_f_sub['function_plan_plan_id'] . "' ";
                                    $query_r4 = mssql_query($sql4, $db_conn);
                                }
                            } else {
//เช็ค update เพิ่มใหม่
                                $sql_n   = "SELECT count(plan_id) AS counts FROM LH_FUNCTION_PLAN_SUB WHERE plan_id = '" . $id . "' AND function_plan_plan_id ='" . $fucn_n . "'";
                                $query_n = mssql_query($sql_n, $db_conn);
                                $row_num = mssql_fetch_array($query_n);
                                $row_num['counts'];
                                if ($row_num['counts'] <= 0) {
//เพิ่มเติ่ม
                                    $sql4 = "INSERT INTO LH_FUNCTION_PLAN_SUB (plan_id,function_plan_plan_id,function_plan_sub_plan_count_room) "
                                        . " VALUES ('$id','$fucn_n','$fucn_room')";
                                    $query_r4 = mssql_query($sql4, $db_conn);
                                }
                            }
                        }
                    } else {
                        ///เพิ่มใหม่ยังไม่มีเลย
                        $sql4 = "INSERT INTO LH_FUNCTION_PLAN_SUB (plan_id,function_plan_plan_id,function_plan_sub_plan_count_room) "
                            . " VALUES ('$id','$fucn_n','$fucn_room')";
                        $query_r4 = mssql_query($sql4, $db_conn);
                    }

                    //ลบข้อมูลที่ไม่ได้ใส่
                    $sql_n    = "SELECT count(plan_id) AS counts FROM LH_FUNCTION_PLAN_SUB WHERE plan_id = '" . $id . "'";
                    $query_n  = mssql_query($sql_n, $db_conn);
                    $row_num2 = mssql_fetch_array($query_n);

                    if ($i < $row_num2['counts']) {

                        $sql_n   = "SELECT * FROM LH_FUNCTION_PLAN_SUB WHERE plan_id = '" . $id . "'";
                        $query_n = mssql_query($sql_n, $db_conn);
                        while ($num = mssql_fetch_array($query_n)) {
                            if ($fucn_n == $num['function_plan_plan_id']) {
                                //ตรงกัน แสดงว่ามี
                            } else {
                                $arrayName = array('plan_id' => $num['function_plan_plan_id']);
                            }
                        }
                        //print_r($arrayName);
                        //ลบ
                        //echo $arrayName['plan_id'];
                        $sql_n   = "DELETE FROM LH_FUNCTION_PLAN_SUB WHERE plan_id = '" . $id . "' AND function_plan_plan_id = '" . $arrayName['plan_id'] . "'";
                        $query_n = mssql_query($sql_n, $db_conn);
                    }

                }

            }

        }
        $i++;}
} //

//Model 
if(isset($_FILES["model_galery"]["name"][0])) {
    if ($_FILES["model_galery"]["name"][0] != "" && $_FILES["model_galery"]["name"][0] != null) {
          
        $numSize = count($_FILES['model_galery']["name"]);
        for ($i = 0; $i < $numSize; $i++) {
             
             $numrand = mt_rand();
             $images = $_FILES["model_galery"]["tmp_name"][$i];
             $images_name = $_FILES["model_galery"]["name"][$i];
             $type_img = strrchr($images_name,".");
             //resize 
             $new_images = "Thumbnails_".$date.$numrand.$type_img;
             $width=600;
             $size=GetimageSize($images);
             $height=round($width*$size[1]/$size[0]);
             $images_orig = ImageCreateFromJPEG($images);
             $photoX = ImagesX($images_orig);
             $photoY = ImagesY($images_orig);
             $images_fin = ImageCreateTrueColor($width, $height);
             ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
             ImageJPEG($images_fin,"fileupload/images/galery_plan/".$new_images,93);
             ImageDestroy($images_orig);
             ImageDestroy($images_fin);

             $pathName_model = "fileupload/images/galery_plan/" .$date.$numrand.$type_img;
             if (move_uploaded_file($_FILES["model_galery"]["tmp_name"][$i], $pathName_model)) {
                 
                $sql = "INSERT INTO LH_MODEL_IMAGE_PLANS
                     (model_img_path, model_seo,plan_id)
                     VALUES ('" . $pathName_model . "', N'" . $_POST['upload_model_galery_seo'][$i] . "','".$id."')";
                 $result = mssql_query($sql, $GLOBALS['db_conn']);
             }   
           
             $j = $i;
              $j = $j+1;
           if(($_POST['model_galery_radio'][0] != '') && ($_POST['model_galery_radio'][0] == $j ) ){
              $pathName = $pathName_model;
              $sql_model = "UPDATE LH_PLANS SET plan_img = '".$pathName."' ,plan_seo = '".$_POST['upload_model_galery_seo'][$i]."' WHERE plan_id = '".$id."' ";
              $result = mssql_query($sql_model, $GLOBALS['db_conn']);
           }
        }
    }
} 


//seo model edit
 $numSize_seo = count($_POST['model_seo_edit']);
  for ($i = 0; $i < $numSize_seo; $i++) {
            $sql = "UPDATE LH_MODEL_IMAGE_PLANS SET
                model_seo = N'".$_POST['model_seo_edit'][$i]."'
                WHERE model_img_id = '".$_POST['model_img_id'][$i]."' ";

     $result = mssql_query($sql, $GLOBALS['db_conn']);

  }

//edit model update image
  if(isset($_FILES["model_edit"]["name"])) {
      //if ($_FILES["model_edit"]["name"][0] != "" && $_FILES["model_edit"]["name"][0] != null) {
        $numSize = count($_FILES['model_edit']["name"]);
        for ($i = 0; $i < $numSize; $i++) {
             
             $numrand = mt_rand();
             $images = $_FILES["model_edit"]["tmp_name"][$i];
             $images_name = $_FILES["model_edit"]["name"][$i];
             $type_img = strrchr($images_name,".");
             //resize 
             $new_images = "Thumbnails_".$date.$numrand.$type_img;
             $width=600;
             $size=GetimageSize($images);
             $height=round($width*$size[1]/$size[0]);
             $images_orig = ImageCreateFromJPEG($images);
             $photoX = ImagesX($images_orig);
             $photoY = ImagesY($images_orig);
             $images_fin = ImageCreateTrueColor($width, $height);
             ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
             ImageJPEG($images_fin,"fileupload/images/galery_plan/".$new_images,93);
             ImageDestroy($images_orig);
             ImageDestroy($images_fin);

               $pathName_model = "fileupload/images/galery_plan/" .$date.$numrand.$type_img;
               if (move_uploaded_file($_FILES["model_edit"]["tmp_name"][$i], $pathName_model)) {
                   
                 echo $sql = "UPDATE LH_MODEL_IMAGE_PLANS SET
                       model_img_path = '".$pathName_model."' WHERE model_img_id = '".$_POST['model_img_id'][$i]."' ";
                   $result = mssql_query($sql, $GLOBALS['db_conn']);
               }   
             
            
             if(($_POST['model_galery_radio'][0] != '') && ($_POST['model_galery_radio'][0] == $i) ){
                $pathName = $pathName_model;
                $sql_model = "UPDATE LH_PLANS SET plan_img = '".$pathName."' ,plan_seo = '".$_POST['upload_model_galery_seo'][$i]."' WHERE plan_id = '".$id."' ";
                $result = mssql_query($sql_model, $GLOBALS['db_conn']);
             }

             if($_POST['model_galery_radio'][0] == $i){
                echo $_POST['model_galery_radio'][0];

             }
        }     
      //}
  }  

  //model update  
$sql="SELECT * FROM LH_PLANS p LEFT JOIN LH_MODEL_IMAGE_PLANS m ON p.plan_id = m.plan_id WHERE p.plan_id = '".$id."'";
 $query = mssql_query($sql);
 while ($row = mssql_fetch_array($query)) {
   if($_POST['model_galery_radio'][0] == $row['model_img_id']){
      $sql_model = "UPDATE LH_PLANS SET plan_img = '".$row['model_img_path']."' WHERE plan_id = '".$id."' ";
      $result = mssql_query($sql_model, $GLOBALS['db_conn']);
   }
}      
   
//img plan

$seo_img_paln = $_POST['seo_img_paln'][0];

if ($_FILES["images_plan"]["name"][0] != "" && $_FILES["images_plan"]["name"][0] != null) {
    $sql    = "SELECT plan_img FROM LH_PLANS WHERE plan_id = '$id'";
    $q      = mssql_query($sql);
    $row    = mssql_fetch_array($q);
    $d_file = $row['plan_img'];
    unlink($d_file);

    //resize
                $images = $_FILES["images_plan"]["tmp_name"][0];
                $type_img = strrchr($_FILES["images_plan"]["name"][0],".");
                $new_images = "Thumbnails_".$numrand_img.$type_img;
                //copy($_FILES["images_plan"]["tmp_name"][0],"fileupload/images/galery_plan/".$_FILES["images_plan"]["name"][0]);
                $width=600; //*** Fix Width & Heigh (Autu caculate)
                $size=GetimageSize($images);
                $height=round($width*$size[1]/$size[0]);
                $images_orig = ImageCreateFromJPEG($images);
                $photoX = ImagesX($images_orig);
                $photoY = ImagesY($images_orig);
                $images_fin = ImageCreateTrueColor($width, $height);
                ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
                ImageJPEG($images_fin,"fileupload/images/galery_plan/".$new_images,93);
                ImageDestroy($images_orig);
                ImageDestroy($images_fin);

        $pathName     = "fileupload/images/galery_plan/" . $numrand_img .$type_img;

    if (move_uploaded_file($_FILES["images_plan"]["tmp_name"][0], $pathName)) {
        $sql           = "UPDATE LH_PLANS SET plan_img = '" . $pathName . "',plan_seo = '" . $seo_img_paln . "' WHERE plan_id ='$id'";
        $query_result2 = mssql_query($sql, $db_conn);
    }
}
//update seo paln img
if(isset($_POST['plan_img_seo_edit'])){
    $sql           = "UPDATE LH_PLANS SET plan_seo = '" . $_POST['plan_img_seo_edit'] . "' WHERE plan_id ='$id'";
    $query_result2 = mssql_query($sql, $db_conn);
}



//floor plan 1
if ($_FILES["images_plan_1"]["name"][0] != "" && $_FILES["images_plan_1"]["name"][0] != null) {
    $seo_img_plan1 = $_POST['seo_img_floor_paln1'][0];
    $numrand_img1  = (mt_rand());
    $name          = $numrand_img1 . '_' . $_FILES["images_plan_1"]["name"][0];
    $pathName1     = "fileupload/images/floor_plan_master/" . $numrand_img1 . '_' . $_FILES["images_plan_1"]["name"][0];
    if (move_uploaded_file($_FILES["images_plan_1"]["tmp_name"][0], $pathName1)) {

        $sql = "SELECT floor_plan_img_name FROM LH_GALERY_FLOOR_PLAN_IMG WHERE plan_id = '$id' AND number_floor_plan = '1' ";
        $q   = mssql_query($sql, $db_conn);
        $row = mssql_fetch_row($q);
        $row[0];
        if ($row[0] == '') {
            if (isset($row[0])) {
                $sql     = "UPDATE LH_GALERY_FLOOR_PLAN_IMG SET plan_id = '$id', floor_plan_img_name = '$pathName1', floor_plan_dis_th = N'" . mssql_escape($message1_th) . "', floor_plan_dis_en = '" . mssql_escape($message1_en) . "', floor_plan_img_seo = N'$seo_img_plan1' WHERE plan_id = '$id' AND number_floor_plan = '1' ";
                $query_r = mssql_query($sql, $db_conn);

            } else {
                $sql = "INSERT INTO LH_GALERY_FLOOR_PLAN_IMG
          (plan_id,floor_plan_img_name,floor_plan_dis_th,floor_plan_dis_en,floor_plan_img_seo,number_floor_plan)
          VALUES ('$id','$pathName1',N'" . mssql_escape($message1_th) . "','" . mssql_escape($message1_en) . "',N'$seo_img_plan1','1')";
                $query_r = mssql_query($sql, $db_conn);
            }
        } else if ($row[0] != '') {
            $sql     = "UPDATE LH_GALERY_FLOOR_PLAN_IMG SET plan_id = '$id', floor_plan_img_name = '$pathName1', floor_plan_dis_th = N'" . mssql_escape($message1_th) . "', floor_plan_dis_en = '" . mssql_escape($message1_en) . "', floor_plan_img_seo = N'$seo_img_plan1' WHERE plan_id = '$id' AND number_floor_plan = '1' ";
            $query_r = mssql_query($sql, $db_conn);
        }
    }
}
//update floor paln 1
if(isset($_POST['floorArray_edit_1'])){
    $sql     = "UPDATE LH_GALERY_FLOOR_PLAN_IMG SET  floor_plan_img_seo = N'".$_POST['floorArray_edit_1']."' WHERE plan_id = '$id' AND number_floor_plan = '1' ";
    $query_r = mssql_query($sql, $db_conn);
}


$sql1 = "SELECT floor_plan_img_name FROM LH_GALERY_FLOOR_PLAN_IMG WHERE plan_id = '$id' AND number_floor_plan = '1' ";
$q1   = mssql_query($sql1, $db_conn);
$row1 = mssql_fetch_row($q1);

if ($row1[0] != '') {
    $sql     = "UPDATE LH_GALERY_FLOOR_PLAN_IMG SET plan_id = '$id', floor_plan_dis_th = N'" . mssql_escape($message1_th) . "', floor_plan_dis_en = '" . mssql_escape($message1_en) . "' WHERE plan_id = '$id' AND number_floor_plan = '1' ";
    $query_r = mssql_query($sql, $db_conn);
}



//floor plan 2
if ($_FILES["images_plan_2"]["name"][0] != "" && $_FILES["images_plan_2"]["name"][0] != null) {
    $seo_img_plan2 = $_POST['seo_img_floor_paln2'][0];
    $numrand_img2  = (mt_rand());
    $name          = $numrand_img2 . '_' . $_FILES["images_plan_2"]["name"][0];
    $pathName2     = "fileupload/images/floor_plan_master/" . $numrand_img2 . '_' . $_FILES["images_plan_2"]["name"][0];
    if (move_uploaded_file($_FILES["images_plan_2"]["tmp_name"][0], $pathName2)) {

        $sql = "SELECT floor_plan_img_name FROM LH_GALERY_FLOOR_PLAN_IMG WHERE plan_id = '$id' AND number_floor_plan = '2'";
        $q   = mssql_query($sql, $db_conn);
        $row = mssql_fetch_row($q);
        $row[0];
        if ($row[0] == '') {
            if (isset($row[0])) {
                $sql     = "UPDATE LH_GALERY_FLOOR_PLAN_IMG SET plan_id = '$id', floor_plan_img_name = '$pathName2', floor_plan_dis_th = N'" . mssql_escape($message2_th) . "', floor_plan_dis_en = '" . mssql_escape($message2_en) . "', floor_plan_img_seo = N'$seo_img_plan2' WHERE plan_id = '$id' AND number_floor_plan = '2' ";
                $query_r = mssql_query($sql, $db_conn);
            } else {
                $sql = "INSERT INTO LH_GALERY_FLOOR_PLAN_IMG
          (plan_id,floor_plan_img_name,floor_plan_dis_th,floor_plan_dis_en,floor_plan_img_seo,number_floor_plan)
              VALUES ('$id','$pathName2',N'" . mssql_escape($message2_th) . "','" . mssql_escape($message2_en) . "',N'$seo_img_plan2','2')";
                $query_r = mssql_query($sql, $db_conn);
            }
        } else if ($row[0] != '') {
            $sql     = "UPDATE LH_GALERY_FLOOR_PLAN_IMG SET plan_id = '$id', floor_plan_img_name = '$pathName2', floor_plan_dis_th = N'" . mssql_escape($message2_th) . "', floor_plan_dis_en = '" . mssql_escape($message2_en) . "', floor_plan_img_seo = N'$seo_img_plan2' WHERE plan_id = '$id' AND number_floor_plan = '2' ";
            $query_r = mssql_query($sql, $db_conn);
        }
    }
}
//update floor paln 2
if(isset($_POST['floorArray_edit_2'])){
    $sql     = "UPDATE LH_GALERY_FLOOR_PLAN_IMG SET  floor_plan_img_seo = N'".$_POST['floorArray_edit_2']."' WHERE plan_id = '$id' AND number_floor_plan = '2' ";
    $query_r = mssql_query($sql, $db_conn);
}

$sql2 = "SELECT floor_plan_img_name FROM LH_GALERY_FLOOR_PLAN_IMG WHERE plan_id = '$id' AND number_floor_plan = '2' ";
$q2  = mssql_query($sql2, $db_conn);
$row2 = mssql_fetch_row($q2);

if ($row2[0] != '') {
    $sql     = "UPDATE LH_GALERY_FLOOR_PLAN_IMG SET plan_id = '$id', floor_plan_dis_th = N'" . mssql_escape($message2_th) . "', floor_plan_dis_en = '" . mssql_escape($message2_en) . "' WHERE plan_id = '$id' AND number_floor_plan = '2' ";
    $query_r = mssql_query($sql, $db_conn);
}


//floor plan 3
if ($_FILES["images_plan_3"]["name"][0] != "" && $_FILES["images_plan_3"]["name"][0] != null) {
    $seo_img_plan3 = $_POST['seo_img_floor_paln3'][0];
    $numrand_img3  = (mt_rand());
    $name          = $numrand_img3 . '_' . $_FILES["images_plan_3"]["name"][0];
    $pathName3     = "fileupload/images/floor_plan_master/" . $numrand_img3 . '_' . $_FILES["images_plan_3"]["name"][0];
    if (move_uploaded_file($_FILES["images_plan_3"]["tmp_name"][0], $pathName3)) {

        $sql = "SELECT floor_plan_img_name FROM LH_GALERY_FLOOR_PLAN_IMG WHERE plan_id = '$id' AND number_floor_plan = '3'";
        $q   = mssql_query($sql, $db_conn);
        $row = mssql_fetch_row($q);
        $row[0];

        if ($row[0] == '') {
            if (isset($row[0])) {
                $sql     = "UPDATE LH_GALERY_FLOOR_PLAN_IMG SET plan_id = '$id', floor_plan_img_name = '$pathName3', floor_plan_dis_th = N'" . mssql_escape($message3_th) . "', floor_plan_dis_en = '" . mssql_escape($message3_en) . "', floor_plan_img_seo = N'$seo_img_plan3' WHERE plan_id = '$id' AND number_floor_plan = '3' ";
                $query_r = mssql_query($sql, $db_conn);
            } else {
                $sql = "INSERT INTO LH_GALERY_FLOOR_PLAN_IMG
            (plan_id,floor_plan_img_name,floor_plan_dis_th,floor_plan_dis_en,floor_plan_img_seo,number_floor_plan)
            VALUES ('$id','$pathName3',N'" . mssql_escape($message3_th) . "','" . mssql_escape($message3_en) . "',N'$seo_img_plan3','3')";
                $query_r = mssql_query($sql, $db_conn);
            }
        } else if ($row[0] != '') {
            $sql     = "UPDATE LH_GALERY_FLOOR_PLAN_IMG SET plan_id = '$id', floor_plan_img_name = '$pathName3', floor_plan_dis_th = N'" . mssql_escape($message3_th) . "', floor_plan_dis_en = '" . mssql_escape($message3_en) . "', floor_plan_img_seo = N'$seo_img_plan3' WHERE plan_id = '$id' AND number_floor_plan = '3' ";
            $query_r = mssql_query($sql, $db_conn);
        }
    }
}
//update floor paln 3
if(isset($_POST['floorArray_edit_3'])){
    $sql     = "UPDATE LH_GALERY_FLOOR_PLAN_IMG SET  floor_plan_img_seo = N'".$_POST['floorArray_edit_3']."' WHERE plan_id = '$id' AND number_floor_plan = '3' ";
    $query_r = mssql_query($sql, $db_conn);
}

$sql3 = "SELECT floor_plan_img_name FROM LH_GALERY_FLOOR_PLAN_IMG WHERE plan_id = '$id' AND number_floor_plan = '3' ";
$q3  = mssql_query($sql3, $db_conn);
$row3 = mssql_fetch_row($q3);

if ($row3[0] != '') {
    $sql     = "UPDATE LH_GALERY_FLOOR_PLAN_IMG SET plan_id = '$id', floor_plan_dis_th = N'" . mssql_escape($message3_th) . "', floor_plan_dis_en = '" . mssql_escape($message3_en) . "' WHERE plan_id = '$id' AND number_floor_plan = '3' ";
    $query_r = mssql_query($sql, $db_conn);
}



//360

if ($_FILES["c_360_img"]["name"][0] != "" && $_FILES["c_360_img"]["name"][0] != null && $c_360 != "" && $c_360 != null) {
    $c_360            = $_POST['c_360'];
    $numrand_img3     = (mt_rand());

    //resize
                $images = $_FILES["c_360_img"]["tmp_name"][0];
                $type_img = strrchr($_FILES["c_360_img"]["name"][0],".");
                $new_images = "Thumbnails_".$numrand_img3.$type_img;
                //copy($_FILES["images_plan"]["tmp_name"][0],"fileupload/images/galery_plan/".$_FILES["images_plan"]["name"][0]);
                $width=600; //*** Fix Width & Heigh (Autu caculate) ***//
                $size=GetimageSize($images);
                $height=round($width*$size[1]/$size[0]);
                $images_orig = ImageCreateFromJPEG($images);
                $photoX = ImagesX($images_orig);
                $photoY = ImagesY($images_orig);
                $images_fin = ImageCreateTrueColor($width, $height);
                ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
                ImageJPEG($images_fin,"fileupload/images/360_thumnail/".$new_images,93);
                ImageDestroy($images_orig);
                ImageDestroy($images_fin);

    $path_copy_img360 = "fileupload/images/360_thumnail/" . $numrand_img3 . $type_img;
    if (move_uploaded_file($_FILES['c_360_img']['tmp_name'][0], $path_copy_img360)) {
        $sql = "SELECT c360_img FROM LH_360_FOR_PLAN WHERE plan_id = '$id' ";
        $q   = mssql_query($sql, $db_conn);
        $row = mssql_fetch_row($q);
        $row[0];
        if (isset($row[0])) {
            $sql7     = "UPDATE LH_360_FOR_PLAN SET c360_plan_url = '" . $c_360 . "',c360_img= '" . $path_copy_img360 . "' WHERE plan_id = '" . $id . "'";
            $query_r7 = mssql_query($sql7, $db_conn);
        } else {
            $sql7 = "INSERT INTO LH_360_FOR_PLAN (plan_id,c360_plan_name,c360_plan_url,c360_img) "
                . " VALUES ('$id','$plan_name_th','$c_360','$path_copy_img360')";
            $query_r7 = mssql_query($sql7, $db_conn);
        }
    }
} else if ($_FILES["c_360_img"]["name"][0] == "" && $_FILES["c_360_img"]["name"][0] == null && $c_360 != "" && $c_360 != null) {
    $sql = "SELECT c360_img FROM LH_360_FOR_PLAN WHERE plan_id = '$id' ";
    $q   = mssql_query($sql, $db_conn);
    $row = mssql_fetch_row($q);
    $row[0];
    if ($row[0] != '') {
        $sql7     = "UPDATE LH_360_FOR_PLAN SET c360_plan_url = '" . $c_360 . "' WHERE plan_id = '" . $id . "'";
        $query_r7 = mssql_query($sql7, $db_conn);
    } else {
        $_SESSION['add'] = '-5';

        echo '<script>
                        window.history.go(-1);
                    </script>';
        exit();
    }
} else if ($_FILES["c_360_img"]["name"][0] != "" && $_FILES["c_360_img"]["name"][0] != null && $c_360 == "" && $c_360 == null) {
    $c_360            = $_POST['c_360'];
    $numrand_img3     = (mt_rand());

    //resize
                $images = $_FILES["c_360_img"]["tmp_name"][0];
                $type_img = strrchr($_FILES["c_360_img"]["name"][0],".");
                $new_images = "Thumbnails_".$numrand_img3.$type_img;
                //copy($_FILES["images_plan"]["tmp_name"][0],"fileupload/images/galery_plan/".$_FILES["images_plan"]["name"][0]);
                $width=600; //*** Fix Width & Heigh (Autu caculate) ***//
                $size=GetimageSize($images);
                $height=round($width*$size[1]/$size[0]);
                $images_orig = ImageCreateFromJPEG($images);
                $photoX = ImagesX($images_orig);
                $photoY = ImagesY($images_orig);
                $images_fin = ImageCreateTrueColor($width, $height);
                ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
                ImageJPEG($images_fin,"fileupload/images/360_thumnail/".$new_images,93);
                ImageDestroy($images_orig);
                ImageDestroy($images_fin);

    $path_copy_img360 = "fileupload/images/360_thumnail/" . $numrand_img3 .$type_img;
    if (move_uploaded_file($_FILES['c_360_img']['tmp_name'][0], $path_copy_img360)) {
        $sql = "SELECT c360_plan_url FROM LH_360_FOR_PLAN WHERE plan_id = '$id' ";
        $q   = mssql_query($sql, $db_conn);
        $row = mssql_fetch_row($q);
        $row[0];
        if ($row[0] != '') {
            $sql7     = "UPDATE LH_360_FOR_PLAN SET c360_img = '" . $path_copy_img360 . "' WHERE plan_id = '" . $id . "'";
            $query_r7 = mssql_query($sql7, $db_conn);
        } else {
            $_SESSION['add'] = '-5';

            echo '<script>
                        window.history.go(-1);
                    </script>';
            exit();
        }
    }
} else if ($_FILES["c_360_img"]["name"][0] == "" && $_FILES["c_360_img"]["name"][0] == null && $c_360 == "" && $c_360 == null) {
    $sql = "SELECT c360_img FROM LH_360_FOR_PLAN WHERE plan_id = '$id' ";
    $q   = mssql_query($sql, $db_conn);
    $row = mssql_fetch_row($q);
    $row[0];
    if ($row[0] != '') {
        if ($c_360 == "" && $c_360 == null) {
            $_SESSION['add'] = '-5';

            echo '<script>
                        window.history.go(-1);
                    </script>';
            exit();
        }
        $sql = "SELECT c360_plan_url FROM LH_360_FOR_PLAN WHERE plan_id = '$id' ";
        $q   = mssql_query($sql, $db_conn);
        $row = mssql_fetch_row($q);
        $row[0];
        if ($row[0] != '') {
            $_SESSION['add'] = '1';
            header("location:homemodel_update.php?id=$id");
        } else {
            $_SESSION['add'] = '-5';

            echo '<script>
                        window.history.go(-1);
                    </script>';
            exit();
        }
    } else {
        if ($c_360 == "" && $c_360 == null) {
            $sql7     = "UPDATE LH_360_FOR_PLAN SET c360_plan_url = '" . $c_360 . "' WHERE plan_id = '" . $id . "'";
            $query_r7 = mssql_query($sql7, $db_conn);
        }
        $sql = "SELECT c360_plan_url FROM LH_360_FOR_PLAN WHERE plan_id = '$id' ";
        $q   = mssql_query($sql, $db_conn);
        $row = mssql_fetch_row($q);
        $row[0];
        if ($row[0] != '') {
            $_SESSION['add'] = '-5';

            echo '<script>
                        window.history.go(-1);
                    </script>';
            exit();
        }
    }
}


//tvc

if ($_POST['optradio_vdo'] == 'youtube') {
    $numrand_img3 = (mt_rand());

    if ($_FILES["img_youtube"]["name"] != "" && $_FILES["img_youtube"]["name"] != null && $url_youtube != "" && $url_youtube != null) {
        $path_copy_img = "fileupload/images/tvc_plan/" . $numrand_img3 . '_' . $_FILES["img_youtube"]["name"];
        if (move_uploaded_file($_FILES['img_youtube']['tmp_name'], $path_copy_img)) {

            $sql = "SELECT thumnail FROM LH_CLIP_FOR_PLAN WHERE plan_id = '$id' ";
            $q   = mssql_query($sql, $db_conn);
            $row = mssql_fetch_row($q);
            $row[0];

            if (isset($row[0])) {
                $url_youtube = $_POST['url_youtube'];
                $sql7        = "UPDATE LH_CLIP_FOR_PLAN SET clip_plan_url = '" . $url_youtube . "',thumnail= '" . $path_copy_img . "',clip_type = 'youtube' WHERE plan_id = '" . $id . "'";
                $query_r7    = mssql_query($sql7, $db_conn);
            } else {
                $sql = "INSERT INTO LH_CLIP_FOR_PLAN (plan_id,clip_plan_name,clip_plan_url,thumnail,clip_type) "
                    . " VALUES ('$id','$plan_name_th','" . $url_youtube . "','$path_copy_img','youtube')";
                $query = mssql_query($sql);
            }
        }
    } else if ($_FILES["img_youtube"]["name"] != "" && $_FILES["img_youtube"]["name"] != null && $url_youtube == "" && $url_youtube == null) {
        $sql = "SELECT thumnail FROM LH_CLIP_FOR_PLAN WHERE plan_id = '$id' ";
        $q   = mssql_query($sql, $db_conn);
        $row = mssql_fetch_row($q);
        $row[0];
        if ($row[0]) {

        }
        $_SESSION['add'] = '-3';

        echo '<script>
                        window.history.go(-1);
                    </script>';
        exit();
    } else if ($_FILES["img_youtube"]["name"] == "" && $_FILES["img_youtube"]["name"] == null && $url_youtube != "" && $url_youtube != null) {

        $sql = "SELECT clip_type FROM LH_CLIP_FOR_PLAN WHERE plan_id = '$id' ";
        $q   = mssql_query($sql, $db_conn);
        $row = mssql_fetch_row($q);
        $row[0];
        if ($row[0] == 'vdo') {
            if ($_FILES["img_youtube"]["name"] == "" && $_FILES["img_youtube"]["name"] == null) {
                $_SESSION['add'] = '-3';
                echo '<script>
                        window.history.go(-1);
                    </script>';
                exit();
            }
        }

        $sql = "SELECT thumnail FROM LH_CLIP_FOR_PLAN WHERE plan_id = '$id' ";
        $q   = mssql_query($sql, $db_conn);
        $row = mssql_fetch_row($q);
        $row[0];

        if ($row[0] != '') {
            $url_youtube = $_POST['url_youtube'];
            $sql         = "UPDATE LH_CLIP_FOR_PLAN SET clip_plan_url = '" . $url_youtube . "',clip_type ='youtube' WHERE plan_id ='$id'";
            $query       = mssql_query($sql);
        } else {
            $_SESSION['add'] = '-3';

            echo '<script>
                        window.history.go(-1);
                    </script>';
            exit();
        }
    } else if ($_FILES["img_youtube"]["name"] == "" && $_FILES["img_youtube"]["name"] == null && $url_youtube == "" && $url_youtube == null) {

        $sql      = "SELECT clip_type FROM LH_CLIP_FOR_PLAN WHERE plan_id = '$id' ";
        $q        = mssql_query($sql, $db_conn);
        $type_chk = mssql_fetch_row($q);
        $type_chk[0];
        if ($type_chk[0] == 'vdo') {
            $sql7     = "UPDATE LH_CLIP_FOR_PLAN SET clip_plan_url = '',thumnail= '',clip_type = 'youtube' WHERE plan_id = '$id'";
            $query_r7 = mssql_query($sql7, $db_conn);
            $query    = mssql_query($sql);


        } else if ($type_chk[0] != 'vdo') {
            $sql = "SELECT thumnail FROM LH_CLIP_FOR_PLAN WHERE plan_id = '$id' ";
            $q   = mssql_query($sql, $db_conn);
            $row = mssql_fetch_row($q);
            $row[0];
            if ($row[0] != '') {
                if ($url_youtube == "" && $url_youtube == null) {
                    $_SESSION['add'] = '-3';
                    echo '<script>
                        window.history.go(-1);
                    </script>';
                    exit();
                }
                $sql = "SELECT clip_plan_url FROM LH_CLIP_FOR_PLAN WHERE plan_id = '$id' ";
                $q   = mssql_query($sql, $db_conn);
                $row = mssql_fetch_row($q);
                $row[0];
                if ($row[0] == '') {
                    $_SESSION['add'] = '-3';

                    echo '<script>
                        window.history.go(-1);
                    </script>';
                    exit();

                }
            } else {
                if ($url_youtube == "" && $url_youtube == null) {
                    $sql7     = "UPDATE LH_CLIP_FOR_PLAN SET clip_plan_url = '" . $url_youtube . "', clip_type = 'youtube' WHERE plan_id = '" . $id . "'";
                    $query_r7 = mssql_query($sql7, $db_conn);
                }
                $sql = "SELECT clip_plan_url FROM LH_CLIP_FOR_PLAN WHERE plan_id = '$id' ";
                $q   = mssql_query($sql, $db_conn);
                $row = mssql_fetch_row($q);
                $row[0];
                if ($row[0] != '') {
                    $_SESSION['add'] = '-3';

                    echo '<script>
                        window.history.go(-1);
                    </script>';
                    exit();
                } else {
                    $path_copy_img = "fileupload/images/tvc_plan/" . $numrand_img3 . '_' . $_FILES["img_youtube"]["name"];
                    if (move_uploaded_file($_FILES['img_youtube']['tmp_name'], $path_copy_img)) {
                        $sql7     = "UPDATE LH_CLIP_FOR_PLAN SET clip_plan_url = '" . $url_youtube . "',thumnail= '" . $path_copy_img . "',clip_type = 'youtube' WHERE plan_id = '" . $id . "'";
                        $query_r7 = mssql_query($sql7, $db_conn);
                    }

                }
            }
        }
    }
} else if ($_POST['optradio_vdo'] == 'vdo') {

    $numrand = (mt_rand());

    $images = $_FILES['thumbnail_vdo'];
    $vdo    = $_FILES['vdo_file'];

    $file_image = $images['name'];
    $file_vdo   = $vdo['name'];

    if ($_FILES["thumbnail_vdo"]["name"] != "" && $_FILES["thumbnail_vdo"]["name"] != null && $_FILES['vdo_file']['name'] != "" && $_FILES['vdo_file']['name'] != null) {

        $dates_file = date("Y-m-d");
        $path_img   = "fileupload/images/tvc_plan/";

        $type_img = strrchr($file_image, ".");
        $type_vdo = strrchr($file_vdo, ".");

        $newname_img = $date . $numrand . $type_img;
        $newname_vdo = $date . $numrand . "_vdo" . $type_vdo;

        $path_copy_img = $path_img . $newname_img;
        $path_copy_vdo = $path_img . $newname_vdo;

        if (move_uploaded_file($_FILES['thumbnail_vdo']['tmp_name'], $path_copy_img)) {
            if (move_uploaded_file($_FILES['vdo_file']['tmp_name'], $path_copy_vdo)) {

                $sql = "SELECT thumnail FROM LH_CLIP_FOR_PLAN WHERE plan_id = '$id' ";
                $q   = mssql_query($sql, $db_conn);
                $row = mssql_fetch_row($q);
                $row[0];

                if (isset($row[0])) {
                    $sql = "UPDATE LH_CLIP_FOR_PLAN SET clip_plan_url = '$path_copy_vdo', "
                        . "thumnail = '$path_copy_img',"
                        . "clip_type = 'vdo'"
                        . " WHERE plan_id = '$id'";
                    $query = mssql_query($sql, $db_conn);
                } else {
                    $sql = "INSERT INTO LH_CLIP_FOR_PLAN (plan_id,clip_plan_name,clip_plan_url,thumnail,clip_type) "
                        . " VALUES ('$id','$plan_name_th','" . $path_copy_vdo . "','$path_copy_img','vdo')";
                    $query = mssql_query($sql);
                }
            }
        }
    } else if ($_FILES["thumbnail_vdo"]["name"] != "" && $_FILES["thumbnail_vdo"]["name"] != null && $_FILES['vdo_file']['name'] == "" && $_FILES['vdo_file']['name'] == null) {
        $dates_file = date("Y-m-d");
        $path_img   = "fileupload/images/tvc_plan/";

        $type_img = strrchr($file_image, ".");

        $newname_img = $date . $numrand . $type_img;

        $path_copy_img = $path_img . $newname_img;

        if (move_uploaded_file($_FILES['thumbnail_vdo']['tmp_name'], $path_copy_img)) {
            $sql = "SELECT clip_type FROM LH_CLIP_FOR_PLAN WHERE plan_id = '$id' ";
            $q   = mssql_query($sql, $db_conn);
            $row = mssql_fetch_row($q);
            $row[0];
            if ($row[0] == 'youtube') {
                if ($_FILES['vdo_file']['name'] == "" && $_FILES['vdo_file']['name'] == null) {
                    $_SESSION['add'] = '-4';
                    echo '<script>
                        window.history.go(-1);
                    </script>';
                    exit();
                }
            }

            $sql = "SELECT clip_plan_url FROM LH_CLIP_FOR_PLAN WHERE plan_id = '$id' ";
            $q   = mssql_query($sql, $db_conn);
            $row = mssql_fetch_row($q);
            $row[0];
            if ($row[0] != '') {
                $sql = "UPDATE LH_CLIP_FOR_PLAN SET thumnail = '$path_copy_img',"
                    . "clip_type = 'vdo'"
                    . " WHERE plan_id = '$id'";
                $query = mssql_query($sql, $db_conn);
            } else {
                $_SESSION['add'] = '-4';

                echo '<script>
                        window.history.go(-1);
                    </script>';
                exit();
            }
        }
    } else if ($_FILES["thumbnail_vdo"]["name"] == "" && $_FILES["thumbnail_vdo"]["name"] == null && $_FILES['vdo_file']['name'] != "" && $_FILES['vdo_file']['name'] != null) {
        $dates_file = date("Y-m-d");
        $path_img   = "fileupload/images/tvc_plan/";

        $type_vdo = strrchr($file_vdo, ".");

        $newname_vdo = $date . $numrand . "_vdo" . $type_vdo;

        $path_copy_vdo = $path_img . $newname_vdo;

        if (move_uploaded_file($_FILES['vdo_file']['tmp_name'], $path_copy_vdo)) {

            $sql = "SELECT clip_type FROM LH_CLIP_FOR_PLAN WHERE plan_id = '$id' ";
            $q   = mssql_query($sql, $db_conn);
            $row = mssql_fetch_row($q);
            $row[0];
            if ($row[0] == 'youtube') {
                if ($_FILES["thumbnail_vdo"]["name"] == "" && $_FILES["thumbnail_vdo"]["name"] == null) {
                    $_SESSION['add'] = '-4';

                    echo '<script>
                        window.history.go(-1);
                    </script>';
                    exit();
                }
            }

            $sql = "SELECT thumnail FROM LH_CLIP_FOR_PLAN WHERE plan_id = '$id' ";
            $q   = mssql_query($sql, $db_conn);
            $row = mssql_fetch_row($q);
            $row[0];
            if ($row[0] != '') {
                $sql = "UPDATE LH_CLIP_FOR_PLAN SET clip_plan_url = '$path_copy_vdo',"
                    . "clip_type = 'vdo'"
                    . " WHERE plan_id = '$id'";
                $query = mssql_query($sql, $db_conn);
            } else {
                $_SESSION['add'] = '-4';

                echo '<script>
                        window.history.go(-1);
                    </script>';
                exit();
            }
        }
    } else if ($_FILES["thumbnail_vdo"]["name"] == "" && $_FILES["thumbnail_vdo"]["name"] == null && $_FILES['vdo_file']['name'] == "" && $_FILES['vdo_file']['name'] == null) {

        $sql      = "SELECT clip_type FROM LH_CLIP_FOR_PLAN WHERE plan_id = '$id' ";
        $q        = mssql_query($sql, $db_conn);
        $type_chk = mssql_fetch_row($q);
        $type_chk[0];
        if ($type_chk[0] == 'youtube') {
            $sql7     = "UPDATE LH_CLIP_FOR_PLAN SET clip_plan_url = '',thumnail= '',clip_type = 'vdo' WHERE plan_id = '$id'";
            $query_r7 = mssql_query($sql7, $db_conn);
            $query    = mssql_query($sql);

        } else if ($type_chk[0] != 'youtube') {
            $sql = "SELECT thumnail FROM LH_CLIP_FOR_PLAN WHERE plan_id = '$id' ";
            $q   = mssql_query($sql, $db_conn);
            $row = mssql_fetch_row($q);
            $row[0];
            if ($row[0] != '') {
                $sql = "SELECT clip_plan_url FROM LH_CLIP_FOR_PLAN WHERE plan_id = '$id' ";
                $q   = mssql_query($sql, $db_conn);
                $row = mssql_fetch_row($q);
                $row[0];
                if ($row[0] == '') {
                    $_SESSION['add'] = '-4';

                    echo '<script>
                        window.history.go(-1);
                    </script>';
                    exit();
                }
            } else {
                $sql = "SELECT clip_plan_url FROM LH_CLIP_FOR_PLAN WHERE plan_id = '$id' ";
                $q   = mssql_query($sql, $db_conn);
                $row = mssql_fetch_row($q);
                $row[0];
                if ($row[0] != '') {
                    $_SESSION['add'] = '-4';

                    echo '<script>
                        window.history.go(-1);
                    </script>';
                    exit();
                }
            }
        }
    }
}




//galery
$dataName = (mt_rand());
//update
$numSize = count($_FILES['project_galery_edit']["name"]);
    if($numSize > 0) {
        for ($i = 0; $i < $numSize; $i++) {
            if ($_FILES['project_galery_edit']["name"][$i] != '') {

                $images = $_FILES["project_galery_edit"]["tmp_name"][$i];
                $type_img = strrchr($_FILES["project_galery_edit"]["name"][$i],".");
                $new_images = "Thumbnails_".$numrand_img.$dataName.$i.$type_img;
                //copy($_FILES["images_plan"]["tmp_name"][0],"fileupload/images/galery_plan/".$_FILES["images_plan"]["name"][0]);
                $width=600; //*** Fix Width & Heigh (Autu caculate) ***//
                $size=GetimageSize($images);
                $height=round($width*$size[1]/$size[0]);
                $images_orig = ImageCreateFromJPEG($images);
                $photoX = ImagesX($images_orig);
                $photoY = ImagesY($images_orig);
                $images_fin = ImageCreateTrueColor($width, $height);
                ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
                ImageJPEG($images_fin,"fileupload/images/galery_plan/".$new_images,93);
                ImageDestroy($images_orig);
                ImageDestroy($images_fin);

                $pathName = "fileupload/images/galery_plan/".$numrand_img.$dataName.$i.$type_img;
                if(move_uploaded_file($_FILES["project_galery_edit"]["tmp_name"][$i], $pathName)) {

                    $sql_update_gallery = "UPDATE LH_GALERY_PLAN SET galery_plan_img_seo = N'" . $_POST['galery_plan_img_seo_edit'][$i] . "' ,
                galery_plan_name_dis = N'" . $_POST['project_galery_text_edit'][$i] . "',
                galery_plan_name = '" .$pathName."',
                galery_plan_name_dis_en = '" . $_POST['project_galery_text_en_edit'][$i] . "'
                WHERE galery_plan_id = '" . $_POST['id_gallery'][$i] . "'";
                    $result_gallery_update = mssql_query($sql_update_gallery, $GLOBALS['db_conn']);
                }
            }
        }
    }


//add
if(isset($_FILES["project_galery"]["name"][0])) {
    if ($_FILES["project_galery"]["name"][0] != "" && $_FILES["project_galery"]["name"][0] != null){
        $date = date('Y-m-d');
        $numSize = count($_FILES['project_galery']["name"]);
        for ($i=0; $i < $numSize; $i++) {

             //resize
                $numrand_img = (mt_rand());
                $images = $_FILES["project_galery"]["tmp_name"][$i];
                $images2 = $_FILES["project_galery"]["name"][$i];
                $date = date('dmY');
                $date2 = date('Y-m-d');
                $type_img = strrchr($images2,".");
                $new_images = "Thumbnails_".$numrand_img.$date.$type_img;
               
                $width=600;
                $size=GetimageSize($images);
                $height=round($width*$size[1]/$size[0]);
                $images_orig = ImageCreateFromJPEG($images);
                $photoX = ImagesX($images_orig);
                $photoY = ImagesY($images_orig);
                $images_fin = ImageCreateTrueColor($width, $height);
                ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
                ImageJPEG($images_fin,"fileupload/images/galery_plan/".$new_images,93);
                ImageDestroy($images_orig);
                ImageDestroy($images_fin);

            $pathName = "fileupload/images/galery_plan/".$numrand_img.$dataName.$type_img;
            $galery_seo = $_POST['upload_project_galery_seo'][$i];
            $galery_dis = $_POST['project_galery_text'][$i];
            $galery_dis_en = $_POST['project_galery_text_en'][$i];
            if(move_uploaded_file($_FILES["project_galery"]["tmp_name"][$i], $pathName)) {

                $sql = "INSERT INTO LH_GALERY_PLAN (plan_id,galery_plan_name,galery_plan_date,galery_plan_img_seo,galery_plan_name_dis,galery_plan_type,galery_plan_name_dis_en)
     VALUES ('$id','$pathName','$date2',N'$galery_seo',N'$galery_dis','home_plan','$galery_dis_en')";
                $result = mssql_query($sql, $GLOBALS['db_conn']);
            }
        }
    }
}
if(isset($_POST['project_galery_text_edit']) || isset($_POST['galery_plan_img_seo_edit']) || isset($_POST['project_galery_text_en_edit'])){
          $sql_gallery = "SELECT * FROM LH_GALERY_PLAN WHERE plan_id = '" . $_POST['id'] . "'";
            $result_gallery = mssql_query($sql_gallery, $GLOBALS['db_conn']);

            $i = 0;

           // while ($row_gallery = mssql_fetch_array($result_gallery)) {
            $numSize = count($_POST['galery_plan_id']);

          for ($i = 0; $i < $numSize; $i++) {
                   $sql_update_gallery = "UPDATE LH_GALERY_PLAN SET galery_plan_img_seo =N'" . $_POST['galery_plan_img_seo_edit'][$i] . "' ,
                galery_plan_name_dis = N'" . $_POST['project_galery_text_edit'][$i] . "',
                galery_plan_name_dis_en = '" . $_POST['project_galery_text_en_edit'][$i] . "'
                WHERE galery_plan_id = '" . $_POST['id_gallery'][$i] . "'";
                    $result_gallery_update = mssql_query($sql_update_gallery, $GLOBALS['db_conn']);
                    //echo $data["project_galery_text_edit"][$i];
            }
}


$_SESSION['add'] = '1';
header("location:homemodel_update.php?id=$id");
