<?php
require_once 'include/dbConnect.php';

	try {
		$galery_project_img_id = $_GET['galery_project_img_id'];

		$conn = (new dbConnect())->getConn();
		$sql = "DELETE FROM LH_GALERY_IMG_PROJECT
				WHERE galery_project_img_id =".$galery_project_img_id;
        $result= $conn->query($sql);

		echo json_encode($result->fetchAll());

	} catch (\Exception $e) {
		return $e->getMessage();
	}
?>