<?php
session_start();
include './include/dbCon_mssql.php';
date_default_timezone_set('Asia/Bangkok');
$date         = date("Y-m-d");
$numrand_logo = (mt_rand());
$numrand_img  = (mt_rand());

$living_tip_name_th        = $_POST['living_tip_name_th'];
$living_tip_name_en        = $_POST['living_tip_name_en'];
$living_tip_category       = $_POST['living_tip_category'];
$living_tip_img            = $_FILES['living_tip_img'];
$living_tip_content_th     = $_POST['living_tip_content_th'];
$living_tip_content_en     = $_POST['living_tip_content_en'];
$living_tip_dis_content    = $_POST['living_tip_dis_content'];
$living_tip_dis_content_en = $_POST['living_tip_dis_content_en'];
$file_image                = $living_tip_img['name'];
$check                     = getimagesize($_FILES["living_tip_img"]["tmp_name"]);
$seo_living_tip_img        = $_POST['seo_living_tip_img'][0];
$date_start = $_POST['date_start'];
$custom_url = trim($_POST['custom_url']);
$h1 = $_POST["h1"];

$fileupload_th = $_FILES['fileupload_th']['name'];
$title_seo_th = $_POST['landing_page_title_seo_th'];
$description = $_POST['landing_page_description_th'];
$keyword_seo = $_POST['keyword_seo'];
$date_seo = date("YmdHis");

$day =substr($date_start, 0, 2);
$mount =substr($date_start, 3, 2);
$year =substr($date_start, 6, 4);

$date_start = $year."-".$mount."-".$day;
$time=date("Y-m-d H:i:s");
$id = $_POST['id'];

$sql          = "SELECT COUNT(*) AS num FROM LH_LIVING_TIPS WHERE living_tip_name_th = N'$living_tip_name_th ' AND NOT living_tip_id = '$id' ";
$query_result = mssql_query($sql, $db_conn);
$count_date   = mssql_fetch_row($query_result);
$count_date[0];
if ($count_date[0] >= 1) {
  $_SESSION['add'] = '2';
  echo '<script>
  window.history.go(-1);
  </script>';
  exit();
}

$path = "fileupload/images/landingpage/";

$type_th = strrchr($_FILES['fileupload_th']['name'], ".");

$newname_th = $date_seo . $numrand_th . $type_th;

$path_img_seo = $path . $newname_th;

if (move_uploaded_file($_FILES['fileupload_th']['tmp_name'], $path_img_seo)) {

  $sql="UPDATE LH_LIVING_TIPS SET "
  ."thumnail_seo = '".$path_img_seo."'"
  ."WHERE living_tip_id = '$id'";

  $query=mssql_query($sql);

}

$query_2 = "UPDATE LH_LIVING_TIPS SET living_tip_name_th = N'$living_tip_name_th', "
. "living_tip_name_en ='$living_tip_name_en',"
. "living_tip_category ='$living_tip_category',"
. "living_tip_content_th =N'" . mssql_escape($living_tip_content_th) . "',"
. "living_tip_content_en ='" . mssql_escape($living_tip_content_en) . "',"
. "living_tip_dis_content =N'" . mssql_escape($living_tip_dis_content) . "',"
. "living_tip_dis_content_en ='" . mssql_escape($living_tip_dis_content_en) . "',"
. "update_date = '$time' ,"
. "title_seo = N'".$title_seo_th."',"
. "description_seo = N'".$description."',"
. "keyword_seo = N'".$keyword_seo."',"
. "custom_url = N'".$custom_url."',"
. "h1 =N'$h1', "
. "living_tip_post_date = '$date_start' "
. "WHERE living_tip_id = '$id'";

$query_result2 = mssql_query($query_2, $db_conn);

function mssql_escape($str)
{
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return str_replace("'", "''", $str);
}

if ($check !== false) {
    //รูป

    $dates_file = date("Y-m-d");
    $path_img   = "fileupload/images/living_tips/";

      $images = $_FILES["living_tip_img"]["tmp_name"];
      $type_img = strrchr($_FILES["living_tip_img"]["name"],".");
      $new_images = "Thumbnails_".$dates_file.$numrand_img.$type_img;
      //copy($_FILES["lead_image"]["tmp_name"][0],"fileupload/images/project_img/".$_FILES["lead_image"]["name"][0]);
      $width=600; //*** Fix Width & Heigh (Autu caculate) ***//
      $size=GetimageSize($images);
      $height=round($width*$size[1]/$size[0]);
      $images_orig = ImageCreateFromJPEG($images);
      $photoX = ImagesX($images_orig);
      $photoY = ImagesY($images_orig);
      $images_fin = ImageCreateTrueColor($width, $height);
      ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
      ImageJPEG($images_fin,"fileupload/images/living_tips/".$new_images,93);
      ImageDestroy($images_orig);
      ImageDestroy($images_fin); 


      $type_img   = strrchr($file_image, ".");

      $newname_img   = $dates_file . $numrand_img . $type_img;
      $path_copy_img = $path_img . $newname_img;

      if (move_uploaded_file($_FILES['living_tip_img']['tmp_name'], $path_copy_img)) {
        $sql   = "SELECT living_tip_img FROM LH_LIVING_TIPS WHERE living_tip_id = '$id'";
        $query = mssql_query($sql, $db_conn);
        $row   = mssql_fetch_array($query);
        unlink($path_img . $row['living_tip_img']);

        $sql = "UPDATE LH_LIVING_TIPS SET living_tip_img = '$path_copy_img' , living_tip_img_seo = N'" . mssql_escape($seo_living_tip_img) . "', "
        ."title_seo = N'".$title_seo_th."',"
        ."description_seo = N'".$description."',"
        ."keyword_seo = N'".$keyword_seo."'"
        . " WHERE living_tip_id = '$id'";
        $query = mssql_query($sql, $db_conn);

    } else {
        $_SESSION['add'] = '-1';
        header("location:living_tips_update.php?id=$id");
    }
}

if ($query_result2) {
    $_SESSION['add'] = '1';
    header("location:living_tips_update.php?id=$id");
} else {
    $_SESSION['add'] = '-2';
    header("location:living_tips_update.php?id=$id");
}
