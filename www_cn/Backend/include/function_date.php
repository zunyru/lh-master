<?php
                         
    function DateThai($strDate)
    {
        //$strYear = date("Y",strtotime($strDate))+543;
        $strYear = date("Y",strtotime($strDate));
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
       
        return "$strDay $strMonthThai $strYear";
    }

    function DateThaiMount($strDate)
    {
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strDay = substr($strDate, 0, 2);
        $strMonth = substr($strDate, 3, 2);
        if($strMonth != 10){
          $strMonth = str_replace("0","",$strMonth);
        }
        $strYear = substr($strDate,6,4);
        $strMonthThai=$strMonthCut[$strMonth];
        //$strMonthThai=$strMonthCut[$strMonth];
        //return "$strDay $strMonthThai $strYear, $strHour:$strMinute น.";
        return "$strMonthThai $strYear";
    }

    function DateThaiYear($strDate)
    {
        $strYear = date("Y",strtotime($strDate))+543;
        //$strYear = date("Y",strtotime($strDate));
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
       
        return "$strDay $strMonthThai $strYear";
    }

    function MyDateThai($strDate)
    {
    $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strDay = substr($strDate, 0, 2);
        $strMonth = substr($strDate, 3, 2);
        if($strMonth != 10){
          $strMonth = str_replace("0","",$strMonth);
        }
        $strYear = substr($strDate,6,4);
        $strMonthThai=$strMonthCut[$strMonth];
        //$strMonthThai=$strMonthCut[$strMonth];
        //return "$strDay $strMonthThai $strYear, $strHour:$strMinute น.";
        return "$strDay $strMonthThai $strYear";
    }

    function DateThai_edu($strDate){
        //01/01/2560
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        //$strDay = substr($strDate, 0, 2);
        $strMonth = substr($strDate, 3, 2);
        if($strMonth != 10){
          $strMonth = str_replace("0","",$strMonth);
        }
        $strYear = substr($strDate,6,4);
        $strMonthThai=$strMonthCut[$strMonth];
        //$strMonthThai=$strMonthCut[$strMonth];
        //return "$strDay $strMonthThai $strYear, $strHour:$strMinute น.";
	return "$strMonthThai $strYear";
    }

    function Age($strDate){
        $Y = date("Y")+543;
        $strYear = substr($strDate,6,4);
        $Age = $Y - $strYear;
        return $Age;
    }

function DateThai_time($strDate)
{
    //$strYear = date("Y",strtotime($strDate))+543;
    $strYear = date("Y",strtotime($strDate));
    $strMonth= date("n",strtotime($strDate));
    $strDay= date("j",strtotime($strDate));
    $strHour= date("H",strtotime($strDate));
    $strMinute= date("i",strtotime($strDate));
    $strSeconds= date("s",strtotime($strDate));
    $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
    $strMonthThai=$strMonthCut[$strMonth];
    return "$strDay $strMonthThai $strYear <br> เวลา $strHour:$strMinute.$strSeconds น.";
    //return "$strDay $strMonthThai $strYear";
}

function DateTime($strDate)
{
 
    //$strYear = date("Y",strtotime($strDate))+543;
    $strYear = date("Y",strtotime($strDate));
    $strMonth= date("n",strtotime($strDate));
    $strDay= date("j",strtotime($strDate));
    $strHour= date("H",strtotime($strDate));
    $strMinute= date("i",strtotime($strDate));
    $strSeconds= date("s",strtotime($strDate));

    $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
    $strMonthThai=$strMonthCut[$strMonth];
    return "$strHour.$strMinute น.";
    //return "$strDay $strMonthThai $strYear";
}
?>
