<?php


require_once 'include/dbConnect.php';
require_once 'include/dbCon_mssql.php'; // for insert utf8

class FunctionProject
{

    public $conn ;

    function __construct() {

        $this->conn = (new dbConnect())->getConn();

    }


    public function getProject($projectId)
    {


        try {


            $sql = ' select  project_name_th ,  project_name_en , zone_id , brand_id , group_id , project_url ,
            company_no_DL200 , project_no_DL200 , area_farm , area_square_m , area_square_w , area_land ,
            public_date ,  project_status ,  latitude , longtitude, project_url_register ,paking ,paking_en ,location,location_en
            from lh_projects
            where project_id = '.$projectId;

        // use exec() because no results are returned
        // $result = $this->conn->query($sql);
        // return $result->fetchAll();
            $result = mssql_query($sql);
            $results = [];
            while ($row = mssql_fetch_assoc($result)) {
              $results[] = $row;
          }

          return $results;

      }
      catch(PDOException $e)
      {
        echo $sql . "<br>" . $e->getMessage();
    }

}

public function getProductshome()
{

    try {

        $sql = " select product_name_th ,product_name_en, product_id
        from lh_products ";

            // use exec() because no results are returned
            // $result = $this->conn->query($sql);
            // return $result->fetchAll();
        $result =mssql_query($sql,$GLOBALS['db_conn']);
        $results = [];
        while ($row = mssql_fetch_assoc($result)) {
          $results[] = $row;
      }

      return $results;

  } catch (PDOException $e)
  {
    echo $sql . "<br>" . $e->getMessage();
}

}


public function getAboutZone($project_id)
{
    try {

        $sql = "SELECT * FROM lh_project_zone
        WHERE project_id =".$project_id;

        $result = $this->conn->query($sql);


        return $result->fetchAll();

    }  catch (PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}



    // public function getZone($zoneId)
    // {
    //     try {

    //         $sql = "SELECT lh_provinces.province_name ,
    //                 lh_provinces.province_id ,
    //                 lh_zones.zone_id ,
    //                 lh_zones.zone_name_th,
    //                 lh_zones.zone_name_en,
    //                 lh_zones.zone_attribute
    //                 FROM lh_zones
    //                 INNER JOIN lh_provinces ON lh_provinces.province_id = lh_zones.province_id
    //                 WHERE lh_zones.zone_id = ".$zoneId;

    //         $result = $this->conn->query($sql);


    //         return $result->fetchAll();

    //     }  catch (PDOException $e) {
    //         echo $sql . "<br>" . $e->getMessage();
    //     }
    // }


public function getSEO($projectId)
{
    try {


        $sql = " select  project_title_seo_th ,
        project_title_seo_en ,
        project_des_seo_th ,
        project_des_seo_en,
        project_keyword_th,
        project_keyword_en,
        project_id
        from LH_PROJECT_SEO
        WHERE project_id = ".$projectId;

        $query_r= mssql_query($sql , $GLOBALS['db_conn']);
        $detailSeo = mssql_fetch_array($query_r);

        if (sizeof($detailSeo) > 0) {
            return $detailSeo;
        } else {
            $dummy = array();
            $dummy['project_title_seo_th'] = '';
            $dummy['project_title_seo_en'] = '';
            $dummy['project_des_seo_th'] = '';
            $dummy['project_des_seo_en'] = '';
            $dummy['project_keyword_th'] = '';
            $dummy['project_keyword_en'] = '';


            return array($dummy);
        }


    }  catch (PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}


public function getSubProject($projectId)
{
    try {

        $sql = " select   lh_products.product_id ,
        lh_products.product_name_th ,
        lh_products.product_name_en ,
        lh_project_sub.project_sub_id,
        lh_project_sub.progress_updat_full_condo

        from lh_project_sub
        INNER JOIN lh_products ON lh_project_sub.product_id = lh_products.product_id
        WHERE lh_project_sub.project_id = ".$projectId;

        $result = $this->conn->query($sql);

        $detilSubProject =  $result->fetchAll();



        return $detilSubProject;

    }  catch (PDOException $e)
    {
        echo $sql . "<br>" . $e->getMessage();
    }

}

public function getBrandByBrandId($brandId)
{
    try {


        $sql = " select   brand_name_th ,
        brand_name_en ,
        brand_concep_th,
        brand_concep_en,
        logo_brand_th,
        logo_brand_en,
        brand_id
        from lh_brands
        WHERE brand_id = ".$brandId;


        $result = $this->conn->query($sql);

        $detailBrand =  $result->fetchAll();

        if (sizeof($detailBrand) > 0) {
            return $detailBrand;
        } else {
            $dummy = array();
            $dummy['brand_id'] = '';
            $dummy['brand_name_th'] = '';
            $dummy['brand_name_en'] = '';
            return array($dummy);
        }


    }  catch (PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}


public function getProjectConcept($projectId)
{

    try {

        $sql = " select
        project_id,
        concept_th,
        concept_en,
        distinctive_th,
        distinctive_en,
        facilities_th,
        facilities_en,
        security_th,
        security_en,
        information_th,
        information_en
        from LH_PROJECT_CONCEPT
        WHERE project_id = ".$projectId;


        $result = mssql_query($sql);
        $detailConcept = 0;
        if ($result) {
            $results= array();
            while ($result_fetch = mssql_fetch_array($result)) {
                $results[]= $result_fetch;
            }
            $detailConcept = $results;
        }

        if (sizeof($detailConcept) > 0) {
            return $detailConcept;
        } else {

            $dummy = array();

            $dummy['project_id'] = "";
            $dummy['concept_th'] = "";
            $dummy['concept_en'] = "";
            $dummy['distinctive_th'] = "";
            $dummy['distinctive_en'] = "";
            $dummy['distinctive_en'] = "";
            $dummy['facilities_th'] = "";
            $dummy['facilities_en'] = "";
            $dummy['security_th'] = "";
            $dummy['security_en'] = "";
            $dummy['information_th'] = "";
            $dummy['information_en'] = "";

            return array($dummy);
        }


    }  catch (PDOException $e)
    {
        echo $sql . "<br>" . $e->getMessage();
    }

}


public function getProjectContact($projectId)
{
    try {

        $sql = "select
        lh_project_contacts.project_id,
        lh_project_contacts.open_day,
        lh_project_contacts.open_time_start,
        lh_project_contacts.open_time_end,
        lh_project_contacts.telephone,
        lh_project_contacts.email,
        lh_project_contacts.telephone_line,
        lh_project_contacts.name_staff,
        lh_project_contacts.sales,
        lh_project_contacts.central_value,
        lh_project_contacts.prepay,
        lh_project_contacts.unit_id
        from lh_project_contacts

        where lh_project_contacts.project_id =".$projectId;

        $result = $this->conn->query($sql);

        $detailConcept =  $result->fetchAll();


        if (sizeof($detailConcept) > 0) {
            return $detailConcept;
        } else {

            $dummy = array();

            $dummy['project_id'] = "";
            $dummy['open_day'] = "0";
            $dummy['open_time_start'] = "";
            $dummy['open_time_end'] = "";
            $dummy['telephone'] = "";
            $dummy['email'] = "";
            $dummy['telephone_line'] = "";
            $dummy['name_staff'] = "";
            $dummy['regular'] = "";
            $dummy['central_value'] = "";
            $dummy['prepay'] = "";
            $dummy['unit_id'] = "1";
            $dummy['unit_name'] = "";

            return array($dummy);
        }


    }catch (PDOException $e)
    {
        echo $sql . "<br>" . $e->getMessage();
    }
}


public function getlh_unit()
{
    try {

        $sql = " select unit_id , unit_name
        from lh_units ";

        $result = $this->conn->query($sql);

        $detailunit =  $result->fetchAll();


        return $detailunit;
    }catch (PDOException $e)
    {
        echo $sql . "<br>" . $e->getMessage();
    }
}



public function getProjectPrice($projectId)
{
    try {

        $sql = " select project_id,price_mode,project_price
        from lh_project_prices
        where project_id = ".$projectId;


        $result = $this->conn->query($sql);

        $detailConcept =  $result->fetchAll();


        if (sizeof($detailConcept) > 0)
        {
            $modePrice = $detailConcept[0]['price_mode'];
            if ($modePrice == "2") {
                $value_pire_mod = explode("-", $detailConcept[0]['project_price']);

                $value_pire_mod_begine = $value_pire_mod[0];
                $value_pire_mod_end = $value_pire_mod[1];

                $detailConcept[]['value_pire_mod_begine'] = $value_pire_mod_begine;
                $detailConcept[]['value_pire_mod_end'] = $value_pire_mod_end;
            }


            return $detailConcept;
        } else {

            $dummy = array();

            $dummy['project_id'] = "";
            $dummy['price_mode'] = "1";
            $dummy['project_price'] = "";
            $dummy['price_status'] = "";
            $dummy['value_pire_mod_begine'] = "";
            $dummy['value_pire_mod_end'] = "";

            return array($dummy);
        }


    }catch (PDOException $e)
    {
        echo $sql . "<br>" . $e->getMessage();
    }

}


public function get_project_nearby($projectId)
{
    try {

        $sql = " select * from lh_project_nearby
        where project_id = ".$projectId ;

        // $result = $this->conn->query($sql);
        // $detailnearby =  $result->fetchAll();
        $result = mssql_query ($sql, $GLOBALS ['db_conn'] );
        $detailnearby = [];
        while ($row = mssql_fetch_assoc($result)) {
          $detailnearby[] = $row;
      }

      if(sizeof($detailnearby) > 0)
      {
        return $detailnearby;
    }
    else{
        $dummy =array();

        $dummy['nearby_id'] = "";
        $dummy['nearby_name_th'] = "";
        $dummy['nearby_name_en'] = "";
        $dummy['interval'] = "";
        $dummy['project_id'] = "";

        return array($dummy);
    }

} catch (PDOException $e) {
    echo $sql . "<br>" . $e->getMessage();
}

}



public function mapCheckedProductOfAllProduct($productAll, $projectSub)
{
    $returnProjectProduct = array();

    $sizeProductAll = sizeof($productAll);

    for ($i = 0; $i < $sizeProductAll; $i++) {
        $checked = false;
        $sizeProjectSub = sizeof($projectSub);
        for ($j = 0; $j < $sizeProjectSub; $j++) {

            if ($productAll[$i]['product_id'] == $projectSub[$j]['product_id']) {
                $checked = true;
            }
        }

        $returnProjectProduct[] = array(
            "product_home_id" => $productAll[$i]['product_id'],
            "product_home_name_th" => $productAll[$i]['product_name_th'],
            "product_home_name_en" => $productAll[$i]['product_name_en'],
            "checked" => $checked);

    }
    return $returnProjectProduct;
}



public function fixValueBuild()
{
    $valuebuild = array(
        array("0", "เลือกอาคาร"),
        array("1", "A"),
        array("2", "B"),
        array("3", "C"),
        array("4", "D"),
        array("5", "E"),
        array("6", "F"),
        array("7", "G"),
        array("8", "H"),
        array("9", "I"),
        array("10", "J"),
        array("11", "K"),
        array("12", "L"),
        array("13", "M"),
        array("14", "N"),
        array("15", "O"),
        array("16", "P"),
        array("17", "Q"),
        array("18", "R"),
        array("19", "S"),
        array("20", "T"),
        array("21", "U"),
        array("22", "V"),
        array("23", "W"),
        array("24", "X"),
        array("25", "Y"),
        array("26", "Z")
    );
    return $valuebuild;

}


public function getDetailSubProject($objectSubProjects)
{
    $returnDetail = array();

    $sizeSubject = sizeof($objectSubProjects);

       //  print_r($objectSubProjects);

        //return '';

    for ($i = 0; $i < $sizeSubject; $i++)
    {
            // pull projectsubId and product_home_id
        $itemSubprojectId = $objectSubProjects[$i]['project_sub_id'];
        $itemProduct_home_id = $objectSubProjects[$i]['product_id'];

        $detailHome = $this->getAreaFirstHome($itemSubprojectId);
        $detailProduct = $this->getProductshomeByProductHomeId($itemProduct_home_id);
        $product_home_name_th = $detailProduct[0]['product_name_en'];


            // Product ID Condo
        if ($itemProduct_home_id == "3")
        {
            $detailCondo = $this->getAreaCondo($itemSubprojectId);

            $arrayBuild = array();
            $arrayDetailCondo = array();
            $arrayDetailUnitCondo = array();

            if ($detailCondo == null) {
                $arrayDetailCondo[] = array(
                    "project_area_condo_id" => '',
                    "area_condofrom" => '',
                    "area_condoto" => '',
                    "num_plan_condo" => '',
                    "building_uni" => '',
                    "floor" => ''
                );

                $arrayBuild[] = array(
                    "building_id" => '',
                    "building_name" => '',
                    "project_area_condo_id" => '',
                    "building_num_layer" => '',
                    "building_unit" => '');

            } else {


                $project_area_condo_id = $detailCondo[0]['project_area_condo_id'];
                $area_condo_array = explode("-", $detailCondo[0]['area_condo']);
                $arra_condofrom = $area_condo_array[0];
                $arra_condoto = $area_condo_array[1];

                $num_plan_condo = $detailCondo[0]['num_plan_condo'];
                $building_unit = $detailCondo[0]['building_unit_id'];
                $floor = $detailCondo[0]['floor'];

                $arrayDetailCondo[] = array(
                    "project_area_condo_id" => $project_area_condo_id,
                    "area_condofrom" => $arra_condofrom,
                    "area_condoto" => $arra_condoto,
                    "num_plan_condo" => $num_plan_condo,
                    "building_uni" => $building_unit,
                    "floor" => $floor 
                );

                $detailBuilding = $this->getBuilding($project_area_condo_id);

                $sizearrayBuild = sizeof($detailBuilding);

                if ($sizearrayBuild < 1) {
                    $arrayBuild[] = array(
                        "building_id" => '',
                        "building_name" => '',
                        "project_area_condo" => '',
                        "building_num_layer" => '',
                        "building_unit" => '');
                } else {
                    for ($j = 0; $j < $sizearrayBuild; $j++) {
                        $itemBuild = $detailBuilding[$j];
                        $building_id = $itemBuild['building_id'];
                        $building_name = $itemBuild['building_name'];
                        $project_area_condo = $itemBuild['project_area_condo_id'];
                        $building_num_layer = $itemBuild['building_num_layer'];
                        $building_unit = $itemBuild['building_unit'];

                        $arrayBuild[] = array(
                            "building_id" => $building_id,
                            "building_name" => $building_name,
                            "project_area_condo" => $project_area_condo,
                            "building_num_layer" => $building_num_layer,
                            "building_unit" => $building_unit);
                    }
                }
            }

            $detailUnitCondo = $this->getUnitAreaCondo($itemSubprojectId);

            $sizearrayUnit = sizeof($detailUnitCondo);

            if($sizearrayUnit == null){
              $arrayDetailUnitCondo[] = array(
                'unit_condo_id' => '',
                'project_area_condo_id' => '',
                'unit_condo_name' => '',
                'size_unit_codo' => '');
          }else{
            for ($j = 0; $j < $sizearrayUnit; $j++) {
                $itemUnit = $detailUnitCondo[$j];
                $unit_condo_id = $itemUnit['unit_condo_id'];
                $unit_condo_name = $itemUnit['unit_condo_name'];
                $size_unit_codo = $itemUnit['size_unit_codo'];
                $project_area_condo_id = $itemUnit['project_area_condo_id'];

                $arrayDetailUnitCondo[] = array(
                    'unit_condo_id' => $unit_condo_id,
                    "project_area_condo_id" => $project_area_condo_id,
                    "unit_condo_name" => $unit_condo_name,
                    "size_unit_codo" => $size_unit_codo);
            }
        }

        $returnDetail[] = array(
            "project_sub_id" => $itemSubprojectId,
            "product_home_id" => $itemProduct_home_id,
            "product_home_name_th" => $product_home_name_th,
            "detailcondo" => $arrayDetailCondo,
            "detailUnitCondo_new" =>  $arrayDetailUnitCondo,
            "optionbuilding" => $arrayBuild,
            "dataUnitPlanCondo" => $this->getUnitPlanForCondo($itemSubprojectId),
            "dataMasterPlanCondo" => $this->getMasterPlanForCondo($itemSubprojectId),
            "dataProgressCondo" => $this->getProgressCondo($itemSubprojectId)
        );




    } else {
        $project_area_home = ($detailHome != null) ? $detailHome[0]['project_area_home_id'] : "";
        $area_home = ($detailHome != null) ? $detailHome[0]['area_home'] : "";
        $area_square_w_home = ($detailHome != null) ? $detailHome[0]['area_square_w_home'] : "";
        $num_plan_home = ($detailHome != null) ? $detailHome[0]['num_plan_home'] : "";


        $returnDetail[] = array(
            "project_sub_id" => $itemSubprojectId,
            "product_home_id" => $itemProduct_home_id,
            "product_home_name_th" => $product_home_name_th,
            "project_area_home" => $project_area_home,
            "area_home" => $area_home,
            "area_square_w_home" => $area_square_w_home,
            "num_plan_home" => $num_plan_home
        );
    }
}
return $returnDetail;
}


public  function getGalery($objectSubProjects){

    $sql="SELECT * FROM LH_GALERY_PROJECT p LEFT JOIN LH_GALERY_IMG_PROJECT m ON p.project_id = m.galery_project_id WHERE p.project_id = $objectSubProjects";
    $result = $this->conn->query($sql);

    $detailnearby =  $result->fetchAll();

    return $detailnearby;
}



private function getAreaFirstHome($projectSubId)
{
    try {

        $sql = " select project_area_home_id,
        project_sub_id,
        area_home,
        num_plan_home,
        area_square_w_home
        from lh_project_area_home
        where project_sub_id = ".$projectSubId ;

        $result = $this->conn->query($sql);
        $detailSeo =  $result->fetchAll();

        return $detailSeo;
    } catch (PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}


public function getProductshomeByProductHomeId($product_id)
{

    try {

        $sql = " select product_id,product_name_th,product_name_en
        from lh_products
        where product_id = ".$product_id ;

        $result = $this->conn->query($sql);
        $products =  $result->fetchAll();

        return $products;
    } catch (PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}



private function getAreaCondo($projectSubId)
{
    try {

        $sql = " select project_area_condo_id,project_sub_id,area_condo,num_plan_condo,building_unit_id,floor
        from lh_project_area_condo
        where project_sub_id = ".$projectSubId ;

        $result = mssql_query($sql ,$GLOBALS['db_conn']);
        $detailSeo = [];
        while ($row = mssql_fetch_assoc($result)) {
          $detailSeo[] = $row;
      }

      return $detailSeo;
  }  catch (PDOException $e) {
    echo $sql . "<br>" . $e->getMessage();
}
}

private function getUnitAreaCondo($projectSubId)
{
    try {

        $sql = " select unit_condo_id,project_area_condo_id,unit_condo_name,size_unit_codo,project_sub_id
        from LH_UNIT_AREA_CONDO
        where project_sub_id = ".$projectSubId ;


        $result = mssql_query($sql);
        $detailSeo = [];
        while ($row = mssql_fetch_assoc($result)) {
          $detailSeo[] = $row;
      }


    return $detailSeo;
    }catch (PDOException $e) {
    echo $sql . "<br>" . $e->getMessage();
    }
}


private function getBuilding($project_area_condo_id)
{
    try {

        $sql = " select building_id,building_name,project_area_condo_id,building_num_layer,building_unit
        from LH_BUILDING_UNIT
        where project_area_condo_id = ".$project_area_condo_id ;

        $result = $this->conn->query($sql);
        $detailBuilding =  $result->fetchAll();

        return $detailBuilding;
    }catch (PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

public function getUnitPlanForCondo($subProjectId)
{
    try {


        $sql = " select lh_unit_plan_condo.*,lh_unit_plan_img.*
        from lh_unit_plan_condo
        INNER JOIN lh_unit_plan_img ON lh_unit_plan_condo.unit_plan_id = lh_unit_plan_img.unit_plan_id
        where lh_unit_plan_condo.project_sub_id = ".$subProjectId ;

            // $result = $this->conn->query($sql);
            // $listunitPlanCondo =  $result->fetchAll();
        $result = mssql_query($sql ,$GLOBALS['db_conn']);
        $listunitPlanCondo = [];
        while ($row = mssql_fetch_assoc($result)) {
          $listunitPlanCondo[] = $row;
      }

      return $listunitPlanCondo;

  } catch (PDOException $e) {
    echo $sql . "<br>" . $e->getMessage();
}

}


public function getMasterPlanForCondo($subProjectId)
{
    try {



        $sql = " select lh_master_plan_condo.*,lh_floor_plan_img.*
        from lh_master_plan_condo
        INNER JOIN lh_floor_plan_img ON lh_floor_plan_img.master_plan_id = lh_master_plan_condo.master_plan_id
        where lh_master_plan_condo.project_sub_id = ".$subProjectId ;



        $result = $this->conn->query($sql);
        $listmasterPlanCondo =  $result->fetchAll();


        return $listmasterPlanCondo;

    }  catch (PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}

public function getProgressCondo($subproject)
{
    $sql = " select *
    from lh_progress_update_condo
    where project_sub_id = ".$subproject;

    try
    {
        $result = $this->conn->query($sql);
        $listProgressCondo =  $result->fetchAll();

        $sizelistProgressCondo = sizeof($listProgressCondo);

        for($i=0 ; $i < $sizelistProgressCondo ; $i++)
        {
            $progress_update_id = $listProgressCondo[$i]["progress_update_id"];
            $list_progress_Image = $this->getProgressCondoByProgressUpdateId($progress_update_id);

            $listProgressCondo[$i]["progressImg"] = $list_progress_Image;

        }

        return $listProgressCondo;

    }
    catch (PDOException $e)
    {
        echo $sql . "<br>" . $e->getMessage();
    }


}

public function getProgressCondoByProgressUpdateId($progress_update_id)
{

    $sql = " select *
    from lh_progress_update_img
    where progress_update_id = ".$progress_update_id;

    try
    {
        $result = $this->conn->query($sql);
        $listProgressCondoImg =  $result->fetchAll();
        return $listProgressCondoImg ;
    }
    catch (PDOException $e)
    {
        echo $sql . "<br>" . $e->getMessage();
    }


}


public function getProjectMaps($projectId)
{
    try {

        $sql = "SELECT * FROM LH_PROJECT_MAPS
        WHERE project_id =".$projectId;

        $result = $this->conn->query($sql);
        $result_map =  $result->fetchAll();

        return $result_map;

    }  catch (PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}

public function getImagesProjectInitialPreview($projectId)
{
    try {

        $sql = "SELECT * FROM LH_LEAD_IMAGE_PROJECT_FILE
        WHERE project_id =".$projectId;

        // $result = $this->conn->query($sql);
        // $result_images =  $result->fetchAll();
        $result = mssql_query ( $sql, $GLOBALS ['db_conn'] );

        $result_images = [];
        while ($row = mssql_fetch_assoc($result)) {
          $result_images[] = $row;
      }
      $result_images;

      $url = "http://devwww2.lh.co.th/Land_and_house/Backend/";
      $initialPreview = [];
      for ($i = 0; $i < count($result_images); $i++) {
        array_push($initialPreview, $url.$result_images[$i]['LEAD_IMAGE_PROJECT_FILE_NAME']);
    }

    return json_encode([
        'initialPreview' => [$initialPreview]
    ]);

}  catch (PDOException $e) {
    echo $sql . "<br>" . $e->getMessage();
}
}


public function getImagesProjectInitialPreviewConfig($projectId)
{
    try {

        $sql = "SELECT * FROM LH_LEAD_IMAGE_PROJECT_FILE
        WHERE project_id =".$projectId;

        // $result = $this->conn->query($sql);
        // $result_images =  $result->fetchAll();
        $result = mssql_query ( $sql, $GLOBALS ['db_conn'] );

        $result_images = [];
        while ($row = mssql_fetch_assoc($result)) {
          $result_images[] = $row;
      }
      $result_images;

      $urlDelete = "http://devwww2.lh.co.th/Land_and_house/Backend/delete_image_project.php";
      $initialPreviewConfig = [];
      for ($i = 0; $i < count($result_images); $i++) {
        array_push($initialPreviewConfig,[
            'key' => $result_images[$i]['LEAD_IMAGE_PROJECT_FILE_ID'],
            'url' => $urlDelete
        ]);
    }

    return json_encode([
        'initialPreviewConfig' => [$initialPreviewConfig]
    ]);

}  catch (PDOException $e) {
    echo $sql . "<br>" . $e->getMessage();
}

}


public function getfindValue($detailSubProject)
{
        // Find Type 3 == Type Condo


    foreach( $detailSubProject as $item )
    {
        if($item["product_id"] == 3)
        {
            return $item["progress_updat_full_condo"] ;
        }
    }
    return "" ;
}


public function getUrlClipByIdProject($projectId)
{
    try {

        $sql = "SELECT * FROM LH_PROJECT_CLIPS
        WHERE project_id =".$projectId;

        $result = $this->conn->query($sql);
        $result_clip =  $result->fetchAll();

        return $result_clip;

    }  catch (PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}













}


?>