<?php
session_start();
$_SESSION['group_id'];
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>

    <!-- Bootstrap -->
    <link href="../../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
     <!-- Switchery -->
    <link href="../../vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md" ng-app="user"  ng-controller="userAddController">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

            <?php  include  '../master/navbar_regis.php' ?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
          <?php include '../master/top_nav_regis.php'; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                  
                  <div class="bs-example-popovers">
                        <a href="apply1.php" class="btn btn-default">
                           ตำแหน่งงานว่าง 
                        </a>
                       <a href="candidacy1.php" class="btn btn-default">
                           ผู้สมัครงาน 
                        </a>
                       <a href="report.php" class="btn btn-default">
                           รายงาน 
                        </a>
                       <a href="position1.php" class="btn btn-default">
                           ชื่อตำแหน่งงาน 
                        </a>
                        <a href="news1.php" class="btn btn-default">
                           ข่าวสาร 
                        </a>
                        <a href="area1.php" class="btn btn-default">
                           กำหนดพื้นที่ทำงาน 
                        </a>
                        <a href="user1.php" class="btn btn-success active">
                           ผู้ใช้งาน 
                        </a>
                    </div>
                   <br>
                  
                <div class="x_title">
                  <h2><a href="user1.php">ข้อมูลผู้ใช้</a> > <a href="#">สร้างข้อมูลผู้ใช้</a></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">



                <div class="row" ng-if="success">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>อัปเดทข้อมูลเรียบร้อย</strong>  
                      </div>
                    </div>
                    <div class="col-md-4"></div> 
                   </div> 
                  
                    <div class="row"  ng-if="duplicate">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>มีชื่อนี้อยู่ในระบบแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div> 
                   </div> 
                  
                    <div class="row"  ng-if="unsuccess" >
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>ผิดพลาด !</strong> กรุณากรอกข้มูลให้ครบถ้วน
                      </div>
                    </div>
                    <div class="col-md-4"></div> 
                   </div> 


                  <p class="font-gray-dark"> 
                  </p><br>

                  <form class="form-horizontal form-label-left" name="Form" ng-submit="submitForm()" novalidate >
                    <div class="form-group" ng-class="{ 'has-error': Form.user.$invalid && Form.user.$dirty }" >
                      <label class="control-label col-md-2" for="first-name">ชื่อผู้ใช้งาน <span class="required">*</span>
                      </label>
                      <div class="col-md-4">
                          <input type="text" id="first-name2" required class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อผู้ใช้งาน" ng-model="value.user" name="user">  
                          <span style="color:red" ng-show="Form.user.$dirty && Form.user.$invalid">
                            <span ng-show="Form.user.$error.required">กรุณากรอกชื่อผู้ใช้งาน</span>
                            </span>
                      </div>
                    </div>
                    <div class="form-group" ng-class="{ 'has-error': Form.loginname.$invalid && Form.loginname.$dirty }" >
                      <label class="control-label col-md-2" for="first-name">Login Name <span class="required">*</span>
                      </label>
                      <div class="col-md-4">
                          <input type="text" id="first-name2" required class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อผู้ใช้" ng-model="value.loginname" name="loginname" > 
                           <span style="color:red" ng-show="Form.loginname.$dirty && Form.loginname.$invalid">
                            <span ng-show="Form.loginname.$error.required">กรุณากรอก login name</span>
                            </span>
                      </div>
                    </div>
                     <div class="form-group" ng-class="{ 'has-error': Form.pass.$invalid && Form.pass.$dirty }" >
                      <label class="control-label col-md-2" for="first-name">Password <span class="required">*</span>
                      </label>
                      <div class="col-md-4">
                          <input type="password" id="first-name2" required class="form-control col-md-7 col-xs-12" placeholder="กรอกรหัสผ่าน" ng-model="value.pass" name="pass" > 
                           <span style="color:red" ng-show="Form.pass.$dirty && Form.pass.$invalid">
                            <span ng-show="Form.pass.$error.required">กรุณากรอก Password</span>
                            </span>
                      </div>
                    </div>
    
                    <br>
                    <center>
                    <button type="submit" ng-disabled="Form.$invalid" class="btn btn-success">Submit</button>
                  </center>
                  </form>

                </div>
              </div>
            </div>
          </div>
        <!-- /page content -->

        <!-- footer content -->
        <!-- <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer> -->
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../../vendors/iCheck/icheck.min.js"></script>
    <!-- jQuery Tags Input -->
     <!-- Switchery -->
    <script src="../../vendors/switchery/dist/switchery.min.js"></script>
    <script src="../../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Datatables -->
    <script src="../../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../../vendors/pdfmake/build/vfs_fonts.js"></script>
            <!-- bootstrap-daterangepicker -->
    <script src="js/moment/moment.min.js"></script>
    <script src="js/datepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../../build/js/custom.min.js"></script>

    <!-- Angular -->
    <script src="js/lib/angular.min.js"></script>
    <!--App Controller -->
    <script src="js/app.js"></script>
    <script src="js/service.js"></script>

    
  </body>
</html>