<?php
session_start();
$_SESSION['group_id'];
?>

<html><head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <title>LH - Home</title>

    <link rel="shortcut icon" href="images/global/favicon.png" type="image/x-icon">


     <!-- Bootstrap -->
    <link href="../../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../../build/css/custom.min.css" rel="stylesheet">

    <link rel="stylesheet" href="owl.carousel/assets/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="css/fonts.css" type="text/css">
    <link rel="stylesheet" href="css/job.css" type="text/css">
    <link rel="stylesheet" href="css/lib/featherlight.min.css" type="text/css">
    <link rel="stylesheet" href="css/lib/featherlight.gallery.min.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="libs/css/jquery-confirm.css"/>

    <!-- JS -->
    <script src="js/jquery-2.2.3.min.js"></script>
    <script src="js/lib/bootstrap.min.js"></script>
    <script src="js/lib/bootstrap-datepicker.min.js"></script>
    <script src="owl.carousel/owl.carousel.min.js"></script>
    <script src="js/lib/parallax.min.js"></script>
    <script src="js/lib/featherlight.min.js"></script>
    <script src="js/lib/featherlight.gallery.min.js"></script>
    <script src="js/lib/freewall.js"></script>
    <script src="js/global.js"></script>
    <script src="js/job.js"></script>
    <script src="js/banner-md.js"></script>
    <script type="text/javascript" src="libs/js/jquery-confirm.js"></script>
    <!-- FastClick -->
    <script src="../../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../../vendors/iCheck/icheck.min.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Datatables -->
    <script src="../../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../../vendors/pdfmake/build/vfs_fonts.js"></script>
            <!-- bootstrap-daterangepicker -->
    <script src="js/moment/moment.min.js"></script>
    <script src="js/datepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../../build/js/custom.min.js"></script>



    <!-- Angular -->
    <script src="js/lib/angular.min.js"></script>
    <!--App Controller -->
    <script src="js/app.js"></script>
    <script src="js/service.js"></script>



</head>
<body class="nav-md" ng-app="candidacy"  ng-controller="candidacyEditController" >
   <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

            <?php  include  '../master/navbar_regis.php' ?>
       
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
          <?php include '../master/top_nav_regis.php'; ?>
        <!-- /top navigation -->
        
        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                   <!-- menu--> 
                    <div class="bs-example-popovers">
                        <a href="apply1.php" class="btn btn-default">
                           ตำแหน่งงานว่าง 
                        </a>
                       <a href="candidacy1.php" class="btn btn-success active">
                           ผู้สมัครงาน 
                        </a>
                       <a href="report.php" class="btn btn-default">
                           รายงาน 
                        </a>
                       <a href="position1.php" class="btn btn-default">
                           ชื่อตำแหน่งงาน 
                        </a>
                        <a href="news1.php" class="btn btn-default">
                           ข่าวสาร 
                        </a>
                        <a href="area1.php" class="btn btn-default">
                           กำหนดพื้นที่ทำงาน 
                        </a>
                        <a href="user1.php" class="btn btn-default">
                           ผู้ใช้งาน 
                        </a>
                    </div>
                  <!-- -->
                   
               
                  <div class="x_content">



                        <div id="page">
    <div class="sticky-block">    
        <div id="content" class="content">
            <div class="container">
                <div class="row">
                    <div class="job-form current" style="display: block;">
                        <div class="apply-job-container">



    <form action="" class="apply-job-form" name=jobform id="jobform" ng-submit="myFunc()"  novalidate>
        <div class="form-title">ข้อมูลส่วนตัว</div>
        <div class="personal_info form-section">           
            <div class="row">
                <div class="col-md-6 form-row">
                     <div class="col-md-12 col-sm-3"><label>ชื่อ</label></div>
                    <div class="col-md-4">
                        <input type="text" class="textbox" value="{{profile.PREFIX}} " disabled>
                    </div><br>
                    
                    <div class="col-md-8">
                        <input type="text" class="textbox" value="{{profile.NAME}}" disabled>
                    </div>

                </div>
                <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>นามสกุล</label></div>
                    <div class="col-md-12">
                        <input type="text" class="textbox" value="{{profile.LASTNAME}} " disabled>
                    </div>
                    
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>ชื่อเล่น</label></div>
                    <div class="col-md-12">
                        <input type="text" class="textbox" value="{{profile.NICKNAME}} " disabled>
                    </div>
                </div> 
                <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>เพศ</label></div>
                    <div class="col-md-8">
                        <input type="text" class="textbox" value="{{profile.SEX=='M'?'ชาย':'หญิง'}} " disabled>
                    </div>
                </div>
            </div>

            <div class="row">
                  <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>วันเกิด</label></div>
                    <div class="col-md-12 form-group has-feedback">
                        <input type="text" class="textbox" value="{{MydateFormat(profile.BIRTHDAY) }}" disabled> 
                    </div>
                </div>

                <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>ภูมิลำเนา</label></div>
                    <div class="col-md-12">
                       <input type="text" class="textbox" value="{{profile.PROVINCE_NAME}}" disabled>
                    </div>
                </div>
        </div>

            <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>ส่วนสูง(เซนติเมตร) </label></div>
                    <div class="col-md-12">
                       <input type="text" class="textbox" value="{{profile.HEIGHT}}" disabled>                   
                    </div>   
                </div>
                
                <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>น้ำหนัก(กิโลกรัม) </label>
                    </div>  
                    <div class="col-md-12">
                        <input type="text" class="textbox" value="{{profile.WEIGHT}}" disabled>
                    </div>
                </div>
            </div>
           
            <div class="row">
                <div class="col-md-12 form-row">
                    <div class="col-md-12"><label>ศาสนา</label></div>
                    <div class="col-md-12">
                       <input type="text" class="textbox" value="{{profile.RELIGION}}" disabled>
                    </div>
                </div>
            </div>
            
            <div class="row">    
                <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>เชื้อชาติ</label></div> 
                    <div class="col-md-12">
                        <input type="text" class="textbox" value="{{profile.RACE}}" disabled>
                    </div>
                </div>

                <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>สัญชาติ</label></div>
                    <div class="col-md-12">
                        <input type="text" class="textbox" value="{{profile.NATIONSLITY}}" disabled>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12 form-row">
                    <div class="col-md-12"><label>เกณฑ์ทหาร</label></div>
                    <div class="col-md-12">
                        <input type="text" class="textbox" value="{{profile.DRAFT}}" disabled>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-md-12"><label class="text-left">เลขบัตรประจำตัวประชาชน</label></div>
                    <div class="col-md-12">
                        <input type="text" class="textbox" value="{{profile.CARD_NAMBER}}" disabled>
                    </div>  
                 </div>               
            </div>                
                      
             
          
        </div>
        <div class="form-title">ข้อมูลครอบครัว</div>
        <div class="family_info form-section">
            <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-md-12">
                        <label>สถานภาพสมรส</label>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <input type="text" class="textbox" value="{{profile.STATUS}}" disabled>
                     </div>
                </div>
            </div>
            <div class="row title">
                 <div class="col-md-12 form-row">
                        <div class="col-md-4"><label>ครอบครัว</label></div> 
                </div>
            </div>
            
            <div class="row hide-mobile">
                <div class="col-md-12 form-row">
                <div class="col-md-2"></div>
                <div class="col-md-3">ชื่อ-นามสกุล</div>
                <div class="col-md-1">อายุ</div>
                <div class="col-md-3">อาชีพ/ตำแหน่ง</div>
                <div class="col-md-3">ที่อยู่/ที่ทำงาน</div>
                </div>
            </div>

            <div class="row" ng-repeat="contact in family" >
                <div class="col-md-12 form-row">
                    <div class="col-md-2"><label>{{ contact.STATUS }}</label></div>
                    <div class="show-mobile col-md-4"><label>ชื่อนาม-นามสกุล
                    </label></div>
                    <div class="col-md-3">
                    <input type="text" class="textbox" value="{{contact.NAME}}" disabled>
                    </div>
                    <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อายุ</label>
                    </div>
                    <div class="col-md-1">
                    <input type="text" class="textbox" value="{{contact.AGE}}" disabled>
                    </div>
                    <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อาชีพ/ตำแหน่ง
                    </label></div>
                    <div class="col-md-3">
                    <input type="text" class="textbox" value="{{contact.CAREER}}" disabled>
                    </div>
                    <div class="show-mobile col-md-4"><label style="padding-top: 10px;">ที่อยู่/ที่ทำงาน
                    </label></div>
                    <div class="col-md-3">
                    <input type="text" class="textbox" value="{{contact.ADDRESS}}" disabled>
                    </div>
               </div>
            </div>
            
            <div class="row">
                <div class="col-md-12 form-row">
                    <div class="col-md-2"><label>จำนวนบุตร (คน)</label></div>
                    <div class="col-md-3">
                     <input type="text" class="textbox" value="{{profile.CHILDREN}}" disabled>
                    </div>
     
                </div>
            </div>
        </div>
        <div class="form-title">ข้อมูลการติดต่อ</div>
        <div class="contact_info form-section">
            <div class="row">
                <div class="col-md-12 form-row"><div class="col-md-6 title" style="font-size:22px;">ที่อยู่ปัจจุบันที่ติดต่อได้</div></div>
            </div>
            
            <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>ที่อยู่</label></div>
                    <div class="col-md-12">
                        <input type="text" class="textbox" value="{{contact.ADDRESS}}" disabled>
                    </div>
                </div>

                <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>จังหวัด</label></div>
                    <div class="col-md-12">
                       <input type="text" class="textbox" value="{{contact.PROVINCE_NAME}}" disabled>
                    </div>
                </div>
            </div>
          
            <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>เบอร์โทรศัพท์ที่สามารถติดต่อได้</label></div>
                    <div class="col-md-12">
                       <input type="text" class="textbox" value="{{contact.PHONE}}" disabled>
                    </div>
                </div>
                 <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>E-mail</label></div>
                    <div class="col-md-12">
                      <input type="text" class="textbox" value="{{contact.EMAIL}}" disabled>
                    </div>
                </div>
            </div>
            <div class="row">
               
            </div>
            <div class="row">
             <div class="col-md-12 form-row"><div class="col-md-6 title" style="font-size:22px;">  
                    บุคคลที่ติดต่อได้ในกรณีฉุกเฉิน</div></div>
             </div>
            <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>ชื่อ</label></div>
                    <div class="col-md-12">
                        <input type="text" class="textbox" value="{{contact.NAME_EMER}}" disabled> 
                    </div>
                </div>
                <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>เบอร์โทรศัพท์</label></div>
                    <div class="col-md-12">
                       <input type="text" class="textbox" value="{{contact.PHONE_EMER}}" disabled>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-title">ข้อมูลการศึกษา</div>
        <div class="education_info form-section">
            <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>การศึกษาสูงสุด</label></div>
                    <div class="col-md-12">
                        <input type="text" class="textbox" value="{{ edu_ifvalue(education[0].TOP_GRADUATE) }}" disabled>
                    </div>
                </div>
            </div>
            <!-- ส่วนบนแก้ไข -->
            <div class="row hidden-lg hidden-md"  >

                <div ng-repeat="contact in education">
                <div class="col-md-4 form-row" align="center"><br>
                 <button type="button" class="btn" id="btnMobile{{ $index+1 }}" data-toggle="collapse" 
                 data-target="#MobiletabStudy{{ $index+1 }}">ประวัติการศึกษา {{ $index+1 }} 
                 <span class="glyphicon glyphicon-triangle-bottom iconmobiledown"></span>
                 <span class='glyphicon glyphicon-triangle-top iconmobileup' 
                 style="display: none;"> </span></button>
                </div>

             <div id="MobiletabStudy{{ $index+1 }}" class="collapse"> 
                    <div class="row">
                        <div class="col-md-6 form-row">
                             <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ระดับการศึกษา</label></div>
                                <div class="col-md-12">
                                    <input type="text" class="textbox" value="{{ contact.GRADUATE_N }}" disabled>
                                </div>
                            </div>
                            <div class="col-md-12"><label class="text-left">สถาบันการศึกษา</label></div>
                            <div class="col-md-12">
                            <input type="text" class="textbox" value="{{ contact.INSTITUTION }}" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                     <div class="col-md-6 form-row">
                            <div class="col-md-12"><label>คณะ</label></div>
                            <div class="col-md-12">
                            <input type="text" class="textbox" value="{{ contact.FACULTY }}" disabled>
                            </div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="col-md-12"><label class="text-left">กลุ่มสาขาวิชา</label></div>
                            <div class="col-md-12">
                            <input type="text" class="textbox" value="{{ contact.BRANCH }}" disabled>
                            </div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-md-12 form-row">
                            <div class="col-md-12">ระยะเวลา</div> 
                     </div> 
                     </div>      
                    
                <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-md-3">
                                ตั้งแต่
                            </div>
                            <div class="col-md-9">
                                <input type="text" class="textbox" value="{{ dateFormatM(contact.DATE_START) }}" disabled> 
                            </div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="col-md-3">
                                ถึง
                            </div>
                            <div class="col-md-9">
                                <input type="text" class="textbox" value="{{ dateFormatM(contact.DATE_STOP) }}" disabled>
                            </div>
                        </div>
                </div>
                    <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-md-12"><label class="text-left">เกรดเฉลี่ย</label></div>
                            <div class="col-md-12"><input type="text" class="textbox" value="{{ contact.GPA }}" disabled></div>
                        </div>
                    </div>
                    <br>

                </div>

                </div>
            </div>   

            <style type="text/css">
                .tab-active{
                    border-bottom:none;
                }
                .tab{
                    display:none; 
                    border-bottom:none;
                }
                .active{
                    background-color: rgb(224, 224, 224);
                }
            </style> 

            <div class="col-md-12  hidden-sm hidden-xs" style="text-align: center; margin-top: 15px;">
                <div class="row">

                        <ul class="tabs-menu" style="display: inline-flex;list-style: none;" >
                            <li ng-repeat="contact in education" ng-class="$index==0? 'current':''"   class="item " id="headTabStudy{{ $index+1 }}" >
                                <a href="#">
                                    ประวัติการศึกษา {{ $index+1 }}
                                </a>
                            </li>
                        </ul>


                 <!--    <div ng-repeat="contact in education"  id="headTabStudy{{ $index+1 }}" class="col-md-4 tab-btn" ng-class="$index == 0?'active':''"  >
                    <a href="">ประวัติการศึกษา {{ $index+1 }}</a></div> -->
                </div>
            </div>
            <div claas="col-md-12">

                <div ng-repeat="contact in education"  id="tabStudy{{ $index+1 }}" class="hidden-sm hidden-xs" ng-class="$index == 0? 'tab-active' : 'tab'">
                    <br><br><br>
                    <div class="row">
                        <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>{{ $index+1 }}. ระดับการศึกษา</label></div>
                                <div class="col-md-12">
                                    <input type="text" class="textbox" value="{{ contact.GRADUATE_N }}" disabled>
                                </div>
                            </div>
                        <div class="col-md-6 form-row">
                            <div class="col-md-12"><label class="text-left">สถาบันการศึกษา</label></div>
                            <div class="col-md-12">
                            <input type="text" class="textbox" value="{{ contact.INSTITUTION }}" disabled>
                            </div>
                        </div>
                    </div>
                   
                    <div class="row">
                       
                        <div class="col-md-6 form-row">
                            <div class="col-md-12"><label>คณะ</label></div>
                            <div class="col-md-12">
                           <input type="text" class="textbox" value="{{ contact.FACULTY }}" disabled>
                            </div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="col-md-12"><label class="text-left">กลุ่มสาขาวิชา</label></div>
                            <div class="col-md-12">
                            <input type="text" class="textbox" value="{{ contact.BRANCH }}" disabled>
                            </div>
                        </div>
                    </div>
                      <div class="row">
                        <div class="col-md-12 form-row">
                            <div class="col-md-12"> ระยะเวลา</div> 
                     </div> 
                     </div>      
                    
                <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-md-3">
                                ตั้งแต่
                            </div>
                            <div class="col-md-9">
                               <input type="text" class="textbox" value="{{ dateFormatM(contact.DATE_START) }}" disabled>
                            </div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="col-md-3">
                                ถึง
                            </div>
                            <div class="col-md-9">
                                <input type="text" class="textbox" value="{{ dateFormatM(contact.DATE_STOP) }}" disabled>
                            </div>
                        </div>
                </div>
                    <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-md-12"><label class="text-left">เกรดเฉลี่ย</label></div>
                            <div class="col-md-12">
                            <input type="text" class="textbox" value="{{ contact.GPA }}" disabled>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    <div class="form-title">ข้อมูลการทำงาน</div>
        <div class="experience_info form-section">
         <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-md-9"><label>เพิ่งสำเร็จการศึกษา</label></div><br>
                    <div class="col-md-12">
                         <input type="text" class="textbox" value="{{ job[0].FINISH }}" disabled>
                    </div>
                </div>

                <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>ประสบการณ์การทำงาน (ปี)</label></div>
                    <div class="col-md-12">
                        <input type="text" class="textbox" value="{{ job[0].EXP }}" disabled>
                    </div>
                </div>
        </div>
           
            <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>สายงานล่าสุด</label></div>
                    <div class="col-md-12">
                        <input type="text" class="textbox" value="{{ job[0].LINE_LAST }}" disabled>
                    </div>
                </div>

                  <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>ตำแหน่งสุดท้าย</label></div>
                    <div class="col-md-12">
                    <input type="text" class="textbox" value="{{ job[0].LAST_POSITION }}" disabled>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>เงินเดือนสุดท้าย (บาท) </label></div>
                    <div class="col-md-12">
                    <input type="text" class="textbox" value="{{ job[0].LAST_MONEY |number:0 }}" disabled>
                     </div>
                </div>
            </div><br>
            <!-- แก้ไขส่วนบน -->

            <!-- ส่วนmobile ประวัติทำงาน -->
             <div class="row hidden-lg hidden-md">

                <div ng-repeat="contact in job">
                <div class="col-md-4 form-row" align="center">
                 <button type="button" class="btn" id="btnWorkMobile{{$index +1}}" data-toggle="collapse" 
                 data-target="#Mobiletab{{$index +1}}">ประวัติการทำงาน {{$index +1}}
                 <span class="glyphicon glyphicon-triangle-bottom iconmobiledown"></span>
                 <span class='glyphicon glyphicon-triangle-top iconmobileup' 
                 style="display: none;"> </span></button>
                </div>
                 <div id="Mobiletab{{$index +1}}" class="collapse">
                     
                    <div class="row">
                        <div class="col-md-12 form-row">
                            <div class="col-md-12">ระยะเวลา</div> 
                        </div> 
                    </div>
                    
                    <div class="row">
                         <div class="col-md-6 form-row">
                            <div class="col-md-3">ตั้งแต่</div>
                            <div class="col-md-9">
                                <input type="text" class="textbox" value="{{ dateFormatM(contact.DATE_START) }}" disabled>
                            </div>
                          </div>
                         <div class="col-md-6 form-row">
                            <div class="col-md-3">ถึง</div>
                            <div class="col-md-9">
                                <input type="text" class="textbox" value="{{ dateFormatM(contact.DATE_STOP) }}" disabled>

                            </div>        
                         </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ตำแหน่ง</label></div>
                                <div class="col-md-12">

                                    <input type="text" class="textbox" value="{{ contact.POSITION }}" disabled>
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>บริษัท</label></div>
                                <div class="col-md-12">
                                <input type="text" class="textbox" value="{{ contact.COMPANY }}" disabled>
                                </div>
                            </div>
                                         
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>เงินเดือน (บาท) </label></div>
                                <div class="col-md-12">
                                <input type="text" class="textbox" value="{{ contact.SALARY |number:0 }}" disabled>
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>รายได้อื่น ๆ (บาท) </label></div>
                                <div class="col-md-12">
                                <input type="text" class="textbox" value="{{ contact.OTHER_SALARY }}" disabled>
                                </div>
                            </div>
                     
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ระบุรายได้อื่น ๆ</label></div>
                                <div class="col-md-12">
                                <input type="text" class="textbox" value="{{ contact.MORE_OTHER_SALARY }}" disabled>
                                </div>
                            </div>
                             <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>สาเหตุที่ออก</label></div>
                                <div class="col-md-12">
                                <input type="text" class="textbox" value="{{ contact.FEATURES }}" disabled>
                                </div>
                            </div><br>
                        
                    </div>
                </div>
                </div>

        </div>
            
            <div class="col-sm-12  hidden-xs hidden-sm" style="text-align: center;">
                <div class="row">

                    <ul class="tabs-menu" style="display: inline-flex;list-style: none;">
                        <li class="item " ng-repeat="contact in job"  ng-class="$index==0? 'current':''" id="headTab{{$index +1}}">
                            <a href="#">
                                ประวัติการทำงาน {{$index +1}}
                            </a>
                        </li>
                    </ul>

                  <!--   <div ng-repeat="contact in job" id="headTab{{$index +1}}" class="col-20 tab-btn" ng-class="$index == 0?'active':''">
                    <a href="">ประวัติการทำงาน {{$index +1}}</a></div> -->
                </div>
            </div>
           
             <div ng-repeat="contact in job" id="tab{{$index +1}}" style=" border-bottom:none" class="hidden-xs hidden-sm" ng-class="$index == 0? 'tab-active' : 'tab'">
                     
                    <div class="row">
                        <div class="col-md-12 form-row">
                            <div class="col-md-12">{{$index +1}}. ระยะเวลา</div> 
                        </div> 
                    </div>
                    
                    <div class="row">
                         <div class="col-md-6 form-row">
                            <div class="col-md-3">ตั้งแต่</div>
                            <div class="col-md-9">
                                <input type="text" class="textbox" value="{{dateFormatM(contact.DATE_START) }}" disabled>
                            </div>
                          </div>
                         <div class="col-md-6 form-row">
                            <div class="col-md-3">ถึง</div>
                            <div class="col-md-9">
                                <input type="text" class="textbox" value="{{ dateFormatM(contact.DATE_STOP) }}" disabled>

                            </div>        
                         </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ตำแหน่ง</label></div>
                                <div class="col-md-12">
                                    <input type="text" class="textbox" value="{{ contact.POSITION }}" disabled>
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>บริษัท</label></div>
                                <div class="col-md-12">
                               <input type="text" class="textbox" value="{{ contact.COMPANY }}" disabled>
                                </div>
                            </div>
                                         
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>เงินเดือน (บาท) </label></div>
                                <div class="col-md-12">
                                <input type="text" class="textbox" value="{{ contact.SALARY |number:0 }}" disabled>
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>รายได้อื่น ๆ (บาท) </label></div>
                                <div class="col-md-12">
                                <input type="text" class="textbox" value="{{ contact.OTHER_SALARY }}" disabled>
                                </div>
                            </div>
                     
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ระบุรายได้อื่น ๆ</label></div>
                                <div class="col-md-12">
                                <input type="text" class="textbox" value="{{ contact.MORE_OTHER_SALARY }}" disabled>
                                </div>
                            </div>
                             <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ลักษณะงานที่ทำ</label></div>
                                <div class="col-md-12">
                                <input type="text" class="textbox" value="{{ contact.FEATURES }}" disabled>
                                </div>
                            </div>
                        
                    </div>
                </div>

            </div>
            
            <div class="">
            <div class="form-title">ความสามารถด้านภาษา</div>
            <div class="language_form form-section">
                <div class="row hide-mobile"  >
                    <div class="row form-row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-2">ภาษา</div>
                        <div class="col-md-10 col-sm-offset-1"></div>
                    </div>
                   
                     <div class="col-md-6 form-row">
                        <div class="col-md-4">การพูด</div>
                        <div class="col-md-4">การเขียน</div>
                        <div class="col-md-4">การอ่าน</div>
                    </div>
                     </div>
                </div>
           
                <div class="row" ng-repeat="contact in language">
                    <div class="row form-row">
                    <div class="col-md-6">
                        <div class="show-mobile col-md-12"><span>ภาษา</span></div>
                        <div class="col-md-1">{{$index+1}}.</div>
                        <div class="col-md-11">
                        <input type="text" class="textbox" value="{{ contact.LANGUAGE }}" disabled></div> 
                    </div>

                    <div class="col-md-6 form-row">
                        <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การพูด</label></div>
                        <div class="col-md-4">
                            <input type="text" class="textbox" value="{{ contact.TALK }}" disabled>
                        </div>
                        <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การเขียน</label></div>
                        <div class="col-md-4">
                            <input type="text" class="textbox" value="{{ contact.WRITE }}" disabled>
                        </div>
                        <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การอ่าน</label></div>
                        <div class="col-md-4">
                            <input type="text" class="textbox" value="{{ contact.READS }}" disabled>
                        </div>
                    </div>
                   </div>
                </div>
  
                <div class="row">
                 <div class="row form-row">
                    <div class="col-md-6 col-xs-12 form-row">
                        <div class="col-md-2 col-sm-2 col-xs-12"> <label>พิมพ์ดีด </label></div>
                        <div class="col-md-4 col-sm-4 col-xs-6">
                             <input type="text" class="textbox" value="{{profile.PRINT_TH}}" disabled> &nbsp; ไทย
                        </div>
                        <div class="col-md-4 col-sm-5 col-xs-6">
                             <input type="text" class="textbox" value="{{profile.PRINT_EN}}" disabled> &nbsp; อังกฤษ
                        </div>
                    </div>
                  </div>
                </div>
            </div>

                <div class="form-title">ความสามารถทางคอมพิวเตอร์</div>
                <div class="form-section">
                    <textarea name="txbFurtherInfo" id="txbFurtherInfo" class="w100-m"   style="height:150px;">{{profile.MOREDATA}}</textarea>
                </div>


            <div class="form-title">ลักษณะงานที่สนใจ</div>
            <div class="job_interesting form-section">        
                <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>ตำแหน่งงาน</label></div>
                        <div class="col-md-12">
                            <input type="text" class="textbox" value="{{job_interested.POSITION}}" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>ประเภทงาน</label></div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" class="textbox" value="{{job_interested.TYPE_JOB == 'F'?'Full Time':'Part Time'}}" disabled>
                        </div>
                        
                </div>
               
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>เงินเดือนที่คาดหวัง (บาท)</label>
                        </div>
                        <div class="col-md-12">
                            <input type="text" class="textbox" value="{{job_interested.SALARY_HOPE  |number:0}}" disabled>  
                        </div>
                    </div>
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>พร้อมเริ่มงานตั้งแต่วันที่</label></div>
                        <div class="col-md-12 form-group has-feedback">
                           <input type="text" class="textbox" value="{{MydateFormat(job_interested.START_DAY)}}" disabled>  
                        </div>
                    </div>
           
            </div>
            <div class="form-title">บุคคลอ้างอิง (ไม่ใช่ญาติ หรือคนในครอบครัว)</div>
            <div class="ref_person form-section">
                <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>ชื่อ-นามสกุล</label></div>
                        <div class="col-md-12">
                        <input type="text" class="textbox" value="{{person.NAME}}" disabled>  
                        </div>
                    </div>
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>ตำแหน่ง</label></div>
                        <div class="col-md-12">
                        <input type="text" class="textbox" value="{{person.POSITION}}" disabled>  
                        </div>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>บริษัท</label></div>
                        <div class="col-md-12">
                        <input type="text" class="textbox" value="{{person.COMPANY}}" disabled>  
                        </div>
                    </div>
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>ความสัมพันธ์</label></div>
                        <div class="col-md-12">
                        <input type="text" class="textbox" value="{{person.RELATIONSHIP}}" disabled>  
                        </div>
                    </div>
                </div>
                <div class="row">                    
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>เบอร์โทรติดต่อ</label></div>
                        <div class="col-md-12">
                        <input type="text" class="textbox" value="{{person.TELPERSON}}" disabled>  
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-title">ข้อมูลทั่วไป</div>
            <div class="common_info form-section">
                <div class="row">
                    <div class="col-md-12 form-row">
                        <div class="col-md-12">
                            <span>การไปปฏิบัติงาน ณ โครงการหมู่บ้านท่านขัดข้องหรือไม่</span>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12 form-row">
                        <div class="col-md-3"><label>เป็นประจำ</label></div>
                            <div class="col-md-9">
                                <input type="text" class="textbox" value="{{ datas.WORKING_ALWAYS == 'Y'?'ไม่ขัดข้อง':'ขัดข้อง: '+datas.WORKING_ALWAYS.split('N')[1] }}" disabled> 
                            </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 form-row">
                        <div class="col-md-3"><label>เป็นครั้งคราว</label></div>
                            <div class="col-md-9">
                                <input type="text" class="textbox" value="{{ datas.WORKING_SOMETIMES == 'Y'?'ไม่ขัดข้อง':'ขัดข้อง: '+datas.WORKING_SOMETIMES.split('N')[1] }}" disabled>
                            </div>
                            
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 form-row">
                        <div class="col-md-3"><label>ท่านมีโรคเเรื้อรัง/โรคประจำตัวหรือไม่</label></div>
                            <div class="col-md-9">
                                <input type="text" class="textbox" value="{{ datas.DISEASE == 'N'?'ไม่มี':'มี'+datas.DISEASE.split('Y')[1] }}" disabled>
                            </div>
                           
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 form-row">
                        <div class="col-md-3"><label>ท่านเคยต้องโทษทางคดีอาญาหรือคดีแพ่งหรือไม่</label></div>
                            <div class="col-md-9">
                                <input type="text" class="textbox" value="{{ datas.CRIME == 'N'?'ไม่เคย':'เคย: '+datas.CRIME.split('Y')[1] }}" disabled>
                            </div>
                            
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 form-row">
                        <div class="col-md-3"><label>ท่ายเคยถูกศาลสั่งให้เป็นบุคคล้มละลาย หรือถูกศาลสั่งพิทักษ์ทรัพย์ 
                        หรือมีหนี้สินล้นพ้นตัวหรือไม่</label></div>
                            <div class="col-md-9">
                                 <input type="text" class="textbox" value="{{ datas.BANKRUPT == 'N'?'ไม่เคย':'เคย: '+datas.BANKRUPT.split('Y')[1] }}" disabled>
                            </div>
                           
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 form-row">
                        <div class="col-md-3"><label>ท่ายเคยมีประวัติถูกเลิกจ้างเพราะเหตุกระทำผิดมาก่อนหรือไม่</label></div>
                            <div class="col-md-9">
                                <input type="text" class="textbox" value="{{ datas.LAY_OFF == 'N'?'ไม่เคย':'เคย: '+datas.LAY_OFF.split('Y')[1] }}" disabled>
                            </div>
                            
                    </div>
                </div>
            </div>
            <div class="form-title">ข้อมูลเพิ่มเติม</div>
            <div class="form-section">
                <textarea name="txbFurtherInfo" id="txbFurtherInfo" class="w100-m"   style="height:150px;">{{profile.MOREDATA}}</textarea>
            </div>
            <div class="form-title">เอกสารแนบ</div>
            <div class="document_form">
             
            <div ng-if="profile.ATTACHMENT_FILE" >
            <label style="font-size: 25px;">แนบ Resume</label><br>
               <a href="http://www.lh.co.th/www/Backend/fileupload/candidacy/{{profile.ATTACHMENT_FILE}}" download> คลิกดูไฟล์ resume </a>
            </div>

            <div ng-if="profile.ATTACHMENTFILE_IMAGE" >
                <label style="font-size: 25px;">แนบรูป</label> <br>
               <a href="http://www.lh.co.th/www/Backend/fileupload/candidacy/{{profile.ATTACHMENTFILE_IMAGE}}" download> คลิกดูไฟล์ รูป </a>
            </div>
               
            </div>
            
            <div class="row">
                    <div class="form-row wrap-btns">

                        <a class="btn"  href="./candidacy1.php" style="background-color: #E1E1E1" 
                         >ย้อนกลับ</a>

                      <a class="btn btn-danger confirms"  data-title="ยืนยันการลบข้อมูล" >Delete</a>

                        <a class="btn btn-primary"  href="./LHPrint.php?id=<?php echo $_GET['id']?>" style="background-color: #67ae67" target="_blank"
                        >ปริ้น</a>

                    </div>
            </div>
    </form>




                        </div>
                    </div>
                </div>
            </div>           
        </div>
    </div>
</div>


            

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>






<script>
  setTimeout(function(){
// tab การศึกษา การทำงาน
    $('#headTabStudy1 a').click(function (e) {
            
            e.preventDefault()
             $('#headTabStudy1').addClass( "current" )
             $('#headTabStudy2').removeClass( "current" )
             $('#headTabStudy3').removeClass( "current" )
             $('#tabStudy1').show();
             $('#tabStudy2').hide();
             $('#tabStudy3').hide();
    })
    $('#headTabStudy2 a').click(function (e) {
            e.preventDefault()
             $('#headTabStudy2').addClass( "current" )
             $('#headTabStudy1').removeClass( "current" )
             $('#headTabStudy3').removeClass( "current" )
             $('#tabStudy2').show();
             $('#tabStudy1').hide();
             $('#tabStudy3').hide();
    })
    $('#headTabStudy3 a').click(function (e) {
            e.preventDefault()
             $('#headTabStudy3').addClass( "current" )
             $('#headTabStudy1').removeClass( "current" )
             $('#headTabStudy2').removeClass( "current" )
             $('#tabStudy3').show();
             $('#tabStudy1').hide();
             $('#tabStudy2').hide();
    })

     $('#headTab1 a').click(function (e) {
            
            e.preventDefault()
             $('#headTab1').addClass( "current" )
             $('#headTab2').removeClass( "current" )
             $('#headTab3').removeClass( "current" )
             $('#headTab4').removeClass( "current" )
             $('#headTab5').removeClass( "current" )
             $('#tab1').show();
             $('#tab2').hide();
             $('#tab3').hide();
             $('#tab4').hide();
             $('#tab5').hide();
    })
    $('#headTab2 a').click(function (e) {
            e.preventDefault()
             $('#headTab2').addClass( "current" )
             $('#headTab1').removeClass( "current" )
             $('#headTab3').removeClass( "current" )
             $('#headTab4').removeClass( "current" )
             $('#headTab5').removeClass( "current" )
             $('#tab2').show();
             $('#tab1').hide();
             $('#tab3').hide();
             $('#tab4').hide();
             $('#tab5').hide();
    })
    $('#headTab3 a').click(function (e) {
            e.preventDefault()
             $('#headTab3').addClass( "current" )
             $('#headTab1').removeClass( "current" )
             $('#headTab2').removeClass( "current" )
             $('#headTab4').removeClass( "current" )
             $('#headTab5').removeClass( "current" )
             $('#tab3').show();
             $('#tab1').hide();
             $('#tab2').hide();
             $('#tab4').hide();
             $('#tab5').hide();
    })
     $('#headTab4 a').click(function (e) {
            e.preventDefault()
             $('#headTab4').addClass( "current" )
             $('#headTab1').removeClass( "current" )
             $('#headTab2').removeClass( "current" )
             $('#headTab3').removeClass( "current" )
             $('#headTab5').removeClass( "current" )
             $('#tab4').show();
             $('#tab1').hide();
             $('#tab2').hide();
             $('#tab3').hide();
             $('#tab5').hide();
    })
      $('#headTab5 a').click(function (e) {
            e.preventDefault()
             $('#headTab5').addClass( "current" )
             $('#headTab1').removeClass( "current" )
             $('#headTab2').removeClass( "current" )
             $('#headTab3').removeClass( "current" )
             $('#headTab4').removeClass( "current" )
             $('#tab5').show();
             $('#tab1').hide();
             $('#tab2').hide();
             $('#tab3').hide();
             $('#tab4').hide();
    })

    // ประวัติการศึกษาmobile
  
  
        $('.btn').bind("click", function () {
            $(this).find('.iconmobiledown').hide();
            $(this).find('.iconmobileup').show();
            $(this).css('background-color','#6f9755');
            $(this).css('color','white');
            $(this).css('font-size','25px');
 
            if ($(this).attr('aria-expanded') == "true") {
                $(this).css('background-color','white');
                $(this).css('color','#6f9755');
                $(this).css('font-size','25px');
                $(this).find('.iconmobileup').hide();
                $(this).find('.iconmobiledown').show();
            }
        })

    },1000);

</script>
               

</body>
</html>