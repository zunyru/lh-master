$(document).ready(function () {

    //Page Load Start
    selectArea();
    selectProvince();
    tabSlide();

    $('#formJob, #formJob2, #formJob3, #formJob4, #formJob5, #formJob6').click(function() {
        $('.tab-content .current').removeClass('current').hide()
            .next().show().addClass('current');
        if ($('.tab-content .current').hasClass('last')) {
            $('#formJob, #formJob2, #formJob3, #formJob4, #formJob5, #formJob6').attr('disabled', true);
        }
        $('#backJobLists, #backJobListsBtm').attr('disabled', null);
        $('#choose-location').hide();
    });
    $('#backJobLists, #backJobListsBtm').click(function() {
        $('.tab-content .current').removeClass('current').hide()
            .prev().show().addClass('current');
        if ($('.tab-content .current').hasClass('first')) {
            $('#backJobLists, #backJobListsBtm').attr('disabled', true);
        }
        $('#formJob, #formJob2, #formJob3, #formJob4, #formJob5, #formJob6').attr('disabled', null);
        $('#choose-location').show();
    });

    $('#dateBirth').datepicker();
    $('#dateWork').datepicker();
    //Page Load End
});


function tabSlide() {
    var winW = $(window).width();

    // responsive
    if( winW < 768 ) {
        $('.tabs-menu').owlCarousel({
            loop:true,
            margin: 0,
            nav: true,
            dots: false,
            responsive:{
                0:{
                    items:2
                }
            }
        });

        $('.tabs-menu a').click(function(e) {
            e.preventDefault();
            $(this).parent().addClass('current');
            $('.tabs-menu li').removeClass('current');
        });
    }

    $('.tabs-menu a').click(function(e) {
        e.preventDefault();
        $(this).parent().addClass('current');
        $('.tabs-menu li').removeClass('current');
        if($(this).attr('href') != '#tab-1'){
            $('#choose-location').hide();
        }else{
            $('#choose-location').show();
        }
    });


}


//Function Start
function selectProvince() {
    $('.select-block.selectProvince .sort').click(function(e){
        e.stopImmediatePropagation();
        var status = $(this).data('status');
        if( status == 0 ) {
            $('.select-block.selectProvince ul').fadeIn(200);
            $(this).data('status', 1);
        }
        else if( status == 1 ) {
            $('.select-block.selectProvince ul').fadeOut(200);
            $(this).data('status', 0);
        }

        $('.select-block.selectArea ul').fadeOut(200);
        $('.select-block.selectArea .sort').data('status', 0);
    });

    $('.select-block.selectProvince ul li').click(function(e){
        var value = $(this).data('value');
        $('.select-block.selectProvince .sort span').text(value);
        $('input[name="listsProvince"]').val(value);

        $('.select-block.selectProvince ul').fadeOut(200);
        $('.select-block.selectProvince .sort').data('status', 0);
    });

    $(document).click(function(e) {
        var target = $(e.target);
        var sortLists = $('#listsProvince');
        if(!(target.is(sortLists) || sortLists.find(target).length )) {
            sortLists.fadeOut(200);
            $('.select-block.selectProvince .sort').data('status', 0);
        }
    });
}

function selectArea() {
    $('.select-block.selectArea .sort').click(function(e){
        e.stopImmediatePropagation();
        var status = $(this).data('status');
        if( status == 0 ) {
            $('.select-block.selectArea ul').fadeIn(200);
            $(this).data('status', 1);
        }
        else if( status == 1 ) {
            $('.select-block.selectArea ul').fadeOut(200);
            $(this).data('status', 0);
        }
        $('.select-block.selectProvince ul').fadeOut(200);
        $('.select-block.selectProvince .sort').data('status', 0);
    });

    $('.select-block.selectArea ul li').click(function(e){
        var value = $(this).data('value');
        $('.select-block.selectArea .sort span').text(value);
        $('input[name="listsArea"]').val(value);

        $('.select-block.selectArea ul').fadeOut(200);
        $('.select-block.selectArea .sort').data('status', 0);
    });

    $(document).click(function(e) {
        var target = $(e.target);
        var sortLists = $('#listsArea');
        if(!(target.is(sortLists) || sortLists.find(target).length )) {
            sortLists.fadeOut(200);
            $('.select-block.selectArea .sort').data('status', 0);
        }
    });
}


//Function End