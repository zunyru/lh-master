var path = window.location.origin+'/www/';

angular.module('services', [])

/** ===================================================================
 * The `data Service` service allows for messages 
 *
 * @param {*} message Message to be logged.
 */
.service('Service', function ($http) {

    //simply returns the data list
    this.list = function (id,contro) {
        
        var url=path+"build/api/service/register_be.php?n="+contro+"&id="+id; 
        return $http({
          method: 'GET',
          url: url
        }).then(function successCallback(response) {
            if(response.data[0].message == 'unsuccess'){
                return []
            }else{
                return response.data
            }
        }, function errorCallback(response) {
            console.log(response)
        });   
    }

    //save method create a new contact if not already exists
    //else update the existing object
    this.save = function (d,contro) {
        var url=path+"build/api/service/register_be.php?n="+contro; 
 
        return $http({
            url: url,
            method: "POST",
            data: $.param(d),
            headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
        })
        .then(function(response) {
             return response.data
        }, 
        function(response) { // optional
           console.log(response) 
        });    

    }

    //edit method create a new contact if not already exists
    //else update the existing object
    this.edit = function (d,contro) {
        var url=path+"build/api/service/register_be.php?n="+contro; 
 
        return $http({
            url: url,
            method: "POST",
            data: $.param(d),
            headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
        })
        .then(function(response) {
             return response.data
        }, 
        function(response) { // optional
           console.log(response) 
        });    

    }

    //delete method create a new contact if not already exists
    //else update the existing object
    this.del = function (d) {
        var url=path+"build/api/service/register_be.php?n=delete"; 
 
        return $http({
            url: url,
            method: "POST",
            data: $.param(d),
            headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
        })
        .then(function(response) {
             return response.data
        }, 
        function(response) { // optional
           console.log(response) 
        });    

    }


    //delete method create a new contact if not already exists
    //else update the existing object
    this.del_appltcant = function (d) {
        var url=path+"build/api/service/register_be.php?n=deleteAppltcant "; 
 
        return $http({
            url: url,
            method: "POST",
            data: $.param(d),
            headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
        })
        .then(function(response) {
             return response.data
        }, 
        function(response) { // optional
           console.log(response) 
        });    

    }

    //simply returns the data select option postion
    this.selPosition = function () {
        var url=path+"build/api/service/register_be.php?n=position"; 

        return $http({
          method: 'GET',
          url: url
        }).then(function successCallback(response) {
             return response.data
        }, function errorCallback(response) {
            console.log(response)
        });
    }

    //simply returns the data select option province
    this.selProvince = function () {
        var url=path+"build/api/service/register_be.php?n=prvinces"; 

        return $http({
          method: 'GET',
          url: url
        }).then(function successCallback(response) {
             return response.data
        }, function errorCallback(response) {
            console.log(response)
        });
    }

    //simply returns the data select option area
    this.selArea = function () {
        var url=path+"build/api/service/register_be.php?n=location"; 

        return $http({
          method: 'GET',
          url: url
        }).then(function successCallback(response) {
             return response.data
        }, function errorCallback(response) {
            console.log(response)
        });
    }

    //simply returns the data select option area
    this.selStatus = function () {
        var url=path+"build/api/service/register_be.php?n=comment"; 

        return $http({
          method: 'GET',
          url: url
        }).then(function successCallback(response) {
             return response.data
        }, function errorCallback(response) {
            console.log(response)
        });
    }

})