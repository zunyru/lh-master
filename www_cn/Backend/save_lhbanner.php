<?php
/**
 * @author jacky
 * @createdate May 11 2017
 **/
if (! isset ( $_SESSION )) {
	session_start ();
}

$group_id = $_SESSION ['group_id'];
$group_name=$_SESSION['group_name'];
$action=isset ( $_POST['action'])?$_POST['action']:'';

include './include/dbCon_mssql.php';
require_once './include/function_query.php';
require_once './include/BannerHelper.php';
date_default_timezone_set ( 'Asia/Bangkok' );
//die(var_dump($_FILES));
if($action=='ADD'){
	$pageType = isset ( $_POST ['PageTypeId'] ) ? $_POST ['PageTypeId'] : '';
	$startdate = isset ( $_POST ['startdate'] ) ? $_POST ['startdate'] : '';
	$enddate = isset ( $_POST ['enddate'] ) ? $_POST ['enddate'] : '';
	$date = new DateTime ();

	$original_date = date("d");
	$original_month = date("m");
	$original_year = date("Y")+10;

	$date_now = date('Y-m-d');
	$date_new = $original_year."-".$original_month."-".$original_date;

	$today =date('Y-m-d H:i:s'); 
	if (! empty ( $startdate ) && ! empty ( $enddate )) {
		$startdate = DateTime::createFromFormat ( 'd/m/Y', $startdate );
		$enddate = DateTime::createFromFormat ( 'd/m/Y', $enddate );
        // startdate->format("Y-m-d")
		$startdate = $startdate->format('Y-m-d');
		$enddate = $enddate->format('Y-m-d');

	}



     //leade default PC
	if(isset($_FILES['lead_image_d'])){
		$numSize = count($_FILES['lead_image_d']["name"]);
		$date_name = date('dmY');
		if($numSize > 0){
			
            //save Lead Banner default
			$sql = "Insert into LH_BANNER_MAIN (type_page_id,type_show_id,lead_img_mobile,createby,updateby,startdate,enddate)
			VALUES ('$pageType','0','d','$group_id','$group_id','$date_now','$date_new')";
			exeQuery ( $sql );
		    // get banner_main_id Default
			$sql_get_id ="SELECT banner_main_id FROM LH_BANNER_MAIN WHERE type_page_id = '$pageType' AND lead_img_mobile = 'd'";
			$query = mssql_query($sql_get_id);
			$row = mssql_fetch_array($query);
			$banner_main_id = $row['banner_main_id'];
		    //save Lead Banner lead
			if($pageType == 1){
				$p = 'h';
			}else{
				$p = 'p';
			}
			$sql = "Insert into LH_BANNER_MAIN (type_page_id,type_show_id,lead_img_mobile,createby,updateby,startdate,enddate)
			VALUES ('$pageType','0','$p','$group_id','$group_id','$startdate','$enddate')";
			exeQuery ( $sql );

			for ($i = 0; $i < $numSize; $i++) {
			    $seo = $_POST['lead_image_d_seo'][$i];
			    $url = $_POST['lead_image_d_url'][$i]; 
			         
				$type_img = strrchr($_FILES["lead_image_d"]["name"][$i],".");
				$picname = "BD_". mt_rand ().'_'.$date_name.$type_img;
				$pathName = "fileupload/banner_file/" . $picname ;
				$fname = "fileupload/banner_file/Thumbnails_" . $picname;

				if (move_uploaded_file($_FILES["lead_image_d"]["tmp_name"][$i], $pathName)) {
					$sql = "INSERT INTO LH_BANNER_LEAD (banner_main_id,lead_path,lead_img_mobile,seq_leade_img,seo_lead,img_url)  ";
					$sql .= "  values ($banner_main_id,'$pathName','d',$i,N'$seo','$url')";
					exeQuery($sql);
					BannerHelper::genFacebookImg($pathName,$fname);
				}
			}
		}
	}

    //Lead default MB
	if(isset($_FILES['lead_image_mb_d'])){
		$date_name = date('dmY');
		$numSize = count($_FILES['lead_image_mb_d']["name"]);
		for ($i = 0; $i < $numSize; $i++) {
			$seo = $_POST['lead_image_mb_seo'][$i];
			$url_mb = $_POST['lead_image_mb_d_url'][$i];
			$image = $_FILES["lead_image_mb_d"]["name"][$i];
			$type_img = strrchr($_FILES["lead_image_mb_d"]["name"][$i],".");
			$picname = "BDM_". mt_rand ().'_' .$date_name. $type_img;
			$pathName = "fileupload/banner_file/" . $picname ;
			$fname = "fileupload/banner_file/Thumbnails_" . $picname;

			if (move_uploaded_file($_FILES["lead_image_mb_d"]["tmp_name"][$i], $pathName)) {
				$sql = "INSERT INTO LH_BANNER_LEAD (banner_main_id,lead_path,lead_img_mobile,seq_leade_img,seo_lead,img_url)  ";
				$sql .= "  values ($banner_main_id,'$pathName','m',$i,'$seo','$url_mb')";
				exeQuery($sql);
				BannerHelper::genFacebookImg($pathName,$fname);
			}
		}
	} 


    //------------------------------------------  Add Bannner Laed -----------------------------------------//
	$banner=isset($_POST['banner'])?$_POST['banner']:'';

	if($banner == 'on'){
		$bannerChoose = isset ( $_POST ['bannerchoose'] ) ? $_POST ['bannerchoose'] : '';
    		// Add Banner Lead iamge PC 
		if($bannerChoose == 1){

               //PC 
			if(isset($_FILES['lead_image_p'])){
				$numSize = count($_FILES['lead_image_p']["name"]);
                $date_name = date('dmY');
               	// get banner_main_id Default
				$sql_get_id ="SELECT banner_main_id FROM LH_BANNER_MAIN WHERE type_page_id = '$pageType' AND lead_img_mobile != 'd'";
				$query = mssql_query($sql_get_id);
				$row = mssql_fetch_array($query);
				$banner_main_id_Lead = $row['banner_main_id'];

				for ($i = 0; $i < $numSize; $i++) {

					$seo_p = $_POST['lead_image_p_seo'][$i];
					$url_p = $_POST['lead_image_p_url'][$i];

					$type_img = strrchr($_FILES["lead_image_p"]["name"][$i],".");
					$picname = "BL_". mt_rand ().'_' .$date_name .$type_img;
					$pathName = "fileupload/banner_file/" . $picname ;
					$fname = "fileupload/banner_file/Thumbnails_" . $picname;

					if (move_uploaded_file($_FILES["lead_image_p"]["tmp_name"][$i], $pathName)) {
						$sql = "INSERT INTO LH_BANNER_LEAD (banner_main_id,lead_path,lead_img_mobile,seq_leade_img,seo_lead,img_url)  ";
						$sql .= "  values ($banner_main_id_Lead,'$pathName','p',$i,N'$seo_p','$url_p')";
						exeQuery($sql);
						BannerHelper::genFacebookImg($pathName,$fname);
					}
				}
			} 

               //MB
			if(isset($_FILES['lead_image_p_m'])){
				$numSize = count($_FILES['lead_image_p_m']["name"]);
                $date_name = date('dmY');
               	// get banner_main_id Default
				$sql_get_id ="SELECT banner_main_id FROM LH_BANNER_MAIN WHERE type_page_id = '$pageType' AND lead_img_mobile != 'd'";
				$query = mssql_query($sql_get_id);
				$row = mssql_fetch_array($query);
				$banner_main_id_Lead = $row['banner_main_id'];

				for ($i = 0; $i < $numSize; $i++) {

					$seo_pm = $_POST['lead_image_p_m_seo'][$i];
					$url_pm = $_POST['lead_image_p_m_url'][$i];

					$type_img = strrchr($_FILES["lead_image_p_m"]["name"][$i],".");
					$picname = "BLM_". mt_rand ().'_' .$date_name .$type_img ;
					$pathName = "fileupload/banner_file/" . $picname ;
					$fname = "fileupload/banner_file/Thumbnails_" . $picname;

					if (move_uploaded_file($_FILES["lead_image_p_m"]["tmp_name"][$i], $pathName)) {
						 $sql = "INSERT INTO LH_BANNER_LEAD (banner_main_id,lead_path,lead_img_mobile,seq_leade_img,seo_lead,img_url)  ";
						$sql .= "  values ($banner_main_id_Lead,'$pathName','m',$i,N'$seo_pm','$url_pm')";
						exeQuery($sql);
						BannerHelper::genFacebookImg($pathName,$fname);
					}
				}
			} 

    		}// END $bannerChoose ==1	
    		else if ($bannerChoose == 2) {
    			$color = isset ( $_POST ['activitycolor'] ) ? $_POST ['activitycolor'] : '';
    			$editTh = isset ( $_POST ['editTh'] ) ? $_POST ['editTh'] : '';
    			$editEn = isset ( $_POST ['editEn'] ) ? $_POST ['editEn'] : '';
    			$seo = isset ( $_POST ['activityimg_seo'][0] ) ? $_POST ['activityimg_seo'][0] : '';
    			if (! empty ( $_FILES ['activityimg'] )) {
    				$picname = "AV_". mt_rand ();
    				$savePath = "fileupload/banner_file/" . $picname . ".jpg";
    				if (move_uploaded_file ( $_FILES ['activityimg']["tmp_name"], $savePath )) {
    					echo $sql="UPDATE  LH_BANNER_MAIN SET
    					type_show_id = '1',
    					lead_img_mobile = 'p',
    					pic_activity = '$savePath',
    					color_code_activity ='$color',
    					text_activity_th =N'".mssql_escape($editTh)."',
    					text_activity_en ='".mssql_escape($editEn)."',
    					createby ='$group_id',
    					updateby = '$group_id',
    					startdate ='$startdate',
    					seo_main = N'$seo',
    					enddate ='$enddate'
    					WHERE type_page_id = '$pageType' AND lead_img_mobile != 'd'";  
    					exeQuery ( $sql );
    				}
    			}
    		}// END END $bannerChoose ==2
    		else if ($bannerChoose == 3) {
    			$upvido = isset ( $_POST ['upvido'] ) ? $_POST ['upvido'] : '';
    			if(!empty($upvido)){
    				if($upvido=='youtube'){
    				   	   if (! empty ( $_FILES ['thumbnailYoutube'] )) { //url_youtube,thumbnailYoutube
    				   	   	$seo = isset ( $_POST ['thumbnailYoutube_seo'][0] ) ? $_POST ['thumbnailYoutube_seo'][0] : '';
    				   	   	$urlyoutube= isset ( $_POST ['url_youtube'] ) ? $_POST ['url_youtube'] : '';
    				   	   	$picname = "TY_". mt_rand ();
    				   	   	$savePath = "fileupload/banner_file/" . $picname . ".jpg";
    				   	   	if (move_uploaded_file ( $_FILES ["thumbnailYoutube"] ["tmp_name"], $savePath )) {
    				   	   		$sql = "UPDATE  LH_BANNER_MAIN SET
    				   	   		type_show_id = '3',
    				   	   		lead_img_mobile = 'p',
    				   	   		url_vdo = '$urlyoutube',
    				   	   		thumnail_path = '$savePath',
    				   	   		pic_activity = NULL,
    				   	   		color_code_activity = NULL,
    				   	   		text_activity_th = NULL,
    				   	   		text_activity_en = NULL,
    				   	   		createby ='$group_id',
    				   	   		updateby = '$group_id',
    				   	   		startdate ='$startdate',
    				   	   		seo_main = N'$seo',
    				   	   		enddate ='$enddate'
    				   	   		WHERE type_page_id = '$pageType' AND lead_img_mobile != 'd'";
    				   	   		exeQuery ( $sql );
    				   	   	}
    				   	   }
    				   	}else if($upvido=='vdo'){
    				   		if (! empty ( $_FILES ['vdofile'] ) && ! empty ( $_FILES ['thumbnailVDO'] )) {
    				   			$seo = isset ( $_POST ['thumbnailVDO_seo'][0] ) ? $_POST ['thumbnailVDO_seo'][0] : '';
    				   	        	//video
    				   			$videoname = $group_name."VDO_". mt_rand ();
    				   			$savePath = "fileupload/banner_file/" . $videoname . ".mp4";
    				   	        	//image
    				   			$picname = "TV_". mt_rand ();
    				   			$savePic = "fileupload/banner_file/" . $picname . ".jpg";
    				   			if (move_uploaded_file ( $_FILES ["vdofile"] ["tmp_name"], $savePath ) &&
    				   				move_uploaded_file ( $_FILES ["thumbnailVDO"] ["tmp_name"], $savePic )	) {

    				   				$sql = "UPDATE  LH_BANNER_MAIN SET
    				   			type_show_id = '2',
    				   			lead_img_mobile = 'p',
    				   			pic_activity = NULL,
    				   			url_vdo= '$savePath',
    				   			thumnail_path ='$savePic',
    				   			color_code_activity = NULL,
    				   			text_activity_th = NULL,
    				   			text_activity_en = NULL,
    				   			createby ='$group_id',
    				   			updateby = '$group_id',
    				   			startdate ='$startdate',
    				   			seo_main = N'$seo',
    				   			enddate ='$enddate'
    				   			WHERE type_page_id = '$pageType' AND lead_img_mobile != 'd'";

    				   			exeQuery ( $sql );
    				   		}

    				   	}
    				   }
    				}
    			}
    		}
    	}

    	header("Location:lhbanner.php?saveAdd=success");
        exit;

?>











