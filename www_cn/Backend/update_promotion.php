<?php

session_start();
include './include/dbCon_mssql.php';
date_default_timezone_set('Asia/Bangkok');
$_SESSION['group_id'];



$promotion_name_th = mssql_escape($_POST['promotion_name_th']);
$promotion_name_en = mssql_escape($_POST['promotion_name_en']);
$promotion_detail_th = mssql_escape($_POST['promotion_detail_th']);
$promotion_detail_en = mssql_escape($_POST['promotion_detail_en']);
$project_id = $_POST['project_id'];
$iCheck = $_POST['iCheck'];
$date_start = $_POST['date_start'];
$date = $_POST['date'];
$promotion_url = $_POST['promotion_url'];
$id = $_POST['id'];
$approve1 = $_POST['approve1'];
//echo $test =str_replace(" ","-","Hello world!",$var);
//http://devwww2.lh.co.th/Land_and_house/Backend/tset-project
//Frontend/project-info-home.php

$dates = $date_start;
$year = substr($dates, 6, 4);
$day = substr($dates, 0, 2);
$mont = substr($dates, 3, 2);

$date_end = $date;
$mont2 = substr($date_end, 3, 2);
$day2 = substr($date_end, 0, 2);
$year2 = substr($date_end, 6, 4);

$start_date = $year . "-" . $mont . "-" . $day;
$end_date = $year2 . "-" . $mont2 . "-" . $day2;

function mssql_escape($str) {
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return str_replace("'", "''", $str);
}

if (isset($_POST['delete'])) {
    $sql1 = "DELETE LH_PROMOTION "
            . " WHERE promotion_id = '$id' ";
    $query = mssql_query($sql1, $db_conn);

    $sql2 = "DELETE LH_IMG_PROMOTION "
            . " WHERE promotion_id = '$id' ";
    $query = mssql_query($sql2, $db_conn);
    header("location:promotion.php?delete=d");
    exit();
}

$dates        = date("Y-m-d H:i:s");


if (isset($_POST['approve'])) {

    if($approve1 == "Y"){
       $approve = "Y";
    }else{
       $approve = "N";
    }

    $query1 = "SELECT project_id FROM LH_PROMOTION WHERE promotion_id = '$id' ";
    $query_result1 = mssql_query($query1, $db_conn);
    $chk_id = mssql_fetch_row($query_result1);
    $chk_id[0];

    // case update same project
    if ($chk_id[0] == "$project_id") {
        $query1 = "SELECT promotion_start_date FROM LH_PROMOTION WHERE promotion_id = '$id' ";
        $query_result1 = mssql_query($query1, $db_conn);
        $promotion_sdate_up = mssql_fetch_row($query_result1);
        $promotion_sdate_up[0];

        $query2 = "SELECT promotion_end_date FROM LH_PROMOTION WHERE promotion_id = '$id' ";
        $query_result2 = mssql_query($query2, $db_conn);
        $promotion_edate_up = mssql_fetch_row($query_result2);
        $promotion_edate_up[0];

        // update same project (change content && not change date)
        if ($promotion_sdate_up[0] == "$start_date" && $promotion_edate_up[0] == "$end_date") {

            $sql = "SELECT galery_project_img_id,galery_project_img_name FROM LH_GALERY_IMG_PROJECT WHERE  galery_project_img_id = '$iCheck' ";

            $query_result3 = mssql_query($sql, $db_conn);
            $result = mssql_fetch_array($query_result3);
            $img = $result['galery_project_img_name'];
            $url = $promotion_url;
            $id_img = $result['galery_project_img_id'];

            $sql = "UPDATE LH_PROMOTION SET promotion_name_th = N'$promotion_name_th',promotion_name_en = '$promotion_name_en',"
                    . " promotion_detail_th = N'$promotion_detail_th' , promotion_detail_en = '$promotion_detail_en' ,update_date = '$dates',"
                    . " promotion_start_date = '$start_date' , promotion_end_date = '$end_date' ,promotion_url =N'$promotion_url'"
                    . " WHERE promotion_id = '$id'";
            $query = mssql_query($sql, $db_conn);

            $sql = "UPDATE LH_PROMOTION SET "
                    . " promotion_approve = '$approve' "
                    . " WHERE promotion_id = '$id ' ";
            $query = mssql_query($sql, $db_conn);

            ///update img
            $sql2 = "UPDATE LH_IMG_PROMOTION SET img_promotion_name = '$img',img_id_promotion = '$id_img'"
                . " WHERE promotion_id = '$id' ";
            $query = mssql_query($sql2, $db_conn);

        // update same project (change content && change date)
        } else {
            $query = "SELECT COUNT(*) AS counts FROM LH_PROMOTION WHERE project_id = '" . $project_id . "' AND (( promotion_start_date BETWEEN '$start_date'  AND '$end_date') OR (promotion_end_date BETWEEN '$start_date' AND '$end_date') OR ('$start_date' BETWEEN promotion_start_date AND promotion_end_date ) OR ('$end_date' BETWEEN promotion_start_date AND promotion_end_date )) AND NOT promotion_id = '$id' ";
            $query_result = mssql_query($query, $db_conn);
            $count_date = mssql_fetch_row($query_result);
            $count_date[0];
            if ($count_date[0] >= 1) {
                $_SESSION['add']='-1';
                         echo '<script>
                                    window.history.go(-1);
                                </script>';
                        exit();
            } else {
                $sql = "UPDATE LH_PROMOTION SET promotion_name_th = N'$promotion_name_th',promotion_name_en = '$promotion_name_en',"
                        . " promotion_detail_th = N'$promotion_detail_th' , promotion_detail_en = '$promotion_detail_en' ,update_date = '$dates',"
                        . " promotion_start_date = '$start_date' , promotion_end_date = '$end_date' ,promotion_url =N'$promotion_url'"
                        . " WHERE promotion_id = '$id'";
                $query = mssql_query($sql, $db_conn);

                $sql = "UPDATE LH_PROMOTION SET "
                        . " promotion_approve = '$approve',update_date = '$dates'"
                        . " WHERE promotion_id = '$id ' ";
                $query = mssql_query($sql, $db_conn);
            }
        }

    // case update change project
    } else if ($chk_id[0] != "$project_id") {
        $query = "SELECT COUNT(*) AS counts FROM LH_PROMOTION WHERE project_id = '" . $project_id . "'";

        $query_result = mssql_query($query, $db_conn);
        $check_id = mssql_fetch_row($query_result);
        $check_id[0];

        // case project not duplicate
        if ($check_id[0] == 0) {
            if ($iCheck == "") {
                $_SESSION['add']='-2';
                         echo '<script>
                                    window.history.go(-1);
                                </script>';
                        exit();
            } else if ($iCheck != "") {
                $sql = "UPDATE LH_PROMOTION SET promotion_name_th = N'$promotion_name_th',promotion_name_en = '$promotion_name_en',"
                        . " promotion_detail_th = N'$promotion_detail_th' , promotion_detail_en = '$promotion_detail_en' ,update_date = '$dates',"
                        . " promotion_start_date = '$start_date' , promotion_end_date = '$end_date' ,promotion_url =N'$promotion_url'"
                        . " WHERE promotion_id = '$id'";
                $query = mssql_query($sql, $db_conn);
                if ($project_id != '0') {
                    if ($promotion_url == "") {
                        $sql = "SELECT project_name_en,galery_project_img_id,galery_project_img_name FROM LH_PROJECTS p
                                           LEFT JOIN LH_GALERY_PROJECT g_p ON g_p.project_id = p.project_id
                                           LEFT JOIN LH_GALERY_IMG_PROJECT g_img ON g_img.galery_project_id = g_p.galery_project_id
                                           WHERE  g_img.galery_project_img_id = '$iCheck'  ";

                        $query = mssql_query($sql, $db_conn);
                        $result = mssql_fetch_array($query);
                        $url = str_replace(" ", "-", $result['project_name_en'], $var);
                        $img = $result['galery_project_img_name'];
                        $id_img = $result['galery_project_img_id'];
                        //$url_status='No url'; //ไม่ได้กำหนด
                    } else {
                        $sql = "SELECT galery_project_img_id,galery_project_img_name FROM LH_GALERY_IMG_PROJECT WHERE  galery_project_img_id = '$iCheck' ";

                        $query_result3 = mssql_query($sql, $db_conn);
                        $result = mssql_fetch_array($query_result3);
                        $img = $result['galery_project_img_name'];
                        $url = $promotion_url;
                        $id_img = $result['galery_project_img_id'];
                        //$url_status='Yes url';
                    }
                    $sql1 = "UPDATE LH_PROMOTION SET project_id = '$project_id', promotion_url = N'$url',update_date = '$dates' "
                            . " WHERE promotion_id = '$id ' ";
                    $query = mssql_query($sql1, $db_conn);

                    $sql2 = "UPDATE LH_IMG_PROMOTION SET img_promotion_name = '$img',img_id_promotion = '$id_img'"
                            . " WHERE promotion_id = '$id' ";
                    $query = mssql_query($sql2, $db_conn);
                }

                $sql = "UPDATE LH_PROMOTION SET "
                        . " promotion_approve = '$approve',update_date = '$dates' "
                        . " WHERE promotion_id = '$id ' ";
                $query = mssql_query($sql, $db_conn);
            }

        // case project duplicate
        } else if ($check_id[0] != 0) {
            $query = "SELECT COUNT(*) AS counts FROM LH_PROMOTION WHERE project_id = '" . $project_id . "' AND (( promotion_start_date BETWEEN '$start_date'  AND '$end_date') OR (promotion_end_date BETWEEN '$start_date' AND '$end_date') OR ('$start_date' BETWEEN promotion_start_date AND promotion_end_date ) OR ('$end_date' BETWEEN promotion_start_date AND promotion_end_date )) AND project_id IN ($project_id) ";
            $query_result = mssql_query($query, $db_conn);
            $count_date = mssql_fetch_row($query_result);
            $count_date[0];
            if ($count_date[0] >= 1) {
                $_SESSION['add']='-1';
                         echo '<script>
                                    window.history.go(-1);
                                </script>';
                        exit();
            } else {
                if ($iCheck == "") {
                    $_SESSION['add']='-2';
                         echo '<script>
                                    window.history.go(-1);
                                </script>';
                        exit();
                } else if ($iCheck != "") {
                    $sql = "UPDATE LH_PROMOTION SET promotion_name_th = N'$promotion_name_th',promotion_name_en = '$promotion_name_en',"
                            . " promotion_detail_th = N'$promotion_detail_th' , promotion_detail_en = '$promotion_detail_en' ,update_date = '$dates',"
                            . " promotion_start_date = '$start_date' , promotion_end_date = '$end_date' ,promotion_url =N'$promotion_url'"
                            . " WHERE promotion_id = '$id'";
                    $query = mssql_query($sql, $db_conn);
                    if ($project_id != '0') {
                        if ($promotion_url == "") {
                            $sql = "SELECT project_name_en,galery_project_img_id,galery_project_img_name FROM LH_PROJECTS p
                                                                LEFT JOIN LH_GALERY_PROJECT g_p ON g_p.project_id = p.project_id
                                                                LEFT JOIN LH_GALERY_IMG_PROJECT g_img ON g_img.galery_project_id = g_p.galery_project_id
                                                                WHERE  g_img.galery_project_img_id = '$iCheck'  ";

                            $query = mssql_query($sql, $db_conn);
                            $result = mssql_fetch_array($query);
                            $url = str_replace(" ", "-", $result['project_name_en'], $var);
                            $img = $result['galery_project_img_name'];
                            $id_img = $result['galery_project_img_id'];
                            //$url_status='No url'; //ไม่ได้กำหนด
                        } else {
                            $sql = "SELECT galery_project_img_id,galery_project_img_name FROM LH_GALERY_IMG_PROJECT WHERE  galery_project_img_id = '$iCheck' ";

                            $query_result3 = mssql_query($sql, $db_conn);
                            $result = mssql_fetch_array($query_result3);
                            $img = $result['galery_project_img_name'];
                            $url = $promotion_url;
                            $id_img = $result['galery_project_img_id'];
                            //$url_status='Yes url';
                            $sql2 = "UPDATE LH_IMG_PROMOTION SET img_promotion_name = '$img',img_id_promotion = '$id_img'"
                                . " WHERE promotion_id = '$id' ";
                            $query = mssql_query($sql2, $db_conn);
                        }

                        $sql1 = "UPDATE LH_PROMOTION SET project_id = '$project_id', promotion_url = N'$url',update_date = '$dates'"
                                . " WHERE promotion_id = '$id ' ";
                        $query = mssql_query($sql1, $db_conn);

                        $sql2 = "UPDATE LH_IMG_PROMOTION SET img_promotion_name = '$img',img_id_promotion = '$id_img'"
                                . " WHERE promotion_id = '$id' ";
                        $query = mssql_query($sql2, $db_conn);
                    }

                    $sql = "UPDATE LH_PROMOTION SET "
                            . " promotion_approve = '$approve' ,update_date = '$dates'"
                            . " WHERE promotion_id = '$id ' ";
                    $query = mssql_query($sql, $db_conn);
                }
            }
        }
    }

    if ($query) {
        $_SESSION['add']='1';
        header("location:promotion_update.php?id=$id&add=1");
    } else {
        $_SESSION['add']='-3';
        header("location:promotion_update.php?id=$id&add=-3");
    }
} else if (isset($_POST['update'])) {
    //echo "แก้ไข";

    $query1 = "SELECT project_id FROM LH_PROMOTION WHERE promotion_id = '$id' ";
    $query_result1 = mssql_query($query1, $db_conn);
    $chk_id = mssql_fetch_row($query_result1);
    $chk_id[0];

    // case update same project
    if ($chk_id[0] == "$project_id") {
        $query1 = "SELECT promotion_start_date FROM LH_PROMOTION WHERE promotion_id = '$id' ";
        $query_result1 = mssql_query($query1, $db_conn);
        $promotion_sdate_up = mssql_fetch_row($query_result1);
        $promotion_sdate_up[0];

        $query2 = "SELECT promotion_end_date FROM LH_PROMOTION WHERE promotion_id = '$id' ";
        $query_result2 = mssql_query($query2, $db_conn);
        $promotion_edate_up = mssql_fetch_row($query_result2);
        $promotion_edate_up[0];

        // update same project (change content && not change date)
        if ($promotion_sdate_up[0] == "$start_date" && $promotion_edate_up[0] == "$end_date") {
            $sql = "UPDATE LH_PROMOTION SET promotion_name_th = N'$promotion_name_th',promotion_name_en = '$promotion_name_en',"
                    . " promotion_detail_th = N'$promotion_detail_th' , promotion_detail_en = '$promotion_detail_en' ,update_date = '$dates',"
                    . " promotion_start_date = '$start_date' , promotion_end_date = '$end_date' ,promotion_url =N'$promotion_url'"
                    . " WHERE promotion_id = '$id'";
            $query = mssql_query($sql, $db_conn);

            $sql = "SELECT galery_project_img_id,galery_project_img_name FROM LH_GALERY_IMG_PROJECT WHERE  galery_project_img_id = '$iCheck' ";

            $query_result3 = mssql_query($sql, $db_conn);
            $result = mssql_fetch_array($query_result3);
            $img = $result['galery_project_img_name'];
            $url = $promotion_url;
            $id_img = $result['galery_project_img_id'];

            ///update
            $sql2 = "UPDATE LH_IMG_PROMOTION SET img_promotion_name = '$img',img_id_promotion = '$id_img'"
                . " WHERE promotion_id = '$id' ";
            $query = mssql_query($sql2, $db_conn);



            // update same project (change content && change date)
        } else {
          // $query = "SELECT COUNT(*) AS counts FROM LH_PROMOTION WHERE project_id = '" . $project_id . "' AND (( promotion_start_date BETWEEN '$start_date'  AND '$end_date') OR (promotion_end_date BETWEEN '$start_date' AND '$end_date')) AND NOT promotion_id = '$id' ";
          $query = "SELECT COUNT(*) AS counts FROM LH_PROMOTION WHERE project_id = '" . $project_id . "' AND (( promotion_start_date BETWEEN '$start_date'  AND '$end_date') OR (promotion_end_date BETWEEN '$start_date' AND '$end_date') OR ('$start_date' BETWEEN promotion_start_date AND promotion_end_date ) OR ('$end_date' BETWEEN promotion_start_date AND promotion_end_date )) AND NOT promotion_id = '$id' ";
            $query_result = mssql_query($query, $db_conn);
            $count_date = mssql_fetch_row($query_result);
            $count_date[0];
            if ($count_date[0] >= 1) {
                $_SESSION['add']='-1';
                         echo '<script>
                                    window.history.go(-1);
                                </script>';
                        exit();
            } else {
                $sql = "UPDATE LH_PROMOTION SET promotion_name_th = N'$promotion_name_th',promotion_name_en = '$promotion_name_en',"
                        . " promotion_detail_th = N'$promotion_detail_th' , promotion_detail_en = '$promotion_detail_en' ,update_date = '$dates',"
                        . " promotion_start_date = '$start_date' , promotion_end_date = '$end_date' ,promotion_url =N'$promotion_url'"
                        . " WHERE promotion_id = '$id'";
                $query = mssql_query($sql, $db_conn);
            }
        }

        // case update change project
    } else if ($chk_id[0] != "$project_id") {
        $query = "SELECT COUNT(*) AS counts FROM LH_PROMOTION WHERE project_id = '" . $project_id . "'";

        $query_result = mssql_query($query, $db_conn);
        $check_id = mssql_fetch_row($query_result);
        $check_id[0];

        // case project not duplicate
        if ($check_id[0] == 0) {
            if ($iCheck == "") {
                $_SESSION['add']='-2';
                         echo '<script>
                                    window.history.go(-1);
                                </script>';
                        exit();
            } else if ($iCheck != "") {
                $sql = "UPDATE LH_PROMOTION SET promotion_name_th = N'$promotion_name_th',promotion_name_en = '$promotion_name_en',"
                        . " promotion_detail_th = N'$promotion_detail_th' , promotion_detail_en = '$promotion_detail_en' ,update_date = '$dates',"
                        . " promotion_start_date = '$start_date' , promotion_end_date = '$end_date' ,promotion_url =N'$promotion_url'"
                        . " WHERE promotion_id = '$id'";
                $query = mssql_query($sql, $db_conn);
                if ($project_id != '0') {
                    if ($promotion_url == "") {
                        $sql = "SELECT project_name_en,galery_project_img_id,galery_project_img_name FROM LH_PROJECTS p
                                           LEFT JOIN LH_GALERY_PROJECT g_p ON g_p.project_id = p.project_id
                                           LEFT JOIN LH_GALERY_IMG_PROJECT g_img ON g_img.galery_project_id = g_p.galery_project_id
                                           WHERE  g_img.galery_project_img_id = '$iCheck'  ";

                        $query = mssql_query($sql, $db_conn);
                        $result = mssql_fetch_array($query);
                        $url = str_replace(" ", "-", $result['project_name_en'], $var);
                        $img = $result['galery_project_img_name'];
                        $id_img = $result['galery_project_img_id'];
                        //$url_status='No url'; //ไม่ได้กำหนด
                    } else {
                        $sql = "SELECT galery_project_img_id,galery_project_img_name FROM LH_GALERY_IMG_PROJECT WHERE  galery_project_img_id = '$iCheck' ";

                        $query_result3 = mssql_query($sql, $db_conn);
                        $result = mssql_fetch_array($query_result3);
                        $img = $result['galery_project_img_name'];
                        $url = $promotion_url;
                        $id_img = $result['galery_project_img_id'];
                        //$url_status='Yes url';
                    }
                    $sql1 = "UPDATE LH_PROMOTION SET project_id = '$project_id', promotion_url = N'$url',update_date = '$dates'"
                            . " WHERE promotion_id = '$id ' ";
                    $query = mssql_query($sql1, $db_conn);

                    $sql2 = "UPDATE LH_IMG_PROMOTION SET img_promotion_name = '$img',img_id_promotion = '$id_img'"
                            . " WHERE promotion_id = '$id' ";
                    $query = mssql_query($sql2, $db_conn);
                }
            }

            // case project duplicate
        } else if ($check_id[0] != 0) {
            // $query = "SELECT COUNT(*) AS counts FROM LH_PROMOTION WHERE project_id = '" . $project_id . "' AND ((promotion_start_date BETWEEN '$start_date'  AND '$end_date') OR (promotion_end_date BETWEEN '$start_date' AND '$end_date')) AND project_id IN ($project_id) ";
            $query = "SELECT COUNT(*) AS counts FROM LH_PROMOTION WHERE project_id = '" . $project_id . "' AND (( promotion_start_date BETWEEN '$start_date'  AND '$end_date') OR (promotion_end_date BETWEEN '$start_date' AND '$end_date') OR ('$start_date' BETWEEN promotion_start_date AND promotion_end_date ) OR ('$end_date' BETWEEN promotion_start_date AND promotion_end_date )) AND project_id IN ($project_id) ";
            $query_result = mssql_query($query, $db_conn);
            $count_date = mssql_fetch_row($query_result);
            $count_date[0];
            if ($count_date[0] >= 1) {
                $_SESSION['add']='-1';
                         echo '<script>
                                    window.history.go(-1);
                                </script>';
                        exit();
            } else {
                if ($iCheck == "") {
                    $_SESSION['add']='-2';
                         echo '<script>
                                    window.history.go(-1);
                                </script>';
                        exit();
                } else if ($iCheck != "") {
                    $sql = "UPDATE LH_PROMOTION SET promotion_name_th = N'$promotion_name_th',promotion_name_en = '$promotion_name_en',"
                            . " promotion_detail_th = N'$promotion_detail_th' , promotion_detail_en = '$promotion_detail_en' ,update_date = '$dates',"
                            . " promotion_start_date = '$start_date' , promotion_end_date = '$end_date' ,promotion_url =N'$promotion_url'"
                            . " WHERE promotion_id = '$id'";
                    $query = mssql_query($sql, $db_conn);
                    if ($project_id != '0') {
                        if ($promotion_url == "") {
                            $sql = "SELECT project_name_en,galery_project_img_id,galery_project_img_name FROM LH_PROJECTS p
                                                                        LEFT JOIN LH_GALERY_PROJECT g_p ON g_p.project_id = p.project_id
                                                                        LEFT JOIN LH_GALERY_IMG_PROJECT g_img ON g_img.galery_project_id = g_p.galery_project_id
                                                                        WHERE  g_img.galery_project_img_id = '$iCheck'  ";

                            $query = mssql_query($sql, $db_conn);
                            $result = mssql_fetch_array($query);
                            $url = str_replace(" ", "-", $result['project_name_en'], $var);
                            $img = $result['galery_project_img_name'];
                            //$url_status='No url'; //ไม่ได้กำหนด
                        } else {
                            $sql = "SELECT galery_project_img_id,galery_project_img_name FROM LH_GALERY_IMG_PROJECT WHERE  galery_project_img_id = '$iCheck' ";

                            $query_result3 = mssql_query($sql, $db_conn);
                            $result = mssql_fetch_array($query_result3);
                            $img = $result['galery_project_img_name'];
                            $url = $promotion_url;
                            //$url_status='Yes url';
                        }

                        $sql1 = "UPDATE LH_PROMOTION SET project_id = '$project_id' , promotion_url = '$url' ,update_date = '$dates'"
                                . " WHERE promotion_id = '$id ' ";
                        $query = mssql_query($sql1, $db_conn);

                        $sql2 = "UPDATE LH_IMG_PROMOTION SET img_promotion_name = '$img',img_id_promotion = '$id_img'"
                                . " WHERE promotion_id = '$id' ";
                        $query = mssql_query($sql2, $db_conn);
                    }
                }
            }
        }
    }
    if ($query) {
        $_SESSION['add']='1';
        header("location:promotion_update.php?id=$id&add=1");
    }else {
        $_SESSION['add']='-3';
        header("location:promotion_update.php?id=$id&add=-3");
    }
}
?>