<?php
ini_set ( 'display_errors', 1 );
ini_set ( 'display_startup_errors', 1 );
error_reporting ( E_ALL );

$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$lang = explode("/", $actual_link);

// if($lang[8] == 'gateway'): //go to path EN
// 	define ( 'ROOT_PATH', '/gateway' );
// 	define ( 'DOC_PATH', '/gateway' );
// 	define ( 'FILE_PATH', '/gateway/' );
// 	define ( 'ROUTE_PATH', '/gateway/' );

if($lang[3] == 'en'): //go to path EN
	define ( 'ROOT_PATH', '/' );
	define ( 'DOC_PATH', '/www_en' );
	define ( 'FILE_PATH', '/www_en/Frontend/' );
	define ( 'ROUTE_PATH', 'www_en/Frontend/' );
elseif ($lang[3] == 'cn'): //go to path CH
	define ( 'ROOT_PATH', '/' );
	define ( 'DOC_PATH', '/www_cn' );
	define ( 'FILE_PATH', '/www_cn/Frontend/' );
	define ( 'ROUTE_PATH', 'www_cn/Frontend/' );
	/*elseif ($lang[3] == 'th'): //go to path TH
	define ( 'ROOT_PATH', '/' );
	define ( 'DOC_PATH', '/www_th' );
	define ( 'FILE_PATH', '/www_th/Frontend/' );
	define ( 'ROUTE_PATH', 'www_th/Frontend/' );*/
else: //go to path uat
	define ( 'ROOT_PATH', '/' );
	define ( 'DOC_PATH', '/www_th' );
	define ( 'FILE_PATH', '/www_th/Frontend/' );
	define ( 'ROUTE_PATH', 'www_th/Frontend/' );
endif;

require 'vendor/autoload.php';
require_once route_path ( 'include/help/begin.php' );
require_once route_path ( 'include/help/query_function.php' );
require_once route_path ( 'include/help/query_independent.php' );


$router = new AltoRouter ();

$slugUrls = getListSlugURL ();

$router->map ( 'GET', url_path (  $slugUrls [35] ['slug'] ), function () use ($router, $slugUrls) {
	require_once route_path ( $slugUrls [35] ['file'] );
}, 'landing_home' );

// for uat
$router->map ( 'GET', url_path ( $slugUrls [1] ['slug'] ), function () use ($router, $slugUrls) {
	require_once route_path ( $slugUrls [1] ['file'] );
}, 'home' );


#### SEARCH
$router->map ( 'GET', url_path ( $slugUrls [33] ['slug'] .'/[*:product]?/[*:zone]?/[*:brand]?' ), function ($request) use ($router, $slugUrls) {
	require_once route_path ( $slugUrls [33] ['file']  );
}, 'search' );

$router->map ( 'POST', '/th/search-form-th', function ($request) use ($router, $slugUrls) {
	require_once route_path ( 'search_form.php' );
}, 'search-form-th' );

$router->map ( 'POST', '/en/search-form-en', function ($request) use ($router, $slugUrls) {
	require_once route_path ( 'search_form.php' );
}, 'search-form-en' );

$router->map ( 'POST', '/cn/search-form-cn', function ($request) use ($router, $slugUrls) {
	require_once route_path ( 'search_form.php' );
}, 'search-form-cn' );
#### END SEARCH


$router->map ( 'GET', url_path ( $slugUrls [3] ['slug'] ), function () use ($router, $slugUrls) {
	require_once route_path ( $slugUrls [3] ['file'] );
}, 'single-home-list' );

$router->map ( 'GET', url_path ( $slugUrls [3] ['slug'] . '/zone/[*:zone_name]' ), function ($request) use ($router, $slugUrls) {
	$zone_name = str_replace ( '', '', urldecode ( $request ['zone_name'] ) );
	$zone = getZoneByName ( $zone_name );
	$_GET ['zone_id'] = $zone->zone_id;
	require_once route_path ( $slugUrls [3] ['file'] );
}, 'single-home-list-by-zone' );
$router->map ( 'GET', url_path ( $slugUrls [28] ['slug'] . '/[*:product_name]/project/[*:project_name]/[*:product_id]/homesell/[*:plan_name]/[*:home_sell_id]' ), function ($request) use ($router, $slugUrls) {
	$home_sell_id = urldecode ( $request ['home_sell_id'] );
	$_GET ['home_sell_id'] = $home_sell_id;
	$_GET ['menu'] = 'homesell';
	$project_name = str_replace ( '', '', urldecode ( $request ['project_name'] ) );
	$product_id = $request ['product_id'];

	$project = getProjectByName ( $project_name );
	if(!isset($project->project_id)){
		$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $lang = explode("/", $actual_link);
		header('Location: //'.$_SERVER['SERVER_NAME'].'/'.$lang[3]);
		exit();
	}
	$_GET ['project_id'] = $project->project_id;
	$_GET ['product_id'] = $product_id;
	require_once route_path ( 'project-info-sale.php' );
}, 'home-sell-detail' );

$router->map ( 'GET', url_path ( $slugUrls [3] ['slug'] . '/project/[*:project_name]/[*:product_id]' ), function ($request) use ($router, $slugUrls) {
	$project_name = str_replace ( '', '', urldecode ( $request ['project_name'] ) );
	$product_id = $request ['product_id'];
	$project = getProjectByName ( $project_name );
	$_GET ['project_id'] = $project->project_id;
	$_GET ['product_id'] = $product_id;
	if(!isset($project->project_id)){
		$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $lang = explode("/", $actual_link);
		header('Location: //'.$_SERVER['SERVER_NAME'].'/'.$lang[3]);
		exit();
	}
	$_GET ['menu'] = 'single';
	require_once route_path ( 'project-info-home.php' );
}, 'single-home-detail' );

$router->map ( 'GET', url_path ( $slugUrls [26] ['slug'] ), function () use ($router, $slugUrls) {
	require_once route_path ( $slugUrls [26] ['file'] );
}, 'town-home-list' );

$router->map ( 'GET', url_path ( $slugUrls [26] ['slug'] . '/zone/[*:zone_name]' ), function ($request) use ($router, $slugUrls) {
	$zone_name = str_replace ( '', '', urldecode ( $request ['zone_name'] ) );
	$zone = getZoneByName ( $zone_name );
	$_GET ['zone_id'] = $zone->zone_id;
	require_once route_path ( $slugUrls [26] ['file'] );
}, 'town-home-list-by-zone' );

$router->map ( 'GET', url_path ( $slugUrls [26] ['slug'] . '/project/[*:project_name]/[*:product_id]' ), function ($request) use ($router, $slugUrls) {
	$project_name = str_replace ( '', '', urldecode ( $request ['project_name'] ) );
	$product_id = $request ['product_id'];
	$project = getProjectByName ( $project_name );
	$_GET ['project_id'] = $project->project_id;
	$_GET ['product_id'] = $product_id;
	if(!isset($project->project_id)){
		$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $lang = explode("/", $actual_link);
		header('Location: //'.$_SERVER['SERVER_NAME'].'/'.$lang[3]);
		exit();
	}
	$_GET ['menu'] = 'single';
	require_once route_path ( 'project-info-home.php' );
}, 'town-home-detail' );

$router->map ( 'GET', url_path (  $slugUrls [5] ['slug'] ), function ($request) use ($router, $slugUrls) {
	$lang = ! empty ( $request ['lang'] ) ? urldecode ( $request ['lang'] ) : '';
	$_GET ['lang'] = $lang;
	require_once route_path ( $slugUrls [5] ['file'] );
}, 'condominium-list' );

$router->map ( 'GET', url_path (  $slugUrls [5] ['slug'] . '/zone/[*:zone_name]' ), function ($request) use ($router, $slugUrls) {
	$lang = ! empty ( $request ['lang'] ) ? urldecode ( $request ['lang'] ) : '';
	$zone_name = str_replace ( '', '', urldecode ( $request ['zone_name'] ) );
	$_GET ['lang'] = $lang;
	//if($lang == 'th'){
	  $zone = getZoneByName ( $zone_name );
	//}else{
	  //$zone = getZoneByName_EN ( $zone_name );
	//}
	@$_GET ['zone_id'] = @$zone->zone_id;
	require_once route_path ( $slugUrls [5] ['file'] );
}, 'condominium-list-by-zone' );

$router->map ( 'GET', url_path (  $slugUrls [5] ['slug'] . '/[*:project_name]' ), function ($request) use ($router, $slugUrls) {
	$project_name = str_replace ( '', '', urldecode ( $request ['project_name'] ) );
	$lang = ! empty ( $request ['lang'] ) ? urldecode ( $request ['lang'] ) : '';
	$project = getProjectByName ( $project_name );

	$_GET ['project_id'] = $project->project_id;
	$_GET ['lang'] = $lang;
	if(!isset($project->project_id)){
		$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $lang = explode("/", $actual_link);
		header('Location: //'.$_SERVER['SERVER_NAME'].'/'.$lang[3]);
		exit();
	}
	$_GET ['menu'] = 'condominium';
	require_once route_path ( 'project-info-condominium.php' );
}, 'condominium-detail' );

$router->map ( 'GET', url_path ( $slugUrls [2] ['slug'] . '/[*:plan_name]/[*:home_sell_id]/zone/[*:zone_name]?' ), function ($request) use ($router, $slugUrls) {
	$home_sell_id = urldecode ( $request ['home_sell_id'] );
	$zone_name = ! empty ( $request ['zone_name'] ) ? str_replace ( '', '', urldecode ( $request ['zone_name'] ) ) : '';
	$zone = getZoneByName ( $zone_name );
	$_GET ['home_sell_id'] = $home_sell_id;
	$_GET ['zone_id'] = ! empty ( $request ['zone_id'] ) ? urldecode ( $zone->zone_id ) : '';
	require_once route_path ( 'project-info-furnished.php' );
}, 'furnished-home-detail-by-zone' );

$router->map ( 'GET', url_path ( $slugUrls [2] ['slug'] . '/[*:plan_name]/[*:home_sell_id]' ), function ($request) use ($router, $slugUrls) {
	$home_sell_id = urldecode ( $request ['home_sell_id'] );
	$_GET ['home_sell_id'] = $home_sell_id;
	$_GET ['zone_id'] = ! empty ( $request ['zone_id'] ) ? urldecode ( $request ['zone_id'] ) : '';
	require_once route_path ( 'project-info-furnished.php' );
}, 'furnished-home-detail' );

$router->map ( 'GET', url_path ( $slugUrls [2] ['slug'] ), function ($request) use ($router, $slugUrls) {
	$zone_name = ! empty ( $request ['zone_name'] ) ? urldecode ( $request ['zone_name'] ) : '';
	$zone = getZoneByName ( $zone_name );
	$_GET ['zone_id'] = ! empty ( $zone ) ? $zone->zone_id : '';
	require_once route_path ( $slugUrls [2] ['file'] );
}, 'furnished-home-list' );

$router->map ( 'GET', url_path ( $slugUrls [2] ['slug'] . '-by-zone' . '/zone/[*:zone_name]?' ), function ($request) use ($router, $slugUrls) {
	$zone_name = ! empty ( $request ['zone_name'] ) ? str_replace ( '', '', urldecode ( $request ['zone_name'] ) ) : '';
	$zone = getZoneByName ( $zone_name );
	$_GET ['zone_id'] = ! empty ( $zone ) ? $zone->zone_id : '';
	require_once route_path ( $slugUrls [2] ['file'] );
}, 'furnished-home-list-by-zone' );

$router->map ( 'GET', url_path ( $slugUrls [6] ['slug'] ), function ($request) use ($router, $slugUrls) {
	$zone_name = ! empty ( $request ['zone_name'] ) ? str_replace ( '', '', urldecode ( $request ['zone_name'] ) ) : '';
	$zone = getZoneByName ( $zone_name );
	$_GET ['zone_id'] = ! empty ( $zone ) ? $zone->zone_id : '';
	require_once route_path ( $slugUrls [6] ['file'] );
}, 'ladawan-list' );

$router->map ( 'GET', url_path ( $slugUrls [6] ['slug'] . '/zone/[*:zone_name]?' ), function ($request) use ($router, $slugUrls) {
	$zone_name = ! empty ( $request ['zone_name'] ) ? str_replace ( '', '', urldecode ( $request ['zone_name'] ) ) : '';
	$zone = getZoneByName ( $zone_name );
	$_GET ['zone_id'] = ! empty ( $zone ) ? $zone->zone_id : '';
	require_once route_path ( $slugUrls [6] ['file'] );
}, 'ladawan-list-by-zone' );

$router->map ( 'GET', url_path ( $slugUrls [6] ['slug'] . '/[*:project_name]/[*:product_id]' ), function ($request) use ($router, $slugUrls) {
	$project_name = str_replace ( '', '', urldecode ( $request ['project_name'] ) );
	$product_id = $request ['product_id'];
	$project = getProjectByName ( $project_name );
	$_GET ['project_id'] = $project->project_id;
	$_GET ['product_id'] = $product_id;
	if(!isset($project->project_id)){
		$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $lang = explode("/", $actual_link);
		header('Location: //'.$_SERVER['SERVER_NAME'].'/'.$lang[3]);
		exit();
	}
	if ($product_id == 1) {
		$_GET ['menu'] = 'single';
		require_once route_path ( 'project-info-luxuryhome.php' );
	} elseif ($product_id == 3) {
		$_GET ['menu'] = 'condominium';
		require_once route_path ( 'project-info-luxury.php' );
	}
}, 'ladawan-detail' );

$router->map ( 'GET', url_path ( $slugUrls [7] ['slug'] ), function ($request) use ($router, $slugUrls) {
	$zone_name = ! empty ( $request ['zone_name'] ) ? urldecode ( $request ['zone_name'] ) : '';
	$zone = getZoneByName ( $zone_name );
	$_GET ['zone_id'] = ! empty ( $zone ) ? $zone->zone_id : '';
	require_once route_path ( $slugUrls [7] ['file'] );
}, 'country-list' );

$router->map ( 'GET', url_path ( $slugUrls [7] ['slug'] . '/zone/[*:zone_name]?' ), function ($request) use ($router, $slugUrls) {
	$zone_name = ! empty ( $request ['zone_name'] ) ? str_replace ( '', '', urldecode ( $request ['zone_name'] ) ) : '';
	$zone = getZoneByName ( $zone_name );
	$_GET ['zone_id'] = ! empty ( $zone ) ? $zone->zone_id : '';
	require_once route_path ( $slugUrls [7] ['file'] );
}, 'country-list-zone' );



$router->map ( 'GET', url_path ( $slugUrls [13] ['slug'] ), function ($request) use ($router, $slugUrls) {
	require_once route_path ( $slugUrls [13] ['file'] );
}, 'home-series' );

$router->map ( 'GET', url_path ( $slugUrls [13] ['slug'] . '/series/[*:series_name]/plan/[*:plan_name]/zone/[*:zone_name]' ), function ($request) use ($router, $slugUrls) {
	$plan_name = ! empty ( $request ['plan_name'] ) ? str_replace ( '', '',  ( $request ['plan_name'] ) ) : '';
	$zone_name = ! empty ( $request ['zone_name'] ) ? str_replace ( '', '', urldecode ( $request ['zone_name'] ) ) : '';
	$series_name = str_replace ( '', '', urldecode ( $request ['series_name'] ) );
	$plan = getPlanByPlanName ($plan_name);
	$zone = getZoneByName ( $zone_name );
	$series = getSeriesByName ( $series_name );

	$_GET ['plan_id'] = $plan->plan_id;
	$_GET ['zone_id'] = $zone->zone_id;
	$_GET ['series_id'] = $series->series_id;
	require_once route_path ( 'home-series-detail.php' );
}, 'home-series-detail-by-zone' );

$router->map ( 'GET', url_path ( $slugUrls [13] ['slug'] . '/series/[*:series_name]/plan/[*:plan_name]' ), function ($request) use ($router, $slugUrls) {
	$plan_name = ! empty ( $request ['plan_name'] ) ? str_replace ( '', '',  ( $request ['plan_name'] ) ) : '';

	$plan = getPlanByPlanName_code (urldecode($request ['plan_name']),$request ['plan_name']);
	$series_name = str_replace ( '', '', urldecode ( $request ['series_name'] ) );
	$series = getSeriesByName ( $series_name );
	$_GET ['series_id'] = $series->series_id;

	// echo $plan_name;
	// exit();
	$_GET ['plan_id'] = $plan->plan_id;
	require_once route_path ( 'home-series-detail.php' );
}, 'home-series-detail' );


$router->map ( 'GET', url_path ( $slugUrls [13] ['slug'] . '/series/[*:series_name]' ), function ($request) use ($router, $slugUrls) {
	$series_name = str_replace ( '', '', urldecode ( $request ['series_name'] ) );
	$series = getSeriesByName ( $series_name );
	$_GET ['series_id'] = $series->series_id;
	require_once route_path ( 'split-home-series.php' );
}, 'split-home-series' );

$router->map ( 'GET', url_path ( $slugUrls [17] ['slug'] . '/living/[*:category]?' ), function ($request) use ($router, $slugUrls) {
	$category = ! empty ( $request ['category'] ) ? str_replace ( '', '', urldecode ( $request ['category'] ) ) : '';
	$_GET ['category'] = $category;
	require_once route_path ( $slugUrls [17] ['file'] );
}, 'living-tips' );

$router->map ( 'GET', url_path ( $slugUrls [17] ['slug'] . '/tips/[*:living_tip_name]?' ), function ($request) use ($router, $slugUrls) {
	$living_tip_name = ! empty ( $request ['living_tip_name'] ) ? str_replace ( '[\':\', \'\\\', \'/\',\'?\', \'*\']', '', urldecode ( $request ['living_tip_name'] ) ) : '';

	$living_tip = getLivingTipByName ( $living_tip_name );
	$_GET ['living_tip_id'] = $living_tip->living_tip_id;
	require_once route_path ( 'living-tips-details.php' );
}, 'living-tips-detail' );

$router->map ( 'GET', url_path ( $slugUrls [10] ['slug'] ), function ($request) use ($router, $slugUrls) {
	require_once route_path ( $slugUrls [10] ['file'] );
}, 'review' );

$router->map ( 'GET', url_path ( $slugUrls [10] ['slug'] . '/[*:review_name]?' ), function ($request) use ($router, $slugUrls) {
	$review_name = ! empty ( $request ['review_name'] ) ? str_replace ( '', '', urldecode ( $request ['review_name'] ) ) : '';
	$review = getReviewByName ( $review_name );
	$_GET ['review_id'] = $review->project_review_id;
	require_once route_path ( 'review-details.php' );
}, 'review-detail' );

$router->map ( 'GET', url_path ( $slugUrls [19] ['slug'] . '/[*:community_name]?' ), function ($request) use ($router, $slugUrls) {
	$community_name = ! empty ( $request ['community_name'] ) ? str_replace ( '', '', urldecode ( $request ['community_name'] ) ) : '';
	$community = getCommunityByName ( $community_name );
	$_GET ['community_id'] = ! empty ( $community ) ? $community->community_id : '';
	require_once route_path ( $slugUrls [19] ['file'] );
}, 'community' );

$router->map ( 'GET', url_path ( $slugUrls [27] ['slug'] . '/[*:landing_page_name]?' ), function ($request) use ($router, $slugUrls) {
	$landing_page_name = ! empty ( $request ['landing_page_name'] ) ? str_replace ( '', '', urldecode ( $request ['landing_page_name'] ) ) : '';
	$landing_page = getLandingPageByName ( $landing_page_name );
	$_GET ['landing_page_id'] = ! empty ( $landing_page ) ? $landing_page->landing_page_id : '';
	require_once route_path ( $slugUrls [27] ['file'] );
}, 'landing-page-detail' );

$router->map ( 'GET', url_path ( $slugUrls [8] ['slug'] ), function ($request) use ($router, $slugUrls) {
	require_once route_path ( $slugUrls [8] ['file'] );
}, 'new-project-list' );

// $router->map ( 'GET', url_path ( $slugUrls [9] ['slug'] ), function ($request) use ($router, $slugUrls) {
// 		require_once route_path ( $slugUrls [9] ['file'] );
// }, 'promotion' );

$router->map('GET', url_path($slugUrls[9]['slug'].'/[*:zone_name]?'), function ($request) use ($router,$slugUrls) {
    $zone_name = str_replace('','',urldecode(@$request['zone_name']));
    $zone = getZoneByName($zone_name);
    @$_GET['zone_id'] = $zone->zone_id;
    require_once route_path($slugUrls[9]['file']);
}, 'promotion');

$router->map ( 'GET', url_path ( $slugUrls [12] ['slug'] ), function ($request) use ($router, $slugUrls) {
	require_once route_path ( $slugUrls [12] ['file'] );
}, 'tvc' );

$router->map ( 'GET', url_path ( $slugUrls [14] ['slug'] . '/[*:type_360]?' ), function ($request) use ($router, $slugUrls) {
	$type_360 = ! empty ( $request ['type_360'] ) ? urldecode ( $request ['type_360'] ) : '';
	$_GET ['type_360'] = $type_360;
	require_once route_path ( $slugUrls [14] ['file'] );
}, '360-virtual' );

$router->map ( 'GET', url_path ( $slugUrls [15] ['slug'] ), function ($request) use ($router, $slugUrls) {
	require_once route_path ( $slugUrls [15] ['file'] );
}, 'air-plus' );

$router->map ( 'GET', url_path ( $slugUrls [16] ['slug'] ), function ($request) use ($router, $slugUrls) {
	require_once route_path ( $slugUrls [16] ['file'] );
}, 'concept' );

$router->map ( 'GET', url_path ( $slugUrls [18] ['slug'] ), function ($request) use ($router, $slugUrls) {
	require_once route_path ( $slugUrls [18] ['file'] );
}, 'motivo' );

$router->map ( 'GET', url_path ( 'service-online/[*:step]?' ), function ($request) use ($router, $slugUrls) {
	$_GET ['menu'] = 'service-online';
	$step = ! empty ( $request ['step'] ) ? urldecode ( $request ['step'] ) : 1;
	switch ($step) {
		case 1 :
			require_once route_path ( 'service-online-step1.php' );
			break;
		case 2 :
			require_once route_path ( 'service-online-step2.php' );
			break;
		case 3 :
			require_once route_path ( 'service-online-step3.php' );
			break;
	}
}, 'service-online' );

$router->map ( 'GET', url_path ( 'complain-step/[*:step]?' ), function ($request) use ($router, $slugUrls) {
	$step = ! empty ( $request ['step'] ) ? urldecode ( $request ['step'] ) : 1;
	switch ($step) {
		case 1 :
			require_once route_path ( 'complain-step1.php' );
			break;
		case 2 :
			require_once route_path ( 'complain-step2.php' );
			break;
		case 3 :
			require_once route_path ( 'complain-step3.php' );
			break;
		case 4 :
			require_once route_path ( 'complain-step4.php' );
			break;
		case 5 :
			require_once route_path ( 'complain-step5.php' );
			break;
	}
}, 'complain-step' );

$router->map ( 'GET', url_path ( $slugUrls [20] ['slug'] ), function ($request) use ($router, $slugUrls) {
	require_once route_path ( $slugUrls [20] ['file'] );
}, 'job-with-us' );
// START Additional Modal content for job by Yothin
$router->map ( 'GET', url_path('th/jd'), function ($request) use ($router, $slugUrls) {
	require_once route_path ( 'job-description.php' );
}, 'job-description' );
// END Additional Modal content for job by Yothin

// START job-news by Yothin
$router->map ( 'GET', url_path('th/job-news'. '/[*:title]?'), function ($request) use ($router, $slugUrls) {
	$title = ! empty ( $request ['title'] ) ? str_replace ( '', '', urldecode ( $request ['title'] ) ) : '';
	$news = getNewsJobByName ( $title );
	$_GET['id'] = $news->ID;
	require_once route_path ( 'job-news-details.php' );
}, 'job-news-details' );
// END job-news by Yothin
$router->map ( 'GET', url_path ( $slugUrls [32] ['slug']), function ($request) use ($router, $slugUrls) {
	require_once route_path ( $slugUrls [32] ['file'] );
}, 'contact-us' );

$router->map ( 'GET', url_path ( $slugUrls [23] ['slug'] ), function ($request) use ($router, $slugUrls) {
	require_once route_path ( $slugUrls [23] ['file'] );
}, 'land-offer' );

$router->map ( 'GET', url_path ( $slugUrls [24] ['slug'] ), function ($request) use ($router, $slugUrls) {
	require_once route_path ( $slugUrls [24] ['file'] );
}, 'report' );

$router->map ( 'POST', url_path ( $slugUrls [29] ['slug'] ), function ($request) use ($router, $slugUrls) {
	require_once route_path ( $slugUrls [29] ['file'] );
}, 'mail-report' );

$router->map ( 'POST', url_path ( $slugUrls [30] ['slug'] ), function ($request) use ($router, $slugUrls) {
	require_once route_path ( $slugUrls [30] ['file'] );
}, 'mail-contactus' );

$router->map ( 'POST', url_path ( $slugUrls [36] ['slug'] ), function ($request) use ($router, $slugUrls) {
	require_once route_path ( $slugUrls [36] ['file'] );
}, 'mail-snippet-contact' );

$router->map ( 'GET', url_path ( 'calculator' ), function ($request) use ($router, $slugUrls) {
	require_once route_path ( 'calculator.php' );
}, 'calculator' );

$router->map ( 'GET', url_path ( '[*:lang]?/' . $slugUrls [11] ['slug'] ), function ($request) use ($router, $slugUrls) {
	$_GET ['menu'] = 'lh-project';
	$_GET ['lang'] = ! empty ( $request ['lang'] ) ? urldecode ( $request ['lang'] ) : 'th';
	if ($_GET ['lang'] == 'th') {
		require_once route_path ( 'lh-project-th.php' );
	} else {
		require_once route_path ( 'lh-project.php' );
	}
}, 'lh-project' );

$router->map ( 'GET', url_path ( $slugUrls [1] ['slug'].'/foreign-ownership/realestate' ), function ($request) use ($router, $slugUrls) {
	$_GET ['menu'] = 'foreign-owner';
	$_GET ['lang'] = ! empty ( $request ['lang'] ) ? urldecode ( $request ['lang'] ) : 'th';
	if ($_GET ['lang'] == 'th') {
		require_once route_path ( 'foreign-ownership-th.php' );
	} else {
		require_once route_path ( 'foreign-ownership.php' );
	}
}, 'foreign-owner' );
$router->map ( 'POST', '/genDefaultBanner', function ($request) use ($router, $slugUrls) {
                require_once route_path ( 'gendefault-banner.php' );
}, 'genDefaultBanner' );

$router->map ( 'POST', '/get-direction-form-th', function ($request) use ($router, $slugUrls) {
	require_once route_path ( 'get_direction_form.php' );
}, 'direction-form-th' );

$router->map ( 'POST', '/en/get-direction-form-en', function ($request) use ($router, $slugUrls) {
	require_once route_path ( 'get_direction_form.php' );
}, 'direction-form-en' );

$router->map ( 'POST', '/cn/get-direction-form-cn', function ($request) use ($router, $slugUrls) {
	require_once route_path ( 'get_direction_form.php' );
}, 'direction-form-cn' );


$router->map ( 'GET', url_path ( $slugUrls [34] ['slug'].'/[*:zone]/[*:project]/[*:project_id]' ), function ($request) use ($router, $slugUrls) {
	require_once route_path ( $slugUrls [34] ['file'] );
}, 'get-direction' );


$router->map ( 'GET', url_path ( 'smart-cal-howto' ), function ($request) use ($router, $slugUrls) {
	require_once route_path ( 'smart-cal-howto.php' );
}, 'smart-cal-howto' );

$match = $router->match ();

 $uri_segments = explode('/', $actual_link);
 $url_lang = $uri_segments[3];

if ($match) {
	call_user_func( $match ['target'], $match ['params'] );
}else {
	// header("HTTP/1.0 404 Not Found");
	// echo "404";
  header('Location: //'.$_SERVER['SERVER_NAME'].'/'.$url_lang);

}
function url_path($path) {
	return ROOT_PATH . $path;
}
function file_path($path) {
	return FILE_PATH . $path;
}
function route_path($path) {
	return ROUTE_PATH . $path;
}