<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';
require_once 'include/help/query_independent.php';
require_once 'include/help/Helper.php';
$page = 'lh-community';
$page_index = 0;
$img = '';
?>
<?php
include('header.php');
//elementMetaTitleDisTag($page);
?>
    <!-- CSS -->
    <link rel="stylesheet" href="<?= file_path('css/community.css') ?>" type="text/css">
    <!-- JS -->
    <script src="<?= file_path('js/community.js') ?>"></script>

    <input type="hidden" id="community_choose_id" value="<?= !empty($_GET['community_id']) ? $_GET['community_id'] : '' ?>">

    <div id="content" class="content">
        <div class="container">

        
        <?php
        $SEO = getSEOUrl($actual_link);
        if($actual_link == @$SEO->url_page){
            echo '<h1 class="heading-title">'.$SEO->h1.'<h1>';
        }else{
           echo '<h1 class="heading-title">LH community</h1>'; 
       }
       ?>

            <div id="commuList" class="">

                <div class="row">
                    <div class="commu-lists">
                        <?php
                        $communities = getCommunity();
                        if($communities != false){
                            foreach($communities as $community) {?>
                                <div class="list-item item community_id_<?= $community->community_id ?>">
                                    <div class="review-img">
                                        <img src="<?= backend_url('base',$community->community_img) ?>" alt="">
                                    </div>
                                    <p class="title green"><?= Helper::lang('th',$community->community_name_th,$community->community_name_en) ?></p>
                                    <p class="subtitle"></p>
                                    <p><?= Helper::lang('th',$community->community_dis_th,$community->community_dis_en) ?></p>
                                    <br>
                                    <p><?= Helper::lang('th', $community->communuty_content_th, $community->communuty_content_en) ?></p>
                                </div>
                            <?php
                            }
                        }
                        ?>

                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>


<?php include('footer.php'); ?>
