<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>
<?php include('header.php'); ?>
    <!-- CSS -->
<link rel="stylesheet" href="<?= file_path('css/complain.css') ?>" type="text/css">
    <!-- JS -->
    <script src="js/complain.js"></script>

    <div class="page-banner-md banner-hide">
        <div id="bannerSlide">
            <div class="item">
                <div class="banner-parallax">
                    <img src="<?= file_path('images/temp2/banner_complain.jpg') ?>" alt="">
                </div>
            </div>
        </div>
    </div>

    <div id="content" class="content rps-content-pd">
        <div class="container">
            <h1 class="heading-title">ร้องเรียนเรื่องบ้านและคอนโด</h1>

            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div>
<!--                       <p class="title">คุณสามารถค้นหาชื่อโครงการที่คุณอาศัย และส่งข้อความร้องเรียนบ้านและคอนโดมาที่ แลนด์ แอนด์เฮ้าส์</p>-->
                        <div class="complain-form-block">
                            <form action="" class="complain-form form-container">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="step-label">
                                            step 5
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <p class="title">รายละเอียด</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form step4">
                                        <div class="col-md-3">
                                            <div class="project-name">
                                                <p class="title">
                                                    ชื่อโครงการ<br>
                                                    <span>ชัยพฤกษ์ วัชรพล</span>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="">
                                                <div class="form-group">
                                                    <textarea name="" placeholder="ระบุข้อความ" id="" class="form-control" cols="30" rows="10"></textarea>
                                                    <p class="txt-form-remark">การส่งรายละเอียดภาษาไทยหรือรายละเอียดที่มีทั้งภาษาไทยและภาษาอังกฤษจำนวน 70 ตัวอักษร คิดเป็น 1 ข้อความ (รวมสระ เว้นวรรค และตัวเลข) รายละเอียดภาษาอังกฤษ จำนวน 160 ตัวอักษร คิดเป็น 1 ข้อวาม (รวมเว้นวรรค และตัวเลข)</p>
                                                </div>

                                                <div class="form-group">
                                                    <label for="fileName" class="title">รูปแนบ</label>
                                                    <span class="upload-file" onclick="openFileBrowse()">Attach <i class="i-attach"></i></span>
                                                    <input type="text" class="form-control input-file" id="fileName" name="fileName" placeholder="" readonly>
                                                    <input type="file" name="fileUpload" id="fileUpload" value="" style="display: none">
                                                </div>


                                                <div class="form-group">
                                                    <p class="title pmargin">ต้องการให้ติดต่อกลับ</p>
                                                    <div class="radio-block">
                                                        <input type="radio" id="email" name="contact" />
                                                        <label for="email"><span></span>อีเมล์</label>
                                                    </div>

                                                    <div class="radio-block">
                                                        <input type="radio" id="tel" name="contact" />
                                                        <label for="tel"><span></span>โทรศัพท์</label>
                                                    </div>

                                                    <div class="radio-block">
                                                        <input type="radio" id="addr" name="contact" />
                                                        <label for="addr"><span></span>ที่อยู่</label>
                                                    </div>
                                                    <textarea name="" placeholder="ระบุข้อความ" id="" class="form-control" cols="30" rows="10"></textarea>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-offset-3 col-md-3 pdr0">
                                        <a href="<?= $router->generate('complain-step',['step' => 5]) ?>" class="btn btn-submit next right">Send</a>
                                    </div>
                                </div>

                            </form>
                            <a href="<?= $router->generate('complain-step',['step' => 4]) ?>" class="step-back">< Back</a>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


<?php include('footer.php'); ?>
