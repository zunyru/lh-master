<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>

<?php include('header.php'); ?>
<!-- CSS -->
<link rel="stylesheet" href="css/investor.css" type="text/css">
<link rel="stylesheet" href="css/corporate-all.css" type="text/css">
<link rel="stylesheet" href="css/corporate-menu.css" type="text/css">

<!-- JS -->
<script src="js/corporate-menu.js"></script>

<div id="content" class="content invest-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">

                <h1>ข้อมูลทางการเงิน</h1>

                <div id="corpMenu" class="corp-menu-tab">
                    <ul>
                        <li class="current"><a href="#corpBlock1">งบการเงิน</a></li>
                        <li><a href="#corpBlock2">รายงานประจำปี</a></li>
                        <li><a href="#corpBlock3">แบบแสดงรายการข้อมูลประจำปี</a></li>
                        <li><a href="#corpBlock4">ข้อมูลสำคัญทางการเงิน</a></li>
                        <li><a href="#corpBlock5">บทวิเคราะห์หลักทรัพย์</a></li>
                    </ul>
                </div>

                <div id="corpBlock1" class="current corp-block">
                    <h2>งบการเงิน</h2>
                    <p class="left invest-p-txt">กรุณาเลือกปี พ.ศ. ที่คุณต้องการดูรายงานงบการเงิน ทั้งในแบบรายปีและรายไตรมาสค่ะ</p>
                    <div class="select-year-block">
                        ปี
                        <div class="select-year">
                            <select name="" id="">
                                <option value="2560">2560</option>
                                <option value="2559">2559</option>
                                <option value="2558">2558</option>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <table class="table-budget">
                        <thead>
                            <tr>
                                <th class="txt-center">รายปี</th>
                                <th>รายไตรมาส</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td>
                                    <p>ไตรมาสที่ 3 (30 กันยายน 2559)</p>
                                    <ul>
                                        <li><a href="">รายงานของผู้สอบบัญชี</a></li>
                                        <li><a href="">งบดุลและงบกำไรขาดทุน</a></li>
                                        <li><a href="">หมายเหตุประกอบงบการเงิน</a></li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <p>ไตรมาสที่ 2 (30 มิถุนายน 2559)</p>
                                    <ul>
                                        <li><a href="">รายงานของผู้สอบบัญชี</a></li>
                                        <li><a href="">งบดุลและงบกำไรขาดทุน</a></li>
                                        <li><a href="">หมายเหตุประกอบงบการเงิน</a></li>
                                    </ul>

                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <p>ไตรมาสที่ 1 (31 มีนาคม 2559)</p>
                                    <ul>
                                        <li><a href="">รายงานของผู้สอบบัญชี</a></li>
                                        <li><a href="">งบดุลและงบกำไรขาดทุน</a></li>
                                        <li><a href="">หมายเหตุประกอบงบการเงิน</a></li>
                                    </ul>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>

                <div id="corpBlock2" class="corp-block">
                    <h2>รายงานประจำปี</h2>
                    <p class="invest-p-txt">คุณสามารถคลิกเพื่ออ่านข้อมูลรายงานประจำปีของ แลนด์ แอนด์ เฮ้าส์ ในรูปแบบไฟล์ PDF ได้ที่นี่ค่ะ</p>
                    <div class="inv-report-block">

                        <div class="inv-report-list list-first">
                            <div class="ic-report"></div>
                            <div class="inv-report-detail">
                                <a href="">รายงานประจำปี 2558
                                <span>(2.45 MB)</span> <i class="download"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="inv-report-list">
                            <div class="ic-report"></div>
                            <div class="inv-report-detail">
                                <a href="">รายงานประจำปี 2557
                                    <span>(2.45 MB)</span> <i class="download"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="inv-report-list">
                            <div class="ic-report"></div>
                            <div class="inv-report-detail">
                                <a href="">รายงานประจำปี 2556
                                    <span>(2.45 MB)</span> <i class="download"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="inv-report-list">
                            <div class="ic-report"></div>
                            <div class="inv-report-detail">
                                <a href="">รายงานประจำปี 2555
                                    <span>(2.45 MB)</span> <i class="download"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="inv-report-list">
                            <div class="ic-report"></div>
                            <div class="inv-report-detail">
                                <a href="">รายงานประจำปี 2554
                                    <span>(2.45 MB)</span> <i class="download"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="inv-report-list">
                            <div class="ic-report"></div>
                            <div class="inv-report-detail">
                                <a href="">รายงานประจำปี 2553
                                    <span>(2.45 MB)</span> <i class="download"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </div>
                </div>

                <div id="corpBlock3" class="corp-block">
                    <h2>แบบแสดงรายการข้อมูลประจำปี</h2>
                    <p class="invest-p-txt">ท่านสามารถ อ่านข้อมูลแบบแสดงรายการข้อมูลประจำปี ในรูปแบบไฟล์ PDF ได้ที่นี่ค่ะ</p>
                    <div class="inv-report-block">

                        <div class="inv-report-list list-first">
                            <div class="ic-report"></div>
                            <div class="inv-report-detail">
                                <a href="">2558  แบบแสดงรายการ 56-1
                                    <span>(2.60  MB)</span> <i class="download"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="inv-report-list">
                            <div class="ic-report"></div>
                            <div class="inv-report-detail">
                                <a href="">2557  แบบแสดงรายการ 56-1
                                    <span>(2.60  MB)</span> <i class="download"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="inv-report-list">
                            <div class="ic-report"></div>
                            <div class="inv-report-detail">
                                <a href="">2557  แบบแสดงรายการ 56-1
                                    <span>(2.60  MB)</span> <i class="download"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="inv-report-list">
                            <div class="ic-report"></div>
                            <div class="inv-report-detail">
                                <a href="">2557  แบบแสดงรายการ 56-1
                                    <span>(2.60  MB)</span> <i class="download"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="inv-report-list">
                            <div class="ic-report"></div>
                            <div class="inv-report-detail">
                                <a href="">2557  แบบแสดงรายการ 56-1
                                    <span>(2.60  MB)</span> <i class="download"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>


                    </div>


                </div>

                <div id="corpBlock4" class="corp-block">
                    <table width="100%" class="table-historical-price">
                        <thead>
                        <tr class="">
                            <th class="tb-title">ข้อมูลสำคัญทางการเงิน</th>
                            <th width="10%">2558</th>
                            <th width="10%">2557</th>
                            <th width="10%">2556</th>
                            <th width="10%">2555</th>
                            <th width="10%">2554</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr class="row-title">
                            <td class="tb-descp" colspan="6">การดำเนินงาน </td>
                        </tr>
                        <tr>
                            <td class="tb-descp">จำนวนโครงการที่ยังดำเนินงาน </td>
                            <td>68</td>
                            <td>67</td>
                            <td>67</td>
                            <td>57</td>
                            <td>55</td>
                        </tr>
                        <tr class="row-highlight">
                            <td class="tb-descp">ยอดจอง (ล้านบาท) </td>
                            <td>24,700</td>
                            <td>31,500 </td>
                            <td>30,190</td>
                            <td>25,080</td>
                            <td>19,201</td>
                        </tr>
                        <tr>
                            <td class="tb-descp">ยอดจอง (หน่วย) </td>
                            <td>3,549</td>
                            <td>4,957 </td>
                            <td>5,233</td>
                            <td>5,343</td>
                            <td>4,284</td>
                        </tr>
                        <tr class="row-title">
                            <td class="tb-descp" colspan="6">ข้อมูลสำคัญทางการเงิน (ล้านบาท) </td>
                        </tr>
                        <tr class="row-highlight">
                            <td class="tb-descp">รายได้จากการขาย </td>
                            <td>24,223</td>
                            <td>26,037 </td>
                            <td>22,939</td>
                            <td>22,305</td>
                            <td>18,580</td>
                        </tr>
                        <tr>
                            <td class="tb-descp">รายได้ค่าเช่าและบริการ </td>
                            <td>2,036</td>
                            <td>2,280 </td>
                            <td>2,136</td>
                            <td>1,797</td>
                            <td>649</td>
                        </tr>
                        <tr class="row-highlight">
                            <td class="tb-descp">รายได้รวม </td>
                            <td>29,025</td>
                            <td>31,932 </td>
                            <td>25,742</td>
                            <td>24,432</td>
                            <td>22,139</td>
                        </tr>
                        <tr>
                            <td class="tb-descp">กำไรขั้นต้นจากการขาย </td>
                            <td>8,219</td>
                            <td>9,211 </td>
                            <td>8,203</td>
                            <td>7,586</td>
                            <td>6,064</td>
                        </tr>
                        <tr class="row-highlight">
                            <td class="tb-descp">ส่วนแบ่งกำไรจากเงินลงทุนในบริษัทร่วม </td>
                            <td>2,437</td>
                            <td>2,210 </td>
                            <td>2,109</td>
                            <td>2,285</td>
                            <td>1,156</td>
                        </tr>
                        <tr>
                            <td class="tb-descp">กำไรสุทธิ </td>
                            <td>7,920</td>
                            <td>8,423 </td>
                            <td>6,478</td>
                            <td>5,682</td>
                            <td>5,527</td>
                        </tr>
                        <tr class="row-highlight">
                            <td class="tb-descp">เงินลงทุนในบริษัทร่วม (วิธีส่วนได้เสีย) </td>
                            <td>18,964</td>
                            <td>17,497 </td>
                            <td>15,778</td>
                            <td>14,204</td>
                            <td>14,379</td>
                        </tr>
                        <tr>
                            <td class="tb-descp">สิทธิการเช่า (วิธีราคาทุนสุทธิ) </td>
                            <td>2,402</td>
                            <td>3,667 </td>
                            <td>6,280</td>
                            <td>6,420</td>
                            <td>6,871</td>
                        </tr>
                        <tr class="row-highlight">
                            <td class="tb-descp">สินทรัพย์รวม </td>
                            <td>98,070</td>
                            <td>86,888 </td>
                            <td>75,369</td>
                            <td>65,040</td>
                            <td>60,833</td>
                        </tr>
                        <tr>
                            <td class="tb-descp">จำนวนหุ้นที่เรียกชำระแล้ว (ล้านหุ้น) </td>
                            <td>11,730</td>
                            <td>10,986 </td>
                            <td>10,026</td>
                            <td>10,026</td>
                            <td>10,026</td>
                        </tr>
                        <tr class="row-highlight">
                            <td class="tb-descp">มูลค่าตามราคาตลาด (ล้านบาท) </td>
                            <td>110,849</td>
                            <td>99,419 </td>
                            <td>89,733</td>
                            <td>97,754</td>
                            <td>61,660</td>
                        </tr>
                        <tr class="row-title">
                            <td class="tb-descp" colspan="6">ข้อมูลต่อหุ้น (บาทต่อหุ้น) </td>
                        </tr>
                        <tr class="row-highlight">
                            <td class="tb-descp">กำไรสุทธิต่อหุ้น </td>
                            <td>0.69</td>
                            <td>0.80</td>
                            <td>0.65</td>
                            <td>0.57</td>
                            <td>0.56</td>
                        </tr>
                        <tr>
                            <td class="tb-descp">เงินปันผลต่อหุ้น </td>
                            <td>0.60</td>
                            <td>0.65</td>
                            <td>0.40</td>
                            <td>0.45</td>
                            <td>0.40</td>
                        </tr>
                        <tr class="row-highlight">
                            <td class="tb-descp">อัตราการจ่ายเงินปันผล </td>
                            <td>89%</td>
                            <td>88%</td>
                            <td>62%</td>
                            <td>80%</td>
                            <td>71%</td>
                        </tr>
                        <tr>
                            <td class="tb-descp">อัตราส่วนเงินปันผลตอบแทน </td>
                            <td>6%</td>
                            <td>7%</td>
                            <td>4%</td>
                            <td>5%</td>
                            <td>7%</td>
                        </tr>
                        <tr class="row-highlight">
                            <td class="tb-descp">ราคาตลาด (ณ วันสิ้นปี) </td>
                            <td>9.45</td>
                            <td>9.05</td>
                            <td>8.95</td>
                            <td>9.75</td>
                            <td>6.15</td>
                        </tr>
                        <tr class="row-title">
                            <td class="tb-descp" colspan="6">อัตราส่วนทางการเงิน </td>
                        </tr>
                        <tr class="row-highlight">
                            <td class="tb-descp">รายได้ค่าเช่า (ต่อรายได้รวม) </td>
                            <td>7.01%</td>
                            <td>7.10% </td>
                            <td>8.30%</td>
                            <td>7.40%</td>
                            <td>2.90%</td>
                        </tr>
                        <tr>
                            <td class="tb-descp">อัตรากำไรขั้นต้นจากการขาย </td>
                            <td>33.93%</td>
                            <td>35.40% </td>
                            <td>35.80%</td>
                            <td>34.00%</td>
                            <td>32.60%</td>
                        </tr>
                        <tr class="row-highlight">
                            <td class="tb-descp">ส่วนแบ่งกำไรจากเงินลงทุนในบริษัทร่วม (ต่อกำไรสุทธิ) </td>
                            <td>30.80%</td>
                            <td>26.20% </td>
                            <td>32.60%</td>
                            <td>28.10%</td>
                            <td>20.90%</td>
                        </tr>
                        <tr>
                            <td class="tb-descp">อัตรากำไรสุทธิ </td>
                            <td>32.70%</td>
                            <td>32.40% </td>
                            <td>28.20%</td>
                            <td>25.50%</td>
                            <td>29.70%</td>
                        </tr>
                        <tr class="row-highlight">
                            <td class="tb-descp">อัตราผลตอบแทนผู้ถือหุ้น </td>
                            <td>17.90%</td>
                            <td>22.00% </td>
                            <td>19.90%</td>
                            <td>18.70%</td>
                            <td>19.40%</td>
                        </tr>
                        <tr>
                            <td class="tb-descp">อัตราส่วนหนี้สินสุทธิต่อส่วนของผู้ถือหุ้น </td>
                            <td>0.70</td>
                            <td>0.65 </td>
                            <td>0.96</td>
                            <td>0.80</td>
                            <td>0.84</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div id="corpBlock5" class="corp-block">
                    <table width="100%" class="table-historical-price analyst-reports">
                        <thead>
                            <tr class="">
                                <th class="tb-title">วันที่</th>
                                <th>หลักทรัพย์</th>
                                <th class="center">ดาวน์โหลด</th>
                            </tr>
                        </thead>
                        <tbody> <tr>
                            <td>17 พฤษภาคม 2559</td>
                            <td>บริษัทหลักทรัพย์ ฟิลลิป (ประเทศไทย) จำกัด (มหาชน)</td>
                            <td class="center"><a href="http://ir.listedcompany.com/tracker.pl?type=5&amp;id=78876&amp;m=f48245ff33a5e97453758fca6af478fcf64ae4649f745ab13f0d76436d7d911e&amp;redirect=http%3A%2F%2Flh.listedcompany.com%2Fmisc%2FAnalystReport%2F20160517-lh-pst-th.pdf" target="_blank">0.36 KB </a></td>
                        </tr>   <tr>
                            <td>16 พฤษภาคม 2559</td>
                            <td>บริษัทหลักทรัพย์ โนมูระ พัฒนสิน จำกัด (มหาชน)</td>
                            <td class="center"><a href="http://ir.listedcompany.com/tracker.pl?type=5&amp;id=78874&amp;m=35995afe92d3d8444ff71ee75a3922d704bd1beb20e7fcfe447f1194d5ca26a3&amp;redirect=http%3A%2F%2Flh.listedcompany.com%2Fmisc%2FAnalystReport%2F20160516-lh-cns-th.pdf" target="_blank">0.43 KB </a></td>
                        </tr>   <tr>
                            <td>13 พฤษภาคม 2559</td>
                            <td>บริษัทหลักทรัพย์ เคจีไอ (ประเทศไทย) จำกัด (มหาชน)</td>
                            <td class="center"><a href="http://ir.listedcompany.com/tracker.pl?type=5&amp;id=78880&amp;m=b24e37ba5a64b00a3722964659a2e8af234447fd0364ef3e47325c09035c66b5&amp;redirect=http%3A%2F%2Flh.listedcompany.com%2Fmisc%2FAnalystReport%2F20160513-lh-kgi-th.pdf" target="_blank">0.14 KB </a></td>
                        </tr>   <tr>
                            <td>22 มกราคม 2559</td>
                            <td>บริษัทหลักทรัพย์ เมย์แบงก์ กิมเอ็ง (ประเทศไทย) จำกัด (มหาชน)</td>
                            <td class="center"><a href="http://ir.listedcompany.com/tracker.pl?type=5&amp;id=74628&amp;m=3960ae7a0b249a2c9a5abdac23b560eb995d881390295df12a7350195683a8ed&amp;redirect=http%3A%2F%2Flh.listedcompany.com%2Fmisc%2FAnalystReport%2F20160122-lh-mbket-th.pdf" target="_blank">0.62 KB </a></td>
                        </tr>   <tr>
                            <td>22 มกราคม 2559</td>
                            <td>บริษัทหลักทรัพย์ ฟิลลิป (ประเทศไทย) จำกัด (มหาชน)</td>
                            <td class="center"><a href="http://ir.listedcompany.com/tracker.pl?type=5&amp;id=74627&amp;m=be6b00719bc178c7a061b8bfc31cdfa2ab171b22fc8eb5363c04bf6fb5315540&amp;redirect=http%3A%2F%2Flh.listedcompany.com%2Fmisc%2FAnalystReport%2F20160122-lh-pst-th.pdf" target="_blank">0.34 KB </a></td>
                        </tr>   <tr>
                            <td>18 มกราคม 2559</td>
                            <td>บริษัทหลักทรัพย์ เคจีไอ (ประเทศไทย) จำกัด (มหาชน)</td>
                            <td class="center"><a href="http://ir.listedcompany.com/tracker.pl?type=5&amp;id=74555&amp;m=6ea9fbd3156ae5ae06d75399938c1be23196fc621eb1a0da37a0d3d988996ee1&amp;redirect=http%3A%2F%2Flh.listedcompany.com%2Fmisc%2FAnalystReport%2F20160118-lh-kgi-th.pdf" target="_blank">0.38 KB </a></td>
                        </tr>   <tr>
                            <td>15 มกราคม 2559</td>
                            <td>บริษัทหลักทรัพย์ บัวหลวง จำกัด (มหาชน)</td>
                            <td class="center"><a href="http://ir.listedcompany.com/tracker.pl?type=5&amp;id=74553&amp;m=dc054547c0c73cb5a2b669c502764865b6b8a997f04973fa80fffe46e58481de&amp;redirect=http%3A%2F%2Flh.listedcompany.com%2Fmisc%2FAnalystReport%2F20160115-lh-bls-th.pdf" target="_blank">0.36 KB </a></td>
                        </tr>   <tr>
                            <td>15 ตุลาคม 2558</td>
                            <td>บริษัทหลักทรัพย์ บัวหลวง จำกัด (มหาชน)</td>
                            <td class="center"><a href="http://ir.listedcompany.com/tracker.pl?type=5&amp;id=72287&amp;m=1c5cb8f7592e6e048db72a5069ac660473308a4f8c96d14e1fa7ab513a9633e3&amp;redirect=http%3A%2F%2Flh.listedcompany.com%2Fmisc%2FAnalystReport%2F20151015-lh-bls-th.pdf" target="_blank">0.38 KB </a></td>
                        </tr>   <tr>
                            <td>15 พฤษภาคม 2556</td>
                            <td>บริษัทหลักทรัพย์ เมย์แบงก์ กิมเอ็ง (ประเทศไทย) จำกัด (มหาชน)</td>
                            <td class="center"><a href="http://ir.listedcompany.com/tracker.pl?type=5&amp;id=49409&amp;m=37b644c92885a5df2f45320408ffb74ce8fa19cac82725b5d3e7a3778de33c7c&amp;redirect=http%3A%2F%2Flh.listedcompany.com%2Fmisc%2FAnalystReport%2F20130515-LH-MBKET-TH.pdf" target="_blank">0.53 KB </a></td>
                        </tr>   <tr>
                            <td>15 พฤษภาคม 2556</td>
                            <td>บริษัท หลักทรัพย์ ฟินันเซีย ไซรัส จำกัด (มหาชน)</td>
                            <td class="center"><a href="http://ir.listedcompany.com/tracker.pl?type=5&amp;id=49405&amp;m=925f69518437fced9609b488488b44826652b0ed5f0ec4e492a42a18699bf038&amp;redirect=http%3A%2F%2Flh.listedcompany.com%2Fmisc%2FAnalystReport%2F20130515-LH-FSS-TH.pdf" target="_blank">0.21 KB </a></td>
                        </tr>   <tr>
                            <td>14 พฤษภาคม 2556</td>
                            <td>บริษัทหลักทรัพย์ บัวหลวง จำกัด (มหาชน)</td>
                            <td class="center"><a href="http://ir.listedcompany.com/tracker.pl?type=5&amp;id=49407&amp;m=919702d4e2933c69b0be1b7e29d4c740dd92ccbae4a28ab7fc535f7f7b92d1aa&amp;redirect=http%3A%2F%2Flh.listedcompany.com%2Fmisc%2FAnalystReport%2F20130514-LH-BLS-TH.pdf" target="_blank">0.45 KB </a></td>
                        </tr>   <tr>
                            <td>03 พฤษภาคม 2556</td>
                            <td>บริษัท หลักทรัพย์ ฟินันเซีย ไซรัส จำกัด (มหาชน)</td>
                            <td class="center"><a href="http://ir.listedcompany.com/tracker.pl?type=5&amp;id=48784&amp;m=d9a5f435ca4f632e85c84a9f14802de9e519775491806874ad3d57ef75f02fc0&amp;redirect=http%3A%2F%2Flh.listedcompany.com%2Fmisc%2FAnalystReport%2F20130503-LH-FSS-TH.pdf" target="_blank">0.24 KB </a></td>
                        </tr>   <tr>
                            <td>01 มีนาคม 2556</td>
                            <td>บริษัทหลักทรัพย์ กรุงศรีอยุธยา จำกัด (มหาชน)</td>
                            <td class="center"><a href="http://ir.listedcompany.com/tracker.pl?type=5&amp;id=47070&amp;m=3b06551cae123b78f0c3df822e3538b2296b7102861a73443ffcb4ea5345f3f7&amp;redirect=http%3A%2F%2Flh.listedcompany.com%2Fmisc%2FAnalystReport%2F20130301-LH-KSS-TH.pdf" target="_blank">0.23 KB </a></td>
                        </tr>   <tr>
                            <td>19 พฤศจิกายน 2555</td>
                            <td>บริษัทหลักทรัพย์ ทรีนีตี้ จำกัด</td>
                            <td class="center"><a href="http://ir.listedcompany.com/tracker.pl?type=5&amp;id=43971&amp;m=ee0a4865f2da93b45d7b26728012f96cbb89e0e73ad04cfb6a0ffc0922ea8147&amp;redirect=http%3A%2F%2Flh.listedcompany.com%2Fmisc%2FAnalystReport%2F20121114-LH-TNITY-TH.pdf" target="_blank">0.41 KB </a></td>
                        </tr>   <tr>
                            <td>15 พฤศจิกายน 2555</td>
                            <td>บริษัทหลักทรัพย์ เอเซีย พลัส จำกัด (มหาชน)</td>
                            <td class="center"><a href="http://ir.listedcompany.com/tracker.pl?type=5&amp;id=43965&amp;m=1b8a6c9020f26263dea8847c40139abf09b0313cb31fc182b74bc55803c65f4f&amp;redirect=http%3A%2F%2Flh.listedcompany.com%2Fmisc%2FAnalystReport%2F20121115-LH-ASP-TH.pdf" target="_blank">0.26 KB </a></td>
                        </tr>   <tr>
                            <td>14 พฤศจิกายน 2555</td>
                            <td>บริษัทหลักทรัพย์ กรุงศรีอยุธยา จำกัด (มหาชน)</td>
                            <td class="center"><a href="http://ir.listedcompany.com/tracker.pl?type=5&amp;id=43970&amp;m=a34c53c44f2e44ab64e78630fd99bfe7d4079176d6baf133f0a179fe9e054785&amp;redirect=http%3A%2F%2Flh.listedcompany.com%2Fmisc%2FAnalystReport%2F20121114-LH-KSS-TH.pdf" target="_blank">0.34 KB </a></td>
                        </tr>   <tr>
                            <td>12 กรกฎาคม 2555</td>
                            <td>บริษัทหลักทรัพย์ ทรีนีตี้ จำกัด</td>
                            <td class="center"><a href="http://ir.listedcompany.com/tracker.pl?type=5&amp;id=40805&amp;m=6584c9163964899235f048b2d1fa831baf314b5bb58b6491e07eb1dc9e35d487&amp;redirect=http%3A%2F%2Flh.listedcompany.com%2Fmisc%2FAnalystReport%2F20120712-LH-TNITY-TH.pdf" target="_blank">0.42 KB </a></td>
                        </tr>   <tr>
                            <td>01 พฤศจิกายน 2554</td>
                            <td>บริษัท หลักทรัพย์ ฟินันเซีย ไซรัส จำกัด (มหาชน)</td>
                            <td class="center"><a href="http://ir.listedcompany.com/tracker.pl?type=5&amp;id=33010&amp;m=392774d22beb91a791d77d2ba089af9370a8e3bb29e8102b8cafd6445066b2ae&amp;redirect=http%3A%2F%2Flh.listedcompany.com%2Fmisc%2FAnalystReport%2F20111101-LH-FSS-TH.pdf" target="_blank">0.25 KB </a></td>
                        </tr>   <tr>
                            <td>20 ตุลาคม 2554</td>
                            <td>บริษัทหลักทรัพย์ เอเซีย พลัส จำกัด (มหาชน)</td>
                            <td class="center"><a href="http://ir.listedcompany.com/tracker.pl?type=5&amp;id=32875&amp;m=b271df97a5bb92f69ec8df3810af16472b1d8bb1e67e50624505c254e43a05d9&amp;redirect=http%3A%2F%2Flh.listedcompany.com%2Fmisc%2FAnalystReport%2F20111020-LH-ASP-TH.pdf" target="_blank">0.21 KB </a></td>
                        </tr>         </tbody></table>


                </div>
            </div>
        </div>
    </div>
</div>


<?php include('footer.php'); ?>
