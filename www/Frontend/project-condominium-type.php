<?php include('header.php'); ?>
    <!-- JS -->
    <link rel="stylesheet" href="css/project-condominium-type.css" type="text/css">

    <script src="js/project-condominium-type.js"></script>

    <div class="page-banner page-scroll">
        <span id="scrollNext" class="i-scroll-next"></span>

        <div id="bannerSlide" class="owl-carousel">
            <div class="item">
                <div class="banner-parallax" style="background-image: url(images/home/banner_home1.jpg);">
                    <div class="bg-slide">
                        <div class="banner-txt">
                            <h1>LADAWAN</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="banner-parallax" style="background-image: url(images/home/banner_home2.jpg);">
                    <div class="bg-slide">
                        <div class="banner-txt">
                            <h1>NANTAWAN</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="banner-parallax" style="background-image: url(images/home/banner_home3.jpg);">
                    <div class="bg-slide">
                        <div class="banner-txt">
                            <h1>MANTANA</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="banner-parallax" style="background-image: url(images/home/banner_home6.jpg);">
                    <div class="bg-slide">
                        <div class="banner-txt">
                            <h1>ชัยพฤกษ์</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="banner-parallax" style="background-image: url(images/home/banner_home7.jpg);">
                    <div class="bg-slide">
                        <div class="banner-txt">
                            <h1>พฤกษลดา</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="banner-parallax" style="background-image: url(images/home/banner_home8.jpg);">
                    <div class="bg-slide">
                        <div class="banner-txt">
                            <h1>Inizio</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="banner-parallax" style="background-image: url(images/home/banner_home9.jpg);">
                    <div class="bg-slide">
                        <div class="banner-txt">
                            <h1>Villagio</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="content" class="content project-type-page">
        <div class="container">
            <h1>โครงการบ้านเดี่ยวทั้งหมดของ LH</h1>
            <div class="project-type-block">
                <h2>ขอแนะนำบ้านเดี่ยวคุณภาพ 3 โครงการ</h2>
                <div class="sort-container">
                    <div class="sort-block">
                        <input type="hidden" name="sortSelect" value="">
                        <div class="sort" data-status="0">
                            <span>เลือกทำเลที่สนใจ</span> <i class="i-sort"></i>
                        </div>
                        <ul id="sortLists">
                            <li data-value="กรุงเทพ และปริมณฑล">กรุงเทพ และปริมณฑล</li>
                            <li data-value="อยุธยา">อยุธยา</li>
                            <li data-value="สมุทรสาคร">สมุทรสาคร</li>
                            <li data-value="เชียงราย">เชียงราย</li>
                            <li data-value="เชียงใหม่">เชียงใหม่</li>
                        </ul>
                    </div>

                    <div class="view-block">
                        <ul>
                            <li><a href="" class="i-view-list active"></a></li>
                            <li><a href="" class="i-view-grid"></a></li>
                            <li><a href="" class="i-view-grid-more"></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                </div>

                <input type="hidden" id="status_list" value="list">

                <div id="products" class="list-group">

                    <div class="projecttype-list">
                        <div class="row">
                            <div class="col-md-8">
                                <img src="images/temp/pjt_img1.jpg" alt="">
                            </div>
                            <div class="col-md-4">
                                <div class="projecttype-descrp">
                                    <img src="images/logo/logo_chaiyp.jpg" alt="">
                                    <p class="title">บ้านชัยพฤกษ์ บางนา กม.7</p>
                                    <p>ข้อมูลแนวคิดโครงการ 2 บรรทัด<br>
                                        ราคาเริ่มต้น 4.39 - 6 ล้านบาท</p>
                                    <a href="<?php url('/project-info-home.php'); ?>" class="btn btn-seemore">See more</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="projecttype-list">
                        <div class="row">
                            <div class="col-md-8">
                                <img src="images/temp/pjt_img1.jpg" alt="">
                            </div>
                            <div class="col-md-4">
                                <div class="projecttype-descrp">
                                    <img src="images/logo/logo_chaiyp.jpg" alt="">
                                    <p class="title">บ้านชัยพฤกษ์ บางนา กม.7</p>
                                    <p>ข้อมูลแนวคิดโครงการ 2 บรรทัด<br>
                                        ราคาเริ่มต้น 4.39 - 6 ล้านบาท</p>
                                    <a href="<?php url('/project-info-home.php'); ?>" class="btn btn-seemore">See more</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="projecttype-list">
                        <div class="row">
                            <div class="col-md-8">
                                <img src="images/temp/pjt_img1.jpg" alt="">
                            </div>
                            <div class="col-md-4">
                                <div class="projecttype-descrp">
                                    <img src="images/logo/logo_chaiyp.jpg" alt="">
                                    <p class="title">บ้านชัยพฤกษ์ บางนา กม.7</p>
                                    <p>ข้อมูลแนวคิดโครงการ 2 บรรทัด<br>
                                        ราคาเริ่มต้น 4.39 - 6 ล้านบาท</p>
                                    <a href="<?php url('/project-info-home.php'); ?>" class="btn btn-seemore">See more</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="projecttype-list">
                        <div class="row">
                            <div class="col-md-8">
                                <img src="images/temp/pjt_img1.jpg" alt="">
                            </div>
                            <div class="col-md-4">
                                <div class="projecttype-descrp">
                                    <img src="images/logo/logo_chaiyp.jpg" alt="">
                                    <p class="title">บ้านชัยพฤกษ์ บางนา กม.7</p>
                                    <p>ข้อมูลแนวคิดโครงการ 2 บรรทัด<br>
                                        ราคาเริ่มต้น 4.39 - 6 ล้านบาท</p>
                                    <a href="<?php url('/project-info-home.php'); ?>" class="btn btn-seemore">See more</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>


<?php include('footer.php'); ?>