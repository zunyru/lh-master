<?php

header('Location: //'.$_SERVER['SERVER_NAME'].'/th');
exit();

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

$page = 'LH';
$page_index = 0;

function changeLangSwitch($lang){
	$curr_link=(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	if ($lang=='en') {
		$link=str_replace("lh.co.th","lh.co.th/en",$curr_link);
	}elseif($lang=='cn'){
		$link=str_replace("lh.co.th","lh.co.th/cn",$curr_link);
	}else{
		$link=str_replace("lh.co.th","lh.co.th/th",$curr_link);
	}
	return $link;
}

?>
<!DOCTYPE html>
<?php $lang = !empty($_GET['lang']) ? $_GET['lang'] : 'th'; ?>
<html lang="th">
<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NQRNBR');</script>
<!-- End Google Tag Manager -->

<?php include "title.php"; ?>    

<?php if($_SERVER['REQUEST_URI']=='/'): ?>
	<!-- Edited by Yothin MFEC PCL -> ORGANIZATION STRUCTURED DATA for only Homepage -->
	<script type="application/ld+json"> { "@context" : "https://schema.org", "@type" : "Organization", "name" : "Land & Houses Public Company Limited", "image" : "https://www.lh.co.th/www/Frontend/images/global/header_logo.png", "description" : "บริษัท แลนด์ แอนด์ เฮ้าส์ จากัด (มหาชน) ประกอบธุรกิจประเภท ค้าอสังหาริมทรัพย์ โดยขายบ้านจัดสรรพร้อมที่เป็นส่วนใหญ่ โครงการที่ทาจะเป็นโครงการในเขตกรุงเทพมหานคร ปริมณฑลและโครงการตามจังหวัดใหญ่ๆ ได้แก่ เชียงใหม่ เชียงราย ขอนแก่น นครราชสีมา อุดรธานี หัวหิน ภูเก็ต มหาสารคาม และอยุธยา.", "address" : { "@type" : "PostalAddress", "addressCountry" : "TH", "addressLocality" : "Bangkok", "postalCode" : "10210", "streetAddress" : "Q. House Lumpini Building Fl. 37-38,, 1 South Satorn Rd. Tungmahamek, Sathon" }, "url" : "https://www.lh.co.th", "telephone" : "+66 (0) 2230 8657", "logo" : "https://www.lh.co.th/www/Frontend/images/global/header_logo.png", "sameAs" : ["https://www.facebook.com/landandhouses", "https://twitter.com/lhhome", "https://www.youtube.com/user/LandandHouses", "https://www.instagram.com/lhhome/"] }
	</script>
<?php endif;?>
<!-- Add canonical in each pages -->
<link rel="canonical" href="<?=$url_pure?>" />
<title>บริษัท แลนด์ แอนด์ เฮ้าส์ จำกัด (มหาชน)</title>
<meta name="description"            content="Land & Houses บริษัทธุรกิจอสังหาริมทรัพย์ในประเทศไทย LHเปิดขายทั้งโครงการบ้านเดี่ยว ทาวน์โฮม คอนโดมิเนียมในกรุงเทพฯ ปริมณฑลและต่างจังหวัด" />
<meta name="keywords"               content="บ้านเดี่ยว, ทาวน์โฮม, คอนโด, คอนโดมิเนียม, โครงการบ้านใหม่" />

<meta charset="UTF-8">
<meta name="robots" content="noindex, nofollow">
<meta name="viewport"
content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<link rel="shortcut icon" href="<?= file_path('images/global/favicon.png') ?>" type="image/x-icon">

<!-- CSS -->
<link rel="stylesheet" href="<?= file_path('css/lib/bootstrap.min.css') ?>" type="text/css">
<?php if(@$page == 'job'){?>
	<link rel="stylesheet" href="<?= file_path('css/datepicker.css') ?>" type="text/css">
<?php }else{?>
	<link rel="stylesheet" href="<?= file_path('css/lib/bootstrap-datepicker.css') ?>" type="text/css">
<?php }?>
<link rel="stylesheet" href="<?= file_path('owl.carousel/assets/owl.carousel.css') ?>" type="text/css">
<link rel="stylesheet" href="<?= file_path('css/fonts.css') ?>" type="text/css">
<link rel="stylesheet" href="<?= file_path('css/global.css') ?>" type="text/css">
<link rel="stylesheet" href="<?= file_path('css/lib/featherlight.min.css') ?>" type="text/css">
<link rel="stylesheet" href="<?= file_path('css/lib/featherlight.gallery.min.css') ?>" type="text/css">


<!-- JS -->
<script src="<?= file_path('js/jquery-2.2.3.min.js') ?>"></script>
<script src="<?= file_path('js/lib/bootstrap.min.js') ?>"></script>

<?php if(@$page == 'job'){?>
	<script src="<?= file_path('js/bootstrap-datepicker.js') ?>"></script>
	<script src="<?= file_path('js/bootstrap-datepicker-custom.js')?> "></script>
	<script src="<?= file_path('js/locales/bootstrap-datepicker.th.js') ?>"></script>
<?php }else{?>
	<script src="<?= file_path('js/lib/bootstrap-datepicker.min.js') ?>"></script>

<?php }?>
<script src="<?= file_path('owl.carousel/owl.carousel.min.js') ?>"></script>
<script src="<?= file_path('js/lib/parallax.min.js') ?>"></script>
<script src="<?= file_path('js/lib/featherlight.min.js') ?>"></script>
<script src="<?= file_path('js/lib/featherlight.gallery.min.js') ?>"></script>
<script src="<?= file_path('js/lib/freewall.js') ?>"></script>
<script src="<?= file_path('js/global.js') ?>"></script>
<script src="<?= file_path('js/search-form.js') ?>"></script>
<script src="<?= file_path('js/get-direction-form.js') ?>"></script>

<style type="text/css">
	.btn-th{
		padding-right: 22px;
	}
	.btn-en{
		padding-right: 26px;
	}
	.btn-cn{
		padding-right: 23px;
	}
</style>
</head>
<body>
	
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NQRNBR"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->


		<div id="preloader">
			<div id="preloadLogo">
				<img src="<?= file_path('images/global/loading.gif') ?>" alt="" class="loading-img">
				<p>Loading...</p>
			</div>
		</div>

		<div id="page" class="page_landing_home">
			
			<!-- Facebook Pixel Code -->
			<script>
				!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
					n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
					n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
					t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
						document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '342952452528044'); // Insert your pixel ID here.
        fbq('track', 'PageView');
    </script>
        <noscript><img height="1" width="1" style="display:none"
    	src="https://www.facebook.com/tr?id=342952452528044&ev=PageView&noscript=1"
    	/>
    </noscript>
    	<!-- DO NOT MODIFY -->
    	<!-- End Facebook Pixel Code -->
    	<div id="header" class="landing_home">
    		
    		<div class="landing-pc">
    			<div class="header-container">
    				<a href="<?= changeLangSwitch('th') ?>"  title="" alt="">
    					<img src="http://dummyimage.com/1920x924/4d494d/686a82.gif&text=1920x924px" alt="placeholder+image">
    				</a>
    			</div>
    		</div>
    		<div class="landing-mb">
    			<div class="header-container">
    				<a href="<?= changeLangSwitch('th') ?>"  title="" alt="">
    					<img src="http://dummyimage.com/375x540/4d494d/686a82.gif&text=375x540px" alt="placeholder+image">
    				</a>
    			</div>
    		</div>
    		
    	</div>
    </div>
    	
    	