<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';
require_once 'include/help/query_independent.php';
require_once 'include/help/Helper.php';
$page = 'living';
$page_index = 0;
$img = '';
?>
<?php
include('header.php');
//elementMetaTitleDisTag($page);
?>
    <!-- CSS -->
    <link rel="stylesheet" href="<?= file_path('css/living-tips.css') ?>" type="text/css">
    <!-- JS -->
    <script src="<?= file_path('js/living-tips.js') ?>"></script>
    <script src="<?= file_path('js/imagesloaded.pkgd.min.js') ?>"></script>


    <div id="content" class="content">
        <div class="container">

        
        <?php
        $SEO = getSEOUrl($actual_link);
        if($actual_link == @$SEO->url_page){
            echo '<h1 class="heading-title">'.$SEO->h1.'<h1>';
        }else{
           echo '<h1 class="heading-title">LH Living Tips</h1>'; 
       }
       ?>

        <ul class="menu-livingtip">
            <li><a href="<?= $router->generate('living-tips',['category' => 'homeliving' ]) ?>"><i class="i-homeliving"></i>Home & Living</a></li>
            <li><a href="<?= $router->generate('living-tips',['category' => 'concept' ]) ?>"><i class="i-concept"></i>Living Concept</a></li>
        </ul>
        <div id="livingtipList">
            <div class="row">
                <div class="livingtip-lists">
                    <?php
                    $living_tips = getLivingTip();
                    foreach($living_tips as $living_tip) {
                        if( empty($_GET['category']) || (!empty($_GET['category']) && $_GET['category'] == $living_tip->living_tip_category ) ) {
                            
                            if($living_tip->custom_url!=''){
                                $url_name = $living_tip->custom_url; 
                            }else{
                                $url_name =$living_tip->living_tip_name_th;
                            }
                            ?>
                            <div class="list-item item">
                                <div class="review-img">
                                    <img src="<?= backend_url('base',$living_tip->living_tip_img) ?>" alt="">
                                </div>
                                <a href="<?= $router->generate('living-tips-detail',['living_tip_name' => str_replace(' ','-',$living_tip->living_tip_name_th) ]) ?>">
                                    <p class="title"><i class="i-<?= $living_tip->living_tip_category ?>"></i>
                                        <?= Helper::lang('th', $living_tip->living_tip_name_th, $living_tip->living_tip_name_en) ?>
                                    </p>
                                    <p>
                                        <?= Helper::lang('th', $living_tip->living_tip_content_th, $living_tip->living_tip_content_en) ?>
                                    </p>
                                </a>
                                <a href="<?= $router->generate('living-tips-detail',['living_tip_name' => str_replace(' ','-',$url_name) ]) ?>" class="btn btn-seemore">See more</a>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
        
    </div>
</div>


<?php include('footer.php'); ?>
