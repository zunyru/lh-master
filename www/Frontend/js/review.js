$(document).ready(function () {
    //Page Load Start
    reviewList();

    //Page Load End
});


//Function Start
function masonryList() {
    var wall = new Freewall(".review-lists");
    wall.reset({
        selector: '.list-item',
        animate: true,
        cellW: 180,
        cellH: 'auto',
        onResize: function() {
            wall.fitWidth();
        }
    });

    //wall.container.find('.list-item img').load(function() {
        wall.fitWidth();
    //});
}

function reviewList() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        // var stagePadding = 40;
        // $('.review-lists').owlCarousel({
        //     loop:true,
        //     margin: 10,
        //     stagePadding: stagePadding,
        //     nav:true,
        //     dots: true,
        //     autoHeight: true,
        //     responsive:{
        //         0:{
        //             items:1
        //         },
        //         768:{
        //             items:1
        //         }
        //     }
        // });
    }
    else {
        $('.review-lists').imagesLoaded( function() {
            masonryList();
        });
    }



}


//Function End