$(document).ready(function () {
    //Page Load Start
    corpMenu();
    reportList();
    annualList();
    //Page Load End
});


//Function Start


function corpMenu() {
    $('#corpMenu li a').click(function(e) {
        e.preventDefault();
        $(this).parent().addClass('current');
        $(this).parent().siblings().removeClass('current');
        var tab = $(this).attr('href');
        $('.corp-block').not(tab).css('display', 'none');
        $(tab).fadeIn();
    });

}



function reportList() {
        $('#invReport').owlCarousel({
            nav:true,
            dots: true,
            responsive:{
                0:{
                    items:2
                },
                768:{
                    items:4
                }
            }
        });


}

function annualList() {
    $('.inv-report-block').owlCarousel({
        nav:true,
        dots: false,
        responsive:{
            0:{
                items:2
            },
            768:{
                items:4
            }
        }
    });


}
//Function End