$(document).ready(function () {
    //Page Load Start
    bannerSlide();
    scrollNext();
    var status_list = $('#status_list');
    $('.i-view-list').click(function(event){
        event.preventDefault();
        if(status_list.val()=='grid2'){
            grid2ToList();
        }else if(status_list.val()=='grid3'){
            grid3ToList();
        }
        $('#status_list').val('list');
    });
    $('.i-view-grid').click(function(event){
        event.preventDefault();
        if(status_list.val()=='grid3'){
            grid3ToList();
            listToGrid2();
        }else if(status_list.val()=='list'){
            listToGrid2();
        }
        status_list.val('grid2');
    });
    $('.i-view-grid-more').click(function(event){
        event.preventDefault();
        if(status_list.val()=='grid2'){
            grid2ToList();
            listToGrid3();
        }else if(status_list.val()=='list'){
            listToGrid3();
        }
        status_list.val('grid3');
    });
    //Page Load End
});


//Function Start

function bannerSlide() {
    var winW = $(window).width();
    var winH = $(window).height();
    var bannerH = $('.page-banner').height();

    //Center text
    var slideTxt = $('.banner-txt').height();

    //Full height
    if( winW <= 768 ) {
        //$('.banner-parallax, .page-banner').css('height', 180);
        var center = (bannerH - 100 ) / 2;
    }
    else {
        //$('.banner-parallax').css('height', winH - 60);
        var center = (bannerH - 290) / 2;
    }

    $('.banner-txt').css('top',center);

    var isMulti = ($('#bannerSlide.owl-carousel img').length > 1) ? true : false;
    var isMulti2 = ($('#bannerSlide_count').val() > 1) ? true : false;
    //Slide
    $('#bannerSlide').owlCarousel({
        loop:isMulti,
        autoplay: true,
        autoplayTimeout: 4000,
        autoplaySpeed: 1000,
        margin:5,
        animateOut: 'fadeOut',
        nav:false,
        dots: isMulti2,
        video:true,
        lazyLoad:true,
        center:true,
        responsive:{
            0:{
                items:1
            },
        }
    });
}

function scrollNext() {
    $("#scrollNext").click(function() {
        $('html, body').animate({
            scrollTop: $("#content").offset().top
        }, 800);
    });
}

function listToGrid3(){
    $('#products .projecttype-list .projecttype-descrp').removeClass('projecttype-descrpgrid2').addClass('grid2');
    $('#products .projecttype-list .grid3').removeClass('grid3').addClass('group inner list-group-item-text');
    $('#products .projecttype-list .col-md-8').removeClass('col-md-8').addClass('thumbnail');
    $('#products .projecttype-list .col-md-4').removeClass('col-md-4').addClass('caption');
    $('#products .projecttype-list .row').removeClass('row').addClass('grid1');
    $('#products .projecttype-list').removeClass('projecttype-list').addClass('item col-xs-4 col-lg-4');
}
function listToGrid2(){
    $('#products .projecttype-list .projecttype-descrp').removeClass('projecttype-descrpgrid2').addClass('grid2');
    $('#products .projecttype-list .grid3').removeClass('grid3').addClass('group inner list-group-item-text');
    $('#products .projecttype-list .col-md-8').removeClass('col-md-8').addClass('thumbnail');
    $('#products .projecttype-list .col-md-4').removeClass('col-md-4').addClass('caption');
    $('#products .projecttype-list .row').removeClass('row').addClass('grid1');
    $('#products .projecttype-list').removeClass('projecttype-list').addClass('item col-xs-6 col-lg-6');
}
function grid3ToList(){
    $('#products .item .grid2').removeClass('grid2').addClass('projecttype-descrp');
    $('#products .item .grid2 .group').removeClass('group inner list-group-item-text').addClass('grid3');
    $('#products .item .thumbnail').removeClass('thumbnail').addClass('col-md-8');
    $('#products .item .caption').removeClass('caption').addClass('col-md-4');
    $('#products .item .grid1').removeClass('grid1').addClass('row');
    $('#products .item').removeClass('item col-xs-4 col-lg-4').addClass('projecttype-list');
}
function grid2ToList(){
    $('#products .item .grid2').removeClass('grid2').addClass('projecttype-descrp');
    $('#products .item .grid2 .group').removeClass('group inner list-group-item-text').addClass('grid3');
    $('#products .item .thumbnail').removeClass('thumbnail').addClass('col-md-8');
    $('#products .item .caption').removeClass('caption').addClass('col-md-4');
    $('#products .item .grid1').removeClass('grid1').addClass('row');
    $('#products .item').removeClass('item col-xs-6 col-lg-6').addClass('projecttype-list');
}

//Function End