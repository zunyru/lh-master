$(document).ready(function () {
    //Page Load Start
    tpl1Block2List();


});


//Function Start

function tpl1Block2List() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
    }
    else {
        var stagePadding = 0;

        var isMulti = ($('#block2List.owl-carousel img').length > 1) ? true : false;
        $('#block2List').owlCarousel({
            loop:isMulti,
            margin: 10,
            stagePadding: stagePadding,
            nav:true,
            dots: false,
            responsive:{
                979:{
                    items:2
                },
                1199:{
                    items:3
                }
            }
        });


    }


}



//Function End