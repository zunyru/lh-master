$(document).ready(function () {
    //Page Load Start
    gallery();
    colSlide();
    homePlanSlide();
    tabMenu();

    //Page Load End
    $('.gallery').featherlightGallery();
});


//Function Start

function gridLayout5() {
    var wall = new Freewall("#tpl5-img");
    wall.reset({
        selector: '.item',
        animate: true,
        cellW: 300,
        cellH: 'auto',
        onResize: function() {
            wall.fitWidth();
        }
    });

    var images = wall.container.find('.item');
    images.find('img').load(function() {
        wall.fitWidth();
    });
}

function gallery() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        var stagePadding = 40;
        $('#tpl5-img').owlCarousel({
            loop:true,
            margin: 30,
            stagePadding: stagePadding,
            nav:true,
            dots: true,
            responsive:{
                0:{
                    items:1
                },
                768:{
                    items:1
                }
            }
        });
    }
    else {
        gridLayout5();
    }
}

function tabMenu() {
    $('.tabs-menu a').click(function(e) {
        e.preventDefault();
        $(this).parent().addClass('current');
        $(this).parent().siblings().removeClass('current');
        var tab = $(this).attr('href');
        $('.tab-content').not(tab).css('display', 'none');
        $(tab).fadeIn();
    });

}

function homePlanSlide() {

    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        var stagePadding = 50;
    }
    else {
        var stagePadding = 0;
    }

    var homeSlider = $("#homePlanSlide");
    homeSlider.owlCarousel({
        loop:true,
        margin: 10,
        stagePadding: stagePadding,
        nav:true,
        dots: true,
        responsive:{
            0:{
                items:1
            },
        }
    });
    homeSlider.on("click", ".tabs-menu-plan-slide a", function () {
        var value = $(this).attr('data-value');
        homeSlider.trigger("to.owl.carousel", value);
    });
}

function colSlide() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        var stagePadding = 50;
    }
    else {
        var stagePadding = 0;
    }

    $('#colSlide').owlCarousel({
        loop:true,
        margin: 10,
        stagePadding: stagePadding,
        nav:true,
        dots: true,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:1
            },
            979:{
                items:2
            },
            1199:{
                items:3
            }
        }
    });
}


//Function End