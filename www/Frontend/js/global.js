$(document).ready(function () {


    $("#page.page_landing").click(function () { 
        window.location = 'https://uat.lh.co.th/th';
    })


    //Page Load Start
    preloader();
    menu();
    //centerMainNav();
    menuHomeSeries();
    submenuInnerpage();
    responsiveMenu();
    searchDropdown();
    getdirectionDropdown();
    contactDropdown();
    sortBlock();
    scrollNext();
    scrollTop();
    tabMenu();
    projSubmenu();

    hideMenu();

    shareFacebook();
    shareTwitter();
    shareLine();

    //radionAppoint();

    var winW = $(window).width();
    var winH = $(window).height();

    var pageH = winH - 60;
    // $('.page-banner, .topdropdown-container, .page-banner-content').css('height',pageH);
    $('.topdropdown-container, .page-banner-content').css('height',pageH);

    // var imgH = $('.page-banner .item img').height();
    // console.log('win h = ' + winH);
    // console.log('img h = ' + imgH);
    // $('#scrollNext').css('top',pageH - 100);
    // $('.page-banner .owl-controls').css('top',pageH - 25);
    //
    //
    //

    // if(imgH < winH) {
    //     $('#scrollNext').css('top',imgH - 70);
    // }else {
    //     $('#scrollNext').css('top',pageH - 70);
    // }



    var mainmenuW = $('.main-navigation #main-nav').width();
    var menuHf =  (mainmenuW - 100 ) / 2;
    var centerMenu =  '-'+ Math.round(menuHf) + 'px';
    $('.main-navigation').css('margin-left', centerMenu);


    // reponsive
    if( winW < 992 ) {
        var funBlockH = winH;
        $('.topdropdown-container').css('height', funBlockH);
        $('.topdropdown-container').css('height',funBlockH);
    }

    $(window).resize(function(){
        var winW = $(window).width();
        var winH = $(window).height();
        var pageH = winH - 60;
        // $('.page-banner, .topdropdown-container, .page-banner-content').css('height',pageH);
        $('.topdropdown-container, .page-banner-content').css('height',pageH);


        var imgH = $('.page-banner .item img').height();

        var boxActH = $('.banner-activity').height();
        // console.log('win h = ' + winH);
        // console.log('slide h = ' + pageH);
        // console.log('box h = ' + boxActH);
        // console.log('img h = ' + imgH);


        //$('#scrollNext').css('top',pageH - 100);
        if( winW > 768 ) {
            // if(imgH < winH) {
            //     $('#scrollNext').css({'top' : 'initial', 'bottom' : '50px'} );
            //     $('.banner-activity').css( {'top' : 'initial', 'bottom' : '20px'} );
            //     $('.page-banner .owl-controls').css( { 'top' : 'initial' , 'bottom' : '10px'} );

            // }else {
                $('#scrollNext').css('top',pageH - 100);
                $('.banner-activity').css('top', pageH - ( boxActH + 40 ) );
                $('.page-banner .owl-controls').css('top',pageH - 35 );
            //}
        }
        if( winW > 768 ) {
            bannerBtn();
        }

        //page - project info ..
        var pBannerImgH = $('.page-banner .background-show').height();
        $('.page-banner .item.slide').css('height',pBannerImgH);
        $('.page-banner .item .banner-logo').css('height',pBannerImgH);


        if( winW < 992 ) {
            var funBlockH = winH;
            $('.topdropdown-container').css('height',funBlockH);
        }

    });

    // var winH = $(window).height();
    // var pageH = winH - 70

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= pageH) {
            $('.scroll-top').addClass('fadeIn');
            $('.float-share').css('bottom','90px');
            $('.ficon-shares').css('bottom','170px');
            if(winW < 992 ) {
                $('.float-share').css('bottom','115px');
                $('.ficon-shares').css('bottom','175px');
            }
        }
        else {
            $('.scroll-top').removeClass('fadeIn');
            $('.float-share').css('bottom','20px');
            $('.ficon-shares').css('bottom','100px');
            if(winW < 992 ) {
                $('.float-share').css('bottom','55px');
                $('.ficon-shares').css('bottom','115px');
            }
                //$('.main-navigation').css({'opacity': 0, 'visibility':'hidden', 'display':'none' });
            // $('.main-navigation, .top-icons, .menu-home-series').fadeOut();
        }


        if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
            $('.page-banner').css({'opacity': 0, 'visibility':'hidden' });
        }
        else{
            $('.page-banner').css({'opacity': 1, 'visibility':'visible' });
        }

    });

    var pos = $('.content').offset().top;
    //console.log(pos);

    $('.view-block li a').click(function(e){
        $('.view-block li a').removeClass('active');
        $(this).addClass('active');
    });

    $('.float-share').click(function(e){
        var status = $(this).data('status');
        if(status == 0) {
            $(this).data('status', 1)
            $('.ficon-shares').css({'opacity': 1, 'visibility':'visible' });
        }
        else {
            $(this).data('status', 0)
            $('.ficon-shares').css({'opacity': 0, 'visibility':'hidden' });
        }

        e.preventDefault();
    });

    $(".line").click(function(){
        $(".line-share").fadeToggle();
    });

    $(".wechat").click(function(){
        $(".wechat-share").fadeToggle();
    });

    $('.banner-activity.banner-rsp .close-acty').click(function(e){
        $('.banner-activity.banner-rsp').hide();
        $('.banner-parallax banner-dktp').show();
        e.preventDefault();
    });



    //Page Load End
});

$(window).load(function(){

    var winH = $(window).height();
    var winW = $(window).width();
    var pageH = winH - 60;
    var imgH = $('.page-banner .item img').height();
    
    var boxActH = $('.banner-activity').height();
    //console.log('win h = ' + winH);
    // console.log('slide h = ' + pageH);
    // console.log('box h = ' + boxActH);
    //console.log('img h = ' + imgH);

    if( winW > 768 ) {
        // if(imgH < winH) {
        //     $('#scrollNext').css({'top' : 'initial', 'bottom' : '50px'} );
        //     $('.banner-activity').css( {'top' : 'initial', 'bottom' : '20px'} );
        //     $('.page-banner .owl-controls').css( { 'top' : 'initial' , 'bottom' : '10px'} );
        // }else {
            $('#scrollNext').css('top',pageH - 100);
            $('.banner-activity').css('top', pageH - ( boxActH + 40 ) );
            $('.page-banner .owl-controls').css('top',pageH - 35 );
        //}
    }
    else {
        $('#scrollNext').css('top',pageH - 130);
        $('.page-banner .owl-controls').css('top',pageH - 80 );
    }


    //page - project info ..
    var pBannerImgH = $('.page-banner .background-show').height();
    //console.log(pBannerImgH);
    $('.page-banner .item.slide').css('height',pBannerImgH);
    $('.page-banner .item .banner-logo').css('height',pBannerImgH);

    
    //if( winW > 768 ) {
        bannerBtn();
    //}

    

});


function bannerBtn() {
    var winH = $(window).height();
    var pageH = winH - 60;
    var imgH = $('.page-banner').height();
    var boxActH = $('.banner-activity').height();

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if( $('.page-banner').length && scroll >= $('.page-banner').offset().top + $('.page-banner').outerHeight() - window.innerHeight) {
            $('#scrollNext').animate({ top: imgH - 100 }, 0);
            $('.page-banner .owl-controls').animate({ top: imgH - 35 }, 0);
            $('.banner-activity').animate({ top: imgH - ( boxActH + 40 ) }, 0);
        }
        else {
            $('#scrollNext').animate({ top: pageH - 100 }, 0);
            $('.page-banner .owl-controls').animate({ top: pageH - 35 }, 0);
            $('.banner-activity').animate({ top: pageH - ( boxActH + 40 ) }, 0);   
        }
        
    });
}

//Function Start

function hideMenu()
{
    $(document).click(function (event) {
        if (!$(event.target).closest(".main-navigation").length) {
            $('.m-submenu-innerpage').each(function () {
                // $(this).removeClass('active');
                $(this).attr('data-status', 0);
                $(this).parent().find('.submenu-innerpage').removeClass('animate');
                $(this).parent().find('.submenu-list').removeClass('animate');
            });
        }
    });
}

function preloader() {
    $(window).on('load', function() {
        $('#preloadLogo').fadeOut();
        $('#preloader').delay(350).fadeOut('slow');
        $('body').delay(350).css({'overflow':'visible'});
    })
}

//Menu
function menu() {
    $('.top-menu .menu').click(function(e){

        $(this).removeClass('show-menu');
        $('.menu-close').addClass('show-menu');
        $('.submenu-bg').addClass('animate');
        $('.submenu-list').addClass('animate');
        $('.submenu-tool').addClass('animate');
        $('body').css('overflow', 'hidden');
        $('.main-navigation, .menu-home-series-close').css({'opacity': 0, 'visibility':'hidden' });
        $('.menu-home-series').removeClass('animate');
        $('#main-nav a.m-home-series, .m-submenu-innerpage').attr('data-status', 0).removeClass('active');

        e.preventDefault();
    });

    $('.menu-close').click(function(e){

        $(this).removeClass('show-menu');
        $('.top-menu .menu').addClass('show-menu');
        $('.submenu-bg').removeClass('animate');
        $('.submenu-list').removeClass('animate');
        $('.submenu-tool').removeClass('animate');
        $('body').css('overflow', 'auto');
        $('.main-navigation').css({'opacity': 1, 'visibility':'visible' });
        $('#main-nav a.m-home-series, .m-submenu-innerpage').attr('data-status', 0).removeClass('active');


        e.preventDefault();
    });

    $('.panel-close').click(function(e){

        $('.top-icons li a#topSearch, .top-icons li a#topGetdirection').data('status', 0).removeClass('active');
        $('.search-container, .getdirection-container, .contact-container').hide();

        e.preventDefault();
    });


    // $('.top-menu a.menu').hover(function(){
    //     $('.top-icons li a#topSearch').data('status', 0).removeClass('active');
    //     $('.search-container').fadeOut(20);
    //     $('.top-icons li a#topGetdirection').data('status', 0).removeClass('active');
    //     $('.getdirection-container').fadeOut(20);
    // });
}

function menuHomeSeries() {
    $('#main-nav a.m-home-series').click(function(e){
        $('#main-nav a.m-home-series').removeClass('active');
        var status = $(this).attr('data-status');
        var buff = $(this).attr('data-buff');
        if(status == 1 && buff == 0) {

            $('.menu-home-series.townhome').removeClass('animate');
            $('.menu-col-left.townhome').removeClass('animate');
            $('.menu-col-right.townhome').removeClass('animate');

            $('.menu-home-series.condo').removeClass('animate');
            $('.menu-col-left.condo').removeClass('animate');
            $('.menu-col-right.condo').removeClass('animate');

            $('.menu-home-series.homesell').removeClass('animate');
            $('.menu-col-left.homesell').removeClass('animate');
            $('.menu-col-right.homesell').removeClass('animate');
            $('.menu-home-series-close').css({'opacity': 0, 'visibility':'hidden' });

            $(this).attr('data-buff', 1).addClass('active');
            $('#main-nav a.m-home-series.t-home').attr('data-buff', 0);
            $('#main-nav a.m-home-series.c-condo').attr('data-buff', 0);
            $('#main-nav a.m-home-series.h-sell').attr('data-buff', 0);

            $('.menu-home-series.home').addClass('animate');
            $('.menu-col-left.home').addClass('animate');
            $('.menu-col-right.home').addClass('animate');
            $('.submenu-bg').removeClass('animate');
            $('.menu-home-series-close').css({'opacity': 1, 'visibility':'visible' });

        }else if(status == 2 && buff == 0){

            $('.menu-home-series.home').removeClass('animate');
            $('.menu-col-left.home').removeClass('animate');
            $('.menu-col-right.home').removeClass('animate');

            $('.menu-home-series.condo').removeClass('animate');
            $('.menu-col-left.condo').removeClass('animate');
            $('.menu-col-right.condo').removeClass('animate');

            $('.menu-home-series.homesell').removeClass('animate');
            $('.menu-col-left.homesell').removeClass('animate');
            $('.menu-col-right.homesell').removeClass('animate');
            $('.menu-home-series-close').css({'opacity': 0, 'visibility':'hidden' });

            $(this).attr('data-buff', 2).addClass('active');
            $('#main-nav a.m-home-series.h-home').attr('data-buff', 0);
            $('#main-nav a.m-home-series.c-condo').attr('data-buff', 0);
            $('#main-nav a.m-home-series.h-sell').attr('data-buff', 0);

            $('.menu-home-series.townhome').addClass('animate');
            $('.menu-col-left.townhome').addClass('animate');
            $('.menu-col-right.townhome').addClass('animate');
            $('.submenu-bg').removeClass('animate');
            $('.menu-home-series-close').css({'opacity': 1, 'visibility':'visible' });
        }else if(status == 3 && buff == 0){

            $('.menu-home-series.home').removeClass('animate');
            $('.menu-col-left.home').removeClass('animate');
            $('.menu-col-right.home').removeClass('animate');

            $('.menu-home-series.townhome').removeClass('animate');
            $('.menu-col-left.townhome').removeClass('animate');
            $('.menu-col-right.townhome').removeClass('animate');

            $('.menu-home-series.homesell').removeClass('animate');
            $('.menu-col-left.homesell').removeClass('animate');
            $('.menu-col-right.homesell').removeClass('animate');
            $('.menu-home-series-close').css({'opacity': 0, 'visibility':'hidden' });

            $(this).attr('data-buff', 3).addClass('active');
            $('#main-nav a.m-home-series.h-home').attr('data-buff', 0);
            $('#main-nav a.m-home-series.t-home').attr('data-buff', 0);
            $('#main-nav a.m-home-series.h-sell').attr('data-buff', 0);

            $('.menu-home-series.condo').addClass('animate');
            $('.menu-col-left.condo').addClass('animate');
            $('.menu-col-right.condo').addClass('animate');
            $('.submenu-bg').removeClass('animate');
            $('.menu-home-series-close').css({'opacity': 1, 'visibility':'visible' });
        }else if(status == 4 && buff == 0){

            $('.menu-home-series.home').removeClass('animate');
            $('.menu-col-left.home').removeClass('animate');
            $('.menu-col-right.home').removeClass('animate');

            $('.menu-home-series.townhome').removeClass('animate');
            $('.menu-col-left.townhome').removeClass('animate');
            $('.menu-col-right.townhome').removeClass('animate');

            $('.menu-home-series.condo').removeClass('animate');
            $('.menu-col-left.condo').removeClass('animate');
            $('.menu-col-right.condo').removeClass('animate');
            $('.menu-home-series-close').css({'opacity': 0, 'visibility':'hidden' });

            $(this).attr('data-buff', 4).addClass('active');
            $('#main-nav a.m-home-series.h-home').attr('data-buff', 0);
            $('#main-nav a.m-home-series.t-home').attr('data-buff', 0);
            $('#main-nav a.m-home-series.c-condo').attr('data-buff', 0);

            $('.menu-home-series.homesell').addClass('animate');
            $('.menu-col-left.homesell').addClass('animate');
            $('.menu-col-right.homesell').addClass('animate');
            $('.submenu-bg').removeClass('animate');
            $('.menu-home-series-close').css({'opacity': 1, 'visibility':'visible' });
        }
        else{
            $(this).attr('data-buff', 0).removeClass('active');
            $('.menu-home-series').removeClass('animate');
            $('.menu-col-left').removeClass('animate');
            $('.menu-col-right').removeClass('animate');
            $('.menu-home-series-close').css({'opacity': 0, 'visibility':'hidden' });

        }

        e.preventDefault();
    });

$('.menu-home-series-close').click(function(e) {
    $('#main-nav a.m-home-series').removeClass('active');
    $('.menu-home-series').removeClass('animate');
    $('.menu-col-left').removeClass('animate');
    $('.menu-col-right').removeClass('animate');
    $('.menu-home-series-close').css({'opacity': 0, 'visibility':'hidden' });
});

}

function submenuInnerpage() {
    $('.m-submenu-innerpage').click(function(e){

        var status = $(this).attr('data-status');
        var menuName = $(this).attr('data-name');
        var parentMenu = $(this);

        $('.m-submenu-innerpage').each(function () {
            if($(this).attr('data-name') && menuName == $(this).attr('data-name')){
                if(status == 0){
                    $(this).attr('data-status', 1).addClass('active');
                    $(this).parent().find('.submenu-innerpage').addClass('animate');
                    $(this).parent().find('.submenu-list').addClass('animate');
                }else if(status == 1){
                    $(this).attr('data-status', 0).removeClass('active');
                    $(this).parent().find('.submenu-innerpage').removeClass('animate');
                    $(this).parent().find('.submenu-list').removeClass('animate');
                }
            }else{
                $(this).removeClass('active');
                if($(this).attr('data-name') && menuName != $(this).attr('data-name')){
                    $(this).attr('data-status', 0);
                    $(this).parent().find('.submenu-innerpage').removeClass('animate');
                    $(this).parent().find('.submenu-list').removeClass('animate');
                }

            }
        });



        // if(status == 0) {
        //     $(this).attr('data-status', 1).addClass('active');
        //     $( '.submenu-innerpage.'+menuName ).addClass('animate');
        //     $('.'+menuName+' .submenu-list').addClass('animate');
        // }
        // else {
        //     $(this).attr('data-status', 0).removeClass('active');
        //     $('.submenu-innerpage').removeClass('animate');
        //     $('.submenu-list').removeClass('animate');
        //     // alert('zz');
        // }

        e.preventDefault();
    });

}

function centerMainNav() {
    var mainNavW = $('#main-nav').outerWidth();
    var mainNav = -(mainNavW/2);
    $('.main-navigation').css('margin-left', mainNav+'px' );
}

function responsiveMenu() {
    var winH = $(window).height();
    $('.rps-menu').css({ 'height' : winH + 60 , 'overflow-y': 'scroll' });

    $('a.burger-menu').click(function(e){
        var active = $(this).data('active');
        if ( $(this).hasClass('menu-open') ) {
            $(this).removeClass('menu-open');
            $(this).css('overflow','auto');
            $('body').css('overflow', 'hidden');
        }
        else {
            $(this).addClass('menu-open');
            $(this).css('overflow','hidden');
            $('body').css('overflow', 'auto');
        }
        if(active == 0) {
            $(this).addClass('active');
            $('.rps-menu').fadeIn(300);
            $(this).data('active', 1);
            $('body').css('overflow', 'hidden');
        }
        else if(active == 1) {
            $(this).removeClass('active');
            $('.rps-menu').fadeOut(300);
            $(this).data('active', 0);
            $('body').css('overflow', 'auto');
        }
        e.preventDefault();

    });


    $('.rps-close').click(function(e){
        $('.rps-menu').fadeOut(300);
        $('a.burger-menu').data('active', 0);
    });

    // var tags = $(".responsive-menu li > ul").addClass("hasSubmenu");

    $('.responsive-menu li').each(function(){
        if( $(this).has('ul').length ){
            $(this).addClass('hasSubmenu');
        }
    });


    $('.responsive-menu ul li a').removeClass("active");

    $('.responsive-menu ul li').click(function() {
        if( $(this).has('ul').length ){
            $(this).children('ul').slideToggle(300);
            $(this).addClass('i-active');
            //$(this).children('a').addClass('active');

            if($(this).children('a').hasClass("active")){
                $(this).children('a').removeClass("active");
            }
            else {
                $(this).children('a').addClass("active");
            }

            //return false;
        }

    });
}

function scrollNext() {
    $("#scrollNext").click(function() {
        if ($("#content").length) {
            $('html, body').animate({
                scrollTop: $("#content").offset().top - 60
            }, 800);
        }
    });
}

function scrollTop() {

    $('.scroll-top').click(function (){
        $('html, body').animate({
            scrollTop: $('#page').offset().top
        }, 1400);
    });



}

function sortBlock() {
    $('.sort-block .sort').click(function(e){
        e.stopImmediatePropagation();
        var status = $(this).data('status');
        if( status == 0 ) {
            $('.sort-block ul').fadeIn(100);
            $(this).data('status', 1);
        }
        else if( status == 1 ) {
            $('.sort-block ul').fadeOut(20);
            $(this).data('status', 0);
        }
    });

    $('.sort-block ul li').click(function(e){
        var value = $(this).data('value');
        $('.sort-block .sort span').text(value);
        $('input[name="sortSelect"]').val(value);

        $('.sort-block ul').fadeOut(20);
        $('.sort-block .sort').data('status', 0);
    });

    $(document).click(function(e) {
        var target = $(e.target);
        var sortLists = $('#sortLists');
        if(!(target.is(sortLists) || sortLists.find(target).length )) {
            sortLists.fadeOut(20);
            $('.sort-block .sort').data('status', 0);
        }
    });
}


function searchDropdown() {
    $('#topSearch, .top-icons li a#topSearch, .rps-footer-sticky li a#topSearch, #searchResult').click(function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var status = $(this).data('status');

        if( status == 0 ) {
            $('.search-container').fadeIn(250);
            $(this).data('status', 1).addClass('active');
        }
        else if( status == 1 ) {
            $('.search-container').hide();
            $(this).data('status', 0).removeClass('active');
        }

        $('.getdirection-container').hide();
        $('.contact-container').hide();
        $('#topGetdirection, .top-icons li a#topGetdirection, .rps-footer-sticky li a#topGetdirection').data('status', 0).removeClass('active');

        $('.menu-close').removeClass('show-menu');
        $('.top-menu .menu').addClass('show-menu');
        $('.submenu-bg').removeClass('animate');
        $('.submenu-list').removeClass('animate');
        $('.submenu-tool').removeClass('animate');


    });

    $(document).click(function(e) {
        var target = $(e.target);
        var block = $('.search-container');
        if(!(target.is(block) || block.find(target).length )) {
            block.hide();
            $('#topSearch, .top-icons li a#topSearch, .rps-footer-sticky li a#topSearch').data('status', 0).removeClass('active');
            $('body').css('overflow', 'auto');
        }
    });

    // Project type
    $('.projecttype-block .searchBlock').click(function(e){
        e.stopImmediatePropagation();
        var status = $(this).data('status');
        if( status == 0 ) {
            $('.projecttype-block ul').fadeIn(100);
            $(this).data('status', 1);
        }
        else if( status == 1 ) {
            $('.projecttype-block ul').hide();
            $(this).data('status', 0);
        }

        $('.projectlocation-block ul').hide();
        $('.projectlocation-block .searchBlock').data('status', 0);
        $('.projectbrand-block ul').hide();
        $('.projectbrand-block .searchBlock').data('status', 0);
        $('.projectname-block ul').hide();
        $('.projectname-block .searchBlock').data('status', 0);
    });

    $('.projecttype-block ul li').click(function(e){
        var value = $(this).data('value');
        $('.projecttype-block .searchBlock span').text(value);
        $('input[name="projecttypeSelect"]').val(value);

        $('.projecttype-block ul').hide();
        $('.projecttype-block .searchBlock').data('status', 0);
    });

    $(document).click(function(e) {
        var target = $(e.target);
        var searchLists = $('#searchLists');
        if(!(target.is(searchLists) || searchLists.find(target).length )) {
            searchLists.fadeOut(20);
            $('.projecttype-block .searchBlock').data('status', 0);
        }
    });

    // Project location
    $('.projectlocation-block .searchBlock').click(function(e){
        e.stopImmediatePropagation();
        var status = $(this).data('status');
        if( status == 0 ) {
            $('.projectlocation-block ul').fadeIn(100);
            $(this).data('status', 1);
        }
        else if( status == 1 ) {
            $('.projectlocation-block ul').fadeOut(20);
            $(this).data('status', 0);
        }

        $('.projecttype-block ul').hide();
        $('.projecttype-block .searchBlock').data('status', 0);
        $('.projectbrand-block ul').hide();
        $('.projectbrand-block .searchBlock').data('status', 0);
        $('.projectname-block ul').hide();
        $('.projectname-block .searchBlock').data('status', 0);
    });

    $('.projectlocation-block ul li').click(function(e){
        var value = $(this).data('value');
        $('.projectlocation-block .searchBlock span').text(value);
        $('input[name="projectlocationSelect"]').val(value);

        $('.projectlocation-block ul').fadeOut(20);
        $('.projecttype-block .searchBlock').data('status', 0);
    });

    $(document).click(function(e) {
        var target = $(e.target);
        var searchLists = $('#projectlocationLists');
        if(!(target.is(searchLists) || searchLists.find(target).length )) {
            searchLists.fadeOut(20);
            $('.projectlocation-block .searchBlock').data('status', 0);
        }
    });

    // Project Brand
    $('.projectbrand-block .searchBlock').click(function(e){
        e.stopImmediatePropagation();
        var status = $(this).data('status');
        if( status == 0 ) {
            $('.projectbrand-block ul').fadeIn(100);
            $(this).data('status', 1);
        }
        else if( status == 1 ) {
            $('.projectbrand-block ul').fadeOut(20);
            $(this).data('status', 0);
        }

        $('.projecttype-block ul').hide();
        $('.projecttype-block .searchBlock').data('status', 0);
        $('.projectlocation-block ul').hide();
        $('.projectlocation-block .searchBlock').data('status', 0);
        $('.projectname-block ul').hide();
        $('.projectname-block .searchBlock').data('status', 0);
    });

    $('.projectbrand-block ul li').click(function(e){
        var value = $(this).data('value');
        $('.projectbrand-block .searchBlock span').text(value);
        $('input[name="projectbrandSelect"]').val(value);

        $('.projectbrand-block ul').fadeOut(20);
        $('.projectbrand-block .searchBlock').data('status', 0);
    });

    $(document).click(function(e) {
        var target = $(e.target);
        var searchLists = $('#projectbrandLists');
        if(!(target.is(searchLists) || searchLists.find(target).length )) {
            searchLists.fadeOut(20);
            $('.projectbrand-block .searchBlock').data('status', 0);
        }
    });

    // Project name
    $('.projectname-block .searchBlock').click(function(e){
        e.stopImmediatePropagation();
        var status = $(this).data('status');
        if( status == 0 ) {
            $('.projectname-block ul').fadeIn(100);
            $(this).data('status', 1);
        }
        else if( status == 1 ) {
            $('.projectname-block ul').fadeOut(20);
            $(this).data('status', 0);
        }

        $('.projecttype-block ul').hide();
        $('.projecttype-block .searchBlock').data('status', 0);
        $('.projectlocation-block ul').hide();
        $('.projectlocation-block .searchBlock').data('status', 0);
        $('.projectbrand-block ul').hide();
        $('.projectbrand-block .searchBlock').data('status', 0);
    });

    $('.projectname-block ul li').click(function(e){
        var value = $(this).data('value');
        $('.projectname-block .searchBlock span').text(value);
        $('input[name="projectnameSelect"]').val(value);

        $('.projectname-block ul').fadeOut(20);
        $('.projectname-block .searchBlock').data('status', 0);
    });

    $(document).click(function(e) {
        var target = $(e.target);
        var searchLists = $('#projectnameLists');
        if(!(target.is(searchLists) || searchLists.find(target).length )) {
            searchLists.fadeOut(20);
            $('.projectname-block .searchBlock').data('status', 0);
        }
    });
}

function getdirectionDropdown() {
    $('#topGetdirection, .top-icons li a#topGetdirection, .rps-footer-sticky li a#topGetdirection').click(function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var status = $(this).data('status');

        if( status == 0 ) {
            $('.getdirection-container').fadeIn(300);
            $(this).data('status', 1).addClass('active');
        }
        else if( status == 1 ) {
            $('.getdirection-container').hide();
            $(this).data('status', 0).removeClass('active');
        }

        $('.search-container').hide();
        $('.contact-container').hide();
        $('#topSearch, .top-icons li a#topSearch, .rps-footer-sticky li a#topSearch').data('status', 0).removeClass('active');
        $('.menu-close').removeClass('show-menu');
        $('.top-menu .menu').addClass('show-menu');
        $('.submenu-bg').removeClass('animate');
        $('.submenu-list').removeClass('animate');
        $('.submenu-tool').removeClass('animate');
    })

    $(document).click(function(e) {
        var target = $(e.target);
        var block = $('.getdirection-container');
        if(!(target.is(block) || block.find(target).length )) {
            block.fadeOut(20);
            $('#topGetdirection, .top-icons li a#topGetdirection, .rps-footer-sticky li a#topGetdirection').data('status', 0).removeClass('active');
            $('body').css('overflow', 'auto');
        }
    });

    // Get Direct type
    $('.directlocation-block .searchBlock').click(function (e) {
        e.stopImmediatePropagation();
        var status = $(this).data('status');
        if (status == 0) {
            $('.directlocation-block ul').fadeIn(100);
            $(this).data('status', 1);
        }
        else if (status == 1) {
            $('.directlocation-block ul').fadeOut(20);
            $(this).data('status', 0);
        }

        $('.directproject-block ul').hide();
        $('.directproject-block .searchBlock').data('status', 0);
    });

    $('.directlocation-block ul li').click(function (e) {
        var value = $(this).data('value');
        $('.directlocation-block .searchBlock span').text(value);
        $('input[name="directlocationSelect"]').val(value);

        $('.directlocation-block ul').fadeOut(20);
        $('.directlocation-block .searchBlock').data('status', 0);
    });

    $(document).click(function (e) {
        var target = $(e.target);
        var searchLists = $('#directlocationLists');
        if (!(target.is(searchLists) || searchLists.find(target).length )) {
            searchLists.fadeOut(20);
            $('.projecttype-block .searchBlock').data('status', 0);
        }
    });

    // Get Direct location
    $('.directproject-block .searchBlock').click(function (e) {
        e.stopImmediatePropagation();
        var status = $(this).data('status');
        if (status == 0) {
            $('.directproject-block ul').fadeIn(100);
            $(this).data('status', 1);
        }
        else if (status == 1) {
            $('.directproject-block ul').fadeOut(20);
            $(this).data('status', 0);
        }

        $('.directlocation-block ul').hide();
        $('.directlocation-block .searchBlock').data('status', 0);
    });

    $('.directproject-block ul li').click(function (e) {
        var value = $(this).data('value');
        $('.directproject-block .searchBlock span').text(value);
        $('input[name="directprojectSelect"]').val(value);

        $('.directproject-block ul').fadeOut(20);
        $('.directproject-block .searchBlock').data('status', 0);
    });

    $(document).click(function (e) {
        var target = $(e.target);
        var searchLists = $('#directprojectLists');
        if (!(target.is(searchLists) || searchLists.find(target).length )) {
            searchLists.fadeOut(20);
            $('.directproject-block .searchBlock').data('status', 0);
        }
    });
}

function contactDropdown() {
    $('#topContact, .top-icons li a#topContact, .rps-footer-sticky li a#topContact').click(function(e) {

        radionAppoint();
        e.preventDefault();
        e.stopImmediatePropagation();
        var status = $(this).data('status');

        if( status == 0 ) {
            $('.contact-container').fadeIn(100);
            $(this).data('status', 1).addClass('active');
        }
        else if( status == 1 ) {
            $('.contact-container').hide();
            $(this).data('status', 0).removeClass('active');
        }

        $('.search-container').hide();
        $('.getdirection-container').hide();
        $('#topContact, .top-icons li a#topContact, .rps-footer-sticky li a#topContact').data('status', 0).removeClass('active');
        $('.menu-close').removeClass('show-menu');
        $('.top-menu .menu').addClass('show-menu');
        $('.submenu-bg').removeClass('animate');
        $('.submenu-list').removeClass('animate');
        $('.submenu-tool').removeClass('animate');
    });

    $('.contact-container-ft .radio-checkmark input[type="radio"]').click(function() {

        if($('#check-appoint-ft').is(':checked')) {
            $('input#date_appointment_ft').css('display','block');
        }
        else {
            $('input#date_appointment_ft').css('display','none');
        }
    });

    $('#date_appointment_ft').datepicker({
        startDate: new Date(),
        todayHighlight: true
    });

    $(document).click(function(e) {
        var target = $(e.target);
        var block = $('.contact-container');
        if(!(target.is(block) || block.find(target).length )) {
            block.fadeOut(20);
            $('#topContact, .top-icons li a#topContact, .rps-footer-sticky li a#topContact').data('status', 0).removeClass('active');
            $('body').css('overflow', 'auto');
        }
    });
}

function radionAppoint() {

    $('.contact-container-ft .radio-checkmark input[type="radio"]').click(function() {

        if($('#check-appoint-ft').is(':checked')) {
            $('input#date_appointment_ft').css('display','block');
        }
        else {
            $('input#date_appointment_ft').css('display','none');
        }
    });

    $('#date_appointment_ft').datepicker({
        startDate: new Date(),
        todayHighlight: true
    });
}

function tabMenu() {
    $('.tabs-menu a').click(function(e) {
        e.preventDefault();
        $(this).parent().addClass('current');
        $(this).parent().siblings().removeClass('current');
        var tab = $(this).attr('href');
        $('.tab-content').not(tab).css('display', 'none');
        $(tab).fadeIn();
    });

}

function shareFacebook() {
    $(document).ready(function () {
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        window.fbAsyncInit = function() {
            FB.init({
                //appId: '266961630392205',  // Change appId 409742669131720 with your Facebook Application ID
                //appID : 507507096307016,
                appID : 659883840880609,
                status: true,
                xfbml: true,
                cookie: true
            });
        };
        $('.ficon-shares .facebook-shared').click(function () {

            var titleShared     = $('.title_shared_facebook').last().attr('content');
            var descShared      = $('.desc_shared_facebook').last().attr('content');
            var imgUrlShared    = $('.img_shared_facebook').last().attr('content');

            var width = 575,
            height = 400,
            left = ($(window).width() - width) / 2,
            top = ($(window).height() - height) / 2,
                // url = 'https://www.facebook.com/sharer.php?title=' + titleShared + '' +
                //     '&description=' + descShared + '&u=' + document.URL + '&picture=' + imgUrlShared,
                // opts = 'status=1' +
                //     ',width=' + width +
                //     ',height=' + height +
                //     ',top=' + top +
                //     ',left=' + left;
                

                url = 'https://www.facebook.com/dialog/share?app_id=659883840880609&display=popup&href=' + document.URL ,
                opts = 'status=1' +
                ',width=' + width +
                ',height=' + height +
                ',top=' + top +
                ',left=' + left; ;

                window.open(url, 'facebook', opts);
            });
    });
}

function shareTwitter() {
    $(document).ready(function () {
        $('.ficon-shares .twitter-shared').click(function () {
            var width  = 575,
            height = 400,
            left   = ($(window).width()  - width)  / 2,
            top    = ($(window).height() - height) / 2,
            url    = 'http://twitter.com/share?url='+ document.URL,
            opts   = 'status=1' +
            ',width='  + width  +
            ',height=' + height +
            ',top='    + top    +
            ',left='   + left;

            window.open(url, 'twitter', opts);
        });
    });
}

function shareLine() {
    $(document).ready(function () {
        $('.ficon-shares .line-shared').click(function () {
            var width  = 575,
            height = 500,
            left   = ($(window).width()  - width)  / 2,
            top    = ($(window).height() - height) / 2,
            url    = 'https://lineit.line.me/share/ui?url='+ document.URL,
            opts   = 'status=1' +
            ',width='  + width  +
            ',height=' + height +
            ',top='    + top    +
            ',left='   + left;

            window.open(url, 'line', opts);
        });
    });
}


function projSubmenu() {
    $('.proj-rps-submenu-container p').click(function(){
        $('.proj-rps-submenu').slideToggle(300);
    });

    $('.proj-rps-submenu li').each(function(){
        if( $(this).has('div.submenu-innerpage').length ){
            $(this).addClass('hasSubmenu');
        }
    });


    $('.proj-rps-submenu ul li a').removeClass("active");


    $('.proj-rps-submenu ul li').click(function() {
        $('.proj-rps-submenu ul li.hasSubmenu').removeClass('i-active');
        // $('.proj-rps-submenu .submenu-innerpage').slideUp(300);

        if( $(this).has('div.submenu-innerpage').length ){
            $(this).children('div.submenu-innerpage').slideToggle(300);
            $(this).addClass('i-active');



            if($(this).children('a.submenu-innerpage').hasClass("active")){
                $(this).children('a.submenu-innerpage').removeClass("active");
            }
            else {
                $(this).children('a.submenu-innerpage').addClass("active");
            }

            //return false;
        }

    });

}
//Function End