$(document).ready(function () {

	//Page Load Start
    searchDropdown();

    $('#searchProject').hide();

    //Page Load End
});


//Function Start


function searchDropdown() {
    // Project type
    $('.subject-block .searchBlock').click(function(e){
        e.stopImmediatePropagation();
        var status = $(this).data('status');
        if( status == 0 ) {
            $('.subject-block ul').fadeIn(200);
            $(this).data('status', 1);
        }
        else if( status == 1 ) {
            $('.subject-block ul').fadeOut(200);
            $(this).data('status', 0);
        }
    });

    $('.subject-block ul li').click(function(e){
        var value = $(this).data('value');
        $('.subject-block .searchBlock span').text(value);
        $('input[name="contactSubject"]').val(value);

        $('.subject-block ul').fadeOut(200);
        $('.subject-block .searchBlock').data('status', 0);
    });

    $(document).click(function(e) {
        var target = $(e.target);
        var searchLists = $('#subjectLists');
        if(!(target.is(searchLists) || searchLists.find(target).length )) {
            searchLists.fadeOut(200);
            $('.subject-block .searchBlock').data('status', 0);
        }
    });


}


//Function End