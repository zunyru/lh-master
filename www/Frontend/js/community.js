$(document).ready(function () {
    //Page Load Start
    commuList();
    $('.more-detail').hide();
    $('.more_button').click(function () {
        if($(this).val() == 0){
            $(this).parent().find('.more-detail').show();
            $(this).val(1);
        }else{
            $(this).parent().find('.more-detail').hide();
            $(this).val(0);
        }
    });
    $('.list-item').mouseleave(function() {
        $(this).find('.more-detail').hide();
        $(this).find('.more_button').val(0);
    }).mouseover(function() {

    });

    setTimeout(function () {
        scrollToElement($('.community_id_'+$('#community_choose_id').val()));
    },2000);

    $(document).ready(function()
    {
        $(document).resize();
    });
    //Page Load End


});


//Function Start
function masonryList() {
    var wall = new Freewall(".commu-lists");
    wall.reset({
        selector: '.list-item',
        animate: true,
        cellW: 180,
        cellH: 'auto',
        gutterY: 30,
        onResize: function() {
            wall.fitWidth();
        }
    });

    wall.container.find('.list-item img').load(function() {
        wall.fitWidth();
    });
}

function commuList() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        // var stagePadding = 40;
        // $('.commu-lists').owlCarousel({
        //     loop:true,
        //     margin: 10,
        //     stagePadding: stagePadding,
        //     nav:true,
        //     dots: true,
        //     autoHeight: true,
        //     responsive:{
        //         0:{
        //             items:1
        //         },
        //         768:{
        //             items:1
        //         }
        //     }
        // });
    }
    else {
        masonryList();
    }



}

function scrollToElement(ele) {
    $('html, body').animate({
        scrollTop: ele.offset().top - 70,
        scrollLeft: ele.offset().left
    }, 1000);
}


//Function End