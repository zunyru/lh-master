$(document).ready(function () {
    //Page Load Start
    bannerSlide();
    scrollNext();
    //Page Load End

    var winW = $(window).width();

    if( winW <= 768 ) {
        hmsLogo();
    }
});


//Function Start

function bannerSlide() {
    var winW = $(window).width();
    var winH = $(window).height();
    var bannerH = $('.page-banner').height();

    //Center text
    var slideTxt = $('.banner-txt').height();

    //Full height
    if( winW <= 768 ) {
        //$('.banner-parallax, .page-banner').css('height', 180);
        var center = (bannerH - 100 ) / 2;
    }
    else {
        //$('.banner-parallax').css('height', winH - 60);
        var center = (bannerH - 290) / 2;
    }

    $('.banner-txt').css('top',center);


    //Slide
    var isMulti = ($('#bannerSlide.owl-carousel img').length > 1) ? true : false;
    var isMulti2 = ($('#bannerSlide_count').val()>1) ? true : false;
    $('#bannerSlide').owlCarousel({
        loop:isMulti2,
        autoplay: true,
        autoplayTimeout: 4000,
        autoplaySpeed: 1000,
        margin:5,
        animateOut: 'fadeOut',
        nav:false,
        dots: isMulti2,
        video:true,
        lazyLoad:true,
        center:true,
        responsive:{
            0:{
                items:1
            },
        }
    });
}

function scrollNext() {
    $("#scrollNext").click(function() {
        $('html, body').animate({
            scrollTop: $("#content").offset().top - 55
        }, 800);
    });
}

function hmsLogo() {
    var winW = $(window).width();
    // responsive
    if( winW <= 768 ) {
        var isMulti = ($('#hmsLogo.owl-carousel img').length > 1) ? true : false;
        $('#hmsLogo').owlCarousel({
            loop:isMulti,
            margin: 30,
            nav: true,
            dots: isMulti,
            autoHeight: false,
            slideBy: 2,
            autoplay:true,
            autoplayTimeout:1500,
            autoplayHoverPause:true,
            responsive:{
                0:{
                    items:2
                },
                768:{
                    items:2
                }
            }
        });
    }

}
//Function End