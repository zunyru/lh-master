$(document).ready(function () {
    //Page Load Start
    bannerSlideHome();
    slideHomeCentertxt();
    vdoControl();



    var winW = $(window).width();
    var winH = $(window).height();

    if( winW > 768 ) {
        homeHightlight();
        homeReview();
        homeTips();
        homeCommunity();
    }

    var pageH = winH - 60;

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= (pageH -10)) {
            $('.main-navigation').css({'opacity': 1, 'visibility':'visible', 'display':'block' });
        }
        else {
            $('.main-navigation').css({'opacity': 0, 'visibility':'hidden', 'display':'none' });
        }

    });
    
    //var screen_size = 992;
    var screen_size = 769;

    if( winW < 769 ) {
        var funBlockH = winH - 110;
        // $('.page-banner, .page-banner-content').css('height',funBlockH);
    }

    $(document).ready(function()
    {
        $(document).resize();
    });

    //activity onclick
    // $("#bannerActivityDiv").on('click', ':not(#closeActivityDkpt,#closeActivityRsp)', function (e) {
    //     location.href=$('#bannerActivityUrl').val();
    // });

    //Page Load End
});

$(window).load(function () {
    var winW = $(window).width();

    if( winW <= 768 ) {
        homeCommunity();
        homeHightlight();
        homeReview();
        homeTips();
    }
});


//Function Start
function bannerSlideHome() {
    var winH = $(window).height();
    var winW = $(window).width();
    $('.banner-parallax.banner-cover-rsp').css('height', winH - 110);

    if( winW <= 768 ) {
        $('.leadDesktop').remove();
    }else{
        $('.leadMobile').remove();
    }

    var isMulti = ($('#homeBanner.owl-carousel .item').length > 1) ? true : false;
    $('#homeBanner').owlCarousel({
        loop: isMulti,
        autoplay: true,
        autoplayTimeout: 3000,
        autoplaySpeed: 1000,
        margin:5,
        animateOut: 'fadeOut',
        nav:false,
        dots: isMulti,
        video:true,
        lazyLoad:true,
        center:true,
        responsive:{
            0:{
                items:1
            },

        }
    });
}

function slideHomeCentertxt() {
    var winH = $(window).height();
    var slideTxt = $('.banner-txt').height();
    var center = (winH - 330) / 2;
    $('.banner-txt').css('top',center);

}

function vdoControl() {
    var winW = $(window).width();
    var winH = $(window).height();

    $('.rps-vdobg').css('height',winH);

    var pageH = winH - 170;
    $('.i-mute').css('top',pageH);

    // reponsive
    if( winW < 769 ) {
        var funBlockH = winH - 190;
        $('.i-mute').css('top',funBlockH);
    }

    $(window).resize(function(){
        var winW = $(window).width();
        var winH = $(window).height();
        var pageH = winH - 170;
        $('.i-mute').css('top',pageH);


        if( winW < 769 ) {
            var funBlockH = winH - 190;
            $('.i-mute').css('top',funBlockH);
        }

    });



    $('#vdoPlay').click(function(){
        $('#slideVdo')[0].play();
        $(this).fadeOut(200);
        $('#vdoPause').fadeIn(200);
        $('.rps-vdobg').fadeOut(200);

        $(".vdo-control").mouseenter(function(event) {
            event.stopPropagation();
            $('#vdoPause').addClass("btnshown");
        }).mouseleave(function(event)
        {
            event.stopPropagation();
            $('#vdoPause').removeClass("btnshown");
        });
    });

    $("#vdoPause").click(function(){
        $('#slideVdo').get(0).pause();
        $(this).fadeOut(200);
        $('#vdoPlay').fadeIn(200);
    });

    //Mute & Unmute
    $("video#slideVdo").prop('muted', false);

    $("#vdoMute").click( function (){
        if( $("video#slideVdo").prop('muted', false) )
        {
            $("video#slideVdo").prop('muted', true);
            $(this).fadeOut(200);
            $("#vdoUnmute").fadeIn(200).css('display', 'block');
        }
    });

    $("#vdoUnmute").click( function (){
        if( $("video#slideVdo").prop('muted', true) )
        {
            $("video#slideVdo").prop('muted', false);
            $(this).fadeOut(200);
            $("#vdoMute").fadeIn(200).css('display', 'block');
        }
    });

}

function homeHightlight() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        var stagePadding = 25;
        var margin = -10;
        var autoH = true;
        var loop = true;
    }
    else {
        var stagePadding = 0;
        var margin = 10;
        var autoH = false;
        var loop = false;
    }

    var isMulti = ($('#homeHighlightSlide .item').length > 1) ? true : false;

    $('#homeHighlightSlide').owlCarousel({
        loop:isMulti,
        margin: margin,
        stagePadding: stagePadding,
        autoplay: isMulti,
        autoplayTimeout: 5500,
        nav:isMulti,
        dots: isMulti,
        autoHeight: autoH,
        responsive:{
            0:{
                items:1
            }
        }
    });
}

function homeCommunity() {
    var winW = $(window).width();

    //responsive
    if( winW <= 768 ) {
        var slideBy = 1;
        var autoH = true;
        var isMulti = false;
    }
    else if( winW >= 768 && winW <= 1024 ) {
        var slideBy = 2;
        var autoH = true;
    }
    else {
        var slideBy = 2;
        var autoH = false;
        var isMulti = ($('#homeCommunitySlide .item').length > 1) ? true : false;
    }


    $('#homeCommunitySlide').owlCarousel({
        slideBy: slideBy,
        loop:isMulti,
        margin: 40,
        autoplay: true,
        autoplayTimeout: 5000,
        scrollPerPage : true,
        nav: isMulti,
        dots: isMulti,
        autoHeight: autoH,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:1
            },
            1199:{
                items:2
            }
        }
    });
}

function homeTips() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        var stagePadding = 25;
        var margin = -10;
        var autoH = true;
        var slideBy = 1;
        var isMulti = true;
    }
    else if( winW >= 768 && winW <= 1024 ) {
        var stagePadding = 25;
        var margin = -10;
        var autoH = true;
        var slideBy = 2;
        var isMulti = ($('#homeTipsSlide .item').length > 1) ? true : false;
    }
    else {
        var stagePadding = 0;
        var margin = 10;
        var autoH = false;
        var slideBy = 3;
        var isMulti = ($('#homeTipsSlide .item').length > 1) ? true : false;
    }


    $('#homeTipsSlide').owlCarousel({
        loop:isMulti,
        slideBy: slideBy,
        margin: margin,
        autoplay: true,
        autoplayTimeout: 5000,
        stagePadding: stagePadding,
        nav:isMulti,
        dots: isMulti,
        autoHeight: autoH,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:1
            },
            1024:{
                items:2
            },
            1199:{
                items:3
            }
        }
    });

}

function masonryList() {
    var winW = $(window).width();
    if( winW > 768 && winW <= 1200 ) {
        var cellW = 100;
    }
    else {
        var cellW = 180;
    }

    var wall = new Freewall(".review-lists");
    wall.reset({
        selector: '.list-item',
        animate: true,
        cellW: cellW,
        cellH: 'auto',
        onResize: function() {
            wall.fitWidth();
        }
    });

    wall.container.find('.list-item img').load(function() {
        wall.fitWidth();
    });
}

function homeReview() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        var stagePadding = 40;

        var isMulti = ($('.review-lists.owl-carousel img').length > 1) ? true : false;
        $('.review-lists').owlCarousel({
            loop:isMulti,
            margin: 30,
            autoplay: true,
            autoplayTimeout: 5000,
            //stagePadding: stagePadding,
            nav:true,
            dots: true,
            autoHeight: true,
            responsive:{
                0:{
                    items:1
                },
                768:{
                    items:1
                }
            }
        });
    }
    else {
        masonryList();
    }



}

function linkToUrl(url) {
    window.location = String(url);
}


//Function End