$(document).ready(function () {
    //Page Load Start
    scrollNextInfoHome();
    $('#pjHomeBanner_default').hide();

    var winW = $(window).width();

    // responsive
    // if( winW > 768 ) {
    //     var winH = $(window).height();
    //     $('.banner-parallax').css('height', winH - 60);
    // }
    var countSlide = 0;
    var isMulti = ($('#pjHomeBanner .item').length > 1) ? true : false;

    $('#pjHomeBanner').owlCarousel({
        loop:isMulti,
        autoplay: true,
        autoplayTimeout: 4000,
        autoplaySpeed: 1000,
        margin:5,
        animateOut: 'fadeOut',
        nav:false,
        dots: isMulti,
        video:true,
        lazyLoad:true,
        center:true,
        singleItem :isMulti,
        margin:0,
        autoHeight:false,
        responsive:{
            0:{
                items:1,
            }
        }
        ,onChanged: function (event) {
            setTimeout(function(){
                if(countSlide == 0){
                    $('.slider').find('.center').addClass('animate');
                }else{
                    $('.slider').find('.center').addClass('animate3');
                }
            }, 1000);
            setTimeout(function(){
                if(countSlide == 0) {
                    $('.slider').find('.center').addClass('animate2');
                }
            }, 1000);
            // setTimeout(function(){
            //     $('#img_logo').fadeOut('slow');
            //     $('#desc_logo').fadeOut('slow');
            // }, 7000);
            setTimeout(function () {
                if(countSlide == 0) {
                    $('.slider').find('.center').addClass('animate3');
                    countSlide++;
                }
            },1000);
        }
    });

    var isMulti_zun = ($('#pjHomeBanner_default .item').length > 1) ? true : false;
    $('#pjHomeBanner_default').owlCarousel({
        loop:isMulti_zun,
        autoplay: true,
        autoplayTimeout: 4000,
        autoplaySpeed: 1000,
        margin:5,
        animateOut: 'fadeOut',
        nav:false,
        dots: isMulti_zun,
        video:true,
        lazyLoad:true,
        center:true,
        singleItem :isMulti_zun,
        responsive:{
            0:{
                items:1
            }
        }
        ,onChanged: function (event) {
            setTimeout(function(){
                if(countSlide == 0){
                    $('.slider').find('.center').addClass('animate');
                }else{
                    $('.slider').find('.center').addClass('animate3');
                }
            }, 1000);
            setTimeout(function(){
                if(countSlide == 0) {
                    $('.slider').find('.center').addClass('animate2');
                }
            }, 1000);
            // setTimeout(function(){
            //     $('#img_logo').fadeOut('slow');
            //     $('#desc_logo').fadeOut('slow');
            // }, 7000);
            setTimeout(function () {
                if(countSlide == 0) {
                    $('.slider').find('.center').addClass('animate3');
                    countSlide++;
                }
            },1000);
        }
    });

    $(".close-acty").click(function(){
       $('#pjHomeBanner_default').show();
       $('#pjHomeBanner').hide();
    });

    function initial()
    {
        $('#logo_s').show();
    }

    if( winW > 768 ) {
        gallery();
    }


    //projectPromoSlide();
    //waiting for all img loaded -> int
    $('#projectModelSlide').imagesLoaded( function() {
      modelThumb();
    });
    otherHomeSlide();
    stickyMenuProject();
    radionAppoint();


    $('#date_appointment').datepicker({
        format: "dd/mm/yy",
        startDate: new Date(),
        todayHighlight: true,
    });

    $('.gallery').featherlightGallery();

    colSlide();

    $('.owl-dots').show();

    $(document).ready(function()
    {
        $(document).resize();
    });
    //Page Load End
});

$(window).load(function () {
    var winW = $(window).width();

    if( winW <= 768 ) {
        gallery();
    }
});


//Function Start
function scrollNextInfoHome() {
    $("#scrollNext").click(function() {
        if ($("#sec-information").length) {
            $('html, body').animate({
                scrollTop: $("#sec-information").offset().top - 60
            }, 800);
        }
    });
}

function gridLayout5() {
    var wall = new Freewall("#tpl5-img");
    wall.reset({
        selector: '.item',
        animate: true,
        cellW: 300,
        cellH: 'auto',
        onResize: function() {
            wall.fitWidth();
        }
    });

    var images = wall.container.find('.item');
    images.find('img').load(function() {
        wall.fitWidth();
    });
}

function gallery() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        //var stagePadding = 40;
        $('#tpl5-img').owlCarousel({
            loop: true,
            margin: 30,
            autoplay:true,
            autoplayTimeout:3000,
            autoplayHoverPause:true,
            //stagePadding: stagePadding,
            nav:true,
            dots: false,
            autoHeight: true,
            responsive:{
                0:{
                    items:1
                },
                768:{
                    items:1
                }
            }
        });
    }
    else {
        gridLayout5();
    }
}

// function projectPromoSlide() {
//     var winW = $(window).width();
//
//     // responsive
//     if( winW <= 768 ) {
//         var stagePadding = 40;
//         var margin = 0;
//         var autoH = true;
//     }
//     else {
//         var stagePadding = 0;
//         var margin = 20;
//         var autoH = false;
//     }
//
//     $('#projectPromoSlide').owlCarousel({
//         loop:true,
//         margin: 20,
//         stagePadding: 0,
//         nav:true,
//         dots: true,
//         autoHeight: autoH,
//         responsive:{
//             0:{
//                 items:1
//             }
//         }
//     });
// }

function otherHomeSlide() {

    var winW = $(window).width();
    // responsive
    if( winW <= 768 ) {
        var stagePadding = 40;
        var margin = 0;
    }
    else {
        var stagePadding = 0;
        var margin = 20;
    }

    var otherHome = $('#otherHome');
    var isMulti = ($('#otherHome .item').length > 3) ? true : false;
    otherHome.owlCarousel({
        loop:isMulti,
        margin: margin,
        stagePadding: stagePadding,
        nav:isMulti,
        dots: false,
        autoHeight: true,
        responsive:{
            0:{
                items:1,
                center: true
            },
            979:{
                items:2
            },
            1199:{
                items:3
            }
        }
    });
    otherHome.trigger('refresh.owl.carousel');

    // $(document).on('click','#otherHome .item-homesell',function () {
    //     var home_sell_status = $(this).find('#home_sell_status').val();
    //     var amount_value = $('#amount_value').val();
    //     var index_value = $(this).find('#index_value').val();
    //     var title_home = $(this).find('#data-slider #title_home').val();
    //     var format_home = $(this).find('#data-slider #format_home').val();
    //     var id_home = $(this).find('#data-slider #id_home').val();
    //     var identity_home = $(this).find('#data-slider #identity_home').val();
    //     var price_home = $(this).find('#data-slider #price_home').val();
    //     var home_sell_id = $(this).find('#data-slider #home_sell_id').val();
    //     var img_arr = [];
    //     var img_list = $(this).find('#data-slider #img_list').children('input').each(function () {
    //         img_arr.push($(this).val());
    //     });
    //     var li_img = '';
    //     var li_img_thumb = '';
    //     $(this).find('#img_list').children('input').each(function () {
    //         li_img = li_img + '<div class="gal-img item"><img src="'+ $(this).val() +'"/></div>';
    //         li_img_thumb = li_img_thumb + '<a class="item"><img src="'+ $(this).val() +'" alt=""></a>';
    //     });

    //     //for tag sold out
    //     if(home_sell_status == 'sold_out'){;
    //         $('#tag_sold').show();
    //     }else{
    //         $('#tag_sold').hide();
    //     }

    //     $('#index_homesell').text(index_value);
    //     $('#projectSlide').html();
    //     $('#projectSlide').html(li_img);
    //     $('#imgThumb').html();
    //     $('#imgThumb').html(li_img_thumb);
    //     $('#projectSlide').trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
    //     $('#projectSlide').find('.owl-stage-outer').children().unwrap();
    //     $('#imgThumb').trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
    //     $('#imgThumb').find('.owl-stage-outer').children().unwrap();
    //     modelThumb();

    //     var desc_home = '';
    //     desc_home += '<p class="title">'+ title_home +'</p>';
    //     desc_home += '<p class="title green">แบบบ้าน '+ format_home +'</p>';
    //     desc_home += '<p class="title green">หมายเลขแปลง <span>'+id_home+'</span></p>';
    //     desc_home += '<p class="title">ลักษณะแปลงที่ดิน</p>'+identity_home;
    //     desc_home += '<p class="title green">ราคา '+ price_home +'</p>';
    //     desc_home += '<a href="'+home_sell_id+'" class="btn btn-seemore">See more</a>';

    //     var index_plan = $('#index_plan');
    //     index_plan.text(index_value+'/'+amount_value+' แปลง')

    //     $('#desc_home').html(desc_home);

    //     $('html, body').animate({
    //         scrollTop: $("#sec-home-sale").offset().top - 60
    //     }, 800);
    // });
}

function modelThumb() {

    var winW = $(window).width();

    if( winW <= 768 ) {
        var slidesPerPage = 3;
    }
    else {
        var slidesPerPage = 6;
    }

    var sync1 = $('#projectSlide');
    var sync2 = $('#imgThumb');
    var flag = false;
    var duration = 300;


    var isMulti = ($('#projectSlide.owl-carousel img').length > 1) ? true : false
    sync1.owlCarousel({
        items: 1,
        animateOut: 'fadeOut',
        nav: isMulti,
        dots: false,
        autoHeight: true,
        loop:isMulti
        //autoplay: true
    })
        .on('change.owl.carousel', function (e) {
            if (e.namespace && e.property.name === 'position' && !flag) {
                flag = true;
                sync2.trigger('to.owl.carousel', [e.relatedTarget.relative(e.property.value), duration, true]);
                flag = false;
            }
        });

    sync2.owlCarousel({
        items: slidesPerPage,
        nav: false,
        slideBy: slidesPerPage,
        dots: false,
        margin: 10,
    })
        .on('click', '.owl-item', function (e) {
            e.preventDefault();
            sync1.trigger('to.owl.carousel', [$(this).index(), duration, true]);
        })
        .on('change.owl.carousel', function (e) {
            if (e.namespace && e.property.name === 'position' && !flag) {
                flag = true;
                sync1.trigger('to.owl.carousel', [e.relatedTarget.relative(e.property.value), duration, true]);
                flag = false;
            }
        });

    sync2.on('click', '.owl-item', function(e) {
        e.preventDefault();
        $('.item').removeClass('active');
        $(this).find('.item').addClass('active');
    });

    // var slider = $('#modelThumb').bxSlider({
    //     pagerCustom: '#modelThumb-pager'
    // });
    //
    // var winW = $(window).width();
    //
    // // responsive
    // if( winW <= 768 ) {
    //     var items = 3;
    // }
    // else {
    //     var items = 6;
    // }
    //
    // $('.background-arrow-left').click(function(){
    //     slider.goToPrevSlide();
    //     return false;
    // });
    //
    // $('.background-arrow-right').click(function(){
    //     slider.goToNextSlide();
    //     return false;
    // });
    //
    // $('#modelThumb-pager').owlCarousel({
    //     loop:true,
    //     margin: 5,
    //     nav:true,
    //     dots: false,
    //     autoHeight: true,
    //     responsive:{
    //         0:{
    //             items:items
    //         }
    //     }
    // });


}

function stickyMenuProject() {
    var winH = $(window).height();
    var pageH = winH - 70

    $('#main-nav li a').click(function() {
        $('#main-nav li a').removeClass('active');
        var id = $(this).attr('id');
        $(this).addClass('active');
        // console.log(id);
        var margin_top_animate = $(this).attr('data-family') == "child" ? 115 : 70;
        $('html, body').animate({
            scrollTop: $('#sec-'+id).offset().top - margin_top_animate
        }, 1000);
    });

}

function toLocation() {
    window.open('https://maps.google.com?saddr=Current+Location&dirflg=d&daddr='+$('#lat').val()+','+$('#lng').val(),'_blank');
}
function toLocationByWalk() {
    window.open('https://maps.google.com?saddr=Current+Location&dirflg=w&daddr='+$('#lat').val()+','+$('#lng').val(),'_blank');
}

function radionAppoint() {

    $('.radio-checkmark input[type="radio"]').click(function() {
        if($('#check-appoint').is(':checked')) {
            $('input#date_appointment').css('display','block');
        }
        else {
            $('input#date_appointment').css('display','none');
        }
    });
}

function colSlide() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        var stagePadding = 25;
        var margin = -10;
        var autoH = true;
    }
    else {
        var stagePadding = 0;
        var margin = 10;
        var autoH = false;
    }

    var isMulti = ($('#colSlide .item').length > 3) ? true : false
    $('#colSlide').owlCarousel({
        loop:isMulti,
        margin: margin,
        stagePadding: stagePadding,
        nav:isMulti,
        dots: isMulti,
        autoHeight: autoH,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:1
            },
            979:{
                items:2
            },
            1199:{
                items:3
            }
        }
    });
}

//Function End