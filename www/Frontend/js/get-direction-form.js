$(document).ready(function () {

    var zone_direction_id    =   null;
    var project_direction_id  =   null;
    var zone_direction_slug = null;

    $(document).on("click","ul#directlocationLists li",function()
    {
        zone_direction_id  =   $(this).attr('data-id');
        zone_direction_slug  = $(this).attr('data-slug');
        var params      =   {
            zone_id:zone_direction_id,
        };

        getDirectionData(params);
        $('#project_zone_id').val(zone_direction_slug);
        $('#project_direction_id').val('');
        if(zone_direction_id){
            $('#validateZone').hide();
        }
    });

    $(document).on("click","ul#directprojectLists li",function()
    {
        project_direction_id  =   $(this).attr('data-id');
        project_direction_slug =   $(this).attr('data-slug');
        $('#project_direction_id').val(project_direction_slug);
        $('#project_real_id').val(project_direction_id);
        if(project_direction_slug){
            $('#validateProjectName').hide();
        }

        // var url = document.URL,
        //     shortUrl=(document.URL).substring(0,url.lastIndexOf("/"));
        // project_id  =   $(this).attr('data-id');
        // window.location.href = shortUrl+'/get-direction.php?project_id='+project_id;
    });

    $('#getDirection').submit(function (e) {
        if($('#project_direction_id').val() != '' ){
            var path_zone=($('#project_zone_id').val()==''?"":$('#project_zone_id').val());
            
            var path_project=($('#project_direction_id').val()==''?"": $('#project_direction_id').val());
            var get_path=path_zone+path_project;
            var get_path_full=path_zone+path_project;
            //window.location.href = path_zone;
            console.log(path_project)
            window.open(path_project , '_blank');
            e.preventDefault();
        }else{
            if($('#project_direction_id').val() == ''){
                $('#validateProjectName').show();
            }
            if($('#project_zone_id').val() == ''){
                $('#validateZone').hide();
            }
            return false;
        }
    });

});

function renderDirectionSelect(result) {
    console.log(result)
    if(result.projects){
        var projects = result.projects;
        $('#directprojectLists').empty();
        $.each(projects,function (i,val) {
    $('#directprojectLists').append('<li data-id="'+val.project_id+'" data-slug="'+val.map_link+'" data-value="'+val.project_name_th+'">'+val.project_name_th+'</li>')
});
getdirectionDropdown();
$('#directprojectLists').parent().find('.searchBlock span').text('เลือกชื่อโครงการ');
}
}

function getDirectionData(params) {
    $.ajax({
        type: "POST",
        url: "https://uat.lh.co.th/get-direction-form-th",
        data: params,
        async: false,
        cache: false
    }).success(function(data) {
        var aaa = params;
        renderDirectionSelect(data);
    });
}