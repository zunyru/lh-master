
     $(function(){

        var ms_check_type = '';
        var ms_namecont   = '';
        var ms_phonecont  ='';
        var ms_emailcust  = '';
        var ms_detailcont ='';
        var ms_bot        ='';
       
       

        if($('#lang').val() == 'TH'){
        
         ms_namecont   = 'กรุณาระบุชื่อ !';
         ms_phonecont  ='กรุณาระบุเบอร์โทรศัพท์ !';
         ms_emailcust  = 'กรุณาระบุอีเมล !';
         ms_detailcont = 'กรุณาระบุข้อความ !';
         ms_check_type = 'กรุณาเลือก !';
         ms_bot        = 'พิสูจน์ว่าท่านไม่ใช่บอท !';
         ms_datecont   = 'เลือกวันที่';
     }else{
        ms_namecont   = 'Please Enter Full Name !';
        ms_phonecont  ='Please Enter Tel No. !';
        ms_emailcust  = 'Please Enter Email !';
        ms_detailcont = 'Please Enter Remark !';
        ms_check_type = 'Please Enter Check !';
        ms_bot        = 'Prove You Are Not A Bot !';
        ms_datecont   = 'Please Enter Appointment Date';

     }

     $('#btnsendmailhome').click(function() { 
        $('#Projectcontact').validate({
            ignore: ".ignore",
            rules: {
                check_type:{
                    required: true,
                },
                namecont: {
                    required: true,
                    minlength: 4
                },
                phonecont : {
                 required: true, 
             },
             emailcust : {
                 required: true,
                 email: true,
             },
             detailcont :{
                required: true,
            },
            datecont:{
                required: function () {
                    if($('#check-contact').is(":checked")){
                        console.log("ติดต่อสอบถาม");
                        return false;
                    } else {
                        console.log("นัดหมายเยี่ยมชมโครงการ");
                        return true;
                    }
                }
            },
            hiddenRecaptcha: {
                required: function () {
                    var googleResponse1 = $('#g-recaptcha-response-1').val();
                    if(googleResponse1 == ''){
                        return true;
                    } else {
                        return false;
                    }
                }
            }
            
        },
        messages: {
            check_type : ms_check_type,
            namecont: ms_namecont,
            phonecont : ms_phonecont,
            emailcust : ms_emailcust,
            detailcont : ms_detailcont,
            hiddenRecaptcha : ms_bot,
            datecont: ms_datecont,
        }, 
           submitHandler: function(form) {
                //var url ="https://www.lh.co.th/edm/service.php";
                var url = " http://uat.lh.co.th/edm_v4/service.php";

                var contact = $(".check-type:checked").val();
                var date_appointment = $('#date_appointment').val();
                var email_lh = $('#mailcont').val();
                var customer_name = $('#nameconts').val();
                var tell = $('#phoneconts').val();
                var email_cus = $('#emailcusts').val();
                var content = $('#detailconts').val();
                var T = $('#check-tel:checked').val();
                var E = $('#check-mail:checked').val();
                var project_id = $('#project_ids').val();
                var project_name = $('#project_name').val();
                console.log(project_name);

                if(T === undefined && E === undefined){
                    contact_us = "";
                }else if(T === undefined){
                    contact_us = 'Email';
                }else if(E === undefined){
                    contact_us = 'Tell';
                }else{
                    contact_us = 'Tell' + ' , ' + 'Email';
                }
                 
               

                var dataContact = {

                    'project_id' : project_id,
                    'project_name' : project_name,
                    'contact': 'contact',
                    'email_lh': email_lh,
                    'customer_name': customer_name,
                    'tell': tell,
                    'email_cus': email_cus,
                    'content': content,
                    'contact_us' : contact_us,
                };

                var dataAppoint = {

                    'project_id' : project_id,
                    'project_name' : project_name,
                    'contact': 'appointment',
                    'date_appointment': date_appointment,
                    'email_lh': email_lh,
                    'customer_name': customer_name,
                    'tell': tell,
                    'email_cus': email_cus,
                    'content': content,
                    'contact_us' : contact_us,
                };

                
                var data_send; 
                if($('#check-contact').is(":checked")){
                    data_send = dataContact;
                }else{
                    data_send = dataAppoint;
                }
                console.log(data_send);

            $.ajax({
                   type: 'POST',
                   url: url,
                   dataType : 'xml',
                   data :  data_send,
                   success: function(data)
                   {
                      var obj_result  =  xmlToJson(data);
                      //$('#btnsendmailhome').prop('disabled', true);
                      if(obj_result['return']['status']['#text'] == 'success'){
                       swal({
                         title: "ส่งข้อมูลเรียบร้อย !",
                         text: "ขอบคุณค่ะ !",
                         //timer: 3000,
                         showConfirmButton: true,
                         type : "success",
                     },
                      function(){
                        //console.log('click On');
                        $(document).ready(function() {
                          $("html,body").animate({scrollTop: 0}, 1000); //100ms for example
                        });

                    });
                   }else{
                      swal({
                         title: "ส่งข้อมูลเรียบร้อย !",
                         text: " แจ้ง "+obj_result['return']['error_message']['#text'],
                         //timer: 5000,
                         showConfirmButton: true,
                         type : "warning",
                     },
                      function(){
                        //console.log('click On');
                        $(document).ready(function() {
                          $("html,body").animate({scrollTop: 0}, 1000); //100ms for example
                        });

                    });
                  }

                   },error:function(e){
                       console.log(e); 
                       swal({
                        title: "ผิดพลาด !",
                        text: "เกิดปัญหาระหว่างการส่ง!",
                        //timer: 3000,
                        showConfirmButton: true,
                        type : "error",
                      }); 
                   }
                 });
                return false;
           },
   

    });    
    });

    $('#Projectcontact').on('submit', function(){
        var googleResponse1 = $('#g-recaptcha-response-1').val();
        if(googleResponse1 == ''){

           // $("#btn_submit").attr('disabled',true);
            //$('#chacheck1').show();
            return false;

        }else{

            //$("#btn_submit").attr('disabled',false);
            $('#chacheck1').hide();
        }
    });  

    $('#mailcont').css("cursor","not-allowed"); 

     $('#phonecont').mask('000-0000000');
     $('#emailcust').mask("A", {
         translation: {
             "A": { pattern: /[\w@\-.+]/, recursive: true }
         }
     });
 });    
