<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

$motivos = getMotivo();
$page = 'motivo-magazine';
$page_index = 0;
$img = '';
?>
<?php
include('header.php');
//elementMetaTitleDisTag($page);
?>
    <!-- CSS -->
    <link rel="stylesheet" href="<?= file_path('css/motivo.css') ?>" type="text/css">


    <div class="page-banner-md banner-hide">
        <div id="bannerSlide">
            <div class="item">
                <div class="banner-parallax">
                    <img src="<?= file_path('images/temp2/banner_motivo.jpg') ?>" alt="">
                </div>
            </div>
        </div>
    </div>

<div id="content" class="content">
    <div class="container">
        
        <?php
        $SEO = getSEOUrl($actual_link);
        if($actual_link == @$SEO->url_page){
            echo '<h1 class="heading-title">'.$SEO->h1.'<h1>';
        }else{
           echo '<h1 class="heading-title">นิตยสาร Motivo</h1>'; 
       }
       ?>

            <div class="latest-lists">
                <div class="row">
                    <?php
                        $first_motivo = null;
                        if(!empty($motivos)){
                            $first_motivo = $motivos[0];

                            array_shift($motivos);
                        }
                    ?>

                    <?php
                    if (!empty($first_motivo)){
                        ?>
                        <div class="col-md-6">
                            <div class="item main-item">

                                <div class="img-block">
                                    <?php
                                    if($first_motivo->motivo_status == 'New'){
                                        ?>
                                        <div class="newseriestag new-md">
                                            <div class="newtagtext">
                                                New
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <img src="<?= backend_url('motivo',$first_motivo->motivo_img_master ) ?>" alt="">
                                </div>
                                <div class="motivo-details">
                                    <p class="m-title"><?= $first_motivo->motivo_name ?></p>

                                    <p>
                                        <?= $first_motivo->motivo_detail ?>
                                        <a href="<?= backend_url('motivo',$first_motivo->motivo_file ) ?>" class="download-ic" target="_blank"><i class="i-download"></i></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    <?php
                    }
                    ?>

                    <?php
                    if (!empty($motivos)){
                        ?>
                        <div class="col-md-6">

                            <?php
                            foreach ($motivos as $i => $motivo){
                                if($i < 3){
                                ?>

                                    <div class="item list-2">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="img-block">
                                                    <?php
                                                    if($motivo->motivo_status == 'New'){
                                                        ?>
                                                        <div class="newseriestag new-sm">
                                                            <div class="newtagtext">
                                                                New
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                    <img src="<?= backend_url('motivo',$motivo->motivo_img_master ) ?>" alt="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="motivo-details">
                                                    <p class="m-title"><?= $motivo->motivo_name ?></p>
                                                    <p><?= $motivo->motivo_detail  ?></p>
                                                    <p>
                                                        <a href="<?= backend_url('motivo',$motivo->motivo_file ) ?>" class="download-ic" target="_blank"><i class="i-download"></i></a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                <?php
                                array_shift($motivos);
                                }else{
                                    break;
                                }
                            }
                            ?>

                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>

            <?php
            if (!empty($motivos)){
                ?>
                <div class="motivo-lists">

                        <?php
                        foreach ($motivos as $i => $motivo){
                            $index = $i +1 ;
                            ?>

                            <?php if($index % 3 == 1) {
                                ?>
                                <div class="row">
                                <?php
                            }?>
                                    <div class="col-md-4">
                                        <div class="item">
                                            <div class="img-block">
                                                <?php
                                                if($motivo->motivo_status == 'New'){
                                                    ?>
                                                    <div class="newseriestag new-sm">
                                                        <div class="newtagtext">
                                                            New
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                <img src="<?= backend_url('motivo',$motivo->motivo_img_master ) ?>" alt="">
                                            </div>
                                            <div class="motivo-details">
                                                <p class="m-title"><?= $motivo->motivo_name ?></p>
                                                <p><?= $motivo->motivo_detail?><br> <a href="<?= backend_url('motivo',$motivo->motivo_file ) ?>" class="download-ic" target="_blank"><i class="i-download"></i></a></p>
                                            </div>
                                        </div>
                                    </div>
                            <?php
                            if($index % 3 == 0){
                                ?>
                                </div>
                                <?php
                            }
                            ?>
                        <?php
                            array_shift($motivos);
                        } ?>

                </div>
                <?php
            }
            ?>
        </div>
    </div>


<?php include('footer.php'); ?>
