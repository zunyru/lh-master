<?php

require_once  'include/help/begin.php';
require_once  'include/help/query_function.php';

$product_id = $_POST['product_id'];
$zone_id    = $_POST['zone_id'];
$brand_id   = $_POST['brand_id'];

//case only product_id

if( !empty($product_id) && empty($zone_id) && empty($brand_id)){
    $response['zones'] = getZoneByProductID($product_id);
    $response['brands'] = getBrandByZoneIDAndProductID($product_id);
    //get project url for each project
    $projects = getSearchDropdownResult($product_id);
    // print_r($projects);
    // exit();
    foreach ($projects as $i => $project){
        $product_arr= Helper::getArrayProductOfProject($project->project_id);
        if(in_array(1,$product_arr) || in_array(2,$product_arr) || !in_array(3,$product_arr)){
            if(isLadawan($project->project_id)){
                $project_url =  $router->generate('ladawan-detail',[
                    'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                    'product_id'    =>  1
                ]); 
            }else{

              if(in_array(2,$product_arr)) {
               $project_url =  $router->generate('town-home-detail',[
                'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                'product_id'    =>  2
            ]); 
           }else{
            $project_url =  $router->generate('single-home-detail',[
                'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                'product_id'    =>  1
            ]);
        }
    }
}
else{
    $project_url = isLadawan($project->project_id) ?
    $router->generate('ladawan-detail',[
        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
        'product_id'    =>  3
    ]) :
    $router->generate('condominium-detail',[
        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
        'lang'          =>  'th'
    ]);
}
$projects[$i]->project_url = $project_url;
}
$response['projects'] = $projects;
responseData($response);
}elseif (!empty($product_id) && !empty($zone_id) && empty($brand_id)){
    $response['brands'] = getBrandByZoneIDAndProductID($product_id,$zone_id);
    $projects = getSearchDropdownResult($product_id,$zone_id,$brand_id);
    foreach ($projects as $i => $project){
        $product_arr= Helper::getArrayProductOfProject($project->project_id);
        if(in_array(1,$product_arr) || in_array(2,$product_arr) || !in_array(3,$product_arr)){
            if(isLadawan($project->project_id)){
                $project_url =  $router->generate('ladawan-detail',[
                    'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                    'product_id'    =>  1
                ]); 
            }else{

              if(in_array(2,$product_arr)) {
               $project_url =  $router->generate('town-home-detail',[
                'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                'product_id'    =>  2
            ]); 
           }else{
            $project_url =  $router->generate('single-home-detail',[
                'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                'product_id'    =>  1
            ]);
        }
    }
}
else{
    $project_url = isLadawan($project->project_id) ?
    $router->generate('ladawan-detail',[
        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
        'product_id'    =>  3
    ]) :
    $router->generate('condominium-detail',[
        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
        'lang'          =>  'th'
    ]);
}
$projects[$i]->project_url = $project_url;
}
$response['projects'] = $projects;
responseData($response);
}elseif (!empty($product_id) && empty($zone_id) && !empty($brand_id)){
    $response['zones'] = getZoneByProductID($product_id);
    //$response['brands'] = getBrandByZoneIDAndProductID($product_id,$zone_id);
    $projects = getSearchDropdownResult($product_id,$zone_id,$brand_id);
    foreach ($projects as $i => $project){
        $product_arr= Helper::getArrayProductOfProject($project->project_id);
        if(in_array(1,$product_arr) || in_array(2,$product_arr) || !in_array(3,$product_arr)){
            if(isLadawan($project->project_id)){
                $project_url =  $router->generate('ladawan-detail',[
                    'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                    'product_id'    =>  1
                ]); 
            }else{

              if(in_array(2,$product_arr)) {
               $project_url =  $router->generate('town-home-detail',[
                'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                'product_id'    =>  2
            ]); 
           }else{
            $project_url =  $router->generate('single-home-detail',[
                'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                'product_id'    =>  1
            ]);
        }
    }
}
else{
    $project_url = isLadawan($project->project_id) ?
    $router->generate('ladawan-detail',[
        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
        'product_id'    =>  3
    ]) :
    $router->generate('condominium-detail',[
        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
        'lang'          =>  'th'
    ]);
}
$projects[$i]->project_url = $project_url;
}
$response['projects'] = $projects;
responseData($response);
}elseif (empty($product_id) && !empty($zone_id) && empty($brand_id)){
    $response['brands'] = getBrandByZoneIDAndProductID(null,$zone_id);
    //$response['projects'] = getSearchDropdownResult(null,$zone_id,null);
    $projects = getSearchDropdownResult($product_id,$zone_id,$brand_id);
    foreach ($projects as $i => $project){
        $product_arr= Helper::getArrayProductOfProject($project->project_id);
        if(in_array(1,$product_arr) || in_array(2,$product_arr) || !in_array(3,$product_arr)){
            if(isLadawan($project->project_id)){
                $project_url =  $router->generate('ladawan-detail',[
                    'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                    'product_id'    =>  1
                ]); 
            }else{

              if(in_array(2,$product_arr)) {
               $project_url =  $router->generate('town-home-detail',[
                'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                'product_id'    =>  2
            ]); 
           }else{
            $project_url =  $router->generate('single-home-detail',[
                'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                'product_id'    =>  1
            ]);
        }
    }
}
else{
    $project_url = isLadawan($project->project_id) ?
    $router->generate('ladawan-detail',[
        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
        'product_id'    =>  3
    ]) :
    $router->generate('condominium-detail',[
        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
        'lang'          =>  'th'
    ]);
}
$projects[$i]->project_url = $project_url;
}
$response['projects'] = $projects;

responseData($response);
}elseif (empty($product_id) && !empty($zone_id) && !empty($brand_id)){
    //$response['brands'] = getBrandByZoneIDAndProductID(null,$zone_id);
    //$response['projects'] = getSearchDropdownResult(null,$zone_id,null);
    $projects = getSearchDropdownResult($product_id,$zone_id,$brand_id);
    foreach ($projects as $i => $project){
        $product_arr= Helper::getArrayProductOfProject($project->project_id);
        if(in_array(1,$product_arr) || in_array(2,$product_arr) || !in_array(3,$product_arr)){
            if(isLadawan($project->project_id)){
                $project_url =  $router->generate('ladawan-detail',[
                    'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                    'product_id'    =>  1
                ]); 
            }else{

              if(in_array(2,$product_arr)) {
               $project_url =  $router->generate('town-home-detail',[
                'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                'product_id'    =>  2
            ]); 
           }else{
            $project_url =  $router->generate('single-home-detail',[
                'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                'product_id'    =>  1
            ]);
        }
    }
}
else{
    $project_url = isLadawan($project->project_id) ?
    $router->generate('ladawan-detail',[
        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
        'product_id'    =>  3
    ]) :
    $router->generate('condominium-detail',[
        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
        'lang'          =>  'th'
    ]);
}
$projects[$i]->project_url = $project_url;
}
$response['projects'] = $projects;

responseData($response);
}elseif (empty($product_id) && empty($zone_id) && !empty($brand_id)){
    //$response['zones'] = getZoneByProductID($product_id);
    //$response['brands'] = getBrandByZoneIDAndProductID(null,$zone_id);
    $projects = getSearchDropdownResult($product_id,$zone_id,$brand_id);
    foreach ($projects as $i => $project){
        $product_arr= Helper::getArrayProductOfProject($project->project_id);
        if(in_array(1,$product_arr) || in_array(2,$product_arr) || !in_array(3,$product_arr)){
            if(isLadawan($project->project_id)){
                $project_url =  $router->generate('ladawan-detail',[
                    'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                    'product_id'    =>  1
                ]); 
            }else{

              if(in_array(2,$product_arr)) {
               $project_url =  $router->generate('town-home-detail',[
                'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                'product_id'    =>  2
            ]); 
           }else{
            $project_url =  $router->generate('single-home-detail',[
                'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                'product_id'    =>  1
            ]);
        }
    }
}
else{
    $project_url = isLadawan($project->project_id) ?
    $router->generate('ladawan-detail',[
        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
        'product_id'    =>  3
    ]) :
    $router->generate('condominium-detail',[
        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
        'lang'          =>  'th'
    ]);
}
$projects[$i]->project_url = $project_url;
}
$response['projects'] = $projects;
    //$response['projects'] = getSearchDropdownResult($product_id,$zone_id,$brand_id);
responseData($response);
}elseif (!empty($product_id) && !empty($zone_id) && !empty($brand_id)){
    $projects = getSearchDropdownResult($product_id,$zone_id,$brand_id);
    foreach ($projects as $i => $project){
        $product_arr= Helper::getArrayProductOfProject($project->project_id);
        if(in_array(1,$product_arr) || in_array(2,$product_arr) || !in_array(3,$product_arr)){
            if(isLadawan($project->project_id)){
                $project_url =  $router->generate('ladawan-detail',[
                    'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                    'product_id'    =>  1
                ]); 
            }else{

              if(in_array(2,$product_arr)) {
               $project_url =  $router->generate('town-home-detail',[
                'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                'product_id'    =>  2
            ]); 
           }else{
            $project_url =  $router->generate('single-home-detail',[
                'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                'product_id'    =>  1
            ]);
        }
    }
}
else{
    $project_url = isLadawan($project->project_id) ?
    $router->generate('ladawan-detail',[
        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
        'product_id'    =>  3
    ]) :
    $router->generate('condominium-detail',[
        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
        'lang'          =>  'th'
    ]);
}
$projects[$i]->project_url = $project_url;
}
$response['projects'] = $projects;
responseData($response);
}

function responseData($objArr)
{
    header('Content-Type: application/json');
    echo json_encode($objArr);
}