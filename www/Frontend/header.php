<!DOCTYPE html>
<?php $lang = !empty($_GET['lang']) ? $_GET['lang'] : 'th'; ?>
<html lang="th">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-NQRNBR');</script>
    <!-- End Google Tag Manager -->




     <?php
    $url = backend_url('base','');
    $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $url_pure = (substr($actual_link, 0, strpos($actual_link, "?"))==''?$actual_link:substr($actual_link, 0, strpos($actual_link, "?")));
    ?>
<!-- for zun -->
<!-- for Google -->
<?php
//add id
$app_id      = '659883840880609';
#new SEO
$url_web = getSEOUrl($actual_link);

if($actual_link == @$url_web->url_page){
  // SEO management
  include 'master_seo.php';

}elseif(isset($project_id)){
    // SEO mata tag Project page
    $project_seo  = getProjectSEOByID($project_id);

    // img Share
    $image = str_replace('fileupload/images/project_img/',$url.'fileupload/images/project_img/Thumbnails_',$project_seo->LEAD_IMAGE_PROJECT_FILE_NAME);

    // SEO NOT Home sell
    if(!isset($home_sell_id)){

                  // SEO lang TH
                  if(isset($lang)){


                        if($lang == 'th'){

                             //ShareSocialMedia lang TH
                            echo $Meta_data =  MataTagShareSocialMedia ($project_seo->project_title_seo_th,$project_seo->project_des_seo_th,$project_seo->project_keyword_th,$actual_link,$image,'1TH');
                        }else{

                             //ShareSocialMedia lang EN
                            echo $Meta_data =  MataTagShareSocialMedia ($project_seo->project_title_seo_en,$project_seo->project_des_seo_en,$project_seo->project_keyword_en,$actual_link,$image,'1EN');
                        }

                 }else{ //Not lang

                    //ShareSocialMedia
                    echo $Meta_data =  MataTagShareSocialMedia ($project_seo->project_title_seo_th,$project_seo->project_des_seo_th,$project_seo->project_keyword_th,$actual_link,$image,2);

                 }
        }else{
         //SEO Home sell
           $image_homesell =  getHomeSell_Image($home_sell_id);

             $image = $image_homesell->plan_img;

             $image = str_replace('fileupload/images/galery_plan/',$url.'fileupload/images/galery_plan/Thumbnails_',$image);

            //ShareSocialMedia Home Sell
            $description = htmlspecialchars($project_seo->project_des_seo_th." แบบบ้าน ".$plan->plan_name_th ." ". $home_sell->features_convert." ราคา ".Helper::getProjectPrice($conditionHomeSell->total_price). " ล้านบาท");

           //zun add in mail
            $title       = htmlspecialchars($project_seo->project_title_seo_th." แบบบ้าน ".$plan->plan_name_th);
           // $description = "Land & Houses บริษัทธุรกิจอสังหาริมทรัพย์ในประเทศไทย LHเปิดขายทั้งโครงการบ้านเดี่ยว ทาวน์โฮม คอนโดมิเนียมในกรุงเทพฯ ปริมณฑลและต่างจังหวัด";
            $keywords    = $project_seo->project_keyword_th;
            $Meta_data =  MataTagShareSocialMedia ($title,$description,$keywords,$actual_link,$image,2);
       }
   }elseif(isset($project_searchs)){
    //search
    $num_seaech = count($project_searchs);

        if($num_seaech > 0 ){

        $project_id = $project_searchs[0];
        $project = getProjectByID($project_id);
        $banner  = getBannerByProjectID($project_id);
        $brand   = getBrandByID($project->brand_id);
        $price   = getProjectPrice($project_id);
        $project_promotion = getProjectPromotion($project_id);

        $title = 'พบ ';
        if(!empty($_GET['product_id'])){
            $product_q = getProductByProductID($_GET['product_id']);
            $title .=  $product_q->product_name_th;

        }
        if(!empty($_GET['brand_id'])){
            $brandObj   = getBrandByID($_GET['brand_id']);
            $title .=  ' ' . count($project_searchs). ' โครงการจาก '.$brandObj->brand_name_th.' ที่น่าสนใจ ';
        }else{
            $title .= ' ' . count($project_searchs). ' โครงการที่น่าสนใจ ';
        }
        if(!empty($_GET['zone_id'])){
            $zone_q = getZoneByID($_GET['zone_id']);
            $title .=  ' บนทำเล '.$zone_q->zone_name_th;
        }
        //zun add in mail
        $title = 'บริษัท แลนด์ แอนด์ เฮ้าส์ จำกัด (มหาชน)';

        //$banner->LEAD_IMAGE_PROJECT_FILE_NAME;
        $project_des_seo_th = "Land & Houses บริษัทธุรกิจอสังหาริมทรัพย์ในประเทศไทย LHเปิดขายทั้งโครงการบ้านเดี่ยว ทาวน์โฮม คอนโดมิเนียมในกรุงเทพฯ ปริมณฑลและต่างจังหวัด";
        $project_keyword_th = "บ้านเดี่ยว, ทาวน์โฮม, คอนโด, คอนโดมิเนียม, โครงการบ้านใหม่";

        $image = str_replace('fileupload/images/project_img/',$url.'fileupload/images/project_img/Thumbnails_',$banner->LEAD_IMAGE_PROJECT_FILE_NAME);

        echo $Meta_data =  MataTagShareSocialMedia ($title ,$project_des_seo_th,$project_keyword_th,$actual_link,$image,'search');

    }else{
       //Not search
       include "title.php";
    }

}elseif(isset($page)){
   //ตาม page
    $lang = !empty($_GET['lang']) ? $_GET['lang'] : 'th';
    if(!empty($_GET['zone_id'])):
        #search zone by product
        include "seo_file.php";
    else:
        $mata_ = get_Mata_Title($page);
        if(isset($mata_))
           elementMetaTitleDisTag($page,$page_index,$lang);
    endif;

        }elseif(!isset($page)){
            if(isset($pages)):
              $mata_seo = get_Mata_SEO($pages,$pages_id);
              include "seo_file.php";
            else:
           //Not page
              include "title.php";
            endif;
        }
    ?>

    <?php if($_SERVER['REQUEST_URI']=='/'): ?>
        <!-- Edited by Yothin MFEC PCL -> ORGANIZATION STRUCTURED DATA for only Homepage -->
        <script type="application/ld+json"> { "@context" : "https://schema.org", "@type" : "Organization", "name" : "Land & Houses Public Company Limited", "image" : "https://www.lh.co.th/www/Frontend/images/global/header_logo.png", "description" : "บริษัท แลนด์ แอนด์ เฮ้าส์ จากัด (มหาชน) ประกอบธุรกิจประเภท ค้าอสังหาริมทรัพย์ โดยขายบ้านจัดสรรพร้อมที่เป็นส่วนใหญ่ โครงการที่ทาจะเป็นโครงการในเขตกรุงเทพมหานคร ปริมณฑลและโครงการตามจังหวัดใหญ่ๆ ได้แก่ เชียงใหม่ เชียงราย ขอนแก่น นครราชสีมา อุดรธานี หัวหิน ภูเก็ต มหาสารคาม และอยุธยา.", "address" : { "@type" : "PostalAddress", "addressCountry" : "TH", "addressLocality" : "Bangkok", "postalCode" : "10210", "streetAddress" : "Q. House Lumpini Building Fl. 37-38,, 1 South Satorn Rd. Tungmahamek, Sathon" }, "url" : "https://www.lh.co.th", "telephone" : "+66 (0) 2230 8657", "logo" : "https://www.lh.co.th/www/Frontend/images/global/header_logo.png", "sameAs" : ["https://www.facebook.com/landandhouses", "https://twitter.com/lhhome", "https://www.youtube.com/user/LandandHouses", "https://www.instagram.com/lhhome/"] }
        </script>
    <?php endif;?>
<!-- Add canonical in each pages -->
    <link rel="canonical" href="<?=$url_pure?>" />
    <meta charset="UTF-8">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="shortcut icon" href="<?= file_path('images/global/favicon.png') ?>" type="image/x-icon">

    <!-- CSS -->
     <link rel="stylesheet" href="<?= file_path('css/lib/bootstrap.min.css') ?>" type="text/css">
    <?php if(@$page == 'job'){?>
    <link rel="stylesheet" href="<?= file_path('css/datepicker.css') ?>" type="text/css">
    <?php }else{?>
    <link rel="stylesheet" href="<?= file_path('css/lib/bootstrap-datepicker.css') ?>" type="text/css">
    <?php }?>
    <link rel="stylesheet" href="<?= file_path('owl.carousel/assets/owl.carousel.css') ?>" type="text/css">
    <link rel="stylesheet" href="<?= file_path('css/fonts.css') ?>" type="text/css">
    <link rel="stylesheet" href="<?= file_path('css/global.css') ?>" type="text/css">
    <link rel="stylesheet" href="<?= file_path('css/lib/featherlight.min.css') ?>" type="text/css">
    <link rel="stylesheet" href="<?= file_path('css/lib/featherlight.gallery.min.css') ?>" type="text/css">
    <link rel="stylesheet" href="<?= file_path('js/cal_reg/css/sweetalert.css') ?>">

    <!-- JS -->
    <script src="<?= file_path('js/jquery-2.2.3.min.js') ?>"></script>
    <script src="<?= file_path('js/lib/bootstrap.min.js') ?>"></script>

    <?php if(@$page == 'job'){?>
    <script src="<?= file_path('js/bootstrap-datepicker.js') ?>"></script>
    <script src="<?= file_path('js/bootstrap-datepicker-custom.js')?> "></script>
    <script src="<?= file_path('js/locales/bootstrap-datepicker.th.js') ?>"></script>
    <?php }else{?>
    <script src="<?= file_path('js/lib/bootstrap-datepicker.min.js') ?>"></script>

    <?php }?>
    <script src="<?= file_path('owl.carousel/owl.carousel.min.js') ?>"></script>
    <script src="<?= file_path('js/lib/parallax.min.js') ?>"></script>
    <script src="<?= file_path('js/lib/featherlight.min.js') ?>"></script>
    <script src="<?= file_path('js/lib/featherlight.gallery.min.js') ?>"></script>
    <script src="<?= file_path('js/lib/freewall.js') ?>"></script>
    <script src="<?= file_path('js/global.js') ?>"></script>
    <script src="<?= file_path('js/search-form.js') ?>"></script>
    <script src="<?= file_path('js/get-direction-form.js') ?>"></script>
    <script src="<?= file_path('js/cal_reg/sweetalert.min.js') ?>"></script>
</head>

<body>

    <?php   
      if(isset($_SESSION['send'])){
         $_SESSION['send'];
        if($_SESSION['send']==1){

    ?>
       <script type="text/javascript">
           swal({
              title: "ส่งเรียบร้อยแล้ว! <br> ขอบคุณค่ะ",
              type: "success",
              html: true
            });
       </script>

    <?php
        }else{
    ?>
        <script type="text/javascript">
           swal({
              title: "ผิดพลาด ! <br> เกิดข้อผิดพลาดระหว่างดำเนินการส่ง",
              type: "error",
              html: true
            });
       </script>

    <?php
        }

        session_unset($_SESSION['send']);

    } ?>


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NQRNBR"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<div id="preloader">
    <div id="preloadLogo">
        <img src="<?= file_path('images/global/loading.gif') ?>" alt="" class="loading-img">
        <p>Loading...</p>
    </div>
</div>
<div id="page">
    <?php $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

    function url($str){
        if($str[0] == '/') $str = ltrim($str, '/');
        $full_url = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $full_path = str_replace(basename($full_url),'',$full_url);
        if($full_path == 'http:///') echo $full_url.$str;
        else echo str_replace(preg_replace("/(.*)\//", "",$full_url),"",$full_url).$str;
    }
    function backend_url($type,$str){
        if($type == 'base') {
            return 'http://devwww3.lh.co.th/www/Backend/'.$str;
        }
        if($type == 'logo') {
            return 'http://devwww3.lh.co.th/www/Backend/fileupload/images/brand_img/'.$str;
        }
        if($type == 'plan_img'){
            return 'http://devwww3.lh.co.th/www/Backend/fileupload/images/galery_plan/'.$str;
        }
        if($type == 'floor_plan'){
            return 'http://devwww3.lh.co.th/www/Backend/fileupload/images/floor_plan/'.$str;
        }
        if($type == 'floor_plan_master'){
            return 'http://devwww3.lh.co.th/www/Backend/fileupload/images/floor_plan_master/'.$str;
        }
        if($type == 'motivo'){
            return 'http://devwww3.lh.co.th/www/Backend/fileupload/images/motivo/'.$str;
        }
        if($type == 'series'){
            return 'http://devwww3.lh.co.th/www/Backend/fileupload/images/series_img/'.$str;
        }
        if($type == 'series_logo'){
            return 'http://devwww3.lh.co.th/www/Backend/fileupload/images/series_img/series_img_logo/'.$str;
        }
        if($type == 'galery_plan'){
            return 'http://devwww3.lh.co.th/www/Backend/fileupload/images/galery_plan/'.$str;
        }
        if($type == 'icon_activity'){
            return 'http://devwww3.lh.co.th/www/Backend/fileupload/images/icon_img/'.$str;
        }
    if($type == 'icon_line'){
                return 'http://devwww3.lh.co.th/www/Frontend/images/global/'.$str;
        }
        return null;
    }

        $current_page = basename($_SERVER["SCRIPT_FILENAME"], '.php');
        //print_r($current_page); exit;
//    url('/project_type.php');
//    exit;
    ?>


    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '342952452528044'); // Insert your pixel ID here.
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=342952452528044&ev=PageView&noscript=1"
        /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->

    <header id="header">
        <div class="">
            <div class="header-container">
                <a href="<?= $router->generate('home') ?>" class="main-logo" title="" alt=""></a>
                 <?php if(!isset($_GET['landing_page_id'])){?>
                <div class="lang-switch-m rps">
                    <a href="<?php echo changeLangSwitch('en');?>" >ENG <img src="<?php echo backend_url('icon_line','lang-en.png') ?>"></a> <i></i> <a style="font-size: 13px;" href="<?php echo changeLangSwitch('cn');?>">中文 <img src="<?php echo backend_url('icon_line','lang-cn.png') ?>"></a>
                </div>
            <?php }?>
                <a class="rps burger-menu" data-active="0"></a>

<!--todo hard code url-->

                <?php
                $menu = !empty($_GET['menu']) ? $_GET['menu'] : ''
                ?>

                <?php if ($menu == 'single' || strpos($url,'/project-info-home-soldout.php') !== false): ?>

                    <?php include('snippet/snippet-menu-project-info-home.php'); ?>

                <?php elseif ($menu == 'condominium'): ?>

                    <?php include('snippet/snippet-menu-project-info-condominium.php'); ?>

                <?php elseif ($menu == 'homesell'): ?>

                    <?php include('snippet/snippet-menu-project-info-sale.php'); ?>

                <?php elseif (strpos($url,'/home-vdo.php') !== false): ?>

                <?php //elseif (strpos($url,'/honor-awards.php') !== false || strpos($url,'/corporate-profile.php') !== false  || strpos($url,'/corporate-committee.php') !== false || strpos($url,'/corporate-ethics.php') !== false ): ?>

                    <?php //include('snippet/snippet-menu-company.php'); ?>

                <?php elseif ( $menu == 'lh-project' || $menu == 'foreign-owner' ): ?>

                    <?php include('snippet/snippet-menu-foreign.php'); ?>

                <?php elseif ( $menu == 'service-online' ): ?>

                    <?php include('snippet/snippet-menu-service.php'); ?>

                 <?php elseif (strpos($url,'/governance-rights-of-shareholders.php') !== false || strpos($url,'/governance-practice.php') !== false || strpos($url,'/governance-roles-of-stakeholders.php') !== false ||  strpos($url,'/governance-disclose.php') !== false || strpos($url,'/governance-responsible.php') !== false || strpos($url,'/governance-business-good.php') !== false || strpos($url,'/corporate-news.php') !== false || strpos($url,'/corporate-newsroom-set.php') !== false  || strpos($url,'/corporate-newsroom-press.php') !== false || strpos($url,'/corporate-newsroom-clippings.php') !== false || strpos($url,'/investor-relation.php') !== false || strpos($url,'/financial-information.php') !== false  || strpos($url,'/shareholder-information.php') !== false || strpos($url,'/stock-information.php') !== false || strpos($url,'/presentations.php') !== false || strpos($url,'/analyst-coverage.php') !== false || strpos($url,'/information-request.php') !== false || strpos($url,'/corporate-organization-chart.php') !== false || strpos($url,'/corporate-stakeholder.php') !== false || strpos($url,'/honor-awards.php') !== false || strpos($url,'/corporate-profile.php') !== false  || strpos($url,'/corporate-committee.php') !== false || strpos($url,'/corporate-ethics.php') !== false || strpos($url,'/corporate-committee-details.php') !== false ): ?>

                    <?php include('snippet/snippet-menu-corporate.php'); ?>



                <?php else: ?>
<!--                    <div class="main-navigation">-->
<!--                        <ul id="main-nav">-->
<!--                            <li><a class="m-home-series h-home" data-status="1" data-buff="0">บ้านเดี่ยว</a></li>-->
<!--                            <li><a class="m-home-series t-home" data-status="2" data-buff="0">ทาวน์โฮม</a></li>-->
<!--                            <li><a class="m-home-series c-condo" data-status="3" data-buff="0">คอนโดมิเนียม</a></li>-->
<!--                            <li><a class="m-home-series h-sell" data-status="4" data-buff="0">บ้านตกแต่งพร้อมขาย</a></li>-->
<!--                        </ul>-->
<!--                    </div>-->
                <?php endif; ?>

                <?php include('snippet/snippet-mainmenu.php'); ?>

                <?php include('snippet/snippet-menu-home-series.php'); ?>


<!--                <div class="top-icons">-->
<!--                    --><?php ////include('snippet-top-menuicon.php'); ?>
<!--                </div>-->

                <?php include('snippet/snippet-search.php'); ?>
                <?php include('snippet/snippet-menu-contact.php'); ?>
                <?php include('snippet/snippet-get-direction.php'); ?>
                <?php
                // Change Langs Function
                function changeLangSwitch($lang){
                    $curr_link=(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                    if ($lang=='en') {
                        if (strpos($curr_link, 'lh.co.th/th/') !== false) {
                            $link=str_replace("lh.co.th/th/","lh.co.th/en/",$curr_link);
                        }else{
                            $link=str_replace("lh.co.th","lh.co.th/th",$curr_link);
                        }
                    }elseif($lang=='cn'){
                       if (strpos($curr_link, 'lh.co.th/th/') !== false) {
                            $link=str_replace("lh.co.th/th/","lh.co.th/cn/",$curr_link);
                        }else{
                            $link=str_replace("lh.co.th","lh.co.th/th",$curr_link);
                        }
                    }
                    return $link;
                }
                ?>
                <!--Top Menu-->
                <div class="top-menu">
                    <div class="menu-close">CLOSE</div>
                    <?php if(!isset($_GET['landing_page_id'])){?>
                    <div class="lang-switch">
                        <a href="<?php echo changeLangSwitch('en');?>" >ENG <img src="<?php echo backend_url('icon_line','lang-en.png') ?>"></a> <i></i> <a href="<?php echo changeLangSwitch('cn');?>">中文 <img src="<?php echo backend_url('icon_line','lang-cn.png') ?>"></a>
                    </div>
                    <?php } ?>
                    <div class="menu top-submenu show-menu">
                        <div class="menu-line menu-line-1"></div>
                        <div class="menu-line menu-line-2"></div>
                        <div class="menu-line menu-line-3"></div>
                        <span>MENU</span>
                    </div>
                </div>


                <!--Responsive Menu-->
                <div class="rps-menu">
                    <div class="rps-menu-top">
                        <div class="rps-close"><i class="i-rps-close"></i>close</div>
                        <span class="rps-lang">
                            <?php
                            $lang = !empty($_GET['lang']) ? $_GET['lang'] : 'th';
                            if(!empty($page)){
                             ?>

                                           <?php if ($page == 'condo-type'){ ?>
                                           <?php if($lang == 'en'){ ?>
                                           <a href="<?= $router->generate('condominium-list',[
                                               'lang'         =>  'th'
                                               ]) ?>" style="display: inline-block;" class="<?= $lang == 'th' ? 'active' : '' ?>">TH</a>
                                               <?php }else{ ?>
                                               <a href="<?= $router->generate('condominium-list',[
                                                   'lang'         =>  'en'
                                                   ]) ?>" style="display: inline-block;" class="<?= $lang == 'en' ? 'active' : '' ?>">EN</a>

                                                   <?php } }elseif($page == 'condo-detail'){ ?>

                                                   <?php if($lang == 'en'){ ?>
                                                   <a href="<?= $router->generate('condominium-detail',[
                                                       'lang'         =>  'th',
                                                       'project_name' =>  str_replace(' ', '-', $project_lang ->project_name_th)
                                                       ]) ?>" style="display: inline-block;" class="<?= $lang == 'th' ? 'active' : '' ?>">TH</a>
                                                       <?php }else{ ?>
                                                       <a href="<?= $router->generate('condominium-detail',[
                                                           'lang'         =>  'en',
                                                           'project_name' =>  str_replace(' ', '-', $project_lang ->project_name_th)

                                                           ]) ?>" style="display: inline-block;" class="<?= $lang == 'en' ? 'active' : '' ?>">EN</a>

                                                           <?php } } } ?>

                        </span>
                    </div>
                    <div class="responsive-menu">
                        <p class="submenu-title">ข้อมูลโครงการ</p>
                        <ul>
                            <li><a href="<?= $router->generate('furnished-home-list') ?>">บ้านตกแต่งพร้อมขาย</a></li>
                            <li><a href="<?= $router->generate('single-home-list') ?>">บ้านเดี่ยว</a></li>
                            <li><a href="<?= $router->generate('town-home-list') ?>">ทาวน์โฮม</a></li>
                            <li><a href="<?= $router->generate('condominium-list') ?>">คอนโดมิเนียม</a></li>
                            <li><a href="<?= $router->generate('ladawan-list') ?>">LADAWAN</a></li>
                <li><a href="https://www.lh.co.th/vive">VIVE</a></li>
                            <li><a href="<?= $router->generate('country-list') ?>">โครงการในต่างจังหวัด</a></li>
                            <li><a href="<?= $router->generate('new-project-list') ?>">ข่าวโครงการใหม่</a></li>
                            <li><a href="<?= $router->generate('promotion') ?>">โปรโมชั่น และสิทธิพิเศษ</a></li>
                            <li><a href="<?= $router->generate('review') ?>">Project Review</a></li>

                            <li><a href="#">Foreign Buyers</a>
                                <ul>
                                    <li><a href="<?= $router->generate('lh-project') ?>">LH Projects</a></li>
                                    <li><a href="https://www.lh.co.th/thebestcondominiumsinbangkok/" >LH Condominiums </a></li>
                                    <li><a href="<?= $router->generate('foreign-owner') ?>">Foreign Ownership of Real Estate Property</a></li>
                                </ul>
                            </li>
                            <li><a href="<?= $router->generate('tvc') ?>">TVC</a></li>
                        </ul>
                        <p class="submenu-title">แนวคิดการออกแบบ</p>
                        <ul>
                            <li><a href="<?= $router->generate('home-series') ?>">แบบบ้าน</a></li>
                            <li><a href="<?= $router->generate('360-virtual') ?>">ภาพถ่าย 360 องศา</a></li>
                            <li><a href="<?= $router->generate('air-plus') ?>">เทคโนโลยี Air Plus</a></li>
                            <!--
                            <li><a href="<?= $router->generate('concept') ?>">แนวคิดบ้านสบาย</a></li>
                             -->
                            <li><a href="<?= $router->generate('living-tips') ?>">LH Living Tips</a></li>
                            <li><a href="<?= $router->generate('motivo') ?>">นิตยสาร Motivo</a></li>
                        </ul>
                        <p class="submenu-title">ข้อมูลบริษัท</p>
                        <ul>
                            <li><a href="http://lh-th.listedcompany.com/corporate_information.rev">ข้อมูลบริษัท</a></li>
                            <li><a href="http://lh-th.listedcompany.com/right_shareholder.rev">ข้อมูลบรรษัทภิบาล</a></li>
                            <li><a href="http://lh-th.listedcompany.com/home.rev">ข้อมูลนักลงทุน</a></li>
                            <li><a href="http://lh-th.listedcompany.com/company_news.rev">ข่าวสารองค์กร</a></li>
                        </ul>
                        <p class="submenu-title">ลูกบ้านสัมพันธ์</p>
                        <ul>
                            <li><a href="<?= $router->generate('community') ?>">LH Community</a></li>
                            <li><a href="http://www7.lh.co.th/EService/LoginForm.do">แจ้งซ่อมออนไลน์</a></li>
                            <li><a href="http://www7.lh.co.th/LHComplaint/index.jsp">ร้องเรียนเรื่องบ้านและคอนโด</a></li>
                        </ul>
                        <p class="submenu-title">ติดต่อเรา</p>
                        <ul>
                            <li><a href="<?= $router->generate('job-with-us') ?>">สมัครงาน</a></li>
                            <li><a href="<?= $router->generate('land-offer') ?>">เสนอขายที่ดิน</a></li>
                            <li><a href="<?= $router->generate('report') ?>">แจ้งปัญหาการใช้งานบนเว็บไซต์</a></li>
                            <li hidden><a href="<?= $router->generate('contact-us') ?>">ติดต่อสอบถามข้อมูลทั่วไป</a></li>
                        </ul>
                        <div class="ft-social">
                            <ul>
                                <li><a href="https://www.facebook.com/landandhouses" class="ft-fb" target="_blank">facebook</a></li>
                                <li><a href="https://twitter.com/lhhome" class="ft-tw" target="_blank">twitter</a></li>
                                <li><a href="https://www.youtube.com/user/LandandHouses" class="ft-yt" target="_blank">youtube</a></li>
                                <li><a href="https://www.instagram.com/lhhome/" class="ft-ig" target="_blank">instagram</a></li>
                                <li><a href="https://line.me/R/ti/p/%40yez1340o" target="_blank" class="ft-li" target="_blank">line</a></li>
                                <li><a href="https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=Mzg4NzE1NzAxNA==#wechat_redirect" target="_blank" class="ft-li wechat" target="_blank">we chat</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--Responsive Menu-->

                <span class="ribbon-rip"></span>
            </div>
        </div>
    </header>
        <div class="sticky-block">
