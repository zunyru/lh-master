<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';
require_once 'include/help/query_independent.php';

$landing_page_id = $_GET['landing_page_id'];

if(empty($landing_page_id) || getLandingPageDetail($landing_page_id) == false){
    echo 'not have a project for this project id';
}

$landing_page = getLandingPageDetail($landing_page_id);
$pages = 'LH_LANDING_PAGE';
$pages_id = $landing_page_id;
?>
<?php include('header.php'); ?>
    <!-- CSS -->
    <link rel="stylesheet" href="css/review-details.css" type="text/css">
    <!-- JS -->
    <!-- <script src="js/review-details.js"></script> -->
    <style type="text/css">
        video{
            max-width: 100%;
        }
    </style>



<div class="page-nav">
    <div class="container">

    </div>
</div>

<div id="content" class="content">
    <div class="container">
             <!-- H1 -->
                <?php

                $SEO = getSEOUrl($actual_link);
                $url_ = str_replace("%2F","/",urldecode(@$SEO->url_page));
                $url_ =  str_replace("%3A",":",urldecode($url_));

                if($actual_link == @$SEO->url_page || $actual_link == $url_){
                    echo "<h1>".$SEO->h1."</h1>";
                }elseif (isset($landing_page->h1)) {
                    echo  "<h1>".$landing_page->h1."</h1>";
                }
                ?>
                <!-- End H1 -->
    <div class="review-content landing-content">

        <div class="content-block">
            <!-- H1 -->

            <!-- End H1 -->
            <!--                    /image/file_content_web-->
            <?= str_replace('../build/images/file_content_web/', '/www_th/build/images/file_content_web/',  $landing_page->landing_page_content_th);
            ?>
        </div>

            </div>

        </div>
    </div>

    <script type="text/javascript">
        $( document ).ready(function() {
            $('video').attr("poster","http://uat.lh.co.th/www_th/Frontend//images/logo/vdo_backgroup.png");
            // $('video').attr("muted","muted");
            // $('video').attr("playsinline","playsinline");
        });
    </script>

<?php include('footer.php'); ?>
