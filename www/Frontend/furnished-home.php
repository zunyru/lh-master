<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';
require_once 'include/help/hex2rgba.php';

$home_sells = getHomeSellHouseY('desc');
$page = 'furnished-home';
$page_index = 3;
//Helper::sortByPriceHomeSell($home_sells,'desc');

?>
<?php
include('header.php');
//elementMetaTitleDisTag($page,$page_index);
?>
    <!-- JS -->
    <link rel="stylesheet" href="<?= file_path('css/furnished-home.css') ?>" type="text/css">
    <link rel="stylesheet" href="<?= file_path('css/video.css') ?>" type="text/css">

    <script src="<?= file_path('js/furnished-home.js') ?>"></script>

    <div class="page-banner page-scroll slider">
        <div id="pjHomeBanner" class="owl-carousel owl-theme">

            <?php
            $countProjectZone = 0;
            if($home_sells != false){
                foreach ($home_sells as $home_sell) {
                    $project_sub = getProjectSubBySubID($home_sell->project_sub_id);
                    if(!empty($project_sub)){
                        $project = getProjectByID($project_sub->project_id);
                        if (empty($_GET['zone_id']) || (!empty($_GET['zone_id']) && $project != false && $_GET['zone_id'] == $project->zone_id )) {
                            $countProjectZone++;
                        }
                    }
                }
            }
            ?>

            <?php
            renderBannerPageList(3);
            ?>

        </div>
        <span id="scrollNext" class="i-scroll-next"></span>
    </div>

    <?php
    renderHiddenLeadImage(3,'pjHomeBanner');
    renderActivityResponsive();
    $zones = getHomeSellZones();
    if(!empty($_GET['zone_id'])){
        foreach($zones as $zone) {
            if($_GET['zone_id'] == $zone->zone_id){
                $zone_names =  $zone->zone_name_th;
            }
        }
    }
    ?>
    <?php
      if(!empty($_GET['zone_id'])){
         $title_home = "โครงการบ้านตกแต่งพร้อมขาย : ".$zone_names;
      }else{
        $title_home = "โครงการบ้านตกแต่งพร้อมขายทั้งหมดของ LH";
      }
    ?>

    <div id="content" class="content furnishedhome-page">
        <div class="container">
            <h1 style="cursor: pointer;display: block;" onclick="location.href='<?= $router->generate('furnished-home-list') ?>'"><?=$title_home?></h1>
            <div class="furnishedhome-block">
                <h2 style="color: #4d4c4d;">ขอแนะนำบ้านพร้อมตกแต่งพร้อมขาย <?= $countProjectZone ?> รายการ</h2>
                <?php if($home_sells != false) { ?>
                <div class="sort-container">
                    <div class="sort-block">
                        <input type="hidden" name="sortSelect" value="">
                        <div class="sort" data-status="0">
                            <span>
                                <?php
                                $zones = getHomeSellZones();
                                if(!empty($_GET['zone_id'])){
                                    foreach($zones as $zone) {
                                        if($_GET['zone_id'] == $zone->zone_id){
                                            echo $zone->zone_name_th;
                                        }
                                    }
                                }else{
                                    echo 'เลือกทำเลที่สนใจทั้งหมด';
                                }
                                ?>
                            </span> <i class="i-sort"></i>
                        </div>
                        <ul id="sortLists">
                            <li data-value="เลือกทำเลที่สนใจทั้งหมด" onclick="location.href='<?= $router->generate('furnished-home-list') ?>'">
                                เลือกทำเลที่สนใจทั้งหมด
                            </li>
                            <?php
                            foreach($zones as $zone) {?>
                                <li data-value="<?= $zone->zone_name_th ?>" onclick="location.href='<?= $router->generate('furnished-home-list-by-zone',['zone_name' => str_replace(' ','-',$zone->zone_name_th)]) ?>'">
                                    <?= $zone->zone_name_th ?>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="view-block">
                        <ul>
                            <li><a href="" class="i-view-list active"></a></li>
                            <li><a href="" class="i-view-grid"></a></li>
                            <li><a href="" class="i-view-grid-more"></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                </div>

                <input type="hidden" id="status_list" value="list">
                <?php } ?>
            </div>
            <div class="project-type-block">

                <div id="products" class="list-group">

                    <?php
                    if($home_sells != false){
                        foreach ($home_sells as $i => $home_sell){
                            $project_sub = getProjectSubBySubID($home_sell->project_sub_id);

                            if(!empty($project_sub)) {

                                $project = getProjectByID($project_sub->project_id);
                                $banner = getBannerByProjectID($project_sub->project_id);
                                $brand = is_object($project) ? getBrandByID($project->brand_id) : '';
                                $home_sell_imgs = getHomeSellImgByHomeSellID($home_sell->home_sell_id);
                                $lhPlan = getPlanByPlanID($home_sell->plan_id);
                                $conditionHomeSell = getConditionHomeSellByHomeSellID($home_sell->home_sell_id);
                                $project_concept = getProjectConcept($project_sub->project_id);
                                $logo = getProjectMaps($project_sub->project_id);
                                $conceptOnly = getProjectConceptOnly($project_sub->project_id);

                                $plan = getPlanByPlanID($home_sell->plan_id);
                                $series = getHomeSeriesByID($plan->series_id);

                                $imgBanners = [];
                                foreach ($home_sell_imgs as $home_sell_img) {
                                    $buff['path'] = $home_sell_img->path_funished;
                                    $buff['seo'] = $home_sell_img->alt_seo;
                                    $imgBanners[] = $buff;
                                }

                                if (empty($_GET['zone_id']) || (!empty($_GET['zone_id']) && $project != false && $_GET['zone_id'] == $project->zone_id)) {
                                    ?>

                                    <div class="projecttype-list">
                                        <div class="row">
                                            <div class="col-md-8">
                                            <?php
                                                if($home_sell->home_sell_status == 'SO'){
                                            ?>
                                                    <div class="tagbox2 tagbox2_sold">
                                                        <div class="tag2">
                                                            SOLD
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                <div id="furnishhomeImg" class="furnishHome">
                                                    <?php
                                                    if (!empty($imgBanners)) {
                                                        foreach ($imgBanners as $index => $imgBanner) {
                                                            if ($index < 5) {
                                                                ?>
                                                                <div class="item">
                                                                    <?php
                                                                    $url_see_more = $router->generate('furnished-home-detail', [
                                                                        'plan_name' => str_replace(' ', '-', $lhPlan->plan_name_th),
                                                                        'home_sell_id' => $home_sell->home_sell_id
                                                                    ]);
                                                                    ?>
                                                                    <a href="<?= $url_see_more ?>">
                                                                    <img src="<?= backend_url('base', $imgBanner['path']) ?>"
                                                                         alt="<?= $imgBanner['seo'] ?>">
                                                                    </a>
                                                                </div>
                                                                <?php
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="projecttype-descrp">
                                                    <?php if (!empty($series->series_logo)) {
                                                        ?>
                                                        <img src="<?= is_object($series) ? backend_url('series_logo', $series->series_logo) : '' ?>"
                                                             title="<?= $series->series_logo_seo ?>"
                                                             alt="<?= $series->series_logo_seo ?>">
                                                        <?php
                                                    } ?>
                                                    <p class="subtitle">โครงการ <?=$project->project_name_th;?></p>
                                                    <p class="subtitle"><strong>แบบบ้าน <?= $lhPlan->plan_name_th ?></strong></p>
                                                    <p class="subtitle">ลักษณะเด่นของแปลง<br>
                                                        <span class="feature_convert<?= $i ?>">
                                                        <?=!empty($home_sell->features_convert) ? str_replace(array("\n\r", "\n","n/g"), '<br/>', $home_sell->features_convert) : ''?>

                                                    </span>
                                                    </p>

                                                    <br>
                                                    <p class="subtitle price">
                                                        <?php if ($conditionHomeSell != false) {
                                                            ?>
                                                            <strong>
                                                                ราคาเริ่มต้น
                                                                <?= Helper::getProjectPrice($conditionHomeSell->total_price); ?>
                                                                <?= is_object($conditionHomeSell) ? Helper::getPriceModeName(1, $conditionHomeSell->total_price) : 'ล้านบาท' ?>
                                                            </strong>
                                                            <?php
                                                        } ?>
                                                    </p>
                                                    <a href="<?= $url_see_more ?>"
                                                       class="btn btn-seemore">See more</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                        }
                    }
                    ?>



                </div>
            </div>

        </div>
    </div>

<!-- Zendesk Chat  -->
<!-- Start of lhcoco Zendesk Widget script -->
<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=2be331a5-605a-4a1f-86dc-1eef96e43cf0"> </script>
<!-- End of lhcoco Zendesk Widget script -->


<?php include('footer.php'); ?>