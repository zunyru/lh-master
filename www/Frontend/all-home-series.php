<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>
<?php include('header.php'); ?>
    <!-- CSS -->
    <link rel="stylesheet" href="css/all-home-series.css" type="text/css">
    <link rel="stylesheet" href="bxslider/jquery.bxslider.css" type="text/css">
    <!-- JS -->
    <script src="bxslider/jquery.bxslider.min.js"></script>
    <script src="js/all-home-series.js"></script>

<!--    <div class="page-banner-md">-->
<!--        <div id="bannerSlide">-->
<!--            <div class="item">-->
<!--                <div class="banner-parallax" style="background-image: url(images/temp2/banner_all_home.jpg);">-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->

    <div id="content" class="content project-info-page">

        <div class="container">

            <div id="sec-home-model" class="block-margin">
                <p class="heading-title">บ้านพร้อมขาย</p>
                <div class="row">
                    <div id="projectModelSlide">
                        <div class="col-md-8">
                            <div id="projectSlide" class="owl-carousel owl-theme">
                                <div class="gal-img item"><img src="images/temp2/all_home_1.jpg"/></div>
                                <div class="gal-img item"><img src="images/temp2/all_home_1.jpg"/></div>
                                <div class="gal-img item"><img src="images/temp2/all_home_1.jpg"/></div>
                                <div class="gal-img item"><img src="images/temp2/all_home_1.jpg"/></div>
                                <div class="gal-img item"><img src="images/temp2/all_home_1.jpg"/></div>
                                <div class="gal-img item"><img src="images/temp2/all_home_1.jpg"/></div>
                            </div>

                            <div id="imgThumb" class="slide-thumb owl-thumbs owl-carousel owl-theme">
                                <a class="item"><img src="images/temp2/all_home_1.jpg" alt=""></a>
                                <a class="item"><img src="images/temp2/all_home_1.jpg" alt=""></a>
                                <a class="item"><img src="images/temp2/all_home_1.jpg" alt=""></a>
                                <a class="item"><img src="images/temp2/all_home_1.jpg" alt=""></a>
                                <a class="item"><img src="images/temp2/all_home_1.jpg" alt=""></a>
                                <a class="item"><img src="images/temp2/all_home_1.jpg" alt=""></a>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="promo-descrp" id="desc_home">
                                <p class="title green">แบบบ้าน WALNUT</p>
                                <p class="title">หมายเลขแปลง <span class="green">01U02</span></p>
                                <p class="title">ลักษณะแปลงที่ดิน</p>
                                <p>ลักษณะเด่นของแปลง ราคาที่สุดพิเศษกับทำเลส่วนตัว<br>
                                    private zone</p>
                                <p class="title green">ราคา 5.49 ล้านบาท</p>

                                <a href="" class="btn btn-seemore">See more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="sec-home-sale" class="other-home-sec">
                <p class="heading-title">บ้านพร้อมขายทั้งหมด <span id="index_plan">1/5 แปลง</span></p>
                <input type="hidden" id="amount_value" value="5">
                <div class="row">
                    <div id="otherHome" class="col-slide">
                        <div class="item">
                            <div class="">
                                <div class="other-home-img">
                                    <img src="images/temp2/all_home_2.jpg" alt="">
                                </div>
                                <a>
                                    <p class="title green">แบบบ้าน CEDAR A</p>
                                    <p>หมายเลขแปลง 01I07</p>
                                    <p class="price green">5.49 ล้านบาท</p>
                                    <div id="data-slider" style="display: none">
                                        <input type="hidden" id="index_value" value="1">
                                        <input type="hidden" id="title_home" value="แบบบ้าน CEDAR A">
                                        <input type="hidden" id="format_home" value="แบบบ้าน">
                                        <input type="hidden" id="id_home" value="01I07">
                                        <input type="hidden" id="identity_home" value="บ้านทรง โมเดิร์น">
                                        <input type="hidden" id="price_home" value="6.88 ล้านบาท">
                                        <div id="img_list">
                                            <input type="hidden" class="img_url" value="images/temp2/all_home_2.jpg">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="">
                                <div class="other-home-img">
                                    <img src="images/temp2/all_home_3.jpg" alt="">
                                </div>
                                <a>
                                    <p class="title green">แบบบ้าน WALNUT</p>
                                    <p>หมายเลขแปลง 01U07</p>
                                    <p class="price green">5.49 ล้านบาท</p>
                                    <div id="data-slider" style="display: none">
                                        <input type="hidden" id="index_value" value="2">
                                        <input type="hidden" id="title_home" value="แบบบ้าน WALNUT">
                                        <input type="hidden" id="format_home" value="WALNUT MODERN">
                                        <input type="hidden" id="id_home" value="01U07">
                                        <input type="hidden" id="identity_home" value="บ้านทรง โมเดิร์น">
                                        <input type="hidden" id="price_home" value="ราคา 7.99 ล้านบาท">
                                        <div id="img_list">
                                            <input type="hidden" class="img_url" value="images/temp2/all_home_2.jpg">
                                            <input type="hidden" class="img_url" value="images/temp2/all_home_3.jpg">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="">
                                <div class="other-home-img">
                                    <img src="images/temp2/all_home_2.jpg" alt="">
                                </div>
                                <a>
                                    <p class="title green">แบบบ้าน CEDAR B</p>
                                    <p>หมายเลขแปลง 01H07</p>
                                    <p class="price green">5.49 ล้านบาท</p>
                                    <div id="data-slider" style="display: none">
                                        <input type="hidden" id="index_value" value="3">
                                        <input type="hidden" id="title_home" value="แบบบ้าน CEDAR B">
                                        <input type="hidden" id="format_home" value="CEDAR B">
                                        <input type="hidden" id="id_home" value="01H07">
                                        <input type="hidden" id="identity_home" value="Loof Of Title">
                                        <input type="hidden" id="price_home" value="ราคา 7.98 ล้านบาท">
                                        <div id="img_list">
                                            <input type="hidden" class="img_url" value="images/temp2/all_home_2.jpg">
                                            <input type="hidden" class="img_url" value="images/temp2/all_home_3.jpg">
                                            <input type="hidden" class="img_url" value="images/temp2/all_home_1.jpg">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="">
                                <div class="other-home-img">
                                    <img src="images/temp2/all_home_3.jpg" alt="">
                                </div>
                                <a>
                                    <p class="title green">แบบบ้าน CEDAR C</p>
                                    <p>หมายเลขแปลง 01R07</p>
                                    <p class="price green">5.49 ล้านบาท</p>
                                    <div id="data-slider" style="display: none">
                                        <input type="hidden" id="index_value" value="4">
                                        <input type="hidden" id="title_home" value="แบบบ้าน CEDAR C">
                                        <input type="hidden" id="format_home" value="CEDAR C Custom">
                                        <input type="hidden" id="id_home" value="01I07">
                                        <input type="hidden" id="identity_home" value="CEDAR C Custom Private">
                                        <input type="hidden" id="price_home" value="ราคา 16.88 ล้านบาท">
                                        <div id="img_list">
                                            <input type="hidden" class="img_url" value="images/temp2/all_home_3.jpg">
                                            <input type="hidden" class="img_url" value="images/temp2/all_home_1.jpg">
                                            <input type="hidden" class="img_url" value="images/temp2/all_home_2.jpg">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="">
                                <div class="other-home-img">
                                    <img src="images/temp2/all_home_3.jpg" alt="">
                                </div>
                                <a>
                                    <p class="title green">แบบบ้าน CEDAR C</p>
                                    <p>หมายเลขแปลง 01R07</p>
                                    <p class="price green">5.49 ล้านบาท</p>
                                    <div id="data-slider" style="display: none">
                                        <input type="hidden" id="index_value" value="5">
                                        <input type="hidden" id="title_home" value="แบบบ้าน CEDAR C">
                                        <input type="hidden" id="format_home" value="CEDAR C Custom">
                                        <input type="hidden" id="id_home" value="01I07">
                                        <input type="hidden" id="identity_home" value="CEDAR C Custom Private">
                                        <input type="hidden" id="price_home" value="ราคา 16.88 ล้านบาท">
                                        <div id="img_list">
                                            <input type="hidden" class="img_url" value="images/temp2/all_home_1.jpg">
                                            <input type="hidden" class="img_url" value="images/temp2/all_home_2.jpg">
                                            <input type="hidden" class="img_url" value="images/temp2/all_home_3.jpg">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

<?php include('footer.php'); ?>