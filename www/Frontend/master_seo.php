<?php 
if($lang == 'en'){ 
	$title = $url_web->title_seo_en;
	$description = $url_web->description_seo_en;
	$keywords = $url_web->keyword_seo_en;
}else{

	$title = $url_web->title_seo;
	$description = $url_web->description_seo;
	$keywords = $url_web->keyword_seo;
	
} 
$url         = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$url_cut     = (substr($url, 0, strpos($url, "?"))==''?$url:substr($url, 0, strpos($url, "?")));
$image = backend_url('base', $url_web->thumnail_seo) ;

?>
<!-- for Google Master SEO-->
<title><?=htmlspecialchars($title);?></title>
<meta name     ="description"         content="<?=htmlspecialchars($description);?>" />
<meta name     ="keywords"            content="<?=htmlspecialchars($keywords);?>" />

<!-- for Facebook Master SEO -->
<meta property ="fb:app_id"           content="<?= $app_id;?>" /> 
<meta property ="og:type"             content="website" /> 
<meta property ="og:url"              content="<?= $url_cut;?>" /> 
<meta property ="og:title"            content="<?= $title;?>" />
<meta property ="og:description"      content="<?= $description; ?>" />
<meta property ="og:site_name"        content="<?= $title;?>" /> 
<meta property ="og:image"            content="<?= $image; ?>" />
<meta property ="og:image:type"       content="image/jpeg" />

<meta property="og:image:width"       content="600" />
<!-- <meta property="og:image:height"      content="315" /> -->
<meta property="og:image:alt"         content="<?=$title;?>" /> 

<!-- for Twitter Master SEO-->
<meta name="twitter:card"           content="summary_large_image" />
<meta name="twitter:title"          content="<?= $title;?>" />
<meta name="twitter:description"    content="<?= $description; ?>" />
<meta name="twitter:creator"        content="@lhhome">
<meta name="twitter:site"           content="<?= $url;?>">
<meta name="twitter:image"          content="<?= $image; ?>" />
<meta itemprop="image"              content="<?= $image ?>" /> 