<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>

<?php include('header.php'); ?>
    <!-- CSS -->
    <link rel="stylesheet" href="css/service.css" type="text/css">
    <!-- JS -->

    <div id="content" class="content service-page">
        <div class="container">
            <div class="row">
                <div class="col-md-11 col-md-offset-1">
                    <h1 class="heading-title">ข้อมูลส่วนตัว</h1>

                    <div class="customer-info">
                        <p class="service-title">ข้อมูลลูกค้า</p>

                        <div class="row">
                            <div class="col-md-6">
                                <p><span class="service-label">ชื่อ-สกุล ลูกค้า</span> : นายธนะดิฐ บุญประสิทธิ์</p>
                                <p><span class="service-label">Email</span> : tana456789@gmail.com</p>
                            </div>
                            <div class="col-md-6">
                                <p><span class="service-label txt-long">โทรศัพท์ติดต่อ (บ้าน)</span> : 02-890-6090</p>
                                <p><span class="service-label txt-long">โทรศัพท์ติดต่อ (มือถือ)</span> : 085-099-6566</p>
                            </div>
                        </div>
                    </div>

                    <div class="customer-info edit-profile">
                        <p class="service-title">เปลี่ยนแปลง/แก้ไข (กรุณากรอกรายละเอียด)</p>

                        <div class="row">
                            <div class="col-md-6">
                                <p><span class="service-label">ชื่อ-สกุล ลูกค้า</span> : <input type="text" name="" class="form-control"></p>
                                <p><span class="service-label">Email</span> : <input type="text" name="" class="form-control"></p>
                            </div>
                            <div class="col-md-6">
                                <p><span class="service-label txt-long">โทรศัพท์ติดต่อ (บ้าน)</span> : <input type="text" name="" class="form-control"></p>
                                <p><span class="service-label txt-long">โทรศัพท์ติดต่อ (มือถือ)</span> : <input type="text" name="" class="form-control"></p>
                                * สำหรับส่ง SMS ตอบกลับ
                            </div>
                        </div>

                        <button type="submit" class="btn btn-submit">ยืนยัน</button>

                    </div>

                </div>
            </div>


        </div>
    </div>


<?php include('footer.php'); ?>
