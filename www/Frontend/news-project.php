<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';
require_once 'include/help/hex2rgba.php';

$new_projects               = getNewProject();
$page = 'newproject';
$page_index = 4;

?>
<?php
include('header.php');
//elementMetaTitleDisTag($page,$page_index);
?>
<!-- CSS -->
<link rel="stylesheet" href="<?= file_path('css/news-project.css') ?>" type="text/css">
<link rel="stylesheet" href="<?= file_path('css/video.css') ?>" type="text/css">
<!-- JS -->

<script type="text/javascript" src="<?= file_path('js/news-project.js?1500') ?>"></script>

<div class="page-banner page-scroll">
    <span id="scrollNext" class="i-scroll-next"></span>

    <div id="bannerSlide">

        <?php
        renderBannerPageList(4);
        ?>

    </div>
</div>

<?php
renderHiddenLeadImage(4,'bannerSlide');
renderActivityResponsive();
?>

<div id="content" class="content news-project-page">
    <div class="container">
        <!-- <h1>ข่าวโครงการใหม่</h1> -->
        <?php 
        $SEO = getSEOUrl($actual_link);
        if($actual_link == @$SEO->url_page){
            echo "<h1>".$SEO->h1."</h1>";
        }else{
            echo "<h1>ข่าวโครงการใหม่</h1>";  
        }
        ?>
        <p class="title">พบข่าวโครงการใหม่ <?= count($new_projects) ?> รายการ</p>

        <?php

        if($new_projects != false){
            foreach ($new_projects as $i => $project_buff){
                $project    = getProjectByID($project_buff->project_id);
                $project_id = $project->project_id;
                $banner  = getBannerByProjectID($project->project_id);
                $price   = getProjectPrice($project_id);
                $brand   = getBrandByID($project->brand_id);

                $logo    = getProjectMaps($project_id);

                $project_concept = getProjectConcept($project_id);

                $project_sub = getProjectSub($project_id,1);
                $product_id  = 1;
                if($project_sub == false){
                    $project_sub = getProjectSub($project_id,2);
                    $product_id  = 2;
                }
                if($project_sub == false){
                    $project_sub = getProjectSub($project_id,3);
                    $product_id  = 3;
                }

                
                if($product_id == 3){
                    $url_see_more = isLadawan($project->project_id) ?
                    $router->generate('ladawan-detail',[
                        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                        'product_id'    =>  3
                    ]) :
                    $router->generate('condominium-detail',[
                        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                        'lang'          =>  'th'
                    ]);
                }
                else if($product_id == 2){
                 
                    $url_see_more = isLadawan($project->project_id) ?
                    $router->generate('ladawan-detail',[
                        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                        'product_id'    =>  2
                    ]) :
                    $router->generate('town-home-detail',[
                        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                        'product_id'    =>  2
                    ]);

                    
                }else{
                    $url_see_more = isLadawan($project->project_id) ?
                    $router->generate('ladawan-detail',[
                        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                        'product_id'    =>  1
                    ]) :
                    $router->generate('single-home-detail',[
                        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                        'product_id'    =>  1
                    ]);

                }

                if($i < 100){
                    ?>
                    <div class="news-block">
                        <div class="for-desktop">
                            <div class="row">
                                <div class="col-md-5 col-md-offset-1">
                                    <div class="">
                                        <img src="<?= is_object($logo) ? backend_url('base', $logo->project_logo) : '' ?>" alt="" class="logo-project">

                                        <!--                            <h3>บ้านมัณฑนา ราชพฤกษ์-สะพานมหาเจษฎาบดินทร์ฯ</h3>-->
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="txt-center">
                                        <p class="concept<?= $i ?>">
                                           <?=!empty($project_concept->concept_th) ? str_replace(array("\n\r", "\n","n/g"), '<br/>', $project_concept->concept_th) : ''?>

                                       </p>
                                       <p>
                                        <?php
                                        $project_price = getProjectPrice($project_id);
                                        if($project_price->price_mode == '1'){
                                         echo $mode_price = 'ราคาเริ่มต้น ';
                                     }else{
                                         echo $mode_price = 'ราคา ';
                                     }
                                     if($project_price != false)
                                        echo Helper::getProjectPrice($project_price->project_price,$project_price->price_mode)
                                    ?>
                                    <?= is_object($project_price) ? Helper::getPriceModeName($project_price->price_mode,$project_price->project_price) : 'ล้านบาท' ?>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6 col-md-offset-5">
                            <a href="<?= $url_see_more ?>" class="btn btn-seemore">See more</a>
                            <?php
                            $project->project_url_register = str_replace(' ','',$project->project_url_register);
                            if (!empty($project->project_url_register)) {
                                ?>
                                <a href="<?= $project->project_url_register ?>" class="btn btn-seemore">Register</a>
                                <?php
                            }?>
                        </div>

                        <div class="clear"></div>
                    </div>
                    <div class="news-images-block tpl5-img">
                        <div class="row">
                            <div id="wall-<?= $i+1 ?>" class="tpl5-imgg tpl5-img-slide wall-free">

                                <?php

                                        //$gallerys = getLHGalleryImgProject($project->project_id);
                                $gallerys = getAllBannerByProjectID_Newproject($project->project_id);

                                if($gallerys != false){
                                    foreach ($gallerys as $index => $gallery){
                                        ?>
                                        <div class="item tpl5-img-<?= $index+1 ?>">
                                            <a class="gallery" href="<?= backend_url('base',$gallery->LEAD_IMAGE_PROJECT_FILE_NAME/*$gallery->galery_project_img_name*/) ?>">
                                                <img src="<?= backend_url('base',$gallery->LEAD_IMAGE_PROJECT_FILE_NAME) ?>" alt="">
                                                <!-- <p class="title-gallery"><?php //echo $gallery->galery_project_img_desc ?></p> -->
                                            </a>
                                        </div>
                                        <?php
                                    }
                                }

                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="for-mobile">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="left">
                                <img src="<?= backend_url('base',$logo->project_logo) ?>" alt="" class="logo-project">
                                <!--                            <h3>บ้านมัณฑนา ราชพฤกษ์-สะพานมหาเจษฎาบดินทร์ฯ</h3>-->
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="news-images-block tpl5-img">
                        <div class="row">
                            <div id="wall-<?= $i+1 ?>" class="tpl5-imgg tpl5-img-slide wall-free">

                                <?php

                                $gallerys = getAllBannerByProjectID_Newproject($project->project_id);

                                if($gallerys != false){
                                    foreach ($gallerys as $index => $gallery){
                                        ?>
                                        <div class="item tpl5-img-<?= $index+1 ?>">
                                            <a class="gallery" href="<?= backend_url('base',$gallery->LEAD_IMAGE_PROJECT_FILE_NAME) ?>">
                                                <img src="<?= backend_url('base',$gallery->LEAD_IMAGE_PROJECT_FILE_NAME) ?>" alt="">
                                                <!--                                                        <p class="title-gallery">หน้าโครงการ</p>-->
                                            </a>
                                        </div>
                                        <?php
                                    }
                                }

                                ?>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="txt-center">

                                <p class="concept<?= $i ?> concept-in-list">
                                   <?=!empty($project_concept->concept_th) ? str_replace(array("\n\r", "\n","n/g"), '<br/>', $project_concept->concept_th) : ''?>
                                   
                               </p>
                               <p>เริ่ม
                                <?php
                                $project_price = getProjectPrice($project_id);
                                if($project_price != false)
                                    echo Helper::getProjectPrice($project_price->project_price,$project_price->price_mode)
                                ?>
                            ล้านบาท</p>
                        </div>
                        <a href="<?= $url_see_more ?>" class="btn btn-seemore">See more</a>
                        <?php
                        $project->project_url_register = str_replace(' ','',$project->project_url_register);
                        if (!empty($project->project_url_register)) {
                            ?>
                            <a href="<?= $project->project_url_register ?>" class="btn btn-seemore">Register</a>
                            <?php
                        }?>
                    </div>

                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <?php
    }
}
}
?>

</div>
</div>

<!-- Zendesk Chat  -->
<!-- Start of lhcoco Zendesk Widget script -->
<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=2be331a5-605a-4a1f-86dc-1eef96e43cf0"> </script>
<!-- End of lhcoco Zendesk Widget script -->

<?php include('footer.php'); ?>