
<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';
$page = 'job';
$page_index = 0;
$img = '';
?>
<?php
include('header.php');
//elementMetaTitleDisTag($page);
?>

<!-- CSS -->
<link rel="stylesheet" href="<?= file_path('js/cal_reg/css/job.css') ?>" type="text/css">
<link rel="stylesheet" href="<?= file_path('css/job-news.css') ?>" type="text/css">
<link rel="stylesheet" href="<?= file_path('js/cal_reg/css/sweetalert.css') ?>">

<!-- JS -->
<script src="<?= file_path('js/lib/angular.min.js') ?>"></script>
<script src="<?= file_path('js/lib/angular-sanitize.js') ?>"></script>
<script src="<?= file_path('js/cal_reg/appjob.js') ?>"></script>
<script src="<?= file_path('js/cal_reg/job.js') ?>"></script>
<script src="<?= file_path('js/cal_reg/sweetalert.min.js') ?>"></script>
<script src="<?= file_path('js/cal_reg/jquery.mask.min.js') ?>"></script>
<script src="<?= file_path('js/cal_reg/printThis.js') ?>"></script>


<style>
.select-test select {
    background: transparent;
    border: none;
    width: 300px;
}

.select-test {
    background: #fff url(<?= file_path('images/news/i_sort_blk.png')?>) no-repeat 95% center;
    cursor: pointer;
    font-size: 22px;
    padding: 8px 15px;
    position: relative;
    width: 300px;
    height: 42px;
    border-radius: 6px;
    border: 1px solid #dedede;
}
.prehead{
    background: #F5F5F5;
}
.f-required{
    color: red;
}
</style>

<jobFName ng-app="JobApp"  ng-controller="myController">
    <div class="page-banner-md banner-hide">
        <div id="bannerSlide">
            <div class="item">
                <div class="banner-parallax">
                    <img src="<?= file_path('images/temp3/banner_job.jpg') ?>" alt="">
                </div>
            </div>
        </div>
    </div>

    <div id="content" class="content rps-content-pd">
        <div class="container">
         <?php
         $SEO = getSEOUrl($actual_link);
         if($actual_link == @$SEO->url_page){
            echo '<h1>'.$SEO->h1.'</h1>';
        }
        ?>
        <div class="row">
            <div class="col-md-12">
                <div>
                    <h1 class="heading-title" style="text-align: center;display: none;">ร่วมงานกับเรา</h1>

                    <div id="tabs-container" class="tabs-container">
                        <div class="tab-dd dropdown">
                            <ul class="tabs-menu">
                                <li class="item current">
                                    <a href="#tab-1">
                                        ตำแหน่งงานที่เปิดรับสมัคร
                                    </a>
                                </li>
                                <li class="item">
                                    <a href="#tab-2">
                                        สวัสดิการที่คุณจะได้รับ
                                    </a>
                                </li>
                                <li class="item">
                                    <a href="#tab-3">
                                        ประสบการณ์ที่คุณจะได้จากเรา
                                    </a>
                                </li>
                                <li class="item">
                                    <a href="#tab-4">
                                        ข่าวสารสมัครงาน
                                    </a>
                                </li>
                                <li class="item">
                                    <a href="" onclick="location.href='<?php url('/contact-us'); ?>';">
                                        ติดต่อเรา
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="select-container" id="choose-location">
                            <div class="select-block selectProvince">
                                <input type="hidden" name="selectProvince" value="">
                                <div class="select-test">
                                    <select   ng-model="filter.provinces"  ng-change="filter.area=undefined" >
                                        <option value="{{undefined}}"  ng-selected="true" >&nbsp;&nbsp;เลือกดูตามจังหวัด</option>
                                        <option ng-repeat="dataArr in provinces" value="{{dataArr.PROVINCE_ID}}" >&nbsp;&nbsp; {{ dataArr.PROVINCE_NAME }}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="select-block selectArea">
                                <input type="hidden" name="selectArea" value="">
                                <div class="select-test">
                                    <select  ng-model="filter.area" >
                                        <option value="{{undefined}}" ng-selected="true" >&nbsp;&nbsp;เลือกดูตามพื้นที่ทำงาน</option>
                                        <option  ng-repeat="dataArr in area | filter:{PROVINCE_ID: filter.provinces} " ng-if="filter.provinces == dataArr.PROVINCE_ID" value="{{dataArr.LOCATION_ID}}" >&nbsp;&nbsp;{{ dataArr.LOCATION_NAME }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>




                        <div class="tab">
                            <div id="tab-1" class="tab-content">
                                <div class="tab-block">
                                    <div class="form current" id="table">

                                        <div class="message-lable message-lable-mobile">
                                            <label style="color: red">สมัครงานสอบถามเพิ่มเติ่ม โทร. 0893679279</label>
                                            <br>
                                            <label style="color: red">ในวันและเวลาทำการ จันทร์-ศุกร์ 08.30 - 17.30 น.</label>
                                        </div>

                                        <table class="project-search-list" width="100%">
                                            <thead>
                                                <tr>
                                                    <th style="width: 10%;">
                                                        <span class="hide-rps">วันที่ประกาศ</span>
                                                        <span class="hide-dsktp">วันที่</span>
                                                    </th>
                                                    <th style="width: 25%;">
                                                        <span class="hide-rps">ตำแหน่งงานว่าง</span>
                                                        <span class="hide-dsktp">ตำแหน่ง</span>
                                                    </th>
                                                    <th style="width: 20%;" class="txt-center">
                                                     <span  class="hide-dsktp ">จังหวัด</span>
                                                     <span  class="hide-rps " >จังหวัด</span>
                                                 </th>
                                                 <th style="width: 13%;">
                                                    <span class="hide-rps">พื้นที่ทำงาน</span>
                                                    <span class="hide-dsktp">พื้นที่ทำงาน</span>
                                                </th>
                                                <th style="width: 15%;">
                                                    <span class="hide-rps">อัตราที่เปิดรับ</span>
                                                    <span class="hide-dsktp">อัตราที่เปิดรับ</span>
                                                </th>
                                                <th style="width: 10%;">
                                                    <span class="hide-rps">สมัครงานตำแหน่งนี้</span>
                                                    <span class="hide-dsktp">สมัครงาน</span>
                                                </th>

                                            </tr>
                                        </thead>
                                        <tbody>

                                            <tr ng-if="(positions | filter:{'PROVINCE_ID': filter.provinces}).length <= 0 ">
                                             <td colspan="6">ไม่พบตำแหน่งงานว่างค่ะ</td>
                                         </tr>


                                         <tr ng-repeat="position in positions | filter: {'PROVINCE_ID': filter.provinces} | filter:{'LOCATION_ID': filter.area} ">

                                            <td ng-if="position.message != 'unsuccess' ">{{ dateFormat2(position.POSTED_DAT) }}</td>
                                            <td ng-if="position.message == 'unsuccess' ">ไม่มีตำแหน่งงานว่างในขณะนี้ค่ะ</td>
                                            <td><a data-toggle="modal" data-target="#modal-jd" href="<?=$router->generate('job-description');?>?id={{position.ID}}">{{ position.TITLE }}</a></td>
                                            <td class="txt-center">{{ position.PROVINCE_NAME }}</td>
                                            <td class="hide-rps">{{ position.LOCATION_NAME }}</td>
                                            <td class="txt-center">{{ position.RATES }}</td>
                                            <td class="txt-center" ng-if="position.message != 'unsuccess' "><a class="formJob"  ng-click="test(position.ID,position.TITLE)">สมัครงาน </a></td>
                                        </tr>

                                        <tr ng-if="(positions | filter:{'PROVINCE_ID': filter.provinces}).length >= 0 ">
                                         <td ng-if="(positions | filter:{'LOCATION_ID': filter.area}).length <= 0 " colspan="6">ไม่พบตำแหน่งงานว่างค่ะ</td>
                                     </tr>


                                 </tbody>
                             </table>

                             <a href=""></a>

                         </div>






                         <!-- ======================================= -->
                         <div class="job-form" tyle="display: none;">
                            <div class="apply-job-container">



                                <form action="" class="apply-job-form" name=jobform id="jobform" ng-submit="myFunc()"  novalidate>
                                    <div class="row">

                                        <ul id="progressbar">

                                            <li class="active">ข้อมูลส่วนตัว</li>
                                            <span  class="step_item1" ><img src="<?= file_path('images/global/icon_step_job.png') ?>" id="stepItem1"></span>
                                            <li>ข้อมูลการทำงาน</li>
                                            <span class="step_item2" ><img src="<?= file_path('images/global/icon_step_job.png') ?>" id="stepItem2"></span>
                                            <li>ข้อมูลอื่นๆ</li>
                                        </ul>

                                    </div>
                                    <fieldset>
                                        <div class="form-title">ข้อมูลส่วนตัว</div>
                                        <div class="personal_info form-section">
                                            <div class="row">
                                                <div class="col-md-6 form-row">
                                                   <div class="col-md-12 col-sm-3"><label>ชื่อ<span class="f-required"> *</span></label></div>
                                                   <div class="col-md-4">
                                                    <div class="select-block">
                                                     <select name="cmbTitle" id="cmbTitle" ng-model=data.jobcmbTitle required>
                                                        <option value="" disabled>คำนำหน้าชื่อ</option>
                                                        <option value="นาย">นาย</option>
                                                        <option value="นาง">นาง</option>
                                                        <option value="นางสาว">นางสาว</option>
                                                    </select>
                                                </div>
                                                <span ng-show="!jobform.cmbTitle.$pristine && jobform.cmbTitle.$error.required" style="color : red">โปรดใส่คำนำหน้าชื่อ</span>
                                            </div><br>

                                            <div class="col-md-8">
                                                <input type="text" class="textbox" name="txbFName" id="txbFName" ng-model="data.jobtxbFName"value="" maxlength="50" required>
                                                <span id="spfn" ng-show="!jobform.txbFName.$pristine && jobform.txbFName.$error.required" style="color : red" >
                                                โปรดกรอกชื่อจริง</span>
                                            </div>

                                        </div>
                                        <div class="col-md-6 form-row">
                                            <div class="col-md-12"><label>นามสกุล<span class="f-required"> *</span></label></div>
                                            <div class="col-md-12">
                                                <input type="text" class="textbox" name="txbLName" id="txbLName" ng-model="data.jobtxbLName" value="" maxlength="50" required>
                                                <span id="spln" ng-show="!jobform.txbLName.$pristine && jobform.txbLName.$error.required" style="color : red" >
                                                โปรดกรอกนามสกุล</span>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 form-row">
                                            <div class="col-md-12"><label>ชื่อเล่น<span class="f-required"> *</span></label></div>
                                            <div class="col-md-12">
                                                <input type="text" class="textbox w100-m" name="txbNickname" id="txbNickname" ng-model="data.jobtxbNickname" maxlength="30" value="" required>
                                                <span id="spnn" ng-show="!jobform.txbNickname.$pristine && jobform.txbNickname.$error.required" style="color : red" >โปรดกรอกชื่อเล่น</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 form-row">
                                            <div class="col-md-12"><label>เพศ<span class="f-required"> *</span></label></div>
                                            <div class="col-md-8">
                                                <input type="radio" name="rdoGender" id="rdoGenderM" ng-model="data.jobrdoGender"
                                                value="M" required> <label for="rdoGenderM"><span></span>ชาย</label> &nbsp; &nbsp;
                                                &nbsp; &nbsp;
                                                <input type="radio" name="rdoGender" id="rdoGenderF" ng-model="data.jobrdoGender"
                                                value="F" required> <label for="rdoGenderF"><span></span>หญิง</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                      <div class="col-md-6 form-row">
                                        <div class="col-md-12"><label>วันเกิด<span class="f-required"> *</span></label></div>
                                        <div class="col-md-12 form-group has-feedback">
                                            <input type="text" name="dateBirth" id="dateBirth" ng-model="data.jobdateBirth" data-date-format="mm/dd/yyyy"
                                            class="w100-m hasDatepicker" readonly value="" required>
                                            <span class="glyphicon glyphicon-calendar form-control-feedback" id="iconhbd" ></span>
                                        </div>

                                    </div>

                                    <div class="col-md-6 form-row">
                                        <div class="col-md-12"><label>ภูมิลำเนา<span class="f-required"> *</span></label></div>
                                        <div class="col-md-12">
                                            <div class="select-block">
                                                <select name="cmbBirthProvince" id="cmbBirthProvince" ng-model="data.jobcmbBirthProvince"  class="w100-m" required>
                                                    <option value="" disabled>จังหวัด</option>
                                                    <option value="{{ province.PROVINCE_ID }}"
                                                    ng-repeat="province in provinces">{{ province.PROVINCE_NAME }}</option>
                                                </select>
                                            </div>
                                            <span id="sppro" ng-show="!jobform.cmbBirthProvince.$pristine && jobform.cmbBirthProvince.$error.required" style="color : red" > โปรดเลือกภูมิลำเนา </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 form-row">
                                        <div class="col-md-12"><label>ส่วนสูง<span class="f-required"> * </span>(เซนติเมตร) </label></div>
                                        <div class="col-md-12">
                                            <input type="number" class="textbox" name="txbHeight" id="txbHeight" ng-model="data.jobtxbHeight"
                                            value="" min="0" required>
                                            <span id="sphei" ng-show="!jobform.txbHeight.$pristine && jobform.txbHeight.$error.required" style="color : red" >
                                            โปรดกรอกส่วนสูง </span>
                                        </div>
                                    </div>

                                    <div class="col-md-6 form-row">
                                        <div class="col-md-12"><label>น้ำหนัก<span class="f-required"> * </span>(กิโลกรัม) </label>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="number" class="textbox" name="txbWeight" id="txbWeight" min="0" ng-model="data.jobtxbWeight" required>
                                            <span id="spwei" ng-show="!jobform.txbWeight.$pristine && jobform.txbWeight.$error.required"
                                            style="color : red" >
                                        โปรดกรอกน้ำหนัก </span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>ศาสนา<span class="f-required"> *</span></label></div>
                                    <div class="col-md-12">
                                        <div class="select-block">
                                            <select name="cmbReligion" id="cmbReligion" ng-model="data.jobcmbReligion" required>
                                                <option value="" disabled>ศาสนา</option>
                                                <option value="พุทธ">พุทธ</option>
                                                <option value="คริสต์">คริสต์</option>
                                                <option value="อิสลาม">อิสลาม</option>
                                                <option value="อื่นๆ">อื่น ๆ</option>
                                            </select>
                                        </div>
                                        <span ng-show="!jobform.cmbReligion.$pristine && jobform.cmbReligion.$error.required" style="color : red" >โปรดเลือกศาสนา </span>
                                    </div>
                                </div>
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"> <label> อื่น ๆ โปรดระบุ </label></div>
                                    <div class="col-md-12">
                                        <input type="text" class="textbox" name="txbReligionOther"
                                        id="txbReligionOther" maxlength="30"  ng-disabled="data.jobcmbReligion!='อื่นๆ'"
                                        ng-model = "data.txbReligionOther">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>เชื้อชาติ</label></div>
                                    <div class="col-md-2">
                                        <input type="radio" name="rdoNationality" id="rdoNationalityT" ng-model="data.jobrdoNationality" value="ไทย" checked=""> <label for="rdoNationalityT"><span></span>ไทย</label>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="radio" name="rdoNationality" id="rdoNationalityO" ng-model="data.jobrdoNationality" value="อื่นๆ"><label for="rdoNationalityO" class="txtOther">
                                            <span></span>อื่นๆ ระบุ</label>
                                        </div>
                                        <div class="col-md-7">
                                            <input type="text" class="textbox" name="txbNationalityOther" ng-disabled="data.jobrdoNationality!='อื่นๆ'" id="txbNationalityOther" maxlength="30"  ng-model="data.txbNationalityOther">
                                        </div>
                                    </div>

                                    <div class="col-md-6 form-row">
                                        <div class="col-md-12"><label>สัญชาติ</label></div>
                                        <div class="col-md-2">
                                            <input type="radio" name="rdoRace" id="rdoRaceT" value="ไทย" ng-model="data.jobrdoRace"
                                            > <label for="rdoRaceT"><span></span>ไทย</label>
                                        </div>
                                        <div class="col-md-3">
                                            <input type="radio" name="rdoRace" id="rdoRaceO" value="อื่นๆ" ng-model="data.jobrdoRace">  <label for="rdoRaceO" class="txtOther"><span></span>อื่นๆ ระบุ</label>
                                        </div>
                                        <div class="col-md-7">
                                           <input type="text" class="textbox" name="txbRaceOther" id="txbRaceOther"
                                           maxlength="30" ng-model="data.txbRaceOther" value="f" ng-disabled="data.jobrdoRace!='อื่นๆ'">
                                       </div>
                                   </div>
                               </div>

                               <div class="row">
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>เกณฑ์ทหาร <span class="f-required"> *</span></label></div>
                                    <div class="col-md-12">
                                        <div class="select-block">
                                            <select name="cmbConscription" id="cmbConscription"
                                            ng-model="data.jobcmbConscription">
                                            <option value="" disabled>เลือก</option>
                                            <option value="ผ่อนผัน">ผ่อนผัน</option>
                                            <option value="เกณฑ์แล้ว">เกณฑ์แล้ว</option>
                                            <option value="จบ ร.ด.">จบ ร.ด.</option>
                                            <option value="ยกเว้น">ยกเว้น</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>เลือกยกเว้น โปรดระบุเหตุผล </label></div>
                                <div class="col-md-12">
                                    <input type="text" class="textbox w100-m" name="txbConscription"
                                    id="txbConscription" maxlength="100" ng-disabled="data.jobcmbConscription!='ยกเว้น'"
                                    value="" ng-model="data.txbConscription" ></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label class="text-left">เลขบัตรประจำตัวประชาชน<span class="f-required"> *</span></label></div>
                                    <div class="col-md-12">
                                        <input type="text" class="textbox" name="txbIDNo" id="txbIDNo" ng-model="data.jobtxbIDNo"   required>
                                        <span ng-show="!jobform.txbIDNo.$pristine && jobform.txbIDNo.$error.required" style="color : red" > โปรดกรอกเลขบัตรประจำตัวประชาชน </span>
                                    </div>
                                    <div class="col-md-12" style="font-size: 19px;"> โปรดระบุตัวเลข 13 หลัก โดยไม่มีช่องว่าง  </div>
                                </div>
                            </div>



                        </div>
                        <div class="form-title">ข้อมูลครอบครัว</div>
                        <div class="family_info form-section">
                            <div class="row">
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12">
                                        <label>สถานภาพสมรส</label>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-6">
                                        <input type="radio" name="rdoMarried" id="rdoMarriedS"  ng-model="data.jobrdoMarried" value="โสด"> <label for="rdoMarriedS"><span></span>โสด</label>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-6">
                                        <input type="radio" name="rdoMarried" id="rdoMarriedM" ng-model="data.jobrdoMarried" value="สมรส"> <label for="rdoMarriedM"><span></span>สมรส</label>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-6">
                                        <input type="radio" name="rdoMarried" id="rdoMarriedD" ng-model="data.jobrdoMarried" value="หย่าร้าง"> <label for="rdoMarriedD"><span></span>หย่าร้าง</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row title">
                               <div class="col-md-12 form-row">
                                <div class="col-md-4"><label>ครอบครัว</label></div>
                            </div>
                        </div>

                        <div class="row hide-mobile">
                            <div class="col-md-12 form-row">
                                <div class="col-md-2"></div>
                                <div class="col-md-3">ชื่อ-นามสกุล</div>
                                <div class="col-md-1">อายุ</div>
                                <div class="col-md-3">อาชีพ/ตำแหน่ง</div>
                                <div class="col-md-3">ที่อยู่/ที่ทำงาน</div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 form-row">
                                <div class="col-md-2"><label>บิดา</label></div>
                                <div class="show-mobile col-md-4"><label>ชื่อนาม-นามสกุล
                                </label></div>
                                <div class="col-md-3"><input type="text" class="textbox" name="txbFatherName" id="txbFatherName" maxlength="50" ng-model="data.jobtxbFatherName" value="" ></div>
                                <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อายุ</label>
                                </div>
                                <div class="col-md-1"><input type="number" class="textbox" name="txbFatherAge" id="txbFatherAge" maxlength="2" min="0" value="" ng-model="data.jobtxbFatherAge"></div>
                                <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อาชีพ/ตำแหน่ง
                                </label></div>
                                <div class="col-md-3"><input type="text" class="textbox" name="txbFatherOcc" id="txbFatherOcc" maxlength="100" ng-model="data.jobtxbFatherOcc" value=""></div>
                                <div class="show-mobile col-md-4"><label style="padding-top: 10px;">ที่อยู่/ที่ทำงาน
                                </label></div>
                                <div class="col-md-3"><input type="text" class="textbox" name="txbFatherAddr" id="txbFatherAddr" maxlength="100" ng-model="data.jobtxbFatherAddr" value=""></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 form-row">
                                <div class="col-md-2"><label>มารดา</label></div>
                                <div class="show-mobile col-md-4"><label>ชื่อนาม-นามสกุล</label></div>
                                <div class="col-md-3"><input type="text" class="textbox" name="txbMotherName" id="txbMotherName" maxlength="50" ng-model="data.jobtxbMotherName" value=""></div>
                                <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อายุ</label></div>
                                <div class="col-md-1"><input type="number" class="textbox" name="txbMotherAge" id="txbMotherAge" maxlength="2" min="0" value="" ng-model="data.jobtxbMotherAge"></div>
                                <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อาชีพ/ตำแหน่ง
                                </label></div>
                                <div class="col-md-3"><input type="text" class="textbox" name="txbMotherOcc" id="txbMotherOcc" maxlength="100" ng-model="data.jobtxbMotherOcc" value=""></div>
                                <div class="show-mobile col-md-4"><label style="padding-top: 10px;">ที่อยู่/ที่ทำงาน
                                </label></div>
                                <div class="col-md-3"><input type="text" class="textbox" name="txbMotherAddr" id="txbMotherAddr" maxlength="100" ng-model="data.jobtxbMotherAddr" value=""></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-row">
                                <div class="col-md-2"><label>พี่น้อง 1</label></div>
                                <div class="show-mobile col-md-4"><label>ชื่อนาม-นามสกุล</label></div>
                                <div class="col-md-3"><input type="text" class="textbox" name="txbCousin1Name" id="txbCousin1Name" maxlength="50" value="" ng-model="data.jobtxbCousin1Name"></div>
                                <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อายุ</label></div>
                                <div class="col-md-1"><input type="number" class="textbox" name="txbCousin1Age" id="txbCousin1Age" maxlength="2" min="0" value="" ng-model="data.jobtxbCousin1Age"></div>
                                <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อาชีพ/ตำแหน่ง
                                </label></div>
                                <div class="col-md-3"><input type="text" class="textbox" name="txbCousin1Occ" id="txbCousin1Occ" maxlength="100" value="" ng-model="data.jobtxbCousin1Occ"></div>
                                <div class="show-mobile col-md-4"><label style="padding-top: 10px;">ที่อยู่/ที่ทำงาน
                                </label></div>
                                <div class="col-md-3"><input type="text" class="textbox" name="txbCousin1Addr" id="txbCousin1Addr" maxlength="100" value="" ng-model="data.jobtxbCousin1Addr"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-row">
                                <div class="col-md-2"><label>พี่น้อง 2</label></div>
                                <div class="show-mobile col-md-4"><label>ชื่อนาม-นามสกุล</label></div>
                                <div class="col-md-3"><input type="text" class="textbox" name="txbCousin2Name" id="txbCousin2Name" maxlength="50"  value="" ng-model="data.jobtxbCousin2Name"></div>
                                <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อายุ</label></div>
                                <div class="col-md-1"><input type="number" class="textbox" name="txbCousin2Age" id="txbCousin2Age" maxlength="2" value="" min="0" ng-model="data.jobtxbCousin2Age"></div>
                                <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อาชีพ/ตำแหน่ง
                                </label></div>
                                <div class="col-md-3"><input type="text" class="textbox" name="txbCousin2Occ" id="txbCousin2Occ" maxlength="100" value="" ng-model="data.jobtxbCousin2Occ"></div>
                                <div class="show-mobile col-md-4"><label style="padding-top: 10px;">ที่อยู่/ที่ทำงาน
                                </label></div>
                                <div class="col-md-3"><input type="text" class="textbox" name="txbCousin2Addr" id="txbCousin2Addr" maxlength="100" value="" ng-model="data.jobtxbCousin2Addr"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-row">
                                <div class="col-md-2"><label>พี่น้อง 3</label></div>
                                <div class="show-mobile col-md-4"><label>ชื่อนาม-นามสกุล</label></div>
                                <div class="col-md-3"><input type="text" class="textbox" name="txbCousin3Name" id="txbCousin3Name" maxlength="50" value="" ng-model="data.jobtxbCousin3Name"></div>
                                <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อายุ</label></div>
                                <div class="col-md-1"><input type="number" class="textbox" name="txbCousin3Age" id="txbCousin3Age" maxlength="2" min="0" value="" ng-model="data.jobtxbCousin3Age"></div>
                                <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อาชีพ/ตำแหน่ง
                                </label></div>
                                <div class="col-md-3"><input type="text" class="textbox" name="txbCousin3Occ" id="txbCousin3Occ" maxlength="100" value="" ng-model="data.jobtxbCousin3Occ"></div>
                                <div class="show-mobile col-md-4"><label style="padding-top: 10px;">ที่อยู่/ที่ทำงาน
                                </label></div>
                                <div class="col-md-3"><input type="text" class="textbox" name="txbCousin3Addr" id="txbCousin3Addr" maxlength="100" ng-model="data.jobtxbCousin3Addr" value=""></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 form-row">
                                <div class="col-md-2"><label>คู่สมรส</label></div>
                                <div class="show-mobile col-md-4"><label>ชื่อนาม-นามสกุล</label></div>
                                <div class="col-md-3"><input type="text" class="textbox" name="txbSpouseName" id="txbSpouseName" maxlength="50" value="" ng-model="data.jobtxbSpouseName"></div>
                                <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อายุ</label></div>
                                <div class="col-md-1"><input type="number" class="textbox" name="txbSpouseAge" id="txbSpouseAge" maxlength="2" value="" min="0" ng-model="data.jobtxbSpouseAge"></div>
                                <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อาชีพ/ตำแหน่ง
                                </label></div>
                                <div class="col-md-3"><input type="text" class="textbox" name="txbSpouseOcc" id="txbSpouseOcc" maxlength="100" ng-model="data.jobtxbSpouseOcc" value=""></div>
                                <div class="show-mobile col-md-4"><label style="padding-top: 10px;">ที่อยู่/ที่ทำงาน
                                </label></div>
                                <div class="col-md-3"><input type="text" class="textbox" name="txbSpouseAddr" id="txbSpouseAddr" maxlength="100" ng-model="data.jobtxbSpouseAddr" value=""></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 form-row">
                                <div class="col-md-2"><label>จำนวนบุตร (คน)</label></div>
                                <div class="col-md-3"><input type="number" class="textbox" name="txbChildNum" id="txbChildNum" maxlength="2" min="0" value="" ng-model="data.jobtxbChildNum"></div>

                            </div>
                        </div>
                    </div>
                    <div class="form-title">ข้อมูลการติดต่อ</div>
                    <div class="contact_info form-section">
                        <div class="row">
                            <div class="col-md-12 form-row"><div class="col-md-6 title" style="font-size:22px;">ที่อยู่ปัจจุบันที่ติดต่อได้</div></div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ที่อยู่<span class="f-required"> *</span></label></div>
                                <div class="col-md-12">
                                    <input type="text" class="textbox" name="txbAddress" id="txbAddress" ng-model="data.jobtxbAddress" maxlength="255" value="" required>
                                    <span ng-show="!jobform.txbAddress.$pristine && jobform.txbAddress.$error.required" style="color : red" > โปรดกรอกที่อยู่</span>
                                </div>
                            </div>

                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>จังหวัด<span class="f-required"> *</span></label></div>
                                <div class="col-md-12">
                                    <div class="select-block">
                                        <select name="cmbContactProvince" id="cmbContactProvince"
                                        ng-model="data.jobcmbContactProvince"
                                        class="w100-m" required>
                                        <option value="" disabled>จังหวัด</option>
                                        <option value="{{ province.PROVINCE_ID }}" ng-repeat="province in provinces">{{ province.PROVINCE_NAME }}</option>
                                    </select>
                                </div>
                                <span ng-show="!jobform.cmbContactProvince.$pristine && jobform.cmbContactProvince.$error.required" style="color : red" > โปรดเลือกจังหวัด </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-md-12"><label>เบอร์โทรศัพท์ที่สามารถติดต่อได้<span class="f-required"> *</span></label></div>
                            <div class="col-md-12">
                                <input type="text" class="textbox w100-m tel" name="txbContactTel" id="txbContactTel"
                                ng-model="data.jobtxbContactTel" ng-minlength="10"  min="0" required>

                                <span ng-show="!jobform.txbContactTel.$pristine && jobform.txbContactTel.$error.required" style="color : red" > โปรดกรอกเบอร์โทรศัพท์ที่สามารถติดต่อได้ </span>
                                <span ng-show="jobform.txbContactTel.$error.minlength" style="color : red" id="ConTelMin" > กรุณากรอกเบอร์โทรเฉพาะหมายเลขเท่านั้น ไม่ต้องมีขีด (-)</span>

                            </div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="col-md-12"><label>E-mail<span class="f-required"> *</span></label></div>
                            <div class="col-md-12">
                                <input type="email" class="textbox" name="txbContactEmail" id="txbContactEmail" maxlength="60"
                                ng-model="data.jobtxbContactEmail" value="" style="width: 100%">

                                <span id="spfnemail" ng-show="!jobform.txbContactEmail.$pristine" style="color : red" >
                                กรอก Email ให้ถูกต้องและครบถ้วน</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                    </div>
                    <div class="row">
                       <div class="col-md-12 form-row"><div class="col-md-6 title" style="font-size:22px;">
                       บุคคลที่ติดต่อได้ในกรณีฉุกเฉิน</div></div>
                   </div>
                   <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>ชื่อ<span class="f-required"> *</span></label></div>
                        <div class="col-md-12">
                         <input type="text" class="textbox" name="txbEmergencyPerson" id="txbEmergencyPerson"
                         ng-model="data.jobtxbEmergencyPerson" maxlength="100" value="" required>
                         <span ng-show="!jobform.txbEmergencyPerson.$pristine && jobform.txbEmergencyPerson.$error.required"
                         style="color : red" > โปรดกรอกชื่อ </span>
                     </div>
                 </div>
                 <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>เบอร์โทรศัพท์<span class="f-required"> *</span></label></div>
                    <div class="col-md-12">
                        <input type="text" class="textbox tel" name="txbEmergencyTel" id="txbEmergencyTel"
                        ng-model="data.jobtxbEmergencyTel" min="0" ng-minlength="10" value="" required>

                        <span ng-show="!jobform.txbEmergencyTel.$pristine && jobform.txbEmergencyTel.$error.required" style="color : red" > โปรดกรอกเบอร์โทรศัพท์ </span>
                        <span ng-show="jobform.txbEmergencyTel.$error.minlength" style="color : red" id="EmerTelMin" > กรุณากรอกเบอร์โทรเฉพาะหมายเลขเท่านั้น ไม่ต้องมีขีด (-) </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-title">ข้อมูลการศึกษา</div>
        <div class="education_info form-section">
            <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>การศึกษาสูงสุด<span class="f-required"> *</span></label></div>
                    <div class="col-md-12">
                        <div class="select-block">
                            <select name="cmbHighestGraduate" id="cmbHighestGraduate" ng-model="data.jobcmbHighestGraduate"
                            required>
                            <option value="" disabled>เลือกวุฒิการศึกษา</option>
                            <option value="1">ปวช.</option>
                            <option value="2">ปวส.</option>
                            <option value="3">ป.ตรี</option>
                            <option value="4">ป.โท</option>
                            <option value="5">ป.เอก</option>
                        </select>
                    </div>
                    <span ng-show="!jobform.cmbHighestGraduate.$pristine && jobform.cmbHighestGraduate.$error.required" style="color : red" > โปรดเลือกวุฒิการศึกษา </span>
                </div>
            </div>
        </div>

        <!-- ประวัติการศึกษาแบบ mobile -->
        <div class="row hidden-lg hidden-md">
            <div class="col-md-4 form-row" align="center"><br>
               <button type="button" class="btn" id="btnMobile1" data-toggle="collapse"
               data-target="#MobiletabStudy1">ประวัติการศึกษา 1
               <span class="glyphicon glyphicon-triangle-bottom iconmobiledown"></span>
               <span class='glyphicon glyphicon-triangle-top iconmobileup'
               style="display: none;"> </span></button>
           </div>

           <div id="MobiletabStudy1" class="collapse">
            <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>ระดับการศึกษา</label></div>
                    <div class="col-md-12">
                        <div class="select-block">
                            <select name="cmbGraduateGraduate1" id="cmbGraduateGraduate1_f" class="w100-m"
                            ng-model="data.jobcmbGraduateGraduate1">
                            <option value="" disabled>เลือกวุฒิการศึกษา</option>
                            <option value="1">ปวช.</option>
                            <option value="2">ปวส.</option>
                            <option value="3">ป.ตรี</option>
                            <option value="4">ป.โท</option>
                            <option value="5">ป.เอก</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6 form-row">
                <div class="col-md-12"><label class="text-left">สถาบันการศึกษา</label></div>
                <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateInstitute1" id="txbGraduateInstitute1" ng-model="data.jobtxbGraduateInstitute1" maxlength="50" value=""></div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 form-row">
                <div class="col-md-12"><label>คณะ</label></div>
                <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateFaculty1" id="txbGraduateFaculty1" ng-model="data.jobtxbGraduateFaculty1" maxlength="50" value=""></div>
            </div>
            <div class="col-md-6 form-row">
                <div class="col-md-12"><label class="text-left">กลุ่มสาขาวิชา</label></div>
                <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateField1" id="txbGraduateField1" maxlength="50" ng-model="data.jobtxbGraduateField1" value=""></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 form-row">
                <div class="col-md-12">ระยะเวลา</div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 form-row">
                <div class="col-md-12">
                    ตั้งแต่
                </div>
                <div class="col-md-12">
                    <input type="text" name="cmbStudyFrom1" id="cmbStudyFrom1"
                    ng-model="data.jobcmbStudyFrom1" value="" class="hasDatepicker" readonly>
                    <span class="glyphicon glyphicon-calendar form-control-feedback"
                    id="iconstudyFrom1" ></span>
                </div>
            </div>
            <div class="col-md-6 form-row">
                <div class="col-md-12">
                    ถึง
                </div>
                <div class="col-md-12">
                    <input type="text" name="cmbStudyTo1" id="cmbStudyTo1"
                    ng-model="data.jobcmbStudyTo1" value="" class="hasDatepicker" readonly>
                    <span class="glyphicon glyphicon-calendar form-control-feedback"
                    id="iconstudyTo1" ></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 form-row">
                <div class="col-md-12"><label class="text-left">เกรดเฉลี่ย</label></div>
                <div class="col-md-12"><input type="text" class="textbox" name="txbGraduatePoint1" id="txbGraduatePoint1" ng-model="data.jobtxbGraduatePoint1"  value=""></div>
            </div>
        </div>
        <br>

    </div>

    <div class="col-md-4 form-row" align="center">
       <button type="button" class="btn" id="btnMobile2" data-toggle="collapse"
       data-target="#MobiletabStudy2">ประวัติการศึกษา 2
       <span class="glyphicon glyphicon-triangle-bottom iconmobiledown"></span>
       <span class='glyphicon glyphicon-triangle-top iconmobileup'
       style="display: none;"> </span></button>
   </div>

   <div id="MobiletabStudy2"  class="collapse">
      <div class="row">
        <div class="col-md-6 form-row">
            <div class="col-md-12"><label>ระดับการศึกษา</label></div>
            <div class="col-md-12">
                <div class="select-block">
                    <select name="cmbGraduateGraduate1" id="cmbGraduateGraduate1" class="w100-m"
                    ng-model="data.jobcmbGraduateGraduate1">
                    <option value="" disabled>เลือกวุฒิการศึกษา</option>
                    <option value="1">ปวช.</option>
                    <option value="2">ปวส.</option>
                    <option value="3">ป.ตรี</option>
                    <option value="4">ป.โท</option>
                    <option value="5">ป.เอก</option>
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-6 form-row">
        <div class="col-md-12"><label class="text-left">สถาบันการศึกษา</label></div>
        <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateInstitute2" id="txbGraduateInstitute2" ng-model="data.jobtxbGraduateInstitute2" maxlength="50" value=""></div>
    </div>
</div>

<div class="row">
    <div class="col-md-6 form-row">
        <div class="col-md-12"><label>คณะ</label></div>
        <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateFaculty2" id="txbGraduateFaculty2" ng-model="data.jobtxbGraduateFaculty2" maxlength="50" value=""></div>
    </div>
    <div class="col-md-6 form-row">
        <div class="col-md-12"><label class="text-left">กลุ่มสาขาวิชา</label></div>
        <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateField2" id="txbGraduateField2" maxlength="50" ng-model="data.jobtxbGraduateField2" value=""></div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 form-row">
        <div class="col-md-12">ระยะเวลา</div>
    </div>
</div>

<div class="row">
    <div class="col-md-6 form-row">
        <div class="col-md-12">
            ตั้งแต่
        </div>
        <div class="col-md-12">
            <input type="text" name="cmbStudyFrom2" id="cmbStudyFrom2"
            ng-model="data.jobcmbStudyFrom2" value="" class="hasDatepicker" readonly>
            <span class="glyphicon glyphicon-calendar form-control-feedback"
            id="iconstudyFrom1" ></span>
        </div>
    </div>
    <div class="col-md-6 form-row">
        <div class="col-md-12">
            ถึง
        </div>
        <div class="col-md-12">
            <input type="text" name="cmbStudyTo2" id="cmbStudyTo2"
            ng-model="data.jobcmbStudyTo2" value="" class="hasDatepicker" readonly>
            <span class="glyphicon glyphicon-calendar form-control-feedback"
            id="iconstudyTo1" ></span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 form-row">
        <div class="col-md-12"><label class="text-left">เกรดเฉลี่ย</label></div>
        <div class="col-md-12"><input type="text" class="textbox" name="txbGraduatePoint2" id="txbGraduatePoint2" ng-model="data.jobtxbGraduatePoint2"  value=""></div>
    </div>
</div>
<br>

</div>

<div class="col-md-4 form-row" align="center">
   <button type="button"  data-toggle="collapse"
   data-target="#MobiletabStudy3" class="btn" id="btnMobile3">
   ประวัติการศึกษา 3
   <span class="glyphicon glyphicon-triangle-bottom iconmobiledown"></span>
   <span class='glyphicon glyphicon-triangle-top iconmobileup'
   style="display: none;"> </span></button>
</div>

<div id="MobiletabStudy3"  class="collapse">

    <div class="row">
        <div class="col-md-6 form-row">
            <div class="col-md-12"><label>ระดับการศึกษา</label></div>
            <div class="col-md-12">
                <div class="select-block">
                    <select name="cmbGraduateGraduate1" id="cmbGraduateGraduate1" class="w100-m"
                    ng-model="data.jobcmbGraduateGraduate1">
                    <option value="" disabled>เลือกวุฒิการศึกษา</option>
                    <option value="1">ปวช.</option>
                    <option value="2">ปวส.</option>
                    <option value="3">ป.ตรี</option>
                    <option value="4">ป.โท</option>
                    <option value="5">ป.เอก</option>
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-6 form-row">
        <div class="col-md-12"><label class="text-left">สถาบันการศึกษา</label></div>
        <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateInstitute1" id="txbGraduateInstitute1" ng-model="data.jobtxbGraduateInstitute1" maxlength="50" value=""></div>
    </div>
</div>

<div class="row">
    <div class="col-md-6 form-row">
        <div class="col-md-12"><label>คณะ</label></div>
        <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateFaculty3" id="txbGraduateFaculty3" ng-model="data.jobtxbGraduateFaculty3" maxlength="50" value=""></div>
    </div>
    <div class="col-md-6 form-row">
        <div class="col-md-12"><label class="text-left">กลุ่มสาขาวิชา</label></div>
        <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateField3" id="txbGraduateField3" maxlength="50" ng-model="data.jobtxbGraduateField3" value=""></div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 form-row">
        <div class="col-md-12">ระยะเวลา</div>
    </div>
</div>

<div class="row">
    <div class="col-md-6 form-row">
        <div class="col-md-12">
            ตั้งแต่
        </div>
        <div class="col-md-12">
            <input type="text" name="cmbStudyFrom3" id="cmbStudyFrom3"
            ng-model="data.jobcmbStudyFrom3" value="" class="hasDatepicker" readonly>
            <span class="glyphicon glyphicon-calendar form-control-feedback"
            id="iconstudyFrom1" ></span>
        </div>
    </div>
    <div class="col-md-6 form-row">
        <div class="col-md-12">
            ถึง
        </div>
        <div class="col-md-12">
            <input type="text" name="cmbStudyTo1" id="cmbStudyTo1"
            ng-model="data.jobcmbStudyTo3" value="" class="hasDatepicker" readonly>
            <span class="glyphicon glyphicon-calendar form-control-feedback"
            id="iconstudyTo1" ></span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 form-row">
        <div class="col-md-12"><label class="text-left">เกรดเฉลี่ย</label></div>
        <div class="col-md-12"><input type="text" class="textbox" name="txbGraduatePoint3" id="txbGraduatePoint3" ng-model="data.jobtxbGraduatePoint3"  value=""></div>
    </div>
</div>
<br>

</div>

</div>

<!-- tab ประวะติการศึกษา ปกติ -->

<div class="col-md-12 hidden-sm hidden-xs" style="text-align: center;">
    <div class="row ">
       <ul class="tabs-menu " style="border-bottom: 5px solid #f5f5f5;">
        <li class="item current" id="headTabStudy1">
            <a href="#">
                ประวัติการศึกษา 1
            </a>
        </li>
        <li class="item" id="headTabStudy2">
            <a href="#">
                ประวัติการศึกษา 2
            </a>
        </li>
        <li class="item" id="headTabStudy3">
            <a href="#">
                ประวัติการศึกษา 3
            </a>
        </li>
    </ul>

</div>
</div>


<!-- ประวัติการศึกษา แบบปกติ -->

<div claas="col-md-12">

    <div id="tabStudy1" style=" border-bottom:none" class="hidden-sm hidden-xs">
        <div class="row">
          <div class="col-md-6 form-row">
            <div class="col-md-12"><label>1. ระดับการศึกษา</label></div>
            <div class="col-md-12">
                <div class="select-block">
                    <select name="cmbGraduateGraduate1" id="cmbGraduateGraduate1_f1" class="w100-m"
                    ng-model="data.jobcmbGraduateGraduate1">
                    <option value="" disabled>เลือกวุฒิการศึกษา</option>
                    <option value="1">ปวช.</option>
                    <option value="2">ปวส.</option>
                    <option value="3">ป.ตรี</option>
                    <option value="4">ป.โท</option>
                    <option value="5">ป.เอก</option>
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-6 form-row">
        <div class="col-md-12"><label class="text-left">สถาบันการศึกษา</label></div>
        <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateInstitute1" id="txbGraduateInstitute1_f1" ng-model="data.jobtxbGraduateInstitute1" maxlength="50" value=""></div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 form-row">
        <div class="col-md-12"><label>คณะ</label></div>
        <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateFaculty1" id="txbGraduateFaculty1__f1" ng-model="data.jobtxbGraduateFaculty1" maxlength="50" value=""></div>
    </div>
    <div class="col-md-6 form-row">
        <div class="col-md-12"><label class="text-left">กลุ่มสาขาวิชา</label></div>
        <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateField1" id="txbGraduateField1_f1" maxlength="50" ng-model="data.jobtxbGraduateField1" value=""></div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 form-row">
        <div class="col-md-12">ระยะเวลา</div>
    </div>
</div>

<div class="row">
    <div class="col-md-6 form-row">
        <div class="col-md-12">
            ตั้งแต่
        </div>
        <div class="col-md-12">
            <input type="text" name="cmbStudyFrom1" id="cmbStudyFrom1"
            ng-model="data.jobcmbStudyFrom1" value="" class="hasDatepicker" readonly>
            <span class="glyphicon glyphicon-calendar form-control-feedback"
            id="iconstudyFrom1" ></span>
        </div>
    </div>
    <div class="col-md-6 form-row">
        <div class="col-md-12">
            ถึง
        </div>
        <div class="col-md-12">
            <input type="text" name="cmbStudyTo1" id="cmbStudyTo1"
            ng-model="data.jobcmbStudyTo1" value="" class="hasDatepicker" readonly>
            <span class="glyphicon glyphicon-calendar form-control-feedback"
            id="iconstudyTo1" ></span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 form-row">
        <div class="col-md-12"><label class="text-left">เกรดเฉลี่ย</label></div>
        <div class="col-md-12"><input type="text" class="textbox grade" name="txbGraduatePoint1" id="txbGraduatePoint1" ng-model="data.jobtxbGraduatePoint1"  value=""></div>
    </div>
</div>

</div>

<div id="tabStudy2"  style="display:none; border-bottom:none" class="hidden-sm hidden-xs">

    <div class="row">
      <div class="col-md-6 form-row">
        <div class="col-md-12"><label>2. ระดับการศึกษา</label></div>
        <div class="col-md-12">
            <div class="select-block">
                <select name="cmbGraduateGraduate2" id="cmbGraduateGraduate2" class="w100-m"
                ng-model="data.jobcmbGraduateGraduate2">
                <option value="" disabled>เลือกวุฒิการศึกษา</option>
                <option value="1">ปวช.</option>
                <option value="2">ปวส.</option>
                <option value="3">ป.ตรี</option>
                <option value="4">ป.โท</option>
                <option value="5">ป.เอก</option>
            </select>
        </div>
    </div>
</div>
<div class="col-md-6 form-row">
    <div class="col-md-12"><label class="text-left">สถาบันการศึกษา</label></div>
    <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateInstitute2" id="txbGraduateInstitute2" ng-model="data.jobtxbGraduateInstitute2" maxlength="50" value=""></div>
</div>
</div>
<div class="row">
    <div class="col-md-6 form-row">
        <div class="col-md-12"><label>คณะ</label></div>
        <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateFaculty2" id="txbGraduateFaculty2" ng-model="data.jobtxbGraduateFaculty2" maxlength="50" value=""></div>
    </div>
    <div class="col-md-6 form-row">
        <div class="col-md-12"><label class="text-left">กลุ่มสาขาวิชา</label></div>
        <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateField2" id="txbGraduateField2" maxlength="50" ng-model="data.jobtxbGraduateField2" value=""></div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 form-row">
        <div class="col-md-12">ระยะเวลา</div>
    </div>
</div>

<div class="row">
    <div class="col-md-6 form-row">
        <div class="col-md-12">
            ตั้งแต่
        </div>
        <div class="col-md-12">
            <input type="text" name="cmbStudyFrom2" id="cmbStudyFrom2"
            ng-model="data.jobcmbStudyFrom2" value="" class="hasDatepicker" readonly>
            <span class="glyphicon glyphicon-calendar form-control-feedback"
            id="iconstudyFrom1" ></span>
        </div>
    </div>
    <div class="col-md-6 form-row">
        <div class="col-md-12">
            ถึง
        </div>
        <div class="col-md-12">
            <input type="text" name="cmbStudyTo2" id="cmbStudyTo2"
            ng-model="data.jobcmbStudyTo2" value="" class="hasDatepicker" readonly>
            <span class="glyphicon glyphicon-calendar form-control-feedback"
            id="iconstudyTo1" ></span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 form-row">
        <div class="col-md-12"><label class="text-left">เกรดเฉลี่ย</label></div>
        <div class="col-md-12"><input type="text" class="textbox grade" name="txbGraduatePoint2" id="txbGraduatePoint2" ng-model="data.jobtxbGraduatePoint2"   value=""></div>
    </div>
</div>

</div>

<div id="tabStudy3"  style="display:none; border-bottom:none" class="hidden-sm hidden-xs">

   <div class="row">
      <div class="col-md-6 form-row">
        <div class="col-md-12"><label>3. ระดับการศึกษา</label></div>
        <div class="col-md-12">
            <div class="select-block">
                <select name="cmbGraduateGraduate3" id="cmbGraduateGraduate3" class="w100-m"
                ng-model="data.jobcmbGraduateGraduate3">
                <option value="" disabled>เลือกวุฒิการศึกษา</option>
                <option value="1">ปวช.</option>
                <option value="2">ปวส.</option>
                <option value="3">ป.ตรี</option>
                <option value="4">ป.โท</option>
                <option value="5">ป.เอก</option>
            </select>
        </div>
    </div>
</div>
<div class="col-md-6 form-row">
    <div class="col-md-12"><label class="text-left">สถาบันการศึกษา</label></div>
    <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateInstitute3" id="txbGraduateInstitute3" ng-model="data.jobtxbGraduateInstitute3" maxlength="50" value=""></div>
</div>
</div>
<div class="row">
    <div class="col-md-6 form-row">
        <div class="col-md-12"><label>คณะ</label></div>
        <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateFaculty3" id="txbGraduateFaculty3" ng-model="data.jobtxbGraduateFaculty3" maxlength="50" value=""></div>
    </div>
    <div class="col-md-6 form-row">
        <div class="col-md-12"><label class="text-left">กลุ่มสาขาวิชา</label></div>
        <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateField3" id="txbGraduateField3" maxlength="50" ng-model="data.jobtxbGraduateField3" value=""></div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 form-row">
        <div class="col-md-12">ระยะเวลา</div>
    </div>
</div>

<div class="row">
    <div class="col-md-6 form-row">
        <div class="col-md-12">
            ตั้งแต่
        </div>
        <div class="col-md-12">
            <input type="text" name="cmbStudyFrom3" id="cmbStudyFrom3"
            ng-model="data.jobcmbStudyFrom3" value="" class="hasDatepicker" readonly>
            <span class="glyphicon glyphicon-calendar form-control-feedback"
            id="iconstudyFrom1" ></span>
        </div>
    </div>
    <div class="col-md-6 form-row">
        <div class="col-md-12">
            ถึง
        </div>
        <div class="col-md-12">
            <input type="text" name="cmbStudyTo3" id="cmbStudyTo3"
            ng-model="data.jobcmbStudyTo3" value="" class="hasDatepicker" readonly>
            <span class="glyphicon glyphicon-calendar form-control-feedback"
            id="iconstudyTo1" ></span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 form-row">
        <div class="col-md-12"><label class="text-left">เกรดเฉลี่ย</label></div>
        <div class="col-md-12"><input type="text" class="textbox grade" name="txbGraduatePoint3" id="txbGraduatePoint3" ng-model="data.jobtxbGraduatePoint3"   value=""></div>
    </div>
</div>

</div>
</div>

</div>
<div class="row" align="center">
    <div class="col-md-2"></div>
    <div class="col-md-4 form-row">
        <input type="button"  id="backJobLists" class=" action-button" value="ย้อนกลับ" />
    </div>
    <div class="col-md-4 form-row">
      <input type="button" id="btnnext1" name="next" class="next" value="ต่อไป"/>
  </div>
</div>

</fieldset>

<fieldset>
    <div class="form-title">ข้อมูลการทำงาน</div>
    <div class="experience_info form-section">
       <div class="row">
        <div class="col-md-6 form-row">
            <div class="col-md-9"><label>เพิ่งสำเร็จการศึกษา<span class="f-required"> *</span></label></div><br>
            <div class="col-md-12">
                <input type="radio" name="rdoJustGraduate" id="rdoJustGraduateY"
                ng-model=data.jobrdoJustGraduate value="ใช่" required> &nbsp; <label for="rdoJustGraduateY"><span></span>ใช่</label> &nbsp; &nbsp;
                <input type="radio" name="rdoJustGraduate" id="rdoJustGraduateN"
                ng-model=data.jobrdoJustGraduate value="ไม่ใช่" required> &nbsp; <label for="rdoJustGraduateN"><span></span>ไม่ใช่</label>
            </div>
        </div>

        <div class="col-md-6 form-row">
            <div class="col-md-12"><label>ประสบการณ์การทำงาน (ปี)</label></div>
            <div class="col-md-12">
                <input type="number" class="textbox" name="txbWorkExperience" id="txbWorkExperience"
                ng-model="data.jobtxbWorkExperience" min="0" maxlength="2">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 form-row">
            <div class="col-md-12"><label>สายงานล่าสุด</label></div>
            <div class="col-md-12">
                <!--                                    <div class="select-block">-->
                    <input type="text" name="cmbLastWorkField" id="cmbLastWorkField"
                    ng-model="data.jobcmbLastWorkField" >
                    <!--                                        <select name="cmbLastWorkField" id="cmbLastWorkField" -->
                        <!--                                        ng-model="data.jobcmbLastWorkField" class="w100-m">-->
                        <!--                                            <option value="" disabled>เลือกสายงาน</option>-->
                        <!--                                            <option ng-repeat="job in jobs" -->
                            <!--                                            value="{{ job.TITLE }}"> {{ job.TITLE }}-->
                            <!--                                            </option>-->
                            <!--                                        </select>-->
                            <!--                                    </div>-->
                        </div>
                    </div>

                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>ตำแหน่งสุดท้าย</label></div>
                        <div class="col-md-12"><input type="text" class="textbox w100-m" name="txbLastPosition" id="txbLastPosition" maxlength="50" ng-model="data.jobtxbLastPosition" width="100%" value=""></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>เงินเดือนสุดท้าย (บาท) </label></div>
                        <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbLastSalary" id="txbLastSalary" maxlength="7" min="0" ng-model="data.jobtxbLastSalary" value=""> </div>
                    </div>
                </div><br>


                <!-- ส่วนmobile ประวัติทำงาน -->
                <div class="row hidden-lg hidden-md">
                    <div class="col-md-4 form-row" align="center">
                       <button type="button" class="btn" id="btnWorkMobile1" data-toggle="collapse"
                       data-target="#Mobiletab1">ประวัติการทำงาน 1
                       <span class="glyphicon glyphicon-triangle-bottom iconmobiledown"></span>
                       <span class='glyphicon glyphicon-triangle-top iconmobileup'
                       style="display: none;"> </span></button>
                   </div>
                   <div id="Mobiletab1" class="collapse">

                    <div class="row">
                        <div class="col-md-12 form-row">
                            <div class="col-md-12">ระยะเวลา</div>
                        </div>
                    </div>

                    <div class="row">
                       <div class="col-md-6 form-row">
                        <div class="col-md-12">ตั้งแต่</div>
                        <div class="col-md-12">
                            <input type="text" name="cmbWorkFrom1" id="cmbWorkFrom1" ng-model="data.jobcmbWorkFrom1" class="hasDatepicker" readonly value="">
                            <span class="glyphicon glyphicon-calendar form-control-feedback"
                            id="iconworkFrom1" ></span>
                        </div>
                    </div>
                    <div class="col-md-6 form-row">
                        <div class="col-md-12">ถึง</div>
                        <div class="col-md-12">
                            <span class="cmbWorkTo1_test" style="
                            display: none;
                            top: 31px;
                            padding-right: 25px;
                            position: absolute;
                            left: 12px;
                            background-color: #fff;
                            ">ปัจจุบัน</span>
                            <input type="text" name="cmbWorkTo1" id="cmbWorkTo1" ng-model="data.jobcmbWorkTo1" class="hasDatepicker" readonly value="">
                            <span class="glyphicon glyphicon-calendar form-control-feedback"
                            id="iconworkTo1" ></span>

                        </div>
                        <div class="col-md-12 form-row">
                            <label>หรือ</label> &nbsp; &nbsp;
                            <input type="checkbox" id="chWorkTo1" name="chWorkTo1" > <label for="chWorkTo1">
                                <span class="span-chWorkTo1" style="height: 19px; width: 19px;"></span> ปัจจุบัน </label>
                            </div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="col-md-12"><label>ตำแหน่ง</label></div>
                            <div class="col-md-12">

                                <input type="text" class="textbox" name="txbWorkPosition1" id="txbWorkPosition1" ng-model="data.jobtxbWorkPosition1" maxlength="50" value="">
                            </div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="col-md-12"><label>บริษัท</label></div>
                            <div class="col-md-12"><input type="text" class="textbox" name="txbWorkCompany1" id="txbWorkCompany1" ng-model="data.jobtxbWorkCompany1" maxlength="50" value=""></div>
                        </div>

                        <div class="col-md-6 form-row">
                            <div class="col-md-12"><label>เงินเดือน (บาท) </label></div>
                            <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkSalary1" id="txbWorkSalary1" maxlength="7" ng-model="data.jobtxbWorkSalary1" value="" min="0"></div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="col-md-12"><label>รายได้อื่น ๆ (บาท) </label></div>
                            <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkIncome1" id="txbWorkIncome1" maxlength="7"
                                ng-model="data.jobtxbWorkIncome1" min="0" value=""></div>
                            </div>

                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ระบุรายได้อื่น ๆ</label></div>
                                <div class="col-md-12"><input type="text" class="textbox w100-m"
                                    name="txbWorkIncomeOther1" id="txbWorkIncomeOther1" ng-model="data.jobtxbWorkIncomeOther1" maxlength="100" value=""></div>
                                </div>
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>เหตุผลที่ออก</label></div>
                                    <div class="col-md-12"><input type="text" class="textbox" name="txbWorkDetail1"
                                        id="txbWorkDetail1" ng-model="data.jobtxbWorkDetail1">
                                    </div>
                                </div><br>

                            </div>
                        </div>

                        <div class="col-md-4 form-row" align="center">
                           <button type="button" class="btn" id="btnWorkMobile2" data-toggle="collapse"
                           data-target="#Mobiletab2">ประวัติการทำงาน 2
                           <span class="glyphicon glyphicon-triangle-bottom iconmobiledown"></span>
                           <span class='glyphicon glyphicon-triangle-top iconmobileup'
                           style="display: none;"> </span></button>
                       </div>
                       <div id="Mobiletab2" class="collapse">

                        <div class="row">
                            <div class="col-md-12 form-row">
                                <div class="col-md-12">ระยะเวลา</div>
                            </div>
                        </div>

                        <div class="row">
                         <div class="col-md-6 form-row">
                            <div class="col-md-12">ตั้งแต่</div>
                            <div class="col-md-12">
                                <input type="text" name="cmbWorkFrom2" id="cmbWorkFrom2" ng-model="data.jobcmbWorkFrom2" class="hasDatepicker" readonly value="">
                                <span class="glyphicon glyphicon-calendar form-control-feedback"
                                id="iconworkFrom2" ></span>
                            </div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="col-md-12">ถึง</div>
                            <div class="col-md-12">
                                <input type="text" name="cmbWorkTo2" id="cmbWorkTo2" ng-model="data.jobcmbWorkTo2" class="hasDatepicker" readonly value="">
                                <span class="glyphicon glyphicon-calendar form-control-feedback"
                                id="iconworkTo2" ></span>
                            </div>
                            <div class="col-md-12 form-row">
                                <label>หรือ</label> &nbsp; &nbsp;
                                <input type="checkbox" id="chWorkTo2" name="chWorkTo2" >
                                <label for="chWorkTo2">
                                    <span class="span-chWorkTo2" style="height: 19px; width: 19px;"></span> ปัจจุบัน </label>
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ตำแหน่ง</label></div>
                                <div class="col-md-12">

                                    <input type="text" class="textbox" name="txbWorkPosition2" id="txbWorkPosition2" ng-model="data.jobtxbWorkPosition2" maxlength="50" value="">
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>บริษัท</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbWorkCompany2" id="txbWorkCompany2" ng-model="data.jobtxbWorkCompany2" maxlength="50" value=""></div>
                            </div>

                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>เงินเดือน (บาท) </label></div>
                                <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkSalary2" id="txbWorkSalary2" maxlength="7" ng-model="data.jobtxbWorkSalary2"
                                    value="" min="0"></div>
                                </div>
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>รายได้อื่น ๆ (บาท) </label></div>
                                    <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkIncome2" id="txbWorkIncome2" maxlength="7"
                                        ng-model="data.jobtxbWorkIncome2" min="0" value=""></div>
                                    </div>

                                    <div class="col-md-6 form-row">
                                        <div class="col-md-12"><label>ระบุรายได้อื่น ๆ</label></div>
                                        <div class="col-md-12"><input type="text" class="textbox w100-m"
                                            name="txbWorkIncomeOther2" id="txbWorkIncomeOther2" ng-model="data.jobtxbWorkIncomeOther2" maxlength="100" value=""></div>
                                        </div>
                                        <div class="col-md-6 form-row">
                                            <div class="col-md-12"><label>เหตุผลที่ออก</label></div>
                                            <div class="col-md-12"><input type="text" class="textbox" name="txbWorkDetail2"
                                                id="txbWorkDetail2" ng-model="data.jobtxbWorkDetail2">
                                            </div>
                                        </div><br>

                                    </div>
                                </div>

                                <div class="col-md-4 form-row" align="center">
                                    <button type="button" class="btn" id="btnWorkMobile3" data-toggle="collapse"
                                    data-target="#Mobiletab3">ประวัติการทำงาน 3
                                    <span class="glyphicon glyphicon-triangle-bottom iconmobiledown"></span>
                                    <span class='glyphicon glyphicon-triangle-top iconmobileup'
                                    style="display: none;"> </span></button>
                                </div>

                                <div id="Mobiletab3" class="collapse">
                                    <div class="row">
                                        <div class="col-md-12 form-row">
                                            <div class="col-md-12">ระยะเวลา</div>
                                        </div>
                                    </div>

                                    <div class="row">
                                       <div class="col-md-6">
                                        <div class="col-md-12">ตั้งแต่</div>
                                        <div class="col-md-12">
                                            <input type="text" name="cmbWorkFrom3" id="cmbWorkFrom3" ng-model="data.jobcmbWorkFrom3" class="hasDatepicker" readonly value="">
                                            <span class="glyphicon glyphicon-calendar form-control-feedback"
                                            id="iconworkFrom3" ></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-row">
                                        <div class="col-md-12">ถึง</div>
                                        <div class="col-md-12">
                                            <input type="text" name="cmbWorkTo3" id="cmbWorkTo3" ng-model="data.jobcmbWorkTo3" class="hasDatepicker" readonly value="">
                                            <span class="glyphicon glyphicon-calendar form-control-feedback"
                                            id="iconworkTo3" ></span>
                                        </div>
                                        <div class="col-md-12 form-row">
                                            <label>หรือ</label> &nbsp; &nbsp;
                                            <input type="checkbox" id="chWorkTo3" name="chWorkTo3" >
                                            <label for="chWorkTo3">
                                                <span class="span-chWorkTo3" style="height: 19px; width: 19px;"></span> ปัจจุบัน </label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 form-row">
                                            <div class="col-md-12"><label>ตำแหน่ง</label></div>
                                            <div class="col-md-12">

                                                <input type="text" class="textbox" name="txbWorkPosition3" id="txbWorkPosition3" ng-model="data.jobtxbWorkPosition3" maxlength="50" value="">
                                            </div>
                                        </div>
                                        <div class="col-md-6 form-row">
                                            <div class="col-md-12"><label>บริษัท</label></div>
                                            <div class="col-md-12"><input type="text" class="textbox" name="txbWorkCompany3" id="txbWorkCompany3" ng-model="data.jobtxbWorkCompany3" maxlength="50" value=""></div>
                                        </div>

                                        <div class="col-md-6 form-row">
                                            <div class="col-md-12"><label>เงินเดือน (บาท) </label></div>
                                            <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkSalary3" id="txbWorkSalary3" maxlength="7" ng-model="data.jobtxbWorkSalary3"
                                                value="" min="0"></div>
                                            </div>
                                            <div class="col-md-6 form-row">
                                                <div class="col-md-12"><label>รายได้อื่น ๆ (บาท) </label></div>
                                                <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkIncome3" id="txbWorkIncome3" maxlength="7"
                                                    ng-model="data.jobtxbWorkIncome3" min="0" value=""></div>
                                                </div>

                                                <div class="col-md-6 form-row">
                                                    <div class="col-md-12"><label>ระบุรายได้อื่น ๆ</label></div>
                                                    <div class="col-md-12"><input type="text" class="textbox w100-m"
                                                        name="txbWorkIncomeOther3" id="txbWorkIncomeOther3" ng-model="data.jobtxbWorkIncomeOther3" maxlength="100" value=""></div>
                                                    </div>
                                                    <div class="col-md-6 form-row">
                                                        <div class="col-md-12"><label>เหตุที่ลาออก</label></div>
                                                        <div class="col-md-12"><input type="text" class="textbox" name="txbWorkDetail3"
                                                            id="txbWorkDetail3" ng-model="data.jobtxbWorkDetail3">
                                                        </div>
                                                    </div><br>

                                                </div>
                                            </div>

                                            <div class="col-md-4 form-row" align="center">
                                                <button type="button" class="btn" id="btnWorkMobile4" data-toggle="collapse"
                                                data-target="#Mobiletab4">ประวัติการทำงาน 4
                                                <span class="glyphicon glyphicon-triangle-bottom iconmobiledown"></span>
                                                <span class='glyphicon glyphicon-triangle-top iconmobileup'
                                                style="display: none;"> </span></button>
                                            </div>

                                            <div id="Mobiletab4" class="collapse">
                                             <div class="row">
                                                <div class="col-md-12 form-row">
                                                    <div class="col-md-12">ระยะเวลา</div>
                                                </div>
                                            </div>

                                            <div class="row">
                                             <div class="col-md-6 form-row">
                                                <div class="col-md-12">ตั้งแต่</div>
                                                <div class="col-md-12">
                                                    <input type="text" name="cmbWorkFrom4" id="cmbWorkFrom4" ng-model="data.jobcmbWorkFrom4" class="hasDatepicker" readonly value="">
                                                    <span class="glyphicon glyphicon-calendar form-control-feedback"
                                                    id="iconworkFrom4" ></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6 form-row">
                                                <div class="col-md-12">ถึง</div>
                                                <div class="col-md-12">
                                                    <input type="text" name="cmbWorkTo4" id="cmbWorkTo4" ng-model="data.jobcmbWorkTo4" class="hasDatepicker" readonly value="">
                                                    <span class="glyphicon glyphicon-calendar form-control-feedback"
                                                    id="iconworkTo4" ></span>
                                                </div>
                                                <div class="col-md-12 form-row">
                                                    <label>หรือ</label> &nbsp; &nbsp;
                                                    <input type="checkbox" id="chWorkTo4" name="chWorkTo4" >
                                                    <label for="chWorkTo4">
                                                        <span class="span-chWorkTo4" style="height: 19px; width: 19px;"></span> ปัจจุบัน </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 form-row">
                                                    <div class="col-md-12"><label>ตำแหน่ง</label></div>
                                                    <div class="col-md-12">

                                                        <input type="text" class="textbox" name="txbWorkPosition4" id="txbWorkPosition4" ng-model="data.jobtxbWorkPosition4" maxlength="50" value="">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 form-row">
                                                    <div class="col-md-12"><label>บริษัท</label></div>
                                                    <div class="col-md-12"><input type="text" class="textbox" name="txbWorkCompany4" id="txbWorkCompany4" ng-model="data.jobtxbWorkCompany4" maxlength="50" value=""></div>
                                                </div>

                                                <div class="col-md-6 form-row">
                                                    <div class="col-md-12"><label>เงินเดือน (บาท) </label></div>
                                                    <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkSalary4" id="txbWorkSalary4" maxlength="7" ng-model="data.jobtxbWorkSalary4"
                                                        value="" min="0"></div>
                                                    </div>
                                                    <div class="col-md-6 form-row">
                                                        <div class="col-md-12"><label>รายได้อื่น ๆ (บาท) </label></div>
                                                        <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkIncome4" id="txbWorkIncome4" maxlength="7"
                                                            ng-model="data.jobtxbWorkIncome4" min="0" value=""></div>
                                                        </div>

                                                        <div class="col-md-6 form-row">
                                                            <div class="col-md-12"><label>ระบุรายได้อื่น ๆ</label></div>
                                                            <div class="col-md-12"><input type="text" class="textbox w100-m"
                                                                name="txbWorkIncomeOther4" id="txbWorkIncomeOther4" ng-model="data.jobtxbWorkIncomeOther4" maxlength="100" value=""></div>
                                                            </div>
                                                            <div class="col-md-6 form-row">
                                                                <div class="col-md-12"><label>เหตุที่ลาออก</label></div>
                                                                <div class="col-md-12"><input type="text" class="textbox" name="txbWorkDetail4"
                                                                    id="txbWorkDetail4" ng-model="data.jobtxbWorkDetail4">
                                                                </div>
                                                            </div><br>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-4 form-row" align="center">
                                                        <button type="button" class="btn" id="btnWorkMobile5" data-toggle="collapse"
                                                        data-target="#Mobiletab5">ประวัติการทำงาน 5
                                                        <span class="glyphicon glyphicon-triangle-bottom iconmobiledown"></span>
                                                        <span class='glyphicon glyphicon-triangle-top iconmobileup'
                                                        style="display: none;"> </span></button>
                                                    </div>

                                                    <div id="Mobiletab5" class="collapse">
                                                        <div class="row">
                                                            <div class="col-md-12 form-row">
                                                                <div class="col-md-12">ระยะเวลา</div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                         <div class="col-md-6 form-row">
                                                            <div class="col-md-12">ตั้งแต่</div>
                                                            <div class="col-md-12">
                                                                <input type="text" name="cmbWorkFrom5" id="cmbWorkFrom5" ng-model="data.jobcmbWorkFrom5" class="hasDatepicker" readonly value="">
                                                                <span class="glyphicon glyphicon-calendar form-control-feedback"
                                                                id="iconworkFrom5" ></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 form-row">
                                                            <div class="col-md-12">ถึง</div>
                                                            <div class="col-md-12">
                                                                <input type="text" name="cmbWorkTo5" id="cmbWorkTo5" ng-model="data.jobcmbWorkTo5" class="hasDatepicker" readonly value="">
                                                                <span class="glyphicon glyphicon-calendar form-control-feedback"
                                                                id="iconworkTo5" ></span>
                                                            </div>
                                                            <div class="col-md-12 form-row">
                                                                <label>หรือ</label> &nbsp; &nbsp;
                                                                <input type="checkbox" id="chWorkTo5" name="chWorkTo5" >
                                                                <label for="chWorkTo5">
                                                                    <span class="span-chWorkTo5" style="height: 19px; width: 19px;"></span> ปัจจุบัน </label>
                                                                </div>

                                                            </div>
                                                            <div class="col-md-6 form-row">
                                                                <div class="col-md-12"><label>ตำแหน่ง</label></div>
                                                                <div class="col-md-12">

                                                                    <input type="text" class="textbox" name="txbWorkPosition5" id="txbWorkPosition5" ng-model="data.jobtxbWorkPosition5" maxlength="50" value="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 form-row">
                                                                <div class="col-md-12"><label>บริษัท</label></div>
                                                                <div class="col-md-12"><input type="text" class="textbox" name="txbWorkCompany5" id="txbWorkCompany5" ng-model="data.jobtxbWorkCompany5" maxlength="50" value=""></div>
                                                            </div>

                                                            <div class="col-md-6 form-row">
                                                                <div class="col-md-12"><label>เงินเดือน (บาท) </label></div>
                                                                <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkSalary5" id="txbWorkSalary5" maxlength="7" ng-model="data.jobtxbWorkSalary5"
                                                                    value="" min="0"></div>
                                                                </div>
                                                                <div class="col-md-6 form-row">
                                                                    <div class="col-md-12"><label>รายได้อื่น ๆ (บาท) </label></div>
                                                                    <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkIncome5" id="txbWorkIncome5" maxlength="7"
                                                                        ng-model="data.jobtxbWorkIncome5" min="0" value=""></div>
                                                                    </div>

                                                                    <div class="col-md-6 form-row">
                                                                        <div class="col-md-12"><label>ระบุรายได้อื่น ๆ</label></div>
                                                                        <div class="col-md-12"><input type="text" class="textbox w100-m"
                                                                            name="txbWorkIncomeOther5" id="txbWorkIncomeOther5" ng-model="data.jobtxbWorkIncomeOther5" maxlength="100" value=""></div>
                                                                        </div>
                                                                        <div class="col-md-6 form-row">
                                                                            <div class="col-md-12"><label>เหตุที่ลาออก</label></div>
                                                                            <div class="col-md-12"><input type="text" class="textbox" name="txbWorkDetail5"
                                                                                id="txbWorkDetail5" ng-model="data.jobtxbWorkDetail5">
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <!-- tab ประวัติการทำงาน แบบธรรมดา -->
                                                            <div class="col-sm-12  hidden-xs hidden-sm">
                                                                <div class="row">

                                                                    <ul class="tabs-menu" style="border-bottom: 5px solid #f5f5f5; text-align: center;">
                                                                        <li class="item current" id="headTab1">
                                                                            <a href="#">
                                                                                ประวัติการทำงาน 1
                                                                            </a>
                                                                        </li>
                                                                        <li class="item" id="headTab2">
                                                                            <a href="#">
                                                                                ประวัติการทำงาน 2
                                                                            </a>
                                                                        </li>
                                                                        <li class="item" id="headTab3">
                                                                            <a href="#">
                                                                             ประวัติการทำงาน 3
                                                                         </a>
                                                                     </li>
                                                                     <li class="item" id="headTab4">
                                                                        <a href="#">
                                                                         ประวัติการทำงาน 4
                                                                     </a>
                                                                 </li>
                                                                 <li class="item" id="headTab5">
                                                                    <a href="#">
                                                                        ประวัติการทำงาน 5
                                                                    </a>
                                                                </li>
                                                            </ul>

                                                        </div>
                                                    </div>

                                                    <!-- ประวัติการทำงานแบบปกติ -->
                                                    <div id="tab1" style=" border-bottom:none" class="hidden-xs hidden-sm">

                                                        <div class="row">
                                                            <div class="col-md-12 form-row">
                                                                <div class="col-md-12">1. ระยะเวลา</div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                           <div class="col-md-6 form-row">
                                                            <div class="col-md-12">ตั้งแต่</div>
                                                            <div class="col-md-12">
                                                                <input type="text" name="cmbWorkFrom1" id="cmbWorkFrom1" ng-model="data.jobcmbWorkFrom1" class="hasDatepicker" readonly value="">
                                                                <span class="glyphicon glyphicon-calendar form-control-feedback"
                                                                id="iconworkFrom1" ></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 form-row">
                                                            <div class="col-md-12">ถึง</div>
                                                            <div class="col-md-12">
                                                                <span class="cmbWorkTo1_test" style="
                                                                display: none;
                                                                top: 10px;
                                                                left: 28px;
                                                                background-color: #fff;
                                                                padding-right: 25px;
                                                                position: absolute;
                                                                ">ปัจจุบัน</span>
                                                                <input type="text" name="cmbWorkTo1" id="cmbWorkTo1"
                                                                ng-model="data.jobcmbWorkTo1" class="hasDatepicker" readonly>
                                                                <input type="hidden" name="cmbWorkTo1_is" id="cmbWorkTo1_is">
                                                                <span class="glyphicon glyphicon-calendar form-control-feedback"
                                                                id="iconworkTo1"></span>
                                                            </div>

                                                            <div class="col-md-12 form-row">
                                                                <label>หรือ</label> &nbsp; &nbsp;
                                                                <input type="checkbox" id="chWorkTo1" name="chWorkTo1" > <label for="chWorkTo1">
                                                                    <span class="span-chWorkTo1" style="height: 19px; width: 19px;"></span> ปัจจุบัน </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 form-row">
                                                                <div class="col-md-12"><label>ตำแหน่ง</label></div>
                                                                <div class="col-md-12">

                                                                    <input type="text" class="textbox" name="txbWorkPosition1" id="txbWorkPosition1" ng-model="data.jobtxbWorkPosition1" maxlength="50" value="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 form-row">
                                                                <div class="col-md-12"><label>บริษัท</label></div>
                                                                <div class="col-md-12"><input type="text" class="textbox" name="txbWorkCompany1" id="txbWorkCompany1" ng-model="data.jobtxbWorkCompany1" maxlength="50" value=""></div>
                                                            </div>

                                                            <div class="col-md-6 form-row">
                                                                <div class="col-md-12"><label>เงินเดือน (บาท) </label></div>
                                                                <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkSalary1" id="txbWorkSalary1" maxlength="7" ng-model="data.jobtxbWorkSalary1" value="" min="0"></div>
                                                            </div>
                                                            <div class="col-md-6 form-row">
                                                                <div class="col-md-12"><label>รายได้อื่น ๆ (บาท) </label></div>
                                                                <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkIncome1" id="txbWorkIncome1" maxlength="7"
                                                                    ng-model="data.jobtxbWorkIncome1" min="0" value=""></div>
                                                                </div>

                                                                <div class="col-md-6 form-row">
                                                                    <div class="col-md-12"><label>ระบุรายได้อื่น ๆ</label></div>
                                                                    <div class="col-md-12"><input type="text" class="textbox w100-m"
                                                                        name="txbWorkIncomeOther1" id="txbWorkIncomeOther1" ng-model="data.jobtxbWorkIncomeOther1" maxlength="100" value=""></div>
                                                                    </div>
                                                                    <div class="col-md-6 form-row">
                                                                        <div class="col-md-12"><label>เหตุที่ลาออก</label></div>
                                                                        <div class="col-md-12"><input type="text" class="textbox" name="txbWorkDetail1"
                                                                            id="txbWorkDetail1" ng-model="data.jobtxbWorkDetail1">
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div id="tab2" style="display:none; border-bottom:none" class="hidden-xs hidden-sm">

                                                                <div class="row">
                                                                    <div class="col-md-12 form-row">
                                                                        <div class="col-md-12">2. ระยะเวลา</div>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                 <div class="col-md-6 form-row">
                                                                    <div class="col-md-12">ตั้งแต่</div>
                                                                    <div class="col-md-12">
                                                                        <input type="text" name="cmbWorkFrom2" id="cmbWorkFrom2" ng-model="data.jobcmbWorkFrom2" class="hasDatepicker" readonly value="">
                                                                        <span class="glyphicon glyphicon-calendar form-control-feedback"
                                                                        id="iconworkFrom2" ></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 form-row">
                                                                    <div class="col-md-12">ถึง</div>
                                                                    <div class="col-md-12">
                                                                        <span class="cmbWorkTo2_test" style="
                                                                        display: none;
                                                                        top: 10px;
                                                                        left: 28px;
                                                                        background-color: #fff;
                                                                        padding-right: 25px;
                                                                        position: absolute;
                                                                        ">ปัจจุบัน</span>
                                                                        <input type="text" name="cmbWorkTo2" id="cmbWorkTo2" ng-model="data.jobcmbWorkTo2" class="hasDatepicker" readonly value="">
                                                                        <input type="hidden" name="cmbWorkTo2_is" id="cmbWorkTo2_is">
                                                                        <span class="glyphicon glyphicon-calendar form-control-feedback"
                                                                        id="iconworkTo2" ></span>
                                                                    </div>
                                                                    <div class="col-md-12 form-row">
                                                                        <label>หรือ</label> &nbsp; &nbsp;
                                                                        <input type="checkbox" id="chWorkTo2" name="chWorkTo2">
                                                                        <label for="chWorkTo2">
                                                                            <span class="span-chWorkTo2" style="height: 19px; width: 19px;"></span> ปัจจุบัน </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 form-row">
                                                                        <div class="col-md-12"><label>ตำแหน่ง</label></div>
                                                                        <div class="col-md-12">

                                                                            <input type="text" class="textbox" name="txbWorkPosition2" id="txbWorkPosition2" ng-model="data.jobtxbWorkPosition2" maxlength="50" value="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 form-row">
                                                                        <div class="col-md-12"><label>บริษัท</label></div>
                                                                        <div class="col-md-12"><input type="text" class="textbox" name="txbWorkCompany2" id="txbWorkCompany2" ng-model="data.jobtxbWorkCompany2" maxlength="50" value=""></div>
                                                                    </div>

                                                                    <div class="col-md-6 form-row">
                                                                        <div class="col-md-12"><label>เงินเดือน (บาท) </label></div>
                                                                        <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkSalary2" id="txbWorkSalary2" maxlength="7" ng-model="data.jobtxbWorkSalary2"
                                                                            value="" min="0"></div>
                                                                        </div>
                                                                        <div class="col-md-6 form-row">
                                                                            <div class="col-md-12"><label>รายได้อื่น ๆ (บาท) </label></div>
                                                                            <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkIncome2" id="txbWorkIncome2" maxlength="7"
                                                                                ng-model="data.jobtxbWorkIncome2" min="0" value=""></div>
                                                                            </div>

                                                                            <div class="col-md-6 form-row">
                                                                                <div class="col-md-12"><label>ระบุรายได้อื่น ๆ</label></div>
                                                                                <div class="col-md-12"><input type="text" class="textbox w100-m"
                                                                                    name="txbWorkIncomeOther2" id="txbWorkIncomeOther2" ng-model="data.jobtxbWorkIncomeOther2" maxlength="100" value=""></div>
                                                                                </div>
                                                                                <div class="col-md-6 form-row">
                                                                                    <div class="col-md-12"><label>เหตุที่ลาออก</label></div>
                                                                                    <div class="col-md-12"><input type="text" class="textbox" name="txbWorkDetail2"
                                                                                        id="txbWorkDetail2" ng-model="data.jobtxbWorkDetail2">
                                                                                    </div>
                                                                                </div>

                                                                            </div>

                                                                        </div>

                                                                        <div id="tab3" style="display:none; border-bottom:none" class="hidden-xs hidden-sm">


                                                                            <div class="row">
                                                                                <div class="col-md-12 form-row">
                                                                                    <div class="col-md-12">3. ระยะเวลา</div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="row">
                                                                             <div class="col-md-6">
                                                                                <div class="col-md-12">ตั้งแต่</div>
                                                                                <div class="col-md-12">
                                                                                    <input type="text" name="cmbWorkFrom3" id="cmbWorkFrom3" ng-model="data.jobcmbWorkFrom3" class="hasDatepicker" readonly value="">
                                                                                    <span class="glyphicon glyphicon-calendar form-control-feedback"
                                                                                    id="iconworkFrom3" ></span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 form-row">
                                                                                <div class="col-md-12">ถึง</div>
                                                                                <div class="col-md-12">
                                                                                    <span class="cmbWorkTo3_test" style="
                                                                                    display: none;
                                                                                    top: 10px;
                                                                                    left: 28px;
                                                                                    background-color: #fff;
                                                                                    padding-right: 25px;
                                                                                    position: absolute;
                                                                                    ">ปัจจุบัน</span>
                                                                                    <input type="text" name="cmbWorkTo3" id="cmbWorkTo3" ng-model="data.jobcmbWorkTo3" class="hasDatepicker" readonly value="">
                                                                                    <input type="hidden" name="cmbWorkTo3_is" id="cmbWorkTo3_is">
                                                                                    <span class="glyphicon glyphicon-calendar form-control-feedback"
                                                                                    id="iconworkTo3" ></span>
                                                                                </div>
                                                                                <div class="col-md-12 form-row">
                                                                                    <label>หรือ</label> &nbsp; &nbsp;
                                                                                    <input type="checkbox" id="chWorkTo3" name="chWorkTo3">
                                                                                    <label for="chWorkTo3">
                                                                                        <span class="span-chWorkTo3" style="height: 19px; width: 19px;"></span> ปัจจุบัน </label>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6 form-row">
                                                                                    <div class="col-md-12"><label>ตำแหน่ง</label></div>
                                                                                    <div class="col-md-12">

                                                                                        <input type="text" class="textbox" name="txbWorkPosition3" id="txbWorkPosition3" ng-model="data.jobtxbWorkPosition3" maxlength="50" value="">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6 form-row">
                                                                                    <div class="col-md-12"><label>บริษัท</label></div>
                                                                                    <div class="col-md-12"><input type="text" class="textbox" name="txbWorkCompany3" id="txbWorkCompany3" ng-model="data.jobtxbWorkCompany3" maxlength="50" value=""></div>
                                                                                </div>

                                                                                <div class="col-md-6 form-row">
                                                                                    <div class="col-md-12"><label>เงินเดือน (บาท) </label></div>
                                                                                    <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkSalary3" id="txbWorkSalary3" maxlength="7" ng-model="data.jobtxbWorkSalary3"
                                                                                        value="" min="0"></div>
                                                                                    </div>
                                                                                    <div class="col-md-6 form-row">
                                                                                        <div class="col-md-12"><label>รายได้อื่น ๆ (บาท) </label></div>
                                                                                        <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkIncome3" id="txbWorkIncome3" maxlength="7"
                                                                                            ng-model="data.jobtxbWorkIncome3" min="0" value=""></div>
                                                                                        </div>

                                                                                        <div class="col-md-6 form-row">
                                                                                            <div class="col-md-12"><label>ระบุรายได้อื่น ๆ</label></div>
                                                                                            <div class="col-md-12"><input type="text" class="textbox w100-m"
                                                                                                name="txbWorkIncomeOther3" id="txbWorkIncomeOther3" ng-model="data.jobtxbWorkIncomeOther3" maxlength="100" value=""></div>
                                                                                            </div>
                                                                                            <div class="col-md-6 form-row">
                                                                                                <div class="col-md-12"><label>เหตุที่ลาออก</label></div>
                                                                                                <div class="col-md-12"><input type="text" class="textbox" name="txbWorkDetail3"
                                                                                                    id="txbWorkDetail3" ng-model="data.jobtxbWorkDetail3">
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>

                                                                                    </div>

                                                                                    <div id="tab4" style="display:none; border-bottom:none" class="hidden-xs hidden-sm">


                                                                                        <div class="row">
                                                                                            <div class="col-md-12 form-row">
                                                                                                <div class="col-md-12">4. ระยะเวลา</div>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="row">
                                                                                         <div class="col-md-6 form-row">
                                                                                            <div class="col-md-12">ตั้งแต่</div>
                                                                                            <div class="col-md-12">
                                                                                                <input type="text" name="cmbWorkFrom4" id="cmbWorkFrom4" ng-model="data.jobcmbWorkFrom4" class="hasDatepicker" readonly value="">
                                                                                                <span class="glyphicon glyphicon-calendar form-control-feedback"
                                                                                                id="iconworkFrom4" ></span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-6 form-row">
                                                                                            <div class="col-md-12">ถึง</div>
                                                                                            <div class="col-md-12">
                                                                                                <span class="cmbWorkTo4_test" style="
                                                                                                display: none;
                                                                                                top: 10px;
                                                                                                left: 28px;
                                                                                                background-color: #fff;
                                                                                                padding-right: 25px;
                                                                                                position: absolute;
                                                                                                ">ปัจจุบัน</span>
                                                                                                <input type="text" name="cmbWorkTo4" id="cmbWorkTo4" ng-model="data.jobcmbWorkTo4" class="hasDatepicker" readonly value="">
                                                                                                <input type="hidden" name="cmbWorkTo4_is" id="cmbWorkTo4_is">
                                                                                                <span class="glyphicon glyphicon-calendar form-control-feedback"
                                                                                                id="iconworkTo4" ></span>
                                                                                            </div>
                                                                                            <div class="col-md-12 form-row">
                                                                                                <label>หรือ</label> &nbsp; &nbsp;
                                                                                                <input type="checkbox" id="chWorkTo4" name="chWorkTo4">
                                                                                                <label for="chWorkTo4">
                                                                                                    <span class="span-chWorkTo4" style="height: 19px; width: 19px;"></span> ปัจจุบัน </label>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6 form-row">
                                                                                                <div class="col-md-12"><label>ตำแหน่ง</label></div>
                                                                                                <div class="col-md-12">

                                                                                                    <input type="text" class="textbox" name="txbWorkPosition4" id="txbWorkPosition4" ng-model="data.jobtxbWorkPosition4" maxlength="50" value="">
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6 form-row">
                                                                                                <div class="col-md-12"><label>บริษัท</label></div>
                                                                                                <div class="col-md-12"><input type="text" class="textbox" name="txbWorkCompany4" id="txbWorkCompany4" ng-model="data.jobtxbWorkCompany4" maxlength="50" value=""></div>
                                                                                            </div>

                                                                                            <div class="col-md-6 form-row">
                                                                                                <div class="col-md-12"><label>เงินเดือน (บาท) </label></div>
                                                                                                <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkSalary4" id="txbWorkSalary4" maxlength="7" ng-model="data.jobtxbWorkSalary4"
                                                                                                    value="" min="0"></div>
                                                                                                </div>
                                                                                                <div class="col-md-6 form-row">
                                                                                                    <div class="col-md-12"><label>รายได้อื่น ๆ (บาท) </label></div>
                                                                                                    <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkIncome4" id="txbWorkIncome4" maxlength="7"
                                                                                                        ng-model="data.jobtxbWorkIncome4" min="0" value=""></div>
                                                                                                    </div>

                                                                                                    <div class="col-md-6 form-row">
                                                                                                        <div class="col-md-12"><label>ระบุรายได้อื่น ๆ</label></div>
                                                                                                        <div class="col-md-12"><input type="text" class="textbox w100-m"
                                                                                                            name="txbWorkIncomeOther4" id="txbWorkIncomeOther4" ng-model="data.jobtxbWorkIncomeOther4" maxlength="100" value=""></div>
                                                                                                        </div>
                                                                                                        <div class="col-md-6 form-row">
                                                                                                            <div class="col-md-12"><label>เหตุที่ลาออก</label></div>
                                                                                                            <div class="col-md-12"><input type="text" class="textbox" name="txbWorkDetail4"
                                                                                                                id="txbWorkDetail4" ng-model="data.jobtxbWorkDetail4">
                                                                                                            </div>
                                                                                                        </div>

                                                                                                    </div>

                                                                                                </div>

                                                                                                <div id="tab5" style="display:none; border-bottom:none" class="hidden-xs hidden-sm">


                                                                                                    <div class="row">
                                                                                                        <div class="col-md-12 form-row">
                                                                                                            <div class="col-md-12">5. ระยะเวลา</div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="row">
                                                                                                     <div class="col-md-6 form-row">
                                                                                                        <div class="col-md-12">ตั้งแต่</div>
                                                                                                        <div class="col-md-12">
                                                                                                            <input type="text" name="cmbWorkFrom5" id="cmbWorkFrom5" ng-model="data.jobcmbWorkFrom5" class="hasDatepicker" readonly value="">
                                                                                                            <span class="glyphicon glyphicon-calendar form-control-feedback"
                                                                                                            id="iconworkFrom5" ></span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="col-md-6 form-row">
                                                                                                        <div class="col-md-12">ถึง</div>
                                                                                                        <div class="col-md-12">
                                                                                                            <span class="cmbWorkTo5_test" style="
                                                                                                            display: none;
                                                                                                            top: 10px;
                                                                                                            left: 28px;
                                                                                                            background-color: #fff;
                                                                                                            padding-right: 25px;
                                                                                                            position: absolute;
                                                                                                            ">ปัจจุบัน</span>
                                                                                                            <input type="text" name="cmbWorkTo5" id="cmbWorkTo5" ng-model="data.jobcmbWorkTo5" class="hasDatepicker" readonly value="">
                                                                                                            <input type="hidden" name="cmbWorkTo5_is" id="cmbWorkTo5_is">
                                                                                                            <span class="glyphicon glyphicon-calendar form-control-feedback"
                                                                                                            id="iconworkTo5" ></span>
                                                                                                        </div>
                                                                                                        <div class="col-md-12 form-row">
                                                                                                            <label>หรือ</label> &nbsp; &nbsp;
                                                                                                            <input type="checkbox" id="chWorkTo5" name="chWorkTo5">
                                                                                                            <label for="chWorkTo5">
                                                                                                                <span class="span-chWorkTo5" style="height: 19px; width: 19px;"></span> ปัจจุบัน </label>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-md-6 form-row">
                                                                                                            <div class="col-md-12"><label>ตำแหน่ง</label></div>
                                                                                                            <div class="col-md-12">

                                                                                                                <input type="text" class="textbox" name="txbWorkPosition5" id="txbWorkPosition5" ng-model="data.jobtxbWorkPosition5" maxlength="50" value="">
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-md-6 form-row">
                                                                                                            <div class="col-md-12"><label>บริษัท</label></div>
                                                                                                            <div class="col-md-12"><input type="text" class="textbox" name="txbWorkCompany5" id="txbWorkCompany5" ng-model="data.jobtxbWorkCompany5" maxlength="50" value=""></div>
                                                                                                        </div>

                                                                                                        <div class="col-md-6 form-row">
                                                                                                            <div class="col-md-12"><label>เงินเดือน (บาท) </label></div>
                                                                                                            <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkSalary5" id="txbWorkSalary5" maxlength="7" ng-model="data.jobtxbWorkSalary5"
                                                                                                                value="" min="0"></div>
                                                                                                            </div>
                                                                                                            <div class="col-md-6 form-row">
                                                                                                                <div class="col-md-12"><label>รายได้อื่น ๆ (บาท) </label></div>
                                                                                                                <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkIncome5" id="txbWorkIncome5" maxlength="7"
                                                                                                                    ng-model="data.jobtxbWorkIncome5" min="0" value=""></div>
                                                                                                                </div>

                                                                                                                <div class="col-md-6 form-row">
                                                                                                                    <div class="col-md-12"><label>ระบุรายได้อื่น ๆ</label></div>
                                                                                                                    <div class="col-md-12"><input type="text" class="textbox w100-m"
                                                                                                                        name="txbWorkIncomeOther5" id="txbWorkIncomeOther5" ng-model="data.jobtxbWorkIncomeOther5" maxlength="100" value=""></div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-6 form-row">
                                                                                                                        <div class="col-md-12"><label>เหตุที่ลาออก</label></div>
                                                                                                                        <div class="col-md-12"><input type="text" class="textbox" name="txbWorkDetail5"
                                                                                                                            id="txbWorkDetail5" ng-model="data.jobtxbWorkDetail5">
                                                                                                                        </div>
                                                                                                                    </div>

                                                                                                                </div>

                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="">
                                                                                                            <div class="form-title">ความสามารถด้านภาษา</div>
                                                                                                            <div class="language_form form-section">
                                                                                                                <div class="row hide-mobile">
                                                                                                                    <div class="row form-row">
                                                                                                                        <div class="col-md-6 form-row">
                                                                                                                            <div class="col-md-2">ภาษา</div>
                                                                                                                            <div class="col-md-10 col-sm-offset-1"></div>
                                                                                                                        </div>

                                                                                                                        <div class="col-md-6 form-row">
                                                                                                                            <div class="col-md-4">การพูด</div>
                                                                                                                            <div class="col-md-4">การเขียน</div>
                                                                                                                            <div class="col-md-4">การอ่าน</div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>

                                                                                                                <div class="row">
                                                                                                                    <div class="row form-row">
                                                                                                                        <div class="col-md-6">
                                                                                                                            <div class="show-mobile col-md-12"><span>ภาษา</span></div>
                                                                                                                            <div class="col-md-1">1.</div>
                                                                                                                            <div class="col-md-11">
                                                                                                                                <input type="text" class="textbox" name="txbLanguage1" id="txbLanguage1" maxlength="30"  value="" ng-model="data.jobtxbLanguage1"></div>
                                                                                                                            </div>

                                                                                                                            <div class="col-md-6 form-row">
                                                                                                                                <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การพูด</label></div>
                                                                                                                                <div class="col-md-4">
                                                                                                                                    <div class="select-block">
                                                                                                                                        <select name="cmbLanguageTalk1" id="cmbLanguageTalk1" ng-model="data.jobcmbLanguageTalk1">
                                                                                                                                            <option value=""disabled>เลือก</option>
                                                                                                                                            <option value="ดีมาก">ดีมาก</option>
                                                                                                                                            <option value="ดี">ดี</option>
                                                                                                                                            <option value="พอใช้">พอใช้</option>
                                                                                                                                        </select>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                                <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การเขียน</label></div>
                                                                                                                                <div class="col-md-4">
                                                                                                                                    <div class="select-block">
                                                                                                                                        <select name="cmbLanguageWrite1" id="cmbLanguageWrite1" ng-model="data.jobcmbLanguageWrite1">
                                                                                                                                            <option value="" disabled>เลือก</option>
                                                                                                                                            <option value="ดีมาก">ดีมาก</option>
                                                                                                                                            <option value="ดี">ดี</option>
                                                                                                                                            <option value="พอใช้">พอใช้</option>
                                                                                                                                        </select>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                                <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การอ่าน</label></div>
                                                                                                                                <div class="col-md-4">
                                                                                                                                    <div class="select-block">
                                                                                                                                        <select name="cmbLanguageRead1" id="cmbLanguageRead1" ng-model="data.jobcmbLanguageRead1">
                                                                                                                                            <option value="" disabled>เลือก</option>
                                                                                                                                            <option value="ดีมาก">ดีมาก</option>
                                                                                                                                            <option value="ดี">ดี</option>
                                                                                                                                            <option value="พอใช้">พอใช้</option>
                                                                                                                                        </select>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>

                                                                                                                    <div class="row">
                                                                                                                      <div class="row form-row">
                                                                                                                          <div class="col-md-6">
                                                                                                                            <div class="show-mobile col-md-12"><span>ภาษา</span></div>
                                                                                                                            <div class="col-md-1">2.</div>
                                                                                                                            <div class="col-md-11">
                                                                                                                                <input type="text" class="textbox" name="txbLanguage2" id="txbLanguage2" maxlength="30"  value="" ng-model="data.jobtxbLanguage2">
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="col-md-6 form-row">
                                                                                                                            <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การพูด</label></div>
                                                                                                                            <div class="col-md-4">
                                                                                                                                <div class="select-block">
                                                                                                                                    <select name="cmbLanguageTalk2" id="cmbLanguageTalk2" ng-model="data.jobcmbLanguageTalk2">
                                                                                                                                        <option value=""disabled>เลือก</option>
                                                                                                                                        <option value="ดีมาก">ดีมาก</option>
                                                                                                                                        <option value="ดี">ดี</option>
                                                                                                                                        <option value="พอใช้">พอใช้</option>
                                                                                                                                    </select>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การเขียน</label></div>
                                                                                                                            <div class="col-md-4">
                                                                                                                                <div class="select-block">
                                                                                                                                    <select name="cmbLanguageWrite2" id="cmbLanguageWrite2" ng-model="data.jobcmbLanguageWrite2">
                                                                                                                                        <option value=""disabled>เลือก</option>
                                                                                                                                        <option value="ดีมาก">ดีมาก</option>
                                                                                                                                        <option value="ดี">ดี</option>
                                                                                                                                        <option value="พอใช้">พอใช้</option>
                                                                                                                                    </select>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การอ่าน</label></div>
                                                                                                                            <div class="col-md-4">
                                                                                                                                <div class="select-block">
                                                                                                                                    <select name="cmbLanguageRead2" id="cmbLanguageRead2" ng-model="data.jobcmbLanguageRead2">
                                                                                                                                        <option value="" disabled>เลือก</option>
                                                                                                                                        <option value="ดีมาก">ดีมาก</option>
                                                                                                                                        <option value="ดี">ดี</option>
                                                                                                                                        <option value="พอใช้">พอใช้</option>
                                                                                                                                    </select>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>

                                                                                                                <div class="row">
                                                                                                                   <div class="row form-row">
                                                                                                                    <div class="col-md-6">
                                                                                                                        <div class="col-md-1"><span class="show-mobile">ภาษา</span>3.</div>
                                                                                                                        <div class="col-md-11"><input type="text" class="textbox"
                                                                                                                            ng-model="data.jobtxbLanguage3"
                                                                                                                            name="txbLanguage3" id="txbLanguage3" maxlength="30"  value=""></div>
                                                                                                                        </div>
                                                                                                                        <div class="col-md-6 form-row">
                                                                                                                            <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การพูด</label></div>
                                                                                                                            <div class="col-md-4">
                                                                                                                                <div class="select-block">
                                                                                                                                    <select name="cmbLanguageTalk3" id="cmbLanguageTalk3" ng-model="data.jobcmbLanguageTalk3">
                                                                                                                                        <option value="" disabled>เลือก</option>
                                                                                                                                        <option value="ดีมาก">ดีมาก</option>
                                                                                                                                        <option value="ดี">ดี</option>
                                                                                                                                        <option value="พอใช้">พอใช้</option>
                                                                                                                                    </select>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การเขียน</label></div>
                                                                                                                            <div class="col-md-4">
                                                                                                                                <div class="select-block">
                                                                                                                                    <select name="cmbLanguageWrite3" id="cmbLanguageWrite3" ng-model="data.jobcmbLanguageWrite3">
                                                                                                                                        <option value="" disabled>เลือก</option>
                                                                                                                                        <option value="ดีมาก">ดีมาก</option>
                                                                                                                                        <option value="ดี">ดี</option>
                                                                                                                                        <option value="พอใช้">พอใช้</option>
                                                                                                                                    </select>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การอ่าน</label></div>
                                                                                                                            <div class="col-md-4">
                                                                                                                                <div class="select-block">
                                                                                                                                    <select name="cmbLanguageRead3" id="cmbLanguageRead3" ng-model="data.jobcmbLanguageRead3">
                                                                                                                                        <option value=""disabled>เลือก</option>
                                                                                                                                        <option value="ดีมาก">ดีมาก</option>
                                                                                                                                        <option value="ดี">ดี</option>
                                                                                                                                        <option value="พอใช้">พอใช้</option>
                                                                                                                                    </select>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>

                                                                                                                <div class="row">
                                                                                                                   <div class="row form-row">
                                                                                                                    <div class="col-md-6">
                                                                                                                        <div class="col-md-1"><span class="show-mobile">ภาษา</span>4.</div>
                                                                                                                        <div class="col-md-11"><input type="text" class="textbox" name="txbLanguage4" id="txbLanguage4" ng-model="data.jobtxbLanguage4" maxlength="30"  value=""></div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-6 form-row">
                                                                                                                        <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การพูด</label></div>
                                                                                                                        <div class="col-md-4">
                                                                                                                            <div class="select-block">
                                                                                                                                <select name="cmbLanguageTalk4" id="cmbLanguageTalk4" ng-model="data.jobcmbLanguageTalk4">
                                                                                                                                    <option value=""disabled>เลือก</option>
                                                                                                                                    <option value="ดีมาก">ดีมาก</option>
                                                                                                                                    <option value="ดี">ดี</option>
                                                                                                                                    <option value="พอใช้">พอใช้</option>
                                                                                                                                </select>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การเขียน</label></div>
                                                                                                                        <div class="col-md-4">
                                                                                                                            <div class="select-block">
                                                                                                                                <select name="cmbLanguageWrite4" id="cmbLanguageWrite4" ng-model="data.jobcmbLanguageWrite4">
                                                                                                                                    <option value="" disabled>เลือก</option>
                                                                                                                                    <option value="ดีมาก">ดีมาก</option>
                                                                                                                                    <option value="ดี">ดี</option>
                                                                                                                                    <option value="พอใช้">พอใช้</option>
                                                                                                                                </select>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การอ่าน</label></div>
                                                                                                                        <div class="col-md-4">
                                                                                                                            <div class="select-block">
                                                                                                                                <select name="cmbLanguageRead4" id="cmbLanguageRead4" ng-model="data.jobcmbLanguageRead4" >
                                                                                                                                    <option value=""disabled>เลือก</option>
                                                                                                                                    <option value="ดีมาก">ดีมาก</option>
                                                                                                                                    <option value="ดี">ดี</option>
                                                                                                                                    <option value="พอใช้">พอใช้</option>
                                                                                                                                </select>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                            <div class="row">
                                                                                                               <div class="row form-row">
                                                                                                                <div class="col-md-6 col-xs-12 form-row">
                                                                                                                    <div class="col-md-2 col-sm-2 col-xs-12"> <label>พิมพ์ดีด </label></div>
                                                                                                                    <div class="col-md-4 col-sm-4 col-xs-6">
                                                                                                                       <input type="number" class="textbox" name="txbTypingThai" id="txbTypingThai" maxlength="2" style="width:40%" min="0" ng-model="data.jobtxbTypingThai" value=""> &nbsp; ไทย
                                                                                                                   </div>
                                                                                                                   <div class="col-md-4 col-sm-5 col-xs-6">
                                                                                                                       <input type="number" class="textbox" min="0" name="txbTypingEng" id="txbTypingEng" maxlength="2" style="width:40%" min="0" ng-model="data.jobtxbTypingEng" value=""> &nbsp; อังกฤษ
                                                                                                                   </div>
                                                                                                               </div>
                                                                                                           </div>
                                                                                                       </div>
                                                                                                   </div>
                                                                                                   <div class="form-title">ความสามารถทางคอมพิวเตอร์</div>
                                                                                                   <div class="form-section">
                                                                                                     <textarea name="txbProfile_com" id="txbFurtherInfo" class="w100-m"
                                                                                                     ng-model="data.jobProfile_com"  style="height:150px;"></textarea>
                                                                                                 </div>

                                                                                                 <div class="form-title">ลักษณะงานที่สนใจ</div>
                                                                                                 <div class="job_interesting form-section">
                                                                                                    <div class="row">
                                                                                                        <div class="col-md-6 form-row">
                                                                                                            <div class="col-md-12"><label>ตำแหน่งงาน<span class="f-required"> *</span></label></div>
                                                                                                            <div class="col-md-12">
                                                                                                                <input type="text" class="textbox" name="txbInterestPosition" id="txbInterestPosition"
                                                                                                                ng-model="data.jobtxbInterestPosition" maxlength="255" value="" readonly>
                                                                                                                <!--  <span ng-show="!jobform.txbInterestPosition.$pristine && jobform.txbInterestPosition.$error.required" style="color : red" > โปรดกรอกตำแหน่งงาน </span> -->
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-md-6 form-row">
                                                                                                            <div class="col-md-12"><label>ประเภทงาน</label></div>
                                                                                                            <div class="col-md-4 col-sm-6 col-xs-6">
                                                                                                                <input type="radio" name="rdoInterestPositionType" id="rdoInterestPositionTypeF" ng-model="data.jobrdoInterestPositionType" value="F" checked=""> <label for="rdoInterestPositionTypeF">
                                                                                                                    <span></span>Full Time</label></div>
                                                                                                                    <div class="col-md-4 col-sm-6 col-xs-6">
                                                                                                                        <input type="radio" name="rdoInterestPositionType" id="rdoInterestPositionTypeP" ng-model="data.jobrdoInterestPositionType" value="P">  <label for="rdoInterestPositionTypeP">
                                                                                                                            <span></span>Part Time</label></div>
                                                                                                                        </div>
                                                                                                                    </div>


                                                                                                                    <div class="row">
                                                                                                                        <div class="col-md-6 form-row">
                                                                                                                            <div class="col-md-12"><label>เงินเดือนที่คาดหวัง<span class="f-required"> *</span> (บาท)</label>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-12">
                                                                                                                                <input type="text" class="textbox" name="txbExpectSalary" id="txbExpectSalary"
                                                                                                                                ng-model="data.jobtxbExpectSalary" min="0" required>
                                                                                                                                <span ng-show="!jobform.txbExpectSalary.$pristine && jobform.txbExpectSalary.$error.required" style="color : red" >  โปรดกรอกเงินเดือนที่คาดหวัง </span>

                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="col-md-6 form-row">
                                                                                                                            <div class="col-md-12"><label>พร้อมเริ่มงานตั้งแต่วันที่<span class="f-required"> *</span></label></div>
                                                                                                                            <div class="col-md-12 form-group has-feedback">
                                                                                                                                <input type="text" name="dateWork" id="dateWork" ng-model="data.jobdateWork" readonly=""
                                                                                                                                value="" class="hasDatepicker" required>
                                                                                                                                <span class="glyphicon glyphicon-calendar form-control-feedback" id="iconstartdate" ></span>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>

                                                                                                                </div>
                                                                                                                <div class="form-title">บุคคลอ้างอิง (ไม่ใช่ญาติ หรือคนในครอบครัว)</div>
                                                                                                                <div class="ref_person form-section">
                                                                                                                    <div class="row">
                                                                                                                        <div class="col-md-6 form-row">
                                                                                                                            <div class="col-md-12"><label>ชื่อ-นามสกุล</label></div>
                                                                                                                            <div class="col-md-12"><input type="text" class="textbox" name="txbReferenceName" id="txbReferenceName" ng-model="data.jobtxbReferenceName" maxlength="50" value=""></div>
                                                                                                                        </div>
                                                                                                                        <div class="col-md-6 form-row">
                                                                                                                            <div class="col-md-12"><label>ตำแหน่ง</label></div>
                                                                                                                            <div class="col-md-12"><input type="text" class="textbox" name="txbReferencePosition" id="txbReferencePosition" ng-model="data.jobtxbReferencePosition" maxlength="50" value=""></div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="row">

                                                                                                                        <div class="col-md-6 form-row">
                                                                                                                            <div class="col-md-12"><label>บริษัท</label></div>
                                                                                                                            <div class="col-md-12"><input type="text" class="textbox" name="txbReferenceCompany" id="txbReferenceCompany" ng-model="data.jobtxbReferenceCompany" maxlength="50" value=""></div>
                                                                                                                        </div>
                                                                                                                        <div class="col-md-6 form-row">
                                                                                                                            <div class="col-md-12"><label>ความสัมพันธ์</label></div>
                                                                                                                            <div class="col-md-12"><input type="text" class="textbox" name="txbReferenceRelation" id="txbReferenceRelation" ng-model="data.jobtxbReferenceRelation" maxlength="50" value=""></div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="row">
                                                                                                                        <div class="col-md-6 form-row">
                                                                                                                            <div class="col-md-12"><label>เบอร์โทรติดต่อ</label></div>
                                                                                                                            <div class="col-md-12"><input type="text" class="textbox tel"
                                                                                                                                name="txbReferenceTel" id="txbReferenceTel"
                                                                                                                                ng-model="data.jobtxbReferenceTel"></div>

                                                                                                                                <span ng-show="!jobform.jobtxbReferenceTel.$pristine && jobform.jobtxbReferenceTel.$error.required" style="color : red" > โปรดกรอกเบอร์โทรศัพท์ที่สามารถติดต่อได้ </span>
                                                                                                                                <span ng-show="jobform.jobtxbReferenceTel.$error.minlength" style="color : red" id="ConTelMinRef" > กรุณากรอกเบอร์โทรเฉพาะหมายเลขเท่านั้น ไม่ต้องมีขีด (-) </span>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>

                                                                                                                <div class="row" align="center">
                                                                                                                    <div class="col-md-2"></div>
                                                                                                                    <div class="col-md-4 form-row">
                                                                                                                        <input type="button" name="previous" id="btnprevious1" class="previous action-button" value="ย้อนกลับ" />
                                                                                                                    </div>
                                                                                                                    <div class="col-md-4 form-row">
                                                                                                                     <input type="button" id="btnnext2" name="next" class="next action-button"
                                                                                                                     value="ต่อไป" />
                                                                                                                 </div>
                                                                                                             </div>
                                                                                                         </fieldset>

                                                                                                         <fieldset>

                                                                                                            <div class="form-title">ข้อมูลทั่วไป</div>
                                                                                                            <div class="common_info form-section">
                                                                                                                <div class="row">
                                                                                                                    <div class="col-md-12 form-row">
                                                                                                                        <div class="col-md-12">
                                                                                                                            <span>การไปปฏิบัติงาน ณ โครงการหมู่บ้านท่านขัดข้องหรือไม่<span class="f-required"> *</span></span>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>

                                                                                                                <div class="row">
                                                                                                                    <div class="col-md-12 form-row">
                                                                                                                        <div class="col-md-3"><label>เป็นประจำ</label></div>
                                                                                                                        <div class="col-md-2">
                                                                                                                            <input type="radio" name="rdoWorkSite" id="rdoWorkSiteY" ng-model=data.jobrdoWorkSite value="Y"
                                                                                                                            required> &nbsp; <label for="rdoWorkSiteY"><span></span>ไม่ขัดข้อง</label>
                                                                                                                        </div>
                                                                                                                        <div class="col-md-2">
                                                                                                                            <input type="radio" name="rdoWorkSite" id="rdoWorkSiteN" ng-model=data.jobrdoWorkSite value="N"  required> &nbsp; <label for="rdoWorkSiteN"><span></span>ขัดข้อง</label>
                                                                                                                        </div>
                                                                                                                        <div class="col-md-1"><label>โปรดระบุ</label></div>
                                                                                                                        <div class="col-md-4">
                                                                                                                            <input type="text" class="textbox w80-m" name="txbWorkSite"
                                                                                                                            ng-model="data.jobrdoWorkSiteNo" ng-disabled="data.jobrdoWorkSite!='N'"
                                                                                                                            id="txbWorkSite" maxlength="50">
                                                                                                                        </div>

                                                                                                                    </div>
                                                                                                                </div>

                                                                                                                <div class="row">
                                                                                                                    <div class="col-md-12 form-row">
                                                                                                                        <div class="col-md-3"><label>เป็นครั้งคราว</label></div>
                                                                                                                        <div class="col-md-2">
                                                                                                                            <input type="radio" name="rdoWorkSiteFew" id="rdoWorkSiteFewY" value="Y"
                                                                                                                            ng-model="data.jobrdoWorkSiteFew" required> &nbsp; <label for="rdoWorkSiteFewY"><span></span>ไม่ขัดข้อง</label>
                                                                                                                        </div>
                                                                                                                        <div class="col-md-2">
                                                                                                                            <input type="radio" name="rdoWorkSiteFew" id="rdoWorkSiteFewN" ng-model="data.jobrdoWorkSiteFew" value="N"  required> &nbsp; <label for="rdoWorkSiteFewN"><span></span>ขัดข้อง</label>
                                                                                                                        </div>
                                                                                                                        <div class="col-md-1"><label>โปรดระบุ</label></div>
                                                                                                                        <div class="col-md-4">
                                                                                                                            <input type="text" class="textbox w80-m" name="txbWorkSiteFew" id="txbWorkSiteFew" maxlength="50" ng-model="data.jobrdoWorkSiteFewNo" ng-disabled="data.jobrdoWorkSiteFew!='N'" value="">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="row">
                                                                                                                    <div class="col-md-12 form-row">
                                                                                                                        <div class="col-md-3"><label>ท่านมีโรคเเรื้อรัง/โรคประจำตัวหรือไม่<span class="f-required"> *</span></label></div>
                                                                                                                        <div class="col-md-2">
                                                                                                                            <input type="radio" name="rdoHasDisease" id="rdoHasDiseaseN" ng-model="data.jobrdoHasDisease"
                                                                                                                            value="N" required checked > &nbsp; <label for="rdoHasDiseaseN"><span></span>ไม่มี</label>
                                                                                                                        </div>
                                                                                                                        <div class="col-md-2">
                                                                                                                            <input type="radio" name="rdoHasDisease" id="rdoHasDiseaseY" ng-model="data.jobrdoHasDisease"
                                                                                                                            value="Y" required> &nbsp; <label for="rdoHasDiseaseY"><span></span>มี</label>
                                                                                                                        </div>
                                                                                                                        <div class="col-md-1"><label>โปรดระบุ</label></div>
                                                                                                                        <div class="col-md-4">
                                                                                                                            <input type="text" class="textbox w80-m" ng-model="data.jobrdoHasDiseaseYes" name="txbHasDisease" id="txbHasDisease" maxlength="50"
                                                                                                                            ng-disabled="data.jobrdoHasDisease!='Y'" value="">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="row">
                                                                                                                    <div class="col-md-12 form-row">
                                                                                                                        <div class="col-md-3"><label>ท่านเคยต้องโทษทางคดีอาญาหรือคดีแพ่งหรือไม่<span class="f-required"> *</span></label></div>
                                                                                                                        <div class="col-md-2">
                                                                                                                            <input type="radio" name="rdoHasAccuse" id="rdoHasAccuseN" value="N" ng-model="data.jobrdoHasAccuse"
                                                                                                                            required > &nbsp; <label for="rdoHasAccuseN"><span></span>ไม่เคย</label>
                                                                                                                        </div>
                                                                                                                        <div class="col-md-2">
                                                                                                                            <input type="radio" name="rdoHasAccuse" id="rdoHasAccuseY" value="Y" ng-model="data.jobrdoHasAccuse" required> &nbsp; <label for="rdoHasAccuseY"><span></span>เคย</label>
                                                                                                                        </div>
                                                                                                                        <div class="col-md-1"><label>โปรดระบุ</label></div>
                                                                                                                        <div class="col-md-4">
                                                                                                                            <input type="text" class="textbox w80-m" name="txbHasAccuse"
                                                                                                                            id="txbHasAccuse" maxlength="50" ng-model="data.jobrdoHasAccuseYes"
                                                                                                                            value="" ng-disabled="data.jobrdoHasAccuse!='Y'">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="row">
                                                                                                                    <div class="col-md-12 form-row">
                                                                                                                        <div class="col-md-3"><label>ท่านเคยถูกศาลสั่งให้เป็นบุคคล้มละลาย หรือถูกศาลสั่งพิทักษ์ทรัพย์
                                                                                                                            หรือมีหนี้สินล้นพ้นตัวหรือไม่<span class="f-required">*</span></label></div>
                                                                                                                            <div class="col-md-2">
                                                                                                                                <input type="radio" name="rdoHasBankrupt" id="rdoHasBankruptN" value="N"  ng-model="data.jobrdoHasBankrupt"required > &nbsp; <label for="rdoHasBankruptN"><span></span>ไม่เคย</label>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-2">
                                                                                                                                <input type="radio" name="rdoHasBankrupt" id="rdoHasBankruptY" value="Y" ng-model="data.jobrdoHasBankrupt" required> &nbsp; <label for="rdoHasBankruptY"><span></span>เคย</label>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-1"><label>โปรดระบุ</label></div>
                                                                                                                            <div class="col-md-4">
                                                                                                                                <input type="text" class="textbox w80-m" name="txbHasBankrupt" id="txbHasBankrupt" maxlength="50" value="" ng-model="data.jobrdoHasBankruptYes" ng-disabled="data.jobrdoHasBankrupt!='Y'">
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="row">
                                                                                                                        <div class="col-md-12 form-row">
                                                                                                                            <div class="col-md-3"><label>ท่านเคยมีประวัติถูกเลิกจ้างเพราะเหตุกระทำผิดมาก่อนหรือไม่<span class="f-required">*</span></label></div>
                                                                                                                            <div class="col-md-2">
                                                                                                                                <input type="radio" name="rdoHasLayoff" id="rdoHasLayoffN" ng-model="data.jobrdoHasLayoff" value="N"  required >
                                                                                                                                &nbsp; <label for="rdoHasLayoffN"><span></span>ไม่เคย</label>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-2">
                                                                                                                                <input type="radio" name="rdoHasLayoff" id="rdoHasLayoffY" ng-model="data.jobrdoHasLayoff" value="Y"  required > &nbsp; <label for="rdoHasLayoffY"><span></span>เคย</label>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-1"><label>โปรดระบุ</label></div>
                                                                                                                            <div class="col-md-4">
                                                                                                                                <input type="text" class="textbox w80-m" name="txbHasLayoff"  id="txbHasLayoff" maxlength="50" ng-model="data.jobrdoHasLayoffYes"
                                                                                                                                ng-disabled="data.jobrdoHasLayoff!='Y'">
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="form-title">ข้อมูลเพิ่มเติม</div>
                                                                                                                <div class="form-section">
                                                                                                                    <textarea name="txbFurtherInfo" id="txbFurtherInfo" class="w100-m"
                                                                                                                    ng-model="data.jobtxbFurtherInfo"  style="height:150px;"></textarea>
                                                                                                                </div>
                                                                                                                <div class="form-title">เอกสารแนบ</div>
                                                                                                                <div class="document_form">

                                                                                                                    <label style="font-size: 25px;">แนบ Resume</label><br>

                                                                                                                    <div class="row">
                                                                                                                     <input type="file"  name="flResume" ng-model="data.jobflResume" id="flResume"
                                                                                                                     class="file">
                                                                                                                     <div class="input-group col-md-12" style="padding-left: 10px; padding-right: 10px;">
                                                                                                                        <input type="text" class="form-control input-lg" disabled style="background: white;">
                                                                                                                        <span class="input-group-btn">
                                                                                                                            <button class="browse btn input-lg" type="button" style="border-radius: 1px;
                                                                                                                            background: rgba(111, 151, 85, 0.9); color: white;">
                                                                                                                            <span class="glyphicon glyphicon-folder-open"></span>&nbsp; &nbsp;  เลือกไฟล์ </button>

                                                                                                                        </div>
                                                                                                                        <span id="errresume"  style="color : red; display: none;">ขนาดของ File เกิน 1 MB.</span>
                                                                                                                        <div class="col-md-6">ขนาดของ File ไม่เกิน 1 MB. ในกรณีมีหลาย File ให้ Zip เป็น 1 File <br></div>
                                                                                                                    </div>


                                                                                                                    <label style="font-size: 25px;">แนบรูป</label> <br>
                                                                                                                    <div class="row" style="margin-bottom:25px;">
                                                                                                                        <input type="file"  name="flPicture" ng-model="data.jobflPicture" id="flPicture" class="file">
                                                                                                                        <div class="input-group col-sm-12" style="padding-left: 10px; padding-right: 10px;">
                                                                                                                            <input type="text" class="form-control input-lg" style="background: white;" disabled>
                                                                                                                            <span class="input-group-btn">
                                                                                                                                <button class="browse btn input-lg" type="button" style="border-radius: 1px;
                                                                                                                                background: rgba(111, 151, 85, 0.9); color: white;">
                                                                                                                                <span class="glyphicon glyphicon-folder-open"></span>&nbsp; &nbsp; เลือกไฟล์ </button>
                                                                                                                            </span>
                                                                                                                        </div>
                                                                                                                        <span id="errpic"  style="color : red; display: none;">โปรดอัพโหลดนามสกุล .jpg, .gif,.png หรือ .tif </span>
                                                                                                                        <span id="errsize"   style="color : red; display: none;">ขนาดของ File เกิน 500 KB. </span>
                                                                                                                        <div class="col-md-6">ขนาดของ File ไม่เกิน 500 KB. ในรูปแบบของ .jpg, .gif ,.png หรือ .tif </div>
                                                                                                                    </div>


                                                                                                                    <div class="row">
                                                                                                                        <div class="col-sm-12 form-row">

                                                                                                                            <input type="checkbox" id="chbAccept" name="chbAccept"
                                                                                                                            value="Y" ng-model="data.jobchbAccept" required>
                                                                                                                            <label for="chbAccept" style="font-size: 24px;"> <span style="margin-bottom:10px;"></span>ข้าพเจ้าขอรับรองว่า ข้อความทั้งหมดเป็นจริงทุกประการ การบิดเบือนหรือปิดบังข้อเท็จจริงใด ๆ ในใบสมัครนี้ ย่อมเป็นสาเหตุเพียงพอที่จะเลิกจ้างข้าพเจ้าได้ทันทีโดยไม่มีข้อแม้ใด ๆ ทั้งสิ้น</label>

                                                                                                                        </div>
                                                                                                                    </div><br>
                                                                                                                </div>
                                                                                                                <div class="row" align="center">
                                                                                                                    <div class="col-md-4 form-row">
                                                                                                                        <input type="button" name="previous" id="btnprevious2"
                                                                                                                        class="previous action-button" value="ย้อนกลับ" />
                                                                                                                    </div>
                                                                                                                    <div class="col-md-4 form-row">
                                                                                                                        <input type ="button" name="btnPreview" id="btnPreview" ng-disabled="jobform.$error.required || jobform.$error.minlength || jobform.$error.maxlength" value=" ดูใบสมัครก่อนส่ง">
                                                                                                                    </div>
                                                                                                                    <div  class="col-md-4 form-row">
                                                                                                                        <input type = "submit" name="btnSend" id="btnSend" value="ส่งใบสมัคร" ng-disabled="jobform.$error.required || jobform.$error.minlength || jobform.$error.maxlength"
                                                                                                                        ng-submit="myFunc()">
                                                                                                                    </div>

                                                                                                                </div>
                                                                                                                <br>
                                                                                                                <div class="row">
                                                                                                                    <div class="col-sm-12 form-row">
                                                                                                                        สอบถามข้อมูลการสมัครงาน ติดต่อ คุณยุพาวณี 02-230-8359
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                            <a id="splash_app" class="iframe" href="app_preview.jsp?id=2240" style="display:none">Splash</a>
                                                                                                            <div class="clear"></div>
                                                                                                        </fieldset>

                                                                                                    </form>


                                                                                                </div>
                                                                                            </div>


                                                                                            <!-- ใบสมัคร            -->

                                                                                            <div class="apply-job-container" style="display: none;" id="preview">
                                                                                                <div class="title" style="color: #6f9755; font-size: 33px;">ตัวอย่างใบสมัคร</div>
                                                                                                <p class="form-title">ข้อมูลส่วนตัว</p>
                                                                                                <div class="personal_info form-section">

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-3 form-row prehead">ชื่อ<span class="f-required"></span> </div>
                                                                                                        <div class="col-md-3 form-row textMobile"> {{ data.jobcmbTitle + "  " + data.jobtxbFName }} </div>
                                                                                                        <div class="col-md-3 form-row prehead">นามสกุล<span class="f-required"></span></div>
                                                                                                        <div class="col-md-3 form-row textMobile">{{ data.jobtxbLName }} </div>


                                                                                                    </div>

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-3 form-row prehead">ชื่อเล่น<span class="f-required"></span></div>
                                                                                                        <div class="col-md-3 form-row textMobile"> {{ data.jobtxbNickname }} </div>
                                                                                                        <div class="col-md-3 form-row prehead">เพศ<span class="f-required"></span></div>
                                                                                                        <div class="col-md-3 form-row textMobile"> {{ data.jobrdoGender == 'M' ? 'ชาย' : 'หญิง' }}  </div>
                                                                                                    </div>

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-3 form-row prehead">วันเกิด<span class="f-required"></span></div>
                                                                                                        <div class="col-md-3 form-row textMobile">{{ dateFormat(data.jobdateBirth)}}</div>
                                                                                                        <div class="col-md-3 form-row prehead">ภูมิลำเนา<span class="f-required"></span></div>
                                                                                                        <div class="col-md-3 form-row textMobile"> {{ mounts(data.jobcmbBirthProvince) }} </div>
                                                                                                    </div>

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-3 form-row prehead">ส่วนสูง<span class="f-required"> </span></div>
                                                                                                        <div class="col-md-3 form-row textMobile">{{ data.jobtxbHeight }} &nbsp; เซนติเมตร</div>
                                                                                                        <div class="col-md-3 form-row prehead"> น้ำหนัก <span class="f-required"> </span></div>
                                                                                                        <div class="col-md-3 form-row textMobile"> {{ data.jobtxbWeight }} &nbsp; กิโลกรัม </div>
                                                                                                    </div>

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-3 form-row prehead">ศาสนา<span class="f-required"></span></div>
                                                                                                        <div class="col-md-3 form-row textMobile">{{ data.jobcmbReligion }} </div>
                                                                                                        <div class="col-md-3 form-row prehead">  อื่น ๆ โปรดระบุ </div>
                                                                                                        <div class="col-md-3 form-row textMobile"> {{ data.txbReligionOther }} </div>
                                                                                                    </div>

                                                                                                    <div class="row">

                                                                                                        <div class="col-md-3 form-row prehead">เชื้อชาติ</div>
                                                                                                        <div class="col-md-3 form-row textMobile"> {{ data.jobrdoNationality + " " + data.txbNationalityOther }}</div>
                                                                                                        <div class="col-md-3 form-row prehead">สัญชาติ</div>
                                                                                                        <div class="col-md-3 form-row textMobile">{{ data.jobrdoRace + " " + data.txbRaceOther }} </div>
                                                                                                    </div>

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-3 form-row prehead">เกณฑ์ทหาร</div>
                                                                                                        <div class="col-md-3 form-row textMobile">{{ data.jobcmbConscription }} </div>
                                                                                                        <div class="col-md-3 form-row prehead">เลือกยกเว้น โปรดระบุเหตุผล</div>
                                                                                                        <div class="col-md-3 form-row textMobile">{{ data.txbConscription }}</div>
                                                                                                    </div>

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-3 form-row prehead">
                                                                                                            เลขบัตรประจำตัวประชาชน<span class="f-required"></span></div>
                                                                                                            <div class="col-md-9 form-row textMobile">{{ data.jobtxbIDNo }} </div>
                                                                                                        </div>



                                                                                                    </div>
                                                                                                    <!-- end section ข้อมูลส่วนตัว -->

                                                                                                    <div class="form-title">ข้อมูลครอบครัว</div>
                                                                                                    <div class="family_info form-section">
                                                                                                        <div class="row" style="margin-bottom: 10px;">
                                                                                                         <div class="col-md-2 form-row prehead">สถานภาพสมรส</div>
                                                                                                         <div class="col-md-10 form-row textMobile">
                                                                                                            {{ data.jobrdoMarried }}
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="row hide-mobile">
                                                                                                        <div class="col-md-2 form-row prehead">ครอบครัว </div>
                                                                                                        <div class="col-md-3 form-row prehead"> ชื่อ-นามสกุล </div>
                                                                                                        <div class="col-md-1 form-row prehead"> อายุ </div>
                                                                                                        <div class="col-md-3 form-row prehead"> อาชีพ/ตำแหน่ง </div>
                                                                                                        <div class="col-md-3 form-row prehead"> ที่อยู่/ที่ทำงาน </div>
                                                                                                    </div>

                                                                                                    <div class="row LangMobile">
                                                                                                        <div class="col-md-2 form-row prehead">บิดา</div>
                                                                                                        <div class="show-mobile col-md-4">ชื่อนาม-นามสกุล</div>
                                                                                                        <div class="col-md-3 form-row textMobile">{{ data.jobtxbFatherName }} </div>
                                                                                                        <div class="show-mobile col-md-4">อายุ</div>
                                                                                                        <div class="col-md-1 form-row textMobile">{{ data.jobtxbFatherAge == 0 ? '' : data.jobtxbFatherAge }}</div>
                                                                                                        <div class="show-mobile col-md-4">อาชีพ/ตำแหน่ง</div>
                                                                                                        <div class="col-md-3 form-row textMobile">{{ data.jobtxbFatherOcc }} </div>
                                                                                                        <div class="show-mobile col-md-4">ที่อยู่/ที่ทำงาน</div>
                                                                                                        <div class="col-md-3 form-row textMobile">{{ data.jobtxbFatherAddr }} </div>
                                                                                                    </div>

                                                                                                    <div class="row LangMobile">
                                                                                                        <div class="col-md-2 form-row prehead">มารดา</div>
                                                                                                        <div class="show-mobile col-md-4">ชื่อนาม-นามสกุล</div>
                                                                                                        <div class="col-md-3 form-row textMobile"> {{ data.jobtxbMotherName }} </div>
                                                                                                        <div class="show-mobile col-md-4">อายุ</div>
                                                                                                        <div class="col-md-1 form-row textMobile">{{ data.jobtxbMotherAge == 0 ? '' : data.jobtxbMotherAge }} </div>
                                                                                                        <div class="show-mobile col-md-4">อาชีพ/ตำแหน่ง</div>
                                                                                                        <div class="col-md-3 form-row textMobile">{{ data.jobtxbMotherOcc }} </div>
                                                                                                        <div class="show-mobile col-md-4">ที่อยู่/ที่ทำงาน</div>
                                                                                                        <div class="col-md-3 form-row textMobile">{{ data.jobtxbMotherAddr }} </div>
                                                                                                    </div>
                                                                                                    <div class="row LangMobile">
                                                                                                        <div class="col-md-2 form-row prehead">พี่น้อง 1</div>
                                                                                                        <div class="show-mobile col-md-4">ชื่อนาม-นามสกุล</div>
                                                                                                        <div class="col-md-3 form-row textMobile"> {{ data.jobtxbCousin1Name }} </div>
                                                                                                        <div class="show-mobile col-md-4">อายุ</div>
                                                                                                        <div class="col-md-1 form-row textMobile">{{ data.jobtxbCousin1Age == 0 ? '' : data.jobtxbCousin1Age }} </div>
                                                                                                        <div class="show-mobile col-md-4">อาชีพ/ตำแหน่ง</div>
                                                                                                        <div class="col-md-3 form-row textMobile"> {{ data.jobtxbCousin1Occ }} </div>
                                                                                                        <div class="show-mobile col-md-4">ที่อยู่/ที่ทำงาน</div>
                                                                                                        <div class="col-md-3 form-row textMobile">{{ data.jobtxbCousin1Addr }} </div>
                                                                                                    </div>
                                                                                                    <div class="row LangMobile">
                                                                                                        <div class="col-md-2 form-row prehead">พี่น้อง 2</div>
                                                                                                        <div class="show-mobile col-md-4">ชื่อนาม-นามสกุล</div>
                                                                                                        <div class="col-md-3 form-row textMobile">{{ data.jobtxbCousin2Name }} </div>
                                                                                                        <div class="show-mobile col-md-4">อายุ</div>
                                                                                                        <div class="col-md-1 form-row textMobile">{{ data.jobtxbCousin2Age == 0 ? '' : data.jobtxbCousin2Age  }} </div>
                                                                                                        <div class="show-mobile col-md-4">อาชีพ/ตำแหน่ง</div>
                                                                                                        <div class="col-md-3 form-row textMobile">{{ data.jobtxbCousin2Occ }} </div>
                                                                                                        <div class="show-mobile col-md-4">ที่อยู่/ที่ทำงาน</div>
                                                                                                        <div class="col-md-3 form-row textMobile"> {{ data.jobtxbCousin2Addr }} </div>
                                                                                                    </div>

                                                                                                    <div class="row LangMobile">
                                                                                                        <div class="col-md-2 form-row prehead">พี่น้อง 3</div>
                                                                                                        <div class="show-mobile col-md-4">ชื่อนาม-นามสกุล</div>
                                                                                                        <div class="col-md-3 form-row textMobile"> {{ data.jobtxbCousin3Name }} </div>
                                                                                                        <div class="show-mobile col-md-4">อายุ</div>
                                                                                                        <div class="col-md-1 form-row textMobile"> {{ data.jobtxbCousin3Age == 0 ? '' : data.jobtxbCousin3Age  }} </div>
                                                                                                        <div class="show-mobile col-md-4">อาชีพ/ตำแหน่ง</div>
                                                                                                        <div class="col-md-3 form-row textMobile"> {{ data.jobtxbCousin3Occ }} </div>
                                                                                                        <div class="show-mobile col-md-4">ที่อยู่/ที่ทำงาน</div>
                                                                                                        <div class="col-md-3 form-row textMobile">{{ data.jobtxbCousin3Addr }}</div>
                                                                                                    </div>

                                                                                                    <div class="row LangMobile">
                                                                                                        <div class="col-md-2 form-row prehead">คู่สมรส</div>
                                                                                                        <div class="show-mobile col-md-3">ชื่อนาม-นามสกุล</div>
                                                                                                        <div class="col-md-3 form-row textMobile"> {{ data.jobtxbSpouseName }} </div>
                                                                                                        <div class="show-mobile col-md-3">อายุ</div>
                                                                                                        <div class="col-md-1 form-row textMobile"> {{ data.jobtxbSpouseAge == 0 ? '' : data.jobtxbSpouseAge }} </div>
                                                                                                        <div class="show-mobile col-md-3">อาชีพ/ตำแหน่ง</div>
                                                                                                        <div class="col-md-3 form-row textMobile"> {{ data.jobtxbSpouseOcc }} </div>
                                                                                                        <div class="show-mobile col-md-3">ที่อยู่/ที่ทำงาน</div>
                                                                                                        <div class="col-md-3 form-row textMobile"> {{ data.jobtxbSpouseAddr }} </div>
                                                                                                    </div>

                                                                                                    <div class="row">

                                                                                                        <div class="col-md-2 form-row prehead">จำนวนบุตร (คน) </div>
                                                                                                        <div class="col-md-10 form-row">{{ data.jobtxbChildNum == 0 ? '' : data.jobtxbChildNum }} </div>
                                                                                                        <!--    <div class="col-md-7 form-row"><label style="opacity: 0.0;"> - </label></div> -->

                                                                                                    </div>
                                                                                                </div>
                                                                                                <!-- end section family -->

                                                                                                <div class="form-title">ข้อมูลการติดต่อ</div>
                                                                                                <div class="contact_info form-section">
                                                                                                    <div class="row">
                                                                                                        <div class="col-md-12 title form-row" style="font-size:22px;">ที่อยู่ปัจจุบันที่ติดต่อได้</div>
                                                                                                    </div>

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-3 form-row prehead">ที่อยู่<span class="f-required"></span></div>
                                                                                                        <div class="col-md-3 form-row textMobile"> {{ data.jobtxbAddress }}</div>
                                                                                                        <div class="col-md-3 form-row prehead">จังหวัด<span class="f-required"></span></div>
                                                                                                        <div class="col-md-3 form-row textMobile">{{ mounts(data.jobcmbContactProvince) }} </div>

                                                                                                    </div>

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-3 form-row prehead">เบอร์โทรศัพท์ที่สามารถติดต่อได้<span class="f-required"></span></div>
                                                                                                        <div class="col-md-3 form-row textMobile">(+66) {{ data.jobtxbContactTel }}</div>
                                                                                                        <div class="col-md-3 form-row prehead">E-mail</div>
                                                                                                        <div class="col-md-3 form-row textMobile">{{ data.jobtxbContactEmail }}</div>
                                                                                                    </div>

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-12 title form-row" style="font-size:22px;">
                                                                                                            บุคคลที่ติดต่อได้ในกรณีฉุกเฉิน
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-3 form-row prehead">ชื่อ<span class="f-required"></span></div>
                                                                                                        <div class="col-md-3 form-row textMobile">{{ data.jobtxbEmergencyPerson }}</div>
                                                                                                        <div class="col-md-3 form-row prehead">เบอร์โทรศัพท์<span class="f-required"></span></div>
                                                                                                        <div class="col-md-3 form-row textMobile">(+66){{ data.jobtxbEmergencyTel }}</div>
                                                                                                    </div>

                                                                                                </div>
                                                                                                <!-- end section contact -->

                                                                                                <div class="form-title">ข้อมูลการศึกษา</div>
                                                                                                <div class="education_info form-section">

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-3 form-row prehead">การศึกษาสูงสุด</div>
                                                                                                        <div class="col-md-9 form-row textMobile" >{{ edu_ifvalue(data.jobcmbHighestGraduate) }} </div>
                                                                                                    </div><br>
                                                                                                    <h4>ประวัติการศึกษา</h4>

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-3 form-row prehead">1. ระดับการศึกษา</div>
                                                                                                        <div class="col-md-3 form-row textMobile"> {{ edu_ifvalue(data.jobcmbGraduateGraduate1) }} </div>
                                                                                                        <div class="col-md-3 form-row prehead">สถาบันการศึกษา</div>
                                                                                                        <div class="col-md-3 form-row textMobile">{{ data.jobtxbGraduateInstitute1 }}</div>
                                                                                                    </div>
                                                                                                    <div class="row">
                                                                                                        <div class="col-md-3 form-row prehead">คณะ</div>
                                                                                                        <div class="col-md-3 form-row textMobile">{{ data.jobtxbGraduateFaculty1 }}</div>
                                                                                                        <div class="col-md-3 form-row prehead">กลุ่มสาขาวิชา</div>
                                                                                                        <div class="col-md-3 form-row textMobile">{{ data.jobtxbGraduateField1 }}</div>
                                                                                                    </div>
                                                                                                    <div class="row">
                                                                                                        <div class="col-md-3 form-row prehead">ระยะเวลา   ตั้งแต่</div>
                                                                                                        <div class="col-md-3 form-row textMobile">{{ dateFormatM(data.jobcmbStudyFrom1) }} </div>
                                                                                                        <div class="col-md-3 form-row prehead"> ถึง </div>
                                                                                                        <div class="col-md-3 form-row textMobile">  {{ dateFormatM(data.jobcmbStudyTo1) }} </div>
                                                                                                    </div>

                                                                                                    <div class="row" style="margin-bottom: 10px;">
                                                                                                        <div class="col-md-3 form-row prehead">เกรดเฉลี่ย</div>
                                                                                                        <div class="col-md-9 form-row textMobile">{{ data.jobtxbGraduatePoint1 == 0 ? '' : data.jobtxbGraduatePoint1 }}</div>
                                                                                                    </div>


                                                                                                    <div ng-if="data.jobcmbStudyFrom2 != ''">
                                                                                                        <div class="row">
                                                                                                            <div class="col-md-3 form-row prehead">2. ระดับการศึกษา</div>
                                                                                                            <div class="col-md-3 form-row textMobile"> {{ edu_ifvalue(data.jobcmbGraduateGraduate2) }} </div>
                                                                                                            <div class="col-md-3 form-row prehead">สถาบันการศึกษา</div>
                                                                                                            <div class="col-md-3 form-row textMobile">{{ data.jobtxbGraduateInstitute2 }}</div>
                                                                                                        </div>
                                                                                                        <div class="row">
                                                                                                            <div class="col-md-3 form-row prehead">คณะ</div>
                                                                                                            <div class="col-md-3 form-row textMobile">{{ data.jobtxbGraduateFaculty2 }}</div>
                                                                                                            <div class="col-md-3 form-row prehead">กลุ่มสาขาวิชา</div>
                                                                                                            <div class="col-md-3 form-row textMobile">{{ data.jobtxbGraduateField2 }}</div>
                                                                                                        </div>
                                                                                                        <div class="row">
                                                                                                            <div class="col-md-3 form-row prehead">ระยะเวลา   ตั้งแต่</div>
                                                                                                            <div class="col-md-3 form-row textMobile">{{ dateFormatM(data.jobcmbStudyFrom2) }} </div>
                                                                                                            <div class="col-md-3 form-row prehead"> ถึง </div>
                                                                                                            <div class="col-md-3 form-row textMobile">  {{ dateFormatM(data.jobcmbStudyTo2) }} </div>
                                                                                                        </div>

                                                                                                        <div class="row" style="margin-bottom: 10px;">
                                                                                                            <div class="col-md-3 form-row prehead">เกรดเฉลี่ย</div>
                                                                                                            <div class="col-md-9 form-row textMobile">{{ data.jobtxbGraduatePoint2 == 0 ? '' : data.jobtxbGraduatePoint2 }}</div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div ng-if="data.jobcmbStudyFrom3 != ''">
                                                                                                        <div class="row">
                                                                                                            <div class="col-md-3 form-row prehead">3. ระดับการศึกษา</div>
                                                                                                            <div class="col-md-3 form-row textMobile"> {{ edu_ifvalue(data.jobcmbGraduateGraduate3) }} </div>
                                                                                                            <div class="col-md-3 form-row prehead">สถาบันการศึกษา</div>
                                                                                                            <div class="col-md-3 form-row textMobile">{{ data.jobtxbGraduateInstitute3 }}</div>
                                                                                                        </div>
                                                                                                        <div class="row">
                                                                                                            <div class="col-md-3 form-row prehead">คณะ</div>
                                                                                                            <div class="col-md-3 form-row textMobile">{{ data.jobtxbGraduateFaculty3 }}</div>
                                                                                                            <div class="col-md-3 form-row prehead">กลุ่มสาขาวิชา</div>
                                                                                                            <div class="col-md-3 form-row textMobile">{{ data.jobtxbGraduateField3 }}</div>
                                                                                                        </div>
                                                                                                        <div class="row">
                                                                                                            <div class="col-md-3 form-row prehead">ระยะเวลา   ตั้งแต่</div>
                                                                                                            <div class="col-md-3 form-row textMobile">{{ dateFormatM(data.jobcmbStudyFrom3) }} </div>
                                                                                                            <div class="col-md-3 form-row prehead"> ถึง </div>
                                                                                                            <div class="col-md-3 form-row textMobile">  {{ dateFormatM(data.jobcmbStudyTo3) }} </div>
                                                                                                        </div>

                                                                                                        <div class="row" style="margin-bottom: 10px;">
                                                                                                            <div class="col-md-3 form-row prehead">เกรดเฉลี่ย</div>
                                                                                                            <div class="col-md-9 form-row textMobile">{{ data.jobtxbGraduatePoint3 == 0 ? '' : data.jobtxbGraduatePoint3 }}</div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <!-- end section study -->

                                                                                                <div class="form-title">ข้อมูลการทำงาน</div>
                                                                                                <div class="experience_info form-section">
                                                                                                   <div class="row">
                                                                                                    <div class="col-md-3 form-row prehead">เพิ่งสำเร็จการศึกษา</div>
                                                                                                    <div class="col-md-3 form-row textMobile"> {{ data.jobrdoJustGraduate }}</div>
                                                                                                    <div class="col-md-3 form-row prehead">ประสบการณ์การทำงาน</div>
                                                                                                    <div class="col-md-3 form-row textMobile"> {{ data.jobtxbWorkExperience == 0 ? 'ไม่มี' : data.jobtxbWorkExperience+'&nbsp; ปี'}} </div>
                                                                                                </div>
                                                                                                <div class="row">
                                                                                                    <div class="col-md-3 form-row prehead"> สายงานล่าสุด</div>
                                                                                                    <div class="col-md-3 form-row textMobile"> {{ data.jobcmbLastWorkField  }}</div>
                                                                                                    <div class="col-md-3 form-row prehead">ตำแหน่งสุดท้าย</div>
                                                                                                    <div class="col-md-3 form-row textMobile"> {{  data.jobtxbLastPosition }} </div>
                                                                                                </div>

                                                                                                <div class="row">
                                                                                                    <div class="col-md-3 form-row prehead">เงินเดือนสุดท้าย</div>
                                                                                                    <div class="col-md-9 form-row textMobile"> {{ data.jobtxbLastSalary }} &nbsp; บาท</div>
                                                                                                </div><br>


                                                                                                <h4>ประวัติการทำงาน</h4>
                                                                                                <div class="row">
                                                                                                    <div class="col-md-3 form-row prehead"> 1. ระยะเวลา ตั้งแต่ </div>
                                                                                                    <div class="col-md-3 form-row textMobile"> {{  dateFormatM(data.jobcmbWorkFrom1) }}</div>
                                                                                                    <div class="col-md-3 form-row prehead"> ถึง </div>
                                                                                                    <div class="col-md-3 form-row textMobile" id="rechWorkTo1"> {{ dateFormatM(data.jobcmbWorkTo1) }} </div>
                                                                                                </div>
                                                                                                <div class="row">
                                                                                                    <div class="col-md-3 form-row prehead">ตำแหน่ง</div>
                                                                                                    <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkPosition1 }}</div>
                                                                                                    <div class="col-md-3 form-row prehead">บริษัท</div>
                                                                                                    <div class="col-md-3 form-row textMobile">{{  data.jobtxbWorkCompany1 }}</div>
                                                                                                </div>

                                                                                                <div class="row">
                                                                                                   <div class="col-md-3 form-row prehead">เงินเดือน</div>
                                                                                                   <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkSalary1 }}  &nbsp; บาท</div>
                                                                                                   <div class="col-md-3 form-row prehead">รายได้อื่น ๆ</div>
                                                                                                   <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkIncome1 }} &nbsp; บาท</div>
                                                                                               </div>

                                                                                               <div class="row" style="margin-bottom: 10px;">
                                                                                                   <div class="col-md-3 form-row prehead">ระบุรายได้อื่น ๆ</div>
                                                                                                   <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkIncomeOther1}}</div>
                                                                                                   <div class="col-md-3 form-row prehead">เหตุที่ลาออก</div>
                                                                                                   <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkDetail1 }}</div>
                                                                                               </div>

                                                                                               <div ng-if="data.jobcmbWorkFrom2 != '' ">
                                                                                                <div class="row" ng-if="data.jobcmbWorkFrom2">
                                                                                                    <div class="col-md-3 form-row prehead"> 2. ระยะเวลา ตั้งแต่ </div>
                                                                                                    <div class="col-md-3 form-row textMobile"> {{ dateFormatM(data.jobcmbWorkFrom2) }}</div>
                                                                                                    <div class="col-md-3 form-row prehead"> ถึง </div>
                                                                                                    <div class="col-md-3 form-row textMobile" id="rechWorkTo2"> {{ dateFormatM(data.jobcmbWorkTo2) }} </div>
                                                                                                </div>

                                                                                                <div class="row">
                                                                                                    <div class="col-md-3 form-row prehead">ตำแหน่ง</div>
                                                                                                    <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkPosition2 }}</div>
                                                                                                    <div class="col-md-3 form-row prehead">บริษัท</div>
                                                                                                    <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkCompany2 }}</div>

                                                                                                </div>

                                                                                                <div class="row">
                                                                                                    <div class="col-md-3 form-row prehead">เงินเดือน</div>
                                                                                                    <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkSalary2 }}  &nbsp; บาท</div>
                                                                                                    <div class="col-md-3 form-row prehead">รายได้อื่น ๆ</div>
                                                                                                    <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkIncome2 }} &nbsp; บาท</div>
                                                                                                </div>

                                                                                                <div class="row" style="margin-bottom: 10px;">
                                                                                                    <div class="col-md-3 form-row prehead">ระบุรายได้อื่น ๆ</div>
                                                                                                    <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkIncomeOther2 }}</div>
                                                                                                    <div class="col-md-3 form-row prehead">เหตุที่ลาออก</div>
                                                                                                    <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkDetail2 }}</div>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div ng-if="data.jobcmbWorkFrom3 != '' ">
                                                                                                <div class="row">
                                                                                                    <div class="col-md-3 form-row prehead"> 3. ระยะเวลา ตั้งแต่ </div>
                                                                                                    <div class="col-md-3 form-row textMobile"> {{ dateFormatM(data.jobcmbWorkFrom3) }}</div>
                                                                                                    <div class="col-md-3 form-row prehead"> ถึง </div>
                                                                                                    <div class="col-md-3 form-row textMobile" id="rechWorkTo3"> {{ dateFormatM(data.jobcmbWorkTo3) }} </div>
                                                                                                </div>

                                                                                                <div class="row">
                                                                                                    <div class="col-md-3 form-row prehead">ตำแหน่ง</div>
                                                                                                    <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkPosition3 }} </div>
                                                                                                    <div class="col-md-3 form-row prehead">บริษัท</div>
                                                                                                    <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkCompany3 }}</div>
                                                                                                </div>

                                                                                                <div class="row">
                                                                                                    <div class="col-md-3 form-row prehead">เงินเดือน</div>
                                                                                                    <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkSalary3 }}  &nbsp; บาท</div>
                                                                                                    <div class="col-md-3 form-row prehead">รายได้อื่น ๆ</div>
                                                                                                    <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkIncome3 }} &nbsp; บาท</div>
                                                                                                </div>

                                                                                                <div class="row" style="margin-bottom: 10px;">
                                                                                                    <div class="col-md-3 form-row prehead">ระบุรายได้อื่น ๆ</div>
                                                                                                    <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkIncomeOther3 }}</div>
                                                                                                    <div class="col-md-3 form-row prehead">เหตุที่ลาออก</div>
                                                                                                    <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkDetail3 }}</div>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div ng-if="data.jobcmbWorkFrom4 != '' ">
                                                                                             <div class="row">
                                                                                                <div class="col-md-3 form-row prehead">4. ระยะเวลา ตั้งแต่ </div>
                                                                                                <div class="col-md-3 form-row textMobile"> {{ dateFormatM(data.jobcmbWorkFrom4) }}</div>
                                                                                                <div class="col-md-3 form-row prehead"> ถึง </div>
                                                                                                <div class="col-md-3 form-row textMobile" id="rechWorkTo4"> {{ dateFormatM(data.jobcmbWorkTo4) }} </div>
                                                                                            </div>

                                                                                            <div class="row">
                                                                                                <div class="col-md-3 form-row prehead">ตำแหน่ง</div>
                                                                                                <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkPosition4 }} </div>
                                                                                                <div class="col-md-3 form-row prehead">บริษัท</div>
                                                                                                <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkCompany4 }}</div>

                                                                                            </div>

                                                                                            <div class="row">
                                                                                                <div class="col-md-3 form-row prehead">เงินเดือน</div>
                                                                                                <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkSalary4 }}  &nbsp; บาท</div>
                                                                                                <div class="col-md-3 form-row prehead">รายได้อื่น ๆ</div>
                                                                                                <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkIncome4 }} &nbsp; บาท</div>
                                                                                            </div>

                                                                                            <div class="row" style="margin-bottom: 10px;">
                                                                                                <div class="col-md-3 form-row prehead">ระบุรายได้อื่น ๆ</div>
                                                                                                <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkIncomeOther4 }}</div>
                                                                                                <div class="col-md-3 form-row prehead">เหตุที่ลาออก</div>
                                                                                                <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkDetail4 }}</div>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div ng-if="data.jobcmbWorkFrom5 != '' ">
                                                                                            <div class="row">
                                                                                                <div class="col-md-3 form-row prehead">5. ระยะเวลา ตั้งแต่ </div>
                                                                                                <div class="col-md-3 form-row textMobile"> {{ dateFormatM(data.jobcmbWorkFrom5) }}</div>
                                                                                                <div class="col-md-3 form-row prehead"> ถึง </div>
                                                                                                <div class="col-md-3 form-row textMobile" id="rechWorkTo5"> {{ dateFormatM(data.jobcmbWorkTo5)}} </div>
                                                                                            </div>

                                                                                            <div class="row">
                                                                                                <div class="col-md-3 form-row prehead">ตำแหน่ง</div>
                                                                                                <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkPosition5 }}</div>
                                                                                                <div class="col-md-3 form-row prehead">บริษัท</div>
                                                                                                <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkCompany5 }}</div>
                                                                                            </div>

                                                                                            <div class="row">
                                                                                                <div class="col-md-3 form-row prehead">เงินเดือน</div>
                                                                                                <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkSalary5 }}  &nbsp; บาท</div>
                                                                                                <div class="col-md-3 form-row prehead">รายได้อื่น ๆ</div>
                                                                                                <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkIncome5 }} &nbsp; บาท</div>
                                                                                            </div>

                                                                                            <div class="row">
                                                                                                <div class="col-md-3 form-row prehead">ระบุรายได้อื่น ๆ</div>
                                                                                                <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkIncomeOther5 }}</div>
                                                                                                <div class="col-md-3 form-row prehead">เหตุที่ลาออก</div>
                                                                                                <div class="col-md-3 form-row textMobile">{{ data.jobtxbWorkDetail5 }}</div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- end section work -->

                                                                                        <div class="form-title">ความสามารถด้านภาษา</div>
                                                                                        <div class="language_form form-section">

                                                                                            <div class="row hide-mobile prehead">
                                                                                                <div class="col-md-6 form-row">ภาษา</div>
                                                                                                <div class="col-md-2 form-row">การพูด</div>
                                                                                                <div class="col-md-2 form-row">การเขียน</div>
                                                                                                <div class="col-md-2 form-row">การอ่าน</div>
                                                                                            </div>

                                                                                            <div class="row LangMobile">

                                                                                                <div class="col-md-3 show-mobile form-row prehead">ภาษาที่  1</div>
                                                                                                <div class="col-md-3 form-row show-mobile textMobile">{{ data.jobtxbLanguage1 }}</div>
                                                                                                <div class="col-md-6 form-row hide-mobile"> 1. &nbsp; {{ data.jobtxbLanguage1 }}</div>
                                                                                                <div class="col-md-3 show-mobile form-row prehead">การพูด</div>
                                                                                                <div class="col-md-2 form-row textMobile">{{ data.jobcmbLanguageTalk1 }}</div>
                                                                                                <div class="col-md-3 show-mobile form-row prehead">การเขียน</div>
                                                                                                <div class="col-md-2 form-row textMobile">{{ data.jobcmbLanguageWrite1 }}</div>
                                                                                                <div class="col-md-3 show-mobile form-row prehead">การอ่าน</div>
                                                                                                <div class="col-md-2 form-row textMobile">{{ data.jobcmbLanguageRead1 }}</div>
                                                                                            </div>

                                                                                            <div class="row LangMobile">
                                                                                                <div class="col-md-3 show-mobile form-row prehead">ภาษาที่ 2 </div>
                                                                                                <div class="col-md-3 form-row show-mobile textMobile">{{ data.jobtxbLanguage2 }}</div>
                                                                                                <div class="col-md-6 form-row hide-mobile">2. &nbsp; {{ data.jobtxbLanguage2 }}</div>
                                                                                                <div class="col-md-3 show-mobile form-row prehead">การพูด</div>
                                                                                                <div class="col-md-2 form-row textMobile">{{ data.jobcmbLanguageTalk2 }}</div>
                                                                                                <div class="col-md-3 show-mobile form-row prehead">การเขียน</div>
                                                                                                <div class="col-md-2 form-row textMobile">{{ data.jobcmbLanguageWrite2 }}</div>
                                                                                                <div class="col-md-3 show-mobile form-row prehead">การอ่าน</div>
                                                                                                <div class="col-md-2 form-row textMobile">{{ data.jobcmbLanguageRead2 }}</div>
                                                                                            </div>

                                                                                            <div class="row LangMobile">
                                                                                                <div class="col-md-3 show-mobile form-row prehead">ภาษาที่ 3</div>
                                                                                                <div class="col-md-3 form-row show-mobile textMobile">{{ data.jobtxbLanguage3 }}</div>
                                                                                                <div class="col-md-6 form-row hide-mobile">3. &nbsp; {{ data.jobtxbLanguage3 }}</div>
                                                                                                <div class="col-md-3 show-mobile form-row prehead">การพูด</div>
                                                                                                <div class="col-md-2 form-row textMobile">{{ data.jobcmbLanguageTalk3 }}</div>
                                                                                                <div class="col-md-3 show-mobile form-row prehead">การเขียน</div>
                                                                                                <div class="col-md-2 form-row textMobile">{{ data.jobcmbLanguageWrite3 }}</div>
                                                                                                <div class="col-md-3 show-mobile form-row prehead">การอ่าน</div>
                                                                                                <div class="col-md-2 form-row textMobile">{{ data.jobcmbLanguageRead3 }}</div>
                                                                                            </div>

                                                                                            <div class="row LangMobile">
                                                                                                <div class="col-md-3 show-mobile form-row prehead">ภาษาที่ 4 </div>
                                                                                                <div class="col-md-6 form-row show-mobile textMobile">{{ data.jobtxbLanguage4 }}</div>
                                                                                                <div class="col-md-6 form-row hide-mobile"> 4. &nbsp; {{ data.jobtxbLanguage4 }}</div>
                                                                                                <div class="col-md-3 show-mobile form-row prehead">การพูด</div>
                                                                                                <div class="col-md-2 form-row textMobile">{{ data.jobcmbLanguageTalk4 }}</div>
                                                                                                <div class="col-md-3 show-mobile form-row prehead">การเขียน</div>
                                                                                                <div class="col-md-2 form-row textMobile">{{ data.jobcmbLanguageWrite4 }}</div>
                                                                                                <div class="col-md-3 show-mobile form-row prehead">การอ่าน</div>
                                                                                                <div class="col-md-2 form-row textMobile">{{ data.jobcmbLanguageRead4 }}</div>
                                                                                            </div>

                                                                                            <div class="row">
                                                                                                <div class="col-md-3 form-row prehead"> พิมพ์ดีด </div>
                                                                                                <div class="col-md-9 form-row">
                                                                                                    <div class="col-md-2 textMobile">{{ data.jobtxbTypingThai }} ไทย </div>
                                                                                                    <div class="col-md-2 textMobile">{{ data.jobtxbTypingEng }} อังกฤษ </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <!-- end section language -->

                                                                                            <!--      ความสามารถทางคอม      -->
                                                                                            <div class="form-title">ความสามารถทางคอมพิวเตอร์</div>
                                                                                            <div class="job_interesting form-section">
                                                                                                <div class="row" style="height: 150px">
                                                                                                    {{ data.jobProfile_com }}
                                                                                                </div>
                                                                                            </div>


                                                                                            <div class="form-title">ลักษณะงานที่สนใจ</div>
                                                                                            <div class="job_interesting form-section">

                                                                                               <div class="row">
                                                                                                <div class="col-md-3 form-row prehead">ตำแหน่งงาน</div>
                                                                                                <div class="col-md-3 form-row textMobile" id="myjob"></div>
                                                                                                <div class="col-md-3 form-row prehead">ประเภทงาน</div>
                                                                                                <div class="col-md-3 form-row textMobile">{{ data.jobrdoInterestPositionType == 'F' ? 'Full Time' : 'Part Time' }}</div>
                                                                                            </div>

                                                                                            <div class="row">
                                                                                                <div class="col-md-3 form-row prehead">เงินเดือนที่คาดหวัง</div>
                                                                                                <div class="col-md-3 form-row textMobile">  {{ data.jobtxbExpectSalary }} &nbsp; บาท  </div>
                                                                                                <div class="col-md-3 form-row prehead">พร้อมเริ่มงานตั้งแต่วันที่</div>
                                                                                                <div class="col-md-3 form-row textMobile"> {{ dateFormat(data.jobdateWork) }}</div>
                                                                                            </div>

                                                                                        </div>
                                                                                        <!-- end section interesting -->

                                                                                        <div class="form-title">บุคคลอ้างอิง (ไม่ใช่ญาติ หรือคนในครอบครัว)</div>
                                                                                        <div class="ref_person form-section">

                                                                                            <div class="row">
                                                                                                <div class="col-md-3 form-row prehead">ชื่อ-นามสกุล</div>
                                                                                                <div class="col-md-3 form-row textMobile">{{ data.jobtxbReferenceName }}</div>
                                                                                                <div class="col-md-3 form-row prehead">ตำแหน่ง</div>
                                                                                                <div class="col-md-3 form-row textMobile">{{ data.jobtxbReferencePosition }}</div>
                                                                                            </div>

                                                                                            <div class="row">
                                                                                                <div class="col-md-3 form-row prehead">บริษัท</div>
                                                                                                <div class="col-md-3 form-row textMobile">{{ data.jobtxbReferenceCompany }}</div>
                                                                                                <div class="col-md-3 form-row prehead">ความสัมพันธ์</div>
                                                                                                <div class="col-md-3 form-row textMobile">{{ data.jobtxbReferenceRelation }}</div>

                                                                                            </div>

                                                                                            <div class="row">
                                                                                                <div class="col-md-3 form-row prehead">เบอร์โทรติดต่อ</div>
                                                                                                <div class="col-md-9 form-row textMobile"> {{ data.jobtxbReferenceTel != '' ? '(+66)'+data.jobtxbReferenceTel : '' }}</div>
                                                                                            </div>

                                                                                        </div>
                                                                                        <!-- end section ref -->

                                                                                        <div class="form-title">ข้อมูลทั่วไป</div>
                                                                                        <div class="common_info form-section">
                                                                                            <div class="row">
                                                                                                <div class="col-md-12 form-row prehead">
                                                                                                    <span>การไปปฏิบัติงาน ณ โครงการหมู่บ้านท่านขัดข้องหรือไม่<span class="f-required"></span></span>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="row">
                                                                                                <div class="col-md-7 form-row prehead">เป็นประจำ</div>
                                                                                                <div class="col-md-5 form-row textMobile"> {{ data.jobrdoWorkSite == 'Y' ? 'ไม่ขัดข้อง' : 'ขัดข้อง' + " " + data.jobrdoWorkSiteNo }} </div>
                                                                                            </div>

                                                                                            <div class="row">
                                                                                                <div class="col-md-7 form-row prehead">เป็นครั้งคราว</div>
                                                                                                <div class="col-md-5 form-row textMobile"> {{ data.jobrdoWorkSiteFew  == 'Y' ? 'ไม่ขัดข้อง' : 'ขัดข้อง' + " " + data.jobrdoWorkSiteFewNo }}</div>
                                                                                            </div>

                                                                                            <div class="row">
                                                                                                <div class="col-md-7 form-row prehead">ท่านมีโรคเเรื้อรัง/โรคประจำตัวหรือไม่<span class="f-required"></span></div>
                                                                                                <div class="col-md-5 form-row textMobile"> {{ data.jobrdoHasDisease == 'Y' ? 'มี' : 'ไม่มี' + " " + data.jobrdoHasDiseaseYes }} </div>
                                                                                            </div>

                                                                                            <div class="row">
                                                                                                <div class="col-md-7 form-row prehead">ท่านเคยต้องโทษทางคดีอาญาหรือคดีแพ่งหรือไม่<span class="f-required"></span></div>
                                                                                                <div class="col-md-5 form-row textMobile">{{ data.jobrdoHasAccuse == 'Y' ? 'เคย' : 'ไม่เคย' + " " + data.jobrdoHasAccuseYes }}  </div>
                                                                                            </div>

                                                                                            <div class="row">
                                                                                                <div class="col-md-7 form-row prehead">
                                                                                                    ท่ายเคยถูกศาลสั่งให้เป็นบุคคล้มละลาย หรือถูกศาลสั่งพิทักษ์ทรัพย์ หรือมีหนี้สินล้นพ้นตัวหรือไม่
                                                                                                    <span class="f-required"></span></div>
                                                                                                    <div class="col-md-5 form-row textMobile">
                                                                                                        {{ data.jobrdoHasBankrupt  == 'Y' ? 'เคย' : 'ไม่เคย'  + " " + data.jobrdoHasBankruptYes }}
                                                                                                    </div>
                                                                                                </div>


                                                                                            </div>
                                                                                            <!-- end section general -->

                                                                                            <div class="form-title">ข้อมูลเพิ่มเติม</div>
                                                                                            <div class="form-section">
                                                                                                <div class="row" style="height: 150px">
                                                                                                   {{ data.jobtxbFurtherInfo }}
                                                                                               </div>
                                                                                           </div>
                                                                                           <!-- end section Info -->
                                                                                           <!-- end print-->


                                                                                           <div class="row" align="center" style="border:none;">
                                                                                            <div class="col-md-3"></div>
                                                                                            <div class="col-md-3 form-row">
                                                                                                <input type="button"  id="backpreview" class=" action-button" value="ย้อนกลับ" />
                                                                                            </div>
                                                                                            <div class="col-md-3 form-row">
                                                                                              <input type="button" id="btnsend2" class="next" ng-click="myFunc()" value="ส่งใบสมัคร" />
                                                                                          </div>
                                                                                          <!--                <div class="col-md-3 form-row">-->
                                                                                            <!--                    <input type='button' id='btn' value='Print' onclick='printDiv();'>-->
                                                                                            <!--                </div>-->

                                                                                        </div>


                                                                                    </div>



                                                                                </div>
                                                                                <!-- end ใยสมัตร -->
                                                                                <!-- ======================================= -->
                                                                            </div>

                                                                        </div>


                                                                        <div id="tab-2" class="tab-content">
                                                                            <div class="tab-block">
                                                                                <div class="indent">
                                                                                    <div class="row">
                                                                                        <div class="col-md-4">
                                                                                            <h2>ด้านอาชีพและอนาคตของพนักงาน</h2>
                                                                                            <ul class="job-lists">
                                                                                                <li>โบนัส</li>
                                                                                                <li>ส่วนลดซื้อสินค้าบริษัท ฯ</li>
                                                                                                <li>รางวัลที่ได้จากอายุงาน</li>
                                                                                                <li>กองทุนสำรองเลี้ยงชีพ</li>
                                                                                            </ul>
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <h2>ด้านความเป็นอยู่พนักงานและครอบครัว</h2>
                                                                                            <ul class="job-lists">
                                                                                                <li>ประกันสุขภาพพนักงาน (ผู้ป่วยใน, ผู้ป่วยนอก)</li>
                                                                                                <li>ประกันสุขภาพ (ครอบครัวพนักงาน)</li>
                                                                                                <li>การทำประกันขีวิตให้พนักงาน</li>
                                                                                                <li>การตรวจสุขภาพประจำปี</li>
                                                                                                <li>เงินช่วยเหลือด้านทุนการศึกษาบุตร</li>
                                                                                            </ul>
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <h2>ด้านอื่น ๆ</h2>
                                                                                            <ul class="job-lists">
                                                                                                <li>เงินช่วยพิธีมงคลสมรส</li>
                                                                                                <li>เงินช่วยพิธีมรณะกรรมพนักงาน และครอบครัวพนักงาน</li>
                                                                                                <li>ชุดฟอร์มพนักงาน</li>
                                                                                                <li>สันทนาการประจำปี</li>
                                                                                                <li>วันลาต่าง ๆ (ลากิจ ลาป่วย ลาคลอด ลาทำบัตรประชาชน ลาพิธีมงคลสมรส ลาเพื่อไปคัดเลือกในการเกณฑ์ทหาร ลาพักผ่อนประจำปี)</li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div id="tab-3" class="tab-content">
                                                                            <div class="tab-block">
                                                                                <div class="indent">
                                                                                    <h2>มีเหตุผลมากมายที่คุณจะเลือกเรา</h2>
                                                                                    <ul class="job-lists">
                                                                                        <li>เราเป็นหนึ่งในกลุ่มธุรกิจพัฒนาอสังหาริมทรัพย์ของประเทศ เรามอบที่อยู่อาศัยที่ดีและมีความสุขให้แก่ลูกค้า ที่ครบถ้วนและหลายหลาย และสำหรับคุณ นี่หมายถึงโอกาสการทำงานที่กว้างไกลและหลากหลาย</li>
                                                                                        <li>นอกจากนี้ ชื่อเสียงของบริษัทฯ ที่ได้รับรางวัลระดับประเทศอย่างมากมาย คุณลองนึกภาพดูว่าคุณจะสามารถเรียนรู้อะไรได้บ้างจากบุคคลเหล่านี้ ที่มีทั้งทักษะ ความสามารถ และประสบการณ์จนทำให้ Land &amp; Houses ได้รับการยกย่องชมเชยเช่นนี้</li>
                                                                                        <li>และที่สำคัญที่สุด บริษัทฯ ได้มีการฝึกอบรมพนักงานในบริษัทฯ อย่างต่อเนี่อง หลักสูตรการฝึกอบรมจะจัดขึ้นให้สอดคล้องกับลักษณะการทำงานแต่ละหน่วยงาน เพื่อจะส่งผลการพัฒนาศักยภาพของพนักงานอย่างสม่ำเสมอ</li>
                                                                                        <li>ไม่ว่าสิ่งทีคุณกำลังหาอยู่คืออะไร เราเชื่อว่าคุณจะพบกับประสบการณ์ที่มีค่าจากการมาร่วมงานกับเรา</li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div id="tab-4" class="tab-content">
                                                                            <div class="tab-block">
                                                                                <div class="tab-block">
                                                                                    <div class="select-year-block">
                                                                                        ปี
                                                                                        <div class="select-year" style="width: 135px">

                                                                                            <select   ng-init="filter.year=undefined" ng-model="filter.year" style="width: 100px" >
                                                                                                <option value="{{undefined}}"  ng-selected="true" >เลือกดูตามปี</option>
                                                                                                <option ng-repeat="dataArr in opt_year" value="{{dataArr.YEAR}}" >{{ dateFormatY(dataArr.YEAR) }}</option>
                                                                                            </select>


                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="clearfix"></div>

                                                                                    <table class="project-search-list tb-job-news" width="100%">
                                                                                        <thead>
                                                                                            <tr>
                                                                                               <th class="col-xs-3 col-sm-2">
                                                                                                <div class="txt-center">
                                                                                                    เผยแพร่เมื่อ
                                                                                                </div>
                                                                                            </th>
                                                                                            <th class="col-xs-9 col-sm-10">
                                                                                                หัวข้อข่าว
                                                                                            </th>

                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>

                                                                                        <tr ng-repeat="news_ in news | filter: {'YEAR': filter.year} ">


                                                                                            <td class="tb-date" ng-if="news_.message != 'unsuccess' ">{{dateFormat2(news_.DATE)}}</td>
                                                                                            <td class="tb-date" ng-if="news_.message == 'unsuccess' ">ไม่มีข่าวสารในขณะนี้ค่ะ</td>
                                                                                            <td>
                                                                                                <a href="job-news/{{news_.TITLE.replace(' ', '-')}}">
                                                                                                    {{news_.TITLE}}
                                                                                                    <p style="font-size: 20px;color:#8e8d8b;font-weight: normal;margin: 0px;">{{news_.SHORT_TITLE}}</p>
                                                                                                </a>
                                                                                            </td>

                                                                                        </tr>


                                                                                    </tbody>
                                                                                </table>

                                                                                <!-- <a href="" class="btn btn-submit">SEE MORE</a> -->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="tab-5" class="tab-content">
                                                                        <div class="tab-block">

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>


                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>






                                    </jobFName>






                                    <script>

                                       $( document ).ready(function() {

 //step form
 var current_fs, next_fs, previous_fs;
 var countclick = 0;

 $(".next").click(function(){

 //check fieldset valid
 if(countclick == 0){

    if($('#cmbTitle option:selected').val() == ""){

        $('#cmbTitle').focus();
        $('#cmbTitle').css('border-color','red');
        return false;

    }
    if($('#txbFName').val() == ""){

        $('#txbFName').focus();
        $('#txbFName').css('border-color','red');
        return false;

    }
    if($('#txbLName').val() == ""){

        $('#txbLName').focus();
        $('#txbLName').css('border-color','red');
        return false;

    }
    if($('#txbNickname').val() == ""){

        $('#txbNickname').focus();
        $('#txbNickname').css('border-color','red');
        return false;

    }else if($('#dateBirth').val() == ""){

        $('#dateBirth').focus();
        $('#dateBirth').css('border-color','red');
        return false;

    }
    if($('#cmbBirthProvince option:selected').val() == ""){

        $('#cmbBirthProvince').focus();
        $('#cmbBirthProvince').css('border-color','red');
        return false;

    }
    if($('#txbHeight').val() == ""){

        $('#txbHeight').focus();
        $('#txbHeight').css('border-color','red');
        return false;

    }
    if($('#txbWeight').val() == ""){

        $('#txbWeight').focus();
        $('#txbWeight').css('border-color','red');
        return false;

    }
    if($('#cmbReligion option:selected').val() == ""){

        $('#cmbReligion').focus();
        $('#cmbReligion').css('border-color','red');
        return false;

    }
    if($('#cmbConscription option:selected').val() == ""){

        $('#cmbConscription').focus();
        $('#cmbConscription').css('border-color','red');
        return false;

    }
    if($('#cmbConscription option:selected').val() == "ยกเว้น"){

     if($('#txbConscription').val() == ''){
        $('#txbConscription').focus();
        $('#txbConscription').css('border-color','red');
        return false;
    }else{
        $('#txbConscription').css('border-color','#dededes'); 
    } 

}

if($('#txbIDNo').val() == "" || $('#IDnoMin').is(':visible')){

    $('#txbIDNo').focus();
    $('#txbIDNo').css('border-color','red');
    return false;

}
if($('#txbAddress').val() == ""){

    $('#txbAddress').focus();
    $('#txbAddress').css('border-color','red');
    return false;

}
if($('#txbContactEmail').val() == ""){

    $('#txbContactEmail').focus();
    $('#txbContactEmail').css('border-color','red');
    return false;

}
if($('#cmbContactProvince option:selected').val() == ""){

    $('#cmbContactProvince').focus();
    $('#cmbContactProvince').css('border-color','red');
    return false;

}
if($('#txbContactTel').val() == "" || $('#ConTelMin').is(':visible')){

    $('#txbContactTel').focus();
    $('#txbContactTel').css('border-color','red');
    return false;

}
if($('#txbEmergencyPerson').val() == ""){

    $('#txbEmergencyPerson').focus();
    $('#txbEmergencyPerson').css('border-color','red');
    return false;

}
if($('#txbEmergencyTel').val() == "" || $('#EmerTelMin').is(':visible')){

    $('#txbEmergencyTel').focus();
    $('#txbEmergencyTel').css('border-color','red');
    return false;

}
if($('#cmbHighestGraduate option:selected').val() == ""){

    $('#cmbHighestGraduate').focus();
    $('#cmbHighestGraduate').css('border-color','red');
    return false;

}

if($('#cmbGraduateGraduate1_f1 option:selected').val() == ""){
    $('#cmbGraduateGraduate1_f1').focus();
    $('#cmbGraduateGraduate1_f1').css('border-color','red');
    return false;
}

if($('#txbGraduateInstitute1_f1').val() == ""){
    $('#txbGraduateInstitute1_f1').focus();
    $('#txbGraduateInstitute1_f1').css('border-color','red');
    return false;
}

if($('#txbGraduateFaculty1__f1').val() == ""){
    $('#txbGraduateFaculty1__f1').focus();
    $('#txbGraduateFaculty1__f1').css('border-color','red');
    return false;
}

if($('#txbGraduateField1_f1').val() == ""){
    $('#txbGraduateField1_f1').focus();
    $('#txbGraduateField1_f1').css('border-color','red');
    return false;
}

if($('#txbGraduateInstitute1').val() == ""){
    $('#txbGraduateInstitute1').focus();
    $('#txbGraduateInstitute1').css('border-color','red');
    return false;
}
if($('#txbGraduateFaculty1').val() == ""){
    $('#txbGraduateFaculty1').focus();
    $('#txbGraduateFaculty1').css('border-color','red');
    return false;
}
if($('#txbGraduateField1').val() == ""){
    $('#txbGraduateField1').focus();
    $('#txbGraduateField1').css('border-color','red');
    return false;
}
if ($('#txbContactEmail').val() != '' || $('#spfnemail').is(':visible')){
    var emailField = $('#txbContactEmail');
    var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;

    if (emailField.val().search(emailRegEx) == -1)
    {
        $('#txbContactEmail').focus();
        $('#txbContactEmail').css('border-color','red');
                        //alert('Invalid Email Address');
                        return false;

                    }else{
                     $('#txbContactEmail').css('border-color','#dedede');
                     $('#spfnemail').hide();
                       //return true;
                   }

               }







           }
  //end section 1st fieldset


  if(countclick == 1){

    if($('#txbInterestPosition').val() == ""){

        $('#txbInterestPosition').focus();
        $('#txbInterestPosition').css('border-color','red');
        return false;

    }else if($('#txbExpectSalary').val() == ""){

        $('#txbExpectSalary').focus();
        $('#txbExpectSalary').css('border-color','red');
        return false;

    }else if($('#dateWork').val() == ""){

        $('#dateWork').focus();
        $('#dateWork').css('border-color','red');
        return false;

    }
}
  //end section 2nd fieldset


  current_fs = $(this).parents('fieldset');
  next_fs = $(this).parents('fieldset').next();

  $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
  $("#progressbar span img").eq($("fieldset").index(next_fs)).attr("src","<?= file_path('images/global/icon_step_job_active.png') ?>");

  countclick++;
  next_fs.slideDown();
  current_fs.hide();
            // countnextlib++;
            // spannext++;
            // countbacklib=2;
            // spanback=1;

        });

$(".previous").click(function(){

    current_fs = $(this).parents('fieldset');
    previous_fs = $(this).parents('fieldset').prev();

    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
    $("#progressbar span img").eq($("fieldset").index(current_fs)).attr("src","<?= file_path('images/global/icon_step_job.png') ?>");

    previous_fs.slideDown();
    current_fs.hide();
    countclick--;
    console.log(countclick);

});
//#txbIDNo #txbContactTel txbEmergencyTel
 //border original

 $('#cmbTitle, #txbFName, #txbLName, #txbNickname, #txbHeight, #txbWeight, #txbAddress, txbEmergencyPerson, #txbInterestPosition, #txbExpectSalary,#txbConscription,#txbGraduateInstitute1,#txbGraduateFaculty1,#txbGraduateField1,#txbGraduateInstitute1_f1,#txbGraduateFaculty1__f1,#txbGraduateField1_f1',).keyup(function(){


    $(this).css('border-color','#dedede');

    if($(this).val() == ""){

        $(this).css('border-color','red');

    }

})



 $('#cmbTitle, #dateBirth, #cmbBirthProvince, #cmbReligion, #cmbContactProvince, #cmbHighestGraduate, #dateWork,#cmbConscription,#cmbGraduateGraduate1,#txbGraduateInstitute1_f1,#cmbGraduateGraduate1_f1',).change(function(){

    $(this).css('border-color','#dedede');
})

// keyup valid
$('#txbIDNo').keyup(function(){

    if($(this).val().length == 17){

        $(this).css('border-color','#dedede');

    }
})

$('#txbContactTel, #txbEmergencyTel').keyup(function(){

    if($(this).val().length >= 9 && $(this).val().length <= 17){

        $(this).css('border-color','#dedede');

    }
})

$('#txbContactEmail').keyup(function(){
    var emailField = $('#txbContactEmail');
    var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;

    if (emailField.val().search(emailRegEx) == -1)
    {
        $('#txbContactEmail').focus();
        $('#txbContactEmail').css('border-color','red');
                //alert('Invalid Email Address');

            }else{
             $('#txbContactEmail').css('border-color','#dedede');
             $('#spfnemail').hide();

         }

     })



 //maxlength IDNo

 $('#txbIDNo').keydown(function(event){

    var maxlength = 17;
            var value = $('#txbIDNo').val().length; //length start 0

            if(value >= maxlength){

                if(event.keyCode == 8 || event.keyCode == 46){

                    $(this).css('border-color','red');
                    return true;

                }
                return false;
            }

            if($('#IDnoMin').is(':visible') || $(this).val() == ""){

                $(this).css('border-color','red');

            }
        })

//maxlength ContactTel

$('#txbContactTel').keydown(function(event){

    var maxlength = 15;
    var tel = $('#txbContactTel').val().length;


    if(tel >= maxlength){

        if(event.keyCode == 8 || event.keyCode == 46){
            $(this).css('border-color','red');
            return true;

        }
        return false;
    }
    if($('#ConTelMin').is(':visible') || $(this).val() == ""){

        $(this).css('border-color','red');
    }
})

//maxlength Emer
$('#txbEmergencyTel').keydown(function(event){

    var maxlength = 15;

    var tel = $('#txbEmergencyTel').val().length;

    if(tel >= maxlength){

        if(event.keyCode == 8 || event.keyCode == 46){

            $(this).css('border-color','red');
            return true;

        }
        return false;
    }
    if($('#EmerTelMin').is(':visible') || $(this).val() == ""){

        $(this).css('border-color','red');

    }
})


//maxlength RefTel
$('#txbReferenceTel').keypress(function(event){

    var maxlength = 15;
    var RefTel = $('#txbReferenceTel').val().length;


    if(RefTel >= maxlength){

        if(event.keyCode == 8 || event.keyCode == 46){
            $(this).css('border-color','red');
            return true;

        }
        return false;
    }
    if($('#ConTelMinRef').is(':visible') || $(this).val() == ""){
        if(RefTel <= 9){
          $(this).css('border-color','red');
          $('#ConTelMinRef').removeClass('ng-hide');
      }else{
          $(this).css('border-color','#dedede');
          $('#ConTelMinRef').addClass('ng-hide');
      }
  }
})

//maxlength Height

$('#txbHeight').keydown(function(event){

    var maxlength = 3;

    var height = $('#txbHeight').val().length;

    if(height >= maxlength){

        if(event.keyCode == 8 || event.keyCode == 46){

            return true;

        }
        return false;
    }
})

//maxlength Weight
$('#txbWeight').keydown(function(event){

    var maxlength = 3;

    var weight = $('#txbWeight').val().length;

    if(weight >= maxlength){

        if(event.keyCode == 8 || event.keyCode == 46){

            return true;

        }
        return false;
    }
})


 //browse file

 $(document).on('click', '.browse', function(){
    var file = $(this).parent().parent().parent().find('.file');
    file.trigger('click');
});
 $(document).on('change', '.file', function(){
    $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});

//Mobile tab
$('.btn').bind("click", function () {

    $(this).find('.iconmobiledown').hide();
    $(this).find('.iconmobileup').show();
    $(this).css('background-color','#6f9755');
    $(this).css('color','white');
    $(this).css('font-size','25px');

    if ($(this).attr('aria-expanded') == "true") {

        $(this).css('background-color','white');
        $(this).css('color','#6f9755');
        $(this).css('font-size','25px');
        $(this).find('.iconmobileup').hide();
        $(this).find('.iconmobiledown').show();


    }
})

//default don'tshow
$('#txbWorkSite').val("");
$('#txbWorkSiteFew').val("");
$('#txbHasDisease').val("");
$('#txbHasAccuse').val("");
$('#txbHasBankrupt').val("");
$('#txbHasLayoff').val("");
$('#txbConscription').val("");


//checkbox now date

$("#chWorkTo1").click(function() {


    if($('#chWorkTo1').is(':checked')){

        $("input[name='cmbWorkTo1']").attr('disabled', true);
        $("#cmbWorkTo1").datepicker( "setDate" , new Date());
        $(".span-chWorkTo1").css('background','#6d9853');
        $("#rechWorkTo1").text('ปัจจุบัน');
        $(".cmbWorkTo1_test").show();
        $("#cmbWorkTo1_is").val('ปัจจุบัน');


    }else{

        $("input[name='cmbWorkTo1']").attr('disabled', false);
        $(".span-chWorkTo1").css('background','#fff');
        $(".cmbWorkTo1_test").hide();
        $("#cmbWorkTo1_is").val('');
    }


});


$("#chWorkTo2").click(function() {


    if($('#chWorkTo2').is(':checked')){
        $("input[name='cmbWorkTo2']").attr('disabled', true);
        $("#cmbWorkTo2").datepicker( "setDate" , new Date());
        $(".span-chWorkTo2").css('background','#6d9853');
        $("#rechWorkTo2").text('ปัจจุบัน');
        $(".cmbWorkTo2_test").show();
        $("#cmbWorkTo2_is").val('ปัจจุบัน');


    }else{
        $("input[name='cmbWorkTo2']").attr('disabled', false);
        $(".span-chWorkTo2").css('background','#fff'); 
        $(".cmbWorkTo2_test").hide();  
        $("#cmbWorkTo2_is").val('');

    }


});

$("#chWorkTo3").click(function() {


    if($('#chWorkTo3').is(':checked')){

        $("input[name='cmbWorkTo3']").attr('disabled', true);
        $("#cmbWorkTo3").datepicker( "setDate" , new Date());
        $(".span-chWorkTo3").css('background','#6d9853');
        $("#rechWorkTo3").text('ปัจจุบัน');
         $(".cmbWorkTo3_test").show();
         $("#cmbWorkTo3_is").val('ปัจจุบัน');


    }else{

        $("input[name='cmbWorkTo3']").attr('disabled', false);
        $(".span-chWorkTo3").css('background','#fff');  
         $(".cmbWorkTo3_test").hide(); 
         $("#cmbWorkTo3_is").val('');

    }


});

$("#chWorkTo4").click(function() {


    if($('#chWorkTo4').is(':checked')){

        $("input[name='cmbWorkTo4']").attr('disabled', true);
        $("#cmbWorkTo4").datepicker( "setDate" , new Date());
        $(".span-chWorkTo4").css('background','#6d9853');
        $("#rechWorkTo4").text('ปัจจุบัน');
        $(".cmbWorkTo4_test").show(); 
        $("#cmbWorkTo4_is").val('ปัจจุบัน');   


    }else{

        $("input[name='cmbWorkTo4']").attr('disabled', false);
        $(".span-chWorkTo4").css('background','#fff');
        $(".cmbWorkTo4_test").hide();
        $("#cmbWorkTo4_is").val('');    


    }


});

$("#chWorkTo5").click(function() {


    if($('#chWorkTo5').is(':checked')){

        $("input[name='cmbWorkTo5']").attr('disabled', true);
        $("#cmbWorkTo5").datepicker( "setDate" , new Date());
        $(".span-chWorkTo5").css('background','#6d9853');
        $("#rechWorkTo5").text('ปัจจุบัน');
        $(".cmbWorkTo5_test").show();   
        $("#cmbWorkTo5_is").val('ปัจจุบัน'); 


    }else{

        $("input[name='cmbWorkTo5']").attr('disabled', false);
        $(".span-chWorkTo5").css('background','#fff');  
        $(".cmbWorkTo5_test").hide();
        $("#cmbWorkTo5_is").val(''); 
 


    }


});



//datepicker

$('#cmbStudyFrom1 ,#cmbStudyTo1, #cmbStudyFrom2, #cmbStudyTo2, #cmbStudyFrom3, #cmbStudyTo3')
.datepicker({
    format: 'dd/mm/yyyy',
    todayBtn: true,
    language: 'th',
    thaiyear: true,
    dates : 'th'
});
$('#cmbWorkFrom1 , #cmbWorkTo1 , #cmbWorkFrom2 , #cmbWorkTo2 , #cmbWorkFrom3 , #cmbWorkTo3 ,#cmbWorkFrom4 , #cmbWorkTo4 , #cmbWorkFrom5 , #cmbWorkTo5').datepicker({
    format: 'dd/mm/yyyy',
    todayBtn: true,
    language: 'th',
    thaiyear: true,
    dates : 'th'
});

 //เกณฑ์ทหารอื่นๆ

 $('#cmbConscription').change(function(){

    var value = $('#cmbConscription').val();

    if(value != 'ยกเว้น'){

        $('#txbConscription').val("");
    }

});

 // ศาสนาอื่นๆ

 $('#cmbReligion').change(function(){

    var value = $('#cmbReligion').val();

    if(value != 'อื่นๆ'){

        $('#txbReligionOther').val("");
    }
});


// สัญชาติอื่นๆ

$('input[name=rdoRace]').change(function(){

    if ($("input[name=rdoRace").val() != 'อื่นๆ'){

        $('#txbRaceOther').val("");
    }
});

//เชื้อชาติอื่นๆ

$('input[name=rdoNationality]').change(function(){

    if ($("input[name=rdoNationality").val() != 'อื่นๆ'){

        $('#txbNationalityOther').val("");
    }
});


/// ตอบคำถามอื่นๆ

$('input[name=rdoWorkSite]').change(function(){

    if ($("input[name=rdoWorkSite]").val() != ""){

        $('#txbWorkSite').val("");
    }
});


$('input[name=rdoWorkSiteFew]').change(function(){

    if ($("input[name=rdoWorkSiteFew]").val() != ""){

        $('#txbWorkSiteFew').val("");
    }
});

$('input[name=rdoWorkSiteFew]').change(function(){

    if ($("input[name=rdoWorkSiteFew]").val() != ""){

        $('#txbWorkSiteFew').val("");
    }
});

$('input[name=rdoHasDisease]').change(function(){

    if ($("input[name=rdoHasDisease]").val() != ""){

        $('#txbHasDisease').val("");
    }
});

$('input[name=rdoHasAccuse]').change(function(){

    if ($("input[name=rdoHasAccuse]").val() != ""){

        $('#txbHasAccuse').val("");
    }
});

$('input[name=rdoHasBankrupt]').change(function(){

    if ($("input[name=rdoHasBankrupt]").val() != ""){

        $('#txbHasBankrupt').val("");
    }
});

$('input[name=rdoHasLayoff]').change(function(){

    if ($("input[name=rdoHasLayoff]").val() != ""){

        $('#txbHasLayoff').val("");
    }
});


//validate resume pic
$('form').on('keyup change', 'input, select, textarea', function(){

  if($('#errresume').is(":visible")){

    $('#btnSend').prop('disabled',true);

}if($('#errpic').is(":visible")){

    $('#btnSend').prop('disabled',true);

}if($('#errsize').is(":visible")){

    $('#btnSend').prop('disabled',true);

}
});


});

// date mm/yyyy

$('#cmbStudyFrom1 ,#cmbStudyTo1, #cmbStudyFrom2, #cmbStudyTo2, #cmbStudyFrom3, #cmbStudyTo3 ,#cmbWorkFrom1 , #cmbWorkTo1 , #cmbWorkFrom2 , #cmbWorkTo2 , #cmbWorkFrom3 , #cmbWorkTo3 ,#cmbWorkFrom4 , #cmbWorkTo4 , #cmbWorkFrom5 , #cmbWorkTo5 ').datepicker({

    format: "mm/yyyy",
    startView: "months",
    minViewMode: "months",
    language: 'th',
    thaiyear: true,
    dates : 'th'

});

//tab web
var b = $('#cmbTitle').val();


$('#headTabStudy1 a').click(function (e) {

    e.preventDefault()
    $('#headTabStudy1').addClass( "current" )
    $('#headTabStudy2').removeClass( "current" )
    $('#headTabStudy3').removeClass( "current" )
    $('#tabStudy1').show();
    $('#tabStudy2').hide();
    $('#tabStudy3').hide();
})
$('#headTabStudy2 a').click(function (e) {
    e.preventDefault()
    $('#headTabStudy2').addClass( "current" )
    $('#headTabStudy1').removeClass( "current" )
    $('#headTabStudy3').removeClass( "current" )
    $('#tabStudy2').show();
    $('#tabStudy1').hide();
    $('#tabStudy3').hide();
})
$('#headTabStudy3 a').click(function (e) {
    e.preventDefault()
    $('#headTabStudy3').addClass( "current" )
    $('#headTabStudy1').removeClass( "current" )
    $('#headTabStudy2').removeClass( "current" )
    $('#tabStudy3').show();
    $('#tabStudy1').hide();
    $('#tabStudy2').hide();
})

$('#headTab1 a').click(function (e) {

    e.preventDefault()
    $('#headTab1').addClass( "current" )
    $('#headTab2').removeClass( "current" )
    $('#headTab3').removeClass( "current" )
    $('#headTab4').removeClass( "current" )
    $('#headTab5').removeClass( "current" )
    $('#tab1').show();
    $('#tab2').hide();
    $('#tab3').hide();
    $('#tab4').hide();
    $('#tab5').hide();
})
$('#headTab2 a').click(function (e) {
    e.preventDefault()
    $('#headTab2').addClass( "current" )
    $('#headTab1').removeClass( "current" )
    $('#headTab3').removeClass( "current" )
    $('#headTab4').removeClass( "current" )
    $('#headTab5').removeClass( "current" )
    $('#tab2').show();
    $('#tab1').hide();
    $('#tab3').hide();
    $('#tab4').hide();
    $('#tab5').hide();
})
$('#headTab3 a').click(function (e) {
    e.preventDefault()
    $('#headTab3').addClass( "current" )
    $('#headTab1').removeClass( "current" )
    $('#headTab2').removeClass( "current" )
    $('#headTab4').removeClass( "current" )
    $('#headTab5').removeClass( "current" )
    $('#tab3').show();
    $('#tab1').hide();
    $('#tab2').hide();
    $('#tab4').hide();
    $('#tab5').hide();
})
$('#headTab4 a').click(function (e) {
    e.preventDefault()
    $('#headTab4').addClass( "current" )
    $('#headTab1').removeClass( "current" )
    $('#headTab2').removeClass( "current" )
    $('#headTab3').removeClass( "current" )
    $('#headTab5').removeClass( "current" )
    $('#tab4').show();
    $('#tab1').hide();
    $('#tab2').hide();
    $('#tab3').hide();
    $('#tab5').hide();
})
$('#headTab5 a').click(function (e) {
    e.preventDefault()
    $('#headTab5').addClass( "current" )
    $('#headTab1').removeClass( "current" )
    $('#headTab2').removeClass( "current" )
    $('#headTab3').removeClass( "current" )
    $('#headTab4').removeClass( "current" )
    $('#tab5').show();
    $('#tab1').hide();
    $('#tab2').hide();
    $('#tab3').hide();
    $('#tab4').hide();
})

// script preview
$('#btnPreview').click(function(){

    $('#jobform').hide();
    $('#preview').show();

});

$('#backpreview').click(function(){

    $('#jobform').slideDown();
    $('#preview').hide();

});

$('.tel').mask('000-0000000');
$('#txbIDNo').mask('0-0000-00000-00-0');
$('.number_format').mask("#,##0", { reverse: true });
$('.grade').mask("0.00");


function printDiv()
{

  var divToPrint=document.getElementById('preview');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}
</script>
<!-- ### START MODAL ZONE -->
<div class="modal fade" id="modal-jd" tabindex="-1" role="dialog"
aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content">

    </div>
</div>
</div>
<!-- ### SCRIPT FOR WIPE DATA WHEN MODAL CLOSE -->
<script type="text/javascript">
    $('.modal').on('hidden.bs.modal', function () {
     $('.modal').removeData('bs.modal');
     $('.modal').find('.modal-content').html('');
 });
</script>
<!-- ### END MODAL ZONE -->
<?php include('footer.php'); ?>
