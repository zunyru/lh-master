<?php

//require_once 'include/help/begin.php';
//require_once 'include/help/query_function.php';

?>

<?php include('header.php'); ?>
<!-- CSS -->
<link rel="stylesheet" href="css/corporate-profile.css" type="text/css">
<!-- JS -->
<script src="js/corporate-profile.js"></script>

<div id="content" class="content corporate-page">
    <div class="container">
        <div class="profilecomp-block">
            <div class="row">
                <div class="col-md-6">
                    <img src="images/temp5/org_1.jpg" alt="">
                </div>
                <div class="col-md-6">
                    <div class="profile-content profile-block">
                        <h1>ข้อมุลบริษัท</h1>
                        <p>บริษัท แลนด์ แอนด์ เฮ้าส์ จำกัด (มหาชน) ประกอบธุรกิจประเภทค้าอสังหาริมทรัพย์ <br>
                            โดยเริ่มก่อตั้งตั้งแต่ พ.ศ. 2516 และได้มีการจัดตั้งแลนด์ แอนด์ เฮ้าส์ <br>
                            ขึ้นเป็นบริษัทจำกัดในวันที่ 30 สิงหาคม 2526 บริษัทฯ ทำธุรกิจอสังหาริมทรัพย์ <br>
                            โดยขายบ้านจัดสรรพร้อมที่เป็นส่วนใหญ่ โครงการที่ทำจะเป็นโครงการในเขตกรุงเทพมหานคร
                            ปริมณฑลและโครงการตามจังหวัดใหญ่ๆ ได้แก่ เชียงใหม่ เชียงราย ขอนแก่น นครราชสีมา อุดรธานี
                            หัวหิน ภูเก็ต มหาสารคาม และอยุธยา</p>
                    </div>
                    <div class="detail-block company-register">
                        <p>บริษัทมีทุนจดทะเบียนจำนวน 12,031,105,828 บาท<br>
                            เรียกชำระแล้วจำนวน 11,779,071,429 บาท ณ วันที่ 29 สิงหาคม 2559</p>
                    </div>
                    <div class="detail-block">
                        <p class="heading-title">ที่ตั้งสำนักงานใหญ่</p>
                        <p>อาคารคิวเฮ้าส์ลุมพินี ชั่น 37-38 เลขที่ 1 ถนนสาทรใต้<br>
                            แขวงทุ่งมหาเมฆ เขตสาทร กทม. 10120<br>
                            <span class="green">โทร.</span> 0-2343-8900</p>
                    </div>
                    <div class="detail-block">
                        <div class="row">
                            <div class="col-md-8">
                                <p class="green">หนังสือรับรองการจดทะเบียนเป็นนิติบุคคลและ<br>
                                ข้อบังคับของบริษัท แลนด์ แอนด์ เฮ้าส์ จำกัด (มหาชน)<br>
                                สามารถคลิกเพื่ออ่านข้อมูลได้ในรูปแบบไฟล์ PDF</p>
                            </div>
                            <div class="col-md-4">
                                <a href="" class="download-pdf"><i class="i-download"></i>ข้อบังคับ (881 KB)</a>
                                <a href="" class="download-pdf"><i class="i-download"></i>หนังสือรับรอง (1,046 KB)</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <br><br>
        <div class="profilecomp-block">
            <p class="heading-title">วิสัยทัศน์และพันธกิจ</p>
            <br><br>
            <div class="row">
                <div class="col-md-5">
                    <p>บริษัทฯให้ความสำคัญทั้งกระบวนการตั้งแต่เริ่มต้นจนถึงส่งมอบโครงการและบ้านคุณภาพแก่ผู้บริโภค ทั้งการวิจัย การพัฒนา นวัตกรรม เทคโนโลยี การก่อสร้าง การออกแบบ ตลอดจนการคัดเลือกวัสดุ และการบริการ เพื่อให้ได้ที่อยู่อาศัยที่สามารถตอบสนองความต้องการของผู้บริโภคซึ่งมีการเปลี่ยนแปลงอยู่เสมอได้อย่างดีและลงตัว เพื่อให้ผู้บริโภคมีคุณภาพชีวิตและความเป็นอยู่ที่ดีขึ้น รวมถึงให้ความสำคัญต่อสังคม คู่ค้า ผู้ถือหุ้น พนักงานภายในองค์กร และผู้ที่มีส่วนเกี่ยวข้องทุกฝ่าย ให้เป็นไปตามวิสัยทัศน์ของบริษัทฯดังกล่าว</p>
                </div>
                <div class="col-md-7">
                    <img src="images/temp5/org_2.jpg" alt="">
                </div>
            </div>
        </div>

        <div class="profilecomp-block">
            <p class="heading-title">ประวัติบริษัท</p>
            <div id="historyLists" class="history-block">
                <div class="item first">
                    <a href="#y-2556">
                        <p class="history-title">2556</p>
                        <div class="history-list">
                            <div class="history-detail">
                                <p class="subtitle green">ได้รับรางวัล Trusted Brand 2013</p>
                                <p>โดยแลนด์ แอนด์ เฮ้าส์ เป็นแบรนด์ที่ได้รับการโหวตให้เป็น “แบรนด์สุดยอด หรือ Platinum” ในหมวดของบริษัทพัฒนาอสังหาริมทรัพย์ ติดต่อกันเป็นปีที่ 8</p>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="item">
                    <a href="#y-2555">
                        <p class="history-title">2555</p>
                        <div class="history-list">
                            <div class="history-detail">
                                <p class="subtitle green">ได้รับรางวัล Trusted Brand 2012</p>
                                <p>บริษัทฯ ได้รับรางวัล Trusted Brand 2012 จากการสำรวจของนิตยสาร Reader's Digest ได้รับเลือกให้เป็นแบรนด์ที่น่าเชื่อถือมากสุดจากผู้บริโภค โดยในปีนี้บริษัทฯ เป็นเพียงบริษัทเดียวในประเทศไทยที่ได้รับรางวัลในประเภทอสังหาริมทรัพย์และยังได้รับรางวัลสูงสุด คือ Platinum Award ติดต่อกันเป็นปีที่เจ็ดอีกด้วย</p>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="item third">
                    <a href="#y-2554">
                        <p class="history-title">2554</p>
                        <div class="history-list">
                            <div class="history-detail">
                                <p class="subtitle green">ได้รับรางวัล Trusted Brand</p>
                                <p>บริษัทฯ ได้รับรางวัล Trusted Brand "Platinum Award Winner" จากนิตยสาร Reader 's Digest ติดต่อกันเป็นปีที่ 6</p>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="item fourth">
                    <a href="#y-2553">
                        <p class="history-title">2553</p>
                        <div class="history-list">
                            <div class="history-detail">
                                <p class="subtitle green">พัฒนา product brand ใหม่ที่มีระดับราคาต่ำกว่าสินค้าในปัจจุบัน</p>
                                <ul>
                                    <li><span class="bold">Inizio -</span> บ้านเดี่ยว 2.5 - 3 ล้านบาท</li>
                                    <li><span class="bold">Indy -</span> ทาวน์เฮ้าส์ 1.5 - 2 ล้านบาท</li>
                                    <li><span class="bold">The Key -</span> คอนโดมิเนียม 1.8 - 2 ล้านบาท</li>
                                </ul>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="item first">
                    <a href="#y-2551">
                        <p class="history-title">2551</p>
                        <div class="history-list">
                            <div class="history-detail">
                                <p class="subtitle green">ได้รับรางวัล Trusted Brand</p>
                                <p>บริษัทฯ ได้รับรางวัล Trusted Brand "Platinum Award Winner" จากนิตยสาร Reader 's Digest ติดต่อกันเป็นปีที่ 6</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="history-detail-block">
                <div id="y-2556" class="year-details current">
                    <p class="heading-title">2556</p>
                    <div class="row">
                        <div class="col-md-6">
                            <p>บริษัทฯ ได้รับรางวัล Trusted Brand 2013 โดยแลนด์ แอนด์ เฮ้าส์ เป็นแบรนด์ที่ได้รับการโหวตให้เป็น “แบรนด์สุดยอด หรือ Platinum” ในหมวดของบริษัทพัฒนาอสังหาริมทรัพย์ ติดต่อกันเป็นปีที่ 8 จากการโหวตของผู้อ่านนิตยสารรีดเดอร์ส ไดเจสท์ และเป็นเพียงบริษัทเดียวในเอเชียที่ได้รับรางวัลในหมวดพัฒนาอสังหาริมทรัพย์ การสำรวจนี้เป็นการสำรวจแบรนด์ของสินค้าและบริการที่เลือกโดยผู้บริโภคอย่างแท้จริง</p>

                            <p>ในปีนี้ ภายใต้ “LH SMART แนวคิดเพื่อชีวิตที่ดีกว่า” บริษัท แลนด์ แอนด์ เฮ้าส์ จำกัด (มหาชน) ได้แนะนำ “ AirPlus”เทคโนโลยีการถ่ายเทอากาศภายในบ้านให้เกิดภาวะสบาย โดยการนำอากาศใหม่เข้ามาแทนที่อากาศเก่าในบ้าน ทำให้บ้านหายใจได้ ติดตั้งให้กับบ้านแลนด์ แอนด์ เฮ้าส์ เพื่อเพิ่มคุณภาพชีวิตให้กับลูกบ้าน</p>

                            <p>“ระบบ AirPlus” คือนวัตกรรมที่แลนด์ แอนด์ เฮ้าส์ พัฒนาร่วมกับ ศ. ดร.โจเซฟ เคดารี ผู้อำนวยการวิทยาลัยพลังงานและสิ่งแวดล้อมอย่างยั่งยืนรัตนโกสินทร์ มหาวิทยาลัยเทคโนโลยีราชมงคลรัตนโกสินทร์ และทีมงาน ผู้มีความชำนาญในเรื่องของวัสดุและพลังงาน ซึ่งมีผลงานทางด้านวิชาการมากมายทั้งในระดับสากลและในประเทศไทย โดยนำแนวคิดจากหลักพลศาสตร์ประสานกับเทคโนโลยีสมัยใหม่ ทำให้เกิดพลังงานสะอาด คือ</p>

                        </div>
                        <div class="col-md-6">
                            <p>ใช้โซลาร์เซลล์ผลิตไฟฟ้าเพื่อเป็นตัวขับเคลื่อนระบบการทำงานของ AirPlus ทั้งนี้ยังสามารถใช้ระบบ Hybrid ในวันที่พลังงานแสงอาทิตย์ไม่พียงพอเพื่อให้การทำงานของ AirPlus เป็นไปอย่างต่อเนื่อง โดยกำหนดแนวคิดของ “ระบบ AirPlus” ไว้ดังนี้</p>
                            <ul>
                                <li>1. สะดวกสบายด้วยระบบอัตโนมัติ</li>
                                <li>2. ถ่ายเทอากาศอย่างต่อเนื่อง</li>
                                <li>3. ดูแลรักษาง่ายและอายุการใช้งานยาวนาน</li>
                                <li>4. ใช้พลังงานแสงอาทิตย์เป็นหลัก </li>
                                <li>5. เข้ากับวิถีชีวิตของคนรุ่นใหม่</li>
                                <li>6. ใช้ชีวิตอยู่ในสภาพแวดล้อมที่ดี</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div id="y-2555" class="year-details">
                    <p class="heading-title">2555</p>
                    <div class="row">
                        <div class="col-md-6">
                            <p>บริษัทฯ ได้รับรางวัล Trusted Brand 2012 จากการสำรวจของนิตยสาร Reader's Digest ได้รับเลือกให้เป็นแบรนด์ที่น่าเชื่อถือมากสุดจากผู้บริโภค โดยในปีนี้บริษัทฯ เป็นเพียงบริษัทเดียวในประเทศไทยที่ได้รับรางวัลในประเภทอสังหาริมทรัพย์และยังได้รับรางวัลสูงสุด คือ Platinum Award ติดต่อกันเป็นปีที่เจ็ดอีกด้วย</p>

                            <p>ในแง่การตอบสนองความต้องการของกลุ่มลูกค้า บริษัทได้มีการเปิดตัวสินค้าในกลุ่มคอนโดมิเนียมที่จังหวัดเชียงใหม่ ภายใต้แบรนด์ “North” ในทำเลใกล้ใจกลางเมือง</p>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                </div>
                <div id="y-2554" class="year-details">
                    <p class="heading-title">2554</p>
                    <div class="row">
                        <div class="col-md-6">
                            <p>บริษัทฯ ได้รับรางวัล Trusted Brand "Platinum Award Winner" <br>จากนิตยสาร Reader 's Digest ติดต่อกันเป็นปีที่ 6</p>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                </div>
                <div id="y-2553" class="year-details">
                    <p class="heading-title">2553</p>
                    <div class="row">
                        <div class="col-md-6">
                            <p>ปัจจุบัน สินค้าบ้านเดี่ยวของบริษัทฯอยู่ในระดับราคาสูงกว่า 3 ล้านบาท ทาวน์เฮ้าส์และคอนโดมิเนียม อยู่ในระดับราคาสูงกว่า 2 ล้านบาท เพื่อเป็นการขยายฐานลูกค้าและตอบสนองความต้องการบ้านของผู้บริโภคมากยิ่งขึ้น บริษัทฯจึงได้พัฒนา product brand ใหม่ที่มีระดับราคาต่ำกว่าสินค้าในปัจจุบัน ดังต่อไปนี้</p>
                            <ul>
                                <li>1.บ้านเดี่ยว ระดับราคา 2.5 - 3 ล้านบาท ภายใต้แบรนด์ “Inizio”</li>
                                <li>2.ทาวน์เฮ้าส์ ระดับราคา 1.5 - 2 ล้านบาท ภายใต้แบรนด์ “Indy”</li>
                                <li>3.คอนโดมิเนียม ระดับราคา 1.8 - 2 ล้านบาท ภายใต้แบรนด์ “The Key”</li>
                            </ul>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                </div>
                <div id="y-2551" class="year-details">
                    <p class="heading-title">2551</p>
                    <div class="row">
                        <div class="col-md-6">
                            <p>บริษัท แลนด์ แอนด์ เฮ้าส์ จำกัด(มหาชน) มีความมุ่งมั่นในการดำเนินธุรกิจตามหลักบรรษัทภิบาลส่งเสริมจริยธรรมในการทำงานรวมทั้งให้ความสำคัญในเรื่องการพัฒนาทักษะและคุณภาพบุคลากร ควบคู่กับการพัฒนาระบบงานเพื่อให้ได้มาซึ่งสินค้าและบริการที่มีคุณภาพตลอดมา ส่งผลให้บริษัทฯ ได้รับรางวัล Trusted Brand "Platinum Award Winner" จากนิตยสาร Reader 's Digest ซึ่งเป็นรางวัลขั้นสูงสุดเพียงหนึ่งเดียว ในฐานะบริษัทผู้ให้บริการและพัฒนาด้านอสังหาริมทรัพย์ที่ได้รับความเชื่อมั่นจากผู้บริโภคในประเทศไทยติดต่อกันเป็นปีที่ 3</p>

                            <p>ด้านการพัฒนาสินค้าในปี 2551 นี้ บริษัทฯได้พัฒนารูปบ้านแบบใหม่ 2 Series ได้แก่ My Life แบบบ้านซี่รี่ส์ล่าสุดที่ออกแบบจากผลการวิจัยพฤติกรรมผู้บริโภคในการอยู่อาศัย ความต้องการ และการใช้พื้นที่ รวมถึงวิถีชีวิตประจำวัน และแบบบ้าน Modern Style ล่าสุดที่โครงการ The Land Mark Residence ซึ่งถือเป็นรุ่นพรีเมี่ยมพิเศษสุด สะท้อนความทันสมัย ตอบรับการอยู่อาศัยแบบ Urban Living อย่างที่ไม่เคยมีมาก่อน ในระดับราคา 20 ล้านบาทขึ้นไป</p>

                            <p>แลนด์ แอนด์ เฮ้าส์ ยังมีการพัฒนาโครงการทาวน์โฮม เพื่อตอบรับความต้องการของตลาดที่อยู่อาศัยระดับราคา 2.7-3.2 ล้านบาท ภายใต้ Brand " Baan Mai" ที่กระจายไปในทำเลต่างๆ ของ กรุงเทพมหานครและปริมณฑล</p>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="profilecomp-block">
            <p class="heading-title">บริษัทในเครือ</p>
            <div class="row">
                <div class="col-md-4">
                    <div class="affiliate-block">
                        <div class="affiliate-img">
                            <span></span>
                            <img src="images/temp4/org_logo_1.jpg" alt="">
                        </div>
                        <p class="affiliate-name">บริษัท ควอลิตี้เฮ้าส์ จำกัด (มหาชน)</p>
                        <p>
                            ประเภทธุรกิจ : อสังหาริมทรัพย์<br>
                            ที่อยู่ : อาคารคิวเฮ้าส์ ลุมพินี ชั้น 7<br>
                            เลขที่ 1 ถนนสาทรใต้ แขวงทุ่งมหาเมฆ<br>
                            เขตสาทร กรุงเทพฯ 10120<br>
                            เบอร์โทรศัพท์ : 0 2677 7000<br>
                            เบอร์โทรสาร : 0 2664 3396</p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="affiliate-block">
                        <div class="affiliate-img">
                            <span></span>
                            <img src="images/temp4/org_logo_2.jpg" alt="">
                        </div>
                        <p class="affiliate-name">บริษัท แอล.เอช.เมืองใหม่ จำกัด</p>
                        <p>ประเภทธุรกิจ : อสังหาริมทรัพย์<br>
                            ที่อยู่ : 80/15 ถ.เจ้าฟ้า ต.ฉลอง อ.เมือง จ.ภูเก็ต<br>
                            เบอร์โทรศัพท์ : 0 7638 1150 2</p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="affiliate-block">
                        <div class="affiliate-img">
                            <span></span>
                            <img src="images/temp4/org_logo_3.jpg" alt="">
                        </div>
                        <p class="affiliate-name">บริษัท โฮม โปรดักส์ เซ็นเตอร์ จำกัด (มหาชน)</p>
                        <p>
                            ประเภทธุรกิจ : จำหน่ายสินค้าและบริการ <br>
                            ก่อสร้างต่อเติม และตกแต่งที่อยู่อาศัย<br>
                            ที่อยู่ : 96/27 หมู่ 9 บางเขน อ.เมือง<br>
                            จังหวัดนนทบุรี 11000<br>
                            เบอร์โทรศัพท์ : 0 2832 1000<br>
                            เบอร์โทรสาร : 0 2832 1400</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="center-block">
                    <div class="col-md-4 col-md-offset-2">
                        <div class="affiliate-block">
                            <div class="affiliate-img">
                                <span></span>
                                <img src="images/temp4/org_logo_4.jpg" alt="">
                            </div>
                            <p class="affiliate-name">บริษัท ควอลิตี้คอนสตรัคชั่นโปรดัคส์ จำกัด (มหาชน)</p>
                            <p>
                                ประเภทธุรกิจ : ผลิตวัสดุก่อสร้าง<br>
                                ที่อยู่ : นิคมอุตสาหกรรมบางปะอิน<br>
                                144 หมู่ 16 ถ.อุดมสรยุทธ์ ต.บางกระสั้น<br>
                                อ.บางปะอิน จ.พระนครศรีอยุธยา 13160<br>
                                เบอร์โทรศัพท์ : 0 3522 1264 71<br>
                                เบอร์โทรสาร : 0 3522 1270</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="affiliate-block">
                            <div class="affiliate-img">
                                <span></span>
                                <img src="images/temp4/org_logo_5.jpg" alt="">
                            </div>
                            <p class="affiliate-name">ธนาคารแลนด์ แอนด์ เฮ้าส์ จำกัด (มหาชน)</p>
                            <p>ประเภทธุรกิจ : ธนาคารพาณิชย์<br>
                                ที่อยู่ : อาคารคิวเฮ้าส์ ลุมพินี ชั้น 5<br>
                                เลขที่ 1 ถนนสาทรใต้ แขวงทุ่งมหาเมฆ<br>
                                เขตสาทร กรุงเทพฯ 10120<br>
                                เบอร์โทรศัพท์ : 0 2359 0000<br>
                                เบอร์โทรสาร : 0 2677 7223</p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

    </div>
</div>


<?php include('footer.php'); ?>
