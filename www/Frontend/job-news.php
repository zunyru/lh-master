<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>
<?php include('header.php'); ?>
    <!-- CSS -->
    <link rel="stylesheet" href="css/job-news.css" type="text/css">
    <!-- JS -->
    <script src="js/job-news.js"></script>
    <script src="js/banner-md.js"></script>

    <div class="page-banner-md banner-hide">
        <div id="bannerSlide">
            <div class="item">
                <div class="banner-parallax" style="background-image: url(images/temp3/banner_job.jpg);">
                </div>
            </div>
        </div>
    </div>

    <div id="content" class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div>
                        <h1 class="heading-title">ร่วมงานกับเรา</h1>

                        <div id="tabs-container" class="tabs-container">
                            <div class="tab-dd dropdown">
                                <ul class="tabs-menu">
                                    <li class="item current">
                                        <a href="#tab-1">
                                            ตำแหน่งงานที่เปิดรับสมัคร
                                        </a>
                                    </li>
                                    <li class="item">
                                        <a href="#tab-2">
                                            สวัสดิการที่คุณจะได้รับ
                                        </a>
                                    </li>
                                    <li class="item">
                                        <a href="#tab-3">
                                            ประสบการณ์ที่คุณจะได้จากเรา
                                        </a>
                                    </li>
                                    <li class="item">
                                        <a href="#tab-4">
                                            ข่าวสารสมัครงาน
                                        </a>
                                    </li>
                                    <li class="item">
                                        <a href="" onclick="location.href='<?php url('/contact-us.php'); ?>';">
                                            ติดต่อเรา
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="select-container" id="choose-location">
                                <div class="select-block selectProvince">
                                    <input type="hidden" name="selectProvince" value="">
                                    <div class="sort" data-status="0">
                                        <span>เลือกจังหวัด</span> <i class="i-sort"></i>
                                    </div>
                                    <ul id="listsProvince">
                                        <li data-value="กรุงเทพ และปริมณฑล">กรุงเทพ และปริมณฑล</li>
                                        <li data-value="อยุธยา">อยุธยา</li>
                                        <li data-value="สมุทรสาคร">สมุทรสาคร</li>
                                        <li data-value="เชียงราย">เชียงราย</li>
                                        <li data-value="เชียงใหม่">เชียงใหม่</li>
                                    </ul>
                                </div>

                                <div class="select-block selectArea">
                                    <input type="hidden" name="selectArea" value="">
                                    <div class="sort" data-status="0">
                                        <span>เลือกพื้นที่</span> <i class="i-sort"></i>
                                    </div>
                                    <ul id="listsArea">
                                        <li data-value="เลียบทางด่วนเอกมัย, รามอินทรา ,เกษตรนวมินทร์">เลียบทางด่วนเอกมัย, รามอินทรา ,เกษตรนวมินทร์</li>
                                        <li data-value="บางนา กม.26">บางนา กม.26</li>
                                        <li data-value="รังสิต ,รามอินทรา">รังสิต ,รามอินทรา</li>
                                        <li data-value="ราชพฤกษ์ ,ปิ่นเกล้า ,รัตนาธิเบศน์ ,บางใหญ่">ราชพฤกษ์ ,ปิ่นเกล้า ,รัตนาธิเบศน์ ,บางใหญ่</li>
                                        <li data-value="ตลิ่งชัน,ปิ่นเกล้า,บางใหญ่ ,บางบัวทอง ,นนทบุรี">ตลิ่งชัน,ปิ่นเกล้า,บางใหญ่ ,บางบัวทอง ,นนทบุรี</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="tab">
                                <div id="tab-1" class="tab-content">
                                    <div class="tab-block">
                                        <div class="form current">
                                            <table class="project-search-list" width="100%">
                                                <thead>
                                                <tr>
                                                    <th>
                                                        <span class="hide-rps">วันที่ประกาศ</span>
                                                        <span class="hide-dsktp">วันที่</span>
                                                    </th>
                                                    <th>
                                                        <span class="hide-rps">ตำแหน่งงานว่าง</span>
                                                        <span class="hide-dsktp">ตำแหน่ง</span>
                                                    </th>
                                                    <th class="hide-dsktp">จังหวัด</th>
                                                    <th class="hide-rps">พื้นที่ทำงาน</th>
                                                    <th>
                                                        <span class="hide-rps">อัตราที่เปิดรับ</span>
                                                        <span class="hide-dsktp">อัตรา</span>
                                                    </th>
                                                    <th>
                                                        <span class="hide-rps">สมัครงานตำแหน่งนี้</span>
                                                        <span class="hide-dsktp">สมัครงาน</span>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>14/10/2016</td>
                                                    <td>เจ้าหน้าที่จัดซื้องานออกแบบตกแต่ง</td>
                                                    <td class="txt-center">กรุงเทพฯและปริมณฑล</td>
                                                    <td class="hide-rps">ต่างจังหวัด</td>
                                                    <td class="txt-center">2</td>
                                                    <td class="txt-center"><a id="formJob">สมัครงาน</a></td>
                                                </tr>
                                                <tr>
                                                    <td>14/10/2016</td>
                                                    <td>AVP MARKETING, REAL ESTATE DEVELOPER</td>
                                                    <td class="txt-center">กรุงเทพฯและปริมณฑล</td>
                                                    <td class="hide-rps">สาธร (อาคารคิวเฮ้าส์ลุมพินี)</td>
                                                    <td class="txt-center">2</td>
                                                    <td class="txt-center"><a id="formJob2">สมัครงาน</a></td>
                                                </tr>
                                                <tr>
                                                    <td>14/10/2016</td>
                                                    <td>เจ้าหน้าที่การตลาด (Marketing)</td>
                                                    <td class="txt-center">กรุงเทพฯและปริมณฑล</td>
                                                    <td class="hide-rps">กรุงเทพฯและปริมณฑล</td>
                                                    <td class="txt-center">2</td>
                                                    <td class="txt-center"><a id="formJob3">สมัครงาน</a></td>
                                                </tr>
                                                <tr>
                                                    <td>14/10/2016</td>
                                                    <td>เจ้าหน้าที่การตลาด (Marketing)</td>
                                                    <td class="txt-center">กรุงเทพฯและปริมณฑล</td>
                                                    <td class="hide-rps">กรุงเทพฯและปริมณฑล</td>
                                                    <td class="txt-center">2</td>
                                                    <td class="txt-center"><a id="formJob4">สมัครงาน</a></td>
                                                </tr>
                                                <tr>
                                                    <td>14/10/2016</td>
                                                    <td>เจ้าหน้าที่การตลาด (Marketing)</td>
                                                    <td class="txt-center">กรุงเทพฯและปริมณฑล</td>
                                                    <td class="hide-rps">กรุงเทพฯและปริมณฑล</td>
                                                    <td class="txt-center">2</td>
                                                    <td class="txt-center"><a id="formJob5">สมัครงาน</a></td>
                                                </tr>
                                                <tr>
                                                    <td>14/10/2016</td>
                                                    <td>เจ้าหน้าที่การตลาด (Marketing)</td>
                                                    <td class="txt-center">กรุงเทพฯและปริมณฑล</td>
                                                    <td class="hide-rps">กรุงเทพฯและปริมณฑล</td>
                                                    <td class="txt-center">2</td>
                                                    <td class="txt-center"><a id="formJob6">สมัครงาน</a></td>
                                                </tr>
                                                <tr>
                                                    <td>14/10/2016</td>
                                                    <td>เจ้าหน้าที่การตลาด (Marketing)</td>
                                                    <td class="txt-center">กรุงเทพฯและปริมณฑล</td>
                                                    <td class="hide-rps">กรุงเทพฯและปริมณฑล</td>
                                                    <td class="txt-center">2</td>
                                                    <td class="txt-center"><a id="formJob">สมัครงาน</a></td>
                                                </tr>
                                                <tr>
                                                    <td>14/10/2016</td>
                                                    <td>เจ้าหน้าที่การตลาด (Marketing)</td>
                                                    <td class="txt-center">กรุงเทพฯและปริมณฑล</td>
                                                    <td class="hide-rps">กรุงเทพฯและปริมณฑล</td>
                                                    <td class="txt-center">2</td>
                                                    <td class="txt-center"><a id="formJob">สมัครงาน</a></td>
                                                </tr>
                                                <tr>
                                                    <td>14/10/2016</td>
                                                    <td>เจ้าหน้าที่การตลาด (Marketing)</td>
                                                    <td class="txt-center">กรุงเทพฯและปริมณฑล</td>
                                                    <td class="hide-rps">กรุงเทพฯและปริมณฑล</td>
                                                    <td class="txt-center">2</td>
                                                    <td class="txt-center"><a id="formJob">สมัครงาน</a></td>
                                                </tr>
                                                </tbody>
                                            </table>

                                            <a href="" class="btn btn-submit">SEE MORE</a>
                                        </div>

                                        <div class="job-form">
                                            <?php include('template-part/tpl-apply-job.php'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab-2" class="tab-content">
                                    <div class="tab-block">
                                        <div class="indent">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <h2>ด้านอาชีพและอนาคตของพนักงาน</h2>
                                                    <ul class="job-lists">
                                                        <li>โบนัส</li>
                                                        <li>ส่วนลดซื้อสินค้าบริษัท ฯ</li>
                                                        <li>รางวัลที่ได้จากอายุงาน</li>
                                                        <li>กองทุนสำรองเลี้ยงชีพ</li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4">
                                                    <h2>ด้านความเป็นอยู่พนักงานและครอบครัว</h2>
                                                    <ul class="job-lists">
                                                        <li>ประกันสุขภาพพนักงาน (ผู้ป่วยใน, ผู้ป่วยนอก)</li>
                                                        <li>ประกันสุขภาพ (ครอบครัวพนักงาน)</li>
                                                        <li>การทำประกันขีวิตให้พนักงาน</li>
                                                        <li>การตรวจสุขภาพประจำปี</li>
                                                        <li>เงินช่วยเหลือด้านทุนการศึกษาบุตร</li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4">
                                                    <h2>ด้านอื่น ๆ</h2>
                                                    <ul class="job-lists">
                                                        <li>เงินช่วยพิธีมงคลสมรส</li>
                                                        <li>เงินช่วยพิธีมรณะกรรมพนักงาน และครอบครัวพนักงาน</li>
                                                        <li>ชุดฟอร์มพนักงาน</li>
                                                        <li>สันทนาการประจำปี</li>
                                                        <li>วันลาต่าง ๆ (ลากิจ ลาป่วย ลาคลอด ลาทำบัตรประชาชน ลาพิธีมงคลสมรส ลาเพื่อไปคัดเลือกในการเกณฑ์ทหาร ลาพักผ่อนประจำปี)</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab-3" class="tab-content">
                                    <div class="tab-block">
                                        <div class="indent">
                                            <h2>มีเหตุผลมากมายที่คุณจะเลือกเรา</h2>
                                            <ul class="job-lists">
                                                <li>เราเป็นหนึ่งในกลุ่มธุรกิจพัฒนาอสังหาริมทรัพย์ของประเทศ เรามอบที่อยู่อาศัยที่ดีและมีความสุขให้แก่ลูกค้า ที่ครบถ้วนและหลายหลาย และสำหรับคุณ นี่หมายถึงโอกาสการทำงานที่กว้างไกลและหลากหลาย</li>
                                                <li>นอกจากนี้ ชื่อเสียงของบริษัทฯ ที่ได้รับรางวัลระดับประเทศอย่างมากมาย คุณลองนึกภาพดูว่าคุณจะสามารถเรียนรู้อะไรได้บ้างจากบุคคลเหล่านี้ ที่มีทั้งทักษะ ความสามารถ และประสบการณ์จนทำให้ Land &amp; Houses ได้รับการยกย่องชมเชยเช่นนี้</li>
                                                <li>และที่สำคัญที่สุด บริษัทฯ ได้มีการฝึกอบรมพนักงานในบริษัทฯ อย่างต่อเนี่อง หลักสูตรการฝึกอบรมจะจัดขึ้นให้สอดคล้องกับลักษณะการทำงานแต่ละหน่วยงาน เพื่อจะส่งผลการพัฒนาศักยภาพของพนักงานอย่างสม่ำเสมอ</li>
                                                <li>ไม่ว่าสิ่งทีคุณกำลังหาอยู่คืออะไร เราเชื่อว่าคุณจะพบกับประสบการณ์ที่มีค่าจากการมาร่วมงานกับเรา</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab-4" class="tab-content">
                                    <div class="tab-block">
                                        <div class="select-year-block">
                                            ปี
                                            <div class="select-year">
                                                <select name="" id="">
                                                    <option value="2560">2560</option>
                                                    <option value="2559">2559</option>
                                                    <option value="2558">2558</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>

                                        <table class="project-search-list tb-job-news" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="col-xs-9 col-sm-10">
                                                        หัวข้อข่าว
                                                    </th>
                                                    <th class="col-xs-3 col-sm-2">
                                                        <div class="txt-center">
                                                            เผยแพร่เมื่อ
                                                        </div>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><a href="<?php url('/job-news-details.php') ?>">ประกาศรายชื่อผู้มีสิทธิ์เข้าทำงาน</a></td>
                                                    <td class="tb-date">14/02/2560</td>
                                                </tr>
                                                <tr>
                                                    <td><a href="<?php url('/job-news-details.php') ?>">รับสมัครงานตำแหน่งเจ้าหน้าที่อำนวยการ 1 อัตรา</a></td>
                                                    <td class="tb-date">03/02/2560</td>
                                                </tr>
                                                <tr>
                                                    <td><a href="<?php url('/job-news-details.php') ?>">รับสมัครงานตำแหน่งเจ้าหน้าที่งานขาย 2 อัตรา</a></td>
                                                    <td class="tb-date">01/02/2560</td>
                                                </tr>
                                                <tr>
                                                    <td><a href="<?php url('/job-news-details.php') ?>">รับสมัครงานตำแหน่งบัญชี 2 อัตรา</a></td>
                                                    <td class="tb-date">14/01/2560</td>
                                                </tr>
                                                <tr>
                                                    <td><a href="<?php url('/job-news-details.php') ?>">ประกาศรับสมัครงานตำแหน่งต่างๆ</a></td>
                                                    <td class="tb-date">10/01/2560</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <a href="" class="btn btn-submit">SEE MORE</a>
                                    </div>
                                </div>
                                <div id="tab-5" class="tab-content">
                                    <div class="tab-block">

                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>


                    </div>
                </div>

            </div>

        </div>
    </div>


<?php include('footer.php'); ?>
