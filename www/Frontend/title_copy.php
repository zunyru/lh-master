<?php
    if(isset($title_page)){
        $title_page;
        $description_page;
        $image_page;
    }else{

        $title_page = "บริษัท แลนด์ แอนด์ เฮ้าส์ จำกัด (มหาชน)";
        $description_page = "Land & Houses บริษัทธุรกิจอสังหาริมทรัพย์ในประเทศไทย LHเปิดขายทั้งโครงการบ้านเดี่ยว ทาวน์โฮม คอนโดมิเนียมในกรุงเทพฯ ปริมณฑลและต่างจังหวัด";
        $image_page ="";
    }
 ?>

    <title><?=$title_page;?></title>
    <meta name="description" content="<?=$description_page;?>" />
    <meta name="keywords" content="lh, land and houses, แลนด์ แอนด์ เฮ้าส์, แลนแอนเฮ้า" />

     <!-- for Facebook title-->
    <meta property="og:title" class="title_shared_facebook" content="<?=$title_page  ?>" />
    <meta property="og:description" class="desc_shared_facebook" content="<?=$description_page ?>" />
    <meta property="og:image:secure_url" class="img_shared_facebook" content="<?=$image_page ?>" />
    <meta property="og:image" class="img_shared_facebook" content="<?=$image_page ?>" />
    <meta property="og:url"  content='<?php echo $url_pure = (substr($actual_link, 0, strpos($actual_link, "?"))==''?$actual_link:substr($actual_link, 0, strpos($actual_link, "?")));?>' />
    <meta property="og:type" content="website" />
    <meta property="og:image:type" content="image/jpeg" /> 
    <meta property="fb:app_id" content="659883840880609" />


     <!-- for Twitter -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="<?=$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>">
    <meta name="twitter:creator" content="@lhhome">
    <meta name="twitter:title" content="<?=$title_page;?>">
    <meta name="twitter:description" content="<?=$description_page;?>">
    <meta name="twitter:image" content="<?=$image_page;?>">

<?php ?>