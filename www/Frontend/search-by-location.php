<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>
<?php include('header.php'); ?>
    <!-- JS -->
    <link rel="stylesheet" href="css/search-by.css" type="text/css">

    <script src="js/search-by.js"></script>
    <script src="js/search-by.js"></script>

    <div class="page-banner page-scroll">
        <span id="scrollNext" class="i-scroll-next"></span>

        <div id="bannerSlide" class="owl-carousel">
            <div class="item">
                <div class="banner-parallax" style="background-image: url(images/project-type-condo/pjt_n_2.jpg);">
                    <div class="bg-slide">
                        <div class="banner-txt">
                            <h1>THE ROOM</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="content" class="content furnishedhome-page">
        <div class="container">
            <div class="furnishedhome-block">
                <p class="heading-title">คอนโด > กรุงเทพชั้นใน</p>
                <p class="subtitle grey">ขอแนะนำ 40 โครงการคุณภาพจากแลนด์ แอนด์ เฮ้าส์</p>

                <div class="sort-container">
                    <div class="sort-block">
                        <input type="hidden" name="sortSelect" value="">
                        <div class="sort" data-status="0">
                            <span>ค้นหาโครงการบนทำเลที่คุณสนใจ</span> <i class="i-sort"></i>
                        </div>
                        <ul id="sortLists">
                            <li data-value="กรุงเทพ และปริมณฑล">กรุงเทพ และปริมณฑล</li>
                            <li data-value="อยุธยา">อยุธยา</li>
                            <li data-value="สมุทรสาคร">สมุทรสาคร</li>
                            <li data-value="เชียงราย">เชียงราย</li>
                            <li data-value="เชียงใหม่">เชียงใหม่</li>
                        </ul>
                    </div>

                    <div class="view-block">
                        <ul>
                            <li><a href="" class="i-view-list active"></a></li>
                            <li><a href="" class="i-view-grid"></a></li>
                            <li><a href="" class="i-view-grid-more"></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                </div>

                <input type="hidden" id="status_list" value="list">
            </div>
            <div class="project-type-block">
                <div id="products" class="list-group">

                    <div class="projecttype-list">
                        <div class="row">
                            <div class="col-md-8">
                                <img src="images/temp2/loc_3.jpg" alt="">
                            </div>
                            <div class="col-md-4">
                                <div class="projecttype-descrp">
                                    <img src="images/logo/logo_theroom.jpg" alt="">
                                    <p class="subtitle">
                                        เริ่ม 4 - 6 ล้านบาท
                                    </p>
                                    <p class="title">The New Definition of London Living</p>
                                    <p>เริ่มต้นเรื่องราวชีวิตใหม่ในสไตล์ที่เป็นคุณ</p>
                                    <a href="" class="btn btn-seemore">See more</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="projecttype-list">
                        <div class="row">
                            <div class="col-md-8">
                                <img src="images/temp2/loc_3.jpg" alt="">
                            </div>
                            <div class="col-md-4">
                                <div class="projecttype-descrp">
                                    <img src="images/logo/logo_theroom.jpg" alt="">
                                    <p class="subtitle">
                                        เริ่ม 4 - 6 ล้านบาท
                                    </p>
                                    <p class="title">The New Definition of London Living</p>
                                    <p>เริ่มต้นเรื่องราวชีวิตใหม่ในสไตล์ที่เป็นคุณ</p>
                                    <a href="" class="btn btn-seemore">See more</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>


<?php include('footer.php'); ?>