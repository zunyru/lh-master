<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';
require_once 'include/help/hex2rgba.php';

if(empty($_GET['zone_id'])){
    $project_ids = getProjectIDByProductView(5);
}else{
 $project_ids = getProjectIDByProductViewZone(5,$_GET['zone_id']);
}  
Helper::sortByPriceReturnAssoc($project_ids,'asc');
$page = 'ladawan';
$page_index = 6;
?>
<?php
include('header.php');
//elementMetaTitleDisTag($page,$page_index);
?>
    <!-- JS -->
    <link rel="stylesheet" href="<?= file_path('css/project-type-luxury.css') ?>" type="text/css">
    <link rel="stylesheet" href="<?= file_path('css/video.css') ?>" type="text/css">

    <script src="<?= file_path('js/project-type-luxury.js') ?>"></script>

    <div class="page-banner page-scroll">
        <span id="scrollNext" class="i-scroll-next"></span>

    <div id="bannerSlide" class="owl-carousel">
        <?php
        $countProjectZone = 0;

        foreach ($project_ids as $i => $pro_sub) {
            $project_id = $pro_sub['project_id'];
            if(empty($_GET['zone_id'])){
                $project = getProjectByID($project_id);
            }else{
                $project = getProjectByIDZone_id($project_id,$_GET['zone_id']);
            }
            if( empty($_GET['zone_id']) || (!empty($_GET['zone_id']) && $_GET['zone_id'] == $project->zone_id ) ) {
             $countProjectZone++;
         }
     }  ?>

       <?php
       renderBannerPageList(6);
       ?>

   </div>
</div>

<?php
renderHiddenLeadImage(6,'bannerSlide');
renderActivityResponsive();
?>
<?php  
$zones = getProjectViewZones(5);
if(!empty($_GET['zone_id'])){
    foreach($zones as $zone) {
        if($_GET['zone_id'] == $zone->zone_id){
           $zone_names = $zone->zone_name_th;
        }
    }
}
if(!empty($_GET['zone_id'])){
    $SEO = getSEOUrl($actual_link);
    if($actual_link == @$SEO->url_page){
      $title_home = $SEO->h1;
    }else{
      $title_home = "โครงการ LADAWAN : ".$zone_names; 
    } 
   
}else{
    $SEO = getSEOUrl($actual_link);
    if($actual_link == @$SEO->url_page){
        $title_home = $SEO->h1;
    }else{
      $title_home = "โครงการ LADAWAN ทั้งหมดของ LH";    
    }
   
}
?>

    <div id="content" class="content project-type-page">
        <div class="container">
            <h1 style="cursor: pointer" onclick="location.href='<?= $router->generate('ladawan-list') ?>'">
                <?=$title_home?>
            </h1>
            <div class="project-type-block">
                <h2>ขอแนะนำโครงการ LADAWAN <?= $countProjectZone ?> โครงการ</h2>
                <div class="sort-container">
                    <div class="sort-block">
                        <input type="hidden" name="sortSelect" value="">
                        <div class="sort" data-status="0">
                            <span>
                                <?php
                                $zones = getProjectViewZones(5);
                                if(!empty($_GET['zone_id'])){
                                    foreach($zones as $zone) {
                                        if($_GET['zone_id'] == $zone->zone_id){
                                            echo $zone->zone_name_th;
                                        }
                                    }
                                }else{
                                    echo 'เลือกทำเลที่สนใจทั้งหมด';
                                }
                                ?>
                            </span> <i class="i-sort"></i>
                        </div>
                        <ul id="sortLists">
                            <li data-value="เลือกทำเลที่สนใจทั้งหมด" onclick="location.href='<?= $router->generate('ladawan-list') ?>'">
                                เลือกทำเลที่สนใจทั้งหมด
                            </li>
                            <?php
                            foreach($zones as $zone) {?>
                                <li data-value="<?= $zone->zone_name_th ?>" onclick="location.href='<?= $router->generate('ladawan-list-by-zone',['zone_name' => str_replace(' ','-',$zone->zone_name_th)]) ?>'">
                                    <?= $zone->zone_name_th ?>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>

                    <div class="view-block">
                        <ul>
                            <li><a href="" class="i-view-list active"></a></li>
                            <li><a href="" class="i-view-grid"></a></li>
                            <li><a href="" class="i-view-grid-more"></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                </div>

        <input type="hidden" id="status_list" value="list">

                <div id="products" class="list-group">

                    <?php
                    foreach ($project_ids as $i => $pro_sub) {
                        $project_id = $pro_sub['project_id'];
                        $product_arr= Helper::getArrayProductOfProject($project_id);

                if(empty($_GET['zone_id'])){
                    $project = getProjectByID($project_id);
                }else{
                    $project = getProjectByIDZone_id($project_id,$_GET['zone_id']);
                }
                $banner  = getBannerByProjectID($project_id);
                $brand   = getBrandByID($project->brand_id);
                $price   = getProjectPrice($project_id);
                $project_concept = getProjectConcept($project_id);

                        $logo           = getProjectMaps($project_id);
                        $conceptOnly    = getProjectConceptOnly($project_id);

                        // filter by zone
                        if( empty($_GET['zone_id']) || (!empty($_GET['zone_id']) && $_GET['zone_id'] == $project->zone_id ) ){
                            ?>
                            <div class="projecttype-list">
                                <div class="row veralign-middle">
                                    <div class="col-md-8">

                                        <?php
                                            if(in_array(1,$product_arr) || in_array(2,$product_arr) || !in_array(3,$product_arr)){
                                                $url_see_more = isLadawan($project->project_id) ?
                                                    $router->generate('ladawan-detail',[
                                                        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                                        'product_id'    =>  1
                                                    ]) :
                                                    $router->generate('single-home-detail',[
                                                        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                                        'product_id'    =>  1
                                                    ]);
                                            }
                                            else{
                                                $url_see_more = isLadawan($project->project_id) ?
                                                    $router->generate('ladawan-detail',[
                                                        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                                        'product_id'    =>  3
                                                    ]) :
                                                    $router->generate('condominium-detail',[
                                                        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                                        'lang'          =>  'th'
                                                    ]);
                                            }
                                            ?>
                                        <?php
                                        if($project->project_status == 'NP'){
                                            ?>
                                            <div class="tagbox2">
                                                <div class="tag2">
                                                    New<br>Project
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>

                                        <?php
                                        if($banner != false && ( $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'image' || $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'banner' ) ){
                                            ?>
                                        <a href="<?= $url_see_more ?>">
                                        <img src="<?= backend_url('base',$banner->LEAD_IMAGE_PROJECT_FILE_NAME)?>"
                                             alt="<?= $banner->LEAD_IMAGE_PROJECT_FILE_SEO ?>">
                                        </a>
                                        <?php
                                        }elseif($banner != false && $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'youtube'){
                                        $lead_img = backend_url('base', $banner->banner_img_thum);
                                        $lead_youtube_url = str_replace('watch?v=', 'embed/', $banner->LEAD_IMAGE_PROJECT_FILE_NAME);
                                        ?>

                                            <a href="#" data-featherlight="#lbVdo<?= $i ?>">
                                                <span class="i-view-vdo"></span>
                                                <img src="<?= $lead_img ?>"
                                                     alt="<?= $banner->LEAD_IMAGE_PROJECT_FILE_SEO ?>" class="hm-highl">
                                            </a>
                                            <iframe class="lightbox" src="<?= $lead_youtube_url . "?autoplay=0" ?>"
                                                    width="1000"
                                                    height="560" id="lbVdo<?= $i ?>" style="border:none;" webkitallowfullscreen
                                                    mozallowfullscreen allowfullscreen></iframe>

                                        <?php
                                        } elseif ($banner != false && $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'vdo') {
                                        ?>
                                            <a id="ban_vdo<?= $i ?>">
                                                <span class="i-view-vdo"></span>
                                                <img src="<?= backend_url('base', $banner->banner_img_thum) ?>"
                                                     alt="<?= $banner->LEAD_IMAGE_PROJECT_FILE_SEO ?>" class="hm-highl">
                                            </a>
                                            <script>
                                                $(document).ready(function () {
                                                    $('#ban_vdo<?= $i ?>').click(function () {
                                                        $('#lbVdo<?= $i ?>').show();
                                                        $.featherlight($('#lbVdo<?= $i ?>'),{});
                                                        $('.featherlight-content #video_player<?= $i ?>')[0].play();
                                                        $('#lbVdo<?= $i ?>').hide();
                                                    });
                                                })
                                            </script>
                                            <div id="lbVdo<?= $i ?>" style="position:relative;width: 100%;display: none;">
                                                <video id='video_player<?= $i ?>' preload='none' controls>
                                                    <source src="<?= backend_url('base',$banner->LEAD_IMAGE_PROJECT_FILE_NAME) ?>" type="video/mp4">
                                                </video>
                                            </div>

                                        <?php } ?>


                                    </div>
                                    <div class="col-md-4">
                                        <div class="projecttype-descrp">
                                            <?php if(!empty($logo->project_logo)) {
                                                ?>
                                                <img src="<?= is_object($logo) ? backend_url('base', $logo->project_logo) : '' ?>" title="<?= $logo->project_logo_seo ?>" alt="<?= $logo->project_logo_seo ?>" class="lo-150">
                                                <?php
                                            }?>

                                            <p class="concept<?= $i ?> concept-in-list">
                                            <?=!empty($project_concept->concept_th) ? str_replace(array("\n\r", "\n","n/g"), '<br/>', $project_concept->concept_th) : ''?>
                                               <!--  <script type="text/javascript">
                                                    $('.concept<?= $i ?>').html(`<?php // !empty($project_concept->concept_th) ? $project_concept->concept_th : ''?>`.replace(/\r?\n/g, '<br/>'));
                                                </script> -->
                                            </p>
                                            <br>
                                            <?php if(!empty($price->project_price)) { ?>
                                            <p>
                                                <?php
                                                if(in_array(1,$product_arr) || in_array(2,$product_arr) || !in_array(3,$product_arr)){
                                                    ?>
                                                <?php 
                                                if($price->price_mode == '1'){
                                                    $mode_price = 'ราคาเริ่มต้น';
                                                }else{
                                                    $mode_price = 'ราคา';
                                                }
                                                ?>
                                                    <strong>
                                                        <?=$mode_price;?>
                                                        <?= Helper::getProjectPrice($price->project_price,$price->price_mode); ?>
                                                        <?= is_object($price) ? Helper::getPriceModeName($price->price_mode,$price->project_price) : 'ล้านบาท' ?>
                                                    </strong>
                                                    <?php
                                                }
                                                else{
                                                    ?>
                                                    <strong>
                                                        ราคาเริ่มต้น
                                                        <?php
                                                        $project_price   = getProjectPrice($project_id);
                                                        $priceCondo = Helper::getProjectPrice($project_price->project_price,$project_price->price_mode)
                                                        ?>
                                                        <?= $priceCondo ?>
                                                        <?= is_object($project_price) ? Helper::getPriceModeName($project_price->price_mode,$project_price->project_price) : 'ล้านบาท' ?>
                                                    </strong>
                                                    <?php
                                                }
                                                ?>
                                            </p>
                                        <?php }?>

                                            <a href="<?= $url_see_more ?>" class="btn btn-seemore">
                                                See more
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>

                </div>
            </div>

        </div>
    </div>


<!-- Zendesk Chat  -->
<!-- Start of lhcoco Zendesk Widget script -->
<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=2be331a5-605a-4a1f-86dc-1eef96e43cf0"> </script>
<!-- End of lhcoco Zendesk Widget script -->
<?php include('footer.php'); ?>