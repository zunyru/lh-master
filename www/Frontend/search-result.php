<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

$product_id = empty($request['product']) || $request['product']=='any-products' ? null : $request['product'];
$zone_id    = empty($request['zone']) || $request['zone']=='any-zones'  ? null : $request['zone'];
$brand_id   = !empty($request['brand']) ? $request['brand'] : null;
$project_id = !empty($_GET['project_id']) ? $_GET['project_id'] : null;



//die(var_dump([$product_id,$zone_id,$brand_id]));

$project_searchs = getSearchResultFull($product_id,$zone_id,$brand_id);

$project_searchs = $project_searchs == false ? [] : $project_searchs;

$project_ids = [];
foreach ($project_searchs as $key => $project_s) {
   $project_ids[]= $project_s->project_id;
}
Helper::sortByPrice($project_ids,'desc');
$project_searchs = $project_ids;

$_product_id = getProductsName($product_id);
$_zone_id = getZonesName($zone_id);
$_brand_id = getBrandsName($brand_id);  

$_GET['product_id'] = $_product_id != false ? $_product_id->product_id : NULL;
$_GET['zone_id'] = $_zone_id != false ? $_zone_id->zone_id : NULL;
$_GET['brand_id'] = $_brand_id != false ? $_brand_id->brand_id : NULL ;

?>
<?php include('header.php'); ?>
    <!-- JS -->
    <link rel="stylesheet" href="<?= file_path('css/search.css') ?>" type="text/css">
    <link rel="stylesheet" href="<?= file_path('css/masonry-block.css') ?>" type="text/css">
    <script src="<?= file_path('js/search-result.js') ?>"></script>
    <?php

    // echo $request['product']."This is var #1 <br>";
    // echo $request['zone']."This is var #2 <br>";
    // echo $request['brand']."This is var #3 <br>";

?>
<div id="content" class="content search-result">
    <div class="container">

        <?php
        $SEO = getSEOUrl($actual_link);
        if($actual_link == @$SEO->url_page){
            echo '<h1>'.$SEO->h1.'</h1>';
        }else{
            echo '<h1>SEARCH RESULT</h1>'; 
       }
       ?>
       <p class="title">พบ
        <?php if(!empty($_GET['product_id'])){
            $product_q = getProductByProductID($_GET['product_id']);
            echo $product_q->product_name_th;
        } ?>
        <?php
        if(!empty($_GET['brand_id'])){
            $brandObj   = getBrandByID($_GET['brand_id']);
            echo ' ' . count($project_searchs). ' โครงการจาก '.$brandObj->brand_name_th.' ที่น่าสนใจ ';
        }else{
            echo ' ' . count($project_searchs). ' โครงการที่น่าสนใจ ';
        }
        ?>
        <?php if(!empty($_GET['zone_id'])){
            $zone_q = getZoneByID($_GET['zone_id']);
            echo ' บนทำเล '.$zone_q->zone_name_th;
        } ?>
    </p>
    <a href="" id="searchResult" data-status="0"><i class="i-subm-search"></i>ค้นหาโครงการบนทำเลที่คุณสนใจ</a>
    <div class="masonry-lists">

                <?php
                foreach ($project_searchs as $i => $project){
                    //$project_id = $project->project_id;
                    $project_id = $project;
                    $project = getProjectByID($project_id);
                    $banner  = getBannerByProjectID($project_id);
                    $brand   = getBrandByID($project->brand_id);
                    $price   = getProjectPrice($project_id);
                    $project_promotion = getProjectPromotion($project_id);
                    ?>
                    <div class="list-item" >
                        <?php
                        if($project->project_status == 'NP'){
                            ?>
                            <div class="tagbox2 new-md">
                                <div class="tag2">
                                    New<br>Project
                                </div>
                            </div>
                            <?php
                        }elseif($project->project_status == 'SO'){
                            ?>
                            <div class="tagbox2_sold new-md">
                                <div class="tag2">
                                    Sold<br>Out
                                </div>
                            </div>
                            <?php
                        }
                        ?>

                        <div class="list-item-img">
                            <?php
                            if($banner != false && ($banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'image' || $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'banner')){
                                ?>
                                <img src="<?= backend_url('base',$banner->LEAD_IMAGE_PROJECT_FILE_NAME)?>" alt="">
                            <?php
                            }elseif($banner != false && $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'youtube'){
                                $lead_img = backend_url('base', $banner->banner_img_thum);
                                $lead_youtube_url = str_replace('watch?v=', 'embed/', $banner->LEAD_IMAGE_PROJECT_FILE_NAME);
                                ?>

                                <a href="#" data-featherlight="#lbVdo<?= $i ?>">
                                    <span class="i-view-vdo"></span>
                                    <img src="<?= $lead_img ?>"
                                         alt="<?= $banner->LEAD_IMAGE_PROJECT_FILE_SEO ?>" class="hm-highl">
                                </a>
                                <iframe class="lightbox" src="<?= $lead_youtube_url . "?autoplay=0" ?>"
                                        width="1000"
                                        height="560" id="lbVdo<?= $i ?>" style="border:none;" webkitallowfullscreen
                                        mozallowfullscreen allowfullscreen></iframe>

                            <?php
                            }elseif ($banner != false && $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'vdo') {
                                ?>
                                <a id="ban_vdo<?= $i ?>">
                                    <span class="i-view-vdo"></span>
                                    <img src="<?= backend_url('base', $banner->banner_img_thum) ?>"
                                         alt="<?= $banner->LEAD_IMAGE_PROJECT_FILE_SEO ?>" class="hm-highl">
                                </a>
                                <script>
                                    $(document).ready(function () {
                                        $('#ban_vdo<?= $i ?>').click(function () {
                                            $('#lbVdo<?= $i ?>').show();
                                            $.featherlight($('#lbVdo<?= $i ?>'),{});
                                            $('.featherlight-content #video_player<?= $i ?>')[0].play();
                                            $('#lbVdo<?= $i ?>').hide();
                                        });
                                    })
                                </script>
                                <div id="lbVdo<?= $i ?>" style="position:relative;width: 100%;display: none;">
                                    <video id='video_player<?= $i ?>' preload='none' controls>
                                        <source src="<?= backend_url('base',$banner->LEAD_IMAGE_PROJECT_FILE_NAME) ?>" type="video/mp4">
                                    </video>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                        <a href="
                        <?php
                            $products = getProducts();
                            $product_of_project = $_GET['product_id'];
                            if(empty($product_of_project)){
                                foreach ($products as $product){
                                    $project_sub = getProjectSub($project_id,$product->product_id);
                                    if($project_sub != false){
                                        $product_of_project = $product->product_id;
                                        break;
                                    }
                                }
                            }


                            if($product_of_project == 1 ){
                                $url_see_more = isLadawan($project->project_id) ?
                                    $router->generate('ladawan-detail',[
                                        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                        'product_id'    =>  1
                                    ]) :
                                    $router->generate('single-home-detail',[
                                        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                        'product_id'    =>  1
                                    ]);
                            }else if( $product_of_project == 2){
                                $url_see_more = isLadawan($project->project_id) ?
                                    $router->generate('town-home-detail',[
                                        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                        'product_id'    =>  2
                                    ]) :
                                    $router->generate('town-home-detail',[
                                        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                        'product_id'    =>  2
                                    ]);
                            } elseif( $product_of_project == 3){
                                $url_see_more = isLadawan($project->project_id) ?
                                    $router->generate('ladawan-detail',[
                                        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                        'product_id'    =>  3
                                    ]) :
                                    $router->generate('condominium-detail',[
                                        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                        'lang'          =>  'th'
                                    ]);
                            }else{
                                $url_see_more = isLadawan($project->project_id) ?
                                    $router->generate('ladawan-detail',[
                                        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                        'product_id'    =>  $product_of_project
                                    ]) :
                                    $router->generate('single-home-detail',[
                                        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                        'product_id'    =>  $product_of_project
                                    ]);
                            }
                            echo $url_see_more;
                        ?>">
                            <p class="title"><?= $project->project_name_th ?></p>
                            <p>
                                <?php 
                                if($price->price_mode == 1){
                                   echo "ราคาเริ่มต้น ";
                                }else{
                                   echo "ราคา ";
                                }
                                ?>
                                <?= Helper::getProjectPrice($price->project_price,$price->price_mode); ?>
                                <?= is_object($price) ? Helper::getPriceModeName($price->price_mode,$price->project_price) : 'ล้านบาท' ?>
                            </p>
                            <?php
                            if($project_promotion != false){
                                ?>
                                <p class="title">โปรโมชั่น : <span class="green"><?= $project_promotion->promotion_name_th ?></span></p>
                                <p><?=$project_promotion->promotion_detail_th ?></p>
                            <?php
                            }
                            ?>
                        </a>
                    </div>
                <?php
                }
                ?>

<!--                <div class="list-item">-->
<!--                    <div class="list-item-img">-->
<!--                        <img src="images/temp/msr_img_1.jpg" alt="">-->
<!--                    </div>-->
<!--                    <a href="">-->
<!--                        <p class="title">บ้านชัยพฤกษ์ บางนา กม.7</p>-->
<!--                        <p>ราคาเริ่มต้น 7.49 - 8.19 ล้านบาท</p>-->
<!--                        <p class="title">โปรโมชั่น : <span class="green">2 Day special</span></p>-->
<!--                        <p>2 วันแห่งความสุข ให้คุณเริ่มต้นชีวิต-->
<!--                            คุณภาพกับบ้านหลังใหม่ รับข้อเสนอ-->
<!--                            ราคาพิเศษสุดแห่งปี</p>-->
<!--                    </a>-->
<!--                </div>-->

            </div>
        </div>
    </div>


<?php include('footer.php'); ?>