<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

$page = 'lh-project';
$page_index = 0;
$img = '';
?>
<?php
include('header.php');

?>
<!-- CSS -->
<link rel="stylesheet" href="<?= file_path('css/lh-project.css') ?>" type="text/css">

<!-- JS -->
<script src="<?= file_path('js/lh-project.js') ?>"></script>

<div id="content" class="content lh-project-page">
    <div class="container">
     <?php
        $SEO = getSEOUrl($actual_link);
        if($actual_link == @$SEO->url_page){
            echo '<h1 class="heading-title">'.$SEO->h1.'<h1>';
        }else{
         echo '<h1 class="heading-title">LH Projects</h1>'; 
        }
     ?>
     

        <div id="lhp1" class="project-section lhslide">
            <p class="heading-title">Single Home</p>

            <div class="row">
                <div id="lhpSlide1" class="col-slide">

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_1.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">LADAWAN</p>
                                <p>
                                    Super Luxury single detached house<br>
                                    Start  50-160  MB
                                </p>
                            </a>
<!--                            <a href="#" class="btn btn-seemore">See more</a>-->
                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_2.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">Nuntawan</p>
                                <p>
                                    Luxury single detached house.<br>
                                    Start 20-50 MB
                                </p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_3.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">Mantana</p>
                                <p>
                                    Home for extending family in perfect environment and nature-filled atmosphere.<br>
                                    Start 6-20 MB
                                </p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_4.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">Siwalee</p>
                                <p>
                                    Home for extending family in perfect environment and nature-filled atmosphere on a special location.<br>
                                    Start 5 MB</p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_5.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">Chaiyapruek</p>
                                <p>
                                    Single home for start-up family. Ready made before sales with lush gardens and complete living facilities in project.<br>
                                    Start 6 MB
                                </p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_6.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">Phruek Lada</p>
                                <p>
                                    Single home for start-up family. Ready made before sales with lush gardens and complete living facilities in project.<br>
                                    Start 4 MB
                                </p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_7.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">Inizio</p>
                                <p>
                                    Single home for one family. Ready made before sales with comfortable facilities.<br>
                                    Start 3.6 MB
                                </p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_8.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">Villaggio</p>
                                <p>
                                    Single Home with new design for start-up family. Ready made before sales with lush gardens and complete living facilities in project.<br>
                                    Start 3 MB
                                </p>
                            </a>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="lhp2" class="project-section lhslide">
            <p class="heading-title">Town Home</p>

            <div class="row">
                <div id="lhpSlide2" class="col-slide">

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_9.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">Landmark</p>
                                <p>
                                    Luxury Townhome in a flexible location to offer unlimited exponential investment value near Ekamai-Ramindra.<br>
                                    Start 7.5-15 MB
                                </p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_10.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">Villaggio (Townhome)</p>
                                <p>
                                    Single home and Townhome of unequal beauty with small-town European atmosphere. Magnificent and comfortable with complete living facilities in project.<br>
                                    Start 2-5 MB
                                </p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_11.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">Indy</p>
                                <p>
                                    Beautiful Townhome with small-town European atmosphere. Magnificent and comfortable with complete living facilities in project.<br>
                                    Start 2.2-3 MB
                                </p>
                            </a>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="lhp3" class="project-section lhslide">
            <p class="heading-title">Condominium</p>

            <div class="row">
                <div id="lhpSlide3" class="col-slide">

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_12.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">The Bangkok</p>
                                <p>
                                    “Best in Life” Super Luxury Condominium from Land & Houses. The place where value is limitless and only you realize the utmost quality for your living.<br>
                                    Start 15 - 30 MB
                                </p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_13.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">The Room</p>
                                <p>
                                    “Real Life Real Living” Condominium in the feel of single city home near train transit. Not only meet your everyday practicality, but also fulfill the quality meaning to your life.<br>
                                    Start 5-16 MB
                                </p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_14.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">333 River side</p>
                                <p>
                                    Chaophraya river bend Condominium connected to BTS Sky Train with the best view in Kiak Kai area. Key growth location to empower your everyday living to its full potential.<br>
                                    Start 5.89 MB
                                </p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_15.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">The Key</p>
                                <p>
                                    Complete-living Condominium for life’s aspiring new journey. Flexible commuting routes with large facilities to meet every lifestyle.<br>
                                    Start 3.9 MB
                                </p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_16.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">Wan Vayla</p>
                                <p>“The Endless Vacation” We all want to be successful in life. But sometimes we forgot that success cannot  buy back the time spent pursuing it. How many of us have made “Wan Vayla” time for ourselves?  Welcome to Wan Vayla, your condominium by the sea in Hua Hin-Khao Tao. Relax amid a natural garden,  where sand, sea and sky meet.<br>
                                    Start 12 MB
                                </p>
                            </a>

                        </div>
                    </div>

                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="#">
                                    <img src="<?= file_path('images/lh-project/lhp_17.jpg') ?>" alt="" class="img100">
                                </a>
                            </div>
                            <a href="#">
                                <p class="title">North Condo</p>
                                <p>The most treasured happiness in the heart of Chiang Mai City.
                                    Experience the luxury condominium among the greenery and panoramic view of Doi Kham which stretches to reach Doi Suthep and encompasses over 100 rai area of magnificent mountain lake.
                                    <br>
                                    Start 2.4 MB</p>
                            </a>

                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>
</div>


<?php include('footer.php'); ?>
