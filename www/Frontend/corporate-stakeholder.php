<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>

<?php include('header.php'); ?>
<!-- CSS -->
<link rel="stylesheet" href="css/chart.css" type="text/css">



<div id="content" class="content chart-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">

                <h1>โครงสร้างการถือหุ้นของกลุ่มบริษัท</h1>

                <div class="chart-block">
                    <img src="images/investor/chart_2.jpg" alt="" class="chart-img">
                </div>

            </div>
        </div>
    </div>
</div>


<?php include('footer.php'); ?>
