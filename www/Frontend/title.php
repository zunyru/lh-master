<?php
if(isset($title_page)){

    $title       = $title_page;
    $description = str_replace('"',"'",$description_page);
    $image       = $image_page;
    $url         = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $url_cut     = (substr($url, 0, strpos($url, "?"))==''?$url:substr($url, 0, strpos($url, "?")));
    $keywords    = !isset($keywords) ? 'บ้านเดี่ยว, ทาวน์โฮม, คอนโด, คอนโดมิเนียม, โครงการบ้านใหม่' : $keywords;

}else{
   
   $title       = "บริษัท แลนด์ แอนด์ เฮ้าส์ จำกัด (มหาชน)";
   $description = "Land & Houses บริษัทธุรกิจอสังหาริมทรัพย์ในประเทศไทย LHเปิดขายทั้งโครงการบ้านเดี่ยว ทาวน์โฮม คอนโดมิเนียมในกรุงเทพฯ ปริมณฑลและต่างจังหวัด";
   $image       = "";
   $url         = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
   $url_cut     = (substr($url, 0, strpos($url, "?"))==''?$url:substr($url, 0, strpos($url, "?")));
   $keywords    = !isset($keywords) ? 'บ้านเดี่ยว, ทาวน์โฮม, คอนโด, คอนโดมิเนียม, โครงการบ้านใหม่' : $keywords;
}
?>


<!-- for Google title-->
<title><?=htmlspecialchars($title);?></title>
<meta name     ="description"         content="<?=htmlspecialchars($description);?>" />
<meta name     ="keywords"            content="<?=htmlspecialchars($keywords);?>" />

<!-- for Facebook title -->
<meta property ="fb:app_id"           content="<?= $app_id;?>" /> 
<meta property ="og:type"             content="website" /> 
<meta property ="og:url"              content="<?= $url_cut;?>" /> 
<meta property ="og:title"            content="<?= $title;?>" />
<meta property ="og:description"      content="<?= $description; ?>" />
<meta property ="og:site_name"        content="<?= $title;?>" /> 
<meta property ="og:image"            content="<?= $image; ?>" />
<meta property ="og:image:type"       content="image/jpeg" />

<meta property="og:image:width"       content="600" />
<!-- <meta property="og:image:height"      content="315" /> -->
<meta property="og:image:alt"         content="<?=$title;?>" /> 

<!-- for Twitter title-->
<meta name="twitter:card"           content="summary_large_image" />
<meta name="twitter:title"          content="<?= $title;?>" />
<meta name="twitter:description"    content="<?= $description; ?>" />
<meta name="twitter:creator"        content="@lhhome">
<meta name="twitter:site"           content="<?= $url;?>">
<meta name="twitter:image"          content="<?= $image; ?>" />
<meta itemprop="image"              content="<?= $image ?>" /> 

<?php ?>