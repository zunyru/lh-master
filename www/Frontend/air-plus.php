<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';
$page = 'airplus';
$page_index = 0;
$img = '';
?>
<?php
include('header.php');

?>
<!-- CSS -->
<link rel="stylesheet" href="<?= file_path('css/air-plus.css') ?>" type="text/css">
<!-- JS -->
<script src="<?= file_path('js/air-plus.js') ?>"></script>

<div class="page-banner">
    <div id="bannerSlide">
        <div class="item">
            <div class="banner-container banner-parallax">
                <img src="<?= file_path('images/temp3/air_1.jpg') ?>" alt="" class="vdo-banner">
                <video loop="loop" id="slideVdo" class="" style="display:none;">
                    <source src="<?= file_path('files/vdo/airplus.mp4') ?>" type="video/mp4">
                </video>

                <div class="vdo-control">
                    <div class="play-block">
                        <span id="vdoPlay" class="vdo-icon vdoControl vdoPlay"></span>
                        <span id="vdoPause" class="vdo-icon vdoControl vdoPause"></span>
                    </div>
                </div>
            </div>

<!--            <a href="#" data-featherlight="#lbVdo1">-->
<!--                <span class="i-view-vdo"></span>-->
<!--            </a>-->
<!--            <div class="banner-container banner-parallax" style="background-image: url(images/temp3/air_1.jpg);">-->
<!--            <iframe class="lightbox" src="https://www.youtube.com/embed/GJkPSkZ80bo" width="1000"-->
<!--                        height="560" id="lbVdo1" style="border:none;" webkitallowfullscreen-->
<!--                        mozallowfullscreen allowfullscreen></iframe>-->
<!--            </div>-->
        </div>
    </div>
</div>

                <div id="content" class="content airplus-page">
                    <div class="container">
                        <div class="airplus-content">

                            <?php
                            $SEO = getSEOUrl($actual_link);
                            if($actual_link == @$SEO->url_page){
                                echo '<h1 >'.$SEO->h1.'<h1>';
                            }else{
                               echo ' <h1>เทคโนโลยี Air Plus</h1>'; 
                           }
                           ?>
                           <div class="row">
                            <div class="col-md-4">
                                <img src="<?= file_path('images/temp3/air_02.jpg') ?>" alt="">
                            </div>
                            <div class="col-md-6 col-md-offset-1">
                                <p class="title">อากาศเสียภายในบ้านมาจากไหน</p>
                                <ul class="ap-li">
                                    <li>กิจกรรมต่างๆภายในบ้านล้วนสร้างพิษอากาศโดยไม่รู้ตัว เช่นกลิ่นของสีทาบ้านควันและไขมันที่เกิดจากการทำอาหาร หรือแม้กระทั่งสิ่งสกปรกที่มาพร้อมสัตว์เลี้ยง</li>
                                    <li>เปิดแอร์ทั้งวันลมเย็นๆจากเครื่องปรับอากาศนั้นแท้จริงแล้วคือการดึงอากาศเก่าภายในห้อง
                                    มาใช้แล้วปล่อยอากาศเก่าออกมาอีกครั้งเราจึงหายใจเอาอากาศเก่าเข้าปอดตลอดเวลา บางคนจึงรู้สึกไม่สดชื่นหรือรู้สึกนอนไม่เต็มอิ่มเมื่อเวลาตื่นนอน</li>
                                    <li>ปิดบ้านเป็นเวลานานๆ ปัญหานี้มักเกิดขึ้นเมื่อเราต้องปิดบ้านเพื่อเดินทางเป็นเวลานานๆ ทำให้
                                    อากาศภายในบ้านไม่สามารถถ่ายเท หรือเกิด "Dead air" นำมาซึ่งกลิ่นอับภายในบ้าน</li>
                                    <li>ความอับชื้นจากสภาพอากาศบ้านยุคใหม่มักหลีกเลี่ยงแดดและฝนด้วยการปิดบ้านอย่างมิดชิด
                                    ส่งผลให้บ้านเกิดความอับชื้น และกลายเป็นแหล่งเพาะเชื้อโรคชั้นดี เช่น เชื้อรา ไรฝุ่น เป็นต้น</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

    <div class="airplus-lists-container">
        <div class="container">
            <div class="airplus-lists">
                <div class="row">
                    <div class="col-md-4">
                        <div class="airplus-list">
                            <span class="apicon i-aplist-1"></span>
                            <p class="title">ทำงานอัตโนมัติ<br>
                                (Automatic Control)</p>
                            <p>เทคโนโลยี AirPlus ออกแบบให้ทำงานอัตโนมัติตลอด 24 ชม.    ผ่านพลังงานโซล่าเซลล์เพื่อให้อากาศใหม่ไหลเวียนภายในบ้าน
                                ตลอด 24 ชั่วโมงการใช้พลังงานโซล่าเซลล์นี้ยังช่วยประหยัด
                                การช้ไฟฟ้าทั้งยังเป็นมิตรกับสิ่งแวดล้อมนอกจากระบบถ่ายเท
                                อากาศจะทำงานอัตโนมัติแล้วผู้ใช้งานยังสามารถตั้งค่าเปิดปิด
                                การทำงานของระบบตามความต้องการผ่านอุปกรณ์
                                Controller Unit</p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="airplus-list">
                            <span class="apicon i-aplist-2"></span>
                            <p class="title">ระบบจัดการพลังงานอัจฉริยะ<br>
                                (Smart Power Manager)</p>
                            <p>1. <strong>Solar power</strong> พลังงานแสงอาทิตย์ ซึ่งเป็นพลังงานสะอาด<br><br>
                                2. <strong>Hybrid</strong> การใช้พลังงานไฟฟ้าแบบกระแสผสมระหว่างพลังงาน
                                แสงอาทิตย์และพลังงานไฟฟ้าภายในบ้าน<br><br>
                                3. <strong>Sufficient</strong> การใช้พลัง
                                งานจากไฟฟ้าภายในบ้านอย่างประหยัด</p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="airplus-list">
                            <span class="apicon i-aplist-3"></span>
                            <p class="title">ถ่ายเทอากาศได้เป็นอย่างดี<br>
                                (Well-planned ventilation)</p>
                            <p>ภายในบ้านติดตั้งท่อพัดลมดูดอากาศ (Ventilation tube) เพื่อดูด
                                อากาศภายในห้องผ่านทางท่อลมออกสู่ชายคาภายนอกบ้านจึง
                                ทำให้เกิดการแลกเปลี่ยนอากาศภายในบ้านอยู่ตลอดเวลา ช่วยลด
                                การสะสมความร้อนและความชื้นได้</p>
                        </div>
                    </div>
                </div>


                <span class="img-leafs"></span>
                <span class="img-girl"></span>
            </div>
        </div>
    </div>

</div>


<?php include('footer.php'); ?>
