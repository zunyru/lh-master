<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';
require_once 'include/help/query_independent.php';
require_once 'include/help/Helper.php';
require_once 'include/help/hex2rgba.php';
$page = 'home';
$page_index = 1;
?>
<?php
include('header.php');
//elementMetaTitleDisTag($page);
 ?>
    <!-- CSS -->
    <link rel="stylesheet" href="<?= file_path('css/home.css') ?>" type="text/css">
    <link rel="stylesheet" href="<?= file_path('css/video.css') ?>" type="text/css">

    <!-- JS -->
    <script src="<?= file_path('js/home.js') ?>"></script>
    <div class="page-banner page-scroll">
        <span id="scrollNext" class="i-scroll-next"></span>
        <div id="homeBanner" class="owl-carousel owl-theme">
            <?php

            $checkShowType  = getBannerByTypePage(1,true);
            $banners        = getBannerByTypePage(1);


            if($banners != false) {
                // image only
                if (empty($checkShowType)) {
                    //print_r($banners);
                    $num = getBannerByTypePage_MyBanner($page_index);
                    if($num == 0){
                        $type = 'd';
                    }else{
                        $type = 'p';
                    }
                    foreach ($banners as $i => $banner) {
                        if(($banner->lead_img_mobile != 'm') && ($banner->lead_img_mobile == $type)){
                            ?>
                            <div class="item leadDesktop">
                                <div class="banner-parallax banner-dktp">
                                <?=$banner->img_url != '' ? '<a href="'.$banner->img_url.'" target="_blank">' : ''; ?>
                                    <img src="<?= backend_url('base', $banner->lead_path) ?>" alt="<?=$banner->seo_lead?>">
                                <?=$banner->img_url != '' ? '</a>' : ''; ?>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    $num = getBannerByTypePage_MyBanner_MB($page_index,'d');
                    if($num == 0) {
                        foreach ($banners as $i => $banner) {
                            if ($banner->lead_img_mobile == 'm') {
                                ?>
                                <div class="item leadMobile ">
                                    <div class="banner-parallax">
                                       <?=$banner->img_url != '' ? '<a href="'.$banner->img_url.'" target="_blank">' : ''; ?>
                                           <img src="<?= backend_url('base', $banner->lead_path) ?>" alt="<?=$banner->seo_lead?>">
                                       <?=$banner->img_url != '' ? '</a>' : ''; ?>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                    }else{
                        $banners_MB     = getBannerByTypePage_MB(1,'h');
                        foreach ($banners_MB as $i => $banner_MB) {
                            if ($banner_MB->lead_img_mobile == 'm') {
                                ?>
                                <div class="item leadMobile">
                                    <div class="banner-parallax">
                                       <?=$banner_MB->img_url != '' ? '<a href="'.$banner_MB->img_url.'" target="_blank">' : ''; ?>
                                           <img src="<?= backend_url('base', $banner_MB->lead_path) ?>" alt="<?=$banner_MB->seo_lead?>">
                                       <?=$banner_MB->img_url != '' ? '</a>' : ''; ?>
                                    </div>
                                </div>
                                <?php
                            }
                        }

                    }
                }
                // youtube
                elseif ($checkShowType->type_show_id == 3) {
                    foreach ($banners as $i => $banner) {
                        $lead_img = backend_url('base', $banner->thumnail_path);
                        $lead_youtube_url = str_replace('watch?v=', 'embed/', $banner->url_vdo);
                        ?>
                        <div class="item">
                            <div class="item-video">
                                <div class="banner-container banner-parallax" id="bannerVideo<?= $i ?>">
                                    <div class="rps-vdobg">
                                        <img src="<?= $lead_img ?>" alt="<?=$banner->seo_main?>">
                                    </div>
                                    <video loop="loop" poster="<?= $lead_img ?>" id="slideVdo<?= $i ?>"
                                           class="slideVdo"></video>

                                    <div class="vdo-control">
                                        <div class="play-block">
                                            <span id="vdoPlay<?= $i ?>" class="vdo-icon vdoControl vdoPlay"></span>
                                        </div>
                                    </div>

                                    <script type="text/javascript">
                                        var iframe_src = '<?= $lead_youtube_url ?>';
                                        var youtube_video_id = iframe_src.match(/youtube\.com.*(\?v=|\/embed\/)(.{11})/).pop();
                                        if (youtube_video_id.length == 11) {
                                            $('#vdoPlay<?=$i?>').click(function () {
                                                $("#slideVdo<?=$i?>").remove();
                                                var video_iframe = $('' +
                                                    '<iframe id="video" width="100%" height="100%"' +
                                                    ' src="<?= $lead_youtube_url . "?autoplay=0" ?>">' +
                                                    '</iframe>');
                                                $('#bannerVideo<?=$i?>').find('.vdo-control').remove();
                                                $('#bannerVideo<?=$i?>').append(video_iframe);
                                                $(this).trigger('stop.autoplay.owl');
                                                $('#bannerVideo<?=$i?>').css( "height", "55vw");
                                                $('#bannerVideo<?=$i?> #video').css( "height", "100%");
                                                $('.rps-vdobg').hide();
                                            });
                                        }
                                    </script>
                                </div>
                            </div>

                        </div>
                        <?php
                        break;
                    }
                    ?>
                    <?php
                }
                // vdo
                elseif ($checkShowType->type_show_id == 2) {
                    foreach ($banners as $i => $banner) {
                        ?>
                        <div class="item">
                            <script>
                                $(document).ready(function () {
                                    $('#vdoPlay<?= $i ?>').click(function () {
                                        $('#slideVdo<?= $i ?>').css('display', 'block');
                                        $('#slideVdo<?= $i ?>')[0].play();
                                        $(this).fadeOut(200);
                                        $('#vdoPause<?= $i ?>').fadeIn(200);
                                        $('.rps-vdobg').fadeOut(200);
                                        $('.banner-container.banner-parallax img').css('display', 'none');

                                        $(".vdo-control").mouseenter(function (event) {
                                            event.stopPropagation();
                                            $('#vdoPause<?= $i ?>').addClass("btnshown");
                                        }).mouseleave(function (event) {
                                            event.stopPropagation();
                                            $('#vdoPause<?= $i ?>').removeClass("btnshown");
                                        });
                                    });

                                    $("#vdoPause<?= $i ?>").click(function () {
                                        $('#slideVdo<?= $i ?>').get(0).pause();
                                        $(this).fadeOut(200);
                                        $('#vdoPlay<?= $i ?>').fadeIn(200);
                                    });
                                });
                            </script>
                            <div class="banner-container banner-parallax">
                                <img src="<?= backend_url('base', $banner->thumnail_path) ?>" alt="<?=$banner->seo_main?>">
                                <video loop="loop" id="slideVdo<?= $i ?>" class="" style="display:none;">
                                    <source src="<?= backend_url('base', $banner->url_vdo) ?>" type="video/mp4">
                                </video>

                                <div class="vdo-control">
                                    <div class="play-block">
                                        <span id="vdoPlay<?= $i ?>" class="vdo-icon vdoControl vdoPlay"></span>
                                        <span id="vdoPause<?= $i ?>" class="vdo-icon vdoControl vdoPause"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        break;
                    }
                    ?>
                    <?php
                }
                // activity
                elseif ($checkShowType->type_show_id == 1) {

                    foreach ($banners as $i => $banner) {
                        ?>
                        <div class="item" id="bannerActivityDiv" <?php if (!empty($banner->url_activity)) echo " style=\"cursor:pointer\" "; ?>>
                            <?php
                            if (!empty($banner->url_activity)){
                                ?>
                                <input type="hidden" id="bannerActivityUrl" value="<?= $banner->url_activity ?>">
                                <?php
                            }
                            ?>
                            <?php
                            //print_r($banners);
                            foreach ($banners as $banner_act) {
                                 //print_r($banner_act);
                                if($banner_act->type_show_id == 1 ){
                                    if($banner_act->lead_img_mobile_act){
                                        ?>
                                         <div class="banner-parallax banner-dktp leadDesktop">
                                            <?php if(!empty($banner_act->pic_activity)) {?>
                                               <?=$banner_act->img_url != '' ? '<a href="'.$banner_act->img_url.'" target="_blank">' : ''; ?>
                                                   <img src="<?= backend_url('base', $banner_act->pic_activity) ?>" alt="<?=$banner_act->seo_lead?>">
                                               <?=$banner_act->img_url != '' ? '</a>' : ''; ?>
                                            <?php }?>
                                        </div>
                                        <div class="banner-parallax leadMobile">
                                            <?php if($banner_act->pic_activity){
                                                ?>
                                               <?=$banner_act->img_url != '' ? '<a href="'.$banner_act->img_url.'" target="_blank">' : ''; ?>
                                                     <img src="<?= backend_url('base', $banner_act->pic_activity) ?>" alt="<?=$banner_act->seo_lead?>">
                                                 <?=$banner_act->img_url != '' ? '</a>' : ''; ?>
                                                <?php
                                            } ?>
                                        </div>
                                        <?php
                                    }else{
                                        ?>
                                        <div class="banner-parallax banner-dktp leadDesktop">
                                            <?php if(!empty($banner_act->pic_activity)) {?>
                                               <?=$banner_act->img_url != '' ? '<a href="'.$banner_act->img_url.'" target="_blank">' : ''; ?>
                                                     <img src="<?= backend_url('base', $banner_act->pic_activity) ?>" alt="<?=$banner_act->seo_lead?>">
                                                 <?=$banner_act->img_url != '' ? '</a>' : ''; ?>
                                               </a>
                                            <?php }?>
                                        </div>
                                        <?php
                                    }
                                }
                                //$i++;
                            }?>

                            <?php if (!empty($banner->type_show_id) && $banner->type_show_id == 1) {
                                ?>
                                <div class="banner-activity"
                                    <?php if(!empty($banner->color_code_activity)) {
                                        ?>
                                        style="background-color: <?php echo hex2rgba($banner->color_code_activity, 0.8);?> ;"
                                        <?php
                                    }?>
                                >
                                    <?= $banner->text_activity_th ?>
                                    <?php if(!empty($banner->url_activity)) {
                                        ?>
                                        <span class="link-acty"></span>
                                        <?php
                                    }?>
                                    <span id="closeActivityDkpt" class="close-acty"></span>
                                </div>
                                <?php
                            }?>
                        </div>
                        <?php
                        break;
                    }
                    ?>
                    <?php
                }
            }
            ?>

        </div>

        <!--    hidden item for desktop-->
        <div id="hiddenLeadImageDsk" style="display: none;">
            <?php
            if($checkShowType->type_show_id == 1){
                foreach ($banners as $i => $banner){
                    if($banner->type_show_id == 0) {
                        ?>
                        <?php
                        if($banner->lead_img_mobile != 'm'){
                            ?>
                            <div class="item">
                                <div class="banner-parallax banner-dktp">
                                  <?=$banner->img_url != '' ? '<a href="'.$banner->img_url.'" target="_blank">' : ''; ?>
                                        <img src="<?= backend_url('base', $banner->lead_path) ?>" alt="<?=$banner->seo_lead?>">
                                  <?=$banner->img_url != '' ? '</a>' : ''; ?>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <?php
                    }
                }
            }
            ?>
        </div>

        <!--    hidden item for mobile-->
        <div id="hiddenLeadImageRsp" style="display: none;">
            <?php
            if($checkShowType->type_show_id == 1){
                foreach ($banners as $i => $banner){
                    if($banner->type_show_id == 0) {
                        ?>
                        <?php
                        if($banner->lead_img_mobile == 'm'){
                            ?>
                            <div class="item">
                                <div class="banner-parallax">
                                  <?=$banner->img_url != '' ? '<a href="'.$banner->img_url.'" target="_blank">' : ''; ?>
                                        <img src="<?= backend_url('base', $banner->lead_path) ?>" alt="<?=$banner->seo_lead?>">
                                  <?=$banner->img_url != '' ? '</a>' : ''; ?>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <?php
                    }
                }
            }
            ?>
        </div>

        <script>
            $(document).ready(function () {
                $('#closeActivityDkpt').click(function () {
                    renderBannerItem('hiddenLeadImageDsk');
                });
                $('#closeActivityRsp').click(function () {
                    renderBannerItem('hiddenLeadImageRsp');
                });

                function renderBannerItem(divLeadImage) {
                    var homeBanner = $('#homeBanner');
                    homeBanner.html('');
                    homeBanner.removeAttr('class');
                    homeBanner.append($('#'+divLeadImage).html());
                    homeBanner.trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
                    homeBanner.find('.owl-stage-outer').children().unwrap();
                    var isMulti = ($('#'+divLeadImage+' .item').length > 1) ? true : false;
                    homeBanner.owlCarousel({
                        loop: isMulti,
                        autoplay: true,
                        autoplayTimeout: 4000,
                        autoplaySpeed: 1000,
                        margin:5,
                        animateOut: 'fadeOut',
                        nav:false,
                        dots: isMulti,
                        video:true,
                        lazyLoad:true,
                        center:true,
                        responsive:{
                            0:{
                                items:1
                            },
                        }
                    });
                }
            });
        </script>

    </div>

<?php
if(!empty($banners)){
    foreach ($banners as $i => $banner){
        if(!empty($banner->type_show_id) && $banner->type_show_id == 1) {
            ?>
            <div class="banner-activity banner-rsp"
            <?= !empty($banner->url_activity) ? ' href="'.$banner->url_activity.'" ' : '' ?>
                <?php if(!empty($banner->color_code_activity)) {
                    ?>
	                style="background-color: <?php echo hex2rgba($banner->color_code_activity, 0.8);?> ;"
	                <?php
                }?>
            >
                <?= $banner->text_activity_th ?>
                <?php if(!empty($banner->url_activity)) {
                    ?>
                    <span class="link-acty"></span>
                    <?php
                }?>
                <span id="closeActivityRsp" class="close-acty"></span>
            </div>
            <?php
            break;
        }
    }
}
?>

<div id="content" class="content">
    <div class="container">
        <?php
        $highlights = getAllHighlight();
        if(!empty($highlights)) {
            ?>
            <div id="homeHighlight" class="home-section">
                <h1 class="heading-title">
                    <?php 
                    $SEO = getSEOUrl($actual_link);
                    if($actual_link == @$SEO->url_page){
                        echo $SEO->h1;
                    }else{
                        echo 'LH Highlights'; 
                 }
                 ?>
             </h1>
             <div class="row">
                <div id="homeHighlightSlide" class="slide-highlight">

                            <?php

                            foreach ($highlights as $i => $highlight) {
                                $i += 10;
                                if ($i < 20) {
                                    if ($highlight->highlights_lead_img_type == 'image') {
                                        ?>
                                        <div class="item">
                                            <div class="">
                                                <div class="col-md-8">
                                                        <?php
                                                        $highlight_url = str_replace(' ','',$highlight->highlight_link);
                                                        if(!empty($highlight_url)) {
                                                            $highlight_url = strpos($highlight_url, 'https://') !== false || strpos($highlight_url, 'http://') !== false ? $highlight_url : "http://$highlight_url";
                                                        }
                                                        ?>
                                                    <a <?= !empty($highlight_url) ? 'href="'.$highlight_url.'" target="_blank"' : '' ?> >
                                                    <img src="<?= backend_url('base', $highlight->highlights_lead_img) ?>"
                                                         alt="" class="hm-highl">
                                                    </a>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="highlight-descrp">
                                                        <a <?= !empty($highlight_url) ? 'href="'.$highlight_url.'" target="_blank"' : '' ?> >
                                                            <p class="title"><?= $highlight->highlights_name_th ?></p>
                                                            <p><?= $highlight->highlights_dis_th ?></p>
                                                        </a>
                                                        <?php if (!empty($highlight->highlight_link)) {
                                                            ?>
                                                            <a href="<?= $highlight_url ?>"
                                                               target="_blank"
                                                               class="btn btn-seemore" style="text-align:center">See
                                                                more</a>
                                                            <?php
                                                        } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    } elseif ($highlight->highlights_lead_img_type == 'youtube') {

                                        $lead_img = backend_url('base', $highlight->highlight_thumbnail);
                                        $lead_youtube_url = str_replace('watch?v=', 'embed/', $highlight->highlights_lead_img);

                                        ?>
                                        <div class="item">
                                            <div class="">
                                                <div class="col-md-8">
                                                    <a href="#" data-featherlight="#lbVdo<?= $i ?>">
                                                        <span class="i-view-vdo"></span>
                                                        <img src="<?= $lead_img ?>" alt="" class="hm-highl">
                                                    </a>
                                                    <iframe class="lightbox" src="<?= $lead_youtube_url . "?autoplay=0" ?>"
                                                            width="1000"
                                                            height="560" id="lbVdo<?= $i ?>" style="border:none;"
                                                            webkitallowfullscreen
                                                            mozallowfullscreen allowfullscreen></iframe>

                                                </div>
                                                <div class="col-md-4">
                                                    <div class="highlight-descrp">
                                                        <?php
                                                        $highlight_url = str_replace(' ','',$highlight->highlight_link);
                                                        if(!empty($highlight_url)) {
                                                            $highlight_url = strpos($highlight_url, 'https://') !== false || strpos($highlight_url, 'http://') !== false ? $highlight_url : "http://$highlight_url";
                                                        }
                                                        ?>
                                                        <a <?= !empty($highlight_url) ? 'href="'.$highlight_url.'" target="_blank"' : '' ?> >
                                                            <p class="title"><?= $highlight->highlights_name_th ?></p>
                                                            <p><?= $highlight->highlights_dis_th ?></p>
                                                        </a>
                                                        <?php if (!empty($highlight->highlight_link)) {
                                                            ?>
                                                            <a href="<?= $highlight_url ?>"
                                                               target="_blank"
                                                               class="btn btn-seemore" style="text-align:center">See
                                                                more</a>
                                                            <?php
                                                        } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    } elseif ($highlight->highlights_lead_img_type == 'vdo') {
                                        $lead_img = backend_url('base', $highlight->highlight_thumbnail);
                                        ?>

                                        <div class="item">
                                            <div class="">
                                                <div class="col-md-8">
                                                    <a id="ban_vdo<?= $i ?>">
                                                        <span class="i-view-vdo"></span>
                                                        <img src="<?= $lead_img ?>" class="hm-highl">
                                                    </a>
                                                    <script>
                                                        $(document).ready(function () {
                                                            $('#ban_vdo<?= $i ?>').click(function () {
                                                                $('#lbVdo<?= $i ?>').show();
                                                                $.featherlight($('#lbVdo<?= $i ?>'), {});
                                                                $('.featherlight-content #video_player<?= $i ?>')[0].play();
                                                                $('#lbVdo<?= $i ?>').hide();
                                                            });
                                                        })
                                                    </script>
                                                    <div id="lbVdo<?= $i ?>"
                                                         style="position:relative;width: 100%;display: none;">
                                                        <video id='video_player<?= $i ?>' preload='none' controls>
                                                            <source src="<?= backend_url('base', $highlight->highlights_lead_img) ?>"
                                                                    type="video/mp4">
                                                        </video>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="highlight-descrp">
                                                        <?php
                                                        $highlight_url = str_replace(' ','',$highlight->highlight_link);
                                                        if(!empty($highlight_url)) {
                                                            $highlight_url = strpos($highlight_url, 'https://') !== false || strpos($highlight_url, 'http://') !== false ? $highlight_url : "http://$highlight_url";
                                                        }
                                                        ?>
                                                        <a <?= !empty($highlight_url) ? 'href="'.$highlight_url.'" target="_blank"' : '' ?> >
                                                            <p class="title"><?= $highlight->highlights_name_th ?></p>
                                                            <p><?= $highlight->highlights_dis_th ?></p>
                                                        </a>
                                                        <?php if (!empty($highlight->highlight_link)) {
                                                            ?>
                                                            <a href="<?= $highlight_url ?>"
                                                               target="_blank"
                                                               class="btn btn-seemore" style="text-align:center">See
                                                                more</a>
                                                            <?php
                                                        } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <?php
            }


            $lh_project_reviews_all = getProjectReviewForHome_ALL();
            //var_dump($lh_project_reviews_all);
 if(!empty($lh_project_reviews_all)){
    ?>
    <div id="homeReview" class="home-section">
        <h1 class="heading-title" style="cursor: pointer;" onclick="linkToUrl('<?= $router->generate('review'); ?>')">
            
            <?php 
            $SEO = getSEOUrl($actual_link);
            if($actual_link == @$SEO->url_page){
                echo $SEO->h1;
            }else{
               echo 'Project Review'; 
            }
            ?>
        </h1>
        <div class="row">
            <div class="review-lists">
                <?php
                            //foreach ($lh_project_reviews as $i => $lh_project_review) {
                           for ($i=1; $i <= 6; $i++){
                                $lh_project_reviews = getProjectReviewForHome($i);

                                if (!empty($lh_project_reviews )) {
                                    if($lh_project_reviews[0]->custom_url!=''){
                                        $url_name = $lh_project_reviews[0]->custom_url; 
                                    }else{
                                        $url_name =$lh_project_reviews[0]->project_review_name_th;
                                    }
                                    $url_see_more = $router->generate('review-detail',[
                                        'review_name'  =>  str_replace(' ','-',$url_name),
                                        'review_id'    =>  $lh_project_reviews[0]->project_review_id
                                    ]);
                                    ?>
                                    <div class="list-item item">
                                        <div class="review-img">
                                            <a href="<?= $router->generate('review-detail',[
                                                    'review_name'   => str_replace(' ','-',$lh_project_reviews[0]->project_review_name_th),
                                                    'review_id'     => $lh_project_reviews[0]->project_review_id
                                            ]); ?>">
                                                <img src="<?= backend_url('base', $lh_project_reviews[0]->project_review_img) ?>" alt="">
                                            </a>
                                        </div>
                                        <a href="<?= $url_see_more ?>">
                                            <p class="title"><?= $lh_project_reviews[0]->project_review_name_th ?></p>
                                            <p><?= $lh_project_reviews[0]->project_review_dis_th ?></p>
                                        </a>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            $communities = getCommunity();
            if(!empty($communities)){
                ?>
                <div id="homeCommunity" class="home-section">
                    <h1 class="heading-title" style="cursor: pointer;" onclick="linkToUrl('<?= $router->generate('community'); ?>')" >LH Community</h1>
                    <div class="row">
                        <div class="hm-commu-lists">
                            <div id="homeCommunitySlide">
                                <?php

                                foreach ($communities as $i => $community) {
                                    $class_position_l = $i%2 == 0 ? '2' : '3';
                                    $class_position_r = $i%2 == 0 ? '1' : '4';
                                    $class_pos_l      = $i%2 == 0 ? 'right' : 'left';
                                    $class_pos_r      = $i%2 == 0 ? 'left' : 'right';
                                    if($i < 9){
                                        ?>
                                        <div class="item" style="cursor: pointer;" onclick="linkToUrl('<?= $router->generate('community',['community_name' => str_replace(' ','-',$community->community_name_th)]); ?>')">
                                            <div class="hm-commu-list">
                                                <div class="col-md-6 pull-<?= $class_pos_l ?>">
                                                    <div class="hm-commu-list-c<?= $class_position_l ?>">
                                                        <img src="<?= backend_url('base',$community->community_img) ?>" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 pull-<?= $class_pos_r ?>">
                                                    <div class="hm-commu-list-c<?= $class_position_r ?>">
                                                        <p class="title"><?= Helper::lang('th', $community->community_name_th, $community->community_name_en) ?></p>
                                                        <div><?= Helper::lang('th', $community->community_dis_th, $community->community_dis_en) ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>

                            </div>
                        </div>
                    </div>
                </div>
            <?php }
            $living_tips = getLivingTip();
            if(!empty($living_tips)){
                ?>

                <div id="homeTips" class="home-section">
                    <h1 class="heading-title" style="cursor: pointer;" onclick="linkToUrl('<?= $router->generate('living-tips'); ?>')">LH Living Tips</h1>
                    <div class="row">
                        <div id="homeTipsSlide" class="col-slide">

                            <?php

                            foreach ($living_tips as $i => $living_tip) {
                                if ($i < 5) {
                                    if(!empty($living_tip->custom_url)){
                                        $url_living = $living_tip->custom_url;
                                    }else{
                                        $url_living = $living_tip->living_tip_name_th;
                                    }
                                    ?>
                                    <div class="item">
                                        <div class="hm-tip">
                                            <div class="review-img">
                                                <a href="<?= $router->generate('living-tips-detail',['living_tip_name' => str_replace(' ','-',$living_tip->living_tip_name_th) ]); ?>">
                                                    <img src="<?= backend_url('base',$living_tip->living_tip_img) ?>" alt="" class="img100">
                                                </a>
                                            </div>
                                            <a href="<?= $router->generate('living-tips-detail',['living_tip_name' => str_replace(' ','-',$living_tip->living_tip_name_th) ]); ?>">
                                                <p class="title"><?= Helper::lang('th', $living_tip->living_tip_name_th, $living_tip->living_tip_name_en) ?></p>
                                                <p>
                                                    <?= Helper::lang('th', $living_tip->living_tip_content_th, $living_tip->living_tip_content_en) ?></p>
                                            </a>
                                            <a href="<?= $router->generate('living-tips-detail',['living_tip_name' =>str_replace(' ','-',$url_living) ]); ?>" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>

                        </div>
                    </div>
                </div>
            <?php }?>
        </div>
    </div>

    <style type="text/css">
        #homeHighlightSlide .owl-stage {
            transition: 0.6s !important;
        }
    </style>
<?php include('footer.php'); ?>