</div>

<footer id="footer">
    <div class="container">
        <div class="footer-container">
            <div class="ft-social">
                <ul>
                    <li><a href="https://www.facebook.com/landandhouses" class="ft-fb" target="_blank">facebook</a></li>
                    <li><a href="https://twitter.com/lhhome" class="ft-tw" target="_blank">twitter</a></li>
                    <li><a href="https://www.youtube.com/user/LandandHouses" class="ft-yt" target="_blank">youtube</a></li>
                    <li><a href="https://www.instagram.com/lhhome/" class="ft-ig" target="_blank">instagram</a></li>
                    <li><a href="#" class="ft-li onlymb" target="_blank">line</a></li>
                    <li>
                        <a   class="ft-li onlypc line">line</a>
                        <div class="line-share" data-status="0">
                            <span class="ficon-triangle"></span>
                            <h4>Line Official Account</h4>
                            <div>
                                <img src="<?= backend_url('icon_line','Line_logo.png') ?>" width="80" height="80">
                                <div>

                                    <a >Line ID
                                        <span>@landandhouses</span>
                                    </a>
                                </div>

                                <img width="80" height="80" src="<?php echo backend_url('icon_line','QR_Code.png') ?>">
                                <div>
                                    <a href="https://line.me/R/ti/p/%40yez1340o" target="_blank">Scan QR Code
                                        <span>LINE Official Account</span>
                                    </a>
                                </div>
                            </div>
                        </div> 
                    </li>
                    <li style="display: none;">
                        <a   class="ft-li onlypc wechat">We chat</a>
                        <div class="wechat-share" data-status="0" >
                            <span class="ficon-triangle"></span>
                            <h4>We chat Official Account</h4>
                            <div>
                                <img src="<?= backend_url('icon_line','Wechat_logo.png') ?>" width="80" height="80">
                                <div>

                                    <a >We chat
                                       <!--  <span>@landandhouses</span> -->
                                    </a>
                                </div>

                                <img width="80" height="80" src="<?php echo backend_url('icon_line','wechat_qr.jpg') ?>">
                                <div>
                                    <a href="https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=Mzg4NzE1NzAxNA==#wechat_redirect" target="_blank">Scan QR Code
                                        <span>Wechat Official Account</span>
                                    </a>
                                </div>
                            </div>
                        </div> 
                    </li>
                </ul>
            </div>
            <p class="copyright-txt">&copy; 2017 Land and Houses Public Company Limited. All right reserved</p>
            <!-- footer -->
            <?php
            $footer_ = getFOOTERUrl($actual_link,'');

            if($actual_link == @$footer_->url_page):
                echo $footer_->content;
            else:
                $footer_ = getFOOTERUrl('',1);
                echo $footer_->content;
            endif;
            ?>
            <!-- END footer -->
        </div>
    </div>
</footer>

<div class="float-share" data-status="0">
    <a href="" id="i-share">
        <span class="i-sharelists"><i></i></span>
    </a>

</div>
<div class="ficon-shares" >
    <span class="ficon-triangle"></span>
    <a class="facebook-shared"><i class="i-fb_new"></i></a>
    <a class="twitter-shared" ><i class="i-tw_new"></i></a>
    <a class="line-shared"><i class="ss-line"></i></a>
</div>    



<script src="https://d.line-scdn.net/r/web/social-plugin/js/thirdparty/loader.min.js" async="async" defer="defer"></script>



<a class="scroll-top"></a>

<div class="rps-footer-sticky">
    <?php 
    if(isset($project_id)){
        if(!is_null($project_id) && isset($page) || isset($project_searchs)){
            include('snippet/snippet-top-menuicon.php');
        }else{
            include('snippet/snippet-footer-menuicon-projectinfo.php');  
        }
    }else{
        include('snippet/snippet-top-menuicon.php');
    }
    ?>
</div>

</div>


</body>
</html>