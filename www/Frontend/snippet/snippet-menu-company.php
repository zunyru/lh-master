<div class="main-navigation">
    <ul id="main-nav">
        <li><a class="m-submenu-innerpage" data-name="info" data-status="0">ข้อมูลบริษัท</a>
            <div class="submenu-innerpage info">
                <div class="container">
                    <div class="row">
                        <div class="submenu-list">
                            <ul>
                                <li><a href="<?php url('/corporate-profile.php') ?>" data-family="child">ข้อมูลบริษัท</a></li>
                                <li><a href="<?php url('/') ?>" data-family="child">โครงสร้างองค์กร</a></li>
                                <li><a href="<?php url('/corporate-ethics.php') ?>" data-family="child">จรรยาบรรณของพนักงาน</a></li>
                                <li><a href="<?php url('/corporate-committee.php') ?>" data-family="child">คณะกรรมการ</a></li>
                                <li><a href="<?php url('/') ?>" data-family="child">โครงสร้างการถือหุ้น</a></li>
                                <li><a href="<?php url('/honor-awards.php') ?>" data-family="child">รางวัลเกียรติยศ</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li><a class="m-submenu-innerpage">ข้อมูลบรรษัทภิบาล</a></li>
        <li><a class="m-submenu-innerpage">ข้อมูลนักลงทุน</a></li>
        <li><a class="m-submenu-innerpage">ข่าวสารองค์กร</a></li>
    </ul>
</div>