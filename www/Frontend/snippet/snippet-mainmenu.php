<div class="submenu-bg">
    <div class="submenu-container">
        <div class="container">
            <div class="row">
                <div class="submenu-list">
                    <div class="col-md-7 col-md-offset-1">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="submenu-block">
                                    <p class="submenu-title">ข้อมูลโครงการ</p>
                                    <ul>
                                        <li><a href="<?= $router->generate('furnished-home-list') ?>">บ้านตกแต่งพร้อมขาย</a></li>
                                        <li><a href="<?= $router->generate('single-home-list') ?>">บ้านเดี่ยว</a></li>
                                        <li><a href="<?= $router->generate('town-home-list') ?>">ทาวน์โฮม</a></li>
                                        <li><a href="<?= $router->generate('condominium-list',[
                                                 'lang'         =>  'th'
                                        ]) ?>">คอนโดมิเนียม</a></li>
                                        <li><a href="<?= $router->generate('ladawan-list') ?>">LADAWAN</a></li>
                                        <li><a href="https://www.lh.co.th/vive">VIVE</a></li>
                                        <li><a href="<?= $router->generate('country-list') ?>">โครงการในต่างจังหวัด</a></li>
                                        <li><a href="<?= $router->generate('new-project-list') ?>">ข่าวโครงการใหม่</a></li>
                                        <li><a href="<?= $router->generate('promotion') ?>">โปรโมชั่น และสิทธิพิเศษ</a></li>
                                        <li><a href="<?= $router->generate('review') ?>">Project Review</a></li>
                                        <li><a href="<?= $router->generate('lh-project') ?>">Foreign Buyers</a></li>
                                        <li><a href="<?= $router->generate('tvc') ?>">TVC</a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="submenu-block">
                                            <p class="submenu-title">แนวคิดการออกแบบ</p>
                                            <ul>
                                                <li><a href="<?= $router->generate('home-series') ?>">แบบบ้าน</a></li>
                                                <li><a href="<?= $router->generate('360-virtual') ?>">ภาพถ่าย 360 องศา</a></li>
                                                <li><a href="<?= $router->generate('air-plus') ?>">เทคโนโลยี Air Plus</a></li>
                                                <!--  
                                                <li><a href="<?= $router->generate('concept') ?>">แนวคิดบ้านสบาย</a></li>
                                                -->
                                                <li><a href="<?= $router->generate('living-tips') ?>">LH Living Tips</a></li>
                                                <li ><a href="<?= $router->generate('motivo') ?>">นิตยสาร Motivo</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="submenu-block">
                                            <p class="submenu-title">ข้อมูลบริษัท</p>
                                            <ul>
                                                <li><a href="http://lh-th.listedcompany.com/corporate_information.html">ข้อมูลบริษัท</a></li>
                                                <li><a href="http://lh-th.listedcompany.com/right_shareholder.html">ข้อมูลบรรษัทภิบาล</a></li>
                                                <li><a href="http://lh-th.listedcompany.com/home.html">ข้อมูลนักลงทุน</a></li>
                                                <li><a href="http://lh-th.listedcompany.com/company_news.html">ข่าวสารองค์กร</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="submenu-block">
                                            <p class="submenu-title">ลูกบ้านสัมพันธ์</p>
                                            <ul>
                                                <li><a href="<?= $router->generate('community') ?>">LH Community</a></li>
                                                <li><a href="http://www7.lh.co.th/EService/LoginForm.do">แจ้งซ่อมออนไลน์</a></li>
                                                <li><a href="http://www7.lh.co.th/LHComplaint/index.jsp">ร้องเรียนเรื่องบ้านและคอนโด</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="submenu-block">
                                            <p class="submenu-title">ติดต่อเรา</p>
                                            <ul>
                                                <li><a href="<?= $router->generate('job-with-us') ?>">สมัครงาน</a></li>
                                                <li><a href="<?= $router->generate('land-offer') ?>">เสนอขายที่ดิน</a></li>
                                                <li><a href="<?= $router->generate('report') ?>">แจ้งปัญหาการใช้งานบนเว็บไซต์</a></li>
                                                <li hidden><a href="<?= $router->generate('contact-us') ?>">ติดต่อสอบถามข้อมูลทั่วไป</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="submenu-tool">
                    <div class="col-md-4">
                        <div class="submenu-tools">
                            <a href="" id="topSearch" class="menu-tool-list" data-status="0">
                                <i class="i-subm-search"></i>
                                ค้นหาโครงการบนทำเลที่คุณสนใจ
                            </a>
                            <a href="" id="topGetdirection" class="menu-tool-list" data-status="0">
                                <i class="i-subm-direction"></i>
                                ค้นหาเส้นทางไปยังโครงการที่คุณสนใจ
                            </a>
                           <!--  <a href="" id="topContact" class="menu-tool-list"> -->
                            <a href="<?= $router->generate('contact-us') ?>" class="menu-tool-list">
                                <i class="i-subm-email"></i>
                                สอบถามข้อมูลโครงการ
                            </a>
                            <a href="#" class="menu-tool-list" id="call" style="cursor:text;">
                                <i class="i-subm-call"></i>
                                โทร.1198<br>
                                <span>(ทุกวัน เวลา 09.00 - 17.30 น.)</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script  type="text/javascript" >
    $('#call').click(function(){

        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            window.location.href='tel:1198';
        }else{
            //alert('กรุณาติดต่อ : 1198')
        }

    });
</script>