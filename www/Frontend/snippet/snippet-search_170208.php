<?php
    $products   = getSearchSelectProduct();
    $zones      = getSearchSelectZone();
    $brands     = getSearchSelectBrand();
    $projects   = getSearchSelectProject();

?>
<div class="search-container topdropdown-container">
    <span class="panel-close">close</span>
    <script>
        $(document).ready(function () {
            $('#search_form').submit(function () {
                var product_id  = $('#product_id').val();
                var zone_id     = $('#zone_id').val();
                var brand_id    = $('#brand_id').val();
                if(product_id == '' && zone_id == '' && brand_id == ''){
                    return false;
                }
            });
        });
    </script>
    <form action="search-result.php" id="search_form">
        <h3>Search</h3>
        <p>ค้นหาโครงการที่คุณสนใจ</p>

        <div class="">
            <div class="projecttype-block search-block">
                <input type="hidden" id="product_id" name="product_id" value="">
                <div class="searchBlock" data-status="0">
                    <span>เลือกประเภทโครงการ</span>
                    <div class="search-icon-dropdown"><i class="i-search-triangle"></i></div>
                </div>
                <ul id="searchLists">
                    <?php
                    foreach ($products as $product){
                        ?>
                        <li data-id="<?= $product->product_id ?>" data-value="<?= $product->product_name_th ?>"><?= $product->product_name_th ?></li>
                    <?php
                    }
                    ?>
                </ul>
            </div>

            <div class="projectlocation-block search-block">
                <input type="hidden" id="zone_id" name="zone_id" value="">
                <div class="searchBlock" data-status="0">
                    <span>เลือกทำเลที่ตั้ง</span>
                    <div class="search-icon-dropdown"><i class="i-search-triangle"></i></div>
                </div>
                <ul id="projectlocationLists">
                    <?php
                    foreach ($zones as $zone){
                        ?>
                        <li data-id="<?= $zone->zone_id ?>" data-value="<?= $zone->zone_name ?>"><?= $zone->zone_name ?></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>

            <div class="projectbrand-block search-block">
                <input type="hidden" id="brand_id" name="brand_id" value="">
                <div class="searchBlock" data-status="0">
                    <span>เลือกแบรนด์</span>
                    <div class="search-icon-dropdown"><i class="i-search-triangle"></i></div>
                </div>
                <ul id="projectbrandLists">
                    <?php
                    foreach ($brands as $brand){
                        ?>
                        <li data-id="<?= $brand->brand_id ?>" data-value="<?= $brand->brand_name_th ?>"><?= $brand->brand_name_th ?></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>

            <div class="projectname-block search-block">
                <input type="hidden" id="project_id" name="project_id" value="">
                <div class="searchBlock" data-status="0">
                    <span>เลือกชื่อโครงการ</span>
                    <div class="search-icon-dropdown"><i class="i-search-triangle"></i></div>
                </div>
                <ul id="projectnameLists">
                    <?php
                    foreach ($projects as $project){
                        ?>
                        <li data-id="<?= $project->project_id ?>" data-value="<?= $project->project_name_th ?>"><?= $project->project_name_th ?></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>

            <button class="btn-search"><i></i> ค้นหา</button>
        </div>

    </form>
</div>
<script>
    $('#')
</script>