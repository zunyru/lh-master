<div class="main-navigation corporate-menu">
    <ul id="main-nav">
        <li><a class="m-submenu-innerpage<?php if($current_page == 'corporate-profile' || $current_page == 'corporate-organization-chart' || $current_page == 'corporate-ethics' || $current_page == 'corporate-committee'  || $current_page == 'corporate-committee-details' || $current_page == 'corporate-stakeholder' || $current_page == 'honor-awards') { echo ' active'; } ?>" data-name="profile" data-status="0">ข้อมูลบริษัท</a>
            <div class="submenu-innerpage profile">
                <div class="container">
                    <div class="row">
                        <div class="submenu-list">
                            <ul>
                                <li><a href="<?php url('/corporate-profile.php') ?>" data-family="child" class="<?php if($current_page == 'corporate-profile') { echo 'active'; } ?>">ข้อมูลบริษัท</a></li>
                                <li><a href="<?php url('/corporate-organization-chart.php') ?>" data-family="child" class="<?php if($current_page == 'corporate-organization-chart') { echo 'active'; } ?>">โครงสร้างองค์กร</a></li>
                                <li><a href="<?php url('/corporate-ethics.php') ?>" data-family="child" class="<?php if($current_page == 'corporate-ethics') { echo 'active'; } ?>">จรรยาบรรณของพนักงาน</a></li>
                                <li><a href="<?php url('/corporate-committee.php') ?>" data-family="child" class="<?php if($current_page == 'corporate-committee' || $current_page == 'corporate-committee-details') { echo 'active'; } ?>">คณะกรรมการ</a></li>
                                <li><a href="<?php url('/corporate-stakeholder.php') ?>" data-family="child" class="<?php if($current_page == 'corporate-stakeholder') { echo 'active'; } ?>">โครงสร้างการถือหุ้น</a></li>
                                <li><a href="<?php url('/honor-awards.php') ?>" data-family="child" class="<?php if($current_page == 'honor-awards') { echo 'active'; } ?>">รางวัลเกียรติยศ</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li><a class="m-submenu-innerpage<?php if($current_page == 'governance-rights-of-shareholders' || $current_page == 'governance-practice' || $current_page == 'governance-roles-of-stakeholders' || $current_page == 'governance-disclose' || $current_page == 'governance-responsible' || $current_page == 'governance-business-good' || $current_page == 'governance-business-good') { echo ' active'; } ?>" data-name="info" data-status="0">ข้อมูลบรรษัทภิบาล</a>
            <div class="submenu-innerpage info">
                <div class="container">
                    <div class="row">
                        <div class="submenu-list">
                            <ul>
                                <li><a href="<?php url('/governance-rights-of-shareholders.php') ?>" data-family="child" class="<?php if($current_page == 'governance-rights-of-shareholders') { echo 'active'; } ?>">สิทธิของผู้ถือหุ้น</a></li>
                                <li><a href="<?php url('/governance-practice.php') ?>" data-family="child" class="<?php if($current_page == 'governance-practice') { echo 'active'; } ?>">การปฏิบัติต่อผู้ถือหุ้นอย่างเท่าเทียมกัน</a></li>
                                <li><a href="<?php url('/governance-roles-of-stakeholders.php') ?>" data-family="child" class="<?php if($current_page == 'governance-roles-of-stakeholders') { echo 'active'; } ?>">สิทธิของผู้มีส่วนได้เสีย</a></li>
                                <li><a href="<?php url('/governance-disclose.php') ?>" data-family="child" class="<?php if($current_page == 'governance-disclose') { echo 'active'; } ?>">การเปิดเผยข้อมูลและความโปร่งใส</a></li>
                                <li><a href="<?php url('/governance-responsible.php') ?>" data-family="child" class="<?php if($current_page == 'governance-responsible') { echo 'active'; } ?>">ความรับผิดชอบของคณะกรรมการ</a></li>
                                <li><a href="<?php url('/governance-business-good.php') ?>" data-family="child" class="<?php if($current_page == 'governance-business-good') { echo 'active'; } ?>">การกำกับดูแลกิจการที่ดี</a></li>
                                <li><a href="<?php url('/files/pdf/20160518-lh-csr-report-2015-th.pdf') ?>" data-family="child" class="" target="_blank">รายงานความรับผิดชอบต่อสังคม</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li><a class="m-submenu-innerpage<?php if($current_page == 'investor-relation' || $current_page == 'financial-information' || $current_page == 'shareholder-information' || $current_page == 'stock-information' || $current_page == 'presentations' || $current_page == 'analyst-coverage' || $current_page == 'information-request') { echo ' active'; } ?>" data-name="investor" data-status="0">ข้อมูลนักลงทุน</a>
            <div class="submenu-innerpage investor">
                <div class="container">
                    <div class="row">
                        <div class="submenu-list">
                            <ul>
                                <li><a href="<?php url('/investor-relation.php') ?>" data-family="child" class="<?php if($current_page == 'investor-relation') { echo 'active'; } ?>">นักลงทุนสัมพันธ์</a></li>
                                <li><a href="<?php url('/financial-information.php') ?>" data-family="child" class="<?php if($current_page == 'financial-information') { echo 'active'; } ?>">ข้อมูลการเงิน</a></li>
                                <li><a href="<?php url('/shareholder-information.php') ?>" data-family="child" class="<?php if($current_page == 'shareholder-information') { echo 'active'; } ?>">ข้อมูลผู้ถือหุ้น</a></li>
                                <li><a href="<?php url('/stock-information.php') ?>" data-family="child" class="<?php if($current_page == 'stock-information') { echo 'active'; } ?>">ข้อมูลราคาหลักทรัพย์</a></li>
                                <li><a href="<?php url('/presentations.php') ?>" data-family="child" class="<?php if($current_page == 'presentations') { echo 'active'; } ?>">ข้อมูลนำเสนอ</a></li>
                                <li><a href="<?php url('/analyst-coverage.php') ?>" data-family="child" class="<?php if($current_page == 'analyst-coverage') { echo 'active'; } ?>">รายนามนักวิเคราะห์</a></li>
                                <li><a href="<?php url('/information-request.php') ?>" data-family="child" class="<?php if($current_page == 'information-request') { echo 'active'; } ?>">ติดต่อเพิ่มเติม</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li><a class="m-submenu-innerpage<?php if($current_page == 'corporate-news' || $current_page == 'corporate-newsroom-set' || $current_page == 'corporate-newsroom-press' || $current_page == 'corporate-newsroom-clippings') { echo ' active'; } ?>" data-name="news" data-status="0">ข่าวสารองค์กร</a>
            <div class="submenu-innerpage news">
                <div class="container">
                    <div class="row">
                        <div class="submenu-list">
                            <ul>
                                <li><a href="<?php url('/corporate-news.php') ?>" data-family="child" class="<?php if($current_page == 'corporate-news') { echo 'active'; } ?>">ข่าวสารองค์กร</a></li>
                                <li><a href="<?php url('/corporate-newsroom-set.php') ?>" data-family="child" class="<?php if($current_page == 'corporate-newsroom-set') { echo 'active'; } ?>">ข่าวแจ้งตลาดหลักทรัพย์</a></li>
                                <li><a href="<?php url('/corporate-newsroom-press.php') ?>" data-family="child" class="<?php if($current_page == 'corporate-newsroom-press') { echo 'active'; } ?>">ข่าวแจ้งสื่อมวลชน</a></li>
                                <li><a href="<?php url('/corporate-newsroom-clippings.php') ?>" data-family="child" class="<?php if($current_page == 'corporate-newsroom-clippings') { echo 'active'; } ?>">ข่าวจากสื่อสิ่งพิมพ์</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>
