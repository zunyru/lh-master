<div class="main-navigation service-menu">
    <ul id="main-nav">
        <li><a class="m-submenu-innerpage active" data-name="profile" data-status="0">แจ้งซ่อมบ้าน</a>
            <div class="submenu-innerpage profile">
                <div class="container">
                    <div class="row">
                        <div class="submenu-list">
                            <ul>
                                <li><a href="<?= $router->generate('service-online',['step' => 1]) ?>" data-family="child" class="<?php if($step == 1) { echo 'active'; } ?>">Home</a></li>
                                <li><a href="<?= $router->generate('service-online',['step' => 2]) ?>" data-family="child" class="<?php if($step == 2) { echo 'active'; } ?>">แจ้งซ่อมงานบ้าน</a></li>
                                <li><a href="<?= $router->generate('service-online',['step' => 3]) ?>" data-family="child" class="<?php if($step == 3 || $step == '3-success' || $step == '3-fail') { echo 'active'; } ?>">ข้อมูลส่วนตัว</a></li>
                                <li><a href="<?= $router->generate('service-online',['step' => 3]) ?>" data-family="child" class="<?php if($step == 3) { echo 'active'; } ?>">ข้อมูลบ้าน</a></li>
                                <li><a href="<?= $router->generate('service-online',['step' => 3]) ?>" data-family="child" class="<?php if($step == 3) { echo 'active'; } ?>">DIY</a></li>
                                <li><a href="" data-family="child" class="">Log Out</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>
