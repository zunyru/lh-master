<?php

$zones      = getSearchSelectZoneBydirection();
$projects   = getSearchSelectProjectBydirection();

?>
<div class="getdirection-container topdropdown-container">
    <span class="panel-close">close</span>
    <form id="getDirection" action="<?= $router->generate('get-direction') ?>">
        <h3>Get Direction</h3>
        <p>ค้นหาเส้นทางไปยังโครงการที่คุณสนใจ</p>

        <div class="">
            <div class="directlocation-block topdropdown-block">
                <input type="hidden" id="project_zone_id" name="zone_id" value="">
                <div class="searchBlock" data-status="0">
                    <span>ระบุทำเล</span>
                    <div class="search-icon-dropdown"><i class="i-search-triangle"></i></div>
                </div>
                <ul id="directlocationLists">
                    <?php
                    foreach ($zones as $zone){
                        ?>
                        <li data-id="<?= $zone->zone_id ?>" data-slug="<?= $zone->slug ?>" data-value="<?= $zone->zone_name ?>"><?= $zone->zone_name ?></li>
                        <?php
                    }
                    ?>
                </ul>
                <span id="validateZone" class="txt-error" style="display: none;">โปรดระบุ ทำเล</span>
            </div>

            <div class="directproject-block topdropdown-block">
                <input type="hidden" id="project_direction_id" name="project_id" value="">
                <input type="hidden" id="project_real_id" name="project_real_id" value="">
                <div class="searchBlock" data-status="0">
                    <span>ระบุชื่อโครงการ</span>
                    <div class="search-icon-dropdown"><i class="i-search-triangle"></i></div>
                </div>
                <ul id="directprojectLists">
                    <?php
                    foreach ($projects as $project){
                        ?>
                        <li data-id="<?= $project->project_id ?>" data-slug="<?= $project->map_link ?>" data-value="<?= $project->project_name_th ?>"><?= $project->project_name_th ?></li>
                        <?php
                    }
                    ?>
                </ul>
                <span id="validateProjectName" class="txt-error" style="display: none;">โปรดระบุ ชื่อโครงการ</span>
            </div>

            <button class="btn-search"><i></i> ค้นหา</button>

        </div>

    </form>
</div>