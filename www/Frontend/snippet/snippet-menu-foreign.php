<div class="main-navigation">
    <ul id="main-nav" class="foreign-nav">
        <?php
        $menu = !empty($_GET['menu']) ? $_GET['menu'] : '';
        $lang = !empty($_GET['lang']) ? $_GET['lang'] : '';
        ?>
        <li><a href="<?= $router->generate('lh-project') ?>" class="<?= $menu == 'lh-project' ? 'active' : '' ?>">LH Projects </a></li>
        <li><a href="https://www.lh.co.th/thebestcondominiumsinbangkok/" >|  LH Condominiums | </a></li>
        <li><a href="<?= $router->generate('foreign-owner') ?>" class="<?= $menu == 'foreign-owner' ? 'active' : '' ?>">Foreign Ownership of Real Estate Property</a></li>
    </ul>
</div>