<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>
<?php include('header.php'); ?>
    <!-- CSS -->
<link rel="stylesheet" href="<?= file_path('css/complain.css') ?>" type="text/css">

    <div class="page-banner-md banner-hide">
        <div id="bannerSlide">
            <div class="item">
                <div class="banner-parallax">
                    <img src="<?= file_path('images/temp2/banner_complain.jpg') ?>" alt="">
                </div>
            </div>
        </div>
    </div>

    <div id="content" class="content rps-content-pd">
        <div class="container">
            <h1 class="heading-title">ร้องเรียนเรื่องบ้านและคอนโด</h1>

            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div>
<!--                       <p class="title banner-hide">คุณสามารถค้นหาชื่อโครงการที่คุณอาศัย และส่งข้อความร้องเรียนบ้านและคอนโดมาที่ แลนด์ แอนด์เฮ้าส์</p>-->
                        <div class="complain-form-block">
                            <form action="" class="complain-form form-container">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="step-label">
                                            step 4
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <p class="title">เรื่องที่ร้องเรียน</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form step4">
                                        <div class="col-md-3">
                                            <div class="project-name">
                                                <p class="title">
                                                    ชื่อโครงการ<br>
                                                    <span>ชัยพฤกษ์ วัชรพล</span>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="radio-block">
                                                                <input type="radio" id="topic1" name="complainTopic" checked/>
                                                                <label for="topic1"><span></span>งานก่อสร้างบ้านของท่าน</label>
                                                            </div>

                                                            <div class="radio-block">
                                                                <input type="radio" id="topic2" name="complainTopic" />
                                                                <label for="topic2"><span></span>งานสวนในบ้าน</label>
                                                            </div>

                                                            <div class="radio-block">
                                                                <input type="radio" id="topic3" name="complainTopic" />
                                                                <label for="topic3"><span></span>งานสาธารณูปโภคและบริการสาธารณะ</label>
                                                            </div>

                                                            <div class="radio-block">
                                                                <input type="radio" id="topic4" name="complainTopic" />
                                                                <label for="topic4"><span></span>การบริการของเจ้าหน้าที่</label>
                                                            </div>

                                                            <div class="radio-block">
                                                                <input type="radio" id="topic5" name="complainTopic" />
                                                                <label for="topic5"><span></span>รักษาความปลอดภัย</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="radio-block">
                                                                <input type="radio" id="topic6" name="complainTopic" />
                                                                <label for="topic6"><span></span>ความเป็นอยู่ในโครงการ</label>
                                                            </div>

                                                            <div class="radio-block">
                                                                <input type="radio" id="topic7" name="complainTopic" />
                                                                <label for="topic7"><span></span>ขอคำแนะนำ ขอแบบบ้าน CD</label>
                                                            </div>

                                                            <div class="radio-block">
                                                                <input type="radio" id="topic8" name="complainTopic" />
                                                                <label for="topic8"><span></span>จดหมายขอบคุณ</label>
                                                            </div>

                                                            <div class="radio-block">
                                                                <input type="radio" id="topic9" name="complainTopic" />
                                                                <label for="topic9"><span></span>อื่นๆ</label>
                                                                <input type="text" name="" class="form-control other-complain" placeholder="ระบุรายละเอียด">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-offset-1 col-md-3 pdr0">
                                            <a href="<?= $router->generate('complain-step',['step' => 5]) ?>" class="btn btn-submit next right">Next</a>
                                        </div>
                                    </div>
                                </div>

                            </form>

                            <a href="<?= $router->generate('complain-step',['step' => 3]) ?>" class="step-back">< Back</a>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


<?php include('footer.php'); ?>
