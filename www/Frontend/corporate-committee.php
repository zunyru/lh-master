<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>
<?php include('header.php'); ?>
<!-- CSS -->
<link rel="stylesheet" href="css/corporate-all.css" type="text/css">
<link rel="stylesheet" href="css/corporate-menu.css" type="text/css">

<!-- JS -->
<script src="js/corporate-menu.js"></script>

<div id="content" class="content committee-page corp-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div id="corpMenu" class="corp-menu-tab">
                    <ul>
                        <li class="current"><a href="#corpBlock1">คณะกรรมการบริหาร</a></li>
                        <li><a href="#corpBlock2">คณะกรรมการตรวจสอบ</a></li>
                        <li><a href="#corpBlock3">คณะกรรมการสรรหาและพิจารณาคำตอบแทน</a></li>
                        <li><a href="#corpBlock4">คณะกรรมการบริหารความเสี่ยง</a></li>
                        <li><a href="#corpBlock5">คลังรูปผู้บริหาร</a></li>
                    </ul>
                </div>

                <div id="corpBlock1" class="corp-block current">
                    <h1>คณะกรรมการบริหาร</h1>

                    <div class="committee-lists">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_1.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายอนันต์ อัศวโภคิน</p>
                                            <p>
                                                ประธานคณะกรรมการบริษัท<br>
                                                ประธานกรรมการบริหาร<br>
                                                กรรมการผู้มีอำนาจลงนามผูกพัน
                                            </p>
                                            <a href="<?php url('/corporate-committee-details.php') ?>" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_1.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายอนันต์ อัศวโภคิน</p>
                                            <p>
                                                ประธานคณะกรรมการบริษัท<br>
                                                ประธานกรรมการบริหาร<br>
                                                กรรมการผู้มีอำนาจลงนามผูกพัน
                                            </p>
                                            <a href="<?php url('/corporate-committee-details.php') ?>" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_2.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายนพร สุนทรจิตต์เจริญ</p>
                                            <p>
                                                กรรมการผู้อำนวยลงนามผูกพัน<br>
                                                กรรมการบริหาร และกรรมการผู้จัดการ
                                            </p>
                                            <a href="<?php url('/corporate-committee-details.php') ?>" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_2.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายนพร สุนทรจิตต์เจริญ</p>
                                            <p>
                                                กรรมการผู้มีอำนาจลงนามผูกพัน<br>
                                                กรรมการบริหาร และกรรมการผู้จัดการ
                                            </p>
                                            <a href="<?php url('/corporate-committee-details.php') ?>" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_3.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายอดิศร ธนนันท์นราพูล</p>
                                            <p>
                                                กรรมการผู้มีอำนาจลงนามผูกพัน<br>
                                                กรรมการบริหาร และกรรมการผู้จัดการ
                                            </p>
                                            <a href="<?php url('/corporate-committee-details.php') ?>" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_3.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายอดิศร ธนนันท์นราพูล</p>
                                            <p>
                                                กรรมการผู้มีอำนาจลงนามผูกพัน<br>
                                                กรรมการบริหาร และกรรมการผู้จัดการ
                                            </p>
                                            <a href="<?php url('/corporate-committee-details.php') ?>" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_4.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายเฉลิม เกียรติธนะบำรุง</p>
                                            <p>
                                                กรรมการรองกรรมการผู้จัดการ<br>
                                                ผู้บริหารสูงสุดด้านการปฏิบัติการ
                                            </p>
                                            <a href="<?php url('/corporate-committee-details.php') ?>" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_4.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายเฉลิม เกียรติธนะบำรุง</p>
                                            <p>
                                                กรรมการรองกรรมการผู้จัดการ<br>
                                                ผู้บริหารสูงสุดด้านการปฏิบัติการ
                                            </p>
                                            <a href="<?php url('/corporate-committee-details.php') ?>" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="corpBlock2" class="corp-block">
                    <h1>คณะกรรมการตรวจสอบ</h1>
                    <div class="committee-lists">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_1.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายอนันต์ อัศวโภคิน</p>
                                            <p>
                                                ประธานคณะกรรมการบริษัท<br>
                                                ประธานกรรมการบริหาร<br>
                                                กรรมการผู้มีอำนาจลงนามผูกพัน
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_1.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายอนันต์ อัศวโภคิน</p>
                                            <p>
                                                ประธานคณะกรรมการบริษัท<br>
                                                ประธานกรรมการบริหาร<br>
                                                กรรมการผู้มีอำนาจลงนามผูกพัน
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_2.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายนพร สุนทรจิตต์เจริญ</p>
                                            <p>
                                                กรรมการผู้อำนวยลงนามผูกพัน<br>
                                                กรรมการบริหาร และกรรมการผู้จัดการ
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_2.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายนพร สุนทรจิตต์เจริญ</p>
                                            <p>
                                                กรรมการผู้มีอำนาจลงนามผูกพัน<br>
                                                กรรมการบริหาร และกรรมการผู้จัดการ
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_3.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายอดิศร ธนนันท์นราพูล</p>
                                            <p>
                                                กรรมการผู้มีอำนาจลงนามผูกพัน<br>
                                                กรรมการบริหาร และกรรมการผู้จัดการ
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_3.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายอดิศร ธนนันท์นราพูล</p>
                                            <p>
                                                กรรมการผู้มีอำนาจลงนามผูกพัน<br>
                                                กรรมการบริหาร และกรรมการผู้จัดการ
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_4.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายเฉลิม เกียรติธนะบำรุง</p>
                                            <p>
                                                กรรมการรองกรรมการผู้จัดการ<br>
                                                ผู้บริหารสูงสุดด้านการปฏิบัติการ
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_4.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายเฉลิม เกียรติธนะบำรุง</p>
                                            <p>
                                                กรรมการรองกรรมการผู้จัดการ<br>
                                                ผู้บริหารสูงสุดด้านการปฏิบัติการ
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="corpBlock3" class="corp-block">
                    <h1>คณะกรรมการสรรหาและพิจารณาคำตอบแทน</h1>

                    <div class="committee-lists">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_1.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายอนันต์ อัศวโภคิน</p>
                                            <p>
                                                ประธานคณะกรรมการบริษัท<br>
                                                ประธานกรรมการบริหาร<br>
                                                กรรมการผู้มีอำนาจลงนามผูกพัน
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_1.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายอนันต์ อัศวโภคิน</p>
                                            <p>
                                                ประธานคณะกรรมการบริษัท<br>
                                                ประธานกรรมการบริหาร<br>
                                                กรรมการผู้มีอำนาจลงนามผูกพัน
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_2.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายนพร สุนทรจิตต์เจริญ</p>
                                            <p>
                                                กรรมการผู้อำนวยลงนามผูกพัน<br>
                                                กรรมการบริหาร และกรรมการผู้จัดการ
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_2.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายนพร สุนทรจิตต์เจริญ</p>
                                            <p>
                                                กรรมการผู้มีอำนาจลงนามผูกพัน<br>
                                                กรรมการบริหาร และกรรมการผู้จัดการ
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_3.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายอดิศร ธนนันท์นราพูล</p>
                                            <p>
                                                กรรมการผู้มีอำนาจลงนามผูกพัน<br>
                                                กรรมการบริหาร และกรรมการผู้จัดการ
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_3.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายอดิศร ธนนันท์นราพูล</p>
                                            <p>
                                                กรรมการผู้มีอำนาจลงนามผูกพัน<br>
                                                กรรมการบริหาร และกรรมการผู้จัดการ
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_4.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายเฉลิม เกียรติธนะบำรุง</p>
                                            <p>
                                                กรรมการรองกรรมการผู้จัดการ<br>
                                                ผู้บริหารสูงสุดด้านการปฏิบัติการ
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_4.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายเฉลิม เกียรติธนะบำรุง</p>
                                            <p>
                                                กรรมการรองกรรมการผู้จัดการ<br>
                                                ผู้บริหารสูงสุดด้านการปฏิบัติการ
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="corpBlock4" class="corp-block">
                    <h1>คณะกรรมการบริหารความเสี่ยง</h1>

                    <div class="committee-lists">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_1.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายอนันต์ อัศวโภคิน</p>
                                            <p>
                                                ประธานคณะกรรมการบริษัท<br>
                                                ประธานกรรมการบริหาร<br>
                                                กรรมการผู้มีอำนาจลงนามผูกพัน
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_1.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายอนันต์ อัศวโภคิน</p>
                                            <p>
                                                ประธานคณะกรรมการบริษัท<br>
                                                ประธานกรรมการบริหาร<br>
                                                กรรมการผู้มีอำนาจลงนามผูกพัน
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_2.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายนพร สุนทรจิตต์เจริญ</p>
                                            <p>
                                                กรรมการผู้อำนวยลงนามผูกพัน<br>
                                                กรรมการบริหาร และกรรมการผู้จัดการ
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_2.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายนพร สุนทรจิตต์เจริญ</p>
                                            <p>
                                                กรรมการผู้มีอำนาจลงนามผูกพัน<br>
                                                กรรมการบริหาร และกรรมการผู้จัดการ
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_3.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายอดิศร ธนนันท์นราพูล</p>
                                            <p>
                                                กรรมการผู้มีอำนาจลงนามผูกพัน<br>
                                                กรรมการบริหาร และกรรมการผู้จัดการ
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_3.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายอดิศร ธนนันท์นราพูล</p>
                                            <p>
                                                กรรมการผู้มีอำนาจลงนามผูกพัน<br>
                                                กรรมการบริหาร และกรรมการผู้จัดการ
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_4.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายเฉลิม เกียรติธนะบำรุง</p>
                                            <p>
                                                กรรมการรองกรรมการผู้จัดการ<br>
                                                ผู้บริหารสูงสุดด้านการปฏิบัติการ
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_4.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายเฉลิม เกียรติธนะบำรุง</p>
                                            <p>
                                                กรรมการรองกรรมการผู้จัดการ<br>
                                                ผู้บริหารสูงสุดด้านการปฏิบัติการ
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="corpBlock5" class="corp-block">
                    <h1>คลังรูปผู้บริหาร</h1>

                    <div class="committee-lists">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_1.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายอนันต์ อัศวโภคิน</p>
                                            <p>
                                                ประธานคณะกรรมการบริษัท<br>
                                                ประธานกรรมการบริหาร<br>
                                                กรรมการผู้มีอำนาจลงนามผูกพัน
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_1.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายอนันต์ อัศวโภคิน</p>
                                            <p>
                                                ประธานคณะกรรมการบริษัท<br>
                                                ประธานกรรมการบริหาร<br>
                                                กรรมการผู้มีอำนาจลงนามผูกพัน
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_2.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายนพร สุนทรจิตต์เจริญ</p>
                                            <p>
                                                กรรมการผู้อำนวยลงนามผูกพัน<br>
                                                กรรมการบริหาร และกรรมการผู้จัดการ
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_2.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายนพร สุนทรจิตต์เจริญ</p>
                                            <p>
                                                กรรมการผู้มีอำนาจลงนามผูกพัน<br>
                                                กรรมการบริหาร และกรรมการผู้จัดการ
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_3.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายอดิศร ธนนันท์นราพูล</p>
                                            <p>
                                                กรรมการผู้มีอำนาจลงนามผูกพัน<br>
                                                กรรมการบริหาร และกรรมการผู้จัดการ
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_3.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายอดิศร ธนนันท์นราพูล</p>
                                            <p>
                                                กรรมการผู้มีอำนาจลงนามผูกพัน<br>
                                                กรรมการบริหาร และกรรมการผู้จัดการ
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_4.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายเฉลิม เกียรติธนะบำรุง</p>
                                            <p>
                                                กรรมการรองกรรมการผู้จัดการ<br>
                                                ผู้บริหารสูงสุดด้านการปฏิบัติการ
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comittee-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="images/temp4/cmt_4.jpg" alt="" class="">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="cmt-name">นายเฉลิม เกียรติธนะบำรุง</p>
                                            <p>
                                                กรรมการรองกรรมการผู้จัดการ<br>
                                                ผู้บริหารสูงสุดด้านการปฏิบัติการ
                                            </p>
                                            <a href="" class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<?php include('footer.php'); ?>
