<?php

require_once dirname(dirname(__FILE__)).'/db/dbCon_mssql.php';
require_once 'kint-master/Kint.class.php';
require_once 'Helper.php';

function getCommunity() {
    try {
        $sql = "SELECT * 
                FROM LH_COMMUNITY_PAGE
                ORDER BY community_update DESC";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getCommunityByName($community_name_th) {
    try {
        $community_name_th = preg_replace('~[\\\\/:*?"<>|]~', ' ', $community_name_th);
        $sql = "SELECT * 
                FROM LH_COMMUNITY_PAGE
                WHERE replace(community_name_th, ' ', '-') LIKE '%".$community_name_th."%'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getLivingTip($except_living_tip_id = '') {
    try {
        $sql = "SELECT *
                FROM LH_LIVING_TIPS tips
 				WHERE CONVERT(date, getdate()) >= tips.living_tip_post_date ";
        if(!empty($except_living_tip_id)){
            $sql .= " and living_tip_id != '".$except_living_tip_id."' ";
        }
        $sql   .= " ORDER BY tips.update_date DESC ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getLivingTip_other($except_living_tip_id = '') {
    try {
        $sql = "SELECT TOP(10) *
                FROM LH_LIVING_TIPS tips
 				WHERE CONVERT(date, getdate()) >= tips.living_tip_post_date ";
        if(!empty($except_living_tip_id)){
            $sql .= " and living_tip_id != '".$except_living_tip_id."' ";
        }
        $sql   .= " ORDER BY tips.update_date DESC ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}
function getLivingTipDetail($living_tip_id)
{
    try {
        $sql = "SELECT *
                FROM LH_LIVING_TIPS
				where LH_LIVING_TIPS.living_tip_id = '".$living_tip_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getLivingTipByName($living_tip_name_th)
{
    try {
        $living_tip_name_th = preg_replace('~[\\\\/:*?"<>|]~', ' ', $living_tip_name_th);
        $sql = "SELECT *
                FROM LH_LIVING_TIPS
				where (replace(custom_url, ' ', '-') = '".$living_tip_name_th."') or (replace(LH_LIVING_TIPS.living_tip_name_th, ' ', '-') LIKE '%".$living_tip_name_th."%')";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getNewsJobByName($title)
{
    try {
        $sql = "SELECT *
                FROM JOB_NEWS
                where replace(JOB_NEWS.TITLE, ' ', '-') = '".$title."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}
function getLandingPageDetail($landing_page_id)
{
    try {
        $sql = "SELECT * 
                FROM LH_LANDING_PAGE
				WHERE landing_page_id = '".$landing_page_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getLandingPageByName($landing_page_name_th)
{
    try {
        $sql = "SELECT * 
                FROM LH_LANDING_PAGE
				WHERE (replace(landing_page_url, ' ', '-') = '".$landing_page_name_th."') or (replace(landing_page_name_th, ' ', '-') = '".$landing_page_name_th."')";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}