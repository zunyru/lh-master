<?php


function getSearchSelectProduct()
{
    try {
        $sql = "SELECT DISTINCT(ps.product_id) As product_id , pd.product_name_th,pd.product_name_en,slug  FROM LH_PROJECTS pr
                LEFT JOIN LH_PROJECT_SUB ps ON ps.project_id = pr.project_id
                LEFT JOIN LH_PRODUCTS pd ON pd.product_id = ps.product_id
                WHERE pr.project_data_status = 'PB' AND pr.project_status != 'SO' ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getSearchSelectZone()
{
    try {
        $sql = "SELECT DISTINCT(zs.zone_name_th) AS zone_name , zs.zone_id As zone_id,zs.create_date,slug   FROM LH_PROJECTS pr
                LEFT JOIN LH_PROJECT_SUB ps ON ps.project_id = pr.project_id
                LEFT JOIN LH_PROJECT_ZONE z ON z.project_id = pr.project_id
                LEFT JOIN LH_ZONES zs ON zs.zone_id = z.zone_id
                WHERE CONVERT(date, getdate()) >= pr.public_date AND pr.project_data_status = 'PB' AND pr.project_status != 'SO'
                ORDER BY zs.create_date ASC";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getSearchSelectBrand()
{
    try {
        $sql = "SELECT DISTINCT b.brand_id,b.brand_name_th,slug, b.order_seq FROM LH_PROJECTS pr
                LEFT JOIN LH_BRANDS b ON b.brand_id = pr.brand_id
                WHERE CONVERT(date, getdate()) >= pr.public_date AND pr.project_data_status = 'PB' AND pr.project_status != 'SO' ORDER BY b.order_seq ASC ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getSearchSelectZoneBydirection()
{
    try {
        $sql = "SELECT DISTINCT(zs.zone_name_th) AS zone_name , zs.zone_id As zone_id,zs.create_date,slug   FROM LH_PROJECTS pr
                LEFT JOIN LH_PROJECT_SUB ps ON ps.project_id = pr.project_id
                LEFT JOIN LH_PROJECT_ZONE z ON z.project_id = pr.project_id
                LEFT JOIN LH_ZONES zs ON zs.zone_id = z.zone_id
                WHERE CONVERT(date, getdate()) >= pr.public_date AND pr.project_data_status = 'PB' AND pr.project_status != 'SO' AND pr.latitude != ''
                ORDER BY zs.create_date ASC ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getSearchSelectProject()
{
    try {
        $sql = "SELECT DISTINCT(pr.project_id),pr.project_name_th ,pr.project_name_en,project_url,map_link
                FROM LH_PROJECTS pr
                LEFT JOIN LH_PROJECT_SUB ps ON ps.project_id = pr.project_id

                WHERE CONVERT(date, getdate()) >= pr.public_date  AND pr.project_data_status = 'PB' AND pr.project_status != 'SO' ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getSearchSelectProjectBydirection()
{
    try {
        $sql = "SELECT DISTINCT(pr.project_id),pr.project_name_th ,pr.project_name_en,project_url,map_link
                FROM LH_PROJECTS pr
                LEFT JOIN LH_PROJECT_SUB ps ON ps.project_id = pr.project_id

                WHERE CONVERT(date, getdate()) >= pr.public_date  AND pr.project_data_status = 'PB' AND pr.project_status != 'SO' AND pr.map_link != '' ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getZoneByProductID($product_id)
{
    try {
        $sql = "SELECT DISTINCT(zs.zone_name_th) AS zone_name , zs.zone_id As zone_id , pd.product_id ,pd.product_name_th ,pd.product_name_en,zs.slug,zs.create_date FROM LH_PROJECTS pr
                LEFT JOIN LH_PROJECT_SUB ps ON ps.project_id = pr.project_id
                LEFT JOIN LH_PRODUCTS pd ON pd.product_id = ps.product_id
                LEFT JOIN LH_PROJECT_ZONE z ON z.project_id = pr.project_id
                LEFT JOIN LH_ZONES zs ON zs.zone_id = z.zone_id
                WHERE CONVERT(date, getdate()) >= pr.public_date AND pr.project_data_status = 'PB' AND pr.project_status != 'SO'
                AND pd.product_id = '".$product_id."'"."ORDER BY zs.create_date ASC";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getBrandByZoneIDAndProductID( $product_id = null, $zone_id = null)
{
    try {
        $sql = "SELECT DISTINCT(b.brand_id),b.brand_name_th,b.slug,b.order_seq
                FROM LH_PROJECTS pr
                LEFT JOIN LH_PROJECT_SUB ps ON ps.project_id = pr.project_id
                LEFT JOIN LH_PRODUCTS pd ON pd.product_id = ps.product_id
                LEFT JOIN LH_BRANDS b ON b.brand_id = pr.brand_id
                LEFT JOIN LH_PROJECT_ZONE z ON z.project_id = pr.project_id
                LEFT JOIN LH_ZONES zs ON zs.zone_id = z.zone_id
                WHERE CONVERT(date, getdate()) >= pr.public_date AND pr.project_data_status = 'PB' AND pr.project_status != 'SO' ";
        if(!empty($product_id)){
            $sql .= " AND pd.product_id= '".$product_id."'";
        }
        if(!empty($zone_id)){
            $sql .= " AND zs.zone_id= '".$zone_id."'";
        }
        $sql .= " ORDER BY b.order_seq ASC";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getSearchResult($product_id=null, $zone_id=null, $brand_id=null, $direct=null)
{
    try {
        $sql = "SELECT DISTINCT(pr.project_id),pr.project_name_th,project_url,b.order_seq,map_link
                FROM LH_PROJECTS pr
                LEFT JOIN LH_PROJECT_SUB ps ON ps.project_id = pr.project_id
                LEFT JOIN LH_PRODUCTS pd ON pd.product_id = ps.product_id
                LEFT JOIN LH_BRANDS b ON b.brand_id = pr.brand_id
                LEFT JOIN LH_PROJECT_ZONE z ON z.project_id = pr.project_id
                LEFT JOIN LH_ZONES zs ON zs.zone_id = z.zone_id
                WHERE CONVERT(date, getdate()) >= pr.public_date AND pr.project_data_status = 'PB' AND pr.project_status != 'SO' AND pr.map_link != '' ";
        if(!empty($product_id)){
            $sql .= " AND pd.slug = '".$product_id."'";
        }
        if(!empty($zone_id) AND empty($direct)){
            $sql .= " AND zs.slug= '".$zone_id."'";
        }
        if(!empty($brand_id)){
            $sql .= " AND b.slug = '".$brand_id."'";
        }
        if(!empty($direct)){
            $sql .= " AND zs.zone_id = '".$zone_id."'";
        }
        $sql .= " ORDER BY b.order_seq ASC";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getSearchResultFull($product_id=null, $zone_id=null, $brand_id=null, $direct=null)
{
    try {
        $sql = "SELECT DISTINCT(pr.project_id),pr.project_name_th,project_url,b.order_seq,map_link
                FROM LH_PROJECTS pr
                LEFT JOIN LH_PROJECT_SUB ps ON ps.project_id = pr.project_id
                LEFT JOIN LH_PRODUCTS pd ON pd.product_id = ps.product_id
                LEFT JOIN LH_BRANDS b ON b.brand_id = pr.brand_id
                LEFT JOIN LH_PROJECT_ZONE z ON z.project_id = pr.project_id
                LEFT JOIN LH_ZONES zs ON zs.zone_id = z.zone_id
                WHERE CONVERT(date, getdate()) >= pr.public_date AND pr.project_data_status = 'PB' AND pr.project_status != 'SO'  ";
        if(!empty($product_id)){
            $sql .= " AND pd.slug = '".$product_id."'";
        }
        if(!empty($zone_id) AND empty($direct)){
            $sql .= " AND zs.slug= '".$zone_id."'";
        }
        if(!empty($brand_id)){
            $sql .= " AND b.slug = '".$brand_id."'";
        }
        if(!empty($direct)){
            $sql .= " AND zs.zone_id = '".$zone_id."'";
        }
        $sql .= " ORDER BY b.order_seq ASC";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}



function getSearchDropdownResult($product_id=null, $zone_id=null, $brand_id=null, $direct=null)
{
    try {
        $sql = "SELECT DISTINCT(pr.project_id),pr.project_name_th,project_url,b.order_seq
                FROM LH_PROJECTS pr
                LEFT JOIN LH_PROJECT_SUB ps ON ps.project_id = pr.project_id
                LEFT JOIN LH_PRODUCTS pd ON pd.product_id = ps.product_id
                LEFT JOIN LH_BRANDS b ON b.brand_id = pr.brand_id
                LEFT JOIN LH_PROJECT_ZONE z ON z.project_id = pr.project_id
                LEFT JOIN LH_ZONES zs ON zs.zone_id = z.zone_id
                WHERE CONVERT(date, getdate()) >= pr.public_date AND pr.project_data_status = 'PB' AND pr.project_status != 'SO' ";
        if(!empty($product_id)){
          $sql .= " AND pd.product_id = '".$product_id."'";
        }
        if(!empty($zone_id) AND empty($direct)){
            $sql .= " AND zs.zone_id= '".$zone_id."'";
        }
        if(!empty($brand_id)){
            $sql .= " AND b.brand_id = '".$brand_id."'";
        }
        if(!empty($direct)){
            $sql .= " AND zs.zone_id = '".$zone_id."'";
        }
        $sql .= " ORDER BY b.order_seq ASC";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}


function getProductsName($product_id){
  $sql = "SELECT product_id FROM LH_PRODUCTS WHERE slug = '$product_id'";

  try {
     $result = mssql_query($sql , $GLOBALS['db_conn']);
     return $obj = mssql_fetch_object($result);
  } catch (Exception $e) {
      echo $sql . "<br>" . $e->getMessage();
  }
}

function getZonesName($zone_id){
    $sql = "SELECT zone_id FROM LH_ZONES WHERE slug = '$zone_id'";

    try {
       $result = mssql_query($sql , $GLOBALS['db_conn']);
       return $obj = mssql_fetch_object($result);
    } catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getBrandsName($brand_id){
    $sql = "SELECT brand_id,order_seq FROM LH_BRANDS WHERE slug = '$brand_id'";
    $sql .= " ORDER BY LH_BRANDS.order_seq ASC";
    try {
       $result = mssql_query($sql , $GLOBALS['db_conn']);
       return $obj = mssql_fetch_object($result);
    } catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}