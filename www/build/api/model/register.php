<?php  
require_once '../../../Backend/include/dbCon_mssql.php';
require_once 'Func.php'; 
 
class GetRegister {  

    public function open_register(){

        $sql_ = "SELECT jw.ID, FORMAT( jw.POSTED_DAT, 'd', 'af') as POSTED_DAT ,jw.POSITION_ID,jt.TITLE,lp.PROVINCE_ID,lp.PROVINCE_NAME,jl.LOCATION_ID,jl.LOCATION_NAME,jw.RATES,jw.IS_SHOW FROM JOB_WORK jw,JOB_TITLE jt,JOB_LOCATION jl,Province lp WHERE jw.POSITION_ID = jt.ID AND jw.LOCATION_ID = jl.LOCATION_ID AND jl.PROVINCE_ID = lp.PROVINCE_ID AND jw.IS_SHOW = 1 ORDER BY POSTED_DAT DESC";
        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());

        $intNumField =  mssql_num_rows($objdetail);
        $resultArray = array();
        while($obResult = mssql_fetch_assoc($objdetail))
        {
            $resultArray[] = $obResult;
        }
        mssql_close($db_conn);

        if(count($resultArray) > 0){
            return json_encode($resultArray);
        }else{
            return json_encode(array(['message'=>'unsuccess','status'=>'false']));
        }

    } 

    public function open_news($id){

        $str = '';
        if($id != ''){
            $str = "AND ID ='".$id."'";
        }
        $sql_ = "SELECT ID,TITLE,SHORT_TITLE,DETAIL,NEWS_PIC,FORMAT( PUBLISH_DAT, 'd', 'af') as DATE ,FORMAT(PUBLISH_DAT, 'yyyy') as YEAR,IS_SHOW  FROM JOB_NEWS WHERE IS_SHOW = 1 ".$str ." ORDER BY DATE DESC";
        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());

        $intNumField =  mssql_num_rows($objdetail);
        $resultArray = array();
        while($obResult = mssql_fetch_assoc($objdetail))
        {
            $resultArray[] = $obResult;
        }
        mssql_close($db_conn);

        if(count($resultArray) > 0){
            return json_encode($resultArray);
        }else{
            return json_encode(array(['message'=>'unsuccess','status'=>'false']));
        }

    } 

    public function open_optionnews(){

        $sql_ = "SELECT DISTINCT FORMAT(PUBLISH_DAT, 'yyyy') as YEAR FROM JOB_NEWS WHERE IS_SHOW = 1 ";
        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());

        $intNumField =  mssql_num_rows($objdetail);
        $resultArray = array();
        while($obResult = mssql_fetch_assoc($objdetail))
        {
            $resultArray[] = $obResult;
        }
        mssql_close($db_conn);

        if(count($resultArray) > 0){
            return json_encode($resultArray);
        }else{
            return json_encode(array(['message'=>'unsuccess','status'=>'false']));
        }

    } 
    
     public function comment(){

        $sql_ = "SELECT *  FROM JOB_COMMENT  ";
        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());

        $intNumField =  mssql_num_rows($objdetail);
        $resultArray = array();
        while($obResult = mssql_fetch_assoc($objdetail))
        {
            $resultArray[] = $obResult;
        }
        mssql_close($db_conn);

        if(count($resultArray) > 0){
            return json_encode($resultArray);
        }else{
            return json_encode(array(['message'=>'unsuccess','status'=>'false']));
        }

    } 

    public function prvinces(){

        $sql_ = "SELECT * FROM Province ORDER BY PROVINCE_NAME ASC";
        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());

        $intNumField =  mssql_num_rows($objdetail);
        $resultArray = array();
        while($obResult = mssql_fetch_assoc($objdetail))
        {
            $resultArray[] = $obResult;
        }
        mssql_close($db_conn);

        if(count($resultArray) > 0){
            return json_encode($resultArray);
        }else{
            return json_encode(array(['message'=>'unsuccess','status'=>'false']));
        }

    } 

    
    public function prvincesByposition($id = ''){

        $str = '';
        if($id != ''){
            $str = "AND lo.LOCATION_ID ='".$id."'";
        }
        $sql_ = "SELECT pr.PROVINCE_ID,PROVINCE_NAME,LOCATION_NAME,IS_SHOW,LOCATION_ID FROM Province pr, JOB_LOCATION lo WHERE pr.PROVINCE_ID = lo.PROVINCE_ID ".$str;
        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());

        $intNumField =  mssql_num_rows($objdetail);
        $resultArray = array();
        while($obResult = mssql_fetch_assoc($objdetail))
        {
            $resultArray[] = $obResult;
        }
        mssql_close($db_conn);

        if(count($resultArray) > 0){
            return json_encode($resultArray);
        }else{
            return json_encode(array(['message'=>'unsuccess','status'=>'false']));
        }

    } 


    public function location($id){

        $str = '';
        if($id != ''){
            $str = "AND lo.LOCATION_ID ='".$id."'";
        }
        $sql_ = "SELECT pr.PROVINCE_ID,PROVINCE_NAME,LOCATION_NAME,IS_SHOW,LOCATION_ID FROM Province pr, JOB_LOCATION lo WHERE pr.PROVINCE_ID = lo.PROVINCE_ID ".$str;
        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());

        $intNumField =  mssql_num_rows($objdetail);
        $resultArray = array();
        while($obResult = mssql_fetch_assoc($objdetail))
        {
            $resultArray[] = $obResult;
        }
        mssql_close($db_conn);

        if(count($resultArray) > 0){
            return json_encode($resultArray);
        }else{
            return json_encode(array(['message'=>'unsuccess','status'=>'false']));
        }

    } 

    public function user($id){

        $str = '';
        if($id != ''){
            $str = "AND ID ='".$id."'";
        }
        $sql_ = "SELECT ID,USER_NAME,LOGIN FROM JOB_USER WHERE 1=1  ".$str;
        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());

        $intNumField =  mssql_num_rows($objdetail);
        $resultArray = array();
        while($obResult = mssql_fetch_assoc($objdetail))
        {
            $resultArray[] = $obResult;
        }
        mssql_close($db_conn);

        if(count($resultArray) > 0){
            return json_encode($resultArray);
        }else{
            return json_encode(array(['message'=>'unsuccess','status'=>'false']));
        }

    } 

    public function position($id){

        $str = '';
        if($id != ''){
            $str = "AND ID ='".$id."'";
        }
        $sql_ = "SELECT ID,TITLE FROM JOB_TITLE WHERE 1=1  ".$str;
        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());

        $intNumField =  mssql_num_rows($objdetail);
        $resultArray = array();
        while($obResult = mssql_fetch_assoc($objdetail))
        {
            $resultArray[] = $obResult;
        }
        mssql_close($db_conn);

        if(count($resultArray) > 0){
            return json_encode($resultArray);
        }else{
            return json_encode(array(['message'=>'unsuccess','status'=>'false']));
        }

    } 

    public function work($id){

        $str = '';
        if($id != ''){
            $str = "AND w.ID ='".$id."'";
        }
        $sql_ = "SELECT w.ID,w.POSITION_ID,w.LOCATION_ID,p.PROVINCE_ID,w.ATTRIBUTE,w.RATES,FORMAT( w.POSTED_DAT, 'd', 'af') as DATE ,w.IS_SHOW,t.TITLE,p.PROVINCE_NAME,l.LOCATION_NAME 
            FROM JOB_WORK w,JOB_LOCATION l,Province p,JOB_TITLE t WHERE w.POSITION_ID = t.ID  AND w.LOCATION_ID = l.LOCATION_ID AND l.PROVINCE_ID = p.PROVINCE_ID ".$str;
        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());

        $intNumField =  mssql_num_rows($objdetail);
        $resultArray = array();
        while($obResult = mssql_fetch_assoc($objdetail))
        {
            $resultArray[] = $obResult;
        }
        mssql_close($db_conn);

        if(count($resultArray) > 0){
            return json_encode($resultArray);
        }else{
            return json_encode(array(['message'=>'unsuccess','status'=>'false']));
        }

    } 

    public function news($id){

        $str = '';
        if($id != ''){
            $str = "AND ID ='".$id."'";
        }
        $sql_ = "SELECT ID,TITLE,SHORT_TITLE,DETAIL,NEWS_PIC,FORMAT( PUBLISH_DAT, 'd', 'af') as DATE ,IS_SHOW  FROM JOB_NEWS WHERE 1=1 ".$str;
        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());

        $intNumField =  mssql_num_rows($objdetail);
        $resultArray = array();
        while($obResult = mssql_fetch_assoc($objdetail))
        {
            $resultArray[] = $obResult;
        }
        mssql_close($db_conn);

        if(count($resultArray) > 0){
            return json_encode($resultArray);
        }else{
            return json_encode(array(['message'=>'unsuccess','status'=>'false']));
        }

    } 
    public function report($id){

        $str = '';
        if($id != ''){
            $str = "AND a.ID ='".$id."'";
        }
        $sql_ = "SELECT  a.ID,a.NAME,a.LASTNAME,w.POSITION_ID,t.TITLE,c.COMMENT,c.ID as COMMENT_ID,a.OTHER
                FROM JOB_APPLICANT a, JOB_COMMENT c,
                JOB_WORK w,JOB_TITLE t
                WHERE a.COMMENT_ID = c.ID AND
                a.WORK_ID = w.ID AND
                w.POSITION_ID = t.ID ".$str;

        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());

        $intNumField =  mssql_num_rows($objdetail);
        $resultArray = array();
        while($obResult = mssql_fetch_assoc($objdetail))
        {
            $resultArray[] = $obResult;
        }
        mssql_close($db_conn);

        if(count($resultArray) > 0){
            return json_encode($resultArray);
        }else{
            return json_encode(array(['message'=>'unsuccess','status'=>'false']));
        }
    } 

    public function candidacy(){

        $sql_ = "SELECT a.ID,a.PREFIX,a.NAME,a.LASTNAME,w.POSITION_ID,t.TITLE,w.LOCATION_ID,lp.PROVINCE_ID,lp.PROVINCE_NAME,l.LOCATION_NAME,FORMAT( a.CREATE_DAT, 'd', 'af') as DATE ,ac.PHONE,a.ATTACHMENT_FILE FROM
                    JOB_APPLICANT a,JOB_COMMENT c,JOB_WORK w,JOB_TITLE t,JOB_LOCATION l,Province lp,JOB_APPLICANT_CONTACT ac
                WHERE a.COMMENT_ID = c.ID AND a.WORK_ID = w.ID AND w.POSITION_ID = t.ID AND w.LOCATION_ID = l.LOCATION_ID AND l.PROVINCE_ID = lp.PROVINCE_ID AND ac.APPLICANT_ID = a.ID";

        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());

        $intNumField =  mssql_num_rows($objdetail);
        $resultArray = array();
        while($obResult = mssql_fetch_assoc($objdetail))
        {
            $resultArray[] = $obResult;
        }
        mssql_close($db_conn);

        if(count($resultArray) > 0){
            return json_encode($resultArray);
        }else{
            return json_encode(array(['message'=>'unsuccess','status'=>'false']));
        }
    } 
    // ============================ candidacy ==========================================
    public function candidacy_profile($id){
        $str = '';
        if($id != ''){
            $str = "AND ID ='".$id."'";
        }
        $sql_ = "SELECT  *
                 FROM JOB_APPLICANT app LEFT JOIN Province pv ON app.PROVINCE_ID = pv.PROVINCE_ID
                WHERE 1=1 ".$str;

        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());

        $intNumField =  mssql_num_rows($objdetail);
        $resultArray = array();
        while($obResult = mssql_fetch_assoc($objdetail))
        {
            $resultArray[] = $obResult;
        }
        mssql_close($db_conn);

        if(count($resultArray) > 0){
            return json_encode($resultArray);
        }else{
            return json_encode(array(['message'=>'unsuccess','status'=>'false']));
        }
    } 

    public function candidacy_family($id){
        $str = '';
        if($id != ''){
            $str = "AND APPLICANT_ID ='".$id."'";
        }
        $sql_ = "SELECT  *
                FROM JOB_APPLICANT_FAMILY 
                WHERE 1=1 ".$str;

        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());

        $intNumField =  mssql_num_rows($objdetail);
        $resultArray = array();
        while($obResult = mssql_fetch_assoc($objdetail))
        {
            $resultArray[] = $obResult;
        }
        mssql_close($db_conn);

        if(count($resultArray) > 0){
            return json_encode($resultArray);
        }else{
            return json_encode(array(['message'=>'unsuccess','status'=>'false']));
        }
    } 

    public function candidacy_contact($id){
        $str = '';
        if($id != ''){
            $str = "AND ac.APPLICANT_ID ='".$id."'";
        }
        $sql_ = "SELECT ac.ID, ac.APPLICANT_ID, ac.ADDRESS, ac.EMAIL, ac.PHONE, ac.NAME_EMER, ac.PHONE_EMER, ac.PROVINCE_ID, lp.PROVINCE_NAME FROM JOB_APPLICANT_CONTACT ac, Province lp WHERE ac.PROVINCE_ID = lp.PROVINCE_ID ".$str;

        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());

        $intNumField =  mssql_num_rows($objdetail);
        $resultArray = array();
        while($obResult = mssql_fetch_assoc($objdetail))
        {
            $resultArray[] = $obResult;
        }
        mssql_close($db_conn);

        if(count($resultArray) > 0){
            return json_encode($resultArray);
        }else{
            return json_encode(array(['message'=>'unsuccess','status'=>'false']));
        }
    } 

    public function candidacy_education($id){
        $str = '';
        if($id != ''){
            $str = "WHERE ae.APPLICANT_ID ='".$id."'";
        }
        /*
        $sql_ = "SELECT
                    ae.ID,ae.GRADUATE_ID,g.GRADUATE_N,ae.TOP_GRADUATE,ae.DATE_START,ae.DATE_STOP,ae.INSTITUTION,ae.FACULTY,ae.BRANCH,ae.GPA,ae.APPLICANT_ID
                FROM
                    JOB_APPLICANT_EDUCATION ae,JOB_GRADUATE g
                WHERE
                    ae.GRADUATE_ID = g.ID ".$str;
        */

          $sql_ = "SELECT ae.ID,ae.GRADUATE_ID,g.GRADUATE_N,ae.TOP_GRADUATE,ae.DATE_START,ae.DATE_STOP,ae.INSTITUTION,ae.FACULTY,ae.BRANCH,ae.GPA,ae.APPLICANT_ID
                FROM
                    JOB_APPLICANT_EDUCATION ae LEFT JOIN JOB_GRADUATE g ON ae.GRADUATE_ID = g.ID ".$str;            

        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());

        $intNumField =  mssql_num_rows($objdetail);
        $resultArray = array();
        while($obResult = mssql_fetch_assoc($objdetail))
        {
            $resultArray[] = $obResult;
        }
        mssql_close($db_conn);

        if(count($resultArray) > 0){
            return json_encode($resultArray);
        }else{
            return json_encode(array(['message'=>'unsuccess','status'=>'false']));
        }
    } 

    public function candidacy_language($id){
        $str = '';
        if($id != ''){
            $str = "AND APPLICANT_ID ='".$id."'";
        }
        $sql_ = "SELECT *
                FROM JOB_APPLICANT_LANGUAGE
                WHERE 1=1 ".$str;

        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());

        $intNumField =  mssql_num_rows($objdetail);
        $resultArray = array();
        while($obResult = mssql_fetch_assoc($objdetail))
        {
            $resultArray[] = $obResult;
        }
        mssql_close($db_conn);

        if(count($resultArray) > 0){
            return json_encode($resultArray);
        }else{
            return json_encode(array(['message'=>'unsuccess','status'=>'false']));
        }
    } 

    public function candidacy_job($id){
        $str = '';
        if($id != ''){
            $str = "AND aj.APPLICANT_ID ='".$id."'";
        }
        $sql_ = "SELECT  *
                FROM JOB_APPLICANT_JOB aj,JOB_APPLICANT_JOB_HISTORY ajh
                WHERE aj.ID = ajh.JOB_ID ".$str;

        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());

        $intNumField =  mssql_num_rows($objdetail);
        $resultArray = array();
        while($obResult = mssql_fetch_assoc($objdetail))
        {
            $resultArray[] = $obResult;
        }
        mssql_close($db_conn);

        if(count($resultArray) > 0){
            return json_encode($resultArray);
        }else{
            return json_encode(array(['message'=>'unsuccess','status'=>'false']));
        }
    } 

    public function candidacy_job_interested($id){
        $str = '';
        if($id != ''){
            $str = "AND APPLICANT_ID ='".$id."'";
        }
        $sql_ = "SELECT  *
                FROM JOB_APPLICANT_JOB_INTERESTED 
                WHERE 1=1 ".$str;

        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());

        $intNumField =  mssql_num_rows($objdetail);
        $resultArray = array();
        while($obResult = mssql_fetch_assoc($objdetail))
        {
            $resultArray[] = $obResult;
        }
        mssql_close($db_conn);

        if(count($resultArray) > 0){
            return json_encode($resultArray);
        }else{
            return json_encode(array(['message'=>'unsuccess','status'=>'false']));
        }
    } 

    public function candidacy_person($id){
        $str = '';
        if($id != ''){
            $str = "AND APPLICANT_ID ='".$id."'";
        }
        $sql_ = "SELECT *
                FROM JOB_APPLICANT_PERSON
                WHERE 1=1 ".$str;

        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());

        $intNumField =  mssql_num_rows($objdetail);
        $resultArray = array();
        while($obResult = mssql_fetch_assoc($objdetail))
        {
            $resultArray[] = $obResult;
        }
        mssql_close($db_conn);

        if(count($resultArray) > 0){
            return json_encode($resultArray);
        }else{
            return json_encode(array(['message'=>'unsuccess','status'=>'false']));
        }
    } 

    public function candidacy_datas($id){
        $str = '';
        if($id != ''){
            $str = "AND APPLICANT_ID ='".$id."'";
        }
        $sql_ = "SELECT  *
                FROM JOB_APPLICANT_DATAS
                WHERE 1=1 ".$str;

        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());

        $intNumField =  mssql_num_rows($objdetail);
        $resultArray = array();
        while($obResult = mssql_fetch_assoc($objdetail))
        {
            $resultArray[] = $obResult;
        }
        mssql_close($db_conn);

        if(count($resultArray) > 0){
            return json_encode($resultArray);
        }else{
            return json_encode(array(['message'=>'unsuccess','status'=>'false']));
        }
    } 

}  




// ********************

class PostRegister extends Function_ {  

    public function delete_($data){

    	if($data["tb"] == 'JOB_APPLICANT'){
    		$sql_img = "SELECT * FROM JOB_APPLICANT WHERE ".$data["key"]." = '".$data["id"]."'";
        	$objdetail = mssql_query($sql_img)or die('A error occured: ' . mysql_error());
        	$value = mssql_fetch_assoc($objdetail);
        	@unlink('../fileupload/candidacy/' . $value['ATTACHMENT_FILE']);
        	@unlink('../fileupload/candidacy/' . $value['ATTACHMENTFILE_IMAGE']);
    	}

        $sql_ = "DELETE FROM ".$data["tb"]." WHERE ".$data["key"]." = '".$data["id"]."'";
        $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());

        if($result == 1){ 
            return json_encode(array('message'=>'success','status'=>'1')); 
        }else{ 
            return json_encode(array('message'=>'unsuccess','status'=>'0')); 
        }
    } 
   
    public function location_add($data){

        $sql_ = "SELECT * FROM JOB_LOCATION WHERE LOCATION_NAME = '".$data["location_name"]."' AND PROVINCE_ID = '".$data["province_id"]."'";
        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());
        $num_rows =  mssql_num_rows($objdetail);

        if($num_rows >= 1){
            return json_encode(array('message'=>'data duplicate','status'=>'2'));
        }
        else{
            $sql_ = "INSERT INTO JOB_LOCATION (LOCATION_NAME,PROVINCE_ID,IS_SHOW) VALUES ('".$data["location_name"]."','".$data["province_id"]."','".$data["is_show"]."')";
            $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());

            if($result == 1){ 
                return json_encode(array('message'=>'success','status'=>'1')); 
            }else{ 
                return json_encode(array('message'=>'unsuccess','status'=>'0')); 
            }
            
        }
    } 


    public function location_edit($data){

        $sql_ = "SELECT * FROM JOB_LOCATION WHERE LOCATION_NAME = '".$data["location_name"]."' AND PROVINCE_ID = '".$data["province_id"]."' AND LOCATION_ID != '".$data["id"]."' ";
        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());
        $num_rows =  mssql_num_rows($objdetail);

        if($num_rows >= 1){
            return json_encode(array('message'=>'data duplicate','status'=>'2'));
        }
        else{
            $sql_ = "UPDATE JOB_LOCATION SET LOCATION_NAME = '".$data["location_name"]."', IS_SHOW = '".$data["is_show"]."' , PROVINCE_ID = '".$data["province_id"]."' WHERE LOCATION_ID = ".$data["id"];

            $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());

            if($result == 1){ 
                return json_encode(array('message'=>'success','status'=>'1')); 
            }else{ 
                return json_encode(array('message'=>'unsuccess','status'=>'0')); 
            }
            
        }
    }


    public function user_add($data){

        $sql_ = "SELECT * FROM JOB_USER WHERE LOGIN = '".$data["login"]."' AND LOGIN_PWD = '".self::encode($data["pass"],'LH')."'";
        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());
        $num_rows =  mssql_num_rows($objdetail);

        if($num_rows >= 1){
            return json_encode(array('message'=>'data duplicate','status'=>'2'));
        }
        else{
            $sql_ = "INSERT INTO JOB_USER (USER_NAME,LOGIN,LOGIN_PWD,CREATE_DAT,UPDATE_DAT) VALUES ('".$data["user_name"]."','".$data["login"]."','".self::encode($data["pass"],'LH')."',GETDATE(),GETDATE())";
            $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());

            if($result == 1){ 
                return json_encode(array('message'=>'success','status'=>'1')); 
            }else{ 
                return json_encode(array('message'=>'unsuccess','status'=>'0')); 
            }
            
        }
    }


    public function user_edit($data){

        $sql_ = "SELECT * FROM JOB_USER WHERE LOGIN = '".$data["login"]."' AND LOGIN_PWD = '".self::encode($data["pass"],'LH')."' AND ID != '".$data["id"]."'";
        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());
        $num_rows =  mssql_num_rows($objdetail);

        if($num_rows >= 1){
            return json_encode(array('message'=>'data duplicate','status'=>'2'));
        }
        else{
            $sql_ = "UPDATE JOB_USER SET USER_NAME = '".$data["user_name"]."', LOGIN = '".$data["login"]."' , LOGIN_PWD = '".self::encode($data["pass"],'LH')."',UPDATE_DAT = GETDATE() WHERE ID = ".$data["id"];

            $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());

            if($result == 1){ 
                return json_encode(array('message'=>'success','status'=>'1')); 
            }else{ 
                return json_encode(array('message'=>'unsuccess','status'=>'0')); 
            }
            
        }
    }


    public function position_add($data){

        $sql_ = "SELECT * FROM JOB_TITLE WHERE TITLE = '".$data["position"]."'";
        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());
        $num_rows =  mssql_num_rows($objdetail);

        if($num_rows >= 1){
            return json_encode(array('message'=>'data duplicate','status'=>'2'));
        }
        else{
            $sql_ = "INSERT INTO JOB_TITLE (TITLE,CREATE_DAT,UPDATE_DAT) VALUES ('".$data["position"]."',GETDATE(),GETDATE())";
            $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());

            if($result == 1){
                return json_encode(array('message'=>'success','status'=>'1')); 
            }else{ 
                return json_encode(array('message'=>'unsuccess','status'=>'0')); 
            }
            
        }
    }


    public function position_edit($data){

        $sql_ = "SELECT * FROM JOB_TITLE WHERE TITLE = '".$data["position"]."' AND ID != '".$data["id"]."'";
        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());
        $num_rows =  mssql_num_rows($objdetail);

        if($num_rows >= 1){
            return json_encode(array('message'=>'data duplicate','status'=>'2'));
        }
        else{
            $sql_ = "UPDATE JOB_TITLE SET TITLE = '".$data["position"]."',UPDATE_DAT = GETDATE() WHERE ID = ".$data["id"];

            $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());

            if($result == 1){ 
                return json_encode(array('message'=>'success','status'=>'1')); 
            }else{ 
                return json_encode(array('message'=>'unsuccess','status'=>'0')); 
            }
            
        }
    }

    public function work_add($data){

        $sql_ = "SELECT * FROM JOB_WORK WHERE POSITION_ID = '".$data["position_id"]."' AND  LOCATION_ID = '".$data["location_id"]."' AND POSTED_DAT = '".$data["posted"]."'";
        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());
        $num_rows =  mssql_num_rows($objdetail);

        if($num_rows >= 1){
            return json_encode(array('message'=>'data duplicate','status'=>'2'));
        }
        else{
            $sql_ = "INSERT INTO JOB_WORK (POSITION_ID,LOCATION_ID,ATTRIBUTE,RATES,POSTED_DAT,IS_SHOW,CREATE_DAT,UPDATE_DAT) VALUES ('".$data["position_id"]."','".$data["location_id"]."','".$data["attribute"]."','".$data["rates"]."','".$data["posted"]."','".$data["is_show"]."',GETDATE(),GETDATE())";
            $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());
   
            if($result == 1){ 
                return json_encode(array('message'=>'success','status'=>'1')); 
            }else{ 
                return json_encode(array('message'=>'unsuccess','status'=>'0')); 
            }
            
        }
    }


    public function work_edit($data){

        $sql_ = "SELECT * FROM JOB_WORK WHERE POSITION_ID = '".$data["position_id"]."' AND  LOCATION_ID = '".$data["location_id"]."' AND POSTED_DAT = '".$data["posted"]."' AND ID != '".$data["id"]."'" ;
        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());
        $num_rows =  mssql_num_rows($objdetail);

        if($num_rows >= 1){
            return json_encode(array('message'=>'data duplicate','status'=>'2'));
        }
        else{

            $posted=str_replace('-', '/', $data["posted"]);
             
            $sql_ = "UPDATE JOB_WORK SET POSITION_ID = '".$data["position_id"]."',location_id = '".$data["location_id"]."',ATTRIBUTE = '".$data["attribute"]."' ,RATES = '".$data["rates"]."' ,POSTED_DAT = '".$posted."' ,IS_SHOW = '".$data["is_show"]."' ,UPDATE_DAT = GETDATE() WHERE ID = ".$data["id"];


            $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());

            if($result == 1){ 
                return json_encode(array('message'=>'success','status'=>'1')); 
            }else{ 
                return json_encode(array('message'=>'unsuccess','status'=>'0')); 
            }
            
        }
    }


    public function news_add($data){

        $sql_ = "SELECT * FROM JOB_NEWS WHERE TITLE = '".$data["title"]."'  AND PUBLISH_DAT = '".$data["publish"]."'";
        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());
        $num_rows =  mssql_num_rows($objdetail);

        if($num_rows >= 1){
            return json_encode(array('message'=>'data duplicate','status'=>'2'));
        }
        else{
            $sql_ = "INSERT INTO JOB_NEWS (TITLE,SHORT_TITLE,DETAIL,NEWS_PIC,PUBLISH_DAT,IS_SHOW,CREATE_DAT,UPDATE_DAT) VALUES ('".$data["title"]."','".$data["short"]."','".$data["detail"]."','".$data["imagename"]."','".$data["publish"]."','".$data["is_show"]."',GETDATE(),GETDATE())";
            $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());
   
            if($result == 1){ 
                return json_encode(array('message'=>'success','status'=>'1')); 
            }else{ 
                @unlink('../fileupload/news/' . $data['imagename']);
                return json_encode(array('message'=>'unsuccess','status'=>'0')); 
            }
            
        }
    }


    public function news_edit($data){

        $sql_ = "SELECT * FROM JOB_NEWS WHERE TITLE = '".$data["title"]."' AND  PUBLISH_DAT = '".$data["publish"]."' AND ID != '".$data["id"]."'" ;
        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());
        $num_rows =  mssql_num_rows($objdetail);

        if($num_rows >= 1){
            return json_encode(array('message'=>'data duplicate','status'=>'2'));
        }
        else{
            $sql_ = "UPDATE JOB_NEWS SET TITLE = '".$data["title"]."',SHORT_TITLE = '".$data["short"]."',DETAIL = '".$data["detail"]."' ,NEWS_PIC = '".$data["imagename"]."' ,PUBLISH_DAT = '".$data["publish"]."' ,IS_SHOW = '".$data["is_show"]."' ,UPDATE_DAT = GETDATE() WHERE ID = ".$data["id"];
        
            $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());

            if($result == 1){ 
                return json_encode(array('message'=>'success','status'=>'1')); 
            }else{ 
                @unlink('../fileupload/news/' . $data['imagename']);
                return json_encode(array('message'=>'unsuccess','status'=>'0')); 
            }
            
        }
    }

    public function report_edit($data){

        $sql_ = "UPDATE JOB_APPLICANT SET COMMENT_ID = '".$data["comment_id"]."', OTHER = '".$data["other"]."' WHERE ID = ".$data["id"] ;

        $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());

        if($result == 1){ 
            return json_encode(array('message'=>'success','status'=>'1')); 
        }else{ 
            return json_encode(array('message'=>'unsuccess','status'=>'0')); 
        }
    }

    // ==================== CANDIDACY ========================
    public function family_add($data){
        if(!empty($data["name_father"])){
            $sql_ = "INSERT INTO JOB_APPLICANT_FAMILY 
                    (APPLICANT_ID,NAME,AGE,CAREER,ADDRESS,STATUS) 
                    VALUES (
                    '".$data['id']."',
                    '".$data["name_father"]."',
                    '".$data["age_father"]."',
                    '".$data["career_father"]."',
                    '".$data["address_father"]."',
                    '".$data["status_father"]."'
                    )";
            $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());
        }
        if(!empty($data["name_mother"])){
           $sql_ = "INSERT INTO JOB_APPLICANT_FAMILY 
                    (APPLICANT_ID,NAME,AGE,CAREER,ADDRESS,STATUS) 
                    VALUES (
                    '".$data['id']."',
                    '".$data["name_mother"]."',
                    '".$data["age_mother"]."',
                    '".$data["career_mother"]."',
                    '".$data["address_mother"]."',
                    '".$data["status_mother"]."'
                    )";
            $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());
        }
        if(!empty($data["name_fraternity1"])){
            $sql_ = "INSERT INTO JOB_APPLICANT_FAMILY 
                    (APPLICANT_ID,NAME,AGE,CAREER,ADDRESS,STATUS) 
                    VALUES (
                    '".$data['id']."',
                    '".$data["name_fraternity1"]."',
                    '".$data["age_fraternity1"]."',
                    '".$data["career_fraternity1"]."',
                    '".$data["address_fraternity1"]."',
                    '".$data["status_fraternity1"]."'
                    )";
            $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());
        }
        if(!empty($data["name_fraternity2"])){
            $sql_ = "INSERT INTO JOB_APPLICANT_FAMILY 
                    (APPLICANT_ID,NAME,AGE,CAREER,ADDRESS,STATUS) 
                    VALUES (
                    '".$data['id']."',
                    '".$data["name_fraternity2"]."',
                    '".$data["age_fraternity2"]."',
                    '".$data["career_fraternity2"]."',
                    '".$data["address_fraternity2"]."',
                    '".$data["status_fraternity2"]."'
                    )";
            $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());
        }
        if(!empty($data["name_fraternity3"])){
            $sql_ = "INSERT INTO JOB_APPLICANT_FAMILY 
                    (APPLICANT_ID,NAME,AGE,CAREER,ADDRESS,STATUS) 
                    VALUES (
                    '".$data['id']."',
                    '".$data["name_fraternity3"]."',
                    '".$data["age_fraternity3"]."',
                    '".$data["career_fraternity3"]."',
                    '".$data["address_fraternity3"]."',
                    '".$data["status_fraternity3"]."'
                    )";
            $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());
        }
        if(!empty($data["name_spouse"])){
            $sql_ = "INSERT INTO JOB_APPLICANT_FAMILY 
                    (APPLICANT_ID,NAME,AGE,CAREER,ADDRESS,STATUS) 
                    VALUES (
                    '".$data['id']."',
                    '".$data["name_spouse"]."',
                    '".$data["age_spouse"]."',
                    '".$data["career_spouse"]."',
                    '".$data["address_spouse"]."',
                    '".$data["status_spouse"]."'
                    )";
            $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());
        }

        if($result == 1){ 
            return json_encode(array('message'=>'success','status'=>'1')); 
        }else{ 
             return json_encode(array('message'=>'unsuccess','status'=>'0')); 
        }
    }

    public function education_add($data){

         // =================== JOB_APPLICANT_EDUCATION
         if(!empty($data["date_start1"]) && !empty($data["date_stop1"])){
             $sql_ = "INSERT INTO JOB_APPLICANT_EDUCATION 
                (APPLICANT_ID,TOP_GRADUATE,GRADUATE_ID,DATE_START,DATE_STOP,INSTITUTION,FACULTY,BRANCH,GPA) 
                VALUES (
                '".$data['id']."',
                '".$data["top_graduate"]."',
                '".$data["graduate_id1"]."',
                '".$data["date_start1"]."',
                '".$data["date_stop1"]."',
                '".$data["institution1"]."',
                '".$data["faculty1"]."',
                '".$data["branch1"]."',
                '".$data["gpa1"]."'
                )";
                $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());
        }
         if(!empty($data["date_start2"]) && !empty($data["date_stop2"])){
             $sql_ = "INSERT INTO JOB_APPLICANT_EDUCATION 
                (APPLICANT_ID,TOP_GRADUATE,GRADUATE_ID,DATE_START,DATE_STOP,INSTITUTION,FACULTY,BRANCH,GPA) 
                VALUES (
                '".$data['id']."',
                '".$data["top_graduate"]."',
                '".$data["graduate_id2"]."',
                '".$data["date_start2"]."',
                '".$data["date_stop2"]."',
                '".$data["institution2"]."',
                '".$data["faculty2"]."',
                '".$data["branch2"]."',
                '".$data["gpa2"]."'
                )";
                $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());
        }
         if(!empty($data["date_start3"]) && !empty($data["date_stop3"])){
             $sql_ = "INSERT INTO JOB_APPLICANT_EDUCATION 
                (APPLICANT_ID,TOP_GRADUATE,GRADUATE_ID,DATE_START,DATE_STOP,INSTITUTION,FACULTY,BRANCH,GPA) 
                VALUES (
                '".$data['id']."',
                '".$data["top_graduate"]."',
                '".$data["graduate_id3"]."',
                '".$data["date_start3"]."',
                '".$data["date_stop3"]."',
                '".$data["institution3"]."',
                '".$data["faculty3"]."',
                '".$data["branch3"]."',
                '".$data["gpa3"]."'
                )";
                $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());
        }
            if($result == 1){ 
                return json_encode(array('message'=>'success','status'=>'1')); 
            }else{ 
                return json_encode(array('message'=>'unsuccess','status'=>'0')); 
            }
    }

    public function job_add($data){

         // =================== JOB_APPLICANT_JOB
        $sql_ = "INSERT INTO JOB_APPLICANT_JOB 
                (APPLICANT_ID,FINISH,EXP,LAST_POSITION,LAST_MONEY,LINE_LAST) 
                VALUES (
                '".$data['id']."',
                '".$data["finish"]."',
                '".$data["exp"]."',
                '".$data["last_position"]."',
                ".$data["last_money"].",
                '".$data["line_last"]."'
                )";
        $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());

        if($result == 1){ 

            $sql_id = "SELECT SCOPE_IDENTITY()";
            $id = mssql_fetch_assoc(mssql_query($sql_id));
            $m =date('m');
            $y = date('Y')+543;
            $save_date =  '01/'.$m.'/'.$y;
            if(!empty($data["date_start1"]) && !empty($data["date_stop1"])){
                 $sql_ = "INSERT INTO JOB_APPLICANT_JOB_HISTORY 
                (JOB_ID,DATE_START,DATE_STOP,POSITION,COMPANY,SALARY,OTHER_SALARY,MORE_OTHER_SALARY,FEATURES,APPLICANT_ID,Job_save_date) 
                VALUES (
                '".$id['computed']."',
                '".$data["date_start1"]."',
                '".$data["date_stop1"]."',
                '".$data["position1"]."',
                '".$data["company1"]."',
                '".$data["salary1"]."',
                '".$data["other_salary1"]."',
                '".$data["more_other_salary1"]."',
                '".$data["features1"]."',
                '".$data['id']."',
                '".$save_date."'
                )";
                $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());
            }
            if(!empty($data["date_start2"]) && !empty($data["date_stop2"])){
               $sql_ = "INSERT INTO JOB_APPLICANT_JOB_HISTORY 
                (JOB_ID,DATE_START,DATE_STOP,POSITION,COMPANY,SALARY,OTHER_SALARY,MORE_OTHER_SALARY,FEATURES,APPLICANT_ID,Job_save_date) 
                VALUES (
                '".$id['computed']."',
                '".$data["date_start2"]."',
                '".$data["date_stop2"]."',
                '".$data["position2"]."',
                '".$data["company2"]."',
                '".$data["salary2"]."',
                '".$data["other_salary2"]."',
                '".$data["more_other_salary2"]."',
                '".$data["features2"]."',
                 '".$data['id']."',
                 '".$save_date."'
                 )";
                $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());
            }
            if(!empty($data["date_start3"]) && !empty($data["date_stop3"])){
                $sql_ = "INSERT INTO JOB_APPLICANT_JOB_HISTORY 
                (JOB_ID,DATE_START,DATE_STOP,POSITION,COMPANY,SALARY,OTHER_SALARY,MORE_OTHER_SALARY,FEATURES,APPLICANT_ID,Job_save_date) 
                VALUES (
                '".$id['computed']."',
                '".$data["date_start3"]."',
                '".$data["date_stop3"]."',
                '".$data["position3"]."',
                '".$data["company3"]."',
                '".$data["salary3"]."',
                '".$data["other_salary3"]."',
                '".$data["more_other_salary3"]."',
                '".$data["features3"]."',
                '".$data['id']."',
                '".$save_date."'
                )";
                $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());
            }
            if(!empty($data["date_start4"]) && !empty($data["date_stop4"])){
                $sql_ = "INSERT INTO JOB_APPLICANT_JOB_HISTORY 
                (JOB_ID,DATE_START,DATE_STOP,POSITION,COMPANY,SALARY,OTHER_SALARY,MORE_OTHER_SALARY,FEATURES,APPLICANT_ID,Job_save_date) 
                VALUES (
                '".$id['computed']."',
                '".$data["date_start4"]."',
                '".$data["date_stop4"]."',
                '".$data["position4"]."',
                '".$data["company4"]."',
                '".$data["salary4"]."',
                '".$data["other_salary4"]."',
                '".$data["more_other_salary4"]."',
                '".$data["features4"]."',
                '".$data['id']."',
                '".$save_date."'
                )";
                $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());
            }
            if(!empty($data["date_start5"]) && !empty($data["date_stop5"])){
                $sql_ = "INSERT INTO JOB_APPLICANT_JOB_HISTORY 
                (JOB_ID,DATE_START,DATE_STOP,POSITION,COMPANY,SALARY,OTHER_SALARY,MORE_OTHER_SALARY,FEATURES,APPLICANT_ID,Job_save_date) 
                VALUES (
                '".$id['computed']."',
                '".$data["date_start5"]."',
                '".$data["date_stop5"]."',
                '".$data["position5"]."',
                '".$data["company5"]."',
                '".$data["salary5"]."',
                '".$data["other_salary5"]."',
                '".$data["more_other_salary5"]."',
                '".$data["features5"]."',
                '".$data['id']."',
                '".$save_date."'
                )";
                $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());
            }
            if($result == 1){ 
                return json_encode(array('message'=>'success','status'=>'1')); 
            }else{ 
                return json_encode(array('message'=>'unsuccess','status'=>'0')); 
            }
        }
    }

    public function language_add($data){
        // =================== JOB_APPLICANT_LANGUAGE
        if ($data["language1"]) {
            $sql_ = "INSERT INTO JOB_APPLICANT_LANGUAGE 
                    (APPLICANT_ID,LANGUAGE,TALK,WRITE,READS) 
                    VALUES (
                    '".$data['id']."',
                    '".$data["language1"]."',
                    '".$data["talk1"]."',
                    '".$data["write1"]."',
                    '".$data["read1"]."'
                    )";
            $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());
        }
        if ($data["language2"]) {
            $sql_1 = "INSERT INTO JOB_APPLICANT_LANGUAGE 
                    (APPLICANT_ID,LANGUAGE,TALK,WRITE,READS) 
                    VALUES (
                    '".$data['id']."',
                    '".$data["language2"]."',
                    '".$data["talk2"]."',
                    '".$data["write2"]."',
                    '".$data["read2"]."'
                    )";
                $result = mssql_query($sql_1)or die('A error occured: ' . mysql_error());
        }
        if ($data["language3"]) {
            $sql_2 = "INSERT INTO JOB_APPLICANT_LANGUAGE 
                    (APPLICANT_ID,LANGUAGE,TALK,WRITE,READS) 
                    VALUES (
                    '".$data['id']."',
                    '".$data["language3"]."',
                    '".$data["talk3"]."',
                    '".$data["write3"]."',
                    '".$data["read3"]."'
                    )";
                $result = mssql_query($sql_2)or die('A error occured: ' . mysql_error());
        }
        if ($data["language4"]) {
            $sql_3 = "INSERT INTO JOB_APPLICANT_LANGUAGE 
                    (APPLICANT_ID,LANGUAGE,TALK,WRITE,READS) 
                    VALUES (
                    '".$data['id']."',
                    '".$data["language4"]."',
                    '".$data["talk4"]."',
                    '".$data["write4"]."',
                    '".$data["read4"]."'
                    )";
                $result = mssql_query($sql_3)or die('A error occured: ' . mysql_error());
        }
                // ===================
        if($result == 1){ 
            return json_encode(array('message'=>'success','status'=>'1')); 
        }else{ 
            return json_encode(array('message'=>'unsuccess','status'=>'0')); 
        }
    }

    public function attachment_add_file($data){
        $sql_ = "SELECT * FROM JOB_APPLICANT WHERE ID = '".$data["id"]."'";
        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());
        $num_rows =  mssql_num_rows($objdetail);

        if($num_rows >= 1){
             $sql_ = "UPDATE JOB_APPLICANT SET ATTACHMENT_FILE = '".$data["attachment_file"]."' WHERE ID = '".$data["id"]."'";
        
            $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());

            if($result == 1){ 
                return json_encode(array('message'=>'success','status'=>'1')); 
            }else{ 
                @unlink('../../../fileupload/candidacy/' . $data['attachment_file']);
                return json_encode(array('message'=>'unsuccess','status'=>'0')); 
            }
        }
        else{
            return json_encode(array('message'=>'unsuccess','status'=>'0')); 
        }
    }

    public function attachment_add_img($data){
        $sql_ = "SELECT * FROM JOB_APPLICANT WHERE ID = '".$data["id"]."'";
        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());
        $num_rows =  mssql_num_rows($objdetail);

        if($num_rows >= 1){
             $sql_ = "UPDATE JOB_APPLICANT SET ATTACHMENTFILE_IMAGE = '".$data["attachment_image"]."' WHERE ID = '".$data["id"]."'";
        
            $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());

            if($result == 1){ 
                return json_encode(array('message'=>'success','status'=>'1')); 
            }else{ 
                @unlink('../../../fileupload/candidacy/' . $data['attachment_image']);
                return json_encode(array('message'=>'unsuccess','status'=>'0')); 
            }
        }
        else{
            return json_encode(array('message'=>'unsuccess','status'=>'0')); 
        }
    }

    public function candidacy_add($data){
       

        $sql_ = "SELECT * FROM JOB_APPLICANT WHERE CARD_NAMBER = '".$data["card_namber"]."'  AND WORK_ID = '".$data["work_id"]."' ";
        $objdetail = mssql_query($sql_)or die('A error occured: ' . mysql_error());
        $num_rows =  mssql_num_rows($objdetail);

        if($num_rows >= 1){
            return json_encode(array('message'=>'data duplicate','status'=>'2'));
        }
        else{
           $sql_ = "INSERT INTO JOB_APPLICANT 
            (WORK_ID,PROVINCE_ID,COMMENT_ID,PREFIX,NAME,LASTNAME,NICKNAME,SEX,BIRTHDAY,HEIGHT,WEIGHT,RELIGION,RACE,NATIONSLITY,DRAFT,CARD_NAMBER,STATUS,MOREDATA,CHILDREN,PRINT_TH,PRINT_EN,CREATE_DAT,PROFILE_COM ) 
            VALUES (
            ".$data["work_id"].",".$data["provine_id"].",4,'".$data["prefix"]."','".$data["name"]."','".$data["lastname"]."','".$data["nickname"]."','".$data["sex"]."','".$data["birthday"]."',".$data["height"].",".$data["weight"].",'".$data["religion"]."','".$data["race"]."','".$data["nationslity"]."','".$data["draft"]."','".$data["card_namber"]."','".$data["status"]."','".$data["moredata"]."','".$data["children"]."','".$data["printTH"]."','".$data["printEN"]."',GETDATE(),'".$data["profile_com"]."')";
            $result = mssql_query($sql_)or die('A error occured: ' . mysql_error());

            if($result == 1){ 
                // $sql_id = "SELECT SCOPE_IDENTITY() AS computed";
                // $id = mssql_fetch_assoc(mssql_query($sql_id));
                $sql_id = "SELECT MAX(ID) as computed FROM JOB_APPLICANT ";
                $id = mssql_fetch_assoc(mssql_query($sql_id)); 

                $sql_1 = "INSERT INTO JOB_APPLICANT_CONTACT 
                (APPLICANT_ID,PROVINCE_ID,ADDRESS,PHONE,EMAIL,NAME_EMER,PHONE_EMER) 
                VALUES (
                '".$id['computed']."',
                '".$data["provine_contact_id"]."',
                '".$data["address"]."',
                '".$data["phone"]."',
                '".$data["email"]."',
                '".$data["name_emer"]."',
                '".$data["phone_emer"]."'
                )";
                mssql_query($sql_1)or die('A error occured: ' . mysql_error());

                $sql_2 = "INSERT INTO JOB_APPLICANT_JOB_INTERESTED 
                (APPLICANT_ID,POSITION,TYPE_JOB,SALARY_HOPE,START_DAY) 
                VALUES (
                '".$id['computed']."',
                '".$data["position"]."',
                '".$data["type_job"]."',
                '".$data["salary_hope"]."',
                '".$data["start_day"]."'
                )";
                mssql_query($sql_2)or die('A error occured: ' . mysql_error());

                $sql_3 = "INSERT INTO JOB_APPLICANT_PERSON 
                (APPLICANT_ID,NAME,POSITION,COMPANY,RELATIONSHIP,TELPERSON) 
                VALUES (
                '".$id['computed']."',
                '".$data["name_person"]."',
                '".$data["work_person"]."',
                '".$data["company"]."',
                '".$data["relationship"]."',
                '".$data["telperson"]."'
                )";
                mssql_query($sql_3)or die('A error occured: ' . mysql_error());

                $sql_4 = "INSERT INTO JOB_APPLICANT_DATAS 
                (APPLICANT_ID,WORKING_ALWAYS,WORKING_SOMETIMES,DISEASE,CRIME,BANKRUPT,LAY_OFF) 
                VALUES (
                '".$id['computed']."',
                '".$data["working_always"]."',
                '".$data["working_sometimes"]."',
                '".$data["disease"]."',
                '".$data["crime"]."',
                '".$data["bankrupt"]."',
                '".$data["lay_off"]."'
                )";
                mssql_query($sql_4)or die('A error occured: ' . mysql_error());

               
                return json_encode(array('message'=>'success','status'=>'1','id'=>$id['computed'])); 
            }else{ 
                return json_encode(array('message'=>'unsuccess','status'=>'0')); 
            }
            
        }
    }



}  

?>  