<?php  
require_once 'dbConnect.php';   
    class dbZones {  
            
         public $conn;   
        function __construct() {
             ini_set('display_errors', 1);
             error_reporting(~0);  
              
             $this->conn = (new dbConnect())->getConn();
             //print_r($this->conn);
        }  
        // destructor  
        function __destruct() {  
              
        }  

        public function list_zone($zone_id){ 
            //require_once('dbConnect.php');  
            $query = "SELECT * FROM dbo.LH_ZONES WHERE zone_id ='$zone_id'";
            $stmt = $this->conn->query( $query ); 

                return $stmt;  
             
        }

        public function update_zone($zone_name_th,$zone_name_en,$zone_ac,$zone_id,$zone_provice){ 
            include './include/dbCon_mssql.php'; 
                 $dates = date("Y-m-d"); 
                 $result='';
                foreach ($zone_ac as $key => $value) {
                     $result=$result.$value;       
                }
                 $query_up = "UPDATE LH_ZONES SET zone_name_th='$zone_name_th',zone_name_en='$zone_name_en',zone_attribute='$result', zone_update = '$dates' , province_id='$zone_provice' WHERE zone_id='$zone_id'";
                 $stmt= mssql_query($query_up , $db_conn);

                //$row_count = $stmt->rowCount();
                if($stmt){  
                        return $stmt;  
                    }else{
                       return false; 
                    }
                     
        }

        public function delete_zone($zone_id){ 
            //require_once('dbConnect.php');  
            $query = "DELETE FROM dbo.LH_ZONES WHERE zone_id = '$zone_id' ";
            $stmt = $this->conn->query( $query ); 
            //$row_count = $stmt->rowCount();

            if($stmt){  
                return $stmt;  
            } else {  
                return false;  
            }  
        }

        public function show_zone($zone_id){ 
            //require_once('dbConnect.php');  
            $query = "SELECT * FROM dbo.LH_ZONES";
            $stmt = $this->conn->query( $query ); 
            $row_count = $stmt->rowCount();

            if($row_count < 0 ){  
                return $stmt;  
            } else {  
                return false;  
            }  
        }



        public function add_zone($zone_name_th,$zone_name_en,$zone_ac,$zone_provice){ 
             include './include/dbCon_mssql.php'; 
             $dates = date("Y-m-d");  
             $query_s = "SELECT COUNT(*) AS counts FROM dbo.LH_ZONES WHERE zone_name_th = '$zone_name_th' OR zone_name_en = '$zone_name_en'";
             $stmt_s = $this->conn->query( $query_s );
             $row = $stmt_s->fetch( PDO::FETCH_ASSOC );
             $row['counts'];

            if($row['counts'] <= 0){
                $result='';
                foreach ($zone_ac as $key => $value) {
                     $result=$result.$value;       
                }

                 $query = "INSERT INTO dbo.LH_ZONES (zone_name_th, zone_name_en, zone_attribute, zone_update,province_id )
        VALUES ('$zone_name_th','$zone_name_en','$result','$dates','$zone_provice' ); ";
                    $stmt= mssql_query($query , $db_conn);
                    //$row_count = $stmt->rowCount();

                    if($stmt){  
                        return $stmt;  
                    
                    }else{
                        return false;
                    } 
            }else{
                return false;
            }
        }  
    }  
?>  