<?php
session_start();
include './include/dbCon_mssql.php';
$group_id = $_SESSION['group_id'];

$_GET['page'] = 'managment';
$id = $_GET['id'];

if (isset($_SESSION['add'])) {
	if ($_SESSION['add'] == 1) {
		//header("location:product_add.php");
		$success = "success";
		unset($_SESSION["add"]);
	} else if ($_SESSION['add'] == 2) {

		$error_name = "error_name"; //ค่าซ้ำ
		unset($_SESSION["add"]);

	} else if ($_SESSION['add'] == -1) {
		$error = "errors";
		unset($_SESSION["add"]);
	} else if ($_SESSION['add'] == -2) {
		$error_up = "error_up";
		unset($_SESSION["add"]);
	}
}
?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <!-- Meta, title, CSS, favicons, etc. -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>LAND & HOUSES</title>

      <!-- comfirm -->
      <link rel="stylesheet" href="../build/libs/bundled.css">
      <script src="../build/libs/bundled.js"></script>


      <!-- uploade img -->
      <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
      <!--uploade-->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
      <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>


      <!-- Bootstrap -->
      <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome -->
      <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <!-- NProgress -->
      <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
      <!-- iCheck -->
      <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
      <!-- Datatables -->
      <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
      <!-- Fancybox image popup -->
      <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
      <style type="text/css">

      th.next.available{
        background-color: #6f9755;
      }
      th.prev.available{
        background-color: #6f9755;
      }
    </style>

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
    <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>


    <!-- Include Editor style. -->
    <link href="editor/css/froala_editor.min.css" rel="stylesheet" type="text/css" />
    <link href="editor/css/froala_style.min.css" rel="stylesheet" type="text/css" />

    <!-- Include Editor style. -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.3.4/css/froala_editor.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.3.4/css/froala_style.min.css" rel="stylesheet" type="text/css" />

    <!-- Include JS file. -->
    <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.3.4/js/froala_editor.min.js"></script> -->

    <!-- Include Code Mirror style -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">

    <!-- Include Editor Plugins style. -->
    <link rel="stylesheet" href="editor/css/plugins/char_counter.css">
    <link rel="stylesheet" href="editor/css/plugins/code_view.css">
    <link rel="stylesheet" href="editor/css/plugins/colors.css">
    <link rel="stylesheet" href="editor/css/plugins/emoticons.css">
    <link rel="stylesheet" href="editor/css/plugins/file.css">
    <link rel="stylesheet" href="editor/css/plugins/fullscreen.css">
    <link rel="stylesheet" href="editor/css/plugins/image.css">
    <link rel="stylesheet" href="editor/css/plugins/image_manager.css">
    <link rel="stylesheet" href="editor/css/plugins/line_breaker.css">
    <link rel="stylesheet" href="editor/css/plugins/quick_insert.css">
    <link rel="stylesheet" href="editor/css/plugins/table.css">
    <link rel="stylesheet" href="editor/css/plugins/video.css">
    <!-- Fancybox image popup -->
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />

    <!-- confirm-->
    <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
    <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>
    <style type="text/css">

    th.next.available{
      background-color: #6f9755;
    }
    th.prev.available{
      background-color: #6f9755;
    }
  </style>

  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.js"></script> -->

  <style type="text/css">
  .fr-toolbar.fr-desktop.fr-top.fr-basic.fr-sticky.fr-sticky-off {
    background: #f7f7f7  !important;
    border-top: 5px solid #f5f5f5  !important;
  }
  th.next.available {
    background: #6f9755;
  }
  th.next.available:hover {
    background: #9ab688;
  }
  th.prev.available {
    background: #6f9755;
  }
  th.prev.available:hover {
    background: #9ab688;
  }
  .thumbnail {
    height: 100px;
    margin: 5px;
  }
  .fileUpload {
    position: relative;
    overflow: hidden;
    //margin: 10px;
  }
  .fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
  }
  .file-drop-zone-title {
    padding: 0px 0px !important;
  }

</style>
<style>
.file-caption-main .btn-file {
  overflow: visible;
}

.file-caption-main .btn-file .error {
  position: absolute;
  bottom: -32px;
  right: 30px;
}
</style>
</head>

<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;">
            <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
          </div>

          <div class="clearfix"></div>

          <!-- menu profile quick info -->
          <?php include './master/navbar.php';?>
          <!-- /menu footer buttons -->
        </div>
      </div>

      <!-- top navigation -->
      <?php include './master/top_nav.php';?>
      <!-- /top navigation -->
      <?php
$sql = "SELECT * FROM LH_COMMUNITY_PAGE WHERE community_id = '$id'";
$query = mssql_query($sql);
$row = mssql_fetch_array($query);

?>
      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><a href="managment_web.php?tab=Community">ระบบจัดการข้อมูล LH Community</a> > แก้ไข LH Community :
                    <?php
$row['community_name_th'] . " (" . $row['community_name_en'] . ")";
$str = strlen($row['community_name_th']);
if ($str >= 67) {
	echo iconv_substr($row['community_name_th'], 0, 66, "UTF-8") . "...";
} else {
	echo $row['community_name_th'];
}
?> </h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                   <?php if (isset($success)) {?>
                   <div class="row">
                    <div class="col-md-2"></div>
                    <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>แก้ไขข้อมูลเรียบร้อยแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-6"></div>
                  </div>
                  <?php } else if (isset($error_name)) {?>
                  <div class="row">
                    <div class="col-md-2"></div>
                    <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>มีชื่อนี้อยู่ในระบบแล้ว </strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                  </div>
                  <?php } else if (isset($error_delete)) {?>

                  <div class="row">
                    <div class="col-md-2"></div>
                    <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>ลบข้อมูลเรียบร้อยแล้ว !</strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                  </div>
                  <?php } else if (isset($error_up)) {?>
                  <div class="row">
                    <div class="col-md-2"></div>
                    <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>เกิดข้อผิดพลาด !</strong> ไม่สามารถบันทึกข้อมูลได้
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                  </div>
                  <?php }?>
                  <p class="font-gray-dark">
                  </p><br>

                  <form class="form-horizontal form-label-left" action="update_community.php" method="POST" enctype="multipart/form-data" id="commentForm">
                    <input type="hidden" name="id" value="<?=$row['community_id']?>">
                    <div class="form-group">
                      <label class="control-label col-md-3" for="first-name">ชื่อ LH Community (ไทย)
                        <span class="required">*</span>
                      </label>
                      <div class="col-md-6">

                        <input type="text" id="first-name1" required class="form-control col-md-7 col-xs-12"  name="community_name_th" placeholder="ชื่อ LH Community (ไทย)" value="<?=htmlspecialchars($row['community_name_th']) ?>">
                      </div>
                    </div>

                   


                    <div class="form-group hide-for-th">
                      <label class="control-label col-md-3" for="first-name">ชื่อ LH Community (อังกฤษ)
                        <span class="required">*</span>
                      </label>
                      <div class="col-md-6">
                        <input type="text" id="first-name2"  class="form-control col-md-7 col-xs-12"  name="community_name_en" placeholder="ชื่อ LH Community (อังกฤษ)" value="<?=$row['community_name_en'] ?>">
                      </div>
                    </div>

                    <?php
$req = "*";
$required = "required";
?>
                    <?php if (isset($row['community_img']) && ($row['community_img'] != "")) {
	$req = "";
	$required = "";
	?>
                      <div class="form-group">
                        <label class="control-label col-md-3" for="first-name">รูปตัวอย่าง LH Community * <br> (ความกว้างต่ำสุด 500px - ความกว้างสูงสุด 1920px)<br> (ไฟล์ jpg , gif , png) <br> ภาพขนาดไม่เกิน 300 KB
                        </label>
                        <div class="col-md-6">
                          <div id="map-image-holder-brochure"></div>
                          <div class="file-preview ">
                            <div class="close fileinput-remove"></div>
                            <div class="file-drop-disabled">
                              <div class="file-preview-thumbnails">
                                <div class="file-initial-thumbs">
                                  <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                    <div class="kv-file-content">
                                      <a class="fancybox" href="<?=$row['community_img']?>">
                                        <img src="<?=$row['community_img']?>" class="kv-preview-data file-preview-image" style="width:auto;height:160px;">
                                      </a>
                                    </div>

                                    <div class="file-thumbnail-footer">
                                     <p>Alt text : <?=$row['community_img_seo']?></p>
                                     <div class="file-actions">
                                      <div class="file-footer-buttons">
                                        <button type="button" id="<?php echo $row['community_id']; ?>" onclick="deleteImageCommunity(<?php echo $row['community_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                        <a class="fancybox" href="<?=$row['community_img']?>">
                                          <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                        </a>
                                      </div>
                                      <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="file-preview-status text-center text-success"></div>
                            <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?php }?>



                    <!-- url yputube -->
                    <div class="form-group" id="l1">

                      <label class="control-label col-md-3" for="first-name">เลือกรูป LH Community <?php echo $req; ?><br> (ความกว้างต่ำสุด 500 px - <br> ความกว้างสูงสุด 1920 px)
                      </label>
                      <div class="col-md-6">
                        <input id="community_img" name="community_img" <?php echo $required ?> type="file" multiple class="file-loading" accept="image/*" >
                      </div>
                    </div>

                    <script>
                      $("#community_img").fileinput({
                            uploadUrl: "upload.php", // server upload action
                            maxFileCount: 1,
                            allowedFileExtensions: ["jpg", "gif", "png", "jpeg"],
                            browseLabel: 'เลือกรูป',
                            removeLabel: 'ลบ',
                            browseClass: 'btn btn-success',
                            showUpload: false,
                            showRemove:false,
                            showCaption: false,
                            msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                            msgFilesTooMany: 'ไฟล์ที่คุณเลือกมีจำนวน <b>({n})</b> ซึ่งเกินกว่าที่ระบบอนุญาตที่ <b>{m}</b>, กรุณาลองใหม่อีกครั้ง!',
                            msgZoomModalHeading: 'ตัวอย่างละเอียด',
                            xxx:'seo',
                            dropZoneTitle : 'รูป LH Community',
                             minImageWidth: 500, //ขนาด กว้าง ต่ำสุด
                            // minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                             maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                            //maxImageHeight: 278, //ขนาด ศุง ต่ำสุด
                            maxFileSize :300 ,
                            msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                            msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                            msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                            msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                            msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                          });
                        </script>

                        <script>
                          $(document).ready(function() {
                        //$("div.desc").hide();
                        $("#page2").attr("disabled", 'disabled');
                        $("#check1").click(function() {
                          var test = $(this).val();
                            //$("div.desc").hide();
                            if(test = "project"){
                              $("#page1").removeAttr('disabled');
                              $("#page1").attr("required", true);
                              $("#page2").attr("disabled", 'disabled');
                              $("#page2").val("");
                              //alert(test);
                            }
                            //$("#" + test).show();
                          });

                        $("#check2").click(function() {
                          var test = $(this).val();
                            //$("div.desc").hide();
                            if(test = "page"){
                              $("#page2").removeAttr('disabled');
                              $("#page2").attr("required", true);
                              $("#page1").attr("disabled", 'disabled');
                              $("#page1").val("");


                              //alert(test);
                            }
                            //$("#" + test).show();
                          });
                      });
                    </script>


                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">รายละเอียด LH Community (ไทย) <span class="required">*</span> <br> ใส่ข้อความไม่เกิน 200 คำ</label>
                      <div class="col-md-6 col-sm-9 col-xs-12">
                       <!-- <div id="edit" class="text-left txt-black fr-box fr-basic fr-top" style="z-index: 1000;"></div>  -->
                       <textarea name="community_dis_th" class="form-control" rows="5" placeholder="กรอกรายละเอียด LH Community (ไทย)" required="" maxlength="200"><?=$row['community_dis_th']?></textarea>
                     </div>
                   </div>


                   <div class="form-group hide-for-th">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">รายละเอียด LH Community (อังกฤษ) <br> ใส่ข้อความไม่เกิน 200 คำ</label>
                    <div class="col-md-6 col-sm-9 col-xs-12">

                     <!-- <div id="edit" class="text-left txt-black fr-box fr-basic fr-top" style="z-index: 1000;"></div>  -->
                     <textarea name="community_dis_en" class="form-control" rows="5" placeholder="กรอกรายละเอียด LH Community (อังกฤษ)" maxlength="200"><?=$row['community_dis_en']?></textarea>

                   </div>
                 </div>


                 <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">บทสัมภาษณ์ (ไทย) *</label>
                  <div class="col-md-6 col-sm-9 col-xs-12">

                    <!-- <div id="edit" class="text-left txt-black fr-box fr-basic fr-top" style="z-index: 1000;"></div>  -->
                    <textarea name="community_con_th" required class="form-control" rows="10" placeholder="กรอกรายละเอียด บทสัมภาษณ์ (ไทย)"><?=$row['communuty_content_th']?></textarea>

                  </div>
                </div>


                <div class="form-group hide-for-th">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">บทสัมภาษณ์ (อังกฤษ)</label>
                  <div class="col-md-6 col-sm-9 col-xs-12">

                    <!-- <div id="edit" class="text-left txt-black fr-box fr-basic fr-top" style="z-index: 1000;"></div>  -->
                    <textarea name="community_con_en" class="form-control" rows="10" placeholder="กรอกรายละเอียด บทสัมภาษณ์ (อังกฤษ)"><?=$row['communuty_content_en']?></textarea>

                  </div>
                </div>

                <br>
                <?php
                $url=$url_master.'th/lh-community/'.str_replace(" ", "-", $row['community_name_th']);
                ?>
                <div class="form-group">
                 <label class="control-label col-md-3" for="first-name">
                 </label>
                 <div class="col-md-6">
                  <a href="<?=$url;?>" target="_blank"><button type="button" class="btn btn-warning">Preview</button></a> 
                  <a class="confirms" data-title="ยืนยันการลบข้อมูล" name="delete" href="delete_community.php?id=<?=$_GET['id']?>">
                    <button type="button" class="btn btn-danger">Delete</button></a>
                    <button type="submit" class="btn btn-success">Update</button>
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- /page content -->
      <script type="text/javascript">
        $('a.confirms').confirm({
          content: 'คุณแน่ใจที่จะลบข้อมูลนี้ !',
          buttons: {
            Yes: {
              text: 'Yes',
              btnClass: 'btn-danger',
              keys: ['enter', 'a'],
              action: function(){
                location.href = this.$target.attr('href');
              }
            },
            No: {
              text: 'No',
              btnClass: 'btn-default',
              keys: ['enter', 'a'],
              action: function(){
                          // button action.
                        }
                      },

                    }
                  });
                </script>
                <!-- footer content -->
        <!-- <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer> -->
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <!-- <script src="../vendors/jquery/dist/jquery.min.js"></script> -->
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="js/moment/moment.min.js"></script>
    <script src="js/datepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>


    <!--Editor_script-->

    <!-- Include JS files. -->
    <script type="text/javascript" src="editor/js/froala_editor.min.js"></script>
    <!-- Include Code Mirror. -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
    <!-- Include Plugins. -->
    <script type="text/javascript" src="editor/js/plugins/align.min.js"></script>
    <script type="text/javascript" src="editor/js/plugins/char_counter.min.js"></script>
    <script type="text/javascript" src="editor/js/plugins/code_beautifier.min.js"></script>
    <script type="text/javascript" src="editor/js/plugins/code_view.min.js"></script>
    <script type="text/javascript" src="editor/js/plugins/colors.min.js"></script>
    <script type="text/javascript" src="editor/js/plugins/emoticons.min.js"></script>
    <script type="text/javascript" src="editor/js/plugins/entities.min.js"></script>
    <script type="text/javascript" src="editor/js/plugins/file.min.js"></script>
    <script type="text/javascript" src="editor/js/plugins/font_family.min.js"></script>
    <script type="text/javascript" src="editor/js/plugins/font_size.min.js"></script>
    <script type="text/javascript" src="editor/js/plugins/fullscreen.min.js"></script>
    <script type="text/javascript" src="editor/js/plugins/image.min.js"></script>
    <script type="text/javascript" src="editor/js/plugins/image_manager.min.js"></script>
    <script type="text/javascript" src="editor/js/plugins/inline_style.min.js"></script>
    <script type="text/javascript" src="editor/js/plugins/line_breaker.min.js"></script>
    <script type="text/javascript" src="editor/js/plugins/link.min.js"></script>
    <script type="text/javascript" src="editor/js/plugins/lists.min.js"></script>
    <script type="text/javascript" src="editor/js/plugins/paragraph_format.min.js"></script>
    <script type="text/javascript" src="editor/js/plugins/paragraph_style.min.js"></script>
    <script type="text/javascript" src="editor/js/plugins/quick_insert.min.js"></script>
    <script type="text/javascript" src="editor/js/plugins/quote.min.js"></script>
    <script type="text/javascript" src="editor/js/plugins/table.min.js"></script>
    <script type="text/javascript" src="editor/js/plugins/save.min.js"></script>
    <script type="text/javascript" src="editor/js/plugins/url.min.js"></script>
    <script type="text/javascript" src="editor/js/plugins/video.min.js"></script>

    <!-- Validate -->
    <script src="../build/js/jquery.validate.js"></script>
    <script>
      $(document).ready(function() {

        $("#commentForm").validate({
          rules: {
            community_name_th: "required",
            //community_name_en: "required",
            project: "required",
                    // community_img : "required",
                    community_dis_th : "required",
                    community_con_th : "required",
                  },
                  messages: {
                    community_name_th: "กรุณากรอกชื่อ Community ไทย !",
                    //community_name_en : "กรุณากรอกชื่อ Community อังกฤษ !",
                    project : "กรุณาเลือกโครงการ !",
                    community_img : " &nbsp; กรุณาเลือกรูป !",
                    community_dis_th : "กรุณากรอกรายละเอียด !",
                    community_con_th : "กรุณากรอกบทสัมภาษณ์ !",
                    
                  }
                });
      });
    </script>

    <!-- Fancybox image popup -->
    <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>

    <script type="text/javascript">
      $('.fancybox').fancybox();
    </script>
    <!-- Include Language file if we want to u-->
    <script>
      $(function() {
        $('#edit').froalaEditor({
          height: 500,
          imageUploadURL: 'uploade_highlights.php',
          imageUploadParams: {
            id: 'my_editor'
          },

          fileUploadURL: 'upload_file.php',
          fileUploadParams: {
            id: 'my_editor'
          },

          imageManagerLoadURL: 'uploade_highlights.php',
          imageManagerDeleteURL: "delete_image_highlights.php",
          imageManagerDeleteMethod: "POST"
        })
      // Catch image removal from the editor.
      .on('froalaEditor.image.removed', function (e, editor, $img) {
        $.ajax({
          // Request method.
          method: "POST",

          // Request URL.
          url: "delete_image_highlights.php",

          // Request params.
          data: {
            src: $img.attr('src')
          }
        })
        .done (function (data) {
          console.log ('image was deleted'+$img.attr('src'));
        })
        .fail (function (err) {
          console.log ('image delete problem: ' + JSON.stringify(err));
        })
      })

      // Catch image removal from the editor.
      .on('froalaEditor.file.unlink', function (e, editor, link) {

        $.ajax({
          // Request method.
          method: "POST",

          // Request URL.
          url: "delete_file.php",

          // Request params.
          data: {
            src: link.getAttribute('href')
          }
        })
        .done (function (data) {
          console.log ('file was deleted');
        })
        .fail (function (err) {
          console.log ('file delete problem: ' + JSON.stringify(err));
        })
      })
    });

      $(function() {
        $('#edit_en').froalaEditor({
          height: 500,
          imageUploadURL: 'uploade_highlights.php',
          imageUploadParams: {
            id: 'my_editor'
          },

          fileUploadURL: 'upload_file.php',
          fileUploadParams: {
            id: 'my_editor'
          },

          imageManagerLoadURL: 'uploade_highlights.php',
          imageManagerDeleteURL: "delete_image_highlights.php",
          imageManagerDeleteMethod: "POST"
        })
        // Catch image removal from the editor.
        .on('froalaEditor.image.removed', function (e, editor, $img) {
          $.ajax({
                    // Request method.
                    method: "POST",

                    // Request URL.
                    url: "delete_image_highlights.php",

                    // Request params.
                    data: {
                      src: $img.attr('src')
                    }
                  })
          .done (function (data) {
            console.log ('image was deleted'+$img.attr('src'));
          })
          .fail (function (err) {
            console.log ('image delete problem: ' + JSON.stringify(err));
          })
        })

            // Catch image removal from the editor.
            .on('froalaEditor.file.unlink', function (e, editor, link) {

              $.ajax({
                    // Request method.
                    method: "POST",

                    // Request URL.
                    url: "delete_file.php",

                    // Request params.
                    data: {
                      src: link.getAttribute('href')
                    }
                  })
              .done (function (data) {
                console.log ('file was deleted');
              })
              .fail (function (err) {
                console.log ('file delete problem: ' + JSON.stringify(err));
              })
            })
          });
        </script>

        <script>
          $(document).ready(function(){
            $("#alert").show();
            $("#alert").fadeTo(3000, 500).slideUp(500, function(){
              $("#alert").alert('close');
            });
          });
        </script>

        <script>
          $(document).ready(function(){
            $("#l1").show();
            $("#l2").hide();
            $("#l3").hide();
          });
        </script>

        <script>

          function getval(sel){
           $("#mySelect").val();
           if($("#mySelect").val() == "image"){
            $("#l1").show();
            $("#l2").hide();
            $("#l3").hide();
          }else if($("#mySelect").val() == "banner"){
            $("#l2").show();
            $("#l3").hide();
            $("#l1").hide();
          }else if($("#mySelect").val() == "vdo"){
            $("#l3").show();
            $("#l1").hide();
            $("#l2").hide();
          }
        }
      </script>
      <script>
        function deleteImageCommunity(id) {
          $.confirm({
            title: 'ลบรูปนี้ !',
            content: 'คุณแน่ใจที่จะลบรูปนี้!',
            buttons : {
              Yes: {
                text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                              var self = this;
                              return $.ajax({
                                url: 'ajax_deleteImageCommunity.php?community_id=' + id,
                                dataType: 'json',
                                method: 'get'
                              }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                  }).fail(function () {
                                    self.setContent('Something went wrong.');
                                  });
                                }
                              },
                              No: function () {
                            //$.alert('ยกลเิก!');
                          },
                        }
                      });
        }
                  // function deleteImageLivingTip(id){
                  //     if(confirm('Do you want to delete')) {
                  //         var imageId = id;
                  //         var url_img = "ajax_deleteImageLivingTip.php";
                  //         var link = url_img+'?living_tip_id='+id;
                  //         $.ajax( link )
                  //             .done(function(response) {
                  //                 if(response != ""){
                  //                     //re get images.
                  //                     location.reload();
                  //                 }else{
                  //                     alert('Delete image fail.')
                  //                     console.error('Backend error');
                  //                 }
                  //             })
                  //             .fail(function() {

                  //                 $('#images_in_homesales').empty();
                  //                 alert('Delete image fail.')
                  //                 console.error('Backend error');
                  //             })
                  //             .always(function() {
                  //                 console.info( "complete" );
                  //             });
                  //     }
                  // }
                </script>


  <!-- <script>
  $(function() {
    $('#edit').froalaeditor({
        height: 500,
        // Set the image upload parameter.
        imageUploadParam: 'image_param',

        // Set the image upload URL.
        imageUploadURL: '/upload_image',

        // Additional upload params.
        imageUploadParams: {id: 'my_editor'},

        // Set request type.
        imageUploadMethod: 'POST',

        // Set max image size to 5MB.
        imageMaxSize: 5 * 1024 * 1024,

        // Allow to upload PNG and JPG.
        imageAllowedTypes: ['jpeg', 'jpg', 'png'],
      })

  });
</script> -->
<script src="js/validate_file_300kb.js"></script>

</body>
</html>