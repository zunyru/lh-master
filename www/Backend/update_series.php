<?php
session_start();
include './include/dbCon_mssql.php';
date_default_timezone_set('Asia/Bangkok');
$date         = date("Ymd");
$numrand_logo = (mt_rand());
$numrand_img  = (mt_rand());
$dates=date("Y-m-d H:i:s");

$series_name_th = mssql_escape($_POST['series_name_th']);
$series_name_en = $_POST['series_name_en'];
$series_des_th  = $_POST['series_des_th'];
$series_des_en  = $_POST['series_des_en'];

$logo_series  = $_FILES['logo_series'];
$image_series = $_FILES['images'];

$url_youtube = $_POST['url_youtube'];
$url_360     = $_POST['url_360'];
$id          = $_POST['id'];
$c_360       = $_POST['c_360'];
$switch = $_POST['switch'];

$query        = "SELECT COUNT(*) AS counts FROM dbo.LH_SERIES WHERE series_name_th = '$series_name_th' AND NOT series_id = '$id' ";
$query_result = mssql_query($query, $db_conn);

$count = mssql_fetch_row($query_result);
$count[0];
if ($count[0] >= 1) {
    //มีชื่อ series นี้อยู่แล้ว
    //header("location:series_add.php?add=0");
    $_SESSION['add'] = '2';
    echo '<script>
                        window.history.go(-1);
                    </script>';
    exit();
}

function mssql_escape($str)
{
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return str_replace("'", "''", $str);
}

$file_logo = $logo_series['name'];

if ($file_logo != '') {
    //for($i=0; $i < count($file_logo); $i++){
    $series_logo_seo = $_POST['series_logo_seo'][0];

    $sql = "SELECT series_logo FROM dbo.LH_SERIES WHERE series_id = '" . $_POST['id'] . "' ";

    $query_result = mssql_query($sql, $db_conn);

    $series_logo = mssql_fetch_row($query_result);
    $series_logo['series_logo'];

    unlink('fileupload/images/series_img/series_img_logo/' . $series_logo['series_logo']);

    $path_logo = "fileupload/images/series_img/series_img_logo/";

    $type_logo = strrchr($_FILES['logo_series']['name'], ".");

    $newname_logo   = $date . $numrand_logo . $type_logo;
    $path_copy_logo = $path_logo . $newname_logo;

    //        $seo_img = $_POST["seo_logo"][$i];
    if (move_uploaded_file($_FILES['logo_series']['tmp_name'], $path_copy_logo)) {

        $dates = date("Y-m-d");

        //update logo series
        $query_2 = "UPDATE LH_SERIES SET series_logo = '" . $newname_logo . "', series_logo_seo = '$series_logo_seo',update_date = '$dates', views = '$switch'
                WHERE series_id = '" . $_POST['id'] . "';";

        $query_result2 = mssql_query($query_2, $db_conn);

    }
    //header("location:series_update.php?add=1");
}

$file_image = $image_series['name'];
//update images
if ($file_image != '') {
    $seo_img_series = $_POST['seo_img_series'][0];

    $sql2 = "SELECT series_img_name FROM dbo.LH_SERIES_IMG WHERE series_id = '" . $_POST['id'] . "' ";

    $query_result2 = mssql_query($sql2, $db_conn);
    $series_img    = mssql_fetch_row($query_result2);
    $series_img[0];

    unlink('fileupload/images/series_img/' . $series_img[0]);

    $path_logo = "fileupload/images/series_img/";

    $type_logo = strrchr($_FILES['images']['name'], ".");

    $newname_logo   = $date . $numrand_img . $type_logo;
    $path_copy_logo = $path_logo . $newname_logo;

    if (move_uploaded_file($_FILES['images']['tmp_name'], $path_copy_logo)) {

        $dates = date("Y-m-d");

        //update logo series
        $query_2 = "UPDATE LH_SERIES_IMG SET series_img_name = '" . $newname_logo . "', series_img_seo = '$seo_img_series'
                WHERE series_id = '" . $_POST['id'] . "';";

        $query_result2 = mssql_query($query_2, $db_conn);
        $success_logo  = true;
    }

}

$query_2 = "UPDATE LH_SERIES SET series_name_th = '" . $series_name_th . "' ,"
. " series_name_en = '" . $series_name_en . "' ,"
. " series_des_th = '" . mssql_escape($series_des_th) . "' ,"
. "update_date = '$dates',"
. " series_des_en = '" . mssql_escape($series_des_en) . "', "
. " views = '$switch' "
    . " WHERE series_id = '" . $_POST['id'] . "'";

$query_result2 = mssql_query($query_2, $db_conn);

//360

if ($_FILES["c_360_img"]["name"][0] != "" && $_FILES["c_360_img"]["name"][0] != null && $c_360 != "" && $c_360 != null) {

    $path_img = "fileupload/images/360_thumnail/";

    $c_360            = $_POST['c_360'];
    $numrand_img3     = (mt_rand());
    $path_copy_img360 = "fileupload/images/360_thumnail/" . $numrand_img3 . '_' . $_FILES["c_360_img"]["name"][0];
    if (move_uploaded_file($_FILES['c_360_img']['tmp_name'][0], $path_copy_img360)) {
        $sql   = "SELECT series_360_thumbnail FROM LH_SERIES WHERE series_id = '$id'";
        $query = mssql_query($sql, $db_conn);
        $row   = mssql_fetch_array($query);
        unlink($path_img . $row['series_360_thumbnail']);

        $sql7     = "UPDATE LH_SERIES SET series_360 = '" . $c_360 . "',series_360_thumbnail= '" . $path_copy_img360 . "',update_date = '$dates' WHERE series_id = '$id'";
        $query_r7 = mssql_query($sql7, $db_conn);
    }
} else if ($_FILES["c_360_img"]["name"][0] == "" && $_FILES["c_360_img"]["name"][0] == null && $c_360 == "" && $c_360 == null) {
    $sql2 = "SELECT series_360_thumbnail FROM dbo.LH_SERIES WHERE series_id = '" . $_POST['id'] . "' ";

    $query_result2 = mssql_query($sql2, $db_conn);
    $series_360    = mssql_fetch_row($query_result2);
    $series_360[0];
    if ($series_360[0] != "") {
        $_SESSION['add'] = '-2';

        echo '<script>
                        window.history.go(-1);
                    </script>';
        exit();
    } else {
        $path_img = "fileupload/images/360_thumnail/";

        $c_360            = $_POST['c_360'];
        $numrand_img3     = (mt_rand());
        $path_copy_img360 = "fileupload/images/360_thumnail/" . $numrand_img3 . '_' . $_FILES["c_360_img"]["name"][0];
        if (move_uploaded_file($_FILES['c_360_img']['tmp_name'][0], $path_copy_img360)) {
            $sql   = "SELECT series_360_thumbnail FROM LH_SERIES WHERE series_id = '$id'";
            $query = mssql_query($sql, $db_conn);
            $row   = mssql_fetch_array($query);
            unlink($path_img . $row['series_360_thumbnail']);

            $sql7     = "UPDATE LH_SERIES SET series_360 = '" . $c_360 . "',series_360_thumbnail= '" . $path_copy_img360 . "',update_date = '$dates' WHERE series_id = '$id'";
            $query_r7 = mssql_query($sql7, $db_conn);
        }
    }
} else if ($_FILES["c_360_img"]["name"][0] != "" && $_FILES["c_360_img"]["name"][0] != null && $c_360 == "" && $c_360 == null) {
    $_SESSION['add'] = '-2';

    echo '<script>
                        window.history.go(-1);
                    </script>';
    exit();
} else if ($_FILES["c_360_img"]["name"][0] == "" && $_FILES["c_360_img"]["name"][0] == null && $c_360 != "" && $c_360 != null) {
    $sql2 = "SELECT series_360_thumbnail FROM dbo.LH_SERIES WHERE series_id = '" . $_POST['id'] . "' ";

    $query_result2 = mssql_query($sql2, $db_conn);
    $series_360    = mssql_fetch_row($query_result2);
    $series_360[0];
    if ($series_360[0] != "") {
        $path_img = "fileupload/images/360_thumnail/";

        $sql7     = "UPDATE LH_SERIES SET series_360 = '" . $c_360 . "',update_date = '$dates' WHERE series_id = '$id'";
        $query_r7 = mssql_query($sql7, $db_conn);

        $c_360            = $_POST['c_360'];
        $numrand_img3     = (mt_rand());
        $path_copy_img360 = "fileupload/images/360_thumnail/" . $numrand_img3 . '_' . $_FILES["c_360_img"]["name"][0];
        if (move_uploaded_file($_FILES['c_360_img']['tmp_name'][0], $path_copy_img360)) {
            $sql   = "SELECT series_360_thumbnail FROM LH_SERIES WHERE series_id = '$id'";
            $query = mssql_query($sql, $db_conn);
            $row   = mssql_fetch_array($query);
            unlink($path_img . $row['series_360_thumbnail']);

            $sql7     = "UPDATE LH_SERIES SET series_360 = '" . $c_360 . "',series_360_thumbnail= '" . $path_copy_img360 . "',update_date = '$dates' WHERE series_id = '$id'";
            $query_r7 = mssql_query($sql7, $db_conn);
        }
    } else {
        $_SESSION['add'] = '-2';

        echo '<script>
                        window.history.go(-1);
                    </script>';
        exit();
    }
}

//tvc

if ($_POST['optradio_vdo'] == 'youtube') {

    if ($_FILES["img_youtube"]["name"] != "" && $_FILES["img_youtube"]["name"] != null && $url_youtube != "" && $url_youtube != null) {
        $path_copy_img = "fileupload/images/tvc_plan/" . $numrand_img3 . '_' . $_FILES["img_youtube"]["name"];
        if (move_uploaded_file($_FILES['img_youtube']['tmp_name'], $path_copy_img)) {
            $numrand_img3 = (mt_rand());
            $url_youtube  = $_POST['url_youtube'];
            $sql7         = "UPDATE LH_SERIES SET series_youtube = '" . $url_youtube . "',series_youtube_thumbnail= '" . $path_copy_img . "',series_youtube_type = 'youtube',update_date = '$dates' WHERE series_id = '" . $id . "'";
            $query_r7     = mssql_query($sql7, $db_conn);
            $query        = mssql_query($sql);

        }
    } else if ($_FILES["img_youtube"]["name"] == "" && $_FILES["img_youtube"]["name"] == null && $url_youtube != "" && $url_youtube != null) {

        $sql2 = "SELECT series_youtube_thumbnail FROM dbo.LH_SERIES WHERE series_id = '" . $_POST['id'] . "' ";

        $query_result2            = mssql_query($sql2, $db_conn);
        $series_youtube_thumbnail = mssql_fetch_row($query_result2);
        $series_youtube_thumbnail[0];
        if ($series_youtube_thumbnail[0] == "") {
            $_SESSION['add'] = '-4';

            echo '<script>
                              window.history.go(-1);
                          </script>';
            exit();
        } else {
            $numrand_img3 = (mt_rand());
            $url_youtube  = $_POST['url_youtube'];

            $url_youtube = $_POST['url_youtube'];
            $sql7        = "UPDATE LH_SERIES SET series_youtube= '" . $url_youtube . "',series_youtube_type = 'youtube',update_date = '$dates' WHERE series_id = '" . $id . "'";
            $query_r7    = mssql_query($sql7, $db_conn);
            $query       = mssql_query($sql);
        }
    } else if ($_FILES["img_youtube"]["name"] != "" && $_FILES["img_youtube"]["name"] != null && $url_youtube == "" && $url_youtube == null) {
        $_SESSION['add'] = '-4';
        echo '<script>
                                    window.history.go(-1);
                                </script>';
        exit();
    } else if ($_FILES["img_youtube"]["name"] == "" && $_FILES["img_youtube"]["name"] == null && $url_youtube == "" && $url_youtube == null) {
        $sql1 = "SELECT series_youtube_type FROM dbo.LH_SERIES WHERE series_id = '" . $_POST['id'] . "' ";

        $query_result2 = mssql_query($sql1, $db_conn);
        $series_chk    = mssql_fetch_row($query_result2);
        $series_chk[0];

        if ($series_chk[0] == "vdo") {

            $sql7     = "UPDATE LH_SERIES SET series_youtube = '',series_youtube_thumbnail= '',series_youtube_type = 'youtube',update_date = '$dates' WHERE series_id = '" . $id . "'";
            $query_r7 = mssql_query($sql7, $db_conn);
            $query    = mssql_query($sql);

            $_SESSION['add'] = '1';
            header("location:series_update.php?id=$id&add=1");
        } else if ($series_chk[0] != "vdo") {
            $sql2 = "SELECT series_youtube_thumbnail FROM dbo.LH_SERIES WHERE series_id = '" . $_POST['id'] . "' ";

            $query_result2            = mssql_query($sql2, $db_conn);
            $series_youtube_thumbnail = mssql_fetch_row($query_result2);
            $series_youtube_thumbnail[0];
            if ($series_youtube_thumbnail[0] != "") {
                $_SESSION['add'] = '-4';

                echo '<script>
                              window.history.go(-1);
                          </script>';
                exit();
            } else {
                $numrand_img3 = (mt_rand());
                $url_youtube  = $_POST['url_youtube'];

                $url_youtube = $_POST['url_youtube'];
                $sql7        = "UPDATE LH_SERIES SET series_youtube = '" . $url_youtube . "',series_youtube_type = 'youtube',update_date = '$dates' WHERE series_id = '" . $id . "'";
                $query_r7    = mssql_query($sql7, $db_conn);
                $query       = mssql_query($sql);

            }
        }
    }
} else if ($_POST['optradio_vdo'] == 'vdo') {

    $images = $_FILES['thumbnail_vdo'];
    $vdo    = $_FILES['vdo'];

    $file_image = $images['name'];
    $file_vdo   = $vdo['name'];

    if ($_FILES["thumbnail_vdo"]["name"] != "" && $_FILES["vdo"]["name"] != "") {
        $numrand_img3 = (mt_rand());
        $numrand_vdo  = (mt_rand());

        $dates_file = date("Y-m-d");
        $path_img   = "fileupload/images/tvc_plan/";

        $type_img = strrchr($file_image, ".");
        $type_vdo = strrchr($file_vdo, ".");

        $newname_img = $date . $numrand_img3 . $type_img;
        $newname_vdo = $date . $numrand_vdo . "_vdo" . $type_vdo;

        $path_copy_img = $path_img . $newname_img;
        $path_copy_vdo = $path_img . $newname_vdo;

        if (move_uploaded_file($_FILES['thumbnail_vdo']['tmp_name'], $path_copy_img)) {
            if (move_uploaded_file($_FILES['vdo']['tmp_name'], $path_copy_vdo)) {

                $sql7     = "UPDATE LH_SERIES SET series_youtube = '" . $path_copy_vdo . "',series_youtube_thumbnail= '" . $path_copy_img . "',series_youtube_type = 'vdo',update_date = '$dates' WHERE series_id = '" . $id . "'";
                $query_r7 = mssql_query($sql7, $db_conn);
                $query    = mssql_query($sql);

            }
        }
    } else if ($_FILES["vdo"]["name"] != "" && $_FILES["thumbnail_vdo"]["name"] == "") {
        $sql2 = "SELECT series_youtube_thumbnail FROM dbo.LH_SERIES WHERE series_id = '" . $_POST['id'] . "' ";

        $query_result2        = mssql_query($sql2, $db_conn);
        $series_thumbnail_vdo = mssql_fetch_row($query_result2);
        $series_thumbnail_vdo[0];
        if ($series_thumbnail_vdo[0] == "") {
            $_SESSION['add'] = '-3';

            echo '<script>
                              window.history.go(-1);
                          </script>';
            exit();
        } else {
            $numrand_vdo = (mt_rand());

            $dates_file = date("Y-m-d");
            $path_img   = "fileupload/images/tvc_plan/";

            $type_vdo = strrchr($file_vdo, ".");

            $newname_vdo = $date . $numrand_vdo . "_vdo" . $type_vdo;

            $path_copy_vdo = $path_img . $newname_vdo;

            if (move_uploaded_file($_FILES['vdo']['tmp_name'], $path_copy_vdo)) {
                $sql7     = "UPDATE LH_SERIES SET series_youtube = '" . $path_copy_vdo . "' ,series_youtube_type = 'vdo',update_date = '$dates' WHERE series_id = '" . $id . "'";
                $query_r7 = mssql_query($sql7, $db_conn);
                $query    = mssql_query($sql);

            }
        }
    } else if ($_FILES["thumbnail_vdo"]["name"] != "" && $_FILES["vdo"]["name"] == "") {
        $sql2 = "SELECT series_youtube FROM dbo.LH_SERIES WHERE series_id = '" . $_POST['id'] . "' ";

        $query_result2 = mssql_query($sql2, $db_conn);
        $series_vdo    = mssql_fetch_row($query_result2);
        $series_vdo[0];
        if ($series_vdo[0] == "") {
            $_SESSION['add'] = '-3';

            echo '<script>
                              window.history.go(-1);
                          </script>';
            exit();
        } else {
            $path_copy_img = "fileupload/images/tvc_plan/" . $numrand_img3 . '_' . $_FILES["thumbnail_vdo"]["name"];
            if (move_uploaded_file($_FILES['thumbnail_vdo']['tmp_name'], $path_copy_img)) {
                $sql7     = "UPDATE LH_SERIES SET series_youtube_thumbnail= '" . $path_copy_img . "',series_youtube_type = 'vdo',update_date = '$dates' WHERE series_id = '" . $id . "'";
                $query_r7 = mssql_query($sql7, $db_conn);
                $query    = mssql_query($sql);

            }
        }
    } else if ($_FILES["thumbnail_vdo"]["name"] == "" && $_FILES["vdo"]["name"] == "") {
        $sql1 = "SELECT series_youtube_type FROM dbo.LH_SERIES WHERE series_id = '" . $_POST['id'] . "' ";

        $query_result2 = mssql_query($sql1, $db_conn);
        $series_chk    = mssql_fetch_row($query_result2);
        $series_chk[0];

        if ($series_chk[0] == "youtube") {

            $sql7     = "UPDATE LH_SERIES SET series_youtube = '',series_youtube_thumbnail= '',series_youtube_type = 'vdo',update_date = '$dates' WHERE series_id = '" . $id . "'";
            $query_r7 = mssql_query($sql7, $db_conn);
            $query    = mssql_query($sql);

            $_SESSION['add'] = '1';
            header("location:series_update.php?id=$id&add=1");

        } else if ($series_chk[0] != "youtube") {
            $sql2 = "SELECT series_youtube FROM dbo.LH_SERIES WHERE series_id = '" . $_POST['id'] . "' ";

            $query_result2 = mssql_query($sql2, $db_conn);
            $series_vdo    = mssql_fetch_row($query_result2);
            $series_vdo[0];
            if ($series_vdo[0] != "") {
                $sql2 = "SELECT series_youtube_thumbnail FROM dbo.LH_SERIES WHERE series_id = '" . $_POST['id'] . "' ";

                $query_result2            = mssql_query($sql2, $db_conn);
                $series_youtube_thumbnail = mssql_fetch_row($query_result2);
                $series_youtube_thumbnail[0];
                if ($series_youtube_thumbnail[0] != "") {
                    $_SESSION['add'] = '1';
                    header("location:series_update.php?id=$id&add=1");
                } else {
                    $_SESSION['add'] = '-3';

                    echo '<script>
                                  window.history.go(-1);
                              </script>';
                    exit();
                }
            }
        }
    }
}

if ($query_result2) {
    $_SESSION['add'] = '1';
    header("location:series_update.php?id=$id&add=1");
} else {
    $_SESSION['add'] = '-1';
    echo '<script>
                        window.history.go(-1)+"?add=-1";
              </script>';
}
