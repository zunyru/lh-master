<?php
session_start();
include './connect_db.php';
include './include/dbCon_mssql.php';
require_once './include/function_date.php';
$group_id = $_SESSION['group_id'];

$_GET['page'] = 'lead_page';
if (isset($_GET['add'])) {
    if($_GET['add'] ==1){
        //header("location:product_add.php");
        $success ="success";
    } else if($_GET['add'] ==0){

        $error="error"; //ค่าซ้ำ

    } else if($_GET['add'] ==-1){
        $error="errors";
    } else if($_GET['add'] ==-2){
        $delete="delete";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
            $("#alert").show();
            $("#alert").fadeTo(3000, 400).slideUp(500, function(){
                $("#alert").alert('close');
            });
        });
    </script>
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
                </div>

                <div class="clearfix"></div>
                <!-- menu profile quick info -->
                <?php include './master/navbar.php';?>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php include './master/top_nav.php'; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>ระบบจัดการข้อมูล Banner</h2>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                        <?php if(isset($success)){?>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="form-group">
                                            <div class="alert alert-success col-md-6" id="alert">
                                                <button type="button" class="close" data-dismiss="alert">x</button>
                                                <strong>สร้างข้อมูลเรียบร้อยแล้ว</strong>
                                            </div>
                                        </div>
                                        <div class="col-md-6"></div>
                                    </div>
                                <?php }else if(isset($error)){?>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="form-group">
                                            <div class="alert alert-danger col-md-6" id="alert">
                                                <button type="button" class="close" data-dismiss="alert">x</button>
                                                <strong>มีชื่อนี้อยู่ในระบบแล้ว</strong>
                                            </div>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                <?php }else if(isset($errors)){?>

                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="form-group">
                                            <div class="alert alert-danger col-md-6" id="alert">
                                                <button type="button" class="close" data-dismiss="alert">x</button>
                                                <strong>เกิดข้อผิดพลาด !</strong> ไม่สามารถบันทึกข้อมมูลได้
                                            </div>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                            <?php }else if(isset($delete)){?>
                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="form-group">
                                        <div class="alert alert-success col-md-6" id="alert">
                                            <button type="button" class="close" data-dismiss="alert">x</button>
                                            <strong>ลบข้อมูล!</strong> เรียบร้อย
                                        </div>
                                    </div>
                                    <div class="col-md-4"></div>
                                </div>
                            <?php }?>

                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#tab_content1" id="Highlights" role="tab" data-toggle="tab" aria-expanded="true">Banner</a>
                                    </li>
                                    <li role="presentation" class=""><a href="#tab_content2" role="tab" id="Project" data-toggle="tab" aria-expanded="false">Highlights</a>
                                    </li>

                                </ul>
                                <div id="myTabContent" class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="Highlights">
                                        <div class="form-group">
                                            <div class="col-md-10" >

                                            </div>
                                            <div class="col-md-2" >
                                                <a href="banner_add.php"><button type="button" class="btn btn-success flright">สร้างหน้า Banner +</button></a>
                                            </div>
                                        </div>

                                        <!-- <p class="text-muted font-13 m-b-30">
                                          DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>
                                        </p> -->
                                        <table id="datatable" class="table table-striped table-bordered">
                                            <thead>
                                            <tr>
                                                <th><center>ชื่อ Banner</center></th>
                                                <th><center>ประเภทชิ้นงาน</center></th>
                                                <th><center>หน้าที่แสดง</center></th>

                                            </tr>
                                            </thead>

                                            <tbody>
                                            <?php

                                                ?>
                                                <tr>
                                                    <td>
                                                       </td>
                                                    <td><center></center></td>
                                                    <td><center></center></td>

                                                </tr>

                                            <?php ?>

                                            </tbody>
                                        </table>
                                        <br><br>
                                    </div>

                                    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="Project">
                                        <div class="form-group">
                                            <div class="col-md-9" >

                                            </div>
                                            <div class="col-md-3" >
                                                <a href="highlights_add.php"><button type="button" class="btn btn-success" >สร้างข้อมูล Highlight +</button></a>
                                            </div>
                                        </div>
                                        <table id="Project_tb" class="table table-striped table-bordered">
                                            <thead>
                                            <tr>
                                                <th><center>ชื่อ Highlights</center></th>
                                                <th ><center>รายละเอียด</center></th>
                                                <th ><center>วันที่เริ่ม - สิ้นสุด </center></th>
                                                <th ><center>วันที่แก้ไขล่าสุด</center></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $sql_pr="SELECT * FROM LH_HIGHLIGHTS";
                                            $query_pr = mssql_query($sql_pr);
                                            while ( $row_pr = mssql_fetch_array($query_pr)) {

                                                $date=date_create($row_pr['highlights_update']);
                                                $strDate =date_format($date,"Y-m-d H:i:s");


                                                ?>
                                                <tr>
                                                    <td>
                                                        <a href="highlights_update.php?id=<?=$row_pr['highlights_id']?>">
                                                            <?php
                                                            echo $row_pr['highlights_name_th']; if($row_pr['highlights_name_en'] !=''){" (".$row_pr['highlights_name_en'].")";}
                                                            ?>
                                                        </a></td>
                                                    <td><a href="highlights_update.php?id=<?=$row_pr['highlights_id']?>">
                                                            <?php
                                                            $str = strlen($row_pr['highlights_dis_th']);
                                                            if($str >= 50){
                                                                echo iconv_substr($row_pr['highlights_dis_th'], 0,50, "UTF-8")."...";
                                                            }else{
                                                                echo $row_pr['highlights_dis_th'];
                                                            }
                                                            ?>
                                                        </a></td>
                                                    <td><a href="highlights_update.php?id=<?=$row_pr['highlights_id']?>"><?=DateThai($row_pr['start_date'])." - ".DateThai($row_pr['end_date']);?></a></td>
                                                    <td><p class="hidden"><?=$strDate?></p>
                                                        <center><a href="highlights_update.php?id=<?=$row_pr['highlights_id']?>"><?=DateThai_time($row_pr['highlights_update']);?></a></center></td>
                                                </tr>
                                            <?php  }  ?>
                                            </tbody>
                                        </table>
                                        <br><br>
                                    </div>


                                </div>






                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
<script src="../vendors/jszip/dist/jszip.min.js"></script>
<script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

<!-- Custom Theme Scripts -->
<script src="../build/js/custom.min.js"></script>

<!-- Datatables -->
<script>
    $(document).ready(function() {
        $('#datatable').dataTable({
            "bLengthChange": false,
            "pageLength": 50,
            "order": [[ 0, "DESC" ]]
        });


    });
</script>
<!-- /Datatables -->
    <script>
        $(document).ready(function() {

            $('#Project_tb').dataTable({
                "bLengthChange": false,
                "pageLength": 50,
                "order": [[ 3, "desc" ]]
            });
        });
    </script>

    <?php
    if(!empty($_GET['tab'])){
    if($_GET['tab']=="High"){
        ?>
        <script type="text/javascript">
            $('.nav-tabs a[href="#tab_content2"]').tab('show')
        </script>
        <?php
    }}
    ?>

</body>
</html>