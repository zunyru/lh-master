<?php
session_start();
include './include/dbCon_mssql.php';
date_default_timezone_set('Asia/Bangkok');
$date = date("Y-m-d");
$dates        = date("Y-m-d H:i:s");

$highlights_name_th    = mssql_escape($_POST['highlights_name_th']);
$highlights_name_en    = $_POST['highlights_name_en'];
$highlights_dis_th     = $_POST['highlights_dis_th'];
$highlights_dis_en     = $_POST['highlights_dis_en'];
$page                  = $_POST['page'];
$highlights_url        = $_POST['highlights_url'];
$highlights_content    = $_POST['highlights_content'];
$highlights_content_en = $_POST['highlights_content_en'];
$url_youtube_update    = $_POST['url_youtube_update'];
$url_youtube           = $_POST['url_youtube'];
$date_start = $_POST['date_start'];
$date_end = $_POST['date_end'];


 $year = substr($date_start, 6, 4);
 $mont = substr($date_start, 3, 2);
 $day = substr($date_start, 0, 2);
echo $date_start= $year."-".$mont."-".$day." 00:00:00.000";

$year2 = substr($date_end, 6, 4);
$mont2 = substr($date_end, 3, 2);
$day2 = substr($date_end, 0, 2);
echo $date_end= $year2."-".$mont2."-".$day2." 00:00:00.000";


$id = $_POST['id'];

function mssql_escape($str)
{
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return str_replace("'", "''", $str);
}
$today = date("H:i:s");

$query        = "SELECT COUNT(*) AS counts FROM LH_HIGHLIGHTS WHERE highlights_name_th = '$highlights_name_th' AND NOT highlights_id = '$id' ";
$query_result = mssql_query($query, $db_conn);
$count        = mssql_fetch_row($query_result);
$count[0];
if ($count[0] >= 1) {
    $_SESSION['add'] = '2';
    echo '<script>
                        window.history.go(-1);
                    </script>';
    exit();
}


if ($page == "image") {

     $query_2 = "UPDATE LH_HIGHLIGHTS SET highlights_name_th = '$highlights_name_th', "
    . "highlights_name_en ='$highlights_name_en',"
    . "highlight_link ='$highlights_url',"
    . "highlights_dis_th ='" . mssql_escape($highlights_dis_th) . "',"
    . "highlights_dis_en ='" . mssql_escape($highlights_dis_en) . "',"
    . "highlights_content ='" . mssql_escape($highlights_content) . "',"
    . "highlights_content_en ='" . mssql_escape($highlights_content_en) . "',"
    . "highlights_update = '$dates', "
        . "highlights_time_update = '$today',"
    . "highlights_lead_img_type = '$page'"
        ." ,start_date = '$date_start' ,end_date = '$date_end'"
    . "WHERE highlights_id = '$id'";

    $query_result2 = mssql_query($query_2, $db_conn);

    $numrand = (mt_rand());

    $images     = $_FILES['lead_img_file'];
    $file_image = $images['name'];

    $total = count($_FILES['lead_img_file']['name']);

// Loop through each file

    for ($i = 0; $i < $total; $i++) {

        if (!empty($_FILES['lead_img_file'])) {
            $highlights_lead_img_seo = $_POST['highlights_lead_img_seo'][0];
            $dates_file              = date("Y-m-d");
            $path_img                = "fileupload/highlights_file/";

            $type_img = strrchr($file_image[$i], ".");

            $newname_img   = $date . $i . $numrand . $type_img;
            $path_copy_img = $path_img . $newname_img;

            if (move_uploaded_file($_FILES['lead_img_file']['tmp_name'][$i], $path_copy_img)) {
                $sql_imag  = "SELECT highlights_lead_img FROM LH_HIGHLIGHTS WHERE highlights_id = '$id'";
                $query_img = mssql_query($sql_imag, $db_conn);
                $row       = mssql_fetch_array($query_img);
                unlink($path_img . $row['highlights_lead_img']);

               echo  $sql = "UPDATE LH_HIGHLIGHTS SET highlights_lead_img = '$path_copy_img' ,highlights_lead_img_seo = '$highlights_lead_img_seo',highlights_update = '$dates' "
                    ." ,start_date = '$date_start' ,end_date = '$date_end'"
                    . " WHERE highlights_id = '$id'";
                $query = mssql_query($sql, $db_conn);

                if ($query) {
                    $_SESSION['add'] = '-2';
                    //header("location:highlights_update.php?id=$id");

                } else {
                    $_SESSION['add'] = '-1';
                    //header("location:highlights_update.php?id=$id");
                }
            }
        }
    }
    if ($query_result2) {
        $_SESSION['add'] = '-2';
        header("location:highlights_update.php?id=$id");

    } else {
        $_SESSION['add'] = '-1';
        header("location:highlights_update.php?id=$id");
    }

} else if ($page == "youtube") {
    $url_youtube = $_POST['url_youtube'];

    if ($_FILES["img_youtube"]["name"] != "" && $_FILES["img_youtube"]["name"] != null && $url_youtube != "" && $url_youtube != null) {

        $query_2 = "UPDATE LH_HIGHLIGHTS SET highlights_name_th = '$highlights_name_th', "
        . "highlights_name_en ='$highlights_name_en',"
        . "highlight_link ='$highlights_url',"
        . "highlights_dis_th ='" . mssql_escape($highlights_dis_th) . "',"
        . "highlights_dis_en ='" . mssql_escape($highlights_dis_en) . "',"
        . "highlights_content ='" . mssql_escape($highlights_content) . "',"
        . "highlights_content_en ='" . mssql_escape($highlights_content_en) . "',"
        . "highlights_update = '$dates', "
        . "highlights_lead_img = '$url_youtube',"
        . "highlights_time_update = '$today',"
        . "highlights_lead_img_type = '$page'"
            ." ,start_date = '$date_start' ,end_date = '$date_end'"
        . "WHERE highlights_id = '$id'";

        $query_result2 = mssql_query($query_2, $db_conn);

        if (!empty($_FILES['img_youtube'])) {
            $highlights_thumbnail_seo = $_POST['highlights_thumbnail_seo'][0];
            $numrand                  = (mt_rand());

            $images     = $_FILES['img_youtube'];
            $file_image = $images['name'];

            $dates_file = date("Y-m-d");
            $path_img   = "fileupload/highlights_file/";

            $type_img = strrchr($file_image, ".");

            $newname_img   = $date . $numrand . $type_img;
            $path_copy_img = $path_img . $newname_img;

            if (move_uploaded_file($_FILES['img_youtube']['tmp_name'], $path_copy_img)) {
                //echo $newname_img;
                $sql_imag  = "SELECT highlight_thumbnail FROM LH_HIGHLIGHTS WHERE highlights_id = '$id'";
                $query_img = mssql_query($sql_imag, $db_conn);
                $row       = mssql_fetch_array($query_img);
                unlink($path_img . $row['highlight_thumbnail']);

                $sql = "UPDATE LH_HIGHLIGHTS SET highlight_thumbnail = '$path_copy_img',highlights_update = '$dates', "
                    . "highlights_lead_img = '$url_youtube',"
                    . "highlights_thumbnail_seo = '$highlights_thumbnail_seo'"
                    ." ,start_date = '$date_start' ,end_date = '$date_end'"
                    . " WHERE highlights_id = '$id'";
                $query = mssql_query($sql, $db_conn);

                if ($query) {
                    $_SESSION['add'] = '-2';
                    header("location:highlights_update.php?id=$id");
                } else {
                    $_SESSION['add'] = '-1';
                    echo '<script>
                                        window.history.go(-1);
                                    </script>';
                    exit();
                }
            }
        }
    } else if ($_FILES["img_youtube"]["name"] == "" && $_FILES["img_youtube"]["name"] == null && $url_youtube != "" && $url_youtube != null) {
        $sql2 = "SELECT highlight_thumbnail FROM dbo.LH_HIGHLIGHTS WHERE highlights_id = '$id' ";

        $query_result2       = mssql_query($sql2, $db_conn);
        $highlight_thumbnail = mssql_fetch_row($query_result2);
        $highlight_thumbnail[0];
        if ($highlight_thumbnail[0] == "") {
            $_SESSION['add'] = '-4';
            echo '<script>
                                        window.history.go(-1);
                                    </script>';
            exit();
        } else {
            $query_2 = "UPDATE LH_HIGHLIGHTS SET highlights_name_th = '$highlights_name_th', "
            . "highlights_name_en ='$highlights_name_en',"
            . "highlight_link ='$highlights_url',"
            . "highlights_dis_th ='" . mssql_escape($highlights_dis_th) . "',"
            . "highlights_dis_en ='" . mssql_escape($highlights_dis_en) . "',"
            . "highlights_content ='" . mssql_escape($highlights_content) . "',"
            . "highlights_content_en ='" . mssql_escape($highlights_content_en) . "',"
            . "highlights_update = '$dates', "
            . "highlights_lead_img = '$url_youtube',"
            . "highlights_time_update = '$today',"
            . "highlights_lead_img_type = '$page'"
                ." ,start_date = '$date_start' ,end_date = '$date_end'"
            . "WHERE highlights_id = '$id'";

            $query_result2 = mssql_query($query_2, $db_conn);
            if ($query_result2) {
                $_SESSION['add'] = '-2';
                header("location:highlights_update.php?id=$id");
            } else {
                $_SESSION['add'] = '-1';
                echo '<script>
                                            window.history.go(-1);
                                        </script>';
                exit();
            }

        }
    } else if ($_FILES["img_youtube"]["name"] != "" && $_FILES["img_youtube"]["name"] != null && $url_youtube == "" && $url_youtube == null) {
        $_SESSION['add'] = '-4';
        echo '<script>
                                        window.history.go(-1);
                                    </script>';
        exit();
    } else if ($_FILES["img_youtube"]["name"] == "" && $_FILES["img_youtube"]["name"] == null && $url_youtube == "" && $url_youtube == null) {
        $sql2 = "SELECT highlight_thumbnail FROM dbo.LH_HIGHLIGHTS WHERE highlights_id = '$id' ";

        $query_result2       = mssql_query($sql2, $db_conn);
        $highlight_thumbnail = mssql_fetch_row($query_result2);
        $highlight_thumbnail[0];
        if ($highlight_thumbnail[0] != "") {
            $_SESSION['add'] = '-4';
            echo '<script>
                                        window.history.go(-1);
                                    </script>';
            exit();
        } else {
            $query_2 = "UPDATE LH_HIGHLIGHTS SET highlights_name_th = '$highlights_name_th', "
            . "highlights_name_en ='$highlights_name_en',"
            . "highlight_link ='$highlights_url',"
            . "highlights_dis_th ='" . mssql_escape($highlights_dis_th) . "',"
            . "highlights_dis_en ='" . mssql_escape($highlights_dis_en) . "',"
            . "highlights_content ='" . mssql_escape($highlights_content) . "',"
            . "highlights_content_en ='" . mssql_escape($highlights_content_en) . "',"
            . "highlights_update = '$dates', "
            . "highlights_lead_img = '$url_youtube',"
            . "highlights_lead_img_type = '$page'"
                ." ,start_date = '$date_start' ,end_date = '$date_end'"
            . "WHERE highlights_id = '$id'";

            $query_result2 = mssql_query($query_2, $db_conn);

            if ($query_result2) {
                $_SESSION['add'] = '-2';
                header("location:highlights_update.php?id=$id");
            } else {
                $_SESSION['add'] = '-1';
                echo '<script>
                                        window.history.go(-1);
                                    </script>';
                exit();
            }
        }
    }

} elseif ($page == "vdo") {

    $query_2 = "UPDATE LH_HIGHLIGHTS SET highlights_name_th = '$highlights_name_th', "
    . "highlights_name_en ='$highlights_name_en',"
    . "highlight_link ='$highlights_url',"
    . "highlights_dis_th ='" . mssql_escape($highlights_dis_th) . "',"
    . "highlights_dis_en ='" . mssql_escape($highlights_dis_en) . "',"
    . "highlights_content ='" . mssql_escape($highlights_content) . "',"
    . "highlights_content_en ='" . mssql_escape($highlights_content_en) . "',"
    . "highlights_update = '$dates', "
    . "highlights_lead_img_type = '$page'"
        ." ,start_date = '$date_start' ,end_date = '$date_end'"
    . "WHERE highlights_id = '$id'";

    $query_result2                = mssql_query($query_2, $db_conn);
    $highlights_thumbnail_vdo_seo = $_POST['highlights_thumbnail_vdo_seo'][0];

    $numrand = (mt_rand());

    // <----case change type update---->
    $images = $_FILES['vdo_img'];
    $vdo    = $_FILES['vdo_file'];

    $file_image = $images['name'];
    $file_vdo   = $vdo['name'];

    // <----case change type update pic,vdo---->
    if ($_FILES["vdo_img"]["name"] != "" && $_FILES["vdo_img"]["name"] != null && $_FILES['vdo_file']['name'] != "" && $_FILES['vdo_file']['name'] != null) {
        // if ($_FILES['vdo_img']['name'] != "" && $_FILES['vdo_file']['name'] != "") {

        $dates_file = date("Y-m-d");
        $path_img   = "fileupload/highlights_file/";

        $type_img = strrchr($file_image, ".");
        $type_vdo = strrchr($file_vdo, ".");

        $newname_img = $date . $numrand . $type_img;
        $newname_vdo = $date . $numrand . "_vdo" . $type_vdo;

        $path_copy_img = $path_img . $newname_img;
        $path_copy_vdo = $path_img . $newname_vdo;

        if (move_uploaded_file($_FILES['vdo_img']['tmp_name'], $path_copy_img)) {
            if (move_uploaded_file($_FILES['vdo_file']['tmp_name'], $path_copy_vdo)) {

                $sql_imag  = "SELECT highlight_thumbnail FROM LH_HIGHLIGHTS WHERE highlights_id = '$id'";
                $query_img = mssql_query($sql_imag, $db_conn);
                $row       = mssql_fetch_array($query_img);
                unlink($path_img . $row['highlight_thumbnail']);

                $sql_imag  = "SELECT highlights_lead_img FROM LH_HIGHLIGHTS WHERE highlights_id = '$id'";
                $query_img = mssql_query($sql_imag, $db_conn);
                $row       = mssql_fetch_array($query_img);
                unlink($path_img . $row['highlights_lead_img']);

                $sql = "UPDATE LH_HIGHLIGHTS SET highlight_thumbnail = '$path_copy_img',highlights_update = '$dates', "
                    . "highlights_lead_img = '$path_copy_vdo',"
                    . "highlights_thumbnail_seo = '$highlights_thumbnail_vdo_seo'"
                    ." ,start_date = '$date_start' ,end_date = '$date_end'"
                    . " WHERE highlights_id = '$id'";
                $query = mssql_query($sql, $db_conn);

                if ($query) {
                    $_SESSION['add'] = '-2';
                    header("location:highlights_update.php?id=$id");
                } else {
                    $_SESSION['add'] = '-1';
                    echo '<script>
                                        window.history.go(-1);
                                    </script>';
                    exit();
                }
            }
        }

    } else if ($_FILES["vdo_img"]["name"] == "" && $_FILES["vdo_img"]["name"] == null && $_FILES['vdo_file']['name'] != "" && $_FILES['vdo_file']['name'] != null) {
        // } else if ($_FILES['vdo_img_up']['name'] != "" && $_FILES['vdo_file_up']['name'] != "") {
        $sql2 = "SELECT highlight_thumbnail FROM dbo.LH_HIGHLIGHTS WHERE highlights_id = '$id' ";

        $query_result2       = mssql_query($sql2, $db_conn);
        $highlight_thumbnail = mssql_fetch_row($query_result2);
        $highlight_thumbnail[0];
        if ($highlight_thumbnail[0] == "") {
            $_SESSION['add'] = '-3';
            echo '<script>
                                        window.history.go(-1);
                                    </script>';
            exit();
        } else {
            $dates_file = date("Y-m-d");
            $path_img   = "fileupload/highlights_file/";

            $type_vdo = strrchr($file_vdo, ".");

            $newname_vdo = $date . $numrand . "_vdo" . $type_vdo;

            $path_copy_vdo = $path_img . $newname_vdo;

            if (move_uploaded_file($_FILES['vdo_file']['tmp_name'], $path_copy_vdo)) {

                $sql_imag  = "SELECT highlights_lead_img FROM LH_HIGHLIGHTS WHERE highlights_id = '$id'";
                $query_img = mssql_query($sql_imag, $db_conn);
                $row       = mssql_fetch_array($query_img);
                unlink($path_img . $row['highlights_lead_img']);

                $sql = "UPDATE LH_HIGHLIGHTS SET highlights_lead_img = '$path_copy_vdo',highlights_update = '$dates'"
                    ." ,start_date = '$date_start' ,end_date = '$date_end'"
                    . " WHERE highlights_id = '$id'";
                $query = mssql_query($sql, $db_conn);

                if ($query) {
                    $_SESSION['add'] = '-2';
                    header("location:highlights_update.php?id=$id");
                } else {
                    $_SESSION['add'] = '-1';
                    header("location:highlights_update.php?id=$id");
                }
            }
        }
    } else if ($_FILES["vdo_img"]["name"] != "" && $_FILES["vdo_img"]["name"] != null && $_FILES['vdo_file']['name'] == "" && $_FILES['vdo_file']['name'] == null) {
        // } else if ($_FILES['vdo_img_up']['name'] == "" && $_FILES['vdo_file_up']['name'] != "") {
        $sql2 = "SELECT highlights_lead_img FROM dbo.LH_HIGHLIGHTS WHERE highlights_id = '$id' ";

        $query_result2       = mssql_query($sql2, $db_conn);
        $highlights_lead_img = mssql_fetch_row($query_result2);
        $highlights_lead_img[0];
        if ($highlights_lead_img[0] == "") {
            $_SESSION['add'] = '-3';
            echo '<script>
                                        window.history.go(-1);
                                    </script>';
            exit();
        } else {
            $dates_file = date("Y-m-d");
            $path_img   = "fileupload/highlights_file/";

            $type_img = strrchr($file_image, ".");

            $newname_img = $date . $numrand . $type_img;

            $path_copy_img = $path_img . $newname_img;

            if (move_uploaded_file($_FILES['vdo_img']['tmp_name'], $path_copy_img)) {

                $sql_imag  = "SELECT highlight_thumbnail FROM LH_HIGHLIGHTS WHERE highlights_id = '$id'";
                $query_img = mssql_query($sql_imag, $db_conn);
                $row       = mssql_fetch_array($query_img);
                unlink($path_img . $row['highlight_thumbnail']);

                 $sql = "UPDATE LH_HIGHLIGHTS SET highlight_thumbnail = '$path_copy_img', highlights_thumbnail_seo = '$highlights_thumbnail_vdo_seo',highlights_update = '$dates' "
                    ." ,start_date = '$date_start' ,end_date = '$date_end'"
                    ." WHERE highlights_id = '$id'";
                $query    = mssql_query($sql, $db_conn);

                if ($query) {
                    $_SESSION['add'] = '-2';
                    header("location:highlights_update.php?id=$id");
                } else {
                    $_SESSION['add'] = '-1';
                    header("location:highlights_update.php?id=$id");
                }
            }
        }
    } else if ($_FILES["vdo_img"]["name"] == "" && $_FILES["vdo_img"]["name"] == null && $_FILES['vdo_file']['name'] == "" && $_FILES['vdo_file']['name'] == null) {
        $sql2 = "SELECT highlights_lead_img FROM dbo.LH_HIGHLIGHTS WHERE highlights_id = '$id' ";

        $query_result2       = mssql_query($sql2, $db_conn);
        $highlights_lead_img = mssql_fetch_row($query_result2);
        $highlights_lead_img[0];
        if ($highlights_lead_img[0] == "") {
            $_SESSION['add'] = '-3';
            echo '<script>
                                        window.history.go(-1);
                                    </script>';
            exit();
        } else {
            $sql2 = "SELECT highlight_thumbnail FROM dbo.LH_HIGHLIGHTS WHERE highlights_id = '$id' ";

            $query_result2       = mssql_query($sql2, $db_conn);
            $highlight_thumbnail = mssql_fetch_row($query_result2);
            $highlight_thumbnail[0];
            if ($highlight_thumbnail[0] == "") {
                $_SESSION['add'] = '-3';
                echo '<script>
                                            window.history.go(-1);
                                        </script>';
                exit();
            }
        }
    }
    if ($query_result2) {
        $_SESSION['add'] = '-2';
        header("location:highlights_update.php?id=$id");
    } else {
        $_SESSION['add'] = '-1';
        header("location:highlights_update.php?id=$id");
    }
}
