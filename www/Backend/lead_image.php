<?php
session_start();
include './connect_db.php';
include './include/dbCon_mssql.php';
$group_id = $_SESSION['group_id'];

$_GET['page'] = 'lead_page';

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
     <script>
      $(document).ready(function(){
        $("#alert").show();
            $("#alert").fadeTo(3000, 400).slideUp(500, function(){
              $("#alert").alert('close');
          });
      });
      </script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

             <div class="clearfix"></div>
           <!-- menu profile quick info -->
           <?php include './master/navbar.php';?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
          <?php include './master/top_nav.php'; ?>
        <!-- /top navigation -->
       
        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>ระบบจัดการข้อมูล Banner</h2>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     <?php if(isset($_GET['delete'])){?>
                      <div class="row">
                              <div class="col-md-2"></div>
                               <div class="form-group">
                                <div class="alert alert-success col-md-6" id="alert">
                                  <button type="button" class="close" data-dismiss="alert">x</button>
                                  <strong>ลบข้อมูล!</strong> เรียบร้อย  
                                </div>
                              </div>
                              <div class="col-md-4"></div> 
                             </div> 
                  <?php }?>
                    <a href="lead_image_add.php"><button type="button" class="btn btn-success flright">สร้างหน้า Leade Image +</button></a>
                    <!-- <p class="text-muted font-13 m-b-30">
                      DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>
                    </p> -->
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th><center>ชื่อชิ้นงาน</center></th>
                          <th><center>ประเภทชิ้นงาน</center></th>
                          <th><center>หน้าที่แสดง</center></th>
                          <th><center>เริ่ม – สิ้นสุด</center></th>
                        </tr>
                      </thead>
                     
                      <tbody>
                       <?php 
                        include "include/function_date.php";
                        $sql="SELECT * FROM LH_LEAD_IMAGE lead ";
                        $query= mssql_query($sql);
                        while ($row = mssql_fetch_array($query)) {

                           if(empty($row['leade_imd_date_start']) || empty($row['leade_imd_date_end'])){
                            $date = "ไม่มีกำหนดเวลา";
                           }else{
                            $date = DateThai($row['leade_imd_date_start'])." - ".DateThai($row['leade_imd_date_end']); 
                          }

                          if($row['page_review']=="page"){
                            $page=$row['page_project_id'];
                          }else {
                            $sql2 ="SELECT project_name_th , project_name_en FROM LH_PROJECTS WHERE project_id = '".$row['page_project_id']."'" ;
                            $query2=mssql_query($sql2);
                            $result=mssql_fetch_array($query2);
                            $page=$result['project_name_th']." (".$result['project_name_th'].")";
                          }

                      ?>
                        <tr>
                          <td><a href="lead_image_update.php?id=<?=$row['lead_img_zero_id']?>"><?=$row['lead_img_zero_name']?></a></td>
                          <td><center><a href="lead_image_update.php?id=<?=$row['lead_img_zero_id']?>"><?=$row['lead_img_by_type']?></a></center></td>
                          <td><center><a href="lead_image_update.php?id=<?=$row['lead_img_zero_id']?>"><?=$page;?></a></center></td>
                          <td><a href="lead_image_update.php?id=<?=$row['lead_img_zero_id']?>"><?=$date?></a></td>
                        </tr>
                      
                        <?php } ?>
                       
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

    <!-- Datatables -->
    <script>
      $(document).ready(function() {
        

        $('#datatable').dataTable({
          "bLengthChange": false,
          "pageLength": 50,
          "order": [[ 0, "asc" ]]
        });


      });
    </script>
    <!-- /Datatables -->
  </body>
</html>