<?php
session_start();
include './include/dbCon_mssql.php';
$group_id = $_SESSION['group_id'];

$_GET['page']='footer';


if (isset($_SESSION['add'])) {
  if($_SESSION['add'] ==1){
        //header("location:product_add.php");
    $success ="success";
    unset($_SESSION["add"]);
  } else if($_SESSION['add'] ==-1){

        $error_name="error"; //ค่าซ้ำ
        $_SESSION['add']='';

      }else if($_SESSION['add'] ==-2){

        $error_url="error"; //ค่าซ้ำ
        $_SESSION['add']='';

      } 
    }

    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <!-- Meta, title, CSS, favicons, etc. -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>LAND & HOUSES</title>

      <!-- Bootstrap -->
      <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome -->
      <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <!-- NProgress -->
      <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
      <!-- iCheck -->
      <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
      <!-- Custom Theme Style -->
      <link href="../build/css/custom.min.css" rel="stylesheet">
      <!-- Fancybox image popup -->
      <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
      <!-- bootstrap-progressbar -->
      <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
      <!-- jQuery -->
      <script src="../vendors/jquery/dist/jquery.min.js"></script>

      <!-- uploade img -->
      <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
      <!--uploade-->
      <!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
      <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
      <!-- confirm-->
      <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
      <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>

      <!-- Include Editor style. -->
      <link href="editor/css/froala_editor.min.css" rel="stylesheet" type="text/css" />
      <link href="editor/css/froala_style.min.css" rel="stylesheet" type="text/css" />

      <!--  froala  -->
      <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" href="libs/codemirror/5.3.0/codemirror.min.css">
      <link href="libs/froala-editor/2.6.2/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
      <link href="libs/froala-editor/2.6.2/css/froala_style.min.css" rel="stylesheet" type="text/css" />

      <!-- Include Editor Plugins style. -->
      <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/char_counter.css">
      <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/code_view.css">
      <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/colors.css">
      <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/emoticons.css">
      <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/file.css">
      <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/fullscreen.css">
      <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/image.css">
      <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/image_manager.css">
      <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/line_breaker.css">
      <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/quick_insert.css">
      <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/table.css">
      <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/video.css">

      <script id="fr-fek">try{(function (k){localStorage.FEK=k;t=document.getElementById('fr-fek');t.parentNode.removeChild(t);})('inkH3fiA6B-13a==')}catch(e){}</script>

      <style type="text/css">
      .fr-toolbar.fr-desktop.fr-top.fr-basic.fr-sticky.fr-sticky-off {
        background: #f7f7f7  !important;
        border-top: 5px solid #f5f5f5  !important;
      }
      th.next.available {
        background: #6f9755;
      }
      th.next.available:hover {
        background: #9ab688;
      }
      th.prev.available {
        background: #6f9755;
      }
      th.prev.available:hover {
        background: #9ab688;
      }
      .thumbnail {
        height: 100px;
        margin: 5px;
      }
      .fileUpload {
        position: relative;
        overflow: hidden;
        //margin: 10px;
      }
      .fileUpload input.upload {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
      }
      .file-drop-zone-title {
        padding: 0px 0px !important;
      }
      .form-control[readonly] { /* For Firefox */
        background-color: white;
      }

      .form-control[readonly] {
        background-color: white;
      }


    </style>
    <style>
    .file-caption-main .btn-file {
      overflow: visible;
    }

    .file-caption-main .btn-file .error {
      position: absolute;
      bottom: -32px;
      right: 30px;
    }
  </style>

</head>

<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;">
            <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
          </div>

          <div class="clearfix"></div>

          <!-- menu profile quick info -->
          <?php include './master/navbar.php';?>
          <!-- /menu footer buttons -->
        </div>
      </div>

      <!-- top navigation -->
      <?php include './master/top_nav.php'; ?>
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><a href="footer_func.php">ระบบจัดการข้อมูล Footer</a> > <a href="footer_add.php">สร้าง Footer</a></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                 <?php if(isset($success)){?>
                 <div class="row">
                  <div class="col-md-2"></div>
                  <div class="form-group">
                    <div class="alert alert-success col-md-6" id="alert">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      <strong>สร้างข้อมูลเรียบร้อยแล้ว</strong>
                    </div>
                  </div>
                  <div class="col-md-6"></div>
                </div>
                <?php }else if(isset($error_name)){?>
                <div class="row">
                  <div class="col-md-2"></div>
                  <div class="form-group">
                    <div class="alert alert-danger col-md-6" id="alert">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      <strong>มีชื่อนี้อยู่ในระบบแล้ว </strong>
                    </div>
                  </div>
                  <div class="col-md-4"></div>
                </div>
                <?php }else if(isset($deleted)){?>

                <div class="row">
                  <div class="col-md-2"></div>
                  <div class="form-group">
                    <div class="alert alert-danger col-md-6" id="alert">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      <strong>ลบข้อมูลเรียบร้อยแล้ว</strong>
                    </div>
                  </div>
                  <div class="col-md-4"></div>
                </div>
                 <?php }else if(isset($error_url)){?>

                <div class="row">
                  <div class="col-md-2"></div>
                  <div class="form-group">
                    <div class="alert alert-danger col-md-6" id="alert">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      <strong>มี URL นี้ในระบบแล้ว</strong>
                    </div>
                  </div>
                  <div class="col-md-4"></div>
                </div>
                <?php }?>

                <p class="font-gray-dark">
                </p><br>

                <form class="form-horizontal form-label-left" action="save_footer.php" method="POST"  enctype="multipart/form-data" name="myForm" id="commentForm">
                  <div class="form-group">
                    <label class="control-label col-md-2" for="first-name">Name
                      <span class="required">*</span>
                    </label>
                    <div class="col-md-6">
                      <input type="text" id="first-name1" required class="form-control col-md-7 col-xs-12" value="" name="name" placeholder="Name">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-2" for="first-name">URL<span class="required">*</span>
                    </label>
                    <div class="col-md-6">
                      <input type="url"   class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อ URL" name="url_page" value="">
                    </div>
                  </div>

                  <?php 
                  $sql="SELECT * FROM LH_FOOTERS WHERE footer_id = 1";
                  $query = mssql_query($sql);
                  $row = mssql_fetch_array($query);
                  ?>

                  <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12">แก้ไข footer (สีเขียวของลิงค์จะแสดงที่หน้าเว็บอัตโนมัติ)</label>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <br>

                     <textarea name="footer" id="edit" rows="15"><?=$row['content']?></textarea>

                   </div>
                 </div>

                 <br> 
                 <div class="form-group">
                   <label class="control-label col-md-3" for="first-name">
                   </label>
                   <div class="col-md-6">
                    <input type='hidden' name='pic_err' value='1'>
                    <button type="submit" class="btn btn-success">Submit</button>
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>




  <!--Editor_script-->

  <!-- Include JS files. -->
  <script type="text/javascript" src="libs/codemirror/5.3.0/codemirror.min.js"></script>
  <script type="text/javascript" src="libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/froala_editor.pkgd.min.js"></script>

  <!-- Include Plugins. -->
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/align.min.js"></script>
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/char_counter.min.js"></script>
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/code_beautifier.min.js"></script>
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/code_view.min.js"></script>
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/colors.min.js"></script>
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/emoticons.min.js"></script>
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/entities.min.js"></script>
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/file.min.js"></script>
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/font_family.min.js"></script>
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/font_size.min.js"></script>
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/fullscreen.min.js"></script>
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/image.min.js"></script>
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/image_manager.min.js"></script>
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/inline_style.min.js"></script>
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/line_breaker.min.js"></script>
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/link.min.js"></script>
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/lists.min.js"></script>
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/paragraph_format.min.js"></script>
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/paragraph_style.min.js"></script>
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/quick_insert.min.js"></script>
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/quote.min.js"></script>
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/table.min.js"></script>
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/save.min.js"></script>
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/url.min.js"></script>
  <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/video.min.js"></script>
  <!-- Bootstrap -->
  <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- FastClick -->
  <script src="../vendors/fastclick/lib/fastclick.js"></script>
  <!-- NProgress -->
  <script src="../vendors/nprogress/nprogress.js"></script>
  <!-- iCheck -->
  <script src="../vendors/iCheck/icheck.min.js"></script>
  <script type="text/javascript" src="js/bootstrap-imgupload.js"></script>
  <!-- Validate -->
  <script src="../build/js/jquery.validate.js"></script>

  <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>

  <!-- bootstrap-progressbar -->
  <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
  <!-- Custom Theme Scripts -->
  <script src="../build/js/custom.min.js"></script>


  <!-- date -->
  <link rel="stylesheet" type="text/css" href="../build/css/jquery-ui.css"/>
  <script src="../build/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

  <!-- Validate -->
  <script src="../build/js/jquery.validate.js"></script>
  <script>
   $(document).ready(function() {


    $.validator.setDefaults({ ignore: "[contenteditable='true']" });
    $("#commentForm").validate({
      rules: {
        name: "required",
        url_page : {required : true,url: true},
        footer : "required",
      },
      messages: {
        name: "กรุณากรอก Name",
        url_page : {required : "กรุณากรอก Url", url : "Url ไม่ถูกต้อง"},
        footer : "กรุณาเพิ่ม Footer",
      }
    });


  });
</script>

<!-- Include Language file if we want to u-->
<script>
  $(function() {
    $('#edit').froalaEditor({

      height: 500,
      imageUploadURL: 'uploade_highlights.php',
      imageUploadParams: {
        id: 'edit'
      },
      imageManagerLoadURL: 'images_load.php',
      // Set max image size to 300kB.
      imageMaxSize: 0.3 * 1024 * 1024,

      fileUploadURL: 'upload_file.php',
      fileUploadParams: {
        id: 'edit'
      },
      videoUploadURL: 'upload_video.php',
      videoUploadParams: {
        id: 'edit'
      },
        fontSize: ["8", "9", "10", "11", "12", "14", "18","20","22","24", "30", "36", "48", "60", "72", "96"],
      fontFamily: {
        "LHfont": 'Kittithada',
        "Roboto,sans-serif": 'Roboto',
        "Oswald,sans-serif": 'Oswald',
        "Montserrat,sans-serif": 'Montserrat',
        "'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
      },

      imageManagerDeleteURL: "delete_image_highlights.php",
      imageManagerDeleteMethod: "POST"
    })

    .on('froalaEditor.image.removed', function (e, editor, $img) {
      $.ajax({

        method: "POST",


        url: "delete_image_highlights.php",

        data: {
          src: $img.attr('src')
        }
      })
      .done (function (data) {
        console.log ('image was deleted'+$img.attr('src'));
      })
      .fail (function (err) {
        console.log ('image delete problem: ' + JSON.stringify(err));
      })
    })
    .on('froalaEditor.file.unlink', function (e, editor, link) {

      $.ajax({
        method: "POST",

        url: "delete_file.php",

        data: {
          src: link.getAttribute('href')
        }
      })
      .done (function (data) {
        console.log ('file was deleted');
      })
      .fail (function (err) {
        console.log ('file delete problem: ' + JSON.stringify(err));
      })
    })
  });

  
</script>

<script>
  $(document).ready(function(){
    $("#alert").show();
    $("#alert").fadeTo(3000, 500).slideUp(500, function(){
      $("#alert").alert('close');
    });
  });
</script>
<script src="js/validate_file_300kb.js"></script>


</body>
</html>