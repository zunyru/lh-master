<?php
session_start();
include './include/dbCon_mssql.php';



date_default_timezone_set('Asia/Bangkok');
$date = date("Ymd");
$year=date("m");  
$numrand_1 = (mt_rand());
$numrand_2 = (mt_rand());
$numrand_3 = (mt_rand());
$numrand_galery = (mt_rand());

	function mssql_escape($str)
	{
		if(get_magic_quotes_gpc())
	    {
	        $str= stripslashes($str);
	    }
	    return str_replace("'", "''", $str);
	}

  $series_id=$_POST['series'];
  $plan_code=$_POST['plan_code'];
  $plan_name_th=$_POST['plan_name_th'];
  $plan_name_en=$_POST['plan_name_en'];
  $plan_useful=$_POST['plan_useful'];
  $plan_feature_th=$_POST['plan_feature_th'];
  $plan_feature_en=$_POST['plan_feature_en'];
  $plan_land_size = $_POST['plan_land_size'];

  $message1_th=$_POST['message1_th'];
  $message1_en=$_POST['message1_en'];

  $message2_th=$_POST['message2_th'];
  $message2_th=$_POST['message2_th'];

  $message3_th=$_POST['message3_th'];
  $message3_en=$_POST['message3_en'];

   $c_360=$_POST['c_360'];
   $c_360_img = $_POST['c_360_img'];

  $youtube=$_POST['youtube'];

  $product_id=$_POST['product_id'];

  $optradio = $_POST['optradio'];

//เช็คชื่อซ้ำ
$sql="SELECT COUNT(*) AS counts FROM LH_PLANS WHERE plan_name_th = '$plan_name_th' OR  plan_name_en = '$plan_name_en'";
$query_result2= mssql_query($sql , $db_conn);
$count = mssql_fetch_array($query_result2);
 $count['counts'];
if($count['counts'] >= 1){
    //มีชื่อ series นี้อยู่แล้ว
    $_SESSION['add']='0';
    echo '<script>window.history.go(-1);</script>';
    exit();
}

 $sql="INSERT INTO LH_PLANS (series_id,plan_name_th,plan_name_en,plan_useful,plan_feature_th,plan_feature_en,plan_code,plan_land_size,product_id) "
    ." VALUES ('$series_id','$plan_name_th','$plan_name_en','$plan_useful','$plan_feature_th','$plan_feature_en','$plan_code','$plan_land_size','$product_id')";
$query_result2= mssql_query($sql , $db_conn);

$query_3 ="SELECT MAX(plan_id) AS max_id FROM dbo.LH_PLANS;";
$query_result3= mssql_query($query_3 , $db_conn);
$row = mssql_fetch_array($query_result3);

$id_plans=$row['max_id'];

//เพิ่มฟังก์ชัน
if($_POST['name_f']<>''){
    foreach ($_POST['name_f'] as $key_n => $fucn_n) {

        foreach ($_POST['fucn'] as $key=> $fucn) {
            if($key == $key_n){
                if($fucn != ''){
                    //  		echo  $fucn_n."ขื่อ <br>";
                    // echo  $fucn." ค่า <br>";

                    $sql4="INSERT INTO LH_FUNCTION_PLAN_SUB (plan_id,function_plan_plan_id,function_plan_sub_plan_count_room) "
                        ." VALUES ('$id_plans','$fucn_n','$fucn')";
                    $query_r4= mssql_query($sql4 , $db_conn);
                }

            }

        }
    }
}//

//save LH_GALERY_PLAN

$numrand_img= (mt_rand());
$images_plan = $_FILES['images_plan'];
$file_image = $images_plan['name'];

if (!empty($_FILES['images_plan'])) {
    for($i=0; $i < count($file_image); $i++){
        $dates_file =date("Y-m-d");
        $path_img="fileupload/images/galery_plan/";

        $type_img = strrchr($file_image[$i],".");

        $newname_img = $date.$numrand_img.$type_img;
        $path_copy_img=$path_img.$newname_img;
        $seo_img = $_POST['seo_img_paln'][$i];

        if(move_uploaded_file($_FILES['images_plan']['tmp_name'][$i],$path_copy_img))
        {
             $sql6="INSERT INTO LH_GALERY_PLAN (plan_id,galery_plan_name,galery_plan_date,galery_plan_img_seo) "
                ." VALUES ('$id_plans','$newname_img','$dates_file','$seo_img')";
            $query_r6= mssql_query($sql6 , $db_conn);
            $success_img = true;
        }else {
            $success = false;
            break;
        }
    }
}

//Floor Plan ชั้น 1
$numrand_img_plan1= (mt_rand());
$images_plan_1 = $_FILES['images_plan_1'];
$file_image_paln1 = $images_plan_1['name'];

if (!empty($_FILES['images_plan_1'])) {
    for($i=0; $i < count($file_image_paln1); $i++){
        $dates_file =date("Y-m-d");
        $path_img="fileupload/images/floor_plan_master/";

        $type_img = strrchr($file_image_paln1[$i],".");

        $newname_img_plan1 = $date.$numrand_img_plan1.$type_img;
        $path_copy_img=$path_img.$newname_img_plan1;
        $seo_img_plan1 = $_POST['seo_img_floor_paln1'][$i];

        if(move_uploaded_file($_FILES['images_plan_1']['tmp_name'][$i],$path_copy_img))
        {


           echo $sql8="INSERT INTO LH_GALERY_FLOOR_PLAN_IMG (plan_id,floor_plan_img_name,floor_plan_dis_th,floor_plan_dis_en,floor_plan_img_seo,number_floor_plan) "
                ." VALUES ('$id_plans','$newname_img_plan1','".mssql_escape($message1_th)."','".mssql_escape($message1_en)."','$seo_img_plan1','1')";
            $query_r8= mssql_query($sql8 , $db_conn);
            $success_img = true;
        }else {
            $success = false;
            break;
        }
    }
}

//Floor Plan ชั้น 2
$numrand_img_plan2= (mt_rand());
$images_plan_2 = $_FILES['images_plan_2'];
$file_image_paln2 = $images_plan_2['name'];

if (!empty($_FILES['images_plan_2'])) {
    for($i=0; $i < count($file_image_paln2); $i++){
        $dates_file =date("Y-m-d");
        $path_img="fileupload/images/floor_plan_master/";

        $type_img = strrchr($file_image_paln2[$i],".");

        $newname_img_plan2 = $date.$numrand_img_plan2.$type_img;
        $path_copy_img=$path_img.$newname_img_plan2;
        $seo_img_plan2 = $_POST['seo_img_floor_paln2'][$i];

        if(move_uploaded_file($_FILES['images_plan_2']['tmp_name'][$i],$path_copy_img))
        {
             $sql7="INSERT INTO LH_GALERY_FLOOR_PLAN_IMG (plan_id,floor_plan_img_name,floor_plan_dis_th,floor_plan_dis_en,floor_plan_img_seo,number_floor_plan) "
                ." VALUES ('$id_plans','$newname_img_plan2','".mssql_escape($message2_th)."','".mssql_escape($message2_en)."','$seo_img_plan2','2')";
            $query_r7= mssql_query($sql7 , $db_conn);
            $success_img = true;
        }else {
            $success = false;
            break;
        }
    }
}

//Floor Plan ชั้น 3
$numrand_img_plan3= (mt_rand());
$images_plan_3 = $_FILES['images_plan_3'];
$file_image_paln3 = $images_plan_3['name'];

if (!empty($_FILES['images_plan_3'])) {
    for($i=0; $i < count($file_image_paln3); $i++){
        $dates_file =date("Y-m-d");
        $path_img="fileupload/images/floor_plan_master/";

        $type_img = strrchr($file_image_paln3[$i],".");

        $newname_img_plan3 = $date.$numrand_img_plan3.$type_img;
        $path_copy_img=$path_img.$newname_img_plan3;
        $seo_img_plan3 = $_POST['seo_img_floor_paln3'][$i];

        if(move_uploaded_file($_FILES['images_plan_3']['tmp_name'][$i],$path_copy_img))
        {
           $sql7="INSERT INTO LH_GALERY_FLOOR_PLAN_IMG (plan_id,floor_plan_img_name,floor_plan_dis_th,floor_plan_dis_en,floor_plan_img_seo,number_floor_plan) "
                ." VALUES ('$id_plans','$newname_img_plan3','".mssql_escape($message3_th)."','".mssql_escape($message2_en)."','$seo_img_plan3','3')";
            $query_r7= mssql_query($sql7 , $db_conn);
            $success = true;
        }else {
            $success = false;
            break;
        }
    }
}


$numrand_img= (mt_rand());
$images_plan_360 = $_FILES['c_360_img'];
$file_image_paln360 = $images_plan_360['name'];

if (!empty($_FILES['c_360_img'])) {
    for($i=0; $i < count($file_image_paln360); $i++){
        $dates_file =date("Y-m-d");
        $path_img="fileupload/images/360_thumnail/";

        $type_img = strrchr($file_image_paln360[$i],".");

        $newname_img = $date.$numrand_img.$type_img;
        $path_copy_img=$path_img.$newname_img;


        if(move_uploaded_file($_FILES['c_360_img']['tmp_name'][$i],$path_copy_img))
        {
            echo $sql7="INSERT INTO LH_360_FOR_PLAN (plan_id,c360_plan_name,c360_plan_url,c360_img) "
                ." VALUES ('$id_plans','$plan_name_th','$c_360','$path_copy_img')";
            $query_r7= mssql_query($sql7 , $db_conn);
            $success_img = true;
        }else {
            $success = false;
            break;
        }
    }
}




$page = $_POST['optradio'];

if ($page  ==  "vdo") {

    $numrand = (mt_rand());
    $images = $_FILES['thumbnail_vdo'];
    $vdo = $_FILES['vdo_file'];
    $date_ = date("Ymd");

    //print_r($vdo );
    $file_image = $images['name'];
    $file_vdo = $vdo['name'];

    $total = count($_FILES['vdo_file']['name']);

    //for($i=0; $i<$total; $i++) {
    if (!empty($_FILES['vdo_file'])) {
        $dates_file = date("Y-m-d");
        $path_img = "fileupload/images/tvc/";

        $type_img = strrchr($file_image, ".");
        $type_vdo = strrchr($file_vdo, ".");

        $newname_img = $date_ . $numrand . $type_img;
        $newname_vdo = $date_ . $numrand . "_vdo" . $type_vdo;

         $path_copy_img = $path_img . $newname_img;
         $path_copy_vdo = $path_img . $newname_vdo;

        if (move_uploaded_file($_FILES['thumbnail_vdo']['tmp_name'], $path_copy_img)) {
            if (move_uploaded_file($_FILES['vdo_file']['tmp_name'], $path_copy_vdo)) {
                //echo $newname_img;
                echo $sql="INSERT INTO LH_CLIP_FOR_PLAN (plan_id,clip_plan_name,clip_plan_url,thumnail,clip_type) "
                    ." VALUES ('$id_plans','$plan_name_th','".$path_copy_vdo."','$path_copy_img','$page')";
                $query=mssql_query($sql);

            }
        }
        // }

    }
}else if($page  ==  "youtube"){
    $url_youtube = $_POST['url_youtube'];

    $numrand= (mt_rand());
    $images = $_FILES['img_youtube'];
    $file_image = $images['name'];

    $total = count($_FILES['img_youtube']['name']);

// Loop through each file
    //    for($i=0; $i<$total; $i++) {
    if (!empty($_FILES['img_youtube'])) {
        $dates_file = date("Y-m-d");
        $date_ = date("Ymd");
        $path_img = "fileupload/images/tvc_plan/";

        $type_img = strrchr($file_image, ".");

        $newname_img = $date_.  $numrand . $type_img;
        $path_copy_img = $path_img . $newname_img;

        if (move_uploaded_file($_FILES['img_youtube']['tmp_name'], $path_copy_img)) {
            //echo $newname_img;
           echo $sql="INSERT INTO LH_CLIP_FOR_PLAN (plan_id,clip_plan_name,clip_plan_url,thumnail,clip_type) "
                ." VALUES ('$id_plans','$plan_name_th','".$url_youtube."','$path_copy_img','$page')";
            $query=mssql_query($sql);

        }

    }
    //}

}


if($success_img){
    $_SESSION['add']='1';
    header("location:homemodel_add.php");
}



	



 

 

  