<?php
require_once 'include/dbConnect.php';

	try {

		$conn = (new dbConnect())->getConn();
        $sql = 'SELECT DISTINCT z.zone_id, z.zone_name_th, z.zone_name_en,
                a.AMPHUR_ID ,a.AMPHUR_NAME ,a.AMPHUR_NAME_ENG
               
                ,p.PROVINCE_ID, p.PROVINCE_NAME, p.PROVINCE_NAME_ENG
                FROM LH_ZONES z
                LEFT JOIN LH_PROVINCES P ON z.province_id = P.PROVINCE_ID
                LEFT JOIN LH_AMPHURS a ON  z.province_id = a.province_id
                LEFT JOIN LH_DISTRICTS d ON  d.AMPHUR_ID = a.AMPHUR_ID
                WHERE Z.zone_id = '.$_GET['zone_id'];

        $result['result_data'] = $conn->query($sql);
        $result['result_data'] = $result['result_data']->fetchAll();

		echo json_encode($result);

	} catch (\Exception $e) {
		return $e->getMessage();
	}
?>