﻿<?php
session_start();
include './include/dbCon_mssql.php';
date_default_timezone_set('Asia/Bangkok');
$date = date("Ymd");
$numrand_logo = (mt_rand());
$numrand_img = (mt_rand());

$project_review_name_th = mssql_escape($_POST['project_review_name_th']);
$project_review_name_en = $_POST['project_review_name_en'];


$project_id = $_POST['project'];
$project_review_img = $_FILES['project_review_img'];
$project_review_img_seo = $_POST['seo_lead_img_file'][0];
$project_review_dis_th = $_POST['project_review_dis_th'];
$project_review_dis_en = $_POST['project_review_dis_en'];
$project_review_dis_content = $_POST['project_review_dis_content'];
$project_review_dis_content_en = $_POST['project_review_dis_content_en'];
$file_image = $project_review_img['name'];
$check = getimagesize($_FILES["project_review_img"]["tmp_name"]);
$custom_url = trim($_POST['custom_url']);
$h1 = $_POST["h1"];

$id = $_POST['id'];
$order_seq = $_POST["order_seq"];



$date_start = $_POST['date_start'];

$day =substr($date_start, 0, 2);
$mount =substr($date_start, 3, 2);
$year =substr($date_start, 6, 4);

$date_start = $year."-".$mount."-".$day;
$time=date("Y-m-d H:i:s");

$fileupload_th = $_FILES['fileupload_th']['name'];
$title_seo_th = $_POST['landing_page_title_seo_th'];
$description = $_POST['landing_page_description_th'];
$keyword_seo = $_POST['keyword_seo'];

$date_seo = date("YmdHis");

if($order_seq != ''){
  $sql_old_val = "SELECT COUNT(*) AS counts FROM LH_PROJECT_REVIEW WHERE project_update = '$date_start'  AND order_seq = $order_seq  AND project_review_id != '$id' ";
  $query_result2 = mssql_query($sql_old_val, $db_conn);
  $count2 = mssql_fetch_row($query_result2);
  $count2[0];
  if($count2[0] >= 1){
              //มีชื่อ series นี้อยู่แล้ว
    $_SESSION['add']='3';
    echo '<script>
    window.history.go(-1);
    </script>';
    exit();
  }
}  


$query = "SELECT COUNT(*) AS counts FROM LH_PROJECT_REVIEW WHERE project_review_name_th = '$project_review_name_th' AND NOT project_review_id = '$id'";
$query_result= mssql_query($query , $db_conn);

$count = mssql_fetch_row($query_result);
$count[0];
if($count[0] >= 1){
    //มีชื่อ series นี้อยู่แล้ว
    //header("location:series_add.php?add=0");
  $_SESSION['add']='2';
  echo '<script>
  window.history.go(-1);
  </script>';
  exit();
}

$path = "fileupload/images/landingpage/";

$type_th = strrchr($_FILES['fileupload_th']['name'], ".");

$newname_th = $date_seo . $numrand_th . $type_th;

$path_img_seo = $path . $newname_th;

if (move_uploaded_file($_FILES['fileupload_th']['tmp_name'], $path_img_seo)) {

  $sql="UPDATE LH_PROJECT_REVIEW SET "
  ."thumnail_seo = '".$path_img_seo."'"
  ."WHERE project_review_id = '$id'";

  $query=mssql_query($sql);

}


$query_2 = "UPDATE LH_PROJECT_REVIEW SET project_review_name_th = '$project_review_name_th', "
."project_review_name_en ='$project_review_name_en',"
."project_id ='$project_id',"
."project_review_dis_th ='".mssql_escape($project_review_dis_th)."',"
."project_review_dis_en ='".mssql_escape($project_review_dis_en)."',"
."project_review_dis_content ='".mssql_escape($project_review_dis_content)."',"
."project_review_dis_content_en ='".mssql_escape($project_review_dis_content_en)."',"
."update_date = '$time' ,"
."title_seo = '".$title_seo_th."',"
."description_seo = '".$description."',"
."keyword_seo = '".$keyword_seo."',"
."custom_url = '".$custom_url."',"
."h1 = '$h1',"
."project_update = '$date_start' "
."WHERE project_review_id = '$id'";


$query_result2 = mssql_query($query_2 , $db_conn);


function mssql_escape($str)
{
  if(get_magic_quotes_gpc())
  {
    $str= stripslashes($str);
  }
  return str_replace("'", "''", $str);
}


     if ($check !== false) { //รูป

      $dates_file =date("Y-m-d");
      $path_img="fileupload/images/project_review/";
       //print_r($file_image);


        //resize
      $images = $_FILES["project_review_img"]["tmp_name"];
      $file_image = $_FILES["project_review_img"]["name"];
      $type_img = strrchr($file_image,".");
      $new_images = "Thumbnails_".$dates_file.$numrand_img.$type_img;
          //copy($_FILES["lead_image"]["tmp_name"][0],"fileupload/images/project_img/".$_FILES["lead_image"]["name"][0]);
          $width=600; //*** Fix Width & Heigh (Autu caculate) ***//
          $size=GetimageSize($images);
          $height=round($width*$size[1]/$size[0]);
          $images_orig = ImageCreateFromJPEG($images);
          $photoX = ImagesX($images_orig);
          $photoY = ImagesY($images_orig);
          $images_fin = ImageCreateTrueColor($width, $height);
          ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
          ImageJPEG($images_fin,"fileupload/images/project_review/".$new_images,93);
          ImageDestroy($images_orig);
          ImageDestroy($images_fin); 

          $newname_img = $dates_file.$numrand_img.$type_img;
          $path_copy_img=$path_img.$newname_img;
         // $seo = $_POST['seo_lead_img_file'][0];


          if(move_uploaded_file($_FILES['project_review_img']['tmp_name'],$path_copy_img)){
           $sql="SELECT project_review_img FROM LH_PROJECT_REVIEW WHERE project_review_id = '$id'";
           $query= mssql_query($sql , $db_conn);
           $row=mssql_fetch_array($query);
           unlink($path_img.$row['project_review_img']);

           $sql="UPDATE LH_PROJECT_REVIEW SET project_review_img = '$path_copy_img', project_review_img_seo = '".mssql_escape($project_review_img_seo)."' "
           ." WHERE project_review_id = '$id'";
           $query= mssql_query($sql, $db_conn);

         }else{
        //header('Location: ' . $_SERVER['HTTP_REFERER'].'&add=0');
         }
       }

       $sql = "UPDATE LH_PROJECT_REVIEW SET order_seq = '$order_seq' WHERE project_review_id = '$id'";
       $query_result3 = mssql_query($sql, $db_conn);
     // echo $order_seq;
       if($query_result2 && $query_result3){
        $_SESSION['add']='1';
        header("location:project_review_update.php?id=$id");
      }else{
        $_SESSION['add']='0';
        header("location:project_review_update.php?id=$id");
      }
