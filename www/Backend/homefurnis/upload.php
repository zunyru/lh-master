<?php
	require_once('RealTimeUpload.php');
	$settings = array(
		'uploadFolder' => 'gallery',
		'extension' => ['png', 'jpg', 'jpeg', 'gif'],
		'maxWidth' => 0,
		'maxHeight' => 0,
		'maxFileSize' =>2048,
		'returnLocation' => true
	);
	$uploader = new RealTimeUpload();
	$uploader->init($settings);
?>