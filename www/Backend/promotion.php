<?php
session_start();
include './include/dbCon_mssql.php';
require_once './include/function_date.php';
$group_id=$_SESSION['group_id'];

$_GET['page']='promotion';
if (isset($_GET['add'])) {
           if($_GET['add'] ==1){
             //header("location:product_add.php");
            $success ="success";
          } else if($_GET['add'] ==0){

            $error="error"; //ค่าซ้ำ

          } else if($_GET['add'] ==-1){
            $error="errors";
          }
      }
if(isset($_GET['delete'])){
    $delete = 'd';
}
if(isset($_GET['approve'])){
    $approve = '1';
}


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>

      <!-- Bootstrap -->
      <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome -->
      <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <!-- NProgress -->
      <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
      <!-- iCheck -->
      <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
      <!-- Datatables -->
      <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

      <!-- Custom Theme Style -->
      <link href="../build/css/custom.min.css" rel="stylesheet">

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <?php include './master/navbar.php';?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
          <?php include './master/top_nav.php'; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>ระบบจัดการข้อมูลโปรโมชั่น</h2>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                  <?php if(isset($success)){?>
                   <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>บันทึกข้อมูลแล้ว !</strong> เพิ่มข้อมูล Promotion เรียบร้อย
                      </div>
                    </div>
                    <div class="col-md-6"></div>
                   </div>
                  <?php }else if(isset($error)){?>
                    <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>ลบข้อมูลเรียบร้อยแล้ว !</strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                   <?php }else if(isset($errors)){?>

                  <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>เกิดข้อผิดพลาด !</strong> ไม่สามารถบันทึกข้อมมูลได้
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                  <?php }?>

                   <?php if(isset($delete)){?>

                  <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>ลบข้อมูลเรียบร้อยแล้ว </strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                  <?php }?>

                  <?php if(isset($approve)){?>

                  <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>อนุมัติโปรโมชันเรียบร้อยเรียบร้อย !</strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                  <?php }?>


                  <form method="get">
                    <div class="col-md-2 col-sm-9 col-xs-12">
                          <select class="form-control" name="myselect" id="myselect" onchange="this.form.submit(); redirect();">
                            <option value="0">เลือกดูตามสถานะ</option>
                            <option value="N" <?php if($_GET['myselect']=="N"){echo "selected";} ?>>รออนุมัติ</option>
                            <option value="Y" <?php if($_GET['myselect']=="Y"){echo "selected";} ?>>อนุมัติ</option>
                          </select>
                    </div>
                  </form>
                  <script>
                    function redirect(){
                       var myselect = document.getElementById("myText").value = "Johnny Bravo";
                       window.location.href = "promotion.php?myselect="+myselect ;
                    }
                </script>

                    <?php if($_SESSION['group_account_name'] != 'Admin'){?>
                    <a href="promotion_add.php"><button type="button" class="btn btn-success flright">สร้างข้อมูลโปรโมชั่น</button></a>
                    <?php }?>
                    <!-- <p class="text-muted font-13 m-b-30">
                      DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>
                    </p> -->
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th><center>ชื่อโปรโมชั่น</center></th>
                          <th><center>โครงการ</center></th>
                          <th><center>เริ่ม-สิ้นสุด</center></th>
                          <th><center>สถานะข้อมูล</center></th>
                            <th><center>วันที่แก้ไขล่าสุด</center></th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                          require_once './include/function_date.php';
                         if($_SESSION['group_account_name'] != 'Admin'){
                          if(isset($_GET['myselect'])){
                            $myselect =$_GET['myselect'];
                            if($myselect=='0'){
                             $query_3 ="SELECT prm.promotion_id,prm.promotion_name_th,prm.promotion_name_en,prm.promotion_start_date,prm.promotion_end_date,prm.promotion_approve,prj.project_name_th,prm.update_date FROM LH_PROMOTION prm LEFT JOIN LH_PROJECTS prj ON prm.project_id = prj.project_id WHERE prm.promotion_approve = 'Y' OR prm.promotion_approve = 'N' AND prj.group_id ='$group_id';";
                            }else{
                               $query_3 ="SELECT prm.promotion_id,prm.promotion_name_th,prm.promotion_name_en,prm.promotion_start_date,prm.promotion_end_date,prm.promotion_approve,prj.project_name_th,prm.update_date FROM LH_PROMOTION prm LEFT JOIN LH_PROJECTS prj ON prm.project_id = prj.project_id WHERE prm.promotion_approve = '$myselect' AND prj.group_id ='$group_id';";
                            }
                          }else{

                          $query_3 ="SELECT prm.promotion_id,prm.promotion_name_th,prm.promotion_name_en,prm.promotion_start_date,prm.promotion_end_date,prm.promotion_approve,prj.project_name_th,prm.update_date FROM LH_PROMOTION prm LEFT JOIN LH_PROJECTS prj ON prm.project_id = prj.project_id WHERE prj.group_id ='$group_id' ;";
                          }
                        }else{
                          if(isset($_GET['myselect'])){
                           $myselect =$_GET['myselect'];
                           if($myselect=='0'){
                             $query_3 ="SELECT prm.promotion_id,prm.promotion_name_th,prm.promotion_name_en,prm.promotion_start_date,prm.promotion_end_date,prm.promotion_approve,prj.project_name_th,prm.update_date FROM LH_PROMOTION prm LEFT JOIN LH_PROJECTS prj ON prm.project_id = prj.project_id WHERE prm.promotion_approve = 'Y' OR prm.promotion_approve = 'N' ";
                           }else{
                            $query_3 ="SELECT prm.promotion_id,prm.promotion_name_th,prm.promotion_name_en,prm.promotion_start_date,prm.promotion_end_date,prm.promotion_approve,prj.project_name_th,prm.update_date FROM LH_PROMOTION prm LEFT JOIN LH_PROJECTS prj ON prm.project_id = prj.project_id WHERE prm.promotion_approve = '$myselect' ";
                           }
                          }else{

                          $query_3 ="SELECT prm.promotion_id,prm.promotion_name_th,prm.promotion_name_en,prm.promotion_start_date,prm.promotion_end_date,prm.promotion_approve,prj.project_name_th,prm.update_date FROM LH_PROMOTION prm LEFT JOIN LH_PROJECTS prj ON prm.project_id = prj.project_id ";
                          }
                        }

                          $query_result3= mssql_query($query_3 , $db_conn);
                          while ($row = mssql_fetch_array($query_result3)) {
                                 $date=$row['promotion_start_date'];
                                 $year = substr($date, 0, 4);
                                 $mont = substr($date, 5, 2);
                                 $day = substr($date, 8, 2);
                                 $strDate1= $year."-".$mont."-".$day;

                                 $date2=$row['promotion_end_date'];
                                 $year2 = substr($date2, 0, 4);
                                 $mont2 = substr($date2, 5, 2);
                                 $day2 = substr($date2, 8, 2);
                                 $strDate2= $year2."-".$mont2."-".$day2;

                              $date=date_create($row['update_date']);
                              $strDate =date_format($date,"Y-m-d H:i:s");

                      ?>

                        <tr>
                          <td><p style="display:none;"><?=$row['promotion_start_date']?></p><a href="promotion_update.php?id=<?=$row['promotion_id']; ?>">

                          <?php
                          $str =strlen($row['promotion_name_th']);
                          if($str >= 66){
                              echo iconv_substr($row['promotion_name_th'], 0,66, "UTF-8")."...";
                          }else{
                              echo $row['promotion_name_th'];
                          }
                          ?>
                          </a></td>
                          <td><a href="promotion_update.php?id=<?=$row['promotion_id'];?>"><?=$row['project_name_th'];?></a></td>
                          <td><center><a href="promotion_update.php?id=<?php echo $row['promotion_id'];?>"><?=DateThai($strDate1)." - ".DateThai($strDate2);?></a></center></td>
                          <td><center><a href="promotion_update.php?id=<?php echo $row['promotion_id'];?>">
                            <?php
                              if($row['promotion_approve']=='N'){
                                  echo 'รอนุมัติ';
                              }else if($row['promotion_approve']=='Y'){
                                  echo 'อนุมัติ';
                              }
                            ?>
                          </a></center></td>
                            <td><p class="hidden"><?=$strDate?></p>
                                <center><?=DateThai_time($row['update_date'])?></center></td>
                        </tr>
                        <?php } ?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

     <script>
      $(document).ready(function(){
        $("#alert").show();
            $("#alert").fadeTo(3000, 500).slideUp(500, function(){
              $("#alert").alert('close');
          });
      });
      </script>

    <!-- Datatables -->
    <!-- Datatables -->
    <script>
      $(document).ready(function() {


        $('#datatable').dataTable({
          "bLengthChange": false,
          "pageLength": 50,
          "order": [[ 4, "DESC" ]]
        });


      });
    </script>
    <!-- /Datatables -->
  </body>
</html>