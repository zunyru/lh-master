<?php
session_start();
include './connect_db.php';
include './include/dbCon_mssql.php';
require_once './include/function_date.php';
include_once('./include/dbSeries.php');
$group_id=$_SESSION['group_id'];
unset($_SESSION["add"]);

$_GET['page']='homemodel';

if (isset($_GET['add'])) {
           if($_GET['add'] ==1){
             //header("location:product_add.php");
            $success ="success";
          } else if($_GET['add'] ==0){

            $error="error"; //ค่าซ้ำ

          } else if($_GET['add'] ==-1){
            $error="errors";
          }
      }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">


  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
           <?php include './master/navbar.php';?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
          <?php include './master/top_nav.php'; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>ข้อมูลสไตล์บ้าน (Series)</h2>

                    <div class="clearfix"></div>
                  </div>
                  <?php if(isset($_GET['delete'])){?>
                      <div class="row">
                              <div class="col-md-2"></div>
                               <div class="form-group">
                                <div class="alert alert-success col-md-6" id="alert">
                                  <button type="button" class="close" data-dismiss="alert">x</button>
                                  <strong>ลบข้อมูลเรียบร้อยแล้ว</strong>
                                </div>
                              </div>
                              <div class="col-md-4"></div>
                             </div>
                  <?php }?>
                  <?php if(isset($success)){?>
                   <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>สร้างข้อมูลเรียบร้อยแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-6"></div>
                   </div>
                  <?php }else if(isset($yet)){?>
                    <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>มีชื่อนี้อยู่ในระบบแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                  <?php }else if(isset($error)){?>
                   <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>เกิดข้อผิดพลาด !</strong> กรุณากรอกข้มูลให้ครบถ้วน
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                   <?php } ?>
                  <div class="x_content">
                      <a href="series.php"><button type="button" class="btn btn-success">ข้อมูลสไตล์บ้าน (Series)</button></a>
                      <a href="homemodel.php"><button type="button" class="btn btn-default">ข้อมูลแบบบ้าน</button></a>
                      <a href="fucntion_home.php"><button type="button" class="btn btn-default">เพิ่มฟังก์ชั่นบ้าน</button></a>
                      <a href="fucntion_condo.php"><button type="button" class="btn btn-default">ฟังก์ชั่นคอนโด</button></a>
                      <a href="icon_activity.php"><button type="button" class="btn btn-default">เพิ่มไอคอนกิจกรรม</button></a>
                      <a href="master_gallery_homesell.php"><button type="button" class="btn btn-default">ข้อมูลรูปบ้านตกแต่งพร้อมขาย</button></a>
                      <br>

                    <a href="series_add.php"><button type="button" class="btn btn-success flright">ข้อมูลสไตล์บ้าน (Series)</button></a>
                    <!-- <p class="text-muted font-13 m-b-30">
                      DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>
                    </p> -->
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th width="30%"><center>ชื่อสไตล์</center></th>
                          <th width="45%"><center>รายละเอียด</center></th>
                          <th width="8%"><center>LOGO สไตล์</center></th>
                          <th width="5%"><center>แสดง</center></th>
                          <th width="12%"><center>วันที่แก้ไขล่าสุด</center></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php

                            // $SQL="SELECT * FROM dbo.LH_GROUPS";
                            // $stmt = $conn->query( $SQL );
                            // $row_count = $stmt->rowCount();
                            // $funObj = new dbSeries();
                            // $stmt = $funObj->show_series($group_id);
                            $sql="SELECT * FROM LH_SERIES ";
                              $stmt= mssql_query($sql , $db_conn);
                              //$stmt = mssql_fetch_array($query_result3);
                            if($stmt){
                            while ($row = mssql_fetch_array($stmt)){
                              //print_r($row);
                              $date=date_create($row['update_date']);
                               $strDate =date_format($date,"Y-m-d H:i:s");
                         ?>

                        <tr>
                          <td>
                              <p style="display: none"><?=$row['series_id']?></p>
                              <a href="series_update.php?id=<?=$row['series_id']?>"><?=$row['series_name_th']; ?></a></td>
                          <td><a href="series_update.php?id=<?=$row['series_id']?>"><p><?=$row['series_des_th']?></p></a></td>
                          <td><center><a href="series_update.php?id=<?=$row['series_id']?>"><?php if($row['series_logo']=='not_img'){echo 'N';}else{echo 'Y';}?></a></center></td>
                          <td><?=$row['views'] == 'on' ? 'เปิด' : 'ปิด'; ?></td>
                          <td><p class="hidden"><?=$strDate; ?></p>
                                <center><a href="series_update.php?id=<?=$row['series_id']?>"><?=DateThai_time($row['update_date']);?></a></center></td>
                        </tr>
                        <?php }} ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

     <script>
      $(document).ready(function(){
        $("#alert").show();
            $("#alert").fadeTo(3000, 400).slideUp(500, function(){
              $("#alert").alert('close');
          });
      });
      </script>

    <!-- Datatables -->
    <script>
        $(document).ready(function() {
            var handleDataTableButtons = function() {
                if ($("#datatable-buttons").length) {
                    $("#datatable-buttons").DataTable({
                        dom: "Bfrtip",
                        buttons: [
                            {
                                extend: "copy",
                                className: "btn-sm"
                            },
                            {
                                extend: "csv",
                                className: "btn-sm"
                            },
                            {
                                extend: "excel",
                                className: "btn-sm"
                            },
                            {
                                extend: "pdfHtml5",
                                className: "btn-sm"
                            },
                            {
                                extend: "print",
                                className: "btn-sm"
                            },
                        ],
                        responsive: true
                    });
                }
            };

            TableManageButtons = function() {
                "use strict";
                return {
                    init: function() {
                        handleDataTableButtons();
                    }
                };
            }();

            $('#datatable').dataTable(
                {
                    "order": [[ 4, 'desc' ]],
                    "bLengthChange": false,
                    "pageLength": 50
                });

            $('#datatable-keytable').DataTable({
                keys: true
            });

            $('#datatable-responsive').DataTable();

            $('#datatable-scroller').DataTable({
                ajax: "js/datatables/json/scroller-demo.json",
                deferRender: true,
                scrollY: 380,
                scrollCollapse: true,
                scroller: true
            });

            $('#datatable-fixed-header').DataTable({
                fixedHeader: true
            });

            var $datatable = $('#datatable-checkbox');


            $datatable.dataTable({


            });
            $datatable.on('draw.dt', function() {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_flat-green'
                });
            });

            TableManageButtons.init();
        });
    </script>
    <!-- /Datatables -->
  </body>
</html>