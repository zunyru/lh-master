<?php
session_start();
header( 'Content-Type:text/html; charset=utf8');
if (!$_SESSION['login']) {
      $link = "Location: login.php";
      header($link);
}

require_once 'include/dbConnect.php';
require_once 'include/dbCon_mssql.php'; // for insert utf8
require_once './include/function_date.php';

$conn = (new dbConnect())->getConn();
$project_name_th = "";

if (isset($_GET['project_id'])) {
  $arrProjectName = getProjectsById($_GET['project_id']);
  $GLOBALS['project_name_th'] = $arrProjectName[0]['project_name_th'];
  $arrHomesales = getAllDataOfHomesalesBySearch($_POST);
} else {
  echo 'เกิดข้อผิดพลาด';
}

if (isset($_POST['status_homesales'])) {

    $arrHomesales = getAllDataOfHomesalesBySearch($_POST);
}

function getProjectsById($project_id) {
    try {
        $sql = 'select project_name_th from lh_projects where project_id='.$project_id;
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}

function getAllDataOfHomesalesBySearch($request) {
    try {

        $status_homesales = null;
        if (isset($request['status_homesales'])) {
            $status_homesales = $request['status_homesales'];
        }
        $_SESSION['status_homesales'] = '0';

        $sql = 'SELECT PD.product_name_th, HS.home_sell_id,HS.update_date, P.plan_name_th, P.plan_code, HS.features_convert, HS.home_sell_status,HS.number_converter,HS.order_request,HS.plan_id,HS.project_sub_id
                FROM LH_HOME_SELL AS HS
                LEFT JOIN LH_PLANS P
                ON HS.plan_id = p.plan_id
                LEFT JOIN LH_PROJECT_SUB PS
                ON HS.project_sub_id = PS.project_sub_id
                LEFT JOIN LH_PRODUCTS PD
                ON P.product_id = PD.product_id
                WHERE PS.project_id ='.$_GET['project_id'];

        if ($status_homesales == "sold") {
            $sql .= ' AND HS.home_sell_status ="SO" ';
            $_SESSION['status_homesales'] = $status_homesales;
        } else if ($status_homesales == "open") {
            $sql .= ' AND HS.home_sell_status = null';
            $_SESSION['status_homesales'] = $status_homesales;
        }

        $query = mssql_query($sql);
        $result = array();
        while ( $row = mssql_fetch_assoc($query)) {
          array_push($result, $row);
        }
        
        return $result;
        // $result = $GLOBALS['conn']->query($sql);
        // return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}

$group_id=$_SESSION['group_id'];
$_GET['page']='form';

if (isset($_SESSION['add'])) {
    if($_SESSION['add'] ==1){

        $add="add"; //ค่าซ้ำ
        $_SESSION['add']='';

    }else if($_SESSION['add'] ==0){
        $delete="delete"; //ค่าซ้ำ
    }
    unset($_SESSION["add"]);
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

           <?php include "master/navbar.php"; ?>

          </div>
        </div>

        <!-- top navigation -->
          <?php include './master/top_nav.php'; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>ข้อมูลบ้านพร้อมขายโครงการ : <a href="qc_projectview.php?project_id=<?php echo $_GET['project_id']; ?>"><?php echo $GLOBALS['project_name_th'] ?></a></h2>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="font-gray-dark"></p>
                   <?php if(isset($add)){?>
                      <div class="row">
                        <div class="col-md-2"></div>
                         <div class="form-group">
                          <div class="alert alert-success col-md-12" id="alert">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <strong>สร้างข้อมูลเรียบร้อยแล้ว</strong>
                          </div>
                        </div>
                        <div class="col-md-4"></div>
                       </div>
                  <?php }?>
                   <?php if(isset($delete)){?>
                      <div class="row">
                        <div class="col-md-2"></div>
                         <div class="form-group">
                          <div class="alert alert-success col-md-12" id="alert">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <strong>ลบข้อมูล!</strong> เรียบร้อย
                          </div>
                        </div>
                        <div class="col-md-4"></div>
                       </div>
                  <?php }?>
                    <form action="" method="POST" accept-charset="utf-8">
                      <div class="col-md-3 col-sm-9 col-xs-12">
                            <select class="form-control" name="status_homesales" onchange="searchStatus()">
                              <?php if ($_SESSION['status_homesales'] == 'open') { ?>
                                          <option value="0">เลือกดูตามสถานะ</option>
                                          <option value="open" selected>เปิดขาย</option>
                                          <option value="sold">Sold out</option>
                              <?php } else if ($_SESSION['status_homesales'] == 'sold') { ?>
                                          <option value="0">เลือกดูตามสถานะ</option>
                                          <option value="open">เปิดขาย</option>
                                          <option value="sold" selected>Sold out</option>
                              <?php } else { ?>
                                          <option value="0" selected>เลือกดูตามสถานะ</option>
                                          <option value="open">เปิดขาย</option>
                                          <option value="sold">Sold out</option>
                              <?php } ?>
                            </select>
                      </div>
                      <button type="submit" id="realSubmit" class="hide">Submit-Hidden</button>
                    </form>

                    <a href="homesales_add_master.php?project_id=<?php echo $_GET['project_id'];?>"><button type="button" class="btn btn-success flright">สร้างข้อมูลบ้านพร้อมขาย</button></a>
                    <!-- <p class="text-muted font-13 m-b-30">
                      DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>
                    </p> -->

                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <!-- <th><center>Create Date</center></th>
                          <th><center>Update Date</center></th> -->
                          <th><center>ประเภทแบบบ้าน</center></th>
                          <th><center>หมายเลขแปลง</center></th>
                          <th><center>ชื่อแบบบ้าน</center></th>
                          <th><center>รหัสแบบบ้าน</center></th>
                          <th><center>ลักษณะเด่นของแปลง</center></th>
                          <th><center>Sold Out (ระบุ)</center></th>
                          <th><center>แสดงเป็นบ้านหลัก</center></th>
                            <th><center>วันที่แก้ไขล่าสุด</center></th>
                        </tr>
                      </thead>


                      <tbody>


                         <?php  foreach ($arrHomesales as $value) {
                          $url = 'homesales_edit.php?project_id='.$_GET['project_id'].'&home_sell_id='.$value['home_sell_id'];

                             $date=date_create($value['update_date']);
                             $strDate =date_format($date,"Y-m-d H:i:s");
                          ?>
                        <tr>


                          <td><p style="display: none"><?php echo $value['home_sell_id'] ?></p>
                              <a href="<?php echo $url ?>"><?php echo $value['product_name_th'] ?></a></td>
                            <td><a href="<?php echo $url ?>"><?php echo $value['number_converter'] ?></a></td>
                          <td>
                            <input type="hidden" name="plan" class="plan" value="<?php echo $value['plan_id'] ?>">
                            <input type="hidden" name="sub" class="sub" value="<?php echo $value['project_sub_id'] ?>">
                            <a href="<?php echo $url ?>"><?php echo $value['plan_name_th'] ?></a></td>
                          <td><a href="<?php echo $url ?>"><?php echo $value['plan_code'] ?></a></td>
                          <td><a href="<?php echo $url ?>">

                             <?php 
                                
                                 $str =strlen($value['features_convert'] );
                                  if($str >= 66){
                                      echo iconv_substr($value['features_convert'], 0,66, "UTF-8")."...";
                                  }else{
                                      echo $value['features_convert'];
                                  }

                              ?>
                            
                          </a></td>
                          <td>
                                <?php if($value['home_sell_status'] == 'SO') { ?>
                                    <center><div class="checkbox">
                                      <label><input type="checkbox" value="<?php echo $value['home_sell_id'] ?>" class="flat soldout_checkbox" checked /></label></div>
                                    </center>
                                <?php } else { ?>
                                    <center>
                                      <div class="checkbox">
                                      <label><input type="checkbox" value="<?php echo $value['home_sell_id'] ?>" class="flat soldout_checkbox" /></label></div>
                                    </center>
                                <?php } ?>
                          </td>
                          <td>
                           <?php if($value['order_request'] == 1) { ?> 
                            <center>
                              <div class="checkbox">
                              <label><input type="checkbox" checked value="<?php echo $value['home_sell_id'] ?>" class="flat order_request" />
                              </label></div>
                            </center>
                            <?php } else { ?>
                            <center>
                              <div class="checkbox">
                              <label><input type="checkbox" value="<?php echo $value['home_sell_id'] ?>" class="flat order_request" />
                              </label></div>
                            </center>
                           <?php } ?> 

                          </td>  
                            <td><p class="hidden"><?=$strDate;?></p>
                                <center><?=DateThai_time($value['update_date']);?></center>
                            </td>
                        </tr>
                          <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
    <script type="text/javascript">
      function searchStatus(){
        $("#realSubmit").click();
      }
    </script>
    <!-- Datatables -->
    <!-- Datatables -->

    <!-- /Datatables -->
    <script>
      $(document).ready(function() {

          $(".soldout_checkbox").on("ifChanged", function (event) {
              var value = $(this).val();
              if ($(this).prop('checked')) {
                  checkSoldOut(value, 'checked');
              } else {
                  // remove
                  checkSoldOut(value, 'unchecked');
              }
          });

          //order_
          $(".order_request").on("ifChanged", function (event) {
              var value = $(this).val();
              var plan = $("input[name=plan]").val();
              var sub = $("input[name=sub]").val();
              console.log(plan);
              console.log(sub);
              if ($(this).prop('checked')) {
                  checkOrder(value,plan,sub, 'checked');
              } else {
                  // remove
                  checkOrder(value,plan,sub, 'unchecked');
              }
          });


          $("#alert").fadeTo(3000, 500).slideUp(500, function () {
              $("#alert").slideUp(500);
          });


          var url = "ajax_updateStatusHomeSales.php";

          function checkSoldOut(id, command) {
              if (id != 0) {
                  if (command == 'checked') {
                      var link = url + '?home_sell_id=' + id + '&command=' + command;
                  } else {
                      var link = url + '?home_sell_id=' + id + '&command=' + command;
                  }
                  $.ajax(link)
                      .done(function (response) {
                          if (response) {
                              //update success
                              console.info("update success.");
                          } else {
                              console.info("update fail.");
                          }
                      })
                      .fail(function () {
                          console.error('Backend error');
                      })
                      .always(function () {
                          console.info("complete");
                      });
              } else {

              }
          }

          //fucntion
          var url_c = "ajax_updateOrderHomeSales.php";

          function checkOrder(id,plan,sub, command) {
              if (id != 0) {
                  if (command == 'checked') {
                      var link = url_c + '?home_sell_id=' + id + '&command=' + command +'&plan='+plan +'&sub=' +sub;
                  } else {
                      var link = url_c + '?home_sell_id=' + id + '&command=' + command; +'&plan='+plan +'&sub=' +sub;
                  }
                  $.ajax(link)
                      .done(function (response) {
                          if (response) {
                              //update success
                              window.setTimeout('location.reload()', 100);
                              console.info("update success.");
                          } else {
                              console.info("update fail.");
                          }
                      })
                      .fail(function () {
                          console.error('Backend error');
                      })
                      .always(function () {
                          console.info("complete");
                      });
              } else {

              }
          }
      })
    </script>
    <!-- /Datatables -->
    <script>
        $(document).ready(function() {


            $('#datatable').dataTable({
                "bLengthChange": false,
                "pageLength": 50,
                "order": [[ 6, "desc" ]]
            });


        });
    </script>

    <script>
        $(document).ready(function(){
            $("#alert").show();
            $("#alert").fadeTo(6000, 500).slideUp(500, function(){
                $("#alert").alert('close');
            });
        });
    </script>
  </body>
</html>