<?php
session_start();
header( 'Content-Type:text/html; charset=utf8');

error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once 'include/dbConnect.php';

$conn = (new dbConnect())->getConn();

$key = '<code to parse your image key>';
$url = '<your server action to delete the file>';

try {

    $sql = "DELETE FROM LH_LEAD_IMAGE_PROJECT_FILE
            WHERE LEAD_IMAGE_PROJECT_FILE_ID = ".$_POST['key'];

    $result = $GLOBALS['conn']->query($sql);
    //echo "Project Id : ".$_SESSION['project_id'];

    try {

        $sql = "SELECT * FROM LH_LEAD_IMAGE_PROJECT_FILE
                WHERE project_id = ".$_SESSION['project_id'];

        $result = $GLOBALS['conn']->query($sql);
        $result_images =  $result->fetchAll();

        $url = "http://devwww2.lh.co.th/Land_and_house/Backend/";
        $urlDelete = "http://devwww2.lh.co.th/Land_and_house/Backend/delete_image_project.php";
        $initialPreview = [];
        $initialPreviewConfig = [];
        for ($i = 0; $i < count($result_images); $i++) {
            array_push($initialPreview,[$url.$result_images[$i]['LEAD_IMAGE_PROJECT_FILE_NAME']]);
            array_push($initialPreviewConfig,[
                    'key' => $result_images[$i]['LEAD_IMAGE_PROJECT_FILE_ID'],
                    'url' => $urlDelete
                ]);
        }

        echo json_encode([
                    'initialPreview' => [$initialPreview],
                    'initialPreviewConfig' => [$initialPreviewConfig],
                    'append' => true
                ]);

    }  catch (PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}  catch (PDOException $e) {
    echo $sql . "<br>" . $e->getMessage();
}


if (!function_exists('base_url')) {
    function base_url($atRoot=FALSE, $atCore=FALSE, $parse=FALSE){
        if (isset($_SERVER['HTTP_HOST'])) {
            $http = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
            $hostname = $_SERVER['HTTP_HOST'];
            $dir =  str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

            $core = preg_split('@/@', str_replace($_SERVER['DOCUMENT_ROOT'], '', realpath(dirname(__FILE__))), NULL, PREG_SPLIT_NO_EMPTY);
            $core = $core[0];

            $tmplt = $atRoot ? ($atCore ? "%s://%s/%s/" : "%s://%s/") : ($atCore ? "%s://%s/%s/" : "%s://%s%s");
            $end = $atRoot ? ($atCore ? $core : $hostname) : ($atCore ? $core : $dir);
            $base_url = sprintf( $tmplt, $http, $hostname, $end );
        }
        else $base_url = 'http://localhost/';

        if ($parse) {
            $base_url = parse_url($base_url);
            if (isset($base_url['path'])) if ($base_url['path'] == '/') $base_url['path'] = '';
        }

        return $base_url;
    }
}

?>