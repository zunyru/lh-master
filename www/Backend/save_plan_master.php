<?php
session_start();
include './include/dbCon_mssql.php';

$series = $_POST['series'];
$plan_code = $_POST['plan_code'];
$plan_name_th = $_POST['plan_name_th'];
$plan_name_en = $_POST['plan_name_en'];
$plan_useful = $_POST['plan_useful'];
$product_id = $_POST['product_id'];
$message1_th = $_POST['message1_th'];
$message1_en = mssql_escape($_POST['message1_en']);
$message2_th = $_POST['message2_th'];
$message2_en = mssql_escape($_POST['message2_en']);
$message3_th = $_POST['message3_th'];
$message3_en = mssql_escape($_POST['message3_en']);
$switch = $_POST['switch'];



function mssql_escape($str)
{
    if(get_magic_quotes_gpc())
    {
        $str= stripslashes($str);
    }
    return str_replace("'", "''", $str);
}


$dates=date("Y-m-d H:i:s");

//เช็คชื่อซ้ำ
$sql="SELECT COUNT(*) AS counts FROM LH_PLANS 
WHERE plan_name_th = '$plan_name_th'  ";
$query_result2= mssql_query($sql , $db_conn);
$count = mssql_fetch_array($query_result2);
$count['counts'];
if($count['counts'] >= 1){
    //มีชื่อ series นี้อยู่แล้ว
    $_SESSION['add']='0';
    echo '<script>window.history.go(-1);</script>';
    exit();
}
//print_r($_FILES["images_plan"]);
$numrand_img= (mt_rand());
$date = date('dmY');
/*
$seo_img_paln =$_POST['seo_img_paln'][0];
$images = $_FILES["images_plan"]["tmp_name"][0];
$images2 = $_FILES["images_plan"]["name"][0];
$type_img = strrchr($images2,".");
$pathName = "fileupload/images/galery_plan/" . $numrand_img.$date.$type_img;
*/

    

    //resize
    $sql = "INSERT INTO LH_PLANS (series_id,plan_name_th,plan_name_en,plan_useful,plan_feature_th,plan_feature_en,plan_code,plan_land_size,product_id,plan_img,plan_seo,update_date,views) "
        . " VALUES ('$series','$plan_name_th','$plan_name_en','$plan_useful','$plan_feature_th','$plan_feature_en','$plan_code','$plan_land_size','$product_id','','','$dates','$switch')";
    $query_result2 = mssql_query($sql, $db_conn);


$query_3 ="SELECT MAX(plan_id) AS max_id FROM dbo.LH_PLANS;";
$query_result3= mssql_query($query_3 , $db_conn);
$row = mssql_fetch_array($query_result3);

//Model

if(isset($_FILES["model_galery"]["name"][0])) {
    if ($_FILES["model_galery"]["name"][0] != "" && $_FILES["model_galery"]["name"][0] != null) {
          
        $numSize = count($_FILES['model_galery']["name"]);
        for ($i = 0; $i < $numSize; $i++) {
             
             $numrand = mt_rand();
             $images = $_FILES["model_galery"]["tmp_name"][$i];
             $images_name = $_FILES["model_galery"]["name"][$i];
             $type_img = strrchr($images_name,".");
             //resize 
             $new_images = "Thumbnails_".$date.$numrand.$type_img;
             $width=600;
             $size=GetimageSize($images);
             $height=round($width*$size[1]/$size[0]);
             $images_orig = ImageCreateFromJPEG($images);
             $photoX = ImagesX($images_orig);
             $photoY = ImagesY($images_orig);
             $images_fin = ImageCreateTrueColor($width, $height);
             ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
             ImageJPEG($images_fin,"fileupload/images/galery_plan/".$new_images,93);
             ImageDestroy($images_orig);
             ImageDestroy($images_fin);

             $pathName_model = "fileupload/images/galery_plan/" .$date.$numrand.$type_img;
             if (move_uploaded_file($_FILES["model_galery"]["tmp_name"][$i], $pathName_model)) {
                 
                $sql = "INSERT INTO LH_MODEL_IMAGE_PLANS
                     (model_img_path, model_seo,plan_id)
                     VALUES ('" . $pathName_model . "', '" . $_POST['upload_model_galery_seo'][$i] . "','".$row['max_id']."')";
                 $result = mssql_query($sql, $GLOBALS['db_conn']);
             }   
           
        
           if(($_POST['model_galery_radio'][0] != '') && ($_POST['model_galery_radio'][0] == $i) ){
              $pathName = $pathName_model;
              $sql_model = "UPDATE LH_PLANS SET plan_img = '".$pathName."' ,plan_seo = '".$_POST['upload_model_galery_seo'][$i]."' WHERE plan_id = '".$row['max_id']."' ";
              $result = mssql_query($sql_model, $GLOBALS['db_conn']);
           }
        }
    }
}         


$id_plans=$row['max_id'];

if($_POST['name_f']<>''){
    foreach ($_POST['name_f'] as $key_n => $fucn_n) {

        foreach ($_POST['fucn'] as $key=> $fucn) {
            if($key == $key_n){
                if($fucn != ''){
                   
                    $sql4="INSERT INTO LH_FUNCTION_PLAN_SUB (plan_id,function_plan_plan_id,function_plan_sub_plan_count_room) "
                        ." VALUES ('$id_plans','$fucn_n','$fucn')";
                    $query_r4= mssql_query($sql4 , $db_conn);
                }

            }

        }
    }
}//
if ($_FILES["images_plan_1"]["name"][0] != "" && $_FILES["images_plan_1"]["name"][0] != null) {
    $seo_img_plan1 = $_POST['seo_img_floor_paln1'][0];
    $numrand_img1= (mt_rand());
    
    $images = $_FILES["images_plan_1"]["name"][0];
    $date = date('dmY');
    $type_img = strrchr($images,".");
    $name = $numrand_img1 .$date . $type_img;
    $pathName1 = "fileupload/images/floor_plan_master/" . $numrand_img1 .$date. $type_img;
    if (move_uploaded_file($_FILES["images_plan_1"]["tmp_name"][0], $pathName1)) {

       $sql ="INSERT INTO LH_GALERY_FLOOR_PLAN_IMG
(plan_id,floor_plan_img_name,floor_plan_dis_th,floor_plan_dis_en,floor_plan_img_seo,number_floor_plan)
VALUES ('$id_plans','$pathName1','". mssql_escape($message1_th)."','".mssql_escape($message1_en)."','$seo_img_plan1','1')";
        $query_r = mssql_query($sql, $db_conn);
    }
}

if ($_FILES["images_plan_2"]["name"][0] != "" && $_FILES["images_plan_2"]["name"] != null) {
    $seo_img_plan2 = $_POST['seo_img_floor_paln2'][0];
    $numrand_img2= (mt_rand());
    $images = $_FILES["images_plan_2"]["name"][0];
    $date = date('dmY');
    $type_img = strrchr($images,".");
    $name = $numrand_img2 .$date .$type_img;
    $pathName2 = "fileupload/images/floor_plan_master/" . $name;
    if (move_uploaded_file($_FILES['images_plan_2']['tmp_name'][0], $pathName2)) {
        $sql8 = "INSERT INTO LH_GALERY_FLOOR_PLAN_IMG (plan_id,floor_plan_img_name,floor_plan_dis_th,floor_plan_dis_en,floor_plan_img_seo,number_floor_plan) "
            . " VALUES ('$id_plans','$pathName2','" . mssql_escape($message2_th) . "','" . mssql_escape($message2_en) . "','$seo_img_plan2','2')";
        $query_r8 = mssql_query($sql8, $db_conn);
    }
}

if ($_FILES["images_plan_3"]["name"][0] != "" && $_FILES["images_plan_3"]["name"] != null) {
    $seo_img_plan3 = $_POST['seo_img_floor_paln3'][0];
    $numrand_img3= (mt_rand());
    $images = $_FILES["images_plan_3"]["name"][0];
    $date = date('dmY');
    $type_img = strrchr($images,".");

    $name = $numrand_img3 . $date . $type_img;
    $pathName3 = "fileupload/images/floor_plan_master/" .$name;
    if (move_uploaded_file($_FILES['images_plan_3']['tmp_name'][0], $pathName3)) {
        $sql8 = "INSERT INTO LH_GALERY_FLOOR_PLAN_IMG (plan_id,floor_plan_img_name,floor_plan_dis_th,floor_plan_dis_en,floor_plan_img_seo,number_floor_plan) "
            . " VALUES ('$id_plans','$pathName3','" . mssql_escape($message3_th) . "','" . mssql_escape($message3_en) . "','$seo_img_plan3','3')";
        $query_r8 = mssql_query($sql8, $db_conn);
    }
}

//360

if ($_FILES["c_360_img"]["name"][0] != "" && $_FILES["c_360_img"]["name"][0] != null) {
    $seo_img_plan3 = $_POST['seo_img_floor_paln3'][0];
    $c_360 = $_POST['c_360'];
    $numrand_img3= (mt_rand());

    //resize
                $images = $_FILES["c_360_img"]["tmp_name"][0];
                $images_360 = $_FILES["c_360_img"]["name"][0];
                $date = date('dmY');
                $type_img = strrchr($images_360,".");
                $new_images = "Thumbnails_".$numrand_img.$date.$type_img;
              
                $width=600; 
                $size=GetimageSize($images);
                $height=round($width*$size[1]/$size[0]);
                $images_orig = ImageCreateFromJPEG($images);
                $photoX = ImagesX($images_orig);
                $photoY = ImagesY($images_orig);
                $images_fin = ImageCreateTrueColor($width, $height);
                ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
                ImageJPEG($images_fin,"fileupload/images/360_thumnail/".$new_images,93);
                ImageDestroy($images_orig);
                ImageDestroy($images_fin); 

    $path_copy_img360 = "fileupload/images/360_thumnail/" .$numrand_img.$date.$type_img;
    if (move_uploaded_file($_FILES['c_360_img']['tmp_name'][0], $path_copy_img360)) {

         $sql7="INSERT INTO LH_360_FOR_PLAN (plan_id,c360_plan_name,c360_plan_url,c360_img) "
            ." VALUES ('$id_plans','$plan_name_th','$c_360','$path_copy_img360')";
        $query_r7= mssql_query($sql7 , $db_conn);
    }
}

//tvc

if($_POST['optradio']=='youtube'){
    $numrand_img3= (mt_rand());
    if ($_FILES["img_youtube"]["name"] != "" && $_FILES["img_youtube"]["name"] != null) {
        $images = $_FILES["img_youtube"]["name"];
        $date = date('dmY');
        $type_img = strrchr($images,".");
       $path_copy_img = "fileupload/images/tvc_plan/" . $numrand_img3 .$date .$type_img;
        if (move_uploaded_file($_FILES['img_youtube']['tmp_name'], $path_copy_img)) {
            //echo $newname_img;
            $url_youtube =$_POST['youtube_url'];
          echo $sql = "INSERT INTO LH_CLIP_FOR_PLAN (plan_id,clip_plan_name,clip_plan_url,thumnail,clip_type) "
                . " VALUES ('$id_plans','$plan_name_th','" . $url_youtube . "','$path_copy_img','".$_POST['optradio']."')";
            $query = mssql_query($sql);

        }
    }
}else{
    if ($_FILES["thumbnail_vdo"]["name"] != "" && $_FILES["thumbnail_vdo"]["name"] != null) {
        if ($_FILES["vdo_file"]["name"] != "" && $_FILES["vdo_file"]["name"] != null) {
            $numrand_img3= (mt_rand());
            $numrand_vdo= (mt_rand());
            $images = $_FILES["thumbnail_vdo"]["name"];
            $date = date('dmY');
            $type_img = strrchr($images,".");
            $vdo = $_FILES["vdo_file"]["name"];
            $type_vdo = strrchr($vdo,".");
            $path_copy_img = "fileupload/images/tvc_plan/" . $numrand_img3 .$date.$type_img;
            $path_copy_vdo = "fileupload/images/tvc_plan/" . $numrand_vdo . $date.$type_vdo;
            if (move_uploaded_file($_FILES['thumbnail_vdo']['tmp_name'], $path_copy_img)) {
                if (move_uploaded_file($_FILES['vdo_file']['tmp_name'], $path_copy_vdo)) {

                    $sql = "INSERT INTO LH_CLIP_FOR_PLAN (plan_id,clip_plan_name,clip_plan_url,thumnail,clip_type) "
                        . " VALUES ('$id_plans','$plan_name_th','" . $path_copy_vdo . "','$path_copy_img','".$_POST['optradio']."')";
                    $query = mssql_query($sql);

                }
            }
        }
    }
}

//galery
$dataName = (mt_rand());
if(isset($_FILES["project_galery"]["name"][0])) {
    if ($_FILES["project_galery"]["name"][0] != "" && $_FILES["project_galery"]["name"][0] != null){
        $date = date('Y-m-d');
        $numSize = count($_FILES['project_galery']["name"]);
        for ($i=0; $i < $numSize; $i++) {

            //resize
                $numrand_img = (mt_rand());
                $images = $_FILES["project_galery"]["tmp_name"][$i];
                $images2 = $_FILES["project_galery"]["name"][$i];
                $date = date('dmY');
                $date2 = date('Y-m-d');
                $type_img = strrchr($images2,".");
                $new_images = "Thumbnails_".$numrand_img.$date.$type_img;
               
                $width=600;
                $size=GetimageSize($images);
                $height=round($width*$size[1]/$size[0]);
                $images_orig = ImageCreateFromJPEG($images);
                $photoX = ImagesX($images_orig);
                $photoY = ImagesY($images_orig);
                $images_fin = ImageCreateTrueColor($width, $height);
                ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
                ImageJPEG($images_fin,"fileupload/images/galery_plan/".$new_images,93);
                ImageDestroy($images_orig);
                ImageDestroy($images_fin);   

            $pathName = "fileupload/images/galery_plan/".$numrand_img.$date.$type_img;
            $galery_seo = $_POST['upload_project_galery_seo'][$i];
            $galery_dis = $_POST['project_galery_text'][$i];
            $galery_dis_en = $_POST['project_galery_text_en'][$i];
            if(move_uploaded_file($_FILES["project_galery"]["tmp_name"][$i], $pathName)) {

                echo $sql = "INSERT INTO LH_GALERY_PLAN (plan_id,galery_plan_name,galery_plan_date,galery_plan_img_seo,galery_plan_name_dis,galery_plan_type,galery_plan_name_dis_en)
     VALUES ('$id_plans','$pathName','$date2','$galery_seo','$galery_dis','home_plan','$galery_dis_en')";
                $result = mssql_query($sql, $GLOBALS['db_conn']);
            }
        }
    }
}




    $_SESSION['add']='1';
    header("location:homemodel.php");

