<?php
session_start();
include './include/dbCon_mssql.php';
$group_id=$_SESSION['group_id'];
$zone_id=$_GET['id'];
date_default_timezone_set('Asia/Bangkok');


$_GET['page']='motivo';

if (isset($_SESSION['add'])) {
  if($_SESSION['add'] ==1){
    $success ="success";
    $_SESSION['add']='';
  } else if($_SESSION['add'] ==2){

                $error_name="error_name"; //ค่าซ้ำ
                $_SESSION['add']='';
              } else if($_SESSION['add']== -2){
                $error_img="error_img";
                $_SESSION['add']='';
              }else if($_SESSION['add']== -3){
                $error_up="error_up";
                $_SESSION['add']='';
              }
            }
            ?>
            <!DOCTYPE html>
            <html lang="en">
            <head>
              <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
              <!-- Meta, title, CSS, favicons, etc. -->
              <meta charset="utf-8">
              <meta http-equiv="X-UA-Compatible" content="IE=edge">
              <meta name="viewport" content="width=device-width, initial-scale=1">

              <title>LAND & HOUSES</title>
              <!-- Bootstrap -->
              <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
              <!-- Font Awesome -->
              <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
              <!-- NProgress -->
              <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
              <!-- iCheck -->
              <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
              <!-- bootstrap-progressbar -->
              <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
              <!-- Custom Theme Style -->
              <link href="../build/css/custom.min.css" rel="stylesheet">

              <!-- jQuery -->
              <script src="../vendors/jquery/dist/jquery.min.js"></script>

              <!-- uploade img -->
              <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
              <!--uploade-->
              <!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
              <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
              <!-- confirm-->
              <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
              <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>


              <style type="text/css">
              label.error{
               color: #e74c3c;
               display: block;
               clear: both;
               white-space: nowrap;
               text-align: right;
             }
             .form-horizontal .form-group {
               margin-right: 0;
               margin-left: 0;
               vertical-align: top;
             }
             .thumbnail {
              height: 100px;
              margin: 5px;
            }
            .fileUpload {
              position: relative;
              overflow: hidden;
              //margin: 10px;
            }
            th.next.available{
              background-color: #6f9755;
            }
            th.prev.available{
              background-color: #6f9755;
            }
            .fileUpload input.upload {
              position: absolute;
              top: 0;
              right: 0;
              margin: 0;
              padding: 0;
              font-size: 20px;
              cursor: pointer;
              opacity: 0;
              filter: alpha(opacity=0);
            }
            .file-drop-zone-title {
              padding: 0px 0px !important;
            }
            #seo_img{
              display: none;
            }
            #seo_pdf{
              display: none;
              }#dateend{
                vertical-align: top;
              }
            </style>
            <style>
            #myProgress {
              position: fixed;
              bottom: 0;
              left: 0;
              right: 0;
              top: 0;
              background: rgba(0,0,0,0.5);
              z-index: 999;
            }

            #myCenter {
              margin-top: 230px;
              color: #fff;
            }

          </style>
          <style type="text/css">
          .ui-datepicker-calendar {
            display: none;
            }​
          </style>
          <style>
          .file-caption-main .btn-file {
            overflow: visible;
          }

          .file-caption-main .btn-file .error {
            position: absolute;
            bottom: -32px;
            right: 30px;
            }.error_date{
              color: #e74c3c;
            }
          </style>

        </head>

        <body class="nav-md">
          <div class="container body">
            <div class="main_container">
              <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                  <div class="navbar nav_title" style="border: 0;">
                    <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
                  </div>

                  <div class="clearfix"></div>

                  <!-- menu profile quick info -->
                  <?php include './master/navbar.php';?>
                  <!-- /menu footer buttons -->
                </div>
              </div>

              <!-- top navigation -->
              <?php include './master/top_nav.php'; ?>
              <!-- /top navigation -->

              <!-- page content -->
              <div class="right_col" role="main">
                <div class="">
                  <div class="clearfix"></div>
                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="x_panel">
                        <div class="x_title">
                          <h2><a href="motivo.php">ระบบจัดการข้อมูล Motivo</a> > สร้างข้อมูล Motivo</h2>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                          <p class="font-gray-dark">
                          </p><br>
                          <?php if(isset($success)){?>
                          <div class="row">
                            <div class="col-md-2"></div>
                            <div class="form-group">
                              <div class="alert alert-success col-md-6" id="alert">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <strong>สร้างข้อมูลเรียบร้อยแล้ว</strong>
                              </div>
                            </div>
                            <div class="col-md-4"></div>
                          </div>
                          <?php }else if(isset($error_name)){?>
                          <div class="row">
                            <div class="col-md-2"></div>
                            <div class="form-group">
                              <div class="alert alert-danger col-md-6" id="alert">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <strong>มีชื่อนี้อยู่ในระบบแล้ว</strong>
                              </div>
                            </div>
                            <div class="col-md-4"></div>
                          </div>
                          <?php }else if(isset($error_up)){?>
                          <div class="row">
                            <div class="col-md-2"></div>
                            <div class="form-group">
                              <div class="alert alert-danger col-md-6" id="alert">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <strong>เกิดข้อผิดพลาด !</strong> ไม่สามารถบันทึกข้อมูลได้
                              </div>
                            </div>
                            <div class="col-md-4"></div>
                          </div>
                          <?php }?>
                          <form class="form-horizontal form-label-left" action="save_motivo.php" method="post" enctype="multipart/form-data" id="commentForm">
                            <div class="form-group">
                              <label class="control-label col-md-2" for="first-name">ชื่อนิตยสาร <span class="required">*</span>
                              </label>
                              <div class="col-md-6">
                                <input type="text" id="first-name2" name="motivo_name" required class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อนิตยสาร" value="">
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="control-label col-md-2" for="first-name">รายละเอียดนิตยสาร <span class="required"></span>
                              </label>
                              <div class="col-md-6">
                                <textarea name="motivo_detail" id="motivo_detail" rows="7" class="form-control" placeholder="รายละเอียดนิตยสาร" value=""></textarea>
                              </div>
                            </div>


                            <div class="form-inline">
                              <label class="control-label col-md-2" for="exampleInputEmail3">ระบุช่วงเวลาเริ่มต้น </label>
                              <div class="form-group">
                                <div class="col-md-6" >
                                  <select id="select_month1" class="form-control" name="month_start" >
                                    <option value="" >เลือกเดือน</option>
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                  </select>
                                </div>
                                <div class="col-md-4" >

                                  <select class="form-control" name="year_start" id="select_year1" >
                                    <option value=""  >เลือกปี</option>
                                  </select>
                                </div>
                              </div>
                              <label class="control-label" id="dateend" for="exampleInputPassword3">เวลาสิ้นสุด </label>
                              <div class="form-group">
                                <div class="col-md-6" >
                                  <select id="select_month2" class="form-control" name="month_end" >
                                    <option value="" >เลือกเดือน</option>
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                  </select>
                                </div>
                                <div class="col-md-4" >

                                  <select class="form-control" name="year_end" id="select_year2" >
                                    <option value="" >เลือกปี</option>
                                  </select>
                                </div>
                              </div>
                              <input type="text" name="chk_date" id="chk_date" value="รูปแบบวันที่ไม่ถูกต้อง" readonly style="border: 0px; width: 0px">
                            </div>
                            <br>


                            <div class="form-group">
                              <label class="control-label col-md-2" for="first-name">รูปหน้าปก *<br>(ขนาด 658 x 786 px)<span class="required"></span>
                              </label>
                              <div class="col-md-6">
                                <input id="images_motivi" name="images" type="file" multiple required class="file-loading" accept="image/*">
                              </div>
                            </div>

                            <script>
                              $("#images_motivi").fileinput({
                            uploadUrl: "upload.php", // server upload action
                            maxFileCount: 1,
                            allowedFileExtensions : ['jpg', 'png','gif','jpeg'],
                            browseLabel: 'เลือกรูปหน้าปก',
                            removeLabel: 'ลบ',
                            browseClass: 'btn btn-success',
                            showUpload: false,
                            showRemove:false,
                            showCaption: false,
                            msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                            msgInvalidFileExtension: 'ไฟล์ "{name}" เป็นไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์"{extensions}"',
                            msgZoomModalHeading: 'ตัวอย่างละเอียด',
                            xxx:'seo_img',
                            dropZoneTitle : 'รูปหน้าปก',
                            minImageWidth: 658, //ขนาด กว้าง ต่ำสุด
                            minImageHeight: 786, //ขนาด ศุง ต่ำสุด
                            maxImageWidth: 658, //ขนาด กว้าง ต่ำสุด
                            maxImageHeight: 786, //ขนาด ศุง ต่ำสุด
                            maxFileSize :300 ,
                            msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                            msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                            msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                            msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                            msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.', 
                          });
                        </script>

                        <div class="form-group">
                          <label class="control-label col-md-2" for="first-name">เนื้อหาไฟล์ (PDF) <span class="required">*</span>
                          </label>
                          <div class="col-md-6">
                            <input id="pdf" name="pdf" type="file" multiple  class="file-loading" required accept="application/pdf" >
                          </div>
                        </div>



                        <script>
                          $("#pdf").fileinput({
                            uploadUrl: "upload.php", // server upload action
                            maxFileCount: 1,
                            allowedFileExtensions: ["pdf"],
                            browseLabel: 'เลือกไฟล์ PDF',
                            removeLabel: 'ลบ',
                            browseClass: 'btn btn-success',
                            //maxFileSize: 1024, // max size
                            showUpload: false,
                            showRemove:false,
                            showCaption: false,
                            msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                            msgInvalidFileExtension: 'ไฟล์ "{name}" เป็นไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ "{extensions}"',
                            //msgSizeTooLarge: 'ไฟล์ "{name}" (<b>{size} KB</b>) มีขนาดเกินที่ระบบอนุญาตที่ <b>{maxSize} KB</b>, กรุณาลองใหม่อีกครั้ง!',
                            msgZoomModalHeading: 'ตัวอย่างละเอียด',
                            xxx:'seo_pdf',
                            dropZoneTitle : 'ไฟล์ PDF',
                            previewFileIconSettings: {
                              'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                            },
                          });
                            // }).on('fileuploaded', function(event, files, extra) {
                            //     console.log({size})
                            //      console.log(files)
                            //       console.log(extra)
                            // });
                          </script>


                          <div class="form-group">
                            <label class="control-label col-md-2" >สถานะ <span class="required"></span></label>
                            <div class="col-md-3">
                              <p style="padding: 5px;">
                                <input type="checkbox" name="status" id="hobby1"  value="New" data-parsley-mincheck="1"  class="flat" />  New
                              </p>
                            </div>
                          </div>


                          <center><br>
                            <div class="col-md-8">
                              <button type="submit" name="submit" class="btn btn-success" >Submit</button>
                            </div>
                          </center>
                        </form>
                        <br>
                        <br>
                        <br>
                        <br>


                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-4" > <span class="required"></span></label>
                        <div class="col-md-3">
                          <div id="myProgress">
                            <!-- <div id="myBar"></div> -->
                            <center id="myCenter"><img  src="fileupload/images/motivo/cloud_upload_256.gif" width="50" height="50">
                              <p>กำลังดำเนินการอัปโหลดไฟล์...</p></center>
                            </div>
                          </div>
                        </div>

                        <script>
                          $(document).ready(function(){
                            $("#myProgress").hide();
                          });
                        </script>


                      </div>
                    </div>
                  </div>
                </div>
                <!-- /page content -->

                <!-- footer content -->
        <!-- <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer> -->
        <!-- /footer content -->
      </div>
    </div>
  </div>

  <!-- Bootstrap -->
  <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- FastClick -->
  <script src="../vendors/fastclick/lib/fastclick.js"></script>
  <!-- NProgress -->
  <script src="../vendors/nprogress/nprogress.js"></script>
  <script type="text/javascript" src="js/bootstrap-imgupload.js"></script>
  <!-- Validate -->
  <script src="../build/js/jquery.validate.js"></script>
  <!-- iCheck -->
  <script src="../vendors/iCheck/icheck.min.js"></script>
  <!-- Custom Theme Scripts -->
  <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>
  <!-- bootstrap-progressbar -->
  <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
  <!-- Custom Theme Scripts -->
  <script src="../build/js/custom.min.js"></script>
  <script>

    $(document).ready(function() {



      $("#commentForm").validate({
        rules: {
          motivo_name: "required",

        },
        messages: {
          motivo_name: "กรุณากรอกชื่อนิตยสาร !",
          chk_date : "รูปแบบวันที่ไม่ถูกต้อง !",
        }
      });

      $("#images").rules("add", {
        required:true,
        messages: {
          required: " &nbsp; ไม่มีรูป !"
        }
      });
      $("#pdf").rules("add", {
        required:true,
        messages: {
          required: " &nbsp; ไม่มีไฟล์ !"
        }
      });

    });
  </script>

  <script type="text/javascript">
    $('.fancybox').fancybox();
  </script>
  <script type="text/javascript" src="js/bootstrap-imgupload.js"></script>

  <script>
    $(document).ready(function(){
      $("#alert").show();
      $("#alert").fadeTo(3000, 400).slideUp(500, function(){
        $("#alert").alert('close');
      });
    });
  </script>

  <script>
    $(document).ready(function(){
      $("#alert").show();
      $("#alert").fadeTo(3000, 500).slideUp(500, function(){
        $("#alert").alert('close');
      });
      $("#chk_date").hide();
    });
  </script>

  <script type="text/javascript">
    $("#commentForm").submit(function() {
      var select_month1 = $("#select_month1").val();
      var select_year1 = $("#select_year1").val();
      var select_month2 = $("#select_month2").val();
      var select_year2 = $("#select_year2").val();
      var d_start = new Date(select_year1 + "-" + select_month1 + "-" + 01);
      var d_end = new Date(select_year2 + "-" + select_month2 + "-" + 01);
      if(d_start <= d_end){
        $("#chk_date").hide();
      }else if(d_start > d_end){
        $("#chk_date").show();
        $("#chk_date").val("");
        $("#chk_date").attr('required','true');
      }
    });
  </script>

  <script type="text/javascript">
    $(document).ready(function() {
      var d = (new Date).getFullYear();
      var arr = [];
      for (var i = 0; i <= 10; i++)
      {
        arr[i] = d;
        d++;
        $("#select_year1").append( '<option value="'+arr[i]+'" id="'+arr[i]+'">'+arr[i]+'</option>' );
        $("#select_year2").append( '<option value="'+arr[i]+'" id="'+arr[i]+'">'+arr[i]+'</option>' );
      }
      $("#select_year2 ").attr('disabled', 'true');
      $("#select_month2 ").attr('disabled', 'true');
    });
  </script>

  <script>
    $('#select_month1').change(function(){
      var m_start = $("#select_month1").val();
      if(m_start != ""){
        $('#select_year1').change(function(){
          var y_start = $("#select_year1").val();
          if(y_start != ""){
           $("#select_month2").removeAttr('disabled', 'true');
           $("#select_year2 ").removeAttr('disabled', 'true');
         }
       });
      }
    });

    $('#select_year1').change(function(){
      var y_start = $("#select_year1").val();
      if(y_start != ""){
        $('#select_month1').change(function(){
          var m_start = $("#select_month1").val();
          if(m_start != ""){
           $("#select_month2").removeAttr('disabled', 'true');
           $("#select_year2 ").removeAttr('disabled', 'true');
         }
       });
      }
    });

    $('#select_month1').change(function(){
      var y_start = $("#select_year1").val();
      var y_end = $("#select_year2").val();
      var m_start = $("#select_month1").val();
      if(y_start == y_end){
        $('#select_month2 option:selected').prevAll().removeAttr('disabled','true');
        $("#select_month2").val(m_start);
        $('#select_month2 option:selected').prevAll().attr('disabled','true');
      }
    });

    $('#select_year1').change(function(){
      var y_start = $("#select_year1").val();
      $('#select_year2 option:selected').prevAll().removeAttr('disabled','true');
      $("#select_year2").val(y_start);
      var y_end = $("#select_year2").val();
      $("#select_year2 option:selected").prevAll().attr('disabled', 'true');
      if(y_start == y_end){
        $('#select_month2 option:selected').prevAll().removeAttr('disabled','true');
        var m_start = $("#select_month1").val();
        $("#select_month2").val(m_start);
        $('#select_month2 option:selected').prevAll().attr('disabled','true');
      }
    });

    $('#select_year2').change(function(){
      var y_start = $("#select_year1").val();
      var y_end = $("#select_year2").val();
      if(y_start == y_end){
        $('#select_month2 option:selected').prevAll().removeAttr('disabled','true');
        var m_start = $("#select_month1").val();
        $("#select_month2").val(m_start);
        $('#select_month2 option:selected').prevAll().attr('disabled','true');
      }else if(y_start != y_end){
       $('#select_month2 option:selected').prevAll().removeAttr('disabled','true');
     }
   });
 </script>
 <script src="js/validate_file_300kb.js"></script>


</body>
</html>