<?php
/**
 * Created by PhpStorm.
 * User: zunyru
 * Date: 21/4/2560
 * Time: 0:49
 */
    // Allowed extentions.
    $allowedExts = array("txt", "pdf", "doc","docx","zip","rar","exe","rtf","xls","xlsx","ppt","pptx");

    // Get filename.
    $temp = explode(".", $_FILES["file"]["name"]);

    // Get extension.
    $extension = end($temp);

    // Validate uploaded files.
    // Do not use $_FILES["file"]["type"] as it can be easily forged.
    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    $mime = finfo_file($finfo, $_FILES["file"]["tmp_name"]);

    if ((($mime == "text/plain")
            || ($mime == "application/msword")
            || ($mime == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
            || ($mime == "application/zip")
            || ($mime == "application/x-rar-compressed")
            || ($mime == "application/x-msdownload")
            || ($mime == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            || ($mime == "application/rtf")
            || ($mime == "application/vnd.ms-excel")
            || ($mime == "application/vnd.ms-powerpoint")
            || ($mime == "application/vnd.openxmlformats-officedocument.presentationml.presentation")
            || ($mime == "application/x-pdf")
            || ($mime == "application/pdf"))
        && in_array($extension, $allowedExts)) {
        // Generate new random name.
        $name = sha1(microtime()) . "." . $extension;

        // Save file in the uploads folder.
        move_uploaded_file($_FILES["file"]["tmp_name"],  "../build/images/file_content_web/" . $name);

        // Generate response.
        $response = new StdClass;
        $response->link = "../build/images/file_content_web/" . $name;
        echo stripslashes(json_encode($response));
    }
?>