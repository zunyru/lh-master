<?php

session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include './include/dbCon_mssql.php';
include './include/UpdateSlug.php';



date_default_timezone_set('Asia/Bangkok');
$date       = date("Ymd");
$numrand_th = (mt_rand());
$numrand_en = (mt_rand());


function mssql_escape($str)
{
 if(get_magic_quotes_gpc())
 {
     $str= stripslashes($str);
 }
 return str_replace("'", "''", $str);
}

$name_tag    = mssql_escape($_POST['name_tag']);
$url_page    = mssql_escape($_POST['url_page']);

if(UpdateSlug::notAllow($url_page)==false){
    $_SESSION['add'] = '-4';
    echo '<script>window.history.go(-1);</script>';
    exit();
}


$fileupload_th = $_FILES['fileupload_th'];

$title_seo    = mssql_escape($_POST['landing_page_title_seo_th']);
$description    = mssql_escape($_POST['landing_page_description_th']);
$keyword_seo = mssql_escape($_POST['keyword_seo_th']);

$title_seo_en    = mssql_escape($_POST['landing_page_title_seo_en']);
$description_en    = mssql_escape($_POST['landing_page_description_en']);
$keyword_seo_en = mssql_escape($_POST['keyword_seo_en']);

$h1    = mssql_escape($_POST['h1']);


$dates        = date("Y-m-d H:i:s");
$query        = "SELECT COUNT(*) AS counts FROM LH_SEO WHERE name_tag = '$name_tag'";
$query_result = mssql_query($query, $db_conn);

$count = mssql_fetch_row($query_result);
$count[0];
if ($count[0] >= 1) {
    $_SESSION['add'] = '0';
    echo '<script>window.history.go(-1);</script>';
    exit();
}

function contains($needles, $haystack) {
    return count(array_intersect($needles, explode(" ", preg_replace("/[^A-Za-z0-9' -]/", "", $haystack))));
}
$not_allow_list = array('landing-page');
$not_allow = contains($not_allow_list, $url_page);

$query        = "SELECT COUNT(*) AS counts FROM LH_SEO WHERE url_page = '$url_page'";
$query_result = mssql_query($query, $db_conn);

$count = mssql_fetch_row($query_result);
$count[0];
if ($count[0] >= 1) {
    $_SESSION['add'] = '-2';
    echo '<script>window.history.go(-1);</script>';
    exit();
}elseif($not_allow){
    $_SESSION['add'] = '-4';
    echo '<script>window.history.go(-1);</script>';
    exit();
}

$url = explode("/",$url_page);
$url_ = urldecode($url[5]);
$url_2 = urldecode($url[6]);

$sel_project = "SELECT pr.project_id FROM LH_PROJECT_SEO seo 
LEFT JOIN LH_PROJECTS pr ON seo.project_id = pr.project_id 
LEFT JOIN LH_PROJECT_SUB sub ON sub.project_id = pr.project_id
WHERE pr.project_url = '".$url_."' OR pr.project_url = '".$url_2."'" ;

$resul_check_project = mssql_query($sel_project);
$row= mssql_fetch_array($resul_check_project);
$row_= mssql_num_rows($resul_check_project);


$sql_ = "UPDATE LH_PROJECT_SEO SET project_title_seo_th = '$title_seo', ";
          $sql_ .=" project_des_seo_th = '$description', ";
          $sql_ .=" project_keyword_th = '$keyword_seo', ";
          $sql_ .=" project_title_seo_en = '$title_seo_en', ";
          $sql_ .=" project_des_seo_en = '$description_en', ";
          $sql_ .=" project_keyword_en = '$keyword_seo_en' ";

if($row_ > 0){
          $sql_ .= " WHERE project_id = '".$row['project_id']."' ";
  
}
 $resul_ = mssql_query($sql_);


$path = "fileupload/images/seo/";

$type_th = strrchr($_FILES['fileupload_th']['name'], ".");

$newname_th = $date . $numrand_th . $type_th;

$path_seo = $path . $newname_th;

if (move_uploaded_file($_FILES['fileupload_th']['tmp_name'], $path_seo)) {

    $query_2 = "INSERT INTO LH_SEO (title_seo,description_seo,keyword_seo,thumnail_seo,url_page,name_tag,create_date,update_date,h1,title_seo_en,description_seo_en,keyword_seo_en)
    VALUES ('$title_seo','$description','$keyword_seo','$path_seo','$url_page','$name_tag','$dates','$dates','$h1','$title_seo_en','$description_en','$keyword_seo_en');";

    $query_result2 = mssql_query($query_2, $db_conn);
    if ($query_result2) {
        $_SESSION['add'] = '1';
        header("location:seo_add.php");
    }
} else {
    $_SESSION['add'] = '0';
    echo '<script>window.history.go(-1);</script>';
    exit();
}
