<?php
session_start();
//include './connect_db.php';
include './include/dbCon_mssql.php';
$brand_id=$_SESSION['group_id'];
unset($_SESSION["add"]);

$_GET['page']='brand';

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>

      <!-- Bootstrap -->
      <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome -->
      <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <!-- NProgress -->
      <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
      <!-- iCheck -->
      <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
      <!-- Datatables -->
      <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
      <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

      <!-- Custom Theme Style -->
      <link href="../build/css/custom.min.css" rel="stylesheet">
      <style type="text/css">
        .placeholder {
            border: 1px solid green;
            background-color: white;
            -webkit-box-shadow: 0px 0px 10px #888;
            -moz-box-shadow: 0px 0px 10px #888;
            box-shadow: 0px 0px 10px #888;
        }
        .tile {
            height: 80px;
        }
        .grid {
            margin-top: 1em;
        }
        .hight{
          height: 80px;
        }
        span.indecator {
          padding-left: 15px;
          line-height: 78px;
          display: inline-block;
          vertical-align: middle;
        }
        span.order {
          float: right;
          line-height: 80px;
        }
        img.img_brand{
          width: auto;
          max-height: 78px;
          max-width: 80px;
          vertical-align: middle;
          display: inline-block;
        }
        .well {
          padding: 0 19px;
          cursor: move;
        }
        .ui-sortable-placeholder{
          font-size: 36px;
          line-height: 78px;
        }
      </style>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
           <?php include './master/navbar.php';?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
          <?php include './master/top_nav.php'; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>ระบบจัดการข้อมูล Brand</h2>

                    <div class="clearfix"></div>
                  </div>
                  <?php if(isset($_GET['delete'])){?>
                      <div class="row">
                              <div class="col-md-2"></div>
                               <div class="form-group">
                                <div class="alert alert-success col-md-6" id="alert">
                                  <button type="button" class="close" data-dismiss="alert">x</button>
                                  <strong>ลบข้อมูลเรียบร้อยแล้ว</strong>
                                </div>
                              </div>
                              <div class="col-md-4"></div>
                             </div>
                  <?php }?>
                  <?php if(@$_SESSION['update']==1){
                      unset($_SESSION["update"]);
                    ?>
                      <div class="row">
                              <div class="col-md-2"></div>
                               <div class="form-group">
                                <div class="alert alert-success col-md-6" id="alert">
                                  <button type="button" class="close" data-dismiss="alert">x</button>
                                  <strong>แก้ไขข้อมูลเรียบร้อยแล้ว</strong>
                                </div>
                              </div>
                              <div class="col-md-4"></div>
                             </div>
                  <?php }?>
                  <div class="x_content">

                   <div class="" role="tabpanel" data-example-id="togglable-tabs">
                  <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab_content1" role="tab" id="Brand" data-toggle="tab" aria-expanded="false">Show Brand</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content2" id="Ordering"  data-toggle="tab" aria-expanded="false">Brand Ordering</a>
                        </li>
                      </ul>

                    <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="Project">
                    <a href="brand_add.php"><button type="button" class="btn btn-success flright">สร้างข้อมูล Brand +</button></a>
                    <!-- <p class="text-muted font-13 m-b-30">
                      DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>
                    </p> -->
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                            <th><center>ชื่อ Brand </center></th>
                            <th width="400px;"><center>Brand Concept</center></th>
                            <th><center>Brand Logo</center></th>
                            <th width="100px;"><center>ลำดับความสำคัญ</center></th>
                            <th width="100px;"><center>อัพเดตข้อมูลล่าสุดวันที่</center></th>
                        </tr>
                      </thead>
                       <tbody>
                      <?php
                             require_once './include/function_date.php';
                             include_once('./include/dbBrands.php');
                             // $SQL="SELECT * FROM dbo.LH_BRANDS";
                             // $stmt = $conn->query( $SQL );
                             //$row_count = $stmt->rowCount();
                             $sql="SELECT * FROM LH_BRANDS order by order_seq asc";
                             $stmt= mssql_query($sql , $db_conn);

                            if($stmt){
                             while ($row =mssql_fetch_array($stmt)){
                                  //print_r($row);

                                 $date=date_create($row['brand_update']);
                                 $strDate =date_format($date,"Y-m-d H:i:s");
                                 // $year = substr($date, 0, 4);
                                 // $mont = substr($date, 5, 2);
                                 // $day = substr($date, 8, 2);
                                 // $strDate= $year."-".$mont."-".$day;

                         ?>


                        <tr>
                          <td><a href="brand_update.php?id=<?=$row['brand_id']?>"><?=$row['brand_name_th'];  ?></a></td>
                          <td><a href="brand_update.php?id=<?=$row['brand_id']?>">
                          <?=$row['brand_concep_th']; ?>
                          </a></td>
                          <td><a href="brand_update.php?id=<?=$row['brand_id']?>"><center>
                            <?php if(($row['logo_brand_th']!='no_img')||($row['logo_brand_en']!='no_img')){ echo 'Y';}else{ echo 'N';} ?>
                          </center></a></td>
                            <td></p>
                                <a href="brand_update.php?id=<?=$row['brand_id']?>">
                                    <center><?=$row['order_seq']+1?></center></a></td>
                          <td><p style="display: none"><?=$strDate?></p>
                              <a href="brand_update.php?id=<?=$row['brand_id']?>"><center>
                                      <?=DateThai_time($row['brand_update']);?></center></a></td>

                        </tr>
                      <?php }} ?>

                      </tbody>
                    </table><br><br>
                  </div>
                     <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="Ordering">
                      <div class="form-group">
                        <div class="col-md-9" >

                        </div>
                      </div>
                      <div class="x_content">                 
                      <form action="brand_submit.php" method="post">
                  <div class="row grid span8">
                        <?php
                             require_once './include/function_date.php';
                             include_once('./include/dbBrands.php');
                             // $SQL="SELECT * FROM dbo.LH_BRANDS";
                             // $stmt = $conn->query( $SQL );
                             //$row_count = $stmt->rowCount();
                             $sql="SELECT * FROM LH_BRANDS order by order_seq asc";
                             $stmt= mssql_query($sql , $db_conn);
                            $i=1;
                            if($stmt){
                             while ($row =mssql_fetch_array($stmt)){
                                  //print_r($row);

                                 $date=date_create($row['brand_update']);
                                 $strDate =date_format($date,"Y-m-d H:i:s");
                                 // $year = substr($date, 0, 4);
                                 // $mont = substr($date, 5, 2);
                                 // $day = substr($date, 8, 2);
                                 // $strDate= $year."-".$mont."-".$day;
                         ?>
                          <div class="well span2 tile height" >
                            <input type="hidden" name="brand_id[]" value="<?=$row['brand_id']?>">
                            <input type="hidden" name="brand_oder[]" value='<?=$row['logo_brand_th'];?>'>
                            <input type="hidden" name="brand_order[]" value='<?=$i?>'>
                            <img class="img_brand" src="<?='fileupload/images/brand_img/'.$row['logo_brand_th']; ?>" width="80">
                            <span class='indecator'><?=$row['brand_name_th'];?> </span>
                            <span class='order'>ลำดับความสำคัญที่ <?=$i?></span>
                          </div>

                      <?php  $i++;}

                      } ?>
                      </div>
                      <!-- <input type="submit" value="xxxx"> -->


                       <div class="form-group">
                    <label class="control-label col-md-3" for="first-name">
                    </label>
                    <div class="col-md-6 col-md-offset-2">
                          <button type="submit" class="btn btn-success">Update</button>
                    </div>
                  </div>
                  </form>
                        
                      </div>

                
                <br><br>
                    </div>

                </div>
                </div>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Custom Theme Scripts -->

    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <script src="../build/js/custom.min.js"></script>
     <script  src="js/jqueryui/jquery-ui.js"></script>

    <!--      <link href="../build/css/custom_copy.min.css" rel="stylesheet">-->
<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
    <script>
        $(document).ready(function(){
            $("#alert").show();
            $("#alert").fadeTo(3000, 400).slideUp(500, function(){
                $("#alert").alert('close');
            });
        });
    </script>

    <!-- Datatables -->
    <script>
        $(document).ready(function() {
            var handleDataTableButtons = function() {
                if ($("#datatable-buttons").length) {
                    $("#datatable-buttons").DataTable({
                        dom: "Bfrtip",
                        buttons: [
                            {
                                extend: "copy",
                                className: "btn-sm"
                            },
                            {
                                extend: "csv",
                                className: "btn-sm"
                            },
                            {
                                extend: "excel",
                                className: "btn-sm"
                            },
                            {
                                extend: "pdfHtml5",
                                className: "btn-sm"
                            },
                            {
                                extend: "print",
                                className: "btn-sm"
                            },
                        ],
                        responsive: true
                    });
                }
            };

            TableManageButtons = function() {
                "use strict";
                return {
                    init: function() {
                        handleDataTableButtons();
                    }
                };
            }();

            $('#datatable').dataTable(
                {
                    "order": [[ 3, 'asc' ]],
                    "bLengthChange": false,
                    "pageLength": 50
                });

            $('#datatable-keytable').DataTable({
                keys: true
            });

            $('#datatable-responsive').DataTable();

            $('#datatable-scroller').DataTable({
                ajax: "js/datatables/json/scroller-demo.json",
                deferRender: true,
                scrollY: 380,
                scrollCollapse: true,
                scroller: true
            });

            $('#datatable-fixed-header').DataTable({
                fixedHeader: true
            });

            var $datatable = $('#datatable-checkbox');


            $datatable.dataTable({


            });
            $datatable.on('draw.dt', function() {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_flat-green'
                });
            });

            TableManageButtons.init();
        });
    </script>
    <!-- /Datatables -->

    <script type="text/javascript">
      $("#sortable").sortable({
      cancel: ".fixed"
      });
    $("#sortable").disableSelection();
    </script>
     <script type="text/javascript">

      $(function () {
        $(".grid").sortable({
            helper: 'clone',
            tolerance: 'pointer',
            revert: 'invalid',
            placeholder: 'span2 well placeholder tile',
            forceHelperSize: true,
            sort: function(e, ui) {
              $(ui.placeholder).html(Number($(".grid > div:visible").index(ui.placeholder)) + 1);
            },
             update: function(event, ui) {
                var $lis = $(this).children('div');
                $lis.each(function() {
                var $li = $(this);
                var newVal = $(this).index() + 1;
                $(this).children('.order').html("ลำดับความสำคัญที่ "+newVal);
              });
            }
        });
      });
    </script>
  </body>
</html>