<?php

require_once  'include/dbCon_mssql.php';

$zone_id    = $_POST['zone_id'];

if(!empty($zone_id)){

    $sql = "SELECT lh_pro.PROVINCE_ID province_id ,lh_pro.PROVINCE_NAME province_name,lh_pro.PROVINCE_NAME_ENG province_name_eng
            FROM LH_ZONES lh_zone
            JOIN LH_PROVINCES lh_pro ON lh_zone.province_id = lh_pro.PROVINCE_ID
            WHERE lh_zone.zone_id ='".$zone_id."'";
    $result = mssql_query($sql , $GLOBALS['db_conn']);
    $result = mssql_fetch_object($result);
    $response['provinces'] = $result;

    responseData($response);
}

function responseData($objArr)
{
    header('Content-Type: application/json');
    echo json_encode($objArr);
}