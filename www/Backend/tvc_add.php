<?php
session_start();
include './connect_db.php';
include './include/dbCon_mssql.php';
$group_id=$_SESSION['group_id'];

$_GET['page']='tvc';
if (isset($_SESSION['add'])) {
 if($_SESSION['add'] ==1){
             //header("location:product_add.php");
  $success ="success";
  unset($_SESSION["add"]);
} else if($_SESSION['add'] ==2){

            $yum="yum"; //ค่าซ้ำ
            $_SESSION['add']='';

          } else if($_SESSION['add'] ==-1){
            $error="error";
            unset($_SESSION["add"]);
          }else if($_SESSION['add']== -2){
            $error_360="error_360";
            $_SESSION['add']='';
          }
        }

        ?>
        <!DOCTYPE html>
        <html lang="en">
        <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <!-- Meta, title, CSS, favicons, etc. -->
          <meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1">

          <title>LAND & HOUSES</title>


          <!-- Bootstrap -->
          <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
          <!-- Font Awesome -->
          <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
          <!-- NProgress -->
          <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
          <!-- iCheck -->
          <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
          <!-- bootstrap-progressbar -->
          <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
          <!-- Custom Theme Style -->
          <link href="../build/css/custom.min.css" rel="stylesheet">

          <!-- jQuery -->
          <script src="../vendors/jquery/dist/jquery.min.js"></script>

          <!-- uploade img -->
          <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
          <!--uploade-->
          <!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
          <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
          <!-- Fancybox image popup -->
          <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
          <!-- confirm-->
          <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
          <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>

          <script>
            $(document).ready(function(){
              $("#alert").show();
              $("#alert").fadeTo(3000, 400).slideUp(500, function(){
                $("#alert").alert('close');
              });
            });
          </script>
          <style type="text/css">
          th.next.available{
            background-color: #6f9755;
          }
          th.prev.available{
            background-color: #6f9755;
          }
          .file-caption-main .btn-file {
           overflow: visible;
         }

         .file-caption-main .btn-file .error {
           position: absolute;
           bottom: -32px;
           right: 30px;
         }
         .btn-hidden{
          background: #fff;
        }
        .form-control[readonly] { /* For Firefox */
         background-color: white;
       }

       .form-control[readonly] {
         background-color: white;
       }
       .test{
        overflow: hidden;
        height: 0;
      }

    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <!-- sidebar menu -->
            <?php include './master/navbar.php';?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include './master/top_nav.php'; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><a href="tvc.php">ข้อมูล TVC</a> > <a href="tvc_add.php">สร้างข้อมูล TVC</a></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="font-gray-dark">
                    </p><br>
                    <?php if(isset($success)){?>
                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="form-group">
                        <div class="alert alert-success col-md-6" id="alert">
                          <button type="button" class="close" data-dismiss="alert">x</button>
                          <strong>สร้างข้อมูลเรียบร้อยแล้ว</strong>
                        </div>
                      </div>
                      <div class="col-md-6"></div>
                    </div>
                    <?php }else if(isset($yum)){?>
                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="form-group">
                        <div class="alert alert-danger col-md-6" id="alert">
                          <button type="button" class="close" data-dismiss="alert">x</button>
                          <strong>มีชื่อนี้อยู่ในระบบแล้ว</strong>
                        </div>
                      </div>
                      <div class="col-md-4"></div>
                    </div>
                    <?php }else if(isset($error)){?>

                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="form-group">
                        <div class="alert alert-danger col-md-6" id="alert">
                          <button type="button" class="close" data-dismiss="alert">x</button>
                          <strong>เกิดข้อผิดพลาด !</strong> ไม่สามารถบันทึกข้อมูลได้
                        </div>
                      </div>
                      <div class="col-md-4"></div>
                    </div>
                    <?php }?>
                    <form class="form-horizontal form-label-left" action="save_tvc.php" method="post" id="commentForm" enctype="multipart/form-data">
                      <div class="form-group">
                        <label class="control-label col-md-3" for="first-name">ชื่อ TVC <span class="required">*</span>
                        </label>
                        <div class="col-md-6">
                          <input type="text" name="tvc_name" class="form-control col-md-7 col-xs-12"  placeholder="กรอกชื่อ TVC"  value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3" name="tvc_detail" for="first-name">รายละเอียด <span class="required">*</span>
                        </label>
                        <div class="col-md-6">
                         <div class="control-group">
                           <textarea   class="form-control" name="tvc_detail" rows="7" placeholder="รายละเอียด"></textarea>
                         </div>
                       </div>
                     </div>
                     <div class="test">
                      <div class="form-group">
                        <label class="control-label col-md-11" for="first-name">&nbsp; <span class="required"></span>
                        </label>
                        <div class="col-md-1">
                          <input id="logo_series" name="logo_series" type="file" multiple class="file-loading" accept="image/*" >
                        </div>
                      </div>

                      <script>
                        $("#logo_series").fileinput({
                             // uploadUrl: "upload.php", // server upload action
                             maxFileCount: 1,
                             allowedFileExtensions: ["png"],
                             browseLabel: '&nbsp;',
                             removeLabel: 'ลบ',
                             browseClass: 'btn btn-hidden',
                             showUpload: false,
                             showRemove:false,
                             showCaption: false,
                             msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                             msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                             msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                             msgZoomModalHeading: 'ตัวอย่างละเอียด',
                             xxx:'seo_logo_series',
                             dropZoneTitle : 'รูปโลโก้สไตล์บ้าน',
                              minImageWidth: 150, //ขนาด กว้าง ต่ำสุด
                              minImageHeight: 150, //ขนาด ศุง ต่ำสุด
                              maxImageWidth: 150, //ขนาด กว้าง ต่ำสุด
                              maxImageHeight: 150, //ขนาด ศุง ต่ำสุด
                              msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                              msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                              msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                              msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                            });
                          </script>
                        </div>

                        <div class="form-group" id="inline_content">
                          <label class="control-label col-md-3" name="tvc_detail" for="first-name">เลือกชนิดการอัปไฟล์ <span class="required"></span>
                          </label>
                          <div class="col-md-6">
                            <div class="radio">
                              <label><input type="radio" id="youtube" name="optradio_vdo" checked  value="youtube" class="flat"> VDO Youtube</label>
                            </div>
                            <div class="radio">
                              <label><input type="radio" id="file" name="optradio_vdo" value="vdo" class="flat"> VDO File</label>
                            </div>
                          </div>
                        </div>

                        <div id="l1">
                          <div class="form-group">
                            <label class="control-label col-md-3" for="first-name">URL Youtube <span class="required"> *</span>
                            </label>
                            <div class="col-md-6">
                             <div class="control-group">
                              <input type="text" name="url_youtube" class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube" value="">
                            </div>
                          </div>
                        </div>

                        <div class="form-group">
                         <label class="control-label col-md-3" for="first-name">Thumbnail Youtube <br>(.jpg , .png , .gif <br> ขนาด 1920 x 1080 px) <br> ภาพขนาดไม่เกิน 300 KB <span class="required"> *</span>
                         </label>
                         <div class="col-md-6">
                           <div class="control-group">
                             <input type="file" name="img_youtube" id="img_youtube" class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube" value="" accept="image/*">
                           </div>
                         </div>
                       </div>


                       <script>
                        $("#img_youtube").fileinput({
                              uploadUrl: "upload.php", // server upload action
                              maxFileCount: 1,
                              allowedFileExtensions: ["jpg", "png", "jpeg","gif"],
                              browseLabel: 'เลือกรูป',
                              removeLabel: 'ลบ',
                              browseClass: 'btn btn-success',
                              showUpload: false,
                              showRemove:false,
                              showCaption: false,
                              msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                              msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                              msgZoomModalHeading: 'ตัวอย่างละเอียด',
                              xxx:'seo_img_youtube',
                              dropZoneTitle : 'รูปภาพ Thumbnail Youtube',
                              minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                              minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                              maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                              maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                              maxFileSize :300 ,
                              msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                              msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                              msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                              msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                              msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                            });
                          </script>
                        </div>

                        <div id="l2">
                          <div class="form-group">
                            <label class="control-label col-md-3" for="first-name">Thumbnail VDO <br>(.jpg , .png , .gif <br> ขนาด 1920 x 1080 px) <br> ภาพขนาดไม่เกิน 300 KB<span class="required">*</span>
                            </label>
                            <div class="col-md-6">
                              <div class="control-group">
                                <input type="file" name="thumbnail_vdo" id="thumbnail_vdo" class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube" value="" accept="image/*">
                              </div>
                            </div>
                          </div>

                          <script>
                            $("#thumbnail_vdo").fileinput({
                                  uploadUrl: "upload.php", // server upload action
                                  maxFileCount: 1,
                                  allowedFileExtensions: ["jpg", "png", "jpeg"],
                                  browseLabel: 'เลือกรูป',
                                  removeLabel: 'ลบ',
                                  browseClass: 'btn btn-success',
                                  showUpload: false,
                                  showRemove:false,
                                  showCaption: false,
                                  msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                  msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                  msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                  xxx:'seo_thumbnail_vdo',
                                  dropZoneTitle : 'รูปภาพ Thumbnail VDO',
                                  minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                  minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                  maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                  maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                  maxFileSize :300 ,
                                  msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                  msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                  msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                  msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                  msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                });
                              </script>

                              <div class="form-group">
                                <label class="control-label col-md-3" for="first-name"> VDO<span class="required"> *</span>
                                </label>
                                <div class="col-md-6">
                                  <div class="control-group">
                                    <input type="file" name="vdo_file" id="vdo_file" class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube" value="" accept="video/mp4">
                                  </div>
                                </div>
                              </div>

                              <script>
                                $("#vdo_file").fileinput({
                                  uploadUrl: "upload.php", // server upload action
                                  maxFileCount: 1,
                                  allowedFileExtensions: ["mp4"],
                                  browseLabel: 'เลือกไฟล์',
                                  removeLabel: 'ลบ',
                                  browseClass: 'btn btn-success',
                                  showUpload: false,
                                  showRemove:false,
                                  showCaption: false,
                                  msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                  msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                  msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                  xxx:'seo_lead_img_file',
                                  dropZoneTitle : 'File VDO',
                                });
                              </script>
                            </div>

                            <div class="form-group">
                              <label class="control-label col-md-3" for="first-name">Link URL <span class="required"></span>
                              </label>
                              <div class="col-md-6">
                                <div class="control-group">
                                  <input type="text" name="tvc_url" class="form-control col-md-7 col-xs-12"  placeholder="Link URL" value="">
                                </div>
                              </div>
                            </div>


                            <div class="form-group">
                              <label class="control-label col-md-3" for="first-name">วันที่จะเผยแพร่ <span class="required">*</span>
                              </label>
                              <div class="col-md-3">
                               <div class="control-group">
                                 <div class="controls">

                                   <input  type="text" class="form-control has-feedback-left active" id="single_cal2" placeholder="วันที่จะเผยแพร่" aria-describedby="inputSuccess2Status2" name="tvc_public_date" readonly>
                                   <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>

                                 </div>
                               </div>
                             </div>
                           </div>

                           <br><br>
                           <div class="form-group">
                            <label class="control-label col-md-3" for="first-name"><span class="required"></span>
                            </label>
                            <div class="col-md-6">
                             <div class="control-group">
                              <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                          </div>
                        </div>



                      </form>
                      <br>
                      <br>
                      <br>
                      <br>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /page content -->

              <!-- footer content -->
        <!-- <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer> -->
        <!-- /footer content -->
      </div>
    </div>

    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="js/bootstrap-imgupload.js"></script>
    <!-- Validate -->
    <script src="../build/js/jquery.validate.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

    <!-- date -->
    <link rel="stylesheet" type="text/css" href="../build/css/jquery-ui.css"/>
    <script src="../build/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

    <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>


    <script type="text/javascript">
      $('.fancybox').fancybox();
    </script>

    <script>
      $(document).ready(function() {

        $("#commentForm").validate({
          rules: {
            tvc_name: "required",
            tvc_detail: "required",
            thumbnail : "required",
            img_youtube : "required",
            vdo_file : "required",
            thumbnail_vdo : "required",
            tvc_public_date : "required",
            url_youtube : "required",
          },
          messages: {
            tvc_name: "กรุณากรอกชื่อ TVC !",
            tvc_detail : "กรุณากรอกรายละเอียด TVC !",
            thumbnail : " &nbsp; กรุณาเลือกรูป !",
            img_youtube : " &nbsp; กรุณาเลือกรูป !",
            vdo_file : " &nbsp; กรุณาเลือกไฟล์ !",
            thumbnail_vdo : " &nbsp; กรุณาเลือกรูป !",
            tvc_public_date : "กรุณาเลือกวันที่เผยแพร่ !",
            url_youtube : "กรุณากรอก url youtube !",
          }
        });

        $("#lead_img_file").rules("add", {
          required:true,
          messages: {
            required: " &nbsp; ไม่มีรูป !"
          }
        });

      });
    </script>
    <script>
      $(document).ready(function(){
        $("#l1").show();
        $("#l2").hide();
      });
    </script>

    <script type="text/javascript">
      $('#single_cal2').datepicker({
        autoclose: true,
        changeMonth: true,
        showDropdowns: true,
        changeYear: true,
            //minDate:  new Date(),
            onSelect: function(dateText) {
              var d = new Date($(this).datepicker("getDate"))

            }
          });
        </script>

        <script >
          $("#youtube").on('ifChecked', function(event){
            $("#l1").show();
            $("#l2").hide();
          });
          $("#file").on('ifChecked', function(event){
            $("#l1").hide();
            $("#l2").show();
          });

        </script>
        <script src="js/validate_file_300kb.js"></script>

      </body>
      </html>