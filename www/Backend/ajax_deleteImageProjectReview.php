<?php
require_once 'include/dbConnect.php';

	try {
		$project_review_id = $_GET['project_review_id'];

		$conn = (new dbConnect())->getConn();
		$sql = "UPDATE LH_PROJECT_REVIEW SET project_review_img = ''
				WHERE project_review_id =".$project_review_id;
        $result= $conn->query($sql);

		echo json_encode($result->fetchAll());

	} catch (\Exception $e) {
		return $e->getMessage();
	}
?>