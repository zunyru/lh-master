function addFloorPlan()
{


    var datenow = new Date ;
    var   dformat = [ datenow.getFullYear(),
            (datenow.getMonth()+1).padLeft(),
            datenow.getDate().padLeft()].join('')+
        '' +
        [ datenow.getHours().padLeft(),
            datenow.getMinutes().padLeft(),
            datenow.getSeconds().padLeft()].join('');

    var initImgeFloorPlan = "floorImgPlan_"+dformat+"";


    var form = '<div>'+
        '<div class="form-group">'+
        '<label class="control-label col-md-2">Floor Plan <br>(.JPG ขนาด AxB) </label><br>'+
        '<div class="col-md-4">'+
        '<input type="file" id="'+initImgeFloorPlan+'" name="floorr_plan_img_name[]" class="floorr_plan_img_name"/>'+
        '</div>'+
        '</div>'+
        '<div class="form-group">'+
        '<label class="control-label col-md-2">คำอธิบาย </label>'+
        '<div class="col-md-4">'+
        '<input type="text"  name="master_plan_floor_plan_dis_th[]" class="form-control col-md-7 col-xs-12"  placeholder="กรอกคำอธิบาย (ไทย)">'+
        '</div>'+
        '<div class="col-md-4">'+
        '<input type="text" name="master_plan_floor_plan_dis_en[]" class="form-control col-md-7 col-xs-12" placeholder="กรอกคำอธิบาย (อังกฤษ)">'+
        '</div>'+
        '</div>'+
        '<br>'+
        '<div class="form-group">'+
        '<label class="control-label col-md-1">'+ '</label>'+
        '<div class="col-md-2">'+
        '<button type="button" onclick="removeFloorPlan(this)" class="btn btn-round btn-default">+ Delete Floor Plan </button>'+
        '</div>'+
        '</div>'+
        '</div>';

    $('#floorplan').append(form);


    $("#"+initImgeFloorPlan).fileinput({
        uploadUrl: "upload.php",
        dropZoneTitle : 'ลากและวางภาพ',
        browseLabel: 'เลือกภาพ',
        removeLabel: 'ลบ',
        browseClass: 'btn btn-success',
        showUpload: false,
        maxFileCount: 1,
        mainClass: "input-group-md",
        xxx:'floor_plan_img_seo',
        allowedFileTypes: ["image"]
    });

}