  $(function() {
            $.FroalaEditor.DefineIcon("imageInfo", { NAME: "info" });
            $.FroalaEditor.RegisterCommand("imageInfo", {
                title: "Info",
                focus: false,
                undo: false,
                refreshAfterCallback: false,
                callback: function() {
                    var $img = this.image.get();
                    alert($img.attr("src"));
                }
            });

            $('#editTh').froalaEditor({
                toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'fontFamily', 'fontSize', '|', 'specialCharacters', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', '-', '|', 'undo', 'redo', 'clearFormatting', 'selectAll', 'html', 'applyFormat', 'removeFormat', 'fullscreen', 'print'],    
                height: 200,
                imageUploadURL: 'uploade_highlights.php',
                imageUploadParams: {
                    id: 'edit'
                },
                imageManagerLoadURL: 'images_load.php',

                fileUploadURL: 'upload_file.php',
                fileUploadParams: {
                    id: 'edit'
                },
                fontFamily: {
                    "LHfont": 'Kittithada',
                    "Roboto,sans-serif": 'Roboto',
                    "Oswald,sans-serif": 'Oswald',
                    "Montserrat,sans-serif": 'Montserrat',
                    "'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
                },

                imageManagerDeleteURL: "delete_image_highlights.php",
                imageManagerDeleteMethod: "POST"
            })
                // Catch image removal from the editor.
                .on('froalaEditor.image.removed', function (e, editor, $img) {
                    $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_image_highlights.php",

                            // Request params.
                            data: {
                                src: $img.attr('src')
                            }
                        })
                    .done (function (data) {
                        console.log ('image was deleted'+$img.attr('src'));
                    })
                    .fail (function (err) {
                        console.log ('image delete problem: ' + JSON.stringify(err));
                    })
                })

                    // Catch image removal from the editor.
                    .on('froalaEditor.file.unlink', function (e, editor, link) {

                        $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_file.php",

                            // Request params.
                            data: {
                                src: link.getAttribute('href')
                            }
                        })
                        .done (function (data) {
                            console.log ('file was deleted');
                        })
                        .fail (function (err) {
                            console.log ('file delete problem: ' + JSON.stringify(err));
                        })
                    });

                //
                $('#editEn').froalaEditor({
                    toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'fontFamily', 'fontSize', '|', 'specialCharacters', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', '-', '|', 'undo', 'redo', 'clearFormatting', 'selectAll', 'html', 'applyFormat', 'removeFormat', 'fullscreen', 'print'],    
                    height: 200,
                    imageUploadURL: 'uploade_highlights.php',
                    imageUploadParams: {
                        id: 'edit'
                    },
                    imageManagerLoadURL: 'images_load.php',

                    fileUploadURL: 'upload_file.php',
                    fileUploadParams: {
                        id: 'edit'
                    },
                    fontFamily: {
                        "LHfont": 'Kittithada',
                        "Roboto,sans-serif": 'Roboto',
                        "Oswald,sans-serif": 'Oswald',
                        "Montserrat,sans-serif": 'Montserrat',
                        "'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
                    },

                    imageManagerDeleteURL: "delete_image_highlights.php",
                    imageManagerDeleteMethod: "POST"
                })
                // Catch image removal from the editor.
                .on('froalaEditor.image.removed', function (e, editor, $img) {
                    $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_image_highlights.php",

                            // Request params.
                            data: {
                                src: $img.attr('src')
                            }
                        })
                    .done (function (data) {
                        console.log ('image was deleted'+$img.attr('src'));
                    })
                    .fail (function (err) {
                        console.log ('image delete problem: ' + JSON.stringify(err));
                    })
                })

                    // Catch image removal from the editor.
                    .on('froalaEditor.file.unlink', function (e, editor, link) {

                        $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_file.php",

                            // Request params.
                            data: {
                                src: link.getAttribute('href')
                            }
                        })
                        .done (function (data) {
                            console.log ('file was deleted');
                        })
                        .fail (function (err) {
                            console.log ('file delete problem: ' + JSON.stringify(err));
                        })
                    });
                });

        //////////////////////////////

        $('#startdate').datepicker(
        {
            dateFormat : "dd/mm/yy",
            autoclose : true,
            changeMonth : true,
            showDropdowns : true,
            changeYear : true,
            minDate : new Date(),
            onSelect : function(dateText) {
                var d = new Date($(this).datepicker(
                    "getDate"));
                $("input#enddate").datepicker('option',
                    'minDate', dateText);
                $("input#enddate").prop('disabled', false);
            }
        });

        $('#enddate').datepicker(
        {
            dateFormat : "dd/mm/yy",
            autoclose : true,
            changeMonth : true,
            showDropdowns : true,
            changeYear : true,
            minDate : new Date(),
            onSelect : function(dateText) {
                var d = new Date($(this).datepicker(
                    "getDate"))
                $("input#startdate").datepicker('option',
                    'maxDate', dateText);
            }
        });

        ////
        var elem = document.querySelector('.color-input');
        var hueb = new Huebee( elem, {
             notation: 'hex'
            // options
        });

         /////////////////
        $("#activityimg").fileinput({
            uploadUrl : "save_lhbanner.php", // server
            // upload
            // action
            maxFileCount : 1,
            allowedFileExtensions : [ "jpg", "png","jpeg", "gif" ],
            browseLabel : 'เลือกรูป',
            removeLabel : 'ลบ',
            browseClass : 'btn btn-success',
            showUpload : false,
            showRemove : false,
            showCaption : false,
            autoReplace: false,
            msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
            msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
            msgZoomModalHeading : 'ตัวอย่างละเอียด',
            dropZoneTitle : 'Banner Activity Image',
            xxx:'seo',
            maxFileSize : 500 ,
            minImageWidth: 1920, // ขนาด กว้าง
            // ต่ำสุด
            minImageHeight: 1080, // ขนาด ศุง
            // ต่ำสุด
            maxImageWidth: 1920, // ขนาด กว้าง
            // ต่ำสุด
            maxImageHeight: 1080, // ขนาด ศุง
            // ต่ำสุด
            msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
            msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
            msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
            msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
        });

        $("#thumbnailYoutube")
        .fileinput(
        {
                    uploadUrl : "save_lhbanner.php", // server
                    // upload
                    // action
                    maxFileCount : 1,
                    allowedFileExtensions : [ "jpg", "png","jpeg", "gif" ],
                    browseLabel : 'เลือกรูป',
                    removeLabel : 'ลบ',
                    browseClass : 'btn btn-success',
                    showUpload : false,
                    showRemove : false,
                    showCaption : false,
                    autoReplace: false,
                    msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                    msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                    msgZoomModalHeading : 'ตัวอย่างละเอียด',
                    dropZoneTitle : 'รูปภาพ Thumbnail Youtube',
                    xxx:'seo',
                    maxFileSize : 500 ,
                    minImageWidth: 1920, // ขนาด กว้าง
                    // ต่ำสุด
                    minImageHeight: 1080, // ขนาด ศุง
                    // ต่ำสุด
                    maxImageWidth: 1920, // ขนาด กว้าง
                    // ต่ำสุด
                    maxImageHeight: 1080, // ขนาด ศุง
                    // ต่ำสุด
                    msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                    msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                    msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                    msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                });

        $("#vdofile")
        .fileinput(
        {
                    uploadUrl : "save_lhbanner.php", // server
                    // upload
                    // action
                    maxFileCount : 1,
                    allowedFileExtensions : [ "mp4" ],
                    browseLabel : 'เลือกไฟล์',
                    removeLabel : 'ลบ',
                    browseClass : 'btn btn-success',
                    showUpload : false,
                    showRemove : false,
                    showCaption : false,
                    autoReplace: false,
                    maxFileSize : 204800 , // 200 MB
                    msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                    msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                    msgZoomModalHeading : 'ตัวอย่างละเอียด',
                    xxx:'seo',
                    dropZoneTitle : 'File VDO',
                    msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                    msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                    msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                    msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                });

        $("#thumbnailVDO")
        .fileinput(
        {
                    uploadUrl : "save_lhbanner.php", // server
                    // upload
                    // action
                    maxFileCount : 1,
                    allowedFileExtensions : [ "jpg", "png","jpeg", "gif" ],
                    browseLabel : 'เลือกรูป',
                    removeLabel : 'ลบ',
                    browseClass : 'btn btn-success',
                    showUpload : false,
                    showRemove : false,
                    showCaption : false,
                    autoReplace: false,
                    msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                    msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                    msgZoomModalHeading : 'ตัวอย่างละเอียด',
                    dropZoneTitle : 'รูปภาพ Thumbnail VDO',
                    xxx:'seo',
                    maxFileSize : 500 ,
                    minImageWidth: 1920, // ขนาด กว้าง
                    // ต่ำสุด
                    minImageHeight: 1080, // ขนาด ศุง
                    // ต่ำสุด
                    maxImageWidth: 1920, // ขนาด กว้าง
                    // ต่ำสุด
                    maxImageHeight: 1080, // ขนาด ศุง
                    // ต่ำสุด
                    msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                    msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                    msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                    msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                });

        //add_lead_image mobile
        function add_lead_image()
        {

            var index = $('.setIndexProject_galery_galley').length;

            var form = `<div><div class="col-sm-3">
            <div class="form-group setIndexProject_galery">
            <div class="col-md-12">
            <input type="file" id="lead_image_m${index + 1}" class="lead_image" name="lead_image_m[]" accept="image/*" />
            </div>
            </div>

            <button  type="button" class="btn btn-round btn-danger" style="margin-left: 112px;" onclick="removeLead_image(this)"> - ลบ Banner Mobile</button>

            </div>
            </div>`;

            $('#lead_image_project_m').append(form);

            $(".lead_image").fileinput({
                uploadUrl: "upload.php", // server upload action
                maxFileCount: 1,
                allowedFileExtensions: ["jpg", "png", "jpeg"],
                browseLabel: ' เลือก Banner Mobile',
                browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                removeLabel: 'ลบ',
                browseClass: 'btn btn-success',
                showUpload: false,
                showRemove:false,
                showCaption: false,
                msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                msgZoomModalHeading: 'ตัวอย่างละเอียด',
                xxx:'seo',
                minImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
                minImageHeight: 1920, // ขนาด ศุง ต่ำสุด
                maxImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
                maxImageHeight: 1920, // ขนาด ศุง ต่ำสุด
                dropZoneTitle : 'Banner Mobile',
                msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
            });
        }

        function removeLead_image(element)
        {
            //$('#gallery_add').prop("disabled", false);
            $(element).parent().remove();
        }

        //
        //add_lead_image d
        function add_lead_image_d()
        {

            var index = $('.setIndexProject_galery_galley').length;

            var form = `<div><div class="col-sm-3">
            <div class="form-group setIndexProject_galery">
            <div class="col-md-12">
            <input type="file" id="lead_image${index + 1}" class="lead_image" name="lead_image_d[]" accept="image/*" />
            </div>
            </div>

            <button  type="button" class="btn btn-round btn-danger" style="margin-left: 112px;" onclick="removeLead_image(this)"> - ลบ Banner Default </button>

            </div>
            </div>`;

            $('#lead_image_project_d').append(form);

            $(".lead_image").fileinput({
                uploadUrl: "upload.php", // server upload action
                maxFileCount: 1,
                allowedFileExtensions: ["jpg", "png", "jpeg"],
                browseLabel: ' เลือก Banner Default',
                browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                removeLabel: 'ลบ',
                browseClass: 'btn btn-success',
                showUpload: false,
                showRemove:false,
                showCaption: false,
                msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                msgZoomModalHeading: 'ตัวอย่างละเอียด',
                xxx:'seo',
                minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                dropZoneTitle : 'Banner Default',
                msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
            });
        }

        function removeLead_image(element)
        {
            //$('#gallery_add').prop("disabled", false);
            $(element).parent().remove();
        }

        //add_lead_image p
        function add_lead_image_p()
        {

            var index = $('.setIndexProject_galery_galley').length;

            var form = `<div><div class="col-sm-3">
            <div class="form-group setIndexProject_galery">
            <div class="col-md-12">
            <input type="file" id="lead_image_p${index + 1}" class="lead_image" name="lead_image_p[]" accept="image/*" />
            </div>
            </div>

            <button  type="button" class="btn btn-round btn-danger" style="margin-left: 112px;" onclick="removeLead_image(this)"> - ลบ Banner Lead </button>

            </div>
            </div>`;

            $('#lead_image_project_p').append(form);

            $(".lead_image").fileinput({
                uploadUrl: "upload.php", // server upload action
                maxFileCount: 1,
                allowedFileExtensions: ["jpg", "png", "jpeg"],
                browseLabel: ' เลือก Banner MB',
                browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                removeLabel: 'ลบ',
                browseClass: 'btn btn-success',
                showUpload: false,
                showRemove:false,
                showCaption: false,
                msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                msgZoomModalHeading: 'ตัวอย่างละเอียด',
                xxx:'seo',
                minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                dropZoneTitle : 'Banner MB',
                msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
            });
        }

        function removeLead_image(element)
        {
            //$('#gallery_add').prop("disabled", false);
            $(element).parent().remove();
        }

        //add_lead_image p
        function add_lead_image_p_m()
        {

            var index = $('.setIndexProject_galery_galley').length;

            var form = `<div><div class="col-sm-3">
            <div class="form-group setIndexProject_galery">
            <div class="col-md-12">
            <input type="file" id="lead_image_p${index + 1}" class="lead_image" name="lead_image_p_m[]" accept="image/*" />
            </div>
            </div>

            <button  type="button" class="btn btn-round btn-danger" style="margin-left: 112px;" onclick="removeLead_image(this)"> - ลบ Banner MB </button>

            </div>
            </div>`;

            $('#lead_image_project_p_m').append(form);

            $(".lead_image").fileinput({
                uploadUrl: "upload.php", // server upload action
                maxFileCount: 1,
                allowedFileExtensions: ["jpg", "png", "jpeg"],
                browseLabel: ' เลือก Banner MB',
                browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                removeLabel: 'ลบ',
                browseClass: 'btn btn-success',
                showUpload: false,
                showRemove:false,
                showCaption: false,
                msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                msgZoomModalHeading: 'ตัวอย่างละเอียด',
                xxx:'seo',
                minImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
                minImageHeight: 1920, // ขนาด ศุง ต่ำสุด
                maxImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
                maxImageHeight: 1920, // ขนาด ศุง ต่ำสุด
                dropZoneTitle : 'Banner MB',
                msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
            });
        }

        function removeLead_image(element)
        {
            //$('#gallery_add').prop("disabled", false);
            $(element).parent().remove();
        }


        $("#lead_image_p0").fileinput({
            uploadUrl: "upload.php", // server upload action
            maxFileCount: 1,
            allowedFileExtensions: ["jpg", "png", "jpeg"],
            browseLabel: ' เลือก Banner ',
            browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
            removeLabel: 'ลบ',
            browseClass: 'btn btn-success',
            showUpload: false,
            showRemove:false,
            showCaption: false,
            msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
            msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
            msgZoomModalHeading: 'ตัวอย่างละเอียด',
            xxx:'seo',
            minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
            minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
            maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
            maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
            dropZoneTitle : 'Banner Lead',
            msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
            msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
            msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
            msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
        });

        $("#lead_image_p_m0").fileinput({
            uploadUrl: "upload.php", // server upload action
            maxFileCount: 1,
            allowedFileExtensions: ["jpg", "png", "jpeg"],
            browseLabel: ' เลือก Banner MB',
            browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
            removeLabel: 'ลบ',
            browseClass: 'btn btn-success',
            showUpload: false,
            showRemove:false,
            showCaption: false,
            msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
            msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
            msgZoomModalHeading: 'ตัวอย่างละเอียด',
            xxx:'seo',
            minImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
            minImageHeight: 1920, // ขนาด ศุง ต่ำสุด
            maxImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
            maxImageHeight: 1920, // ขนาด ศุง ต่ำสุด
            dropZoneTitle : 'Banner MB',
            msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
            msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
            msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
            msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
        });


        $("button[name='delBanner']").confirm({
            title : 'ลบข้อมูล',
            content: 'คุณแน่ใจที่จะลบข้อมูลนี้ !',
            buttons: {
                Yes: {
                    text: 'Yes',
                    btnClass: 'btn-danger',
                    keys: ['enter', 'a'],
                    action: function(){
                        $.ajax({ url: 'save_lhbanner.php',
                            data: {action: 'delByMainId' , bannerMainId :bannerMainId },
                            type: 'post',
                            success: function(e) {
                                window.location.href =  'lhbanner.php?delData=success';
                            }
                        });
                    }
                },
                No: {
                    text: 'No',
                    btnClass: 'btn-default',
                    keys: ['enter', 'a'],
                    action: function(){
                        // button action.
                    }
                },

            }
        });

        /////////////////////
    
       <?php
         $show_id = getType_show($bannerMainId);
           
           if($show_id->type_show_id == 0){
              $show_id = 1;
           }else if($show_id->type_show_id == 1){
              $show_id = 2;  
           }else if($show_id->type_show_id == 2){
              $show_id = 3; 
        ?>
            $('#vdo').prop( "checked", true );
            $('#gthumbnailYoutube').hide();
            $('#upYoutube').hide();

        <?php  
           }else if($show_id->type_show_id == 3){
              $show_id = 3;  
       ?>
            $('#youtube').prop( "checked", true );
            $('#gthumbnailVDO').hide();
            $('#upVdo').hide();
       <?php        
           }
       ?>


         $("input[name=bannerchoose][id=b<?=$show_id?>]").prop( "checked", true );

       if($('input[name=bannerchoose][id=b1]').is(':checked')){
         $('#bannerPC').show();
         $('#bannerMB').show();

         $('#ActivityColor').hide();
         $('#gActivityImg').hide(); 
         $('.activityimg').hide(); 

         $('#upVideo').hide();
         $('#upYoutube').hide();
         $('#gthumbnailYoutube').hide();
         $('#gthumbnailVDO').hide();
         $('#upVdo').hide();
       }else if($('input[name=bannerchoose][id=b2]').is(':checked')){
         $('#ActivityColor').show();
         $('#gActivityImg').show(); 
         $('.activityimg').show(); 

         $('#bannerPC').hide();
         $('#bannerMB').hide();

         $('#upVideo').hide();
         $('#upYoutube').hide();
         $('#gthumbnailYoutube').hide();
          $('#gthumbnailVDO').hide();
         $('#upVdo').hide();
       }else if($('input[name=bannerchoose][id=b3]').is(':checked')){

         $('#ActivityColor').hide();
         $('#gActivityImg').hide(); 
         $('.activityimg').hide(); 

         $('#bannerPC').hide();
         $('#bannerMB').hide();

       }
      
      $("#b1").on('ifChecked', function(event){
         $('#bannerPC').show();
         $('#bannerMB').show();

         $('#ActivityColor').hide();
         $('#gActivityImg').hide(); 
         $('.activityimg').hide(); 

         $('#upVideo').hide();
         $('#upYoutube').hide();
         $('#gthumbnailYoutube').hide();
         $('#gthumbnailVDO').hide();
         $('#upVdo').hide();
      });

      $("#b2").on('ifChecked', function(event){
         $('#ActivityColor').show();
         $('#gActivityImg').show(); 
         $('.activityimg').show(); 

         $('#bannerPC').hide();
         $('#bannerMB').hide();

         $('#upVideo').hide();
         $('#upYoutube').hide();
         $('#gthumbnailYoutube').hide();
         $('#upVdo').hide();
         $('#gthumbnailVDO').hide();
      });

       $("#b3").on('ifChecked', function(event){
         $('#ActivityColor').hide();
         $('#gActivityImg').hide(); 
         $('.activityimg').hide(); 

         $('#bannerPC').hide();
         $('#bannerMB').hide();

        
         $('#upYoutube').show();
         $('#gthumbnailYoutube').show();
         $('#upVdo').hide();
         $('#upVideo').show();
         $('#gthumbnailVDO').hide();
      });

      $('#youtube').on('ifChecked', function(event){
          $('#upYoutube').show();
          $('#gthumbnailYoutube').show();

          $('#gthumbnailVDO').hide();
          $('#upVdo').hide();
      }); 

      $('#vdo').on('ifChecked', function(event){
         $('#upYoutube').hide();
         $('#gthumbnailYoutube').hide();

         $('#gthumbnailVDO').show();
         $('#upVdo').show();
      }); 

       
       if($('#banner_chek').val() == 'hidden' ){
             $('#banner').hide();

        }else{
            $('#banner').show();
            //Vakidate_();
        }
        
         //Vakidate_(true,false,false,false,false,false,false,false,false,false,false,false);
        $('#ch_banner').on('ifChecked', function(event){
            $('#banner').show();
            
           
            
        });
        $('#ch_banner').on('ifUnchecked', function (event) {
            $('#banner').hide();
        });

    //
    $(document).ready(function() {
            $.validator.setDefaults({ ignore: "[contenteditable='true']" });

                $('#editBannerForm').validate({
                    rules: {
                        PageTypeId : {
                            required: true
                        },
                        startdate : {
                            required: function(){
                              if($('#ch_banner').is(':checked')){
                                    return true;
                                } else {
                                    return false;
                               }
                            },
                        },
                        enddate : {
                            required: function(){
                              if($('#ch_banner').is(':checked')){
                                    return true;
                                } else {
                                    return false;
                               }
                            },
                        },
                        'lead_image_p[]': {
                             required: function(){
                              if($('#ch_banner').is(':checked')){
                                 if($('#b1').is(':checked')){
                                    return true;
                                  }else{
                                    return false;
                                  }
                                } else {
                                    return false;
                               }
                            },
                        },
                        'lead_image_p_m[]' : {
                            required: false
                        },
                        activityimg : {
                            required: function(){
                              if($('#ch_banner').is(':checked')){
                                 if($('#b2').is(':checked')){
                                    return true;
                                  }else{
                                    return false;
                                  }
                                } else {
                                    return false;
                               }
                            },
                        },
                        activitycolor : {
                            required: function(){
                              if($('#ch_banner').is(':checked')){
                                 if($('#b2').is(':checked')){
                                    return true;
                                  }else{
                                    return false;
                                  }
                                } else {
                                    return false;
                               }
                            },
                        },
                        thumbnailYoutube : {
                            required: function(){
                              if($('#ch_banner').is(':checked')){
                                 if($('#b3').is(':checked')){
                                    if($('#youtube').is(':checked')){
                                       return true;
                                    }
                                  }else{
                                    return false;
                                  }
                                } else {
                                    return false;
                               }
                            },
                        },
                        url_youtube : {
                           required: function(){
                              if($('#ch_banner').is(':checked')){
                                 if($('#b3').is(':checked')){
                                    if($('#youtube').is(':checked')){
                                       return true;
                                    }
                                  }else{
                                    return false;
                                  }
                                } else {
                                    return false;
                               }
                            },
                        },
                        vdofile : {
                            required: function(){
                              if($('#ch_banner').is(':checked')){
                                 if($('#b3').is(':checked')){
                                    if($('#vdo').is(':checked')){
                                       return true;
                                    }
                                  }else{
                                    return false;
                                  }
                                } else {
                                    return false;
                               }
                            },
                        },
                        thumbnailVDO : {
                            required: function(){
                              if($('#ch_banner').is(':checked')){
                                 if($('#b3').is(':checked')){
                                    if($('#vdo').is(':checked')){
                                       return true;
                                    }
                                  }else{
                                    return false;
                                  }
                                } else {
                                    return false;
                               }
                            },
                        },
                        'lead_image_d[]' : {
                            required: true
                        },
                        'lead_image_m[]'  : {
                            required: true
                        },

                    },
                    messages: {
                        PageTypeId : "กรุณาเลือกหน้าจอ",
                        startdate : "กรุณาเลือกวันที่เริ่มต้น",
                        enddate : "กรุณาเลือกวันที่สิ้นสุด",
                        'lead_image_p[]': "กรุณาเลือกรูป",
                        'lead_image_p_m[]': "กรุณาเลือกรูป",
                        activityimg : "กรุณาเลือกรูป",
                        thumbnailYoutube : "กรุณาเลือกรูป",
                        url_youtube : "กรุณากรอก URL",
                        vdofile : "กรุณาเลือก VDO",
                        thumbnailVDO : "กรุณาเลือกรูป",
                        'lead_image_d[]' : "กรุณาเลือกรูป",
                        'lead_image_m[]'  : "กรุณาเลือกรูป",
                        activitycolor : "กรุณาเลือกสี",


                    }
                });

           });

 