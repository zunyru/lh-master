<?php
require_once '../model/register.php'; 
header('Access-Control-Allow-Origin: *');  

$get_register = new GetRegister();
$post_register = new PostRegister();

if($_GET["n"] == 'prvinces'){
    echo $get_register->prvinces();
}
else if($_GET["n"] == 'comment'){
    echo $get_register->comment($_GET["id"]);
}
else if($_GET["n"] == 'open_register'){
    echo $get_register->open_register();
}
else if($_GET["n"] == 'open_news'){
    echo $get_register->open_news($_GET["id"]);
}
else if($_GET["n"] == 'open_optionnews'){
    echo $get_register->open_optionnews();
}
// *************
else if($_GET["n"] == 'user'){
    echo $get_register->user($_GET["id"]);
}
else if($_GET["n"] == 'userAdd'){
    
    $data = array(
        'user_name' => trim($_POST["user"]),
        'login' => $_POST["login"],
        'pass' => $_POST["pass"]
    );

    echo $post_register->user_add($data);
}
else if($_GET["n"] == 'userEdit'){
    
    $data = array(
        'id' =>  trim($_POST["id"]),
        'user_name' => trim($_POST["user"]),
        'login' => $_POST["login"],
        'pass' => $_POST["pass"]
    );

    echo $post_register->user_edit($data);
}
// *************
else if($_GET["n"] == 'location'){
    echo $get_register->location($_GET["id"]);
}
else if($_GET["n"] == 'locationAdd'){
    
    $data = array(
        'location_name' => trim($_POST["area"]),
        'province_id' => $_POST["province"],
        'is_show' => $_POST["show"]
    );

    echo $post_register->location_add($data);
}
else if($_GET["n"] == 'locationEdit'){
    
    $data = array(
        'id' => $_POST["id"],
        'location_name' => trim($_POST["area"]),
        'province_id' => $_POST["province"],
        'is_show' => $_POST["show"]
    );

    echo $post_register->location_edit($data);
}
// *************
else if($_GET["n"] == 'position'){
    echo $get_register->position($_GET["id"]);
}
else if($_GET["n"] == 'positionAdd'){
    
    $data = array(
        'position' => trim($_POST["position"])
    );

    echo $post_register->position_add($data);
}
else if($_GET["n"] == 'positionEdit'){
    
    $data = array(
        'id' => $_POST["id"],
        'position' => trim($_POST["position"])
    );

    echo $post_register->position_edit($data);
}
// *************
else if($_GET["n"] == 'work'){
    echo $get_register->work($_GET["id"]);
}
else if($_GET["n"] == 'workAdd'){
    
    $data = array(
        'position_id' => trim($_POST["position_id"]),
        'location_id' => trim($_POST["location_id"]),
        'attribute' => trim($_POST["attribute"]),
        'rates' => trim($_POST["rates"]),
        'posted' => trim($_POST["posted"]),
        'is_show' => trim($_POST["is_show"])
    );

    echo $post_register->work_add($data);
}
else if($_GET["n"] == 'workEdit'){
    
    $data = array(
        'id' => $_POST["id"],
        'position_id' => trim($_POST["position_id"]),
        'location_id' => trim($_POST["location_id"]),
        'attribute' => trim($_POST["attribute"]),
        'rates' => trim($_POST["rates"]),
        'posted' => trim($_POST["posted"]),
        'is_show' => trim($_POST["is_show"])
    );

    echo $post_register->work_edit($data);
}
// *************
else if($_GET["n"] == 'news'){
    echo $get_register->news($_GET["id"]);
}
else if($_GET["n"] == 'newsAdd'){
    
    if ( 0 < $_FILES['file']['error'] ) {
        echo json_encode(array('message'=>$_FILES['file']['error'],'status'=>'0')); 
    }
    else {
        move_uploaded_file($_FILES['file']['tmp_name'], '../fileupload/news/' . $_POST['imagename']);
        $data = array(
            'title' => trim($_POST["title"]),
            'short' => trim($_POST["short"]),
            'detail' => trim($_POST["detail"]),
            'publish' => trim($_POST["publish"]),
            'is_show' => trim($_POST["show"]),
            'imagename' => trim($_POST["imagename"])
        );
        echo $post_register->news_add($data);
    }

}
else if($_GET["n"] == 'newsEdit'){
    
    if ( 0 < $_FILES['file']['error'] ) {
        echo json_encode(array('message'=>$_FILES['file']['error'],'status'=>'0')); 
    }
    else {
        @unlink('../fileupload/news/'.$_POST["imagename_old"]);
        move_uploaded_file($_FILES['file']['tmp_name'], '../fileupload/news/' . $_POST['imagename']);
        $data = array(
            'id' => $_POST["id"],
            'title' => trim($_POST["title"]),
            'short' => trim($_POST["short"]),
            'detail' => trim($_POST["detail"]),
            'publish' => trim($_POST["publish"]),
            'is_show' => trim($_POST["show"]),
            'imagename' => trim($_POST["imagename"])
        );
        echo $post_register->news_edit($data);
    }

}
// *************
else if($_GET["n"] == 'report'){
    echo $get_register->report($_GET["id"]);
}
else if($_GET["n"] == 'reportEdit'){
    
    $data = array(
        'id' => $_POST["id"],
        'comment_id' => trim($_POST["comment_id"]),
        'other' => trim($_POST["other"])
    );

    echo $post_register->report_edit($data);
}
// *************
else if($_GET["n"] == 'candidacy'){
    echo $get_register->candidacy();
}
else if($_GET["n"] == 'candidacy_profile'){
    echo $get_register->candidacy_profile($_GET["id"]);
}
else if($_GET["n"] == 'candidacy_family'){
    echo $get_register->candidacy_family($_GET["id"]);
}
else if($_GET["n"] == 'candidacy_contact'){
    echo $get_register->candidacy_contact($_GET["id"]);
}
else if($_GET["n"] == 'candidacy_education'){
    echo $get_register->candidacy_education($_GET["id"]);
}
else if($_GET["n"] == 'candidacy_language'){
    echo $get_register->candidacy_language($_GET["id"]);
}
else if($_GET["n"] == 'candidacy_job'){
    echo $get_register->candidacy_job($_GET["id"]);
}
else if($_GET["n"] == 'candidacy_job_interested'){
    echo $get_register->candidacy_job_interested($_GET["id"]);
}
else if($_GET["n"] == 'candidacy_person'){
    echo $get_register->candidacy_person($_GET["id"]);
}
else if($_GET["n"] == 'candidacy_datas'){
    echo $get_register->candidacy_datas($_GET["id"]);
}

else if($_GET["n"] == 'candidacyAdd_file'){
    if ( 0 < $_FILES['file']['error'] ) {
        echo json_encode(array('message'=>$_FILES['file']['error'],'status'=>'0')); 
    }
    else {
        move_uploaded_file($_FILES['file']['tmp_name'], '../fileupload/candidacy/' . $_POST['attachment_file']);
        
        $data = array(
            'id' => trim($_POST["id"]),
            'attachment_file' => trim($_POST["attachment_file"]),
        );
        echo $post_register->attachment_add_file($data);
    }
}
else if($_GET["n"] == 'candidacyAdd_img'){
    if ( 0 < $_FILES['file_img']['error'] ) {
        echo json_encode(array('message'=>$_FILES['file_img']['error'],'status'=>'0')); 
    } else{
        move_uploaded_file($_FILES['file_img']['tmp_name'], '../fileupload/candidacy/' . $_POST['attachment_image']);
        $data = array(
            'id' => trim($_POST["id"]),
            'attachment_image' => trim($_POST["attachment_image"]),
        );
        echo $post_register->attachment_add_img($data);
    } 
}
else if($_GET["n"] == 'candidacyAdd'){

    $data = array(
            'work_id' => trim($_POST["work_id"]),
            'provine_id' => trim($_POST["provine_id"]),
            'prefix' => trim($_POST["prefix"]),
            'name' => trim($_POST["name"]),
            'lastname' => trim($_POST["lastname"]),
            'nickname' => trim($_POST["nickname"]),
            'sex' => trim($_POST["sex"]),
            'birthday' => trim($_POST["birthday"]),
            'height' => trim($_POST["height"]),
            'weight' => trim($_POST["weight"]),
            'religion' => trim($_POST["religion"]),
            'race' => trim($_POST["race"]),
            'nationslity' => trim($_POST["nationslity"]),
            'draft' => trim($_POST["draft"]),
            'card_namber' => trim($_POST["card_namber"]),
            'status' => trim($_POST["status"]),
            'moredata' => trim($_POST["moredata"]),
            'children' => trim($_POST["children"]),
            'printTH' => trim($_POST["printTH"]),
            'printEN' => trim($_POST["printEN"]),
            'provine_contact_id' => trim($_POST["provine_contact_id"]),
            'address' => trim($_POST["address"]),
            'phone' => trim($_POST["phone"]),
            'email' => trim($_POST["email"]),
            'name_emer' => trim($_POST["name_emer"]),
            'phone_emer' => trim($_POST["phone_emer"]),
            'position' => trim($_POST["position"]),
            'type_job' => trim($_POST["type_job"]),
            'salary_hope' => trim($_POST["salary_hope"]),
            'start_day' => trim($_POST["start_day"]),
            'name_person' => trim($_POST["name_person"]),
            'company' => trim($_POST["company"]),
            'relationship' => trim($_POST["relationship"]),
            'telperson' => trim($_POST["telperson"]),
            'working_always' => trim($_POST["working_always"]),
            'working_sometimes' => trim($_POST["working_sometimes"]),
            'disease' => trim($_POST["disease"]),
            'crime' => trim($_POST["crime"]),
            'bankrupt' => trim($_POST["bankrupt"]),
            'lay_off' => trim($_POST["lay_off"])
        );
    echo $post_register->candidacy_add($data);
  
}
else if($_GET["n"] == 'candidacyAdd_language'){

    $data = array(
            'id' => trim($_POST["id"]),
            'language1' => trim($_POST["language1"]),
            'talk1' => trim($_POST["talk1"]),
            'write1' => trim($_POST["write1"]),
            'read1' => trim($_POST["read1"]),
            'language2' => trim($_POST["language2"]),
            'talk2' => trim($_POST["talk2"]),
            'write2' => trim($_POST["write2"]),
            'read2' => trim($_POST["read2"]),
            'language3' => trim($_POST["language3"]),
            'talk3' => trim($_POST["talk3"]),
            'write3' => trim($_POST["write3"]),
            'read3' => trim($_POST["read3"]),
            'language4' => trim($_POST["language4"]),
            'talk4' => trim($_POST["talk4"]),
            'write4' => trim($_POST["write4"]),
            'read4' => trim($_POST["read4"]),
        );
    echo $post_register->language_add($data);
  
}
else if($_GET["n"] == 'candidacyAdd_job'){

    $data = array(
            'id' => trim($_POST["id"]),
            'finish' => trim($_POST["finish"]),
            'exp' => trim($_POST["exp"]),
            'line_last' => trim($_POST["line_last"]),
            'last_position' => trim($_POST["last_position"]),
            'last_money' => trim($_POST["last_money"]),
            'date_start1' => trim($_POST["date_start1"]),
            'date_stop1' => trim($_POST["date_stop1"]),
            'position1' => trim($_POST["position1"]),
            'company1' => trim($_POST["company1"]),
            'salary1' => trim($_POST["salary1"]),
            'other_salary1' => trim($_POST["other_salary1"]),
            'more_other_salary1' => trim($_POST["more_other_salary1"]),
            'features1' => trim($_POST["features1"]),
            'date_start2' => trim($_POST["date_start2"]),
             'date_stop2' => trim($_POST["date_stop2"]),
            'position2' => trim($_POST["position2"]),
            'company2' => trim($_POST["company2"]),
            'salary2' => trim($_POST["salary2"]),
            'other_salary2' => trim($_POST["other_salary2"]),
            'more_other_salary2' => trim($_POST["more_other_salary2"]),
            'features2' => trim($_POST["features2"]),
            'date_start3' => trim($_POST["date_start3"]),
             'date_stop3' => trim($_POST["date_stop3"]),
            'position3' => trim($_POST["position3"]),
            'company3' => trim($_POST["company3"]),
            'salary3' => trim($_POST["salary3"]),
            'other_salary3' => trim($_POST["other_salary3"]),
            'more_other_salary3' => trim($_POST["more_other_salary3"]),
            'features3' => trim($_POST["features3"]),
            'date_start4' => trim($_POST["date_start4"]),
             'date_stop4' => trim($_POST["date_stop4"]),
            'position4' => trim($_POST["position4"]),
            'company4' => trim($_POST["company4"]),
            'salary4' => trim($_POST["salary4"]),
            'other_salary4' => trim($_POST["other_salary4"]),
            'more_other_salary4' => trim($_POST["more_other_salary4"]),
            'features4' => trim($_POST["features4"]),
            'date_start5' => trim($_POST["date_start5"]),
             'date_stop5' => trim($_POST["date_stop5"]),
            'position5' => trim($_POST["position5"]),
            'company5' => trim($_POST["company5"]),
            'salary5' => trim($_POST["salary5"]),
            'other_salary5' => trim($_POST["other_salary5"]),
            'more_other_salary5' => trim($_POST["more_other_salary5"]),
            'features5' => trim($_POST["features5"]),
          
        );
    echo $post_register->job_add($data);
  
}
else if($_GET["n"] == 'candidacyAdd_education'){

    $data = array(
            'id' => trim($_POST["id"]),
            'top_graduate'  => trim($_POST["top_graduate"]),
            'graduate_id1' => trim($_POST["graduate_id1"]),
            'date_start1' => trim($_POST["date_start1"]),
            'date_stop1' => trim($_POST["date_stop1"]),
            'institution1' => trim($_POST["institution1"]),
            'faculty1' => trim($_POST["faculty1"]),
            'branch1' => trim($_POST["branch1"]),
            'gpa1' => trim($_POST["gpa1"]),
            'graduate_id2' => trim($_POST["graduate_id2"]),
            'date_start2' => trim($_POST["date_start2"]),
            'date_stop2' => trim($_POST["date_stop2"]),
            'institution2' => trim($_POST["institution2"]),
            'faculty2' => trim($_POST["faculty2"]),
            'branch2' => trim($_POST["branch2"]),
            'gpa2' => trim($_POST["gpa2"]),
            'graduate_id3' => trim($_POST["graduate_id3"]),
            'date_start3' => trim($_POST["date_start3"]),
            'date_stop3' => trim($_POST["date_stop3"]),
            'institution3' => trim($_POST["institution3"]),
            'faculty3' => trim($_POST["faculty3"]),
            'branch3' => trim($_POST["branch3"]),
            'gpa3' => trim($_POST["gpa3"]),
        );
    echo $post_register->education_add($data);
  
}
else if($_GET["n"] == 'candidacyAdd_family'){

    $data = array(
            'id' => trim($_POST["id"]),
            'name_father' => trim($_POST["name_father"]),
            'age_father' => trim($_POST["age_father"]),
            'career_father' => trim($_POST["career_father"]),
            'address_father' => trim($_POST["address_father"]),
            'status_father' => trim($_POST["status_father"]),
            'name_mother' => trim($_POST["name_mother"]),
            'age_mother' => trim($_POST["age_mother"]),
            'career_mother' => trim($_POST["career_mother"]),
            'address_mother' => trim($_POST["address_mother"]),
            'status_mother' => trim($_POST["status_mother"]),
            'name_fraternity1' => trim($_POST["name_fraternity1"]),
            'age_fraternity1' => trim($_POST["age_fraternity1"]),
            'career_fraternity1' => trim($_POST["career_fraternity1"]),
            'address_fraternity1' => trim($_POST["address_fraternity1"]),
            'status_fraternity1' => trim($_POST["status_fraternity1"]),
            'name_fraternity2' => trim($_POST["name_fraternity2"]),
            'age_fraternity2' => trim($_POST["age_fraternity2"]),
            'career_fraternity2' => trim($_POST["career_fraternity2"]),
            'address_fraternity2' => trim($_POST["address_fraternity2"]),
            'status_fraternity2' => trim($_POST["status_fraternity2"]),
            'name_fraternity3' => trim($_POST["name_fraternity3"]),
            'age_fraternity3' => trim($_POST["age_fraternity3"]),
            'career_fraternity3' => trim($_POST["career_fraternity3"]),
            'address_fraternity3' => trim($_POST["address_fraternity3"]),
            'status_fraternity3' => trim($_POST["status_fraternity3"]),
            'name_spouse' => trim($_POST["name_spouse"]),
            'age_spouse' => trim($_POST["age_spouse"]),
            'career_spouse' => trim($_POST["career_spouse"]),
            'address_spouse' => trim($_POST["address_spouse"]),
            'status_spouse' => trim($_POST["status_spouse"]),
        );
    echo $post_register->family_add($data);
  
}
// *************
else if($_GET["n"] == 'delete'){
    $data = array(
        'id' => $_POST["id"],
        'key' => trim($_POST["key"]),
        'tb' => trim($_POST["tb"])
    );
    
    echo $post_register->delete_($data);
}
else{
    echo json_encode(array(['message'=>'error']));
}


?>