<?php
session_start();
include './include/dbCon_mssql.php';
include './include/UpdateSlug.php';

$group_id = $_SESSION['group_id'];
$zone_id = $_GET['id'];

$_GET['page'] = 'zone';
$zone_id = $_GET['id'];

include_once('include/dbZones.php');

function mssql_escape($str)
{
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return str_replace("'", "''", $str);
}

$funZone = new dbZones();
if (isset($_POST['submit'])) {

	$zone_th = mssql_escape($_POST['zone_th']);
	$zone_en = $_POST['zone_en'];
	$zone_ac = $_POST['hobbies'];
	$zone_provice = $_POST['zone_provice'];

	$sql = "SELECT COUNT(zone_id) AS num FROM LH_ZONES WHERE ((zone_name_th = '$zone_th') OR  (zone_name_en = '$zone_en')) AND NOT zone_id = '$zone_id'";
	$query_ = mssql_query($sql);
	$row = mssql_fetch_array($query_);
	if ($row['num'] == 0) {

		$dates = date("Y-m-d H:i:s");
		$result = '';
		foreach ($zone_ac as $key => $value) {
			$result = $result . $value;
		}
		$query_up = "UPDATE LH_ZONES SET zone_name_th='$zone_th',zone_name_en='$zone_en',zone_attribute='$result', zone_update = '$dates' , province_id='$zone_provice' WHERE zone_id='$zone_id'";
		$stmt = mssql_query($query_up);
		//update slug
		UpdateSlug::run(['zone']);
		$success = "success";
	} else {
		$error2 = "error"; //ค่าซ้ำ
	}
}

if (isset($_SESSION['add'])) {
	if ($_SESSION['add'] == -2) {
		$error = "error"; //ค่าซ้ำ
	}
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>
    <!-- comfirm -->
     <link rel="stylesheet" href="../build/libs/bundled.css">
    <script src="../build/libs/bundled.js"></script>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
    <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>
     <script>
      $(document).ready(function(){
        $("#alert").show();
            $("#alert").fadeTo(3000, 400).slideUp(500, function(){
              $("#alert").alert('close');
          });
      });
      </script>

      <!-- จังหวัด -->
      <script language=Javascript>
        function Inint_AJAX() {
           try { return new ActiveXObject("Msxml2.XMLHTTP");  } catch(e) {} //IE
           try { return new ActiveXObject("Microsoft.XMLHTTP"); } catch(e) {} //IE
           try { return new XMLHttpRequest();          } catch(e) {} //Native Javascript
           alert("XMLHttpRequest not supported");
           return null;
        };

        function dochange(src, val,id) {
            var id = '<?php echo $_GET["id"] ?>';
             var req = Inint_AJAX();
             req.onreadystatechange = function () {
                  if (req.readyState==4) {
                       if (req.status==200) {
                            document.getElementById(src).innerHTML=req.responseText; //รับค่ากลับมา
                       }
                  }
             };
             req.open("GET", "localtion.php?data="+src+"&val="+val+"&id="+id); //สร้าง connection
             req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8"); // set Header
             req.send(null); //ส่งค่า
        }

        window.onLoad=dochange('province', -1);
    </script>
    <script type="text/javascript">

      $(document).ready(function(){
        $("#test_delete").click(function(){
          message();
        });
      $("#cancel").click(function(){
      hide();
    });
  });

  function message()
  {
    $("#delete_message").slideDown();
  }

  function hide()
  {
    $("#delete_message").slideUp();
  }
  </script>


  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
           <?php include './master/navbar.php';?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
          <?php include './master/top_nav.php';?>
        <!-- /top navigation -->
        <?php
$qr_id = $funZone->list_zone($zone_id);
$resulft = $qr_id->fetch(PDO::FETCH_ASSOC);
//print_r($resulft);
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><a href="zone.php">ระบบจัดการข้อมูล Zone</a> > แก้ไขข้อมูล Zone : <?=$resulft['zone_name_th'];if ($resulft['zone_name_en'] != '') {echo " / " . $resulft['zone_name_en'];}?></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <p class="font-gray-dark">
                  </p><br>
                   <?php if (isset($success)) {?>
                   <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>แก้ไขข้อมูลเรียบร้อยแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                  <?php } else if (isset($error)) {?>
                    <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>ไม่สามารถลบข้อมูลได้ !</strong> ข้อมูลนี้มีความสัมพันธ์กับโครงการอื่นอยู่
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                    <?php } else if (isset($error2)) {?>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="form-group">
                                <div class="alert alert-danger col-md-6" id="alert">
                                    <button type="button" class="close" data-dismiss="alert">x</button>
                                    <strong>มีชื่อนี้อยู่ในระบบแล้ว !</strong>
                                </div>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                    <?php }?>
                  <form class="form-horizontal form-label-left" action="" method="post" id="commentForm">
                    <div class="form-group">
                      <label class="control-label col-md-2" for="first-name">ระบุชื่อโซน <span class="required">*</span>
                      </label>
                      <div class="col-md-6">
                        <input type="text"  name="zone_th" required class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อ Zone (ไทย)" value="<?=$resulft['zone_name_th'];?>">
                      </div>
                    </div>
                    <div class="form-group hide-for-th">
                      <label class="control-label col-md-2" for="first-name">ระบุชื่อโซน <span class="required">*</span>
                      </label>
                      <div class="col-md-6">
                        <input type="text"  name="zone_en" required class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อ Zone (อังกฤษ)" value="<?=$resulft['zone_name_en'];?>">
                      </div>
                    </div>

                     <div class="form-group">
                      <label class="control-label col-md-2" for="first-name">ระบุจังหวัด <span class="required">*</span>
                      </label>
                      <div class="col-md-6">
                        <select name="zone_provice" class="form-control" required>
                           <option value="">-เลือกจังหวัด-</option>
                        <?php
$sql_p = "SELECT * FROM LH_PROVINCES ORDER BY PROVINCE_NAME ASC";
$query_p = mssql_query($sql_p);
while ($rows = mssql_fetch_array($query_p)) {
	if ($rows['PROVINCE_ID'] == $resulft['province_id']) {
		$selected = 'selected';
	} else {
		$selected = '';
	}
	?>
                          <option  value="<?=$rows['PROVINCE_ID']?>" <?=$selected;?>><?=$rows['PROVINCE_NAME'] . " (" . $rows['PROVINCE_NAME_ENG'] . ")";?></option>
                        <?php }?>
                        </select>
                      </div>
                    </div>


                    <div class="form-group">
                      <label class="control-label col-md-2" >กำหนดคุณสมบัติข้อมูล <span class="required">*</span></label>
                      <div class="col-md-3">
                      <p style="padding: 5px;">
                        <?php
$website = '';
$flm = '';
if ($resulft['zone_attribute'] == 'websiteflm') {
	$website = 'checked';
	$flm = 'checked';
} else if ($resulft['zone_attribute'] == 'website') {
	$website = 'checked';
} else if ($resulft['zone_attribute'] == 'flm') {
	$flm = 'checked';
}
?>
                        <input type="checkbox" name="hobbies[]" id="hobby1" <?=$website;?> required value="website" data-parsley-mincheck="1"  class="flat" />  ใช้บนเว็บไซต์
                        <br />

                        <input type="checkbox" name="hobbies[]" id="hobby2" <?=$flm;?>  value="flm" class="flat" />  สำหรับ FLM
                        <br />
                      </p>
                        </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-2" for="first-name"></label>
                          <div class="col-md-6">
                              <label for="hobbies[]"  class="error"></label>
                          </div>
                      </div>

                    </div>
                    <center><br>
                    <div class="col-md-8">
                    <button type="submit" name="submit" class="btn btn-success">Update</button>
                     <a class="confirms" data-title="ยืนยันการลบข้อมูล" name="delete" href="delete_zone.php?id=<?=$_GET['id'];?>">
                            <button type="button" class="btn btn-danger">Delete</button></a>

                    </div>
                  </center>
                  </form>
                </div>
              </div>
            </div>
          </div>
        <!-- /page content -->
           <script type="text/javascript">
            $('a.confirms').confirm({
              content: 'คุณแน่ใจที่จะลบข้อมูลนี้ !',
                buttons: {
                  Yes: {
                      text: 'Yes',
                      btnClass: 'btn-danger',
                      keys: ['enter', 'a'],
                      action: function(){
                          location.href = this.$target.attr('href');
                      }
                  },
                  No: {
                      text: 'No',
                      btnClass: 'btn-default',
                      keys: ['enter', 'a'],
                      action: function(){
                          // button action.
                      }
                  },

                }
            });
          </script>
        <!-- footer content -->
        <!-- <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer> -->
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
   <!--  <script src="../vendors/jquery/dist/jquery.min.js"></script> -->
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

        <!-- Validate -->
        <script src="../build/js/jquery.validate.js"></script>
        <script>
            $(document).ready(function() {

                $("#commentForm").validate({
                    rules: {
                        zone_th: "required",
                        zone_en: "required",
                        zone_provice : "required",


                    },
                    messages: {
                        zone_th: "กรุณากรอกชื่อ zone ไทย !",
                        zone_en : "กรุณากรอกชื่อ zone อังกฤษ !",
                        zone_provice : "กรุณาเลือก จังหวัด !",
                        'hobbies[]': "เลือกอย่างน้อย 1 รายการ !"

                    }
                });

                $("#lead_img_file").rules("add", {
                    required:true,
                    messages: {
                        required: " &nbsp; ไม่มีรูป !"
                    }
                });

            });
        </script>
        <!-- jQuery Tags Input -->
    <script>
      function onAddTag(tag) {
        alert("Added a tag: " + tag);
      }

      function onRemoveTag(tag) {
        alert("Removed a tag: " + tag);
      }

      function onChangeTag(input, tag) {
        alert("Changed a tag: " + tag);
      }

      $(document).ready(function() {
        $('#tags_1').tagsInput({
          width: 'auto'
        });
        $('#tags_2').tagsInput({
          width: 'auto'
        });
      });
    </script>
    <!-- /jQuery Tags Input -->

  </body>
</html>