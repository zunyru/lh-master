<?php
session_start();
include './include/dbCon_mssql.php';
$_SESSION['group_id'];

$_GET['page'] = 'product';
$product_id = $_GET['id'];

if (isset($_SESSION['add'])) {
	if ($_SESSION['add'] == 1) {
		//header("location:product_add.php");
		$success = "success";
		unset($_SESSION["add"]);
	} else if ($_SESSION['add'] == 0) {

		$error = "error"; //ค่าซ้ำ
		$_SESSION['add'] = '';

	} else if ($_SESSION['add'] == -1) {
		$errors = "errors";
		unset($_SESSION["add"]);
	} else if ($_SESSION['add'] == -2) {
		$error2 = "errors";
		unset($_SESSION["add"]);
	}
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>
      <!-- Bootstrap -->
      <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome -->
      <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <!-- NProgress -->
      <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
      <!-- Custom Theme Style -->
      <link href="../build/css/custom.min.css" rel="stylesheet">
      <!-- Fancybox image popup -->
      <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
      <!-- bootstrap-progressbar -->
      <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
      <!-- jQuery -->
      <script src="../vendors/jquery/dist/jquery.min.js"></script>

      <script src="../build/js/custom.min.js"></script>      <script src="../build/js/custom.min.js"></script>
      <!-- uploade img -->
      <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
      <!--uploade-->
      <!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
      <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
      <!-- confirm-->
      <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
      <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
           <?php include './master/navbar.php'?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
          <?php include './master/top_nav.php';?>
        <!-- /top navigation -->
        <?php
$qr_id = "SELECT * FROM LH_PRODUCTS WHERE product_id = '" . $_GET['id'] . "'";
$query = mssql_query($qr_id);
$resulft = mssql_fetch_array($query);

?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><a href="product.php">ระบบจัดการประเภทสินค้า</a> >ข้อมูลประเภทสินค้า : <?=$resulft['product_name_th'] . " / " . $resulft['product_name_en']?></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <p class="font-gray-dark">
                  </p><br>
                   <?php if (isset($success)) {?>
                   <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>แก้ไขข้อมูลเรียบร้อยแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                  <?php } else if (isset($error)) {?>
                    <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>มีชื่อนี้อยู่ในระบบแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                   <?php } else if (isset($error2)) {?>
                       <div class="row">
                           <div class="col-md-2"></div>
                           <div class="form-group">
                               <div class="alert alert-danger col-md-6" id="alert">
                                   <button type="button" class="close" data-dismiss="alert">x</button>
                                   <strong>ไม่สามารถลบได้ ! </strong>มีโครงการที่ผูกกับประเภทนี้อยู่
                               </div>
                           </div>
                           <div class="col-md-4"></div>
                       </div>
                  <?php }?>
                  <form class="form-horizontal form-label-left" action="update_product.php" method="post" id="commentForm">
                    <div class="form-group">
                      <label class="control-label col-md-2" for="first-name">ระบุชื่อประเภท <span class="required">*</span>
                      </label>
                      <div class="col-md-6">
                        <input type="text"  name="product_name_th" required="required" class="form-control col-md-7 col-xs-12" value="<?=$resulft['product_name_th']?>" placeholder="กรอกชื่อประเภท (ไทย)">
                      </div>
                    </div>
                     <div class="form-group hide-for-th">
                      <label class="control-label col-md-2" for="first-name">ระบุชื่อประเภท <span class="required">*</span>
                      </label>
                      <div class="col-md-6">
                          <input value="<?php echo $_GET['id'] ?>" name="id" type="hidden">
                        <input type="text"  name="product_name_en" required="required" class="form-control col-md-7 col-xs-12" value="<?=$resulft['product_name_en']?>" placeholder="กรอกชื่อประเภท (อังกฤษ)">
                      </div>
                    </div>

                    <center><br>
                     <div class="col-md-8">
                    <button type="submit" name="submit" class="btn btn-success">Update</button>

                         <?php
$dis1 = '';
if ($product_id == 1) {
	$dis1 = 'disabled';
} else if ($product_id == 2) {
	$dis1 = 'disabled';
} else if ($product_id == 3) {
	$dis1 = 'disabled';
}?>

                    <a class="confirms" data-title="ยืนยันการลบข้อมูล" name="delete" href="delete_product.php?id=<?=$product_id?>">
                    <button type="button" <?=$dis1?>  class="btn btn-danger">Delete</button></a>

                    </div>
                  </center>
                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <!-- /page content -->
            <script type="text/javascript">

            $('a.confirms').confirm({
              content: 'คุณแน่ใจที่จะลบข้อมูลนี้ !',
                buttons: {
                  Yes: {
                      text: 'Yes',
                      btnClass: 'btn-danger',
                      keys: ['enter', 'a'],
                      action: function(){
                          location.href = this.$target.attr('href');
                      }
                  },
                  No: {
                      text: 'No',
                      btnClass: 'btn-default',
                      keys: ['enter', 'a'],
                      action: function(){
                          // button action.
                      }
                  },

                }
            });
          </script>
        <!-- footer content -->
        <!-- <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer> -->
        <!-- /footer content -->
      </div>
    </div>

        <!-- Bootstrap -->
        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../vendors/nprogress/nprogress.js"></script>

        <script type="text/javascript" src="js/bootstrap-imgupload.js"></script>
        <!-- Validate -->
        <script src="../build/js/jquery.validate.js"></script>

        <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>

        <!-- bootstrap-progressbar -->
        <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>

        <!-- Custom Theme Scripts -->
        <script src="../build/js/custom.min.js"></script>
    <script>
        $(document).ready(function() {

            $("#commentForm").validate({
                rules: {
                    product_name_th: "required",
                    product_name_en: "required",

                },
                messages: {
                    product_name_th: "กรุณากรอกชื่อ ประเภทสินค้า ไทย !",
                    product_name_en : "กรุณากรอกชื่อ ประเภทสินค้า อังกฤษ !",

                }
            });

            $("#lead_img_file").rules("add", {
                required:true,
                messages: {
                    required: " &nbsp; ไม่มีรูป !"
                }
            });

        });
    </script>

      <script>
      $(document).ready(function(){
        $("#alert").show();
            $("#alert").fadeTo(3000, 500).slideUp(500, function(){
              $("#alert").alert('close');
          });
      });
      </script>

    <!-- /Datatables -->
        <!-- jQuery Tags Input -->
    <script>
      function onAddTag(tag) {
        alert("Added a tag: " + tag);
      }

      function onRemoveTag(tag) {
        alert("Removed a tag: " + tag);
      }

      function onChangeTag(input, tag) {
        alert("Changed a tag: " + tag);
      }

      $(document).ready(function() {
        $('#tags_1').tagsInput({
          width: 'auto'
        });
        $('#tags_2').tagsInput({
          width: 'auto'
        });
      });
    </script>
    <!-- /jQuery Tags Input -->

  </body>
</html>