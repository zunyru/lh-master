<?php
session_start();
$_SESSION['group_id'];
require_once '../include/dbCon_mssql.php'; 

function DateThai($strDate)
{
  $strYear = date("Y",strtotime($strDate))+543;
  $strMonth= date("n",strtotime($strDate));
  $strDay= date("j",strtotime($strDate));
  $strHour= date("H",strtotime($strDate));
  $strMinute= date("i",strtotime($strDate));
  $strSeconds= date("s",strtotime($strDate));
  $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
  $strMonthThai=$strMonthCut[$strMonth];
  return "$strDay $strMonthThai $strYear";
}

$slug1 = '';
$slug2 = '';
$slug3 = '';
$strSQL = "SELECT a.ID AS app_id ,a.PREFIX,a.NAME,a.LASTNAME,c.PHONE,a.CREATE_DAT,w.ID,t.TITLE,w.POSTED_DAT,l.LOCATION_NAME,w.RATES,w.IS_SHOW,p.PROVINCE_NAME,p.PROVINCE_ID,co.COMMENT,a.COMMENT_ID
FROM JOB_APPLICANT a
LEFT JOIN JOB_WORK w ON a.WORK_ID = w.ID
LEFT JOIN JOB_COMMENT co ON co.ID = a.COMMENT_ID
LEFT JOIN JOB_APPLICANT_CONTACT c ON c.ID = a.ID
LEFT JOIN JOB_TITLE t ON w.POSITION_ID = t.ID
LEFT JOIN JOB_LOCATION l ON w.LOCATION_ID = l.LOCATION_ID
LEFT JOIN Province p ON p.PROVINCE_ID = l.PROVINCE_ID ";

if(isset($_GET['work']) && !empty($_GET['work'])){
 $strSQL .= " WHERE t.ID = '".$_GET['work']."' ";
 $slug1 = '&work='.$_GET['work'];
}
if(isset($_GET['province']) && !empty($_GET['province'])){
  if(empty($_GET['work'])){
    $strSQL .=" WHERE ";
  }else{
    $strSQL .=" AND ";
  }
  $strSQL .= " p.PROVINCE_ID = '".$_GET['province']."' ";
  $slug2 = '&province='.$_GET['province'];
}
if(isset($_GET['location']) && !empty($_GET['location'])){
  if(empty($_GET['work']) && empty($_GET['province'])){
    if(isset($_GET['province']) && !empty($_GET['province'])){
      $strSQL .=" WHERE ";
    }else{
      $strSQL .=" AND ";
    }
  }else{
    if(isset($_GET['province']) && empty($_GET['province'])){
      $strSQL .=" AND ";
    }else{
      $strSQL .=" AND ";
    }
  }
  $strSQL .= " w.LOCATION_ID = '".$_GET['location']."' ";
  $slug3 = '&location='.$_GET['location'];
}
if(isset($_GET['com']) && !empty($_GET['com'])){
 if(empty($_GET['work']) && empty($_GET['province'])){
  $strSQL .=" WHERE ";
}else{
  $strSQL .=" AND ";
} 
$strSQL .= " co.ID = '".$_GET['com']."' ";
}

$strSQL .=" ORDER BY w.UPDATE_DAT DESC ";
$objQuery = mssql_query($strSQL) or die ("Error Query [".$strSQL."]");
$Num_Rows = mssql_num_rows($objQuery);
$num = $Num_Rows / 1000;
if($num >= 1){
  $Per_Page = 100;
}else{
  $Per_Page = 50;
}

$Page = $_GET["Page"];
if(!$_GET["Page"])
{
  $Page=1;
}

$Prev_Page = $Page-1;
$Next_Page = $Page+1;

$Page_Start = (($Per_Page*$Page)-$Per_Page);
if($Num_Rows<=$Per_Page)
{
  $Num_Pages =1;
}
else if(($Num_Rows % $Per_Page)==0)
{
  $Num_Pages =($Num_Rows/$Per_Page) ;
}
else
{
  $Num_Pages =($Num_Rows/$Per_Page)+1;
  $Num_Pages = (int)$Num_Pages;
}
$Page_End = $Per_Page * $Page;
if($Page_End > $Num_Rows)
{
  $Page_End = $Num_Rows;
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>LAND & HOUSES</title>

  <!-- Bootstrap -->
  <link href="../../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="../../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- NProgress -->
  <link href="../../vendors/nprogress/nprogress.css" rel="stylesheet">
  <!-- iCheck -->
  <link href="../../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
  <!-- Datatables -->
  <link href="../../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
  <link href="../../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
  <link href="../../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
  <link href="../../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
  <link href="../../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

  <!-- Custom Theme Style -->
  <link href="../../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md" ng-app="apply"  ng-controller="applyController" >
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;">
            <a href="#" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
          </div>

          <div class="clearfix"></div>

          <?php  include  '../master/navbar_regis.php' ?>
          <!-- /menu footer buttons -->
        </div>
      </div>

      <!-- top navigation -->
      <?php include '../master/top_nav_regis.php'; ?>
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
             <!-- menu--> 
             <div class="bs-example-popovers">
              <a href="apply1.php" class="btn btn-default ">
               ตำแหน่งงานว่าง 
             </a>
             <a href="candidacy1.php" class="btn btn-default">
               ผู้สมัครงาน 
             </a>
             <a href="report.php" class="btn btn-success active">
               รายงาน 
             </a>
             <a href="position1.php" class="btn btn-default">
               ชื่อตำแหน่งงาน 
             </a>
             <a href="news1.php" class="btn btn-default">
               ข่าวสาร 
             </a>
             <a href="area1.php" class="btn btn-default">
               กำหนดพื้นที่ทำงาน 
             </a>
             <a href="user1.php" class="btn btn-default">
               ผู้ใช้งาน 
             </a>
           </div>
           <br>
           <!-- -->

           <div class="x_title">
            <h2>ข้อมูลรายงาน</h2>

            <div class="clearfix"></div>
          </div>
          <div class="x_content">


            <div class="row">   
              <div class="col-md-5 col-sm-9 col-xs-12">
                <form name="test" method="get" action="<?=$_SERVER['SCRIPT_NAME'];?>">
                  <?php 
                  $sql ="SELECT ID, TITLE FROM JOB_TITLE";
                  $query = mssql_query($sql) or die ("Error Query [".$sql."]");

                  ?>
                  <select id="work" name="work" onchange="document.test.submit();" class="form-control" >
                    <option value="">เลือกดูตามตำแหน่งงานทั้งหมด</option>
                    <?php while ($row = mssql_fetch_assoc($query)) {
                      $sql_w = "SELECT DISTINCT w.POSITION_ID FROM JOB_WORK w 
                      LEFT JOIN JOB_LOCATION l ON w.LOCATION_ID = l.LOCATION_ID
                      LEFT JOIN Province p ON l.PROVINCE_ID = p.PROVINCE_ID ";

                      if(isset($_GET['province']) && !empty($_GET['province'])){
                        $sql_w .= "WHERE l.PROVINCE_ID = '".$_GET['province']."'";
                        if(isset($_GET['location']) && !empty($_GET['location'])){
                         $sql_w .= "AND w.LOCATION_ID = '".$_GET['location']."'";
                       }
                     }else if(isset($_GET['location']) && !empty($_GET['location'])){
                       $sql_w .= "WHERE w.LOCATION_ID = '".$_GET['location']."'";
                     }
                     $sql_w = mssql_query($sql_w) or die ("Error Query [".$sql_w."]");
                     while ($row_w = mssql_fetch_assoc($sql_w)){
                      if($row_w['POSITION_ID'] == $row['ID']){
                        echo '<option value="'.$row['ID'].'">'.$row['TITLE'].'</option>';
                      }
                    }
                  } ?>
                </select>
                <script type="text/javascript">
                  document.getElementById("work").value = "<?=$_GET["work"];?>";

                </script>
              </div>
              <div class="col-md-5 col-sm-9 col-xs-12">
                <?php 
                $sql_c ="SELECT DISTINCT co.ID, co.COMMENT FROM JOB_COMMENT co
                LEFT JOIN JOB_APPLICANT a ON co.ID = a.COMMENT_ID
                LEFT JOIN JOB_WORK w ON a.WORK_ID = w.ID ";

                if(isset($_GET['work']) && !empty($_GET['work'])){
                 $sql_c .= "WHERE w.POSITION_ID = '".$_GET['work']."' ";
               }  
               $query_c = mssql_query($sql_c) or die ("Error Query [".$sql_c."]");
               
               ?>
               <select id="com" name="com" onchange="document.test.submit();" class="form-control" >
                <option value="" >เลือกดูตามข้อมูลการสมัคร</option>
                <?php 
                while ($row_c = mssql_fetch_assoc($query_c)){
                    echo '<option value="'.$row_c['ID'].'">'.$row_c['COMMENT'].'</option>';
                }
                ?>
              </select>
              <script type="text/javascript">
                document.getElementById("com").value = "<?=$_GET["com"];?>";

              </script>
              <?php //echo $sql_c;?>
            </div>
          </div>
          <br>

        </form>
        <div class="row">


          <div class="col-md-3 col-sm-9 col-xs-12">


          </div>

        </div>

      </div>


      <table id="example" class="table table-striped table-bordered">
        <thead>
          <tr>
           <th><center>ชื่อ-นามสกุลผู้สมัคร</center></th>
           <th><center>ตำแหน่งงาน</center></th>
           <th><center>ข้อมูลการสมัครล่าสุด</center></th>
         </tr>
       </thead>


       <tbody>
        <?php  
        if($Num_Rows == 0){
          echo "<tr><td colspan='3'>ไม่พบข้อมูล</td></tr>";
        }
        for($i=$Page_Start;$i<$Page_End;$i++)
        {            

          ?>
          <tr>
            <td ><a href="report1.php?id=<?php echo mssql_result($objQuery,$i,"app_id"); ?>"><?php echo mssql_result($objQuery,$i,"PREFIX").mssql_result($objQuery,$i,"NAME")." ".mssql_result($objQuery,$i,"LASTNAME"); ?></a></td>
            <td><a href="report1.php?id=<?php echo mssql_result($objQuery,$i,"app_id"); ?>"><?php echo mssql_result($objQuery,$i,"TITLE"); ?></a></td>
            <td><a href="report1.php?id=<?php echo mssql_result($objQuery,$i,"app_id"); ?>">
              <center><?php echo mssql_result($objQuery,$i,"COMMENT"); ?></center></a>
            </td>

          </tr>
        <?php } ?>

      </tbody>
    </table>
    <br>
    <span>ทั้งหมด <?php echo $Num_Rows;?> รายการ : <?php echo $Num_Pages;?> หน้า : </span>

    <ul class="pagination">
      <?php if($Prev_Page){ ?>
        <li class="paginate_button previous " id="datatable_previous">
          <?php  echo " <a href='$_SERVER[SCRIPT_NAME]?Page=$Prev_Page$slug1$slug2$slug3'> Previous</a> "; ?>
        </li>
      <?php } ?>
      <?php  for($i=1; $i<=$Num_Pages; $i++){
        if($i != $Page)
          { ?>
            <li class="paginate_button ">
             <?php  echo "<a href='$_SERVER[SCRIPT_NAME]?Page=$i$slug1$slug2$slug3'>$i</a> "; ?>
           </li>
         <?php }
         else
          { ?>
            <li class="paginate_button active">
              <a href="#" aria-controls="datatable" data-dt-idx="1" tabindex="0">
                <?php echo $i; ?>
              </a>
            </li>
          <?php } }?>
          <?php  if($Page!=$Num_Pages)
          { ?>
            <li class="paginate_button next" id="datatable_next">
              <?php  echo " <a href ='$_SERVER[SCRIPT_NAME]?Page=$Next_Page$slug1$slug2$slug3'>Next</a> "; ?>
            </li>
          <?php }
          mssql_close($objConnect); ?>
        </ul>

      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>

<!-- jQuery -->
<script src="../../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../../vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="../../vendors/iCheck/icheck.min.js"></script>
<!-- jQuery Tags Input -->
<script src="../../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
<!-- Datatables -->
<script src="../../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../../vendors/jszip/dist/jszip.min.js"></script>
<script src="../../vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../../vendors/pdfmake/build/vfs_fonts.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="js/moment/moment.min.js"></script>
<script src="js/datepicker/daterangepicker.js"></script>

<!-- Custom Theme Scripts -->
<script src="../../build/js/custom.min.js"></script>

<!-- Angular -->
<script src="js/lib/angular.min.js"></script>
<!--App Controller -->
<script src="js/app.js"></script>
<script src="js/service.js"></script>
<script type="text/javascript">
  $('#example').dataTable( {
    "paging": false
  } );
  $('#example_info').hide();
</script>
</body>
</html>