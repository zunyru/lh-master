<?php
session_start();
include '../include/dbCon_mssql.php';
require_once '../include/function_date.php';
$id = $_GET['id'];

$sql = "SELECT a.ID,a.PREFIX,a.NAME,a.LASTNAME,a.NICKNAME,a.SEX,w.POSITION_ID,t.TITLE,w.LOCATION_ID,lp.PROVINCE_ID,lp.PROVINCE_NAME,l.LOCATION_NAME,FORMAT( a.CREATE_DAT, 'd', 'af') as DATE ,ac.PHONE,a.ATTACHMENT_FILE , a.PROFILE_COM,a.ATTACHMENTFILE_IMAGE
,a.RACE,a.NATIONSLITY ,a.RELIGION ,ac.ADDRESS ,ac.EMAIL,ac.NAME_EMER,ac.PHONE,ac.PHONE_EMER ,ai.SALARY_HOPE ,ai.START_DAY ,a.HEIGHT ,a.WEIGHT,a.BIRTHDAY ,a.CARD_NAMBER
,ai.POSITION , a.PRINT_TH,a.PRINT_EN , a.PROFILE_COM ,a.STATUS ,a.CHILDREN ,a.MOREDATA ,a.DRAFT
FROM
 JOB_APPLICANT a,JOB_COMMENT c,JOB_WORK w,JOB_TITLE t,JOB_LOCATION l,Province lp,JOB_APPLICANT_CONTACT ac,JOB_APPLICANT_JOB_INTERESTED ai
 WHERE a.COMMENT_ID = c.ID AND a.WORK_ID = w.ID AND w.POSITION_ID = t.ID AND w.LOCATION_ID = l.LOCATION_ID AND a.PROVINCE_ID = lp.PROVINCE_ID AND ac.APPLICANT_ID = a.ID AND ai.APPLICANT_ID = a.ID
AND a.ID = $id";

$query  = mssql_query($sql);
$data = mssql_fetch_assoc($query);

$sql2 = "SELECT p.PROVINCE_NAME FROM JOB_APPLICANT_CONTACT ac LEFT JOIN Province p 
ON ac.PROVINCE_ID = p.PROVINCE_ID WHERE ac.APPLICANT_ID = $id";
$query2  = mssql_query($sql2);
$data2 = mssql_fetch_assoc($query2);
//print_r($data);
?>
<html>
    <header>
        <title>Print Application form</title>

        <link rel="stylesheet" type="text/css"  href="css/LHPrint.css">
        <link rel="stylesheet" type="text/css" media="print" href="css/Print.css">
      
        
    </header>

    <body>
           <!--Main div--> 
        <div class = "container">
            <!--Logo-->
            <div class="logo">
                <img src ="../images/NEW_LOGO.jpg">

            </div>
            <div  class="profile">
                <img src="<?=$data['ATTACHMENTFILE_IMAGE'] == '' ? '../images/profileEx.jpg' : '../fileupload/candidacy/'.$data['ATTACHMENTFILE_IMAGE']?>">
            </div>
             <div >
              <span id="app">&nbsp;&nbsp;&nbsp;&nbsp;บริษัท แลนด์ แอนด์ เฮ้าส์ จำกัด(มหาชน)
                <br>LAND AND HOUSES PUBLIC CO.,Ltd.
               <br> <br>ใบสมัครงาน / APPLICATION FORM</span>
            </div>
            <!--Personal Data-->
            <div class="personal">
                <table class="info" border="0">
                    <tr>
                        <th colspan="8">ข้อมูลส่วนตัวผู้สมัคร (Personal Data)</th>
                        
                    </tr>
                    <tr class="text_span">
                        <td colspan="2"><p >ชื่อ (Name): <br><input class="info" type="text" value="<?=$data['PREFIX'].$data['NAME']?>" disabled></p></td>
                        <td colspan="2"><p >นามสกุล (Surname): <br><input class="info" type="text" value="<?=$data['LASTNAME']?>" disabled></p></td>
                        <td ><p >ชื่อเล่น (Nickname): <br><input class="info" type="text" value="<?=$data['NICKNAME']?>" disabled></p></td>
                        <td ><p >เพศ (Gender): <br><input class="info" type="text" value="<?=$data['SEX']=='M'? 'ชาย' : 'หญิง' ;?>" disabled></p></td>
                    </tr>
                    <tr class="text_span">
                        <td colspan="2"><p>ส่วนสูง (ซม.)/Height (cms.): <br><input class="info" type="text" value="<?=$data['HEIGHT']?>" size="3" disabled></p></td>
                        <td colspan="2"><p >น้ำหนัก (กก.)/Weight (kgs.): <br><input class="info" type="text" value="<?=$data['WEIGHT']?>" size="3" disabled> </p></td>
                         <td colspan="2"><p >หมายเลขบัตรประจำตัวประชาชน (ID. Card Number): <br><input  class="info" type="text" value="<?=$data['CARD_NAMBER']?>" disabled></p></td>
                    </tr>
                    <tr class="text_span">
                        <td colspan="2"><p >วัน/เดือน/ปีเกิด (Date of Birth): <br><input class="info" type="text" value="<?=MyDateThai($data['BIRTHDAY'])?>" size="8" disabled></p></td>
                        <td colspan="2"><p >อายุ (ปี)/Age (years): <br>
                          <input class="info" type="text" value="<?=Age($data['BIRTHDAY'])?>"  size="3" disabled>  </p></td>
                         <td colspan="2"><p >จังหวัดที่เกิด (Place of Birth): <br><input class="info" type="text" value="<?=$data['PROVINCE_NAME']?>" disabled> </p></td>
                    </tr>

                    <tr class="text_span">
                        <td colspan="2"><p >สัญชาติ (Nationality): <br><input class="info" type="text" value="<?=$data['NATIONSLITY']?>" disabled></p></td>
                        <td colspan="2"><p >เชื้อชาติ (Race): <br><input class="info" type="text" value="<?=$data['RACE']?>" disabled> </p></td>
                        <td colspan="2"><p >ศาสนา (Religion): <br><input class="info" type="text" value="<?=$data['RELIGION']?>" disabled></p></td>
                    </tr>
                    <tr class="text_span">
<!--                        <td colspan="2"><p >เลขทีประจำตัวผูเ้สียภาษี(Tax Identification): <input class="info" type="text" value="1111124358679" disabled></p></td>-->

                        <td colspan="8" >
                        <p >ที่อยู่ปัจจุบันที่ติดต่อได้ (Present Address): <br><span class="p_size p_line">
                          <?=$data['ADDRESS']." จังหวัด ".$data2['PROVINCE_NAME']?>
                        </span></p>
                         
                         </td>
<!--                        <td colspan="2"><p >โทรศัพท์ทีบ้าน(Home Telephone No.): <input class="info" type="text" value="" disabled> </p></td>-->
<!--                        <td colspan="2"> <p >ทีทำงาน(Office No.): <input class="info" type="text" value="" disabled></p></td>-->
                   </tr>
		   <tr class="text_span">   
                        <td colspan="2"> <p >โทรศัพท์มือถือ (Phone No.): <br><input class="info" type="text" value="<?=$data['PHONE']?>" disabled> </p></td>
                       <td colspan="3"> <p>อีเมล์ (E-mail): <br><input class="info" type="text" value="<?=$data['EMAIL']?>" disabled></p></td>
                    </tr>

                    <tr class="text_span">
                     <td colspan="6">
                         <b><p>กรณีเร่งด่วน บุคคลสามารถติดต่อแทนได้ (In case of emergency, please notify)</p></b>
                     </td>
                    
                     </tr> 
                     <tr class="text_span">
                         <td colspan="4"><p>ชือ-นามสกุล (Name-Surname): <br><input class="info" type="text" value="<?=$data['NAME_EMER']?>" disabled></p></td>
                         <td colspan="2">
                        <p>โทรศัพท์ (Telephone No.): <br><input class="info" type="text" value="<?=$data['PHONE_EMER']?>" disabled style="width: 47%;"></p>
                    </td> 
                     </tr>
                </table>
            </div>

            <!--Position and Salary-->
            <div class="position">
                <table>
                    <tr>
                        <th style="width:55%" colspan="8">ตำแหน่งและเงินเดือน (Position and Salary)</th>
                        
                    </tr>

                    <tr>
                        <td colspan="4" >
                            <p >ตำแหน่งงานหรือประเภทงานทีสมัคร (Position or work applied for)</p>
                        </td>

                         <td colspan="4">
                            <p class="position_p">เงินเดือนทีต้องการ (บาท/เดือน)/Salary desired (Bath/Month)<input class="info" type="text" value="<?=$data['SALARY_HOPE']?>" disabled style = "width: 82%;"></P>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:55%" colspan="4">
                            <p class="infoPosition">1.<input style="width: 90%;" type="text" value="<?=$data['POSITION']?>" disabled ></p>
                        </td>
                        <td style="width:55%" rowspan="2" colspan="4">
                            <p class="position_p">ถ้าบริษัทฯ รับเข้าทำงาน จะเริ่มงานได้ตั้งแต่วันที (If employed, will start on) <br><input class="info" type="text" value="<?=MyDateThai($data['START_DAY'])?>" disabled style = "width: 82%;"></p>
                            
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <p class="infoPosition">2.<input style="width: 90%;" type="text" value="" disabled></p>
                        </td>
                    </tr>

                </table>

            </div>

            <!--For interviewer-->

            <div class="interview">
                 <div><h3>ห้ามเขียนช่องข้างล่างนี้/ DO NOT WRITE BELOW THIS LINE</h3></div>
                <table>
                    <tr>
                        <td colspan="3">
                            <p>ว่าจ้างในตำแหน่ง (To be hired as: Title)<input type="text" value="" disabled></p>
                        </td>
                        <td colspan="1">
                            <p>เงินเดือน (บาท/เดือน)/Salary (Baht/month)<input type="text" value="" disabled></p>
                        </td>
                        <td colspan="1">
                            <p>เริ่มงาน (Starting Date) <input  type="text" value="" disabled></p>
                        </td>
                 </tr>
                <tr>
                    <td colspan="1">
                        <p>ฝ่าย (Department) <input type="text" value="" disabled></p>
                    </td>

                    <td colspan="1">
                        <p>ระดับ (Level) <input type="text" value="" disabled></p>
                    </td>

                    <td colspan="1">
                        <p>รหัส (Code) <input type="text" value="" disabled></p>
                    </td>

                    <td colspan="2">
                        <p>กรรมการฯ อนุมัติ (Authorized by) <br><input type="text" value="" disabled   style="width: 42%;" ></p>
                    </td>
                </tr>
                </table>
                <!--Signature and Comment blocks-->
            <div class="comment">
                <table>
                    <tr>
                        <td>
                           <p style="margin-left:0px"> ความเห็นและลายมือผู้สัมภาษณ์ <br/>(Interviewer's Signature & Comment) </P>
                        <textarea rows="8" cols="60" disabled></textarea>
                        
                        </td>

                        <td class="middleline">
                           <p style="margin-left:0px"> ความเห็นและลายมือผู้สัมภาษณ์ <br/>(Interviewer's Signature & Comment) </P>
                        <textarea rows="8" cols="60" disabled></textarea>
                        
                        </td>
                    </tr>

                </table>
            </div>
            </div>

            <!--Education Background-->

            <div class="education">
                <table >
            
            <tr>
                <th colspan="7">ประวัติการศึกษา (Education Background)</th>
            </tr>
            <tr>
                <td style="width: 7%;"><p>ระดับการศึกษา (Education)</p></td>
                <td style="width: 24%;"><p>สถานศึกษา (Institute)</p></td>
                <td style="width: 20%;"><p>คณะ (Faculty)</p></td>
                <td style="width: 18%;"><p>วิชาเอก/สาขา (Major)</p></td>
                <td style="width: 18%;"><p>วิขาโท/สาขา (Minor)</p></td>
                <td style="width:10%;"><p>เดือน/ปีทีจบ (Graduation Date)</p></td>
                <td style="width:5%"><p>GPA</p></td>
               
            </tr>
                    <?php
                     $sql_edu = "SELECT * FROM JOB_APPLICANT_EDUCATION edu WHERE APPLICANT_ID = '".$id."'";
                      $query_edu = mssql_query($sql_edu);
                      $num_edu = mssql_num_rows($query_edu);
                     while ($row_edu = mssql_fetch_assoc($query_edu)){

                    ?>
           <tr >
           		<td><p class="p_size p_center"><?php 
                  if($row_edu['GRADUATE_ID'] == 1){
                	echo 'ปวช.';
                }elseif($row_edu['GRADUATE_ID'] == 2){
                	echo 'ปวส';
                }elseif($row_edu['GRADUATE_ID'] == 3){
                	echo 'ป.ตรี';
                }elseif($row_edu['GRADUATE_ID'] == 4){
                	echo 'ป.โท';
                }elseif($row_edu['GRADUATE_ID'] == 5){
                  echo 'ป.เอก';
                } ?></p></td>
                <td><p class="p_size "><?=$row_edu['INSTITUTION']?></p></td>
                <td><p class="p_size"><?=$row_edu['FACULTY']?></p></td>
                <td><p class="p_size "><?=$row_edu['BRANCH']?></p></td>
                <td><p class="p_size "><input type="text" value="" disabled></p></td>
                <td><p class="p_size p_center"><?=DateThai_edu($row_edu['DATE_STOP'])?></p></td>
                <td><p class="p_size p_center"><?=$row_edu['GPA']?></p></td>
            </tr>

                    <?php
                         }
                        for($num_edu; $num_edu < 4 ; $num_edu++){

                    ?>

             <tr >
                <td><p><input type="text" value="" disabled></p></td>
                <td><p><input type="text" value="" disabled></p></td>
                <td><p><input type="text" value="" disabled></p></td>
                <td><p><input type="text" value="" disabled></p></td>
                <td><p><input type="text" value="" disabled></p></td>
                <td><p><input type="text" value="" disabled></p></td>
                <td><p><input type="text" value="" disabled></p></td>
            </tr>
                    <?php }?>

        </table>

            </div>

            <!--Special Skills-->
            <div class="skills">
                 <table border="1">
            <tr>
                <th colspan="14">ความสามารถพิเศษ (Special Skills)</th>
            </tr>
       
             <tr>
                <td style="width:10rem;" rowspan="2" colspan="2"><p>ภาษา<br>(Language)</p></td>
                <td colspan="3"><p>พูด (Speaking)</p></td>
                <td colspan="3"><p>อ่าน (Reading)</p></td>
                <td colspan="3"><p>เขียน (Writing)</p></td>
                <td rowspan="2" colspan="3" ><p>ความสามารถในการใช้โปรแกรมคอมพิวเตอร์<br>(Computer Ability)</p></td>
             </tr>
            <tr class="evaSize">
                
                <td ><p>ดีมาก</p></td>
                <td ><p>ดี</p></td>
                <td ><p>ดีพอใช้</p></td>
                <td ><p>ดีมาก</p></td>
                <td ><p>ดี</p></td>
                <td ><p>ดีพอใช้</p></td>
                <td ><p>ดีมาก</p></td>
                <td ><p>ดี</p></td>
                <td ><p>ดีพอใช้</p></td>
                
            </tr>
                     <?php
                       $sql_l = "SELECT * FROM JOB_APPLICANT_LANGUAGE WHERE APPLICANT_ID = '$id'";
                       $query_l = mssql_query($sql_l);
                       $num_l = mssql_num_rows($query_l);
                       if($num_l > 0){
                       $i=0;
                           $lang1 =array();
                           $lang2 =array();
                           $lang3 =array();
                         while ($row_l = mssql_fetch_assoc($query_l)){
                             array_push($lang1,$row_l['TALK']);
                             array_push($lang2,$row_l['READS']);
                             array_push($lang3,$row_l['WRITE']);

                             //print_r($lang2[0]);
                             //echo $i;
                     ?>

            <tr>
                <td colspan="2"><p><input type="text" value="<?=$row_l['LANGUAGE']?>" disabled></p></td>
                <td ><p><input type="text" value="<?php if($lang1[$i] == 'ดีมาก'){ echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/';}?>" disabled></p></td>
                <td ><p><input type="text" value="<?php if($lang1[$i] == 'ดี'){ echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/';}?>" disabled></p></td>
                <td ><p><input type="text" value="<?php if($lang1[$i] == 'พอใช้'){ echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/';}?>" disabled></p></td>

                <td ><p><input type="text" value="<?php if($lang2[$i] == 'ดีมาก'){ echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/';}?>" disabled></p></td>
                <td ><p><input type="text" value="<?php if($lang2[$i] == 'ดี'){ echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/';}?>" disabled></p></td>
                <td ><p><input type="text" value="<?php if($lang2[$i] == 'พอใช้'){ echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/';}?>" disabled></p></td>

                <td ><p><input type="text" value="<?php if($lang3[$i] == 'ดีมาก'){ echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/';}?>" disabled></p></td>
                <td ><p><input type="text" value="<?php if($lang3[$i] == 'ดี'){ echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/';}?>" disabled></p></td>
                <td ><p><input type="text" value="<?php if($lang3[$i] == 'พอใช้'){ echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/';}?>" disabled></p></td>

                <?php if($i == 0){?>
                <td rowspan="4" style="max-width: 1000px;"><p><?=$data['PROFILE_COM']?></p></td>
                <?php }?>
            </tr>
                     <?php $i++; }?>

            <?php for($num_l; $num_l <=1 ; $num_l++){ ?>
            <tr>
                <td colspan="2"><p><input type="text" value="" disabled></p></td>
                <td><p><input type="text" value="" disabled></p></td>
                <td><p><input type="text" value="" disabled></p></td>
                <td><p><input type="text" value="" disabled></p></td>
                <td><p><input type="text" value="" disabled></p></td>
                <td><p><input type="text" value="" disabled></p></td>
                <td><p><input type="text" value="" disabled></p></td>
                <td><p><input type="text" value="" disabled></p></td>
                <td><p><input type="text" value="" disabled></p></td>
                <td><p><input type="text" value="" disabled></p></td>
             
            </tr>
                     <?php
                            } }else{

                           for ($j=0; $j <=3; $j++){
                           ?>

                     <tr>
                         <td colspan="2"><p><input type="text" value="" disabled></p></td>
                         <td ><p><input type="text" value="" disabled></p></td>
                         <td ><p><input type="text" value="" disabled></p></td>
                         <td ><p><input type="text" value="" disabled></p></td>
                         <td ><p><input type="text" value="" disabled></p></td>
                         <td ><p><input type="text" value="" disabled></p></td>
                         <td ><p><input type="text" value="" disabled></p></td>
                         <td ><p><input type="text" value="" disabled></p></td>
                         <td ><p><input type="text" value="" disabled></p></td>
                         <td ><p><input type="text" value="" disabled></p></td>
                         <?php if($j == 0){?>
                         <td  rowspan="4"><p class="p_size"><?=$data['PROFILE_COM']?></p></td>     
                         <?php } ?>
                     </tr>
                   <?php }}?>


         </table>
            </div>

         <!--Typing-->

        <div class="typing">
            <p>พิมพ์ดีด (Typing) &nbsp; ไทย (Thai)<input type="text" value="<?="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$data['PRINT_TH']?>" disabled>&nbsp; อังกฤษ (English)<input type="text" value="<?="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$data['PRINT_EN']?>" disabled></p>
        </div>

        <!--Family Details-->
            <div class="family">
                <table >

                    <tr>
                        <th colspan="5">รายละเอียดครอบครัว (Family Details)</th>
                        
                    </tr>
            <tr>
                <td><p>สถานภาพสมรส (Marital Status)</p> </td>
                <td colspan="4"><p>
	                <p class="marital"> <input  type="text" value="<?=$data['STATUS']?>" disabled /> </p>
	            
                </td>
            </tr>
             <tr>
                <td style="width:17rem;" ><p>ครอบครัว (Family Details)</p></td>
                <td  ><p>ชื่อ-นามสกุล (Name-Surname)</p></td>
                <td style="width: 5%;"><p>อายุ (Age)</p></td>
                <td style="width: 17%;"><p>อาชีพ/ตำแหน่ง (Occupation/Position)</p></td>
                <td><p>ที่อยู่/ที่ทำงาน (Address/Office)</p></td>
                
                </tr>
                    <?php
                    $sql_f ="SELECT * FROM JOB_APPLICANT_FAMILY WHERE APPLICANT_ID = '".$id."' AND STATUS = 'บิดา' ";
                    $query_f = mssql_query($sql_f);
                    $row_f = mssql_fetch_assoc($query_f);

                    $sql_m ="SELECT * FROM JOB_APPLICANT_FAMILY WHERE APPLICANT_ID = '".$id."' AND STATUS = 'มารดา' ";
                    $query_m = mssql_query($sql_m);
                    $row_m = mssql_fetch_assoc($query_m);

                    $sql_n1 ="SELECT * FROM JOB_APPLICANT_FAMILY WHERE APPLICANT_ID = '".$id."' AND STATUS = 'พี่น้อง1' ";
                    $query_n1 = mssql_query($sql_n1);
                    $row_n1 = mssql_fetch_assoc($query_n1);

                    $sql_n2 ="SELECT * FROM JOB_APPLICANT_FAMILY WHERE APPLICANT_ID = '".$id."' AND STATUS = 'พี่น้อง2' ";
                    $query_n2 = mssql_query($sql_n2);
                    $row_n2 = mssql_fetch_assoc($query_n2);

                    $sql_n3 ="SELECT * FROM JOB_APPLICANT_FAMILY WHERE APPLICANT_ID = '".$id."' AND STATUS = 'พี่น้อง3' ";
                    $query_n3 = mssql_query($sql_n3);
                    $row_n3 = mssql_fetch_assoc($query_n3);

                    $sql_n4 ="SELECT * FROM JOB_APPLICANT_FAMILY WHERE APPLICANT_ID = '".$id."' AND STATUS = 'พี่น้อง4' ";
                    $query_n4 = mssql_query($sql_n4);
                    $row_n4 = mssql_fetch_assoc($query_n4);

                    $sql_n5 ="SELECT * FROM JOB_APPLICANT_FAMILY WHERE APPLICANT_ID = '".$id."' AND STATUS = 'พี่น้อง5' ";
                    $query_n5 = mssql_query($sql_n5);
                    $row_n5 = mssql_fetch_assoc($query_n5);

                    $sql_q ="SELECT * FROM JOB_APPLICANT_FAMILY WHERE APPLICANT_ID = '".$id."' AND STATUS = 'คู่สมรส' ";
                    $query_q = mssql_query($sql_q);
                    $row_q = mssql_fetch_assoc($query_q);



                    ?>
             <tr>
                 <td><p>บิดา (Father)</p></td>
                 <td><p class="p_size"><?=$row_f['NAME']?></p></td>
                 <td><p><input type="text" value="<?=$row_f['AGE']?>" disabled></p></td>
                 <td><p class="p_size"><?=$row_f['CAREER']?></p></td>
                 <td><p class="p_size"><?=$row_f['ADDRESS']?></p></td>
             </tr>
             <tr>
                 <td><p>มารดา (Mother)</p></td>
                 <td><p class="p_size"><?=$row_m['NAME']?></p></td>
                 <td><p><input type="text" value="<?=$row_m['AGE']?>" disabled></p></td>
                 <td><p class="p_size"><?=$row_m['CAREER']?></p></td>
                 <td><p class="p_size"><?=$row_m['ADDRESS']?></p></td>
             </tr>
             <tr>
                 <td rowspan="5"><p class="underline-fam">พี่น้อง (Brother/Sister)<!--<input type="text" value="" disabled>(คน)--></p></td>
                     <td><p class="p_size"><?=$row_n1['NAME']?></p></td>
                     <td><p><input type="text" value="<?=$row_n1['AGE']?>" disabled></p></td>
                     <td><p class="p_size"><?=$row_n1['CAREER']?></p></td>
                     <td><p class="p_size"><?=$row_n1['ADDRESS']?></p></td>
                 <tr>
                    <td><p class="p_size"><?=$row_n2['NAME']?></p></td>
                    <td><p><input type="text" value="<?=$row_n2['AGE']?>" disabled></p></td>
                    <td><p class="p_size"><?=$row_n2['CAREER']?></p></td>
                    <td><p class="p_size"><?=$row_n2['ADDRESS']?></p></td>
                 </tr>
                  <tr>
                      <td><p class="p_size"><?=$row_n3['NAME']?></p></td>
                      <td><p><input type="text" value="<?=$row_n3['AGE']?>" disabled></p></td>
                      <td><p class="p_size"><?=$row_n3['CAREER']?></p></td>
                      <td><p class="p_size"><?=$row_n3['ADDRESS']?></p></td>
                 </tr>
                  <tr>
                    <td><p class="p_size"><?=$row_n4['NAME']?></p></td>
                    <td><p><input type="text" value="<?=$row_n4['AGE']?>" disabled></p></td>
                    <td><p class="p_size"><?=$row_n4['CAREER']?></p></td>
                    <td><p class="p_size"><?=$row_n4['ADDRESS']?></p></td>
                 </tr>
                  <tr>
                      <td><p class="p_size"><?=$row_n5['NAME']?></p></td>
                      <td><p><input type="text" value="<?=$row_n5['AGE']?>" disabled></p></td>
                      <td><p class="p_size"><?=$row_n5['CAREER']?></p></td>
                      <td><p class="p_size"><?=$row_n5['ADDRESS']?></p></td>
                 </tr>
             </tr>
             <tr>
                 <td><p>คู่สมรส (Spouse)</p></td>
                 <td><p class="p_size"><?=$row_q['NAME']?></p></td>
                 <td><p><input type="text" value="<?=$row_q['AGE']?>" disabled></p></td>
                 <td><p class="p_size"><?=$row_q['CAREER']?></p></td>
                 <td><p class="p_size"><?=$row_q['ADDRESS']?></p></td>
             </tr>
             <tr>
                 <td colspan="5"><p class="underline-fam" style="text-align:left;margin-left:0px">จำนวนบุตร (Number of children)<input type="text" value="<?=$data['CHILDREN']?>" disabled>คน</p></td>
                 
             </tr>
                </table>
            </div>

            <!--Employment experience-->

        <div class="employment">
            <table >
                <tr>
                   <th  style="width: 10%;" colspan="8">ประวัติการทำงาน (เรียงจากปัจจุบันไปหาอดีต)/ Employment (List Last Employment First)</th>            
                </tr>
              <tr>
                 <td rowspan="2"><center><p>ชื่อบริษัทและที่อยู่<br>(Employer's Name and Address)</p></center></td>
                 <td colspan="2"><center><p>วัน/เดือน/ปี ที่ทำงาน (Date of Employment)</p></center></td>
                 <td rowspan="2"><center><p>ตำแหน่ง/ลักษณะงาน<br>(Position / Name of work)</p></center></td>
                 <td rowspan="2"><center><p>เงินเดือน (Salary)</p></center></td>
                 <td rowspan="2"><center><p>รายได้อื่นๆ (บาท)</p></center></td>
                 <td rowspan="2"><center><p>ที่มารายได้อื่นๆ</p></center></td>
                 <td rowspan="2"><center><p>เหตุที่ออก<br>(Reason for Leaving)</p></center></td>
             </tr>
              <tr>
                 
                 <td style="width:5rem"><center><p>ตั้งแต่ (From)</p></center></td>
                 <td style="width:5rem"><center><p>ถึง (To)</p></center></td>
                
             </tr>

                <?php
                  $sql_j = "SELECT * FROM JOB_APPLICANT_JOB_HISTORY WHERE APPLICANT_ID = '".$id."' ORDER BY ID ASC";
                  $query_j = mssql_query($sql_j);
                  $num_j = mssql_num_rows($query_j);
                 while ($row_j = mssql_fetch_assoc($query_j)){
                ?>
              <tr>
                 <td><p class="p_size"><?=$row_j['COMPANY']?></p></td>
                 <td><p class="p_size p_center"><?=DateThaiMount($row_j['DATE_START'])?></p></td>
                 <td><p class="p_size p_center"><?=$row_j['DATE_STOP'] == 'ปัจจุบัน' ? 'ปัจจุบัน' : DateThaiMount($row_j['DATE_STOP']);?></p></td>
                 <td><p class="p_size"><?=$row_j['POSITION']?></p></td>
                 <td><p class="p_size p_center"><?=number_format($row_j['SALARY'])?></p></td>
                 <td><p class="p_size p_center"><?=number_format($row_j['OTHER_SALARY'])?></p></td>
                 <td><p class="p_size"><?=$row_j['MORE_OTHER_SALARY']?></p></td>
                 <td><p class="p_size"><?=$row_j['FEATURES']?></p></td>   
                
             </tr>
                <?php }
                   for($num_j; $num_j < 6 ; $num_j++){
                ?>

                <tr>
                    <td><p><input type="text" value="" disabled></p></td>
                    <td><p><input type="text" value="" disabled></p></td>
                    <td><p><input type="text" value="" disabled></p></td>
                    <td><p><input type="text" value="" disabled></p></td>
                    <td><p><input type="text" value="" disabled></p></td>
                    <td><p><input type="text" value="" disabled></p></td>
                    <td><p><input type="text" value="" disabled></p></td>
                    <td><p><input type="text" value="" disabled></p></td>

                </tr>
                <?php }?>



            </table>
                

        </div>

        <!--General Data-->
            <?php
              $sql_d = "SELECT * FROM JOB_APPLICANT_DATAS WHERE APPLICANT_ID = '$id'";
              $qeury_d  = mssql_query($sql_d);
              $row_d = mssql_fetch_assoc($qeury_d);
            ?>
        <div class="general" >
            <table style="text-align: left !important;">
            <tr>
                   <th colspan="7">ข้อมูลทั่วไป (General Data)</th>            
            </tr>
            <tr>
                <td><center>
                   <p> ข้อ (Number)</p>
                   </center>
                </td>
                <td colspan="2"><center>
                    <p>คำถาม (Question)</p></center>
                </td>
                
                <td><center>
                    <p>คำตอบ (Answer)</p></center>
                </td>
                <td><center>
                   <p>หมายเหตุ (Note/Reason)</p></center>
                </td>
            </tr>
            <tr>
                <td rowspan="2"><center><p>1.</p></center></td>
                <td rowspan="2"><p>การไปปฏิบัติงาน ณ โครงการ<br>Can you work at company's site?</p></td>
                <td><p>เป็นประจำ<br>Permanent</p></td>
                <td><p><input type=text value="<?php $str =  explode(" ",$row_d['WORKING_ALWAYS']); if($str[0] == 'N'){ echo 'ขัดข้อง';}else{echo 'ไม่ขัดข้อง';} ?>" disabled></p></td>
                <td><p class="p_size"><?=str_replace($str[0], '', $row_d['WORKING_ALWAYS']);?></p></td>
            </tr>
            <tr> 
                <td><p>เป็นครั้งคราว<br>Temporary</p></td>
                <td><p><input type=text value="<?php $str2 =  explode(" ",$row_d['WORKING_SOMETIMES']); if($str2[0] == 'N'){ echo 'ขัดข้อง';}else{echo 'ไม่ขัดข้อง';} ?>" disabled></p></td>
                <td><p class="p_sizes"><?=str_replace($str2[0], '', $row_d['WORKING_SOMETIMES']);?></p></td>
            </tr>
<!--            <tr>-->
<!--                <td><p>2.</p></td>-->
<!--                <td colspan="2"><p>การเจ็บป่วยขนาดหนัก หรือโรคติดต่อร้าย<br>Have you ever been seriously ill or contacted with contagious disease?</p></td>-->
<!--                <td><p><input type=text value="" disabled></p></td>-->
<!--                <td><p><input type=text value="" disabled></p></td>-->
<!--            </tr>-->
            <tr>
                <td><center><p>2.</p></center></td>
                <td colspan="2"><p>โรคประจำตัว<br>Any physical disability or handicap?</p></td>
                <td><p><input type=text value="<?php $str3 =  explode(" ",$row_d['DISEASE']); if($str3[0] == 'N'){ echo 'ไม่มี';}else{echo 'มี';} ?>" disabled></p></td>
                <td><p class="p_sizes"><?=str_replace($str3[0], '', $row_d['DISEASE']);?></p></td>
            </tr>
            <tr>
                <td><center><p>3.</p></center></td>
                <td colspan="2"><p>เคยถูกจำคุกหรือต้องโทษทางอาญาหรือไม่<br>Have you ever been arrested, taken into custory, held for investigation
or questioning or charged by any law enforcement authority?</p></td>
                <td><p><input type=text value="<?php $str4 =  explode(" ",$row_d['CRIME']); if($str4[0] == 'N'){ echo 'ไม่มี';}else{echo 'มี';} ?>" disabled></p></td>
                <td><p class="p_sizes"><?=str_replace($str4[0], '', $row_d['CRIME']);?></p></td>
            </tr>
             <tr>
                <td><center><p>4.</p></center></td>
                <td colspan="2"><p>เคยถูกให้ออกจากงานหรือเลิกจ้างหรือไม่<br>Have you ever been discharged from employment for any reason?</p></td>
                <td><p><input type=text value="<?php $str5 =  explode(" ",$row_d['LAY_OFF']); if($str5[0] == 'N'){ echo 'ไม่มี';}else{echo 'มี';} ?>" disabled></p></td>
                <td><p class="p_size"><?=str_replace($str5[0], '', $row_d['LAY_OFF']);?></p></td>
            </tr>
            <tr>
                <td><center><p>5.</p></center></td>
                <td colspan="2"><p>ท่านได้ผ่านการเกณฑ์ทหารหรือไม่<br>Military Status?</p></td>
                <td><strong><?=$data['DRAFT']?><strong></td>
                <td><p><input type=text value="" disabled></p></td>
            </tr>
            <tr>
                <td><center><p>6.</p></center></td>
                <td colspan="2"><p>ท่านมีหรือชอบงานอดิเรกอะไรบ้าง<br>What are your hobbies or interests?</p></td>
                <td colspan="2"><p><input type=text value="" disabled></p></td>
<!--                <td><p><input type=text value="" disabled></p></td>-->
            </tr>
            <tr>
                <td><center><p>7.</p></center></td>
                <td colspan="2"><p>ให้เขียนชื่อญาติหรือเพื่อนที่ทำงานในบริษัทนี้<br>List relatives or friends in this company?</p></td>
                <td  colspan="2"><p><input type=text value="" disabled></p></td>
<!--                <td><p><input type=text value="" disabled></p></td>-->
            </tr>
        </table>
        </div>


        <!--Personal Reference-->

        <div class="reference">
            <table>
                <tr>
                   <th colspan="5">ผู้ให้การรับรอง (Personal Reference)</th>            
                </tr>
           
            <tr>
                <td><p>ชื่อผู้รับรอง (Name)</p></td>
                <td><p>ที่อยู่ของผู้รับรอง (Address)</p></td>
                <td><p>หมายเลขโทรศัพท์ (Tel. No.)</p></td>
                <td><p>ตำแหน่ง (Position)</p></td>
                <td><p>ความสัมพันธ์ทางสายอาชีพ (Professional Relationship)</p></td>
            </tr>
                <?php
                  $sql_r = "SELECT * FROM JOB_APPLICANT_PERSON WHERE APPLICANT_ID  = '$id'";
                  $query_r = mssql_query($sql_r);
                  $row_r = mssql_fetch_assoc($query_r);
                ?>
            <tr>
                <td><p class="p_size"><?=$row_r['NAME']?></p></td>
                <td><p class="p_size"><?=$row_r['COMPANY']?></p></td>
                <td style="width: 12%"><p class="p_size"><?=$row_r['TELPERSON']?></p></td>
                <td><p class="p_size"><?=$row_r['POSITION']?></p></td>
                <td style="width: 20%;"><p class="p_size"><?=$row_r['RELATIONSHIP']?></p></td>
            </tr>
        </table>
        </div>

        <!--Fuether Inforamtion-->

        <div class="further-info">
        <table>
            <tr>
                <th>ข้อมูลเพิ่มเติม (Further Information)</th>            
            </th>
            <tr>
                <td><textarea rows="<?=strlen($data['MOREDATA']) < 1000 ? 10 : 25 ;?>" cols="1" disabled><?=$data['MOREDATA']?></textarea></td>
            </tr>
        </table>
        </div>


        <!--Consent part-->

        <div class="consent">
            <p>&nbsp; ข้าพเจ้าขอรับรองว่า ข้อความดังกล่าวทั้งหมดในใบสมัครนี้เป็นความจริงทุกประกา หากหลังจากบริษัทฯจ้างเข้าทำงาน ปรากฎว่าข้อความในใบสมัครงานเอกสาร
            ที่นำมาแสดง หรือรายละเอียดที่ให้ไว้ไม่เป็นความจริง บริษัทฯมีสิทธิเลิกจ้างข้าพเจ้าโดยไม่ต้องบอกกล่าวล่วงหน้า และไม่ต้องจ่ายเงินค่าชดเชย หรือค่าเสียหายใดๆทั้งสิ้น</p>
            <p>&nbsp; I certify that all statements given in this application form are true. If any found to be untrue after employment, the Company has the right to terminate my
            employment without advance notice or any compensation or severance pay whatoever.</p>
        </div>

        <div class="signature">
            <table>
                <tr>
                    <td ><p>วันที่ (Date)<input type="text" value="<?= DateThaiYear($data['DATE']);?>" disabled></p></td>
                    <td ><p><input type="text" value="" disabled></p></td>
                </tr>
                <tr>
                    <td></td>
                    <td ><p>ลายมือชื่อผู้สมัคร / Applicant's Signature</p></td>
                </tr>
            </table>
        </div>

            <p class="ptext"><a href="#" onclick="MyPrint();">Print</a></p>

        </div>
        </div>

           <script>
               function MyPrint() {
                   window.print();
               }
           </script>

    </body>

</html>