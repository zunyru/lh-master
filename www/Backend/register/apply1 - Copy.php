<?php
session_start();
$_SESSION['group_id'];
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>

    <!-- Bootstrap -->
    <link href="../../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md" ng-app="apply"  ng-controller="applyController" >
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="#" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

              <?php  include  '../master/navbar_regis.php' ?>
            <!-- /menu footer buttons -->
          </div>
        </div>

          <!-- top navigation -->
          <?php include '../master/top_nav_regis.php'; ?>
          <!-- /top navigation -->
        
        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                   <!-- menu--> 
                    <div class="bs-example-popovers">
                        <a href="apply1.php" class="btn btn-success active">
                           ตำแหน่งงานว่าง 
                        </a>
                        <a href="candidacy1.php" class="btn btn-default">
                           ผู้สมัครงาน 
                        </a>
                       <a href="report.php" class="btn btn-default">
                           รายงาน 
                        </a>
                        <a href="position1.php" class="btn btn-default">
                           ชื่อตำแหน่งงาน 
                        </a>
                        <a href="news1.php" class="btn btn-default">
                           ข่าวสาร 
                        </a>
                        <a href="area1.php" class="btn btn-default">
                           กำหนดพื้นที่ทำงาน 
                        </a>
                        <a href="user1.php" class="btn btn-default">
                           ผู้ใช้งาน 
                        </a>
                    </div>
                   <br>
                  <!-- -->
                   
                  <div class="x_title">
                    <h2>ข้อมูลตำแหน่งงานว่าง</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                

                    <a href="apply2.php"><button type="button" class="btn btn-success flright">สร้างข้อมูล ตำแหน่งงานว่าง +</button></a>
                    <!-- <p class="text-muted font-13 m-b-30">
                      DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>
                    </p> -->
                    <div class="row">

                    <div class="col-md-5 col-sm-9 col-xs-12">
                          <select class="form-control" ng-model="filter.position" >
                            <option value="{{undefined}}" >เลือกดูตามตำแหน่งงานทั้งหมด</option>
                            <option ng-repeat="dataArr in position" value="{{dataArr.ID}}" >{{ dataArr.TITLE }}</option>
                          </select>
                    </div>
                   </div>

                   <div class="row">
                     <div class="col-md-2 col-sm-9 col-xs-12">
                           <select class="form-control" ng-model="filter.province"  ng-change="filter.area=undefined" >
                            <option value="{{undefined}}" >เลือกดูตามจังหวัด</option>
                            <option ng-repeat="dataArr in province" value="{{dataArr.PROVINCE_ID}}" >{{ dataArr.PROVINCE_NAME }}</option>
                          </select>
                    </div>  

                    <div class="col-md-3 col-sm-9 col-xs-12">
                          <select class="form-control" ng-model="filter.area" >
                            <option value="{{undefined}}" >เลือกดูตามพื้นที่ทำงาน</option>
                            <option  ng-if="filter.province" ng-repeat="dataArr in area | filter:{PROVINCE_ID: filter.province} " value="{{dataArr.LOCATION_ID}}" >{{ dataArr.LOCATION_NAME }}</option>
                          </select>
                    </div>

                    </div>

                    </div>

                         
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th><center>ตำแหน่งงาน</center></th>
                          <th><center>จังหวัด</center></th>
                          <th><center>พื้นที่ทำงาน</center></th>
                          <th><center>อัตรา</center></th>
                          <th><center>วันที่ประกาศ</center></th>
                          <th><center>แสดงบน Web</center></th>
                        </tr>
                      </thead>


                      <tbody>

                        <tr ng-repeat="contact in data_ | filter:{'POSITION_ID': filter.position} | filter: {'PROVINCE_ID': filter.province} | filter:{'LOCATION_ID': filter.area} " >
                          <td><a href="apply3.php?id={{contact.ID}}">{{ contact.TITLE }}</a></td>
                          <td><a href="apply3.php?id={{contact.ID}}"><center>{{ contact.PROVINCE_NAME }}</center></a></td>
                          <td><a href="apply3.php?id={{contact.ID}}"><center>{{ contact.LOCATION_NAME }}</center></a></td>
                          <td><a href="apply3.php?id={{contact.ID}}"><center>{{ contact.RATES }}</center></a></td>
                          <td><a href="apply3.php?id={{contact.ID}}"><center>{{ dateFormat(contact.DATE) }}</center></a></td>
                          <td><a href="apply3.php?id={{contact.ID}}">
                          <center ng-if="contact.IS_SHOW" >แสดง</center><center ng-if="!contact.IS_SHOW" >-</center>
                          </a></td>
                        </tr>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- jQuery -->
    <script src="../../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../../vendors/iCheck/icheck.min.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Datatables -->
    <script src="../../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../../vendors/pdfmake/build/vfs_fonts.js"></script>
            <!-- bootstrap-daterangepicker -->
    <script src="js/moment/moment.min.js"></script>
    <script src="js/datepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../../build/js/custom.min.js"></script>

    <!-- Angular -->
    <script src="js/lib/angular.min.js"></script>
    <!--App Controller -->
    <script src="js/app.js"></script>
    <script src="js/service.js"></script>
  
  </body>
</html>