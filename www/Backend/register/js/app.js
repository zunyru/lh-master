
var appApply = angular.module('apply', ['services']);

// ****** APPLY ******
appApply.controller('applyController', function ($scope, $timeout, Service) {

  Service.list('','work').then(function(d) {
    $scope.data_  = d
    $timeout(function(){ 
     /*$('#datatable').dataTable({"bLengthChange": false, "pageLength": 50,
     "order": [[ 4, "desc" ]]});*/
     $('#datatable').DataTable({
      "bLengthChange": false,
      "pageLength": 50,
      "order": [[ 4, "desc" ]],
      initComplete: function () {
        //console.log(this);
     
        this.api().columns().every(function () {
          var column = this;
                    if (column.index() == 3 || column.index() == 4 || column.index() == 5 ||  column.index() == 6) {

                      input = $('<p></p>').appendTo($(column.header())).on('keyup change', function () {
                      });
                      return;
                    }
                    

                    var select = $('<select class="form-control apcol'+column.index()+'"><option value="">เลือกทั้งหมด</option></select>')
                    .appendTo($("#filters").find("th").eq(column.index()))
                    .on('change', function () {
                      var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val());                                     

                      column.search(val ? '^' + val + '$' : '', true, false)
                      .draw();
                    });

                   

                   column.data().unique().sort().each(function (d, j) {
                    //  console.log(column.data().unique());
                    select.append('<option value="' + d + '">' + d + '</option>')
                  });
                 });
      }
    });
    $(".apcol0").css({
       'max-width': '190px',
     });
     $(".apcol1").css({
       'max-width': '190px',
     });
     $(".apcol2").css({
      'max-width': '120px',
    });
   },200);

    $('#datatable').on( 'page.dt', function () {
      $('html, body').animate({
        scrollTop: 0
      }, 300);
    } );
  });

        Service.selPosition().then(function(d) {
            $scope.position = d
        });
        Service.selProvince().then(function(d) {
            $scope.province = d
        });
        Service.selArea().then(function(d) {
            $scope.area = d
        });


        $scope.dateFormat = function (d){

            var now = new Date(d)
            var thmonth = new Array ("ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.", "ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");

            return now.getDate()+ " " + thmonth[now.getMonth()]+ " " + now.getFullYear()
       }


         $scope.dateFormat_desc = function (d){

            var now = new Date(d)
            var thmonth = new Array ("ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.", "ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");

            return now.getFullYear() + "-" + now.getMonth()+ "-" + +now.getDate() + "00:00:00.000" ;
        }


    });

    appApply.controller('applyAddController', function ($scope, $timeout, Service) {

        $scope.value = [];
        $scope.success = false;
        $scope.unsuccess = false;
        $scope.duplicate = false;

  Service.selPosition().then(function(d) {
    $scope.position = d
  });
  Service.selProvince().then(function(d) {
    $scope.province = d
  });
  Service.selArea().then(function(d) {
    $scope.area = d
  });

        $timeout(function(){ 
            $('#single_cal2').datepicker({
                autoclose: true,
                changeMonth: true,
                showDropdowns: true,
                changeYear: true,
                minDate:  new Date(),
                onSelect: function(dateText) {
                    var d = new Date($(this).datepicker("getDate"))
                    $scope.value.posted = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate();
                }
             });
            var d = new Date()
             $scope.value.posted = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate();
             $("#single_cal2").datepicker("setDate", d); 
        },250);

        $scope.submitForm = function(){

            var d = {
                'position_id':this.value.position,
                'location_id':this.value.area,
                'attribute':this.value.message,
                'rates':this.value.rates,
                'posted':this.value.posted,
                'is_show':$('#show').is(":checked") == false? "0":"1"
            };
           
            Service.save(d,'workAdd').then(function(d) {
                if(d.status == 0){
                    $scope.unsuccess = true;
                    $timeout(function(){ 
                       $scope.unsuccess = false;
                    },2000);
                }
                if(d.status == 1){
                    $scope.success = true;
                    $timeout(function(){ 
                       $scope.success = false;
                    },2000);
                }
                if(d.status == 2){
                    $scope.duplicate = true;
                    $timeout(function(){ 
                       $scope.duplicate = false;
                    },2000);
                }
            });
        }

    });


    appApply.controller('applyEditController', function ($scope,$timeout, Service) {
    	 
    	$scope.success = false;
        $scope.unsuccess = false;
        $scope.duplicate = false;

        Service.selPosition().then(function(d) {
            $scope.position = d
        });
        Service.selProvince().then(function(d) {
            $scope.province = d
        });
        Service.selArea().then(function(d) {
            $scope.area = d
        });

        $timeout(function(){ 
            $('#single_cal2').datepicker({
                autoclose: true,
                changeMonth: true,
                showDropdowns: true,
                changeYear: true,
                minDate:  new Date(),
                onSelect: function(dateText) {
                    var d = new Date($(this).datepicker("getDate"))
                    $scope.value.posted = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate();
                }
             });

        },250);


        Service.list(location.search.split('id=')[1],'work').then(function(d) {
            $scope.value  = d[0]
            $timeout(function(){ 
                $scope.value.position = $scope.value.POSITION_ID;
                $scope.value.message = $scope.value.ATTRIBUTE;
                $scope.value.rates = $scope.value.RATES;
                $scope.value.IS_SHOW == 0 ? '':$('#show').click();
                $("#province").val($scope.value.PROVINCE_ID)
                $scope.value.province = $("#province").val();
                $timeout(function(){ 
                 $scope.value.area = $scope.value.LOCATION_ID;
                },200);
                var d = new Date($scope.value.DATE)
                $scope.value.posted = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate();
                $("#single_cal2").val(d.getDate()+"/"+(d.getMonth()+1)+"/"+d.getFullYear());
            },300);
        });

        $scope.del = function(){
            var d = {
                'id': location.search.split('id=')[1],
                'key':'ID',
                'tb':'JOB_WORK'
            };
            Service.del(d).then(function(d) {
                document.location.href = 'apply1.php';
            });
        }
        
        $scope.submitForm = function(){
            var d = {
                'id': location.search.split('id=')[1],
                'position_id':this.value.position,
                'location_id':this.value.area,
                'attribute':this.value.message,
                'rates':this.value.rates,
                'posted':this.value.posted,
                'is_show':$('#show').is(":checked") == false? "0":"1"
            };

            Service.edit(d,'workEdit').then(function(d) {
              
                if(d.status == 0){
                    $scope.unsuccess = true;
                    $timeout(function(){ 
                       $scope.unsuccess = false;
                    },2000);
                }
                if(d.status == 1){
                    $scope.success = true;
                    $timeout(function(){ 
                       $scope.success = false;
                    },2000);
                }
                if(d.status == 2){
                    $scope.duplicate = true;
                    $timeout(function(){ 
                       $scope.duplicate = false;
                    },2000);
                }
            });
        }

            $('a.confirms').confirm({
                content: 'คุณแน่ใจที่จะลบข้อมูลนี้ !',
                buttons: {
                    Yes: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        keys: ['enter', 'a'],
                        action: function(){
                             $scope.del();
                        }
                    },  
                    No: {
                        text: 'No',
                        btnClass: 'btn-default',
                        keys: ['enter', 'a'],
                        action: function(){}
                    },             
                }
            });

    });

    // ****** CANDIDACY ******

    var appCandidacy = angular.module('candidacy', ['services']);

    appCandidacy.controller('candidacyController', function ($scope, $timeout, Service) {

        Service.selPosition().then(function(d) {
            $scope.position = d
        });
        Service.selProvince().then(function(d) {
            $scope.province = d
        });
        Service.selArea().then(function(d) {
            $scope.area = d
        });

      Service.list('','candidacy').then(function(d) {
        $scope.data_  = d
        $timeout(function(){ 
         /*$('#datatable').dataTable({"bLengthChange": false, "pageLength": 50,
          "order": [[ 4, "desc" ]]});*/
           $('#datatable').DataTable({
                "bLengthChange": false,
                "pageLength": 50,
                "order": [[ 4, "desc" ]],
                initComplete: function () {

            this.api().columns().every(function () {
              var column = this;

              if (column.index() == 0) {

                input = $('<input class="form-control" type="text" placeholder="คีย์เพื่อค้นหา" />').appendTo($(column.header())).on('keyup change', function () {
                  if (column.search() !== this.value) {
                    column.search(this.value)
                    .draw();
                  }
                });
                return;
              }
             
              if (column.index() == 4 || column.index() == 5 || column.index() == 6 ) {

                input = $('<p></p>').appendTo($(column.header())).on('keyup change', function () {
                });
                return;
              }
              

                    var select = $('<select class="form-control fcol'+column.index()+'"><option value="">เลือกทั้งหมด</option></select>')
                    .appendTo($("#filters").find("th").eq(column.index()))
                    .on('change', function () {
                      var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val());                                     

                      column.search(val ? '^' + val + '$' : '', true, false)
                      .draw();
                    });

                   //console.log(column.data().unique());

                    column.data().unique().sort().each(function (d, j) {
                      select.append('<option value="' + d + '">' + d + '</option>')
                    });
                  });
                }
              });

         $(".fcol1").css({
          'max-width': '170px',
        });
         $(".fcol2").css({
          'max-width': '103px',
        });
         $(".fcol3").css({
          'max-width': '150px',
        });
       },200);
        $('#datatable').on( 'page.dt', function () {
          $('html, body').animate({
            scrollTop: 0
          }, 300);
        } );
      });

        $scope.dateFormat = function (d){

            var now = new Date(d)
            var thmonth = new Array ("ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.", "ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");

            return now.getDate()+ " " + thmonth[now.getMonth()]+ " " + now.getFullYear()
         }

        $scope.MydateFormat = function (d){
          console.log("zun3");
          //var now = new Date(d);
          
          var thmonth = new Array ("ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.", "ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");   
          var day = d;
          var d = day.substring (0,2);
          var m = day.substring (3,5);
          var y = day.substring (6,10);
          var mount = m-1; 
          //console.log(d+ " " + thmonth[mount]+ " " + y);
          return d+ " " + thmonth[mount]+ " " + y;
        };
    });

    appCandidacy.controller('candidacyEditController', function ($scope, $timeout, Service) {

      Service.list(location.search.split('id=')[1],'candidacy_profile').then(function(d) {
        $scope.profile  = d[0]
      });
      Service.list(location.search.split('id=')[1],'candidacy_family').then(function(d) {
        $scope.family  = d
      });
      Service.list(location.search.split('id=')[1],'candidacy_contact').then(function(d) {
        $scope.contact  = d[0]
      });
      Service.list(location.search.split('id=')[1],'candidacy_education').then(function(d) {
        console.log(d)
        $scope.education  = d
      });
      Service.list(location.search.split('id=')[1],'candidacy_language').then(function(d) {
        $scope.language  = d
      });
      Service.list(location.search.split('id=')[1],'candidacy_job').then(function(d) {
        $scope.job  = d
      });
      Service.list(location.search.split('id=')[1],'candidacy_job_interested').then(function(d) {
        $scope.job_interested  = d[0]
      });
      Service.list(location.search.split('id=')[1],'candidacy_person').then(function(d) {
        $scope.person  = d[0]
      });
      Service.list(location.search.split('id=')[1],'candidacy_datas').then(function(d) {
        $scope.datas  = d[0]
      });

        $scope.del = function(){
            var d = {'id': location.search.split('id=')[1],'key':'ID','tb':'JOB_APPLICANT'};
            var d1 = {'id': location.search.split('id=')[1],'key':'APPLICANT_ID','tb':'JOB_APPLICANT_CONTACT'};
            var d2 = {'id': location.search.split('id=')[1],'key':'APPLICANT_ID','tb':'JOB_APPLICANT_DATAS'};
            var d3 = {'id': location.search.split('id=')[1],'key':'APPLICANT_ID','tb':'JOB_APPLICANT_EDUCATION'};
            var d4 = {'id': location.search.split('id=')[1],'key':'APPLICANT_ID','tb':'JOB_APPLICANT_FAMILY'};
            var d5 = {'id': location.search.split('id=')[1],'key':'APPLICANT_ID','tb':'JOB_APPLICANT_JOB_INTERESTED'};
            var d6 = {'id': location.search.split('id=')[1],'key':'APPLICANT_ID','tb':'JOB_APPLICANT_LANGUAGE'};
            var d7 = {'id': location.search.split('id=')[1],'key':'APPLICANT_ID','tb':'JOB_APPLICANT_PERSON'};
            var d8 = {'id': location.search.split('id=')[1],'key':'APPLICANT_ID','tb':'JOB_APPLOCANT_JOB'};
            var d9 = {'id': location.search.split('id=')[1],'key':'APPLICANT_ID','tb':'JOB_APPLOCANT_JOB_HISTORY'};

            Service.del(d).then(function(d) {
                document.location.href = 'candidacy1.php';
            });
            Service.del(d1).then(function(d) {});
            Service.del(d2).then(function(d) {});
            Service.del(d3).then(function(d) {});
            Service.del(d4).then(function(d) {});
            Service.del(d5).then(function(d) {});
            Service.del(d6).then(function(d) {});
            Service.del(d7).then(function(d) {});
            Service.del(d8).then(function(d) {});
            Service.del(d9).then(function(d) {});

        }

        $('a.confirms').confirm({
                content: 'คุณแน่ใจที่จะลบข้อมูลนี้ !',
                buttons: {
                    Yes: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        keys: ['enter', 'a'],
                        action: function(){
                             $scope.del();
                        }
                    },  
                    No: {
                        text: 'No',
                        btnClass: 'btn-default',
                        keys: ['enter', 'a'],
                        action: function(){}
                    },             
                }
            });

        $scope.dateFormat = function (d){
            if(d){
                var t = d.split(' ')
                var now = new Date(t[0]+' '+t[1]+' '+t[2])
                var thmonth = new Array ("ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.", "ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
                return now.getDate()+ " " + thmonth[now.getMonth()]+ " " + now.getFullYear()
            }else{
                return '';
            }
       }

         $scope.MydateFormat = function (d = ''){
          //console.log("zun1");
          //var now = new Date(d);
          if(d != ''){
              var thmonth = new Array ("ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.", "ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");   
              var day = d;
              var d = day.substring (0,2);
              var m = day.substring (3,5);
              var y = day.substring (6,10);
              var mount = m-1; 
              //console.log(d+ " " + thmonth[mount]+ " " + y);
              return d+ " " + thmonth[mount]+ " " + y;
           }
        };

        $scope.dateFormatM = function (d){
            //console.log(d);
            if(d){
                var day = d;
                var t = d.split(' ')
                var now = new Date(t[0]+' '+t[1]+' '+t[2])
                var thmonth = new Array ("ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.", "ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
                var m = day.substring (3,5);
                var y = day.substring (6,10);
                var mount = m-1;
                //return thmonth[now.getMonth()]+ " " + now.getFullYear()
                return  thmonth[mount]+ " " + y;
            }else{
                return '';
            }
        }


    $scope.edu_ifvalue = function (d){
      var values = '';
          if(d == 1){
            values = 'ปวช.';
          }else if(d == 2){
            values = 'ปวส.';
          }else if(d == 3){
            values = 'ป.ตรี';
          }else if(d == 4){
             values = 'ป.โท'
          }
          else if(d == 5){
             values = 'ป.เอก'
          }
          return  values;
    }


    });


    // ****** REPORT  ******

    var appReport = angular.module('report', ['services']);

    appReport.controller('reportController', function ($scope, $timeout, Service) {

      Service.list('','report').then(function(d) {
        $scope.data_  = d
        $timeout(function(){ 
              
              $('#datatable').DataTable({
                "bLengthChange": false,
                "pageLength": 50,
                initComplete: function () {

                  this.api().columns().every(function () {
                    var column = this;

                    if (column.index() == 0) {

                  input = $('<input class="form-control" type="text" placeholder="คีย์เพื่อค้นหา" />').appendTo($(column.header())).on('keyup change', function () {
                    if (column.search() !== this.value) {
                      column.search(this.value)
                      .draw();
                    }
                  });
                  return;
                }
                if (column.index() == 3) {

                  input = $('<p></p>').appendTo($(column.header())).on('keyup change', function () {
                  });
                  return;
                }

                    var select = $('<select class="form-control"><option value="">เลือกทั้งหมด</option></select>')
                    .appendTo($("#filters").find("th").eq(column.index()))
                    .on('change', function () {
                      var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val());                                     

                      column.search(val ? '^' + val + '$' : '', true, false)
                      .draw();
                    });

                   //console.log(column.data().unique());

                    column.data().unique().sort().each(function (d, j) {
                      select.append('<option value="' + d + '">' + d + '</option>')
                    });
                  });
                }
              });  

              //console.log()


            },200);
                         $('#datatable').on( 'page.dt', function () {
                          $('html, body').animate({
                            scrollTop: 0
                          }, 300);
                        } );
                       });

        Service.selStatus().then(function(d) {
            $scope.status = d
        });

        Service.selPosition().then(function(d) {
            $scope.position = d
        });

    });

    appReport.controller('reportEditController', function ($scope, $timeout, Service, $http) {
        
        $scope.success = false;
        $scope.unsuccess = false;
        $scope.duplicate = false;

        Service.list(location.search.split('id=')[1],'report').then(function(d) {
            $scope.value  = d[0]
            $timeout(function(){ 
               $scope.value.status = $scope.value.COMMENT_ID;
               if($scope.value.COMMENT_ID == 11){
                    $scope.value.message = $scope.value.OTHER;
                }
            },200);
        });

        Service.selStatus().then(function(d) {
            $scope.status = d
        });

        $scope.submitForm = function(){
            var d = {
                'id': location.search.split('id=')[1],
                'comment_id':this.value.status,
                'other': this.value.message
            };

            Service.edit(d,'reportEdit').then(function(d) {
              
                if(d.status == 0){
                    $scope.unsuccess = true;
                    $timeout(function(){ 
                       $scope.unsuccess = false;
                    },2000);
                }
                if(d.status == 1){
                    $scope.success = true;
                    $timeout(function(){ 
                       $scope.success = false;
                    },2000);
                }
                if(d.status == 2){
                    $scope.duplicate = true;
                    $timeout(function(){ 
                       $scope.duplicate = false;
                    },2000);
                }
            });
        }

          
        
    });


    // ******  POSITION ******

    var appPostion = angular.module('postion', ['services']);

    appPostion.controller('positionController', function ($scope, $timeout, Service) {

      $scope.data_ = Service.list();

      Service.list('','position').then(function(d) {
        $scope.data_  = d
        $timeout(function(){ 
         $('#datatable').dataTable({"bLengthChange": false, "pageLength": 50,
           "order": [[ 0, "DESC" ]]});
       },200);
        $('#datatable').on( 'page.dt', function () {
          $('html, body').animate({
            scrollTop: 0
          }, 300);
        } );
      });

    });

    appPostion.controller('positionAddController', function ($scope, $timeout, Service, $http) {
        
        $scope.success = false;
        $scope.unsuccess = false;
        $scope.duplicate = false;

        $scope.submitForm = function(){
            var d = {
                'position':this.value.position
            };
            Service.save(d,'positionAdd').then(function(d) {
                $scope.value.province = "";
                $scope.value.area = " ";
                if(d.status == 0){
                    $scope.unsuccess = true;
                    $timeout(function(){ 
                       $scope.unsuccess = false;
                    },2000);
                }
                if(d.status == 1){
                    $scope.success = true;
                    $timeout(function(){ 
                       $scope.success = false;
                    },2000);
                }
                if(d.status == 2){
                    $scope.duplicate = true;
                    $timeout(function(){ 
                       $scope.duplicate = false;
                    },2000);
                }
            });
        }
        
    });


    appPostion.controller('positionEditController', function ($scope, $timeout, Service, $http) {
        
        $scope.success = false;
        $scope.unsuccess = false;
        $scope.duplicate = false;

        Service.list(location.search.split('id=')[1],'position').then(function(d) {
            $scope.value  = d[0]
            $timeout(function(){ 
                $scope.value.position = $scope.value.TITLE;
            },500);
        });

        $scope.del = function(){
            var d = {
                'id': location.search.split('id=')[1],
                'key':'ID',
                'tb':'JOB_TITLE'
            };
            Service.del(d).then(function(d) {
                document.location.href = 'position1.php';
            });
        }
        
        $scope.submitForm = function(){
            var d = {
                'id': location.search.split('id=')[1],
                'position':this.value.position
            };

            Service.edit(d,'positionEdit').then(function(d) {
              
                if(d.status == 0){
                    $scope.unsuccess = true;
                    $timeout(function(){ 
                       $scope.unsuccess = false;
                    },2000);
                }
                if(d.status == 1){
                    $scope.success = true;
                    $timeout(function(){ 
                       $scope.success = false;
                    },2000);
                }
                if(d.status == 2){
                    $scope.duplicate = true;
                    $timeout(function(){ 
                       $scope.duplicate = false;
                    },2000);
                }
            });
        }

            $('a.confirms').confirm({
                content: 'คุณแน่ใจที่จะลบข้อมูลนี้ !',
                buttons: {
                    Yes: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        keys: ['enter', 'a'],
                        action: function(){
                             $scope.del();
                        }
                    },  
                    No: {
                        text: 'No',
                        btnClass: 'btn-default',
                        keys: ['enter', 'a'],
                        action: function(){}
                    },             
                }
            });
        
    });


    // ****** NEW  ******

    var appNew = angular.module('new', ['services']);

    appNew.controller('newsController', function ($scope, $timeout, Service) {

      Service.list('','news').then(function(d) {
        $scope.data_  = d
        $timeout(function(){ 
         $('#datatable').dataTable({"bLengthChange": false, "pageLength": 50,
           "order": [[ 1, "DESC" ]]});
       },200);
        $('#datatable').on( 'page.dt', function () {
          $('html, body').animate({
            scrollTop: 0
          }, 300);
        } );
      });

        $scope.dateFormat = function (d){

            var now = new Date(d)
            var thmonth = new Array ("ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.", "ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");

            return now.getDate()+ " " + thmonth[now.getMonth()]+ " " + now.getFullYear()
        }

         $scope.MydateFormat = function (d){
          console.log("zun2");
          //var now = new Date(d);
          
          var thmonth = new Array ("ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.", "ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");   
          var day = d;
          var d = day.substring (0,2);
          var m = day.substring (3,5);
          var y = day.substring (6,10);
          var mount = m-1; 
          //console.log(d+ " " + thmonth[mount]+ " " + y);
          return d+ " " + thmonth[mount]+ " " + y;
        };
    });

    appNew.controller('newsAddController', function ($scope, $timeout, Service, $http) {
        
        $scope.value = [];
        $scope.meg = 'ใส่รูปประกอบ';
        $('#success').hide();
        $('#unsuccess').hide();
        $('#duplicate').hide();
        $scope.error = false;

        Service.selProvince().then(function(d) {
            $scope.province = d
        });

        $timeout(function(){ 
            $('#edit').froalaEditor({
                height:500,
                imageAllowedTypes: ['jpeg', 'jpg', 'png']
            }).on('froalaEditor.image.beforeUpload', function (e, editor, files) {
                if (files.length) {
                  // Create a File Reader.
                  var reader = new FileReader();
             
                  // Set the reader to insert images when they are loaded.
                  reader.onload = function (e) {
                    var result = e.target.result;
                    editor.image.insert(result, null, null, editor.image.get());
                  };
                  
                  // Read image as base64.
                  reader.readAsDataURL(files[0]);
                }

              // Stop default upload chain.
              return false;
            })

            $('#single_cal2').datepicker({
                autoclose: true,
                changeMonth: true,
                showDropdowns: true,
                changeYear: true,
                minDate:  new Date(),
                onSelect: function(dateText) {
                    var d = new Date($(this).datepicker("getDate"))
                    $scope.value.posted = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate();
                }
             });
            var d = new Date()
             $scope.value.posted = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate();
             $("#single_cal2").datepicker("setDate", d); 
          
        },250);

         $("#sortpicture").change( function(submitEvent) {
                $scope.error = false
                $('#error').hide()
                var fileSize = this.files[0];
                var sizeInMb = (fileSize.size/1024)/1024;
                var sizeLimit= 1;
                var filename = $("#sortpicture").val();
                var extension = filename.replace(/^.*\./, '');
                var type = [ "gif", "png", "jpg", "jpeg" ];
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#blah').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
                $scope.value.file = b64EncodeUnicode(new Date())+"."+extension;
                if (extension == filename) {
                    extension = '';
                } else {
                    extension = extension.toLowerCase();
                }
                if($.inArray(extension, type)< 0){
                    $('#submit').prop('disabled', true);
                    $('#error').show()
                    $scope.error = true
                    $scope.meg = 'Inavlid file type'
                }else if(sizeInMb > sizeLimit){
                    $('#submit').prop('disabled', true);
                    $('#error').show()
                    $scope.error = true
                    $scope.meg = 'File size must be less than 2MB'
                }
                else{
                    $('#error').hide()
                    $scope.error = false
                    $('#submit').prop('disabled', false);
                }
             
        });

        function b64EncodeUnicode(str) {
            return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function(match, p1) {
                return String.fromCharCode('0x' + p1);
            }));
        }

        $scope.submitForm = function(){
            if ($("#sortpicture").val() == '') {$('#error').show();$scope.error = true; $scope.meg = 'ใส่รูปประกอบ';}
            if($scope.error != true){
                var file_data = $('#sortpicture').prop('files')[0];   
                var form_data = new FormData();                  
                form_data.append('file', file_data);
                form_data.append('imagename', this.value.file);
                form_data.append('title', this.value.title);
                form_data.append('short', this.value.message);
                form_data.append('detail',  $('#edit .fr-view').html());
                form_data.append('publish', this.value.posted);
                form_data.append('show', $('#show').is(":checked") == false? "0":"1");   
                console.log(this.value.posted)
                $.ajax({
                    url: 'http://uat.lh.co.th/www_th/Backend/service/register_be.php?n=newsAdd', // point to server-side PHP script
                    dataType: 'json',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,                         
                    type: 'post',
                    success: function(d){
                        $scope.value.title = " ";
                        $scope.value.message = " ";
                        $('#edit .fr-view').html('')
                        $('#blah').attr('src', '');
                        if(d.status == 0){
                            $('#unsuccess').show();
                            $timeout(function(){ 
                                $('#unsuccess').hide();
                            },2000);
                        }
                        if(d.status == 1){
                            $('#success').show();
                            $timeout(function(){ 
                               $('#success').hide();
                            },2000);
                        }
                        if(d.status == 2){
                            $('#duplicate').show();
                            $timeout(function(){ 
                               $('#duplicate').hide();
                            },2000);
                        }
                    }
                });
          
            }
        }
        
    });

     appNew.controller('newsEditController', function ($scope, $timeout, Service, $http) {
        
        $scope.value = [];
        $scope.meg = 'ใส่รูปประกอบ';
        $('#success').hide();
        $('#unsuccess').hide();
        $('#duplicate').hide();
        $scope.error = false;

        Service.list(location.search.split('id=')[1],'news').then(function(d) {
            $scope.value  = d[0]
            $timeout(function(){ 
                $scope.value.title = $scope.value.TITLE;
                $scope.value.message = $scope.value.SHORT_TITLE;
                $scope.value.img = 'http://uat.lh.co.th/www_th/Backend/fileupload/news/'+$scope.value.NEWS_PIC;
                $scope.value.img_old = $scope.value.NEWS_PIC;
                $('#edit .fr-view').html($scope.value.DETAIL)
                $scope.value.IS_SHOW == 0 ? '':$('#show').click();
                var d = new Date($scope.value.DATE)
                $scope.value.posted = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate();
                $("#single_cal2").val(d.getDate()+"/"+(d.getMonth()+1)+"/"+d.getFullYear());

            },500);
        });

        $timeout(function(){ 
            $('#edit').froalaEditor({
                height:500,
                imageAllowedTypes: ['jpeg', 'jpg', 'png']
            }).on('froalaEditor.image.beforeUpload', function (e, editor, files) {
                if (files.length) {
                  // Create a File Reader.
                  var reader = new FileReader();
             
                  // Set the reader to insert images when they are loaded.
                  reader.onload = function (e) {
                    var result = e.target.result;
                    editor.image.insert(result, null, null, editor.image.get());
                  };
                  
                  // Read image as base64.
                  reader.readAsDataURL(files[0]);
                }

              // Stop default upload chain.
              return false;
            })

            $('#single_cal2').datepicker({
                autoclose: true,
                changeMonth: true,
                showDropdowns: true,
                changeYear: true,
                minDate:  new Date(),
                onSelect: function(dateText) {
                    var d = new Date($(this).datepicker("getDate"))
                    $scope.value.posted = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate();
                }
             });
          
        },250);


         $("#sortpicture").change( function(submitEvent) {
                $scope.error = false
                $('#error').hide()
                var fileSize = this.files[0];
                var sizeInMb = (fileSize.size/1024)/1024;
                var sizeLimit= 1;
                var filename = $("#sortpicture").val();
                var extension = filename.replace(/^.*\./, '');
                var type = [ "gif", "png", "jpg", "jpeg" ];
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#blah').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
                $scope.value.file = b64EncodeUnicode(new Date())+"."+extension;
                if (extension == filename) {
                    extension = '';
                } else {
                    extension = extension.toLowerCase();
                }
                if($.inArray(extension, type)< 0){
                    $('#submit').prop('disabled', true);
                    $('#error').show()
                    $scope.error = true
                    $scope.meg = 'Inavlid file type'
                }else if(sizeInMb > sizeLimit){
                    $('#submit').prop('disabled', true);
                    $('#error').show()
                    $scope.error = true
                    $scope.meg = 'File size must be less than 2MB'
                }
                else{
                    $('#error').hide()
                    $scope.error = false
                    $('#submit').prop('disabled', false);
                }
             
        });

        function b64EncodeUnicode(str) {
            return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function(match, p1) {
                return String.fromCharCode('0x' + p1);
            }));
        }

        $scope.del = function(){
            var d = {
                'id': location.search.split('id=')[1],
                'key':'ID',
                'tb':'JOB_NEWS'
            };
            Service.del(d).then(function(d) {
                document.location.href = 'news1.php';
            });
        }

        $scope.submitForm = function(){
            //if ($("#sortpicture").val() == '') {$('#error').show();$scope.error = true; $scope.meg = 'ใส่รูปประกอบ';}
            if($scope.error != true){
                var file_data = $('#sortpicture').prop('files')[0];   
                var form_data = new FormData();                  
                form_data.append('file', file_data);
                form_data.append('id', location.search.split('id=')[1]);
                form_data.append('imagename', this.value.file);
                form_data.append('imagename_old', this.value.img_old);
                form_data.append('title', this.value.title);
                form_data.append('short', this.value.message);
                form_data.append('detail',  $('#edit .fr-view').html());
                form_data.append('publish', this.value.posted);
                form_data.append('show', $('#show').is(":checked") == false? "0":"1");   

                $.ajax({
                    url: 'http://uat.lh.co.th/www_th/Backend/service/register_be.php?n=newsEdit', // point to server-side PHP script
                    dataType: 'json',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,                         
                    type: 'post',
                    success: function(d){
                        if(d.status == 0){
                            $('#unsuccess').show();
                            $timeout(function(){ 
                                $('#unsuccess').hide();
                            },2000);
                        }
                        if(d.status == 1){
                            $('#success').show();
                            $timeout(function(){ 
                               $('#success').hide();
                            },2000);
                        }
                        if(d.status == 2){
                            $('#duplicate').show();
                            $timeout(function(){ 
                               $('#duplicate').hide();
                            },2000);
                        }
                    }
                });
          
            }
        }
        
            $('a.confirms').confirm({
                content: 'คุณแน่ใจที่จะลบข้อมูลนี้ !',
                buttons: {
                    Yes: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        keys: ['enter', 'a'],
                        action: function(){
                             $scope.del();
                        }
                    },  
                    No: {
                        text: 'No',
                        btnClass: 'btn-default',
                        keys: ['enter', 'a'],
                        action: function(){}
                    },             
                }
            });
        
    });



    // ******  LOCATION ******

    var appArea = angular.module('area', ['services']);

    appArea.controller('areaController', function ($scope, $timeout, Service) {

      Service.list('','location').then(function(d) {
        $scope.data_  = d
        $timeout(function(){ 
         $('#datatable').dataTable({"bLengthChange": false, "pageLength": 50,
           "order": [[ 0, "DESC" ]]});
       },200);
        $('#datatable').on( 'page.dt', function () {
          $('html, body').animate({
            scrollTop: 0
          }, 300);
        } );
      });
    });

    appArea.controller('areaAddController', function ($scope, $timeout, Service, $http) {
        
        $scope.success = false;
        $scope.unsuccess = false;
        $scope.duplicate = false;

        Service.selProvince().then(function(d) {
            $scope.province = d
        });

        $scope.submitForm = function(){
            var d = {
                'province':this.value.province,
                'area':this.value.area,
                'show':$('#show').is(":checked") == false? "0":"1"
            };

            Service.save(d,'locationAdd').then(function(d) {
                $scope.value.province = "";
                $scope.value.area = " ";
                if(d.status == 0){
                    $scope.unsuccess = true;
                    $timeout(function(){ 
                       $scope.unsuccess = false;
                    },2000);
                }
                if(d.status == 1){
                    $scope.success = true;
                    $timeout(function(){ 
                       $scope.success = false;
                    },2000);
                }
                if(d.status == 2){
                    $scope.duplicate = true;
                    $timeout(function(){ 
                       $scope.duplicate = false;
                    },2000);
                }
            });
        }
        
    });


    appArea.controller('areaEditController', function ($scope, $timeout, Service, $http) {
        
        $scope.success = false;
        $scope.unsuccess = false;
        $scope.duplicate = false;

        Service.selProvince().then(function(d) {
            $scope.province = d
        });

        Service.list(location.search.split('id=')[1],'location').then(function(d) {
            $scope.value  = d[0]
            $timeout(function(){ 
                $scope.value.IS_SHOW == 0 ? '':$('#show').click();
                $scope.value.area = $scope.value.LOCATION_NAME;
               $("#province").val($scope.value.PROVINCE_ID)
            },500);
        });

        $scope.del = function(){
            var d = {
                'id': location.search.split('id=')[1],
                'key':'LOCATION_ID',
                'tb':'JOB_LOCATION'
            };
            Service.del(d).then(function(d) {
                document.location.href = 'area1.php';
            });
        }
        
        $scope.submitForm = function(){
            var d = {
                'id': location.search.split('id=')[1],
                'province':this.value.province,
                'area':this.value.area,
                'show':$('#show').is(":checked") == false? "0":"1"
            };

            Service.edit(d,'locationEdit').then(function(d) {
              
                if(d.status == 0){
                    $scope.unsuccess = true;
                    $timeout(function(){ 
                       $scope.unsuccess = false;
                    },2000);
                }
                if(d.status == 1){
                    $scope.success = true;
                    $timeout(function(){ 
                       $scope.success = false;
                    },2000);
                }
                if(d.status == 2){
                    $scope.duplicate = true;
                    $timeout(function(){ 
                       $scope.duplicate = false;
                    },2000);
                }
            });
        }

            $('a.confirms').confirm({
                content: 'คุณแน่ใจที่จะลบข้อมูลนี้ !',
                buttons: {
                    Yes: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        keys: ['enter', 'a'],
                        action: function(){
                             $scope.del();
                        }
                    },  
                    No: {
                        text: 'No',
                        btnClass: 'btn-default',
                        keys: ['enter', 'a'],
                        action: function(){}
                    },             
                }
            });
        
    });


    // ****** USER ******

    var appUser = angular.module('user', ['services']);

    appUser.controller('userController', function ($scope, $timeout, Service) {

      Service.list('','user').then(function(d) {
        $scope.data_  = d
        $timeout(function(){ 
         $('#datatable').dataTable({"bLengthChange": false, "pageLength": 50,
          "order": [[ 0, "DESC" ]]});
       },200);
        $('#datatable').on( 'page.dt', function () {
          $('html, body').animate({
            scrollTop: 0
          }, 300);
        } );
      });

    });

    appUser.controller('userAddController', function ($scope, $timeout, Service) {
        
        $scope.success = false;
        $scope.unsuccess = false;
        $scope.duplicate = false;

        $scope.submitForm = function(){
            var d = {
                'user':this.value.user,
                'login':this.value.loginname,
                'pass':this.value.pass
            };

            Service.save(d,'userAdd').then(function(d) {
                $scope.value.province = "";
                $scope.value.area = " ";
                if(d.status == 0){
                    $scope.unsuccess = true;
                    $timeout(function(){ 
                       $scope.unsuccess = false;
                    },2000);
                }
                if(d.status == 1){
                    $scope.success = true;
                    $timeout(function(){ 
                       $scope.success = false;
                    },2000);
                }
                if(d.status == 2){
                    $scope.duplicate = true;
                    $timeout(function(){ 
                       $scope.duplicate = false;
                    },2000);
                }
            });
        }
        
    });

    appUser.controller('userEditController', function ($scope, $timeout, Service, $http) {
        
        $scope.success = false;
        $scope.unsuccess = false;
        $scope.duplicate = false;

        Service.list(location.search.split('id=')[1],'user').then(function(d) {
            $scope.value  = d[0]
            $timeout(function(){ 
                $scope.value.user = $scope.value.USER_NAME;
                $scope.value.loginname = $scope.value.LOGIN;
                $scope.value.pass = "******"
            },500);
        });

        $scope.del = function(){
            var d = {
                'id': location.search.split('id=')[1],
                'key':'ID',
                'tb':'JOB_USER'
            };
            Service.del(d).then(function(d) {
                document.location.href = 'user1.php';
            });
        }
        
        $scope.submitForm = function(){
            var d = {
                'id': location.search.split('id=')[1],
                'user':this.value.user,
                'login':this.value.loginname,
                'pass':this.value.pass
            };

            Service.edit(d,'userEdit').then(function(d) {
              
                if(d.status == 0){
                    $scope.unsuccess = true;
                    $timeout(function(){ 
                       $scope.unsuccess = false;
                    },2000);
                }
                if(d.status == 1){
                    $scope.success = true;
                    $timeout(function(){ 
                       $scope.success = false;
                    },2000);
                }
                if(d.status == 2){
                    $scope.duplicate = true;
                    $timeout(function(){ 
                       $scope.duplicate = false;
                    },2000);
                }
            });
        }

            $('a.confirms').confirm({
                content: 'คุณแน่ใจที่จะลบข้อมูลนี้ !',
                buttons: {
                    Yes: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        keys: ['enter', 'a'],
                        action: function(){
                             $scope.del();
                        }
                    },  
                    No: {
                        text: 'No',
                        btnClass: 'btn-default',
                        keys: ['enter', 'a'],
                        action: function(){}
                    },             
                }
            });
        

    });




