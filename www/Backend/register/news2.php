<?php
session_start();
$_SESSION['group_id'];
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>
<!-- Bootstrap -->
    <link href="../../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
     <!-- Switchery -->
    <link href="../../vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../../build/css/custom.min.css" rel="stylesheet">

  <!-- Include Editor style. -->
  <link href="editor/css/froala_editor.min.css" rel="stylesheet" type="text/css" />
  <link href="editor/css/froala_style.min.css" rel="stylesheet" type="text/css" />

  <!-- Include Editor style. -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.3.4/css/froala_style.min.css" rel="stylesheet" type="text/css" />



  <!-- Include Code Mirror style -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">

  <!-- Include Editor Plugins style. -->
  <link rel="stylesheet" href="editor/css/plugins/char_counter.css">
  <link rel="stylesheet" href="editor/css/plugins/code_view.css">
  <link rel="stylesheet" href="editor/css/plugins/colors.css">
  <link rel="stylesheet" href="editor/css/plugins/emoticons.css">
  <link rel="stylesheet" href="editor/css/plugins/file.css">
  <link rel="stylesheet" href="editor/css/plugins/fullscreen.css">
  <link rel="stylesheet" href="editor/css/plugins/image.css">
  <link rel="stylesheet" href="editor/css/plugins/image_manager.css">
  <link rel="stylesheet" href="editor/css/plugins/line_breaker.css">
  <link rel="stylesheet" href="editor/css/plugins/quick_insert.css">
  <link rel="stylesheet" href="editor/css/plugins/table.css">
  <link rel="stylesheet" href="editor/css/plugins/video.css">

  <!-- jquery-ui Style -->
    <link href="../../build/css/jquery-ui.css" rel="stylesheet">

  
  <style type="text/css">
    .fr-toolbar.fr-desktop.fr-top.fr-basic.fr-sticky.fr-sticky-off {
        background: #f7f7f7  !important;
        border-top: 5px solid #f5f5f5  !important;
    }
    th.next.available {
        background: #6f9755;
    }
    th.next.available:hover {
        background: #9ab688;
    }
    th.prev.available {
        background: #6f9755;
    }
    th.prev.available:hover {
        background: #9ab688;
    }
</style>
  </head>

  <body class="nav-md" ng-app="new"  ng-controller="newsAddController" >
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

            <?php  include  '../master/navbar_regis.php' ?>

           
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
          <?php include '../master/top_nav_regis.php'; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                  
                   <div class="bs-example-popovers">
                        <a href="apply1.php" class="btn btn-default">
                           ตำแหน่งงานว่าง 
                        </a>
                       <a href="candidacy1.php" class="btn btn-default">
                           ผู้สมัครงาน 
                        </a>
                       <a href="report.php" class="btn btn-default">
                           รายงาน 
                        </a>
                       <a href="position1.php" class="btn btn-default">
                           ชื่อตำแหน่งงาน 
                        </a>
                        <a href="news1.php" class="btn btn-success active">
                           ข่าวสาร 
                        </a>
                        <a href="area1.php" class="btn btn-default">
                           กำหนดพื้นที่ทำงาน 
                        </a>
                        <a href="user1.php" class="btn btn-default">
                           ผู้ใช้งาน 
                        </a>
                    </div>
                   <br>
                   
                <div class="x_title">
                  <h2><a href="news1.php">ข้อมูลข่าวสาร</a> > <a href="#">สร้างข้อมูลข่าวสาร</a></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">


                <div class="row"  id="success">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>อัปเดทข้อมูลเรียบร้อย</strong>  
                      </div>
                    </div>
                    <div class="col-md-4"></div> 
                   </div> 
                  
                    <div class="row"   id="duplicate">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>มีชื่อนี้อยู่ในระบบแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div> 
                   </div> 
                  
                    <div class="row"   id="unsuccess">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>ผิดพลาด !</strong> กรุณากรอกข้มูลให้ครบถ้วน
                      </div>
                    </div>
                    <div class="col-md-4"></div> 
                   </div> 


                  <p class="font-gray-dark"> 
                  </p><br>


                  <form class="form-horizontal form-label-left" name="Form" ng-submit="submitForm()" novalidate >

                    <div class="form-group" ng-class="{ 'has-error': Form.title.$invalid && Form.title.$dirty }">
                       <label class="control-label col-md-2" for="first-name">ชื่อข่าว <span class="required">*</span>
                      </label>
                      <div class="col-md-5">
                        <input type="text" id="first-name2" class="form-control col-md-7 col-xs-12" value="" placeholder="กรอกชื่อข่าว" ng-model="value.title" name="title" required/> 

                        <span style="color:red" ng-show="Form.title.$dirty && Form.title.$invalid">
                            <span ng-show="Form.title.$error.required">กรุณากรอกชื่อข่าว</span>
                        </span>

                      </div>
                    </div>
                     
                    <div class="form-group" >
                     
                        <label class="control-label col-md-2" for="first-name">รายละเอียดย่อ</label>
                        <div class="col-md-5">
                       <div class="control-group">
                                  <textarea id="message" class="form-control" rows="5" name="message" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" ng-model="value.message" name="message"  ></textarea>
                        </span>

                          </div>
                      </div>
                      </div>
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-3 col-xs-12">รายละเอียดข่าว </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <br>
                             <div id="edit" class="text-left txt-black fr-box fr-basic fr-top" style="z-index: 1000;"  name="detail"></div>    
                        </div>
                    </div>  
                    
                      <div class="form-group"   >
                      <label class="control-label col-md-2" for="first-name">รูปประกอบ <span class="required">*</span>
                      </label>
                      <div class="col-md-3">

                         <input id="sortpicture" class="btn btn-round btn-default"  name="sortpicture"  type="file" accept="image/*"  />

                          <span style="color:red;display: none;" id="error">
                              <span >กรุณา {{meg}}</span>
                          </span>

                           <img width="100%" height="100%" id="blah">

                      </div>

                    </div>
                      
                     <div class="form-group" ng-class="{ 'has-error': Form.posted.$invalid && Form.posted.$dirty }" >
                      <label class="control-label col-md-2" for="first-name">วันที่ข่าว <span class="required">*</span>
                      </label>
                      <div class="col-md-4">
                        <fieldset>
                        <div class="input-prepend input-group">
                           <input type="text" style="width: 220px;background-color: #fff;"  id="single_cal2" placeholder="เลือกช่วงเวลา" class="form-control" value="" name="posted"  required  readonly  />

                            <span style="color:red" ng-show="Form.posted.$dirty && Form.posted.$invalid">
                              <span ng-show="Form.posted.$error.required">กรุณากรอกวันที่ข่าว </span>
                          </span>

                         </div>
                        </fieldset>
                    </div>
                  </div>
                   <div class="form-group">
                      <label class="control-label col-md-2" for="first-name">แสดงบน web
                        </label>
                          <div class="col-md-3">
                              <input type="checkbox" class="js-switch" ng-model="value.show" id="show" /> แสดง
                          </div>
                    </div>
                      <br>

                    
                    <center>
                    <button type="submit" class="btn btn-success"  ng-disabled="Form.$invalid" id="submit" >Submit</button>
                  </center>
                  </form>


                </div>
              </div>
            </div>
          </div>
        <!-- /page content -->

        <!-- footer content -->
        <!-- <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer> -->
        <!-- /footer content -->
      </div>
    </div>

     <!-- jQuery -->
    <script src="../../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../../vendors/iCheck/icheck.min.js"></script>
    <!-- jQuery Tags Input -->
     <!-- Switchery -->
    <script src="../../vendors/switchery/dist/switchery.min.js"></script>
    <script src="../../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Datatables -->
    <script src="../../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../../vendors/pdfmake/build/vfs_fonts.js"></script>
            <!-- bootstrap-daterangepicker -->
    <script src="js/moment/moment.min.js"></script>
    <script src="../../build/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../../build/js/custom.min.js"></script>
    
    
   <!--Editor_script-->

   <!-- Include JS files. -->
  <script type="text/javascript" src="editor/js/froala_editor.min.js"></script>
    <!-- Include Code Mirror. -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
     <!-- Include Plugins. -->
  <script type="text/javascript" src="editor/js/plugins/align.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/char_counter.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/code_beautifier.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/code_view.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/colors.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/emoticons.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/entities.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/file.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/font_family.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/font_size.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/fullscreen.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/image.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/image_manager.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/inline_style.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/line_breaker.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/link.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/lists.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/paragraph_format.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/paragraph_style.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/quick_insert.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/quote.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/table.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/save.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/url.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/video.min.js"></script>

  <!-- Angular -->
    <script src="js/lib/angular.min.js"></script>
    <!--App Controller -->
    <script src="js/app.js"></script>
    <script src="js/service.js"></script>

  </body>
</html>