<?php
session_start();
include './include/dbCon_mssql.php';
date_default_timezone_set('Asia/Bangkok');
$date         = date("Ymd");
$numrand_img  = (mt_rand());
$numrand_logo = (mt_rand());

$series_name_th = $_POST['series_name_th'];
$series_name_en = $_POST['series_name_en'];
$series_des_th  = $_POST['series_des_th'];
$series_des_en  = $_POST['series_des_en'];

$logo_series  = $_FILES['logo_series'];
$image_series = $_FILES['images'];

$url_Youtube = $_POST['url_Youtube'];
$url_360     = $_POST['url_360'];
$c_360       = $_POST['c_360'];
$switch = $_POST['switch'];

$dates=date("Y-m-d H:i:s");

function mssql_escape($str)
{
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return str_replace("'", "''", $str);
}

$file_logo = $logo_series['name'];

$query        = "SELECT COUNT(*) AS counts FROM dbo.LH_SERIES WHERE series_name_th = '$series_name_th'";
$query_result = mssql_query($query, $db_conn);

$count = mssql_fetch_row($query_result);
$count[0];
if ($count[0] >= 1) {
    //มีชื่อ series นี้อยู่แล้ว
    //header("location:series_add.php?add=0");
    $_SESSION['add'] = '2';
    echo '<script>
                        window.history.go(-1);
                    </script>';
    exit();
}

//360

if ($_FILES["c_360_img"]["name"][0] != "" && $_FILES["c_360_img"]["name"][0] != null && $c_360 != "" && $c_360 != null) {
    if (!empty($_FILES['logo_series'])) {
        for ($i = 0; $i < count($file_logo); $i++) {
            $seo_logo_series = $_POST['seo_logo_series'][0];
            $path_logo       = "fileupload/images/series_img/series_img_logo/";

            $type_logo = strrchr($_FILES['logo_series']['name'], ".");

            $newname_logo   = $date . $numrand_logo . $type_logo;
            $path_copy_logo = $path_logo . $newname_logo;

            $seo_img = $_POST["seo_logo"][$i];
            if (move_uploaded_file($_FILES['logo_series']['tmp_name'], $path_copy_logo)) {


                if ($count[0] == 0) {
                    $query_2 = "INSERT INTO dbo.LH_SERIES (series_name_th,series_name_en,series_des_th,series_des_en,series_logo,series_360,series_youtube,series_logo_seo,update_date)
            VALUES ('$series_name_th','$series_name_en','" . mssql_escape($series_des_th) . "','" . mssql_escape($series_des_en) . "','$newname_logo','$url_360','$url_Youtube','$seo_logo_series','$dates');";

                    $query_result2 = mssql_query($query_2, $db_conn);

                    $query_3       = "SELECT MAX(series_id) AS max_id FROM dbo.LH_SERIES;";
                    $query_result3 = mssql_query($query_3, $db_conn);
                    $row           = mssql_fetch_array($query_result3);

                    $max_ids      = $row['max_id'];
                    $success_logo = true;
                } else {
                    $success_logo = false;
                    break;
                }

            } else {
                $success = false;
                break;
            }
        }
    }

    $file_image = $image_series['name'];
    //$path_img[];
    print_r($file_image);
    if (!empty($_FILES['images'])) {
        $seo_img_series = $_POST['seo_img_series'][0];
        $path_img       = "fileupload/images/series_img/";

        $type_img = strrchr($file_image, ".");

        $newname_img   = $date . $numrand_img . $type_img;
        $path_copy_img = $path_img . $newname_img;
        //$seo_img = $_POST['seo_img'];

        if (move_uploaded_file($_FILES['images']['tmp_name'], $path_copy_img)) {
             $query_4 = "INSERT INTO dbo.LH_SERIES_IMG (series_id,series_img_name,series_img_seo)
                    VALUES ('$max_ids','$newname_img','$seo_img_series')";
            $query_result4   = mssql_query($query_4, $db_conn);
            $success_img     = true;
            $_SESSION['add'] = '1';
        }

    }

    $query_3       = "SELECT MAX(series_id) AS max_id FROM dbo.LH_SERIES;";
    $query_result3 = mssql_query($query_3, $db_conn);
    $row           = mssql_fetch_array($query_result3);

    $max_ids = $row['max_id'];

    $seo_img_plan3    = $_POST['seo_img_floor_paln3'][0];
    $c_360            = $_POST['c_360'];
    $numrand_img3     = (mt_rand());
    $path_copy_img360 = "fileupload/images/360_thumnail/" . $numrand_img3 . '_' . $_FILES["images_plan_3"]["name"][0];
    if (move_uploaded_file($_FILES['c_360_img']['tmp_name'][0], $path_copy_img360)) {

        $sql7 = "UPDATE LH_SERIES SET series_360 = '" . $c_360 . "',series_360_thumbnail= '" . $path_copy_img360 . "',update_date = '$dates' WHERE series_id = '" . $max_ids . "'";
        $query_r7  = mssql_query($sql7, $db_conn);
    }
} else if ($_FILES["c_360_img"]["name"][0] == "" && $_FILES["c_360_img"]["name"][0] == null && $c_360 == "" && $c_360 == null) {

    if (!empty($_FILES['logo_series'])) {
        for ($i = 0; $i < count($file_logo); $i++) {
            $seo_logo_series = $_POST['seo_logo_series'][0];
            $path_logo       = "fileupload/images/series_img/series_img_logo/";

            $type_logo = strrchr($_FILES['logo_series']['name'], ".");

            $newname_logo   = $date . $numrand_logo . $type_logo;
            $path_copy_logo = $path_logo . $newname_logo;

            $seo_img = $_POST["seo_logo"][$i];
            if (move_uploaded_file($_FILES['logo_series']['tmp_name'], $path_copy_logo)) {


                if ($count[0] == 0) {
                    $query_2 = "INSERT INTO dbo.LH_SERIES (series_name_th,series_name_en,series_des_th,series_des_en,series_logo,series_360,series_youtube,series_logo_seo,update_date,views)
            VALUES ('$series_name_th','$series_name_en','" . mssql_escape($series_des_th) . "','" . mssql_escape($series_des_en) . "','$newname_logo','$url_360','$url_Youtube','$seo_logo_series','$dates','$switch');";

                    $query_result2 = mssql_query($query_2, $db_conn);

                    $query_3       = "SELECT MAX(series_id) AS max_id FROM dbo.LH_SERIES;";
                    $query_result3 = mssql_query($query_3, $db_conn);
                    $row           = mssql_fetch_array($query_result3);

                    $max_ids      = $row['max_id'];
                    $success_logo = true;
                } else {
                    $success_logo = false;
                    break;
                }

            } else {
                $success = false;
                break;
            }
        }
    }

    $file_image = $image_series['name'];
    //$path_img[];
    //print_r($file_image);
    if (!empty($_FILES['images'])) {
        $seo_img_series = $_POST['seo_img_series'][0];
        $path_img       = "fileupload/images/series_img/";

        $type_img = strrchr($file_image, ".");

        $newname_img   = $date . $numrand_img . $type_img;
        $path_copy_img = $path_img . $newname_img;
        //$seo_img = $_POST['seo_img'];

        if (move_uploaded_file($_FILES['images']['tmp_name'], $path_copy_img)) {
            $query_4 = "INSERT INTO dbo.LH_SERIES_IMG (series_id,series_img_name,series_img_seo)
                    VALUES ('$max_ids','$newname_img','$seo_img_series')";
            $query_result4   = mssql_query($query_4, $db_conn);
            $success_img     = true;
            $_SESSION['add'] = '1';
        }

    }

    $query_3       = "SELECT MAX(series_id) AS max_id FROM dbo.LH_SERIES;";
    $query_result3 = mssql_query($query_3, $db_conn);
    $row           = mssql_fetch_array($query_result3);

    $max_ids = $row['max_id'];

    $seo_img_plan3    = $_POST['seo_img_floor_paln3'][0];
    $c_360            = $_POST['c_360'];
    $numrand_img3     = (mt_rand());
    $path_copy_img360 = "fileupload/images/360_thumnail/" . $numrand_img3 . '_' . $_FILES["images_plan_3"]["name"][0];
    if (move_uploaded_file($_FILES['c_360_img']['tmp_name'][0], $path_copy_img360)) {

        $sql7 = "UPDATE LH_SERIES SET series_360 = '" . $c_360 . "',series_360_thumbnail= '" . $path_copy_img360 . "',update_date = '$dates' WHERE series_id = '" . $max_ids . "'";
        $query_r7  = mssql_query($sql7, $db_conn);
    }
} else if ($_FILES["c_360_img"]["name"][0] != "" && $_FILES["c_360_img"]["name"][0] != null && $c_360 == "" && $c_360 == null) {
    $_SESSION['add'] = '-2';

    echo '<script>
                        window.history.go(-1);
                    </script>';
    exit();
} else if ($_FILES["c_360_img"]["name"][0] == "" && $_FILES["c_360_img"]["name"][0] == null && $c_360 != "" && $c_360 != null) {
    $_SESSION['add'] = '-2';

    echo '<script>
                        window.history.go(-1);
                    </script>';
    exit();
}

//tvc

if ($_POST['optradio_vdo'] == 'youtube') {
    $numrand_img3 = (mt_rand());
    if ($_FILES["img_youtube"]["name"] != "" && $_FILES["img_youtube"]["name"] != null) {
        $path_copy_img = "fileupload/images/tvc_plan/" . $numrand_img3 . '_' . $_FILES["img_youtube"]["name"];
        if (move_uploaded_file($_FILES['img_youtube']['tmp_name'], $path_copy_img)) {
            //echo $newname_img;
            $url_youtube = $_POST['youtube'];
            $sql7   = "UPDATE LH_SERIES SET series_youtube = '" . $url_youtube . "',series_youtube_thumbnail= '" . $path_copy_img . "',series_youtube_type = 'youtube',update_date = '$dates' WHERE series_id = '" . $max_ids . "'";
            $query_r7    = mssql_query($sql7, $db_conn);
            $query       = mssql_query($sql);

        }
    }
} else {

    $images  = $_FILES['thumbnail_vdo'];
    $vdo     = $_FILES['vdo_file'];

    $file_image = $images['name'];
    $file_vdo   = $vdo['name'];

    if ($_FILES["thumbnail_vdo"]["name"] != "" && $_FILES["thumbnail_vdo"]["name"] != null) {
        if ($_FILES["vdo_file"]["name"] != "" && $_FILES["vdo_file"]["name"] != null) {

            $numrand = (mt_rand());
            $date     = date("Y-m-d");
            $path_img = "fileupload/images/tvc_plan/";

            $type_img = strrchr($file_image, ".");
            $type_vdo = strrchr($file_vdo, ".");

            $newname_img = $date . $numrand . $type_img;
            $newname_vdo = $date . $numrand . "_vdo" . $type_vdo;

            $path_copy_img = $path_img . $newname_img;
            $path_copy_vdo = $path_img . $newname_vdo;

            if (move_uploaded_file($_FILES['thumbnail_vdo']['tmp_name'], $path_copy_img)) {
                if (move_uploaded_file($_FILES['vdo_file']['tmp_name'], $path_copy_vdo)) {

                    $sql7 = "UPDATE LH_SERIES SET series_youtube = '" . $path_copy_vdo . "',series_youtube_thumbnail= '" . $path_copy_img . "',series_youtube_type = 'vdo',update_date = '$dates' WHERE series_id = '" . $max_ids . "'";
                    $query_r7  = mssql_query($sql7, $db_conn);
                    $query     = mssql_query($sql);

                }
            }
        }
    }
}

if ($success == false) {
    //อัปโหลด logo ได้
    if ($success_logo == true) {
        if ($success_img == true) {
            header("location:series_add.php?add=1");
        } else {
            //อัปรูปไม่ได้
            header("location:series_add.php?add=-1");
        }
    }
} else {
    //อัปโหลด logo ไม่ได้
    $_SESSION['add'] = '-1';
    echo '<script>

                        window.history.go(-1);
              </script>';
}
