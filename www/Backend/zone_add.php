<?php
session_start();
include './include/dbCon_mssql.php';
include './include/UpdateSlug.php';
$zone_id = $_SESSION['group_id'];

$_GET['page'] = 'zone';

include_once('include/dbZones.php');

function mssql_escape($str)
{
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return str_replace("'", "''", $str);
}

$funZone = new dbZones();
if (isset($_POST['submit'])) {

	$zone_th = mssql_escape($_POST['zone_th']);
	$zone_en = $_POST['zone_en'];
	$zone_ac = $_POST['hobbies'];
	$zone_provice = $_POST['zone_provice'];

	$qr = $funZone->add_zone($zone_th, $zone_en, $zone_ac, $zone_provice);
	//update slug
	UpdateSlug::run(['zone']);
	if ($qr) {

		//header("location:group_add.php");
		$success = "success";
	} else {

		$error = "error"; //ค่าซ้ำ

	}
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>
 <!-- comfirm -->
     <link rel="stylesheet" href="../build/libs/bundled.css">
    <script src="../build/libs/bundled.js"></script>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
    <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
           <?php include './master/navbar.php';?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
          <?php include './master/top_nav.php';?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><a href="zone.php">ระบบจัดการข้อมูล Zone</a> > สร้างข้อมูล Zone</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <p class="font-gray-dark">
                  </p><br>
                   <?php if (isset($success)) {?>
                   <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-success col-md-4" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>สร้างข้อมูลเรียบร้อยแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-6"></div>
                   </div>
                  <?php } else if (isset($error)) {?>
                    <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-4" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>มีชื่อนี้อยู่ในระบบแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-6"></div>
                   </div>
                  <?php }?>
                  <form class="form-horizontal form-label-left" action="" method="post" id="commentForm">
                    <div class="form-group">
                      <label class="control-label col-md-2" for="first-name">ระบุชื่อโซน <span class="required">*</span>
                      </label>
                      <div class="col-md-6">
                        <input type="text"  name="zone_th" required class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อ Zone (ไทย)">
                      </div>
                    </div>
                    <div class="form-group hide-for-th">
                      <label class="control-label col-md-2" for="first-name">ระบุชื่อโซน <span class="required">*</span>
                      </label>
                      <div class="col-md-6">
                        <input type="text"  name="zone_en" required class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อ Zone (อังกฤษ)">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-2" for="first-name">ระบุจังหวัด <span class="required">*</span>
                      </label>
                      <div class="col-md-6">
                        <select name="zone_provice" class="form-control" required>
                           <option value="">-เลือกจังหวัด-</option>
                        <?php
                        	$sql_p = "SELECT * FROM LH_PROVINCES ORDER BY PROVINCE_NAME ASC";
                        	$query_p = mssql_query($sql_p);
                        	while ($rows = mssql_fetch_array($query_p)) {
                        ?>
                        	<option value="<?=$rows['PROVINCE_ID']?>"><?=$rows['PROVINCE_NAME'];?></option>
                        <?php } ?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-2" >กำหนดคุณสมบัติข้อมูล <span class="required">*</span></label>
                      <div class="col-md-3">
                      <p style="padding: 5px;">

                         <input type="checkbox" name="hobbies[]" id="hobby1" required data-parsley-mincheck="1" value="website" data-parsley-mincheck="1"  class="flat" />  ใช้บนเว็บไซต์
                        <br />

                        <input type="checkbox" name="hobbies[]" id="hobby2"   value="flm" class="flat" />  สำหรับ FLM
                        <br />
                      </p>
                        </div>
                      </div>

                      <div class="form-group">
                          <label class="control-label col-md-2" for="first-name"></label>
                          <div class="col-md-6">
                              <label for="hobbies[]"  class="error"></label>
                          </div>
                      </div>
                    </div>
                    <center><br>
                    <div class="col-md-6">
                    <button type="submit" name="submit" class="btn btn-success">Submit</button>
                    </div>
                  </center>
                  </form>
                </div>
              </div>
            </div>
          </div>
        <!-- /page content -->

        <!-- footer content -->
        <!-- <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer> -->
        <!-- /footer content -->
      </div>
    </div>
<!--  <script src="../vendors/jquery/dist/jquery.min.js"></script> -->
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
    <!-- Validate -->
    <script src="../build/js/jquery.validate.js"></script>
    <script>
        $(document).ready(function() {

            $("#commentForm").validate({
                rules: {
                    zone_th: "required",
                    zone_en: "required",
                    zone_provice : "required",

                },
                messages: {
                    zone_th: "กรุณากรอกชื่อ zone ไทย !",
                    zone_en : "กรุณากรอกชื่อ zone อังกฤษ !",
                    zone_provice : "กรุณาเลือก จังหวัด !",
                    'hobbies[]': "เลือกอย่างน้อย 1 รายการ"

                }
            });

            $("#lead_img_file").rules("add", {
                required:true,
                'hobbies[]': "เลือกอย่างน้อย 1 รายการ !"
            });

        });
    </script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

    <!-- Datatables -->
     <script>
      $(document).ready(function(){
        $("#alert").show();
            $("#alert").fadeTo(3000, 400).slideUp(500, function(){
              $("#alert").alert('close');
          });
      });
      </script>
    <!-- /jQuery Tags Input -->

  </body>
</html>