<?php
require_once 'include/dbConnect.php';

	try {

		$conn = (new dbConnect())->getConn();
        $sql = 'SELECT  a.AMPHUR_ID, a.AMPHUR_NAME, a.AMPHUR_NAME_ENG
                FROM LH_AMPHURS a
                LEFT JOIN LH_PROVINCES p
                ON a.PROVINCE_ID = p.PROVINCE_ID
                WHERE a.PROVINCE_ID = '.$_GET['province_id'];

        $result['result_data'] = $conn->query($sql);
        $result['result_data'] = $result['result_data']->fetchAll();

		echo json_encode($result);

	} catch (\Exception $e) {
		return $e->getMessage();
	}
?>