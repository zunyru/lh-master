<?php
session_start();
include './include/dbCon_mssql.php';
$group_id = $_SESSION['group_id'];
$motivo_id = $_GET['id'];

$_GET['page'] = 'motivo';

if (isset($_SESSION['add'])) {
    if($_SESSION['add'] ==1){
        $success ="success";
        $_SESSION['add']='';
    } else if($_SESSION['add'] ==2){

                $error_name="error_name"; //ค่าซ้ำ
                $_SESSION['add']='';
            }else if($_SESSION['add']== -3){
                $error_up="error_up";
                $_SESSION['add']='';
            }
        }

        ?>


        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <!-- Meta, title, CSS, favicons, etc. -->
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <title>LAND & HOUSES</title>
            <!-- Bootstrap -->
            <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
            <!-- Font Awesome -->
            <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
            <!-- NProgress -->
            <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
            <!-- iCheck -->
            <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
            <!-- bootstrap-progressbar -->
            <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
            <!-- Custom Theme Style -->
            <link href="../build/css/custom.min.css" rel="stylesheet">
            <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>

            <!-- jQuery -->
            <script src="../vendors/jquery/dist/jquery.min.js"></script>
            <!-- Fancybox image popup -->
            <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />

            <!-- uploade img -->
            <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
            <!--uploade-->
            <!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
            <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
            <!-- confirm-->
            <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
            <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>


            <style type="text/css">
            .thumbnail {
                height: 100px;
                margin: 5px;
            }
            .fileUpload {
                position: relative;
                overflow: hidden;
                //margin: 10px;
            }
            th.next.available{
                background-color: #6f9755;
            }
            th.prev.available{
                background-color: #6f9755;
            }
            .fileUpload input.upload {
                position: absolute;
                top: 0;
                right: 0;
                margin: 0;
                padding: 0;
                font-size: 20px;
                cursor: pointer;
                opacity: 0;
                filter: alpha(opacity=0);
            }
            .file-drop-zone-title {
                padding: 0px 0px !important;
            }
            #seo_img{
                display: none;
                }#seo_pdf{
                    display: none;
                    }#dateend{
                        vertical-align: top;
                    }
                </style>
                <style>
                #myProgress {
                    position: fixed;
                    bottom: 0;
                    left: 0;
                    right: 0;
                    top: 0;
                    background: rgba(0,0,0,0.5);
                    z-index: 999;
                }

                #myCenter {
                    margin-top: 230px;
                    color: #fff;
                }
            </style>
            <style>
            .file-caption-main .btn-file {
                overflow: visible;
            }

            .file-caption-main .btn-file .error {
                position: absolute;
                bottom: -32px;
                right: 30px;
            }
        </style>

    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
                        </div>

                        <div class="clearfix"></div>

                        <!-- menu profile quick info -->
                        <?php include './master/navbar.php'; ?>
                        <!-- /menu footer buttons -->
                    </div>
                </div>

                <!-- top navigation -->
                <?php include './master/top_nav.php'; ?>
                <!-- /top navigation -->
                <?php
                $sql = "SELECT * FROM LH_MOTIVO WHERE motivo_id = '" . $_GET['id'] . "'";
                $query_r = mssql_query($sql, $db_conn);
                $result = mssql_fetch_array($query_r);
                ?>
                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2><a href="motivo.php">ระบบจัดการข้อมูล Motivo</a> > แก้ข้อมูล Motivo : <?= $result['motivo_name'] ?></h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">

                                        <p class="font-gray-dark">
                                        </p><br>
                                        <?php if (isset($success)) { ?>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="form-group">
                                                <div class="alert alert-success col-md-6" id="alert">
                                                    <button type="button" class="close" data-dismiss="alert">x</button>
                                                    <strong>แก้ไขข้อมูลเรียบร้อยแล้ว</strong>
                                                </div>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                        <?php } else if (isset($error_name)) { ?>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="form-group">
                                                <div class="alert alert-danger col-md-6" id="alert">
                                                    <button type="button" class="close" data-dismiss="alert">x</button>
                                                    <strong>มีชื่อนี้อยู่ในระบบแล้ว</strong>
                                                </div>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                        <?php } ?>
                                        <?php if (isset($error_up)) { ?>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="form-group">
                                                <div class="alert alert-danger col-md-6" id="alert">
                                                    <button type="button" class="close" data-dismiss="alert">x</button>
                                                    <strong>เกิดข้อผิดพลาด !</strong> กรุณากรอกข้มูลให้ครบถ้วน
                                                </div>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                        <?php } ?>
                                        <form class="form-horizontal form-label-left" action="update_motivo.php" method="post" enctype="multipart/form-data" id="commentForm">
                                            <div class="form-group">
                                                <label class="control-label col-md-2" for="first-name">ชื่อนิตยสาร <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6">
                                                    <input type="hidden" name="id" value="<?= $result['motivo_id'] ?>">
                                                    <input type="text" id="first-name2" name="motivo_name" required="required" class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อนิตยสาร" value="<?= htmlspecialchars($result['motivo_name']); ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2" for="first-name">รายละเอียดนิตยสาร <span class="required"></span>
                                                </label>
                                                <div class="col-md-6">
                                                    <textarea name="motivo_detail"  rows="7" class="form-control" placeholder="รายละเอียดนิตยสาร" ><?= $result['motivo_detail']; ?></textarea>
                                                </div>
                                            </div>
                                            <?php
                                            //echo ''.(!empty($_GET['icon_id'])? $strDate." - ".$strDate2 : "" ).'';
                                            ?>

                                            <!-- <div class="form-group">
                                              <label class="control-label col-md-2" for="first-name">ช่วงเวลา <span class="required">*</span>
                                              </label>
                                              <div class="col-md-6">

                                                <input type="text" class="form-control has-feedback-left active" id="reservation" name="reservation" placeholder="ระบุช่วงเวลา" value="<?= $result['motivo_date']; ?>" />
                                                  <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                              </div>
                                          </div> -->
                                          <?php
                                          $sql = "SELECT * FROM LH_MOTIVO WHERE motivo_id = '$motivo_id' ";
                                          $query = mssql_query($sql, $db_conn);
                                          while ($row = mssql_fetch_array($query)) {

                                            $date_start = $row['motivo_date_start'];
                                            $month_start = substr($date_start, 5, 2);
                                            $year_start = substr($date_start, 0, 4);

                                            $date_end = $row['motivo_date_end'];
                                            $month_end = substr($date_end, 5, 2);
                                            $year_end = substr($date_end, 0, 4);

                                            $arrayName = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
                                            $arraymonth = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
                                        }
                                        ?>

                                        <div class="form-inline">
                                            <label class="control-label col-md-2" for="exampleInputEmail3">ระบุช่วงเวลาเริ่มต้น </label>
                                            <div class="form-group">
                                                <div class="col-md-6" >
                                                    <select id="select_month1" class="form-control" name="month_start">
                                                        <option value="">เลือกเดือน</option>
                                                        <?php
                                                        for ($i = 0; $i <= 11; $i++) {
                                                            if ($month_start == $arraymonth[$i]) {
                                                                $selected = 'selected';
                                                            } else {
                                                                $selected = '';
                                                            }
                                                            ?>
                                                            <option value="<?php echo $arraymonth[$i]; ?>"<?php echo $selected ?>><?php echo $arrayName[$i]; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4" >
                                                        <?php
                                                        $year1 = date("Y")-10;
                                                        for ($i = 1; $i <= 21; $i++) {
                                                            $year_all[$i] = $year1;
                                                            $year1++;
                                                        }
                                                        ?>
                                                        <select class="form-control" name="year_start" id="select_year1">
                                                            <option value="">เลือกปี</option>
                                                            <?php for ($i = 1; $i <= 11; $i++) { ?>
                                                            <?php
                                                            if ($year_all[$i] == $year_start) {
                                                                $selected = 'selected';
                                                            } else {
                                                                $selected = '';
                                                            }
                                                            ?>
                                                            <option id="<?php echo ($year_all[$i]); ?>" value="<?php echo ($year_all[$i]); ?>" <?php echo $selected ?> ><?php echo($year_all[$i]); ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <label class="control-label" id="dateend" for="exampleInputPassword3">เวลาสิ้นสุด </label>
                                                <div class="form-group">
                                                    <div class="col-md-6" >
                                                        <select class="form-control" name="month_end" id="select_month2" >
                                                            <option value="">เลือกเดือน</option>
                                                            <?php
                                                            for ($i = 0; $i <= 11; $i++) {

                                                                if($month_start == $arraymonth[$i] ){
                                                                    $mon  = "id='mon'";
                                                                }else{
                                                                    $mon = '';
                                                                }

                                                                if ($month_end == $arraymonth[$i]) {
                                                                    $selected = 'selected';
                                                                } else {
                                                                    $selected = '';
                                                                }
                                                                ?>
                                                                <option <?=$mon?> value="<?php echo $arraymonth[$i]; ?>"<?php echo $selected ?>><?php echo $arrayName[$i]; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select class="form-control" name="year_end" id="select_year2" >
                                                                <option value="">เลือกปี</option>
                                                                <?php for ($i = 1; $i <= 21; $i++) { ?>
                                                                <?php
                                                                if($year_start == $year_all[$i] ){
                                                                    $end  = "id='end'";
                                                                }else{
                                                                    $end = '';
                                                                }

                                                                if ($year_all[$i] == $year_end) {
                                                                    $selected = 'selected';
                                                                } else {
                                                                    $selected = '';
                                                                }
                                                                ?>
                                                                <option <?=$end?> value="<?php echo ($year_all[$i]); ?>" <?php echo $selected ?> ><?php echo($year_all[$i]); ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>

                                                <?php
                                                $req = "*";
                                                $required = "required";
                                                ?>

                                                <?php
                                                if (isset($result['motivo_img_master']) && ($result['motivo_img_master'] != "")) {
                                                    $req = "";
                                                    $required = "";
                                                    ?>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-2">รูปตัวอย่างหน้าปก <br>(ขนาด 658 x 786 px)<span class="required"></span></label>
                                                        <div class="col-md-6">
                                                            <div id="map-image-holder-brochure"></div>
                                                            <?php if (isset($result['motivo_img_master'])) { ?>
                                                            <div class="file-preview ">
                                                                <div class="close fileinput-remove"></div>
                                                                <div class="file-drop-disabled">
                                                                    <div class="file-preview-thumbnails">
                                                                        <div class="file-initial-thumbs">
                                                                            <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                                                                <div class="kv-file-content">
                                                                                    <a class="fancybox" href="fileupload/images/motivo/<?= $result['motivo_img_master']; ?>">
                                                                                        <img src="fileupload/images/motivo/<?= $result['motivo_img_master']; ?>" class="kv-preview-data file-preview-image"  style="width:auto;height:160px;">
                                                                                    </a>
                                                                                </div>
                                                                                <div class="file-thumbnail-footer">
                                                                                    <div class="file-actions">
                                                                                        <div class="file-footer-buttons">
                                                                                            <button type="button" id="<?php echo $result['motivo_id']; ?>" onclick="deleteImageMotivo(<?php echo $result['motivo_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                                                            <a class="fancybox" href="fileupload/images/motivo/<?= $result['motivo_img_master'] ?>">
                                                                                                <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                                                            </a>
                                                                                        </div>
                                                                                        <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                    <div class="file-preview-status text-center text-success"></div>
                                                                    <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                                                </div>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>


                                                    <?php } ?>

                                                    <div class="form-group">

                                                        <label class="control-label col-md-2">เลือกรูปหน้าปก <?php echo $req ?><br>(ขนาด 658 x 786 px)</label><span class="required"></span>

                                                        <div class="col-md-6">

                                                            <input type="file" id="images_motivi" name="images" <?php echo $required; ?> multiple class="file-loading" accept="image/*" >
                                                            <!-- <input id="project_review_img" name="project_review_img" <?php echo $required; ?> type="file"  class="file-loading" > -->
                                                        </div>
                                                    </div>

                                                    <script>
                                                        $("#images_motivi").fileinput({
                                                    uploadUrl: "upload.php", // server upload action
                                                    maxFileCount: 1,
                                                    allowedFileExtensions: ['jpg', 'png', 'gif', 'jpeg'],
                                                    browseLabel: 'เลือกรูปหน้าปก',
                                                    removeLabel: 'ลบ',
                                                    browseClass: 'btn btn-success',
                                                    showUpload: false,
                                                    showRemove:false,
                                                    showCaption: false,
                                                    msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                                    msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                    msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                    xxx: 'seo_img',
                                                    dropZoneTitle: 'รูปหน้าปก',
                                                    minImageWidth: 658, //ขนาด กว้าง ต่ำสุด
                                                    minImageHeight: 786, //ขนาด ศุง ต่ำสุด
                                                    maxImageWidth: 658, //ขนาด กว้าง ต่ำสุด
                                                    maxImageHeight: 786, //ขนาด ศุง ต่ำสุด
                                                    maxFileSize :300 ,
                                                    msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                                    msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                                    msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                                    msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                                    msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.', 
                                                });
                                            </script>
                                            <?php
                                            $req = "*";
                                            $required = "required";
                                            ?>

                                            <?php
                                            if (isset($result['motivo_file']) && ($result['motivo_file'] != "")) {
                                                $req = "";
                                                $required = "";
                                                ?>

                                                <div class="form-group">
                                                    <label class="control-label col-md-2">เนื้อหาไฟล์ (PDF) <span class="required"></span></label>
                                                    <div class="col-md-6">
                                                        <div id="map-image-holder-brochure"></div>
                                                        <?php if (isset($result['motivo_file'])) { ?>
                                                        <div class="file-preview ">
                                                            <div class="close fileinput-remove"></div>
                                                            <div class="file-drop-disabled">
                                                                <div class="file-preview-thumbnails">
                                                                    <div class="file-initial-thumbs">
                                                                        <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                                                            <div class="kv-file-content">
                                                                                <a class="fancybox" href="fileupload/images/motivo/<?= $result['motivo_file']; ?>" target="_blank" >
                                                                                    <i class="fa fa-file-pdf-o text-danger" style="width: 160px;font-size: 114px;"></i>
                                                                                </a>
                                                                            </div>
                                                                            <div class="file-thumbnail-footer">
                                                                                <p><?= $result['motivo_name'] ?></p>
                                                                                <div class="file-actions">
                                                                                    <div class="file-footer-buttons">
                                                                                        <button type="button" id="<?php echo $result['motivo_id']; ?>" onclick="deleteFileMotivo(<?php echo $result['motivo_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                                                        <a class="fancybox" href="fileupload/images/motivo/<?= $result['motivo_file'] ?>" target="_blank" >
                                                                                            <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                                                        </a>
                                                                                    </div>
                                                                                    <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <div class="file-preview-status text-center text-success"></div>
                                                                <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                <div class="form-group">

                                                    <label class="control-label col-md-2">เลือกเนื้อหาไฟล์ <?php echo $req ?> <span class="required"></span></label>

                                                    <div class="col-md-6">

                                                        <input type="file" id="pdf" name="pdf" <?php echo $required; ?>  class="file-loading">
                                                    </div>
                                                </div>
                                                <script>
                                                    $("#pdf").fileinput({
                                                    uploadUrl: "upload.php", // server upload action
                                                    maxFileCount: 1,
                                                    allowedFileExtensions: ["pdf"],
                                                    browseLabel: 'เลือกไฟล์ PDF',
                                                    removeLabel: 'ลบ',
                                                    browseClass: 'btn btn-success',
                                                    maxFileSize: 102400, // max size
                                                    showUpload: false,
                                                    showRemove:false,
                                                    showCaption: false,
                                                    msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                                                    msgInvalidFileExtension: 'ไฟล์ "{name}" เป็นไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ "{extensions}"',
                                                    msgSizeTooLarge: 'ไฟล์ "{name}" (<b>{size} KB</b>) มีขนาดเกินที่ระบบอนุญาตที่ <b>{maxSize} KB</b>, กรุณาลองใหม่อีกครั้ง!',
                                                    msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                    xxx: 'seo_pdf',
                                                    dropZoneTitle: 'ไฟล์ PDF',
                                                    previewFileIconSettings: {
                                                        'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                                                    },
                                                });
                                                // }).on('fileuploaded', function(event, files, extra) {
                                                //     console.log({size})
                                                //      console.log(files)
                                                //       console.log(extra)
                                                // });
                                            </script>


                                            <div class="form-group">
                                                <label class="control-label col-md-2" >สถานะ <span class="required"></span></label>
                                                <div class="col-md-3">
                                                    <p style="padding: 5px;">
                                                        <input type="checkbox" name="status" id="hobby1"   value="New" data-parsley-mincheck="1" <?php
                                                        if ($result['motivo_status'] == 'New') {
                                                            echo "checked";
                                                        } else {
                                                            echo "";
                                                        }
                                                        ?>   class="flat" />  New
                                                    </p>
                                                </div>
                                            </div>


                                            <center><br>
                                                <div class="col-md-8">

                                                    <a class="confirms" data-title="ยืนยันการลบข้อมูล" name="delete" href="delete_motivo.php?id_d=<?= $result['motivo_id'] ?>">
                                                        <button type="button" class="btn btn-danger">Delete</button></a>
                                                        <button type="submit" name="submit" class="btn btn-success" onclick="move()">Update</button>

                                                    </div>
                                                </center>
                                            </form>
                                            <br>
                                            <br>
                                            <br>
                                            <br>

                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-4" > <span class="required"></span></label>
                                            <div class="col-md-3">
                                                <div id="myProgress">
                                                    <!-- <div id="myBar"></div> -->
                                                    <center id="myCenter"><img  src="fileupload/images/motivo/cloud_upload_256.gif" width="50" height="50">
                                                        <p>กำลังดำเนินการอัปโหลดไฟล์...</p></center>
                                                    </div>
                                                </div>
                                            </div>

                                            <script>
                                                $(document).ready(function () {
                                                    $("#myProgress").hide();
                                                });
                                            </script>

                                            <script type="text/javascript">

                                                function move() {
                                                    if (window.ActiveXObject) {
                                                        var fso = new ActiveXObject("Scripting.FileSystemObject");
                                                        var filepath = document.getElementById('pdf').value;
                                                        var thefile = fso.getFile(filepath);
                                                        var sizeinbytes = thefile.size;
                                                    } else {
                                                        var sizeinbytes = document.getElementById('pdf').files[0].size;
                                                    }

                                                    var fSExt = new Array('Bytes', 'KB', 'MB', 'GB');
                                                    fSize = sizeinbytes;
                                                    i = 0;
                                                    while (fSize > 900) {
                                                        fSize /= 1024;
                                                        i++;
                                                    }

                                                    var progess2 = (Math.round(fSize * 100) * 100) + ' ' + fSExt[i];

                                                    var progess = (Math.round(fSize * 100) / 100);
                                                    parseInt(progess);
                                            //alert(progess);
                                            var elem = document.getElementById("myBar");
                                            var width = 1;
                                            var id = setInterval(frame, 1000);
                                            function frame() {
                                                if (width >= 100) {
                                                    clearInterval(id);
                                                    //alert("OK");
                                                } else {
                                                    width++;
                                                    //elem.style.width = width + '%';
                                                }
                                            }

                                            // $("#myProgress").show();
                                        }
                                    </script>


                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /page content -->

                    <!-- footer content -->
                    <!-- <footer>
                      <div class="pull-right">
                        Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
                      </div>
                      <div class="clearfix"></div>
                  </footer> -->
                  <!-- /footer content -->
              </div>
          </div>
      </div>

      <!-- Bootstrap -->
      <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
      <!-- FastClick -->
      <script src="../vendors/fastclick/lib/fastclick.js"></script>
      <!-- NProgress -->
      <script src="../vendors/nprogress/nprogress.js"></script>
      <script type="text/javascript" src="js/bootstrap-imgupload.js"></script>
      <!-- Validate -->
      <script src="../build/js/jquery.validate.js"></script>
      <!-- iCheck -->
      <script src="../vendors/iCheck/icheck.min.js"></script>
      <!-- Custom Theme Scripts -->
      <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>
      <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>
      <!-- bootstrap-progressbar -->
      <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
      <!-- Custom Theme Scripts -->
      <script src="../build/js/custom.min.js"></script>
      <script>
        $(document).ready(function () {

            $("#commentForm").validate({
                rules: {
                    motivo_name: "required",

                },
                messages: {
                    motivo_name: "กรุณากรอกชื่อนิตยสาร !",
                    images: " &nbsp; กรุณาเลือกรูป !",
                    pdf: "  &nbsp; กรุณาเลือกไฟล์ !",
                }
            });

        });
    </script>
    <script type="text/javascript">

        $('a.confirms').confirm({
            content: 'คุณแน่ใจที่จะลบข้อมูลนี้ !',
            buttons: {
                Yes: {
                    text: 'Yes',
                    btnClass: 'btn-danger',
                    keys: ['enter', 'a'],
                    action: function () {
                        location.href = this.$target.attr('href');
                    }
                },
                No: {
                    text: 'No',
                    btnClass: 'btn-default',
                    keys: ['enter', 'a'],
                    action: function () {
                                                            // button action.
                                                        }
                                                    },

                                                }
                                            });
                                        </script>
                                        <script>
                                            $(document).ready(function () {
                                                $("#alert").show();
                                                $("#alert").fadeTo(3000, 500).slideUp(500, function () {
                                                    $("#alert").alert('close');
                                                });
                                            });
                                        </script>

                                        <script type="text/javascript">
                                            $('.fancybox').fancybox();
                                        </script>
                                        <script type="text/javascript" src="js/bootstrap-imgupload.js"></script>

                                        <!-- /jQuery Tags Input -->

                                        <script>
                                            function deleteImageMotivo(id) {
                                                $.confirm({
                                                    title: 'ลบรูปนี้ !',
                                                    content: 'คุณแน่ใจที่จะลบรูปนี้!',
                                                    buttons: {
                                                        Yes: {
                                                            text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                                var self = this;
                                return $.ajax({
                                    url: 'ajax_deleteImageMotivo.php?motivo_id=' + id,
                                    dataType: 'json',
                                    method: 'get'
                                }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                }).fail(function () {
                                    self.setContent('Something went wrong.');
                                });
                            }
                        },
                        No: function () {
                            //$.alert('ยกลเิก!');
                        },
                    }
                });
                                            }
                                        </script>
                                        <script>
                                            function deleteFileMotivo(id) {
                                                $.confirm({
                                                    title: 'ลบไฟล์นี้ !',
                                                    content: 'คุณแน่ใจที่จะลบไฟล์นี้!',
                                                    buttons: {
                                                        Yes: {
                                                            text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                                var self = this;
                                return $.ajax({
                                    url: 'ajax_deleteFileMotivo.php?motivo_id=' + id,
                                    dataType: 'json',
                                    method: 'get'
                                }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                }).fail(function () {
                                    self.setContent('Something went wrong.');
                                });
                            }
                        },
                        No: function () {
                            //$.alert('ยกลเิก!');
                        },
                    }
                });
                                            }
                                        </script>
                                        <script type="text/javascript">
                                            $(document).ready(function(){
                                                var y_start = $("#select_year1").val();
                                                var y_end = $("#select_year2").val();
                                                $("#end").prevAll().attr('disabled', 'true');
                                                if(y_start == y_end){
                                                    $('#mon').prevAll().attr('disabled','true');
                                                }
                                            });
                                        </script>
                                        <script>
                                            $('#select_month1').change(function(){
                                              var y_start = $("#select_year1").val();
                                              var y_end = $("#select_year2").val();
                                              if(y_start == y_end){
                                                $('#select_month2 option:selected').prevAll().removeAttr('disabled','true');
                                                var m_start = $("#select_month1").val();
                                                $("#select_month2").val(m_start);
                                                $('#select_month2 option:selected').prevAll().attr('disabled','true');
                                            }
                                        });

                                            $('#select_year1').change(function(){
                                                var y_start = $("#select_year1").val();
                                                $('#select_year2 option:selected').prevAll().removeAttr('disabled','true');
                                                $("#select_year2").val(y_start);
                                                var y_end = $("#select_year2").val();
                                                $("#select_year2 option:selected").prevAll().attr('disabled', 'true');
                                                if(y_start == y_end){
                                                    $('#select_month2 option:selected').prevAll().removeAttr('disabled','true');
                                                    var m_start = $("#select_month1").val();
                                                    $("#select_month2").val(m_start);
                                                    $('#select_month2 option:selected').prevAll().attr('disabled','true');
                                                }
                                            });

                                            $('#select_year2').change(function(){
                                              var y_start = $("#select_year1").val();
                                              var y_end = $("#select_year2").val();
                                              if(y_start == y_end){
                                                $('#select_month2 option:selected').prevAll().removeAttr('disabled','true');
                                                var m_start = $("#select_month1").val();
                                                $("#select_month2").val(m_start);
                                                $('#select_month2 option:selected').prevAll().attr('disabled','true');
                                            }else if(y_start != y_end){
                                             $('#select_month2 option:selected').prevAll().removeAttr('disabled','true');
                                         }
                                     });
                                 </script>
                                  <script src="js/validate_file_300kb.js"></script>



                             </body>
                             </html>