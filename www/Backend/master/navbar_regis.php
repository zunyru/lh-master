<?php
header( 'Content-Type:text/html; charset=utf8');

if(empty($_SESSION['group_status'])){
    header("location:login.php?errors=e");
}
$active_product='';
$active_from='';
$active_group='';
$active_homesle='';
$active_brand='';
$active_homemodel='';
$active_zone='';
$active_promotion='';
$active_motivo='';
$active_tvc='';
$active_landing_page='';
$active_lead_image='';
$active_managment='';

if($_GET['page']=='product'){
    $active_product='current-page';
}else if($_GET['page']=='form'){
    $active_from='current-page';
}elseif ($_GET['page']=='group') {
    $active_group='current-page';
}elseif($_GET['page']=='homesles'){
    $active_homesle='current-page';
}elseif ($_GET['page']=='brand') {
    $active_brand='current-page';
}elseif ($_GET['page']=='homemodel') {
    $active_homemodel='current-page';
}elseif ($_GET['page']=='zone') {
    $active_zone='current-page';
}elseif($_GET['page']=='promotion'){
    $active_promotion='current-page';
}elseif($_GET['page']=='motivo'){
    $active_motivo='current-page';
}elseif($_GET['page']=='tvc'){
    $active_tvc='current-page';
}elseif ($_GET['page']=='landing_page') {
    $active_landing_page ='landing_page';
}elseif ($_GET['page']=='lead_image') {
    $active_lead_image = 'current-page';
}else if($_GET['page']=='managment'){
    $active_managment='current-page';
}else if($_GET['page'] == 'regis'){
    $active_regis = 'current-page';
}

?>
<?php if($_SESSION['group_status']== 'Admin'){?>
    <div class="profile">
        <div class="profile_pic">
            <img src="../images/img.jpg" alt="..." class="img-circle profile_img">
        </div>
        <div class="profile_info">
            <span>Welcome,</span>
            <h2>Admin</h2>
        </div>
    </div>
    <!-- /menu profile quick info -->

    <br />
    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <div class="menu_section">
            <h3>Admin</h3>
            <ul class="nav side-menu">
                <li class="<?=$active_from?>"><a href="../project.php"><i class="fa fa-home"></i> ระบบจัดการข้อมูลโครงการ </a></li>
                <li class="<?=$active_brand?>"><a href="../brand.php"><img src="../images/brand.png" width="25px"></i> ระบบจัดการข้อมูล Brand </a></li>
                <li class="<?=$active_homemodel?>"><a href="../homemodel.php"><img src="../images/plan.png" width="25px"></i> ระบบจัดการข้อมูลแบบบ้าน <!-- <span class="fa fa-chevron-down"></span> --></a>
                    <!-- <ul class="nav child_menu">
                      <li><a href="homemodel.php">การจัดการข้อมูลแบบบ้าน</a></li>
                      <li><a href="series.php">การจัดการข้อมูลสไตล์</a></li>
                      <li><a href="fucntion_home.php">การจัดการฟังก์ชันบ้าน</a></li>
                      <li><a href="fucntion_condo.php">การจัดการฟังก์ชันคอนโด</a></li>
                      <li><a href="icon_activity.php">การจัดการไอคอนกิจกรรม</a></li>
                    </ul> -->
                </li>

                <li class="<?=$active_zone?>"><a href="../zone.php"><img src="../images/zone.png" width="25px"> ระบบจัดการข้อมูล Zone </a></li>
                <li class="<?=$active_product?>"><a href="../product.php"><img src="../images/produck.png" width="25px"> ระบบจัดการประเภทสินค้า </a></li>
                <li class="<?=$active_promotion?>"><a href="../promotion.php"><img src="../images/promotion.png" width="25px">ระบบจัดการข้อมูลโปรโมชั่น </a></li>
                <li class="<?=$active_motivo?>"><a href="../motivo.php"><i class="fa fa-file-pdf-o "></i> ระบบจัดการข้อมูล Motivo </a></li>
                <li class="<?=$active_tvc?>"><a href="../tvc.php"><i class="fa fa-file-video-o"></i> ระบบจัดการข้อมูล TVC </a></li>

                <li class="<?=$active_managment?>"><a href="../managment_web.php"><i class="fa fa-cog"></i> ระบบจัดการ Content </a>
                    <!-- <ul class="nav child_menu">
                      <li><a href="index.html">การจัดการ Project Review</a></li>
                      <li><a href="index2.html">การจัดการ Living Tips</a></li>
                      <li><a href="index3.html">การจัดการ Community</a></li>
                      <li><a href="index3.html">การจัดการ Highlights</a></li>
                    </ul> -->
                </li>
<!--                <li class="--><?//=$active_regis?><!--"><a href="apply1.php"><img src="../images/job.png" width="25px"> ระบบจัดการข้อมูลสมัครงาน </a></li>-->
                <li class="<?=$active_landing_page?>"><a href="../landing_page.php"><i class="fa fa-file-powerpoint-o "></i> Landing Page </a></li>
                <li class="<?=$active_lead_image?>"><a href="../banner.php"><i class="fa fa-indent "></i> ระบบจัดการ Banner </a></li>
                <li class="<?=$active_group?>"><a href="../group.php"><i class="fa fa-user "></i> ระบบจัดการ Group </a></li>
            </ul>
        </div>
    </div>
    <!-- /sidebar menu -->
    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
        <a href="../logout.php"  title="Logout">
            <span class="glyphicon glyphicon-off" ></span>
        </a>
    </div>
<?php }else if($_SESSION['group_status']=='sell'){?>
    <div class="profile">
        <div class="profile_pic">
            <img src="images/picture.jpg" alt="..." class="img-circle profile_img">
        </div>
        <div class="profile_info">
            <span>Welcome,</span>
            <h2>Seller</h2>
        </div>
    </div>
    <!-- /menu profile quick info -->

    <br />
    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <div class="menu_section">
            <h3><?=$_SESSION['group_name']?></h3>
            <ul class="nav side-menu">
                <li class="<?=$active_from?>"><a href="project.php"><i class="fa fa-home"></i> ระบบจัดการข้อมูลโครงการ </a></li>
                <li class="<?=$active_promotion?>"><a href="promotion.php"><img src="images/promotion.png" width="25px"> ระบบจัดการข้อมูลโปรโมชั่น </a></li>
            </ul>
        </div>
    </div>
    <!-- /sidebar menu -->
    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
        <a href="logout.php"  title="Logout">
            <span class="glyphicon glyphicon-off" ></span>
        </a>
    </div>
<?php }else if($_SESSION['group_status']=='job'){?>
    <div class="profile">
        <div class="profile_pic">
            <img src="../images/hr.png" alt="..." class="img-circle profile_img">
        </div>
        <div class="profile_info">
            <span>Welcome,</span>
            <h2>Job</h2>
        </div>
    </div>
    <!-- /menu profile quick info -->

    <br />
    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <div class="menu_section">
            <h3><?=$_SESSION['group_name']?></h3>
            <ul class="nav side-menu">
                <li class=""><a href="#"><img src="../images/job.png" width="25px"> ระบบจัดการข้อมูลสมัครงาน </a></li>
            </ul>
        </div>
    </div>
    <!-- /sidebar menu -->
    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
        <a href="../logout.php"  title="Logout">
            <span class="glyphicon glyphicon-off" ></span>
        </a>
    </div>
<?php } ?>