<?php
session_start();
include './include/dbCon_mssql.php';
date_default_timezone_set('Asia/Bangkok');
$dates2=date("Y-m-d H:i:s");

$sql   = "SELECT COUNT(*) AS counts FROM LH_TVC WHERE tvc_name = '" . $_POST['tvc_name'] . "'";
$query = mssql_query($sql);
$nums  = mssql_fetch_array($query);

if ($nums['counts'] >= 1) {
     $_SESSION['add']='2';
             echo '<script>
                        window.history.go(-1);
                    </script>';
            exit();
}

function mssql_escape($str)
{
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return str_replace("'", "''", $str);
}

$dates = $_POST['tvc_public_date'];

$year = substr($dates, 6, 4);
$day  = substr($dates, 0, 2);
$mont = substr($dates, 3, 2);
$dates = $year . "/" . $mont . "/" . $day;

$page = $_POST['optradio_vdo'];

if ($page == "vdo") {

    $numrand = (mt_rand());
    $images  = $_FILES['thumbnail_vdo'];
    $vdo     = $_FILES['vdo_file'];
    $seo_thumbnail_vdo = $_POST['seo_thumbnail_vdo'][0];

    //print_r($vdo );
    $file_image = $images['name'];
    $file_vdo   = $vdo['name'];

    $total = count($_FILES['vdo_file']['name']);

    //for($i=0; $i<$total; $i++) {

    if ($_FILES["thumbnail_vdo"]["name"] != "" && $_FILES["vdo_file"]["name"] != "") {
        $date     = date("Y-m-d");

        $path_img = "fileupload/images/tvc/";

        $type_img = strrchr($file_image, ".");
        $type_vdo = strrchr($file_vdo, ".");

        $newname_img = $date . $numrand . $type_img;
        $newname_vdo = $date . $numrand . "_vdo" . $type_vdo;

        $path_copy_img = $path_img . $newname_img;
        $path_copy_vdo = $path_img . $newname_vdo;

        $images = $_FILES["lead_img_banner"]["tmp_name"][$i];
        $new_images_tvc = "Thumbnails_".$path_copy_img;
        $new_images_vdo = "Thumbnails_".$path_copy_vdo;
        //copy($_FILES["images_plan"]["tmp_name"][0],"fileupload/images/galery_plan/".$_FILES["images_plan"]["name"][0]);
        $width=600; //*** Fix Width & Heigh (Autu caculate) ***//
        $size=GetimageSize($images);
        $height=round($width*$size[1]/$size[0]);
        $images_orig = ImageCreateFromJPEG($images);
        $photoX = ImagesX($images_orig);
        $photoY = ImagesY($images_orig);
        $images_fin = ImageCreateTrueColor($width, $height);
        ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
        ImageJPEG($images_fin,"fileupload/images/tvc/".$new_images,93);
        ImageJPEG($images_fin,"fileupload/images/tvc/".$new_images_vdo,93);
        ImageDestroy($images_orig);
        ImageDestroy($images_fin); 

        if (move_uploaded_file($_FILES['thumbnail_vdo']['tmp_name'], $path_copy_img)) {
            if (move_uploaded_file($_FILES['vdo_file']['tmp_name'], $path_copy_vdo)) {

                $sql = "INSERT INTO LH_TVC (tvc_name,tvc_detail,tvc_youtube_link,tvc_url,tvc_public_date,tvc_thumbnail,tvc_type,tvc_thumbnail_vdo_seo,update_date) "
                . " VALUES ('" . mssql_escape($_POST['tvc_name']) . "','" . mssql_escape($_POST['tvc_detail']) . "','" . $path_copy_vdo . "','" . $_POST['tvc_url'] . "','" . $dates . "',"
                    . "'$path_copy_img','$page','$seo_thumbnail_vdo','$dates2')";
                $query = mssql_query($sql);
                $_SESSION['add']='1';
                header("location:tvc_add.php?add=1");

            }
        }

    }else{
        $_SESSION['add']='-1';
         header("location:tvc_add.php?add=-1");
    }
} else if ($page == 'youtube') {

    $numrand_img3 = (mt_rand());
    $images       = $_FILES['img_youtube'];
    $seo_img_youtube = $_POST['seo_img_youtube'][0];

    //print_r($vdo );
    $file_image = $images['name'];

    $total = count($_FILES['vdo_file']['name']);

    if ($_FILES["img_youtube"]["name"] != "" && $_FILES["img_youtube"]["name"] != null) {
        $date     = date("Y-m-d");
        $path_img = "fileupload/images/tvc/";

        $type_img = strrchr($file_image, ".");

        $newname_img = $date . $numrand_img3 . $type_img;

        $path_copy_img = $path_img . $newname_img;

        if (move_uploaded_file($_FILES['img_youtube']['tmp_name'], $path_copy_img)) {

            $url_youtube = $_POST['url_youtube'];
            $sql         = "INSERT INTO LH_TVC (tvc_name,tvc_detail,tvc_youtube_link,tvc_url,tvc_public_date,tvc_thumbnail,tvc_type,tvc_thumbnail_youtube_seo) "
            . " VALUES ('" . mssql_escape($_POST['tvc_name']) . "','" . mssql_escape($_POST['tvc_detail']) . "','" . $url_youtube . "','" . $_POST['tvc_url'] . "','$dates'," . "'$path_copy_img','$page','$seo_img_youtube')";
            $query = mssql_query($sql);
            $_SESSION['add']='1';
            header("location:tvc_add.php?add=1");

        }

    } else {
        $_SESSION['add']='-1';
        header("location:tvc_add.php?add=-1");
    }

}
