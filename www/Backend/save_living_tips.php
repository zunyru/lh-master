<?php
session_start();
include './include/dbCon_mssql.php';
$date = date("Y-m-d");

$living_tip_name_th        = $_POST['living_tip_name_th'];
$living_tip_name_en        = $_POST['living_tip_name_en'];
$living_tip_category       = $_POST['living_tip_category'];
$living_tip_content_th     = $_POST['living_tip_content_th'];
$living_tip_content_en     = $_POST['living_tip_content_en'];
$living_tip_dis_content    = $_POST['living_tip_dis_content'];
$living_tip_dis_content_en = $_POST['living_tip_dis_content_en'];
$seo_living_tip_img        = $_POST['seo_living_tip_img'][0];
$date_start = $_POST['date_start'];
$custom_url = trim($_POST['custom_url']);
$h1 = $_POST["h1"];

$fileupload_th = $_FILES['fileupload_th']['name'];
$title_seo_th = $_POST['landing_page_title_seo_th'];
$description = $_POST['landing_page_description_th'];
$keyword_seo = $_POST['keyword_seo'];
$date_seo = date("YmdHis");

$day =substr($date_start, 0, 2);
$mount =substr($date_start, 3, 2);
$year =substr($date_start, 6, 4);

$date_start = $year."-".$mount."-".$day;
 $time=date("Y-m-d H:i:s");


function mssql_escape($str)
{
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return str_replace("'", "''", $str);
}
$sql          = "SELECT COUNT(*) AS num FROM LH_LIVING_TIPS WHERE living_tip_name_th = '$living_tip_name_th ' ";
$query_result = mssql_query($sql, $db_conn);
$count_date   = mssql_fetch_row($query_result);
$count_date[0];
if ($count_date[0] >= 1) {
    $_SESSION['add'] = '2';
    echo '<script>
            window.history.go(-1);
          </script>';
    exit();
}

if (empty($_FILES['living_tip_img'])) {
  $_SESSION['add'] = '-1';
  echo '<script>
  window.history.go(-1);
  </script>';
  exit();
}

$numrand_img   = (mt_rand());
$lead_img_file = $_FILES['living_tip_img'];
// print_r($lead_img_file);
$file_image = $lead_img_file['name'];

if (!empty($_FILES['living_tip_img'])) {
    //for($i=0; $i < count($file_image); $i++){
  $dates_file = date("Y-m-d");
  $path_img   = "fileupload/images/living_tips/";

     //resize
  $images = $_FILES["living_tip_img"]["tmp_name"];
  $images2 = $_FILES["living_tip_img"]["name"];
  $type_img = strrchr($images2,".");
  $new_images = "Thumbnails_".$date.$numrand_img .$type_img;
      $width=600; //*** Fix Width & Heigh (Autu caculate) ***//
      $size=GetimageSize($images);
      $height=round($width*$size[1]/$size[0]);
      $images_orig = ImageCreateFromJPEG($images);
      $photoX = ImagesX($images_orig);
      $photoY = ImagesY($images_orig);
      $images_fin = ImageCreateTrueColor($width, $height);
      ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
      ImageJPEG($images_fin,"fileupload/images/living_tips/".$new_images,93);
      ImageDestroy($images_orig);
      ImageDestroy($images_fin); 

      $newname_img   = $date . $numrand_img . $type_img;
      $path_copy_img = $path_img . $newname_img;

    //seo
      $path = "fileupload/images/landingpage/";
      $type_th = strrchr($_FILES['fileupload_th']['name'], ".");
      $newname_th = $date_seo . $numrand_th . $type_th;
      $path_seo = $path . $newname_th;

      if (move_uploaded_file($_FILES['fileupload_th']['tmp_name'], $path_seo)) {

        if (move_uploaded_file($_FILES['living_tip_img']['tmp_name'], $path_copy_img)) {

          $sql6 = "INSERT INTO LH_LIVING_TIPS "
          . "(living_tip_name_th,living_tip_name_en,living_tip_category,living_tip_content_th,living_tip_content_en,living_tip_post_date,living_tip_img,living_tip_dis_content,living_tip_img_seo,living_tip_dis_content_en,update_date,title_seo,description_seo,thumnail_seo,keyword_seo,custom_url,h1) "
          . " VALUES ('$living_tip_name_th','$living_tip_name_en','$living_tip_category','" . mssql_escape($living_tip_content_th) . "','" . mssql_escape($living_tip_content_en) . "',"
          . "'$date_start','" . $path_img . $newname_img . "','" . mssql_escape($living_tip_dis_content) . "','$seo_living_tip_img','" . mssql_escape($living_tip_dis_content_en) . "','$time','$title_seo_th','$description','$path_seo','$keyword_seo','$custom_url','$h1')";
          $query_r6 = mssql_query($sql6, $db_conn);
          if ($query_r6) {
            header("location:managment_web.php?add=1&tab=Living");

          } else {

            header("location:managment_web.php?add=-2");
          }
          $success_img = true;
        } else {
          $success         = false;
          $_SESSION['add'] = '-2';
          header("location:living_tips_add.php");
        }
      }
    }
