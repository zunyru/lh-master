<?php
require_once 'include/dbConnect.php';

try {
    $galery_plan_img_id = $_GET['galery_plan_img_id'];

    $conn = (new dbConnect())->getConn();
    $sql = "DELETE FROM LH_GALERY_PLAN
				WHERE galery_plan_id =".$galery_plan_img_id;
    $result= $conn->query($sql);

    echo json_encode($result->fetchAll());

} catch (\Exception $e) {
    return $e->getMessage();
}
?>