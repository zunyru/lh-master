<?php
session_start();
header( 'Content-Type:text/html; charset=utf8');

if (!$_SESSION['login']) {
      $link = "Location: login.php";
      header($link);
}

require_once 'include/dbConnect.php';
require_once 'include/dbCon_mssql.php'; // for insert utf8

$conn = (new dbConnect())->getConn();
$project_name_th = "";
$lastIdImageFk = 0;

if (isset($_GET['project_id'])) {

  $arrProjectName = getProjectsById($_GET['project_id']);
  $GLOBALS['project_name_th'] = $arrProjectName[0]['project_name_th'];
  //$conn->close();
}

if (isset($_POST['plan_id']) &&
  isset($_POST['number_convert']) &&
  isset($_POST['features_convert']) &&
  isset($_POST['built_on'])) {


  $dataName = date("d-m-Y-H:i:s");
  $images_id = 0;
  if ($_POST['optionsImg'] == 'option2'){

      $sql = "INSERT INTO LH_HOME_SELL_IMG (home_sell_img_name, project_id)
              VALUES ('".$GLOBALS['project_name_th']."', '".$_GET['project_id']."')";
      $result = mssql_query($sql, $GLOBALS['db_conn']);
      $maxId = getMaxId('LH_HOME_SELL_IMG', 'home_sell_img_id');
      $GLOBALS['lastIdImageFk'] = $maxId[0][0];

      $numSize = count($_FILES['galery_images']["name"]);
      for ($i=0; $i < $numSize; $i++) {
          $pathName = "fileupload/images/homesales/".$dataName.'-'.$_FILES["galery_images"]["name"][$i];
          if(move_uploaded_file($_FILES["galery_images"]["tmp_name"][$i],$pathName))
          {

            $sql = "INSERT INTO LH_HOME_SELL_IMG_FILE (home_sell_img_id, home_sell_img_name, home_sell_img_seo_name)
                    VALUES (".$GLOBALS['lastIdImageFk'].", '".$pathName."', '".$_POST['homesale_seo_img'][$i]."')";
            $result = mssql_query($sql, $GLOBALS['db_conn']);
          }
      }
  }
  if ($_POST['optionsImg'] == 'option1') {

      $img_id = array();
      $size_selected_img = count($_POST['idImageSelected']);

        if ($size_selected_img > 0) {
            for ($i=0; $i < $size_selected_img; $i++) {
              $img_id[$i] = $_POST['idImageSelected'][$i];
            }

            //get id by $_POST['idImageSelected']
            $sql = "SELECT gip.galery_project_img_id, gip.galery_project_img_name, gip.galery_project_img_seo
                    FROM LH_GALERY_IMG_PROJECT gip
                    WHERE gip.galery_project_img_id IN( ". implode($img_id, ", ") . ")";
            $result = $GLOBALS['conn']->query($sql);
            $result_img = $result->fetchAll();

            //insert new  name
            $sql = "INSERT INTO LH_HOME_SELL_IMG (home_sell_img_name, project_id)
                    VALUES ('".$GLOBALS['project_name_th']."', '".$_GET['project_id']."')";
            $result = mssql_query($sql, $GLOBALS['db_conn']);
            $maxId = getMaxId('LH_HOME_SELL_IMG', 'home_sell_img_id');
            $GLOBALS['lastIdImageFk'] = $maxId[0][0];

            //get only images name and seo
            for ($i=0; $i < count($result_img); $i++) {

                $sql = "INSERT INTO LH_HOME_SELL_IMG_FILE (home_sell_img_id, home_sell_img_name, home_sell_img_seo_name)
                        VALUES (".$GLOBALS['lastIdImageFk'].", '".$result_img[$i]['galery_project_img_name']."', '".$result_img[$i]['galery_project_img_seo']."')";
                $result = mssql_query($sql, $GLOBALS['db_conn']);
            }
        }
  }
  setHomeSales($_POST);
  //$conn->close();
}

function setHomeSales($request) {
    try {
          $size = count($request['project_sub_id']);
          for ($i=0; $i < $size; $i++) {

            $sql = "INSERT INTO lh_home_sell (project_sub_id, plan_id, number_converter, features_convert, built_on, house, image_in_home_sell_id)
                    VALUES (".$request['project_sub_id'][$i].", '".$request['plan_id']."', '".$request['number_convert']."', '".$request['features_convert']."', '".$request['built_on']."', 'N', ".$GLOBALS['lastIdImageFk']." )";

            if (isset($request['house'])) {
              $sql = "INSERT INTO lh_home_sell (project_sub_id, plan_id, number_converter, features_convert, built_on, house, image_in_home_sell_id)
                      VALUES (".$request['project_sub_id'][$i].", '".$request['plan_id']."', '".$request['number_convert']."', '".$request['features_convert']."', '".$request['built_on']."', 'Y', ".$GLOBALS['lastIdImageFk']." )";
            }
            if ($request['icon_id'] != 0) {
              $sql = "INSERT INTO lh_home_sell (project_sub_id, plan_id, number_converter, features_convert, built_on, icon_id, image_in_home_sell_id)
                    VALUES (".$request['project_sub_id'][$i].", '".$request['plan_id']."', '".$request['number_convert']."', '".$request['features_convert']."', '".$request['built_on']."', '".$request['icon_id']."', ".$GLOBALS['lastIdImageFk']." )";
            }
            if ($request['icon_id'] != 0 && isset($request['house'])) {
              $sql = "INSERT INTO lh_home_sell (project_sub_id, plan_id, number_converter, features_convert, built_on, icon_id, house, image_in_home_sell_id)
                    VALUES (".$request['project_sub_id'][$i].", '".$request['plan_id']."', '".$request['number_convert']."', '".$request['features_convert']."', '".$request['built_on']."', '".$request['icon_id']."', 'Y', ".$GLOBALS['lastIdImageFk'].")";
            }

            $result = mssql_query($sql, $GLOBALS['db_conn']);
            $maxId = getMaxId('lh_home_sell', 'home_sell_id');
            $lastId= $maxId[0][0];

            if (isset($request['peculiarity'])) {
              setPeculiaritySubHomeSales($request, $lastId);
            }
            setConditionHomeSales($request, $lastId);

            $link = "Location: homesales.php?project_id=".$_GET['project_id']."&add=y";
            header($link); /* Redirect browser */
        }
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}

function getMaxId($table, $column) {
    try {
        $sql = 'select max('.$column.') from '.$table;
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}

function setPeculiaritySubHomeSales($request, $home_sell_id) {
    try {
        $size = count($request['peculiarity']);
        for ($i=0; $i < $size; $i++) {
              $sql = "INSERT INTO LH_PECULIARITY_SUB_HOME_SELL (home_sell_id, plan_id, peculiarity_id)
                      VALUES (".$home_sell_id.", ".$request['plan_id'].",
                          ".$request['peculiarity'][$i]." )";
            $result = mssql_query($sql, $GLOBALS['db_conn']);
        }
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}

function setConditionHomeSales($request,$home_sell_id) {
    try {
          $sql = "INSERT INTO LH_CONDITION_HOME_SELL (land_size, total_price, down, money, payments, contract, transfer_money, starting_pay, interest, repayment_period, home_sell_id)
                  VALUES ('".$request['size_land']."', '".$request['sum_net']."',
                          '".$request['down']."', '".$request['money']."',
                          '".$request['money_down']."', '".$request['contract']."',
                          '".$request['transfer_money']."', '".$request['installment']."',
                          '".$request['interest']."', '".$request['length']."',
                          ".$home_sell_id." )";

        $result = mssql_query($sql, $GLOBALS['db_conn']);
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}

function getHomeSales($projectId) {
    try {
        $sql = 'select * from lh_home_sell';
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}

function getPlans() {
    try {
        $sql = 'select * from lh_plans';
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}

function getProjectsById($project_id) {
    try {
        $sql = 'select project_name_th from lh_projects where project_id='.$project_id;
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}

//befor edit add homesales to many

// function getProjectSubById() {
//     try {
//         $sql = 'SELECT PS.project_sub_id, PD.product_name_th FROM LH_PROJECT_SUB AS PS
//                 JOIN LH_PROJECTS AS P ON PS.project_id = P.project_id
//                 JOIN LH_PRODUCTS AS PD ON PS.product_id = PD.product_id
//                 WHERE PD.product_id != 3 AND PS.project_id ='.$_GET['project_id'];
//         $result = $GLOBALS['conn']->query($sql);
//         return $result->fetchAll();
//     }
//     catch(PDOException $e) {
//         echo $sql . "<br>" . $e->getMessage();
//     }

// }

function getIcon() {
    try {
        $sql = 'select * from LH_ICON_ACTIVITY';
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}

function getPeculiarity() {
    try {
        $sql = 'select * from LH_PECULIARITY';
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}

function getImgGaleryProject() {
    try {
        $sql = 'SELECT * from LH_GALERY_PROJECT
                WHERE project_id ='.$_GET['project_id'];
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}


function getProductByIdProject() {
    try {
        $sql = 'SELECT PS.project_sub_id, PD.product_name_th FROM LH_PROJECT_SUB AS PS
                JOIN LH_PROJECTS AS P ON PS.project_id = P.project_id
                JOIN LH_PRODUCTS AS PD ON PS.product_id = PD.product_id
                WHERE PD.product_id != 3 AND PS.project_id ='.$_GET['project_id'];
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}
//print_r(getHomeSales($projectid,$conn));
$group_id=$_SESSION['group_id'];

$_GET['page']='form';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>

    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
    <link href="js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <!-- Fancybox image popup -->
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />

  <style>
    .thumb-image{float:left;width:200px;position:relative;padding:5px;}
    .file-drop-zone-title {
        padding: 0px 0px !important;
    }
  </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

           <?php include "master/navbar.php"; ?>

          </div>
        </div>

        <!-- top navigation -->
          <?php include './master/top_nav.php'; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><a href="homesales.php?project_id=<?php echo $_GET['project_id']; ?>">สร้างข้อมูลบ้านพร้อมขายโครงการ</a> : <?php echo $GLOBALS['project_name_th'] ?></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <p class="font-gray-dark">
                  </p><br>
                  <form class="form-horizontal form-label-left" action="" method="POST" enctype="multipart/form-data">
                      <!-- <?php //foreach (getProjectSubById() as $value) { ?>
                        <input type="hidden" id="<?php //echo($value['product_name_th']) ?>" name="project_sub_id[]" value="<?php //echo($value['project_sub_id']) ?>">
                      <?php //} ?> -->

                      <div class="form-group">
                      <label class="control-label col-md-2" for="brand">เลือกประเภทที่อยู่อาศัยที่เปิดขาย<span class="">*</span>
                      </label>
                      <br>
                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select class="form-control" name="project_sub_id[]" required>
                            <option value="">เลือกประเภทที่อยู่อาศัย</option>
                            <?php foreach (getProductByIdProject() as $value) { ?>
                                      <?php if (count(getProductByIdProject()) == 1) { ?>
                                                  <option value="<?php  echo($value['project_sub_id']) ?>" selected><?php  echo($value['product_name_th']) ?></option>
                                      <?php } else { ?>
                                                  <option value="<?php  echo($value['project_sub_id']) ?>"><?php  echo($value['product_name_th']) ?></option>
                                      <?php } ?>
                            <?php } ?>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                      <label class="control-label col-md-2" for="brand">เลือกชื่อแบบบ้าน <span class="">*</span>
                      </label>
                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select class="form-control" name="plan_id" onChange="changePlan(value);" required>
                            <option value="">เลือกแบบบ้าน</option>
                            <?php foreach (getPlans() as $value) { ?>
                              <option value="<?php  echo($value['plan_id']) ?>">
                                <?php  echo($value['plan_name_th']) ?>
                              </option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                       <div class="form-group">
                        <label class="control-label col-md-2">ลักษณะเด่น <span class="">*</span>
                        </label>
                        <div class="col-md-6">
                          <textarea class="form-control" id="feature_th" rows="7" placeholder="กรอกลักษณะเด่น (ไทย)" disabled></textarea>
                        </div>
                       </div>
                      <div class="form-group">
                        <label class="control-label col-md-2">ลักษณะเด่น <span class="">*</span>
                        </label>
                        <div class="col-md-6">
                           <textarea class="form-control" id="feature_en" rows="7" placeholder="กรอกลักษณะเด่น (อังกฤษ)" disabled></textarea>
                        </div>
                      </div><br>
                      <div class="form-group">
                      <label class="control-label col-md-2">หมายเลขแปลง <span class="">*</span>
                      </label>
                      <div class="col-md-6">
                        <input type="text" name="number_convert" class="form-control col-md-7 col-xs-12"  required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2">ระบุลักษณะเด่นของแปลง <span class="">*</span>

                      </label>
                      <div class="col-md-6">
                        <input type="text" name="features_convert" class="form-control col-md-7 col-xs-12"  required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2">สร้างบนพื้นที่ตั้งแต่ <span class="">*</span>
                      </label>
                      <div class="col-md-4">
                        <input type="text" name="built_on" class="form-control col-md-7 col-xs-12"  required>
                      </div><label class="control-label col-md-1" style=" text-align: left; ">ตารางวา</label>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2" for="brand">พื้นที่ใช้สอย <span class="">*</span>
                      </label>
                        <div class="col-md-4 col-sm-9 col-xs-12">
                          <input type="text" id="use_ful" class="form-control col-md-7 col-xs-12"  disabled>
                        </div><label class="control-label col-md-2" style=" text-align: left; ">ตารางเมตร</label>
                      </div>
                      <div class="form-group">
                      <label class="control-label col-md-2">ถ้าเป็นบ้านพร้อมเฟอร์ ระบุ
                      </label>
                      <div class="col-md-3">
                        <div class="checkbox"><label><input type="checkbox" name="house" class="flat" > Yes</label></div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2" for="brand">ระบุไอคอนกิจกรรม (ถ้ามี)
                      </label>
                        <div class="col-md-4 col-sm-9 col-xs-12">
                          <select class="form-control" name="icon_id" onChange="changeIcon(value);">
                            <option value="0">เลือกไอคอน</option>
                            <?php foreach (getIcon() as $value) { ?>
                              <option value="<?php  echo($value['icon_id']) ?>">
                                <?php  echo($value['icon_name']) ?>
                              </option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <br><br>
                      <h4>ระบุฟังก์ชั่นของบ้าน</h4>
                      <div id="rooms">
                        <div class="form-group">
                          <label class="control-label col-md-2">
                          </label>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-md-2">ห้องนอน
                          </label>
                          <div class="col-md-3">
                            <input type="text" class="form-control col-md-7 col-xs-12 wdpercent1" disabled>
                            <label class="control-label col-md-3 wdlabel1" >ห้อง
                          </label>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-2">ห้องน้ำ
                          </label>
                          <div class="col-md-3">
                            <input type="text"  class="form-control col-md-7 col-xs-12 wdpercent1" disabled>
                            <label class="control-label col-md-3 wdlabel1" >ห้อง
                          </label>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-2">ที่จอดรถ
                          </label>
                          <div class="col-md-3">
                            <input type="text"  class="form-control col-md-7 col-xs-12 wdpercent1" disabled>
                            <label class="control-label col-md-3 wdlabel1" >ห้อง
                          </label>
                          </div>
                        </div>
                      </div>
                      <br><br>
                      <h4>ระบุรายละเอียดเพิ่มเติม (ถ้ามี) </h4>
                        <div class="form-group">
                          <label class="control-label col-md-2"></label>
                          <div class="col-md-5">
                            <p style="padding: 5px;">
                                <?php foreach (getPeculiarity() as $value) { ?>
                                <input type="checkbox" name="peculiarity[]"
                                value="<?php  echo($value['peculiarity_id']) ?>"
                                class="flat" />
                                <?php  echo($value['peculiarity_name_th']) ?>
                                <br />
                                <?php } ?>
                            <p>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-2">
                          </label>
                        <div class="col-md-5"><span style="color:#929292;">(แต่งเพิ่มพิเศษจากมาตรฐานที่มี เช่น แต่งเฟอร์ทั้งหลัง)</span>
                        </div>
                      </div>
                      <br><br>
                      <div id="icon_img"></div>
                      <div id="master"></div>
                      <div id="floors"></div>
                      <br><br>
                      <h4 style="display: inline;">ระบุภาพภายในบ้านพร้อมขาย (เลือกวิธีใดวิธีหนึ่ง) </h4><div id="alertRadio" style="color: #F44336;font-size: 22px;font-weight: 700;display: inline;"></div>
                      <div class="form-group">
                        <label class="control-label col-md-2"></label>
                        <div class="col-md-8">
                          <div class="radio">
                              <label>
                                <input type="radio" value="option1" onclick="checkRadioImage(value)" name="optionsImg"/>
                                <select class="form-control" id="option1" name="images_id" onChange="getStoreImageHomeSales(value)" disabled="true">
                                  <option value="">เลือกภาพจากส่วนกลาง</option>
                                  <?php foreach (getImgGaleryProject() as $value) { ?>
                                    <option value="<?php echo($value['galery_project_id']) ?>">
                                      <?php  echo($value['galery_project_name']) ?>
                                    </option>
                                  <?php } ?>
                                </select>
                              </label>
                            </div>
                        </div>
                      </div>
                      <!-- preview store images -->
                      <div class="form-group">
                          <label class="control-label col-md-2"></label>
                          <div class="col-md-10">
                            <div id="images_in_homesales"></div>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-2">
                          </label>
                        <div class="col-md-10">
                          <div class="radio">
                              <label>
                                <input type="radio" value="option2" onclick="checkRadioImage(value)" name="optionsImg"/>
                                <span>(อัปโหลดรูปสูงสุด 30 รูป)</span>
                                <input type="file" id="option2" name="galery_images[]" class="file-loading" multiple>
                              </label>
                            </div>
                        </div>
                      </div>
                      <br><br>
                    <h4>ตารางราคาและเงื่อนไขการผ่อน</h4>
                    <div class="form-group">
                        <label class="control-label col-md-2">ขนาดที่ดิน <span class="">*</span>
                        </label>
                        <div class="col-md-3">
                          <input type="text" name="size_land" class="form-control col-md-7 col-xs-12"  required>
                        </div><label class="control-label col-md-1 textleft">ตารางวา
                        </label>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-2">ราคารวมสุทธิ <span class="">*</span>
                        </label>
                        <div class="col-md-3">
                          <input type="text" name="sum_net" class="form-control col-md-7 col-xs-12"  required>
                        </div><label class="control-label col-md-1 textleft">บาท </label>
                      </div>
                       <div class="form-group">
                          <label class="control-label col-md-2">ดาวน์
                        </label> <div class="col-md-1">
                          <input type="text" name="down" class="form-control col-md-7 col-xs-12" >
                        </div>
                        <label class="control-label col-md-1 textleft"> % เป็นเงิน
                        </label>
                        <div class="col-md-1">
                          <input type="text" name="money" class="form-control col-md-7 col-xs-12" >
                        </div><label class="control-label col-md-1 textleft"> บาท<span class=""></span>
                        </label>
                        </div>
                        <div class="form-group">
                        <label class="control-label col-md-2">เงินจอง
                        </label>
                        <div class="col-md-3">
                          <input type="text" name="money_down" class="form-control col-md-7 col-xs-12"  >
                        </div><label class="control-label col-md-1 textleft">บาท </label>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-2">ทำสัญญา
                        </label>
                        <div class="col-md-3">
                          <input type="text" name="contract" class="form-control col-md-7 col-xs-12"  >
                        </div><label class="control-label col-md-1 textleft">บาท </label>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-2">เงินโอน  <span class="">*</span>
                        </label>
                        <div class="col-md-3">
                          <input type="text" name="transfer_money" class="form-control col-md-7 col-xs-12"  required>
                        </div><label class="control-label col-md-1 textleft">บาท </label>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-2">ผ่อนเริ่มต้น
                        </label>
                        <div class="col-md-3">
                          <input type="text" name="installment" class="form-control col-md-7 col-xs-12" >
                        </div><label class="control-label col-md-1 textleft">ต่อเดือน </label>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-2">ดอกเบี้ย
                        </label>
                        <div class="col-md-3">
                          <input type="text" name="interest" class="form-control col-md-7 col-xs-12"  >
                        </div><label class="control-label col-md-1 textleft">% </label>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-2">ระยะเวลาผ่อนชำระ
                        </label>
                        <div class="col-md-3">
                          <input type="text" name="length" class="form-control col-md-7 col-xs-12"  >
                        </div><label class="control-label col-md-1 textleft">ปี </label>
                      </div>
                      <br><br>
                      <h2>หมายเหตุ<br>
                        * กรุณาตรวจสอบเงื่อนไขและราคากับทางโครงการอีกครัง<br>
                        ** อัตราดอกเบี้ย กรุณาตรวจสอบกับธนาคารที่ใช้บริการอีกครั้ง</h2>
                      <center>
                      <button type="button" class="btn btn-success" disabled>Preview</button>
                      <button type="button" onclick="validateBeforSubmit()" class="btn btn-success">Submit</button>
                      <button type="submit" id="realSubmit" class="btn btn-success hide">Submit</button>
                    </center>
                  </form>
                </div>
              </div>
            </div>
          </div>
        <!-- /page content -->

        <!-- footer content -->
        <!-- <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer> -->
        <!-- /footer content -->
      </div>
    </div>

    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
    <!-- Fancybox image popup -->
    <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>

    <script>

      var tempClickRadioDuplicate = 0;
      $(document).ready(function() {

        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');


        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
        fancybox_init();


        $("#option2").fileinput({
            uploadUrl: "upload.php",
            dropZoneTitle : 'ลากและวางภาพภายในบ้านพร้อมขาย',
            browseLabel: 'เลือกภาพภายในบ้านพร้อมขาย',
            removeLabel: 'ลบ',
            browseClass: 'btn btn-success',
            showUpload: false,
            maxFileCount: 30,
            mainClass: "input-group-md",
            xxx:'homesale_seo_img',
            allowedFileTypes: ["image"]
        });

      });

      var url = "ajax_getplan.php";
      function changePlan(id){
        if(id != 0) {

            var link = url+'?plan_id='+id;
            $.ajax( link )
                  .done(function(response) {
                    console.log(response)
                    if (response != 0) {

                        if(response != ""){
                            if(typeof response === "object"){
                                var json_obj = response;
                            }else{
                                var json_obj =  $.parseJSON(response);//parse JSON
                            }
                        }

                        //Clear Elemant <Option>
                        $('#feature_th').empty();
                        $('#feature_th').val(json_obj.plan[0].plan_feature_th);
                        $('#feature_en').empty();
                        $('#feature_en').val(json_obj.plan[0].plan_feature_en);
                        $('#use_ful').empty();
                        $('#use_ful').val(json_obj.plan[0].plan_useful);
                        //Clear Elemant
                        $('#rooms').empty();
                        var rooms = '';
                        for (var i in json_obj.plan_sub) {
                              rooms += '<div class="form-group">\
                                          <label class="control-label col-md-2">'
                                            +json_obj.plan_sub[i].function_plan_name_th+
                                          '</label>\
                                          <div class="col-md-3">\
                                            <input type="text" class="form-control col-md-7 col-xs-12 wdpercent1" \
                                            value="'+json_obj.plan_sub[i].function_plan_sub_plan_count_r+'" disabled>\
                                            <label class="control-label col-md-3 wdlabel1">'
                                            +json_obj.plan_sub[i].function_plan_pronoun+
                                          '</label>\
                                          </div>\
                                        </div>';
                        }
                        $('#rooms').append(rooms);

                        $('#master').empty();
                        var master = '';
                           master += '<div class="form-group">\
                              <label class="control-label col-md-2">ภาพแบบบ้าน\
                              </label><div class="col-md-2"></div>\
                              <div class="col-md-10">';
                        if (json_obj.gallery_master != null && json_obj.gallery_master != '') {
                            for (var i in json_obj.gallery_master) {
                                var galleryMasterImg = 'fileupload/images/galery_plan/'+json_obj.gallery_master[i].galery_plan_name;
                                var galleryImgSeo = json_obj.gallery_master[i].galery_plan_img_seo;
                                master += `<div class="file-preview">
                                              <div class="close fileinput-remove"></div>
                                              <div class="file-drop-disabled">
                                                  <div class="file-preview-thumbnails">
                                                      <div class="file-initial-thumbs">
                                                          <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                                              <div class="kv-file-content">
                                                                  <a class="fancybox" rel="group_galery_master" href="${galleryMasterImg}">
                                                                      <img src="${galleryMasterImg}" class="kv-preview-data file-preview-image"  style="width:auto;height:160px;">
                                                                  </a>
                                                              </div>
                                                              <div class="file-thumbnail-footer">
                                                                  <p>Alt Text : ${galleryImgSeo}</p>
                                                              </div>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div class="clearfix"></div>
                                                  <div class="file-preview-status text-center text-success"></div>
                                                  <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                              </div>
                                          </div>`;
                            }
                        } else {
                          master += '<p>This plan no image.</p>';
                        }
                        master += '</div></div>';
                        $('#master').append(master);

                        $('#floors').empty();
                        var floor = '';
                        for (var i in json_obj.gallery_floor) {
                            i = parseInt(i);
                            var floorImg = 'fileupload/images/floor_plan_master/'+json_obj.gallery_floor[i].floor_plan_img_name;
                            var floorId  = json_obj.gallery_floor[i].number_floor_plan;
                            var floorSeoImg  = json_obj.gallery_floor[i].floor_plan_img_seo;
                                if (floorId != null) {
                                      if (floorId == 1) {
                                              floor += `<div class="form-group">
                                                            <label class="control-label col-md-2">ภาพ Floor Plan ชั้น 1</label>
                                                            <div class="col-md-6">
                                                                <div class="file-preview">
                                                                    <div class="close fileinput-remove"></div>
                                                                    <div class="file-drop-disabled">
                                                                        <div class="file-preview-thumbnails">
                                                                            <div class="file-initial-thumbs">
                                                                                <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                                                                    <div class="kv-file-content">
                                                                                        <a class="fancybox" href="${floorImg}">
                                                                                            <img src="${floorImg}" class="kv-preview-data file-preview-image"  style="width:auto;height:160px;">
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="file-thumbnail-footer">
                                                                                        <p>Alt Text : ${floorSeoImg}</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="file-preview-status text-center text-success"></div>
                                                                        <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>`;
                                              floor += '<div class="form-group">\
                                                            <label class="control-label col-md-2">คำบรรยาย Floor Plan ชั้น 1</label>\
                                                            <div class="col-md-6">\
                                                             <textarea class="form-control" \
                                                              rows="7" disabled>'
                                                                  +json_obj.gallery_floor[i].floor_plan_dis_th+
                                                             '</textarea>\
                                                          </div>\
                                                        </div>';
                                      }
                                      if (floorId == 2) {
                                              floor += `<div class="form-group">
                                                            <label class="control-label col-md-2">ภาพ Floor Plan ชั้น 2</label>
                                                            <div class="col-md-6">
                                                                <div class="file-preview">
                                                                    <div class="close fileinput-remove"></div>
                                                                    <div class="file-drop-disabled">
                                                                        <div class="file-preview-thumbnails">
                                                                            <div class="file-initial-thumbs">
                                                                                <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                                                                    <div class="kv-file-content">
                                                                                        <a class="fancybox" href="${floorImg}">
                                                                                            <img src="${floorImg}" class="kv-preview-data file-preview-image"  style="width:auto;height:160px;">
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="file-thumbnail-footer">
                                                                                        <p>Alt Text : ${floorSeoImg}</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="file-preview-status text-center text-success"></div>
                                                                        <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>`;
                                              floor += '<div class="form-group">\
                                                            <label class="control-label col-md-2">คำบรรยาย Floor Plan ชั้น 2</label>\
                                                            <div class="col-md-6">\
                                                             <textarea class="form-control" \
                                                              rows="7" disabled>'
                                                                  +json_obj.gallery_floor[i].floor_plan_dis_th+
                                                             '</textarea>\
                                                          </div>\
                                                        </div>';
                                      }
                                      if (floorId == 3) {
                                              floor += `<div class="form-group">
                                                            <label class="control-label col-md-2">ภาพ Floor Plan ชั้น 3</label>
                                                            <div class="col-md-6">
                                                                <div class="file-preview">
                                                                    <div class="close fileinput-remove"></div>
                                                                    <div class="file-drop-disabled">
                                                                        <div class="file-preview-thumbnails">
                                                                            <div class="file-initial-thumbs">
                                                                                <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                                                                    <div class="kv-file-content">
                                                                                        <a class="fancybox" href="${floorImg}">
                                                                                            <img src="${floorImg}" class="kv-preview-data file-preview-image"  style="width:auto;height:160px;">
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="file-thumbnail-footer">
                                                                                        <p>Alt Text : ${floorSeoImg}</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="file-preview-status text-center text-success"></div>
                                                                        <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>`;
                                              floor += '<div class="form-group">\
                                                            <label class="control-label col-md-2">คำบรรยาย Floor Plan ชั้น 3</label>\
                                                            <div class="col-md-6">\
                                                             <textarea class="form-control" \
                                                              rows="7" disabled>'
                                                                  +json_obj.gallery_floor[i].floor_plan_dis_th+
                                                             '</textarea>\
                                                          </div>\
                                                        </div>';
                                      }
                                } else {
                                      floor += `<div class="form-group">
                                                    <label class="control-label col-md-2">ภาพ Floor Plan ชั้น ${i+1}</label>
                                                    <div class="col-md-6">
                                                        <div class="file-preview">
                                                            <div class="close fileinput-remove"></div>
                                                            <div class="file-drop-disabled">
                                                                <div class="file-preview-thumbnails">
                                                                    <div class="file-initial-thumbs">
                                                                        <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                                                            <div class="kv-file-content">
                                                                                <a class="fancybox" href="${floorImg}">
                                                                                    <img src="${floorImg}" class="kv-preview-data file-preview-image"  style="width:auto;height:160px;">
                                                                                </a>
                                                                            </div>
                                                                            <div class="file-thumbnail-footer">
                                                                                <p>Alt Text : ${floorSeoImg}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <div class="file-preview-status text-center text-success"></div>
                                                                <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>`;
                                      floor += `<div class="form-group">
                                                    <label class="control-label col-md-2">คำบรรยาย Floor Plan ${i+1}</label>
                                                    <div class="col-md-6">
                                                     <textarea class="form-control"
                                                      rows="7" disabled>${json_obj.gallery_floor[i].floor_plan_dis_th}</textarea>
                                                  </div>
                                                </div>`;
                                }
                        }
                        $('#floors').append(floor);
                    }else{

                    }
                  })
                  .fail(function() {

                        //Clear Elemant <Option>
                        $('#zone').empty();

                        var option = "<option value=''>เลือก Zone</option>";
                        $('#zone').append(option);
                      console.error('Backend error');
                  })
                  .always(function() {
                    console.info( "complete" );
                  });
        }else {

            //Clear Elemant <Option>
            $('#zone').empty();
        }
      }

        var url_icon = "ajax_geticon.php";
        function changeIcon(id){
          if(id != 0) {

              var link = url_icon+'?icon_id='+id;
              $.ajax( link )
                    .done(function(response) {
                      if (response != 0) {

                          if(response != ""){
                              if(typeof response === "object"){
                                  var json_obj = response;
                              }else{
                                  var json_obj =  $.parseJSON(response);//parse JSON
                              }
                          }

                          //Clear Elemant
                          $('#icon_img').empty();

                          var icon = '';
                          for (var i in json_obj.icon) {
                                    var iconImg = 'fileupload/images/icon_img/'+json_obj.icon[i].icon_img;
                                    var iconImgSeo = json_obj.icon[i].icon_img;
                                    icon = `<div class="form-group">
                                                <label class="control-label col-md-2">ภาพ ICON ซีรี่ส์แบบบ้าน </label>
                                                <div class="col-md-6">
                                                      <div class="file-preview ">
                                                          <div class="close fileinput-remove"></div>
                                                          <div class="file-drop-disabled">
                                                              <div class="file-preview-thumbnails">
                                                                  <div class="file-initial-thumbs">
                                                                      <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                                                          <div class="kv-file-content">
                                                                              <a class="fancybox" href="${iconImg}">
                                                                                  <img src="${iconImg}" class="kv-preview-data file-preview-image"  style="width:auto;height:160px;">
                                                                              </a>
                                                                          </div>
                                                                          <div class="file-thumbnail-footer">
                                                                              <p>Alt Text :
                                                                              </p>
                                                                          </div>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                              <div class="clearfix"></div>
                                                              <div class="file-preview-status text-center text-success"></div>
                                                              <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                                          </div>
                                                      </div><!-- ./file-preview -->
                                                </div>
                                            </div>`;
                          }
                          $('#icon_img').append(icon);
                      }else{

                      }
                    })
                    .fail(function() {
                        console.error('Backend error');
                    })
                    .always(function() {
                      console.info( "complete" );
                    });
          }else {

          }
        }

    function getStoreImageHomeSales(id){
        if (id == '') {
            //Clear Elemant
            $('#images_in_homesales').empty();
            return;
        }
        var url_img = "ajax_getImageFromProjectForByIdProject.php";
        var link = url_img+'?galery_project_id='+id;
          $.ajax( link )
        .done(function(response) {
          console.log(response)
          if(response != ""){
            if(typeof response === "object"){
              var json_obj = response;
            }else{
              var json_obj =  $.parseJSON(response);//parse JSON
            }

            //Clear Elemant
            $('#images_in_homesales').empty();
            var master = '';
                master += `<div class="file-preview ">
                                <div class="close fileinput-remove"></div>
                                <div class="file-drop-disabled">`;
            for (var i in json_obj) {

                master += `<div class="file-preview-thumbnails">
                              <div class="file-initial-thumbs">
                                  <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                      <div class="kv-file-content">
                                          <a class="fancybox" href="${json_obj[i].galery_project_img_name.trim()}">
                                              <img src="${json_obj[i].galery_project_img_name.trim()}" class="kv-preview-data file-preview-image"  style="width:auto;height:160px;">
                                          </a>
                                      </div>
                                      <div class="file-thumbnail-footer">
                                          <p>Alt Text : ${json_obj[i].galery_project_img_seo}
                                          </p>
                                          <div class="file-actions">
                                              <div class="file-footer-buttons">
                                                  <input type="checkbox" name="idImageSelected[]" value="${json_obj[i].galery_project_img_id.trim()}" class="flat" >
                                              </div>
                                              <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>`;

            }
              master += ` <div class="clearfix"></div>
                          <div class="file-preview-status text-center text-success"></div>
                          <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                        </div></div>`;

            $('#images_in_homesales').append(master);
            fancybox_init();
          }else{

          }
        })
        .fail(function() {
              //Clear Elemant <Option>
            $('#images_in_homesales').empty();
            console.error('Backend error');
        })
        .always(function() {
          console.info( "complete" );
        });
    }

    function checkRadioImage(value){
      if(value == 'option1') {
        $("#option2").prop('required',false);
        $("#option1").prop('required',true);
        $("#option1").prop('disabled',false);
        $("#option2").fileinput('disable');
        if(!tempClickRadioDuplicate) {
          $('#images_in_homesales').empty();
          tempClickRadioDuplicate = 1;
        }
      }else{
        tempClickRadioDuplicate = 0;
        $("#option2").fileinput('enable');
        $("#option1").prop('required',false);
        $("#option2").attr('required', true);
        //$("#option2").prop('required',true);
        $("#option1").prop('disabled',true);

        $('#option1').prop('selectedIndex',0);
        $('#images_in_homesales').empty();
      }
    }

    function validateBeforSubmit(){
      $("#alertRadio").empty();
      if($('input[name=optionsImg]:checked').length<=0) {
          $("#alertRadio").append(" โปรดติ๊กระบุภาพภายในบ้านพร้อมขาย");
          $('html, body').animate({
            scrollTop: $("#alertRadio").offset().top
        }, 500);
          $("#option2").focus();
      }else{

          if ($('input[name=optionsImg]:checked').val() == 'option1') {

                if($('[name="idImageSelected\\[\\]"]:checked').length<=0) {
                    $("#alertRadio").append(" โปรดติ๊กเพื่อเลือกภาพที่ต้องการ");
                    $('html, body').animate({
                      scrollTop: $("#alertRadio").offset().top
                    }, 500);
                    $("#option2").focus();
                } else {
                    $("#realSubmit").click();
                }
          } else {
              $("#realSubmit").click();
          }
      }
    }

    function fancybox_init(){
        $("a[href*='.jpg'], a[href*='.png']").attr('rel', 'fancybox').fancybox({
            maxWidth  : 800,
            maxHeight : 600,
            fitToView : true,
            width     : '70%',
            height    : '70%',
            autoSize  : true,
            closeClick    : false,
            openEffect    : 'none',
            closeEffect   : 'none'
        });
    }
    </script>
    <!-- /Datatables -->
        <!-- jQuery Tags Input -->
    <script>
      function onAddTag(tag) {
        alert("Added a tag: " + tag);
      }

      function onRemoveTag(tag) {
        alert("Removed a tag: " + tag);
      }

      function onChangeTag(input, tag) {
        alert("Changed a tag: " + tag);
      }

      $(document).ready(function() {
        $('#tags_1').tagsInput({
          width: 'auto'
        });
        $('#tags_2').tagsInput({
          width: 'auto'
        });
      });
    </script>
    <!-- /jQuery Tags Input -->

  </body>
</html>