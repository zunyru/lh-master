<?php
/**
 * @author jacky
 * @createdate May 11 2017
 **/
if(!isset($_SESSION))
{
	session_start();
}
include './include/dbCon_mssql.php';
require_once 'include/function_query.php';
date_default_timezone_set('Asia/Bangkok');
$date = date("Y-m-d");
$action =isset ( $_GET ['action']) ? $_GET ['action'] : '';
if($action=='delBannerLead'){
	$bannerLeadId=isset( $_GET ['bannerLeadId']) ? $_GET ['bannerLeadId'] : '';
	$imgPath=isset( $_GET ['imgPath']) ? $_GET ['imgPath'] : '';
	if(file_exists("$imgPath") && unlink("$imgPath")){
		$sql=" delete from LH_BANNER_LEAD where banner_lead_id=".$bannerLeadId;
		$result=exeQuery($sql);
		header('Content-type: application/json');
		echo json_encode($result);
	}
}
if($action=='delPicActivity'){
	$bannerMainId=isset( $_GET ['bannerMainId']) ? $_GET ['bannerMainId'] : '';
	$imgPath=isset( $_GET ['imgPath']) ? $_GET ['imgPath'] : '';
	if(file_exists("$imgPath") && unlink("$imgPath")){
		$sql=" update LH_BANNER_MAIN set pic_activity='' where banner_main_id=".$bannerMainId;
		$result=exeQuery($sql);
		header('Content-type: application/json');
		echo json_encode($result);
	}
}
if($action=='delPicVDO'){
	$bannerMainId=isset( $_GET ['bannerMainId']) ? $_GET ['bannerMainId'] : '';
	$imgPath=isset( $_GET ['imgPath']) ? $_GET ['imgPath'] : '';
	if(file_exists("$imgPath") && unlink("$imgPath")){
		$sql=" update LH_BANNER_MAIN set thumnail_path='' where banner_main_id=".$bannerMainId;
		$result=exeQuery($sql);
		header('Content-type: application/json');
		echo json_encode($result);
	}
}
if($action=='delVDOFile'){
	$bannerMainId=isset( $_GET ['bannerMainId']) ? $_GET ['bannerMainId'] : '';
	$imgPath=isset( $_GET ['imgPath']) ? $_GET ['imgPath'] : '';
	if(file_exists("$imgPath") && unlink("$imgPath")){
		$sql=" update LH_BANNER_MAIN set url_vdo='' where banner_main_id=".$bannerMainId;
		$result=exeQuery($sql);
		header('Content-type: application/json');
		echo json_encode($result);
	}
}
if($action=='delPicYoutube'){
	$bannerMainId=isset( $_GET ['bannerMainId']) ? $_GET ['bannerMainId'] : '';
	$imgPath=isset( $_GET ['imgPath']) ? $_GET ['imgPath'] : '';
	if(file_exists("$imgPath") && unlink("$imgPath")){
		$sql=" update LH_BANNER_MAIN set thumnail_path='' where banner_main_id=".$bannerMainId;
		$result=exeQuery($sql);
		header('Content-type: application/json');
		echo json_encode($result);
	}
}

?>








