<?php
require_once 'include/dbConnect.php';

	try {
		$community_id = $_GET['community_id'];

		$conn = (new dbConnect())->getConn();
		$sql = "UPDATE LH_COMMUNITY_PAGE SET community_img = ''
				WHERE community_id =".$community_id;
        $result= $conn->query($sql);

		echo json_encode($result->fetchAll());

	} catch (\Exception $e) {
		return $e->getMessage();
	}
?>