<?php
/**
 * @author jacky
 * @createdate May 10 2017
 **/
?>
<?php
require_once 'dbConnect.php';
require_once 'dbCon_mssql.php'; // for insert utf8
require_once 'BannerHelper.php';

function mssql_escape($str)
{
	if (get_magic_quotes_gpc()) {
		$str = stripslashes($str);
	}
	return str_replace("'", "''", $str);
}

function getBannerListAll() {
	$sql = " select banner_main_id,type_page_id,type_show_id,url_lead ";
	$sql .= " ,lead_img_mobile ,startdate,enddate,convert(varchar,updatedate, 120) updatedate from LH_BANNER_MAIN bm ";
	$sql .= " where bm.lead_img_mobile not in ('d') ";
	$sql .= " order by updatedate desc ";

	try {
		$result = mssql_query ( $sql, $GLOBALS ['db_conn'] );
		return BannerHelper::toArrayList ( $result );
	} catch ( Exception $e ) {
		echo $sql . "<br>" . $e->getMessage ();
	}
}
function insertTransactionQuery($sql) {
	if (! is_null ( $sql ) && ! empty ( $sql )) {
		mssql_query ( "BEGIN TRAN" );
		try {
			$result= mssql_query($sql, $GLOBALS ['db_conn'] );
			if(!$result){
				echo $sql . "<br> can't save data.";
				mssql_query("ROLLBACK");
			}
			mssql_query ( "COMMIT" );
		} catch ( Exception $e ) {
			echo $sql . "<br>" . $e->getMessage ();
			mssql_query ( "ROLLBACK" );
		}
	}
}
function exeQuery($sql){
	if (! is_null ( $sql ) && ! empty ( $sql )) {
		try {
			$result= mssql_query($sql, $GLOBALS ['db_conn'] );
			if(!$result){
				echo $sql . "<br> can't execute query.";
			}
			return $result;
		} catch ( Exception $e ) {
			echo $sql . "<br>" . $e->getMessage ();
		}
	}
}

function getMaxIDLHBannerMain(){
	$sql=" SELECT MAX(banner_main_id) as maxid FROM LH_BANNER_MAIN ";
	try{
		$result=mssql_query($sql, $GLOBALS ['db_conn'] );
		$row = mssql_fetch_array($result);
		$maxid =$row['maxid'];
		return is_null($maxid)?0:$maxid;
	} catch ( Exception $e ) {
			echo $sql . "<br>" . $e->getMessage ();
	}
}

function  getDefaultBannerImageByPageType($pageType){
	$sql=" select m.banner_main_id,l.* ";
	$sql.=" from LH_BANNER_MAIN m left join LH_BANNER_LEAD l on m.banner_main_id = l.banner_main_id ";
	$sql.=" where m.lead_img_mobile='d'and l.lead_img_mobile='d' and m.type_page_id=".$pageType;
	$sql.=" order by l.seq_leade_img asc ";
	try {
		$result = mssql_query ( $sql, $GLOBALS ['db_conn'] );
		return BannerHelper::toArrayList ( $result );
	} catch ( Exception $e ) {
		echo $sql . "<br>" . $e->getMessage ();
	}
}

function  getJsonDataDefaultBannerImageByPageType($pageType){
	$sql ="select * from LH_BANNER_MAIN m where m.lead_img_mobile='d' and m.type_page_id= $pageType";
//	$sql=" select DISTINCT m.banner_main_id,l.* ";
//	$sql.=" from LH_BANNER_MAIN m left join LH_BANNER_LEAD l on m.banner_main_id = l.banner_main_id ";
//	$sql.=" where m.lead_img_mobile='d'and l.lead_img_mobile='d' and m.type_page_id=".$pageType;
//	$sql.=" order by l.seq_leade_img asc ";
	try {
		$result = mssql_query ( $sql, $GLOBALS ['db_conn'] );
		$jsondata=array();
		while ($row = mssql_fetch_object($result)) {
			$data['banner_main_id'] = $row->banner_main_id;
			$data['banner_lead_id'] =$row->banner_lead_id;
			$data['lead_path'] =$row->lead_path;
			$data['filename'] =  substr($row->lead_path,strrpos($row->lead_path, '/') + 1);
			$data['filesize'] = filesize($row->lead_path);
			$data['lead_img_mobile'] = $row->lead_img_mobile;
			$data['seq_leade_img'] =$row->seq_leade_img;
			array_push($jsondata,$data);
		}
		return json_encode($jsondata);
	} catch ( Exception $e ) {
		echo $sql . "<br>" . $e->getMessage ();
	}
}

function  getBannerLeadByMainId($mainId){
	$sql=" select m.banner_main_id,l.* ";
	$sql.=" from LH_BANNER_MAIN m left join LH_BANNER_LEAD l on m.banner_main_id = l.banner_main_id ";
	$sql.=" where m.banner_main_id= ".$mainId ;
	$sql.=" order by l.seq_leade_img asc ";
	try {
		$result = mssql_query ( $sql, $GLOBALS ['db_conn'] );
		return BannerHelper::toArrayList ( $result );
	} catch ( Exception $e ) {
		echo $sql . "<br>" . $e->getMessage ();
	}
}

function  getBannerImageByMainId($mainId,$leadImgMobile){
	$sql=" select m.banner_main_id,l.* ";
	$sql.=" from LH_BANNER_MAIN m left join LH_BANNER_LEAD l on m.banner_main_id = l.banner_main_id ";
	$sql.=" where m.banner_main_id= ".$mainId." and l.lead_img_mobile = '".$leadImgMobile."' ";
	$sql.=" order by l.seq_leade_img asc ";
	try {
		$result = mssql_query ( $sql, $GLOBALS ['db_conn'] );
		return BannerHelper::toArrayList ( $result );
	} catch ( Exception $e ) {
		echo $sql . "<br>" . $e->getMessage ();
	}
}


function  getJsonBannerImageByMainId($mainId,$leadImgMobile){
	$sql=" select DISTINCT m.banner_main_id,l.* ";
	$sql.=" from LH_BANNER_MAIN m left join LH_BANNER_LEAD l on m.banner_main_id = l.banner_main_id ";
	$sql.=" where m.banner_main_id= ".$mainId." and l.lead_img_mobile = '".$leadImgMobile."' ";
	$sql.=" order by l.seq_leade_img asc ";
	try {
		$result = mssql_query ( $sql, $GLOBALS ['db_conn'] );
		$jsondata=array();
		while ($row = mssql_fetch_object($result)) {
			$data['banner_main_id'] = $row->banner_main_id;
			$data['banner_lead_id'] =$row->banner_lead_id;
			$data['lead_path'] =$row->lead_path;
			if(file_exists($row->lead_path)){
			   $data['filename'] =  substr($row->lead_path,strrpos($row->lead_path, '/') + 1);
			   $data['filesize'] =  filesize($row->lead_path);
		    }
			$data['lead_img_mobile'] = $row->lead_img_mobile;
			$data['seq_leade_img'] =$row->seq_leade_img;
			array_push($jsondata,$data);
		}
		return json_encode($jsondata);
	} catch ( Exception $e ) {
		echo $sql . "<br>" . $e->getMessage ();
	}
}


function getBannerMainById($mainid){
	$sql=" select banner_main_id,type_page_id,type_show_id,url_lead ";
	$sql.=" ,pic_activity,color_code_activity,text_activity_th,text_activity_en ";
	$sql.=" ,url_vdo,thumnail_path,lead_img_mobile,startdate,enddate ";
	$sql.=" from LH_BANNER_MAIN where banner_main_id=".$mainid;
	try {
		$result = mssql_query ( $sql, $GLOBALS ['db_conn'] );
		$result = mssql_fetch_object($result);
		return $result;
	} catch ( Exception $e ) {
		echo $sql . "<br>" . $e->getMessage ();
	}
}

function getJsonBannerMainById($mainid){
	$sql=" select banner_main_id,type_page_id,type_show_id,url_lead ";
	$sql.=" ,pic_activity,color_code_activity,text_activity_th,text_activity_en ";
	$sql.=" ,url_vdo,thumnail_path,lead_img_mobile,startdate,enddate ";
	$sql.=" from LH_BANNER_MAIN where banner_main_id=".$mainid;
	try {
		$result = mssql_query ( $sql, $GLOBALS ['db_conn'] );
	    $row = mssql_fetch_object($result);
			$data['banner_main_id'] = $row->banner_main_id;
			$data['type_page_id'] =$row->type_page_id;
			$data['type_show_id'] =$row->type_show_id;
			$data['url_lead'] =  $row->url_lead;
			$data['pic_activity'] = $row->pic_activity;
			if(isset($row->pic_activity) && !empty($row->pic_activity) && file_exists($row->pic_activity)){
				$data['pic_activity_name'] =  substr($row->pic_activity,strrpos($row->pic_activity, '/') + 1); 
				$data['pic_activity_size'] =  filesize($row->pic_activity);
			}
			$data['color_code_activity'] = $row->color_code_activity;
			$data['text_activity_th'] =$row->text_activity_th;
			$data['text_activity_en'] =$row->text_activity_en;
			$data['url_vdo'] =  $row->url_vdo;
			if($row->type_show_id==2 && isset($row->url_vdo) && !empty($row->url_vdo) && file_exists($row->url_vdo)){
				$data['vdo_name'] =  substr($row->url_vdo,strrpos($row->url_vdo, '/') + 1);
				$data['vdo_size'] =  filesize($row->url_vdo);
			}
			$data['thumnail_path'] = $row->thumnail_path;
			if(isset($row->thumnail_path) && !empty($row->thumnail_path) && file_exists($row->thumnail_path)){
				$data['pic_vdo_name'] =  substr($row->thumnail_path,strrpos($row->thumnail_path, '/') + 1);
				$data['pic_vdo_size'] =  filesize($row->thumnail_path);
			}
			$data['lead_img_mobile'] = $row->lead_img_mobile;
			$data['startdate'] =$row->startdate;
			$data['enddate'] =$row->enddate;
			
		
		return json_encode($data);
	} catch ( Exception $e ) {
		echo $sql . "<br>" . $e->getMessage ();
	}
}

function getListPageType(){
	 $sql=" select distinct(type_page_id) pageTypeId ";
	 $sql.=" from LH_BANNER_MAIN order by pageTypeId asc ";
	 try{
	 	$result = mssql_query ( $sql, $GLOBALS ['db_conn'] );
	 	return BannerHelper::toArrayList ( $result );
	 }catch( Exception $e ) {
		echo $sql . "<br>" . $e->getMessage ();
	}
}

function getBannerMainDefaultLeadImage($pageType){
	$sql=" select banner_main_id,type_page_id,type_show_id,url_lead ";
	$sql.=" ,pic_activity,color_code_activity,text_activity_th,text_activity_en ";
	$sql.=" ,url_vdo,thumnail_path,lead_img_mobile,startdate,enddate ";
	$sql.=" from LH_BANNER_MAIN where lead_img_mobile='d' and type_page_id=".$pageType;
	try {
		$result = mssql_query ( $sql, $GLOBALS ['db_conn'] );
		$result = mssql_fetch_object($result);
		return $result;
	} catch ( Exception $e ) {
		echo $sql . "<br>" . $e->getMessage ();
	}
}


function getType_show($banner_main_id){
	$sql="SELECT m.type_show_id FROM LH_BANNER_MAIN m 
LEFT JOIN LH_BANNER_LEAD l ON m.banner_main_id = l.banner_main_id
WHERE   m.banner_main_id = $banner_main_id  AND m.lead_img_mobile != 'd'";
try {
   	  $result = mssql_query ( $sql, $GLOBALS ['db_conn'] );
   	  $result = mssql_fetch_object($result);
	  return $result;
   } catch (Exception $e) {
		echo $sql . "<br>" . $e->getMessage ();
   	  
   }
}

//fucntion ShowBanner_btn
function getShowBanner_btn($banner_main_id){
	$sql ="SELECT l.banner_lead_id , m.type_show_id FROM LH_BANNER_MAIN m 
LEFT JOIN LH_BANNER_LEAD l ON m.banner_main_id = l.banner_main_id
WHERE   m.banner_main_id = '$banner_main_id'  AND m.lead_img_mobile != 'd'";
try {
   	  $result = mssql_query ( $sql, $GLOBALS ['db_conn'] );
   	  $result = mssql_fetch_object($result);
	  return $result;
   } catch (Exception $e) {
		echo $sql . "<br>" . $e->getMessage ();
   	  
   }
}
//--------------------- qc_project view.php -------------------------------//

function getBanner_lead_Project($project_id){
   $sql = "SELECT * FROM LH_LEAD_IMAGE_PROJECT_FILE WHERE PROJECT_ID = '$project_id' 
   AND LEAD_IMAGE_PROJECT_FILE_TYPE != 'image' ";
try {
   	  $result = mssql_query ( $sql, $GLOBALS ['db_conn'] );
   	  $result = mssql_fetch_object($result);
	  return $result;
   } catch (Exception $e) {
		echo $sql . "<br>" . $e->getMessage ();
   	  
   }
}

function getBanner_lead_image($project_id){
	   $sql = "SELECT LEAD_IMAGE_PROJECT_FILE_ID AS id ,LEAD_IMAGE_PROJECT_FILE_NAME AS name,
LEAD_IMAGE_PROJECT_FILE_SEO AS seo , LEAD_IMAGE_PROJECT_FILE_TYPE AS type ,img_url,seo_lead FROM LH_LEAD_IMAGE_PROJECT_FILE WHERE PROJECT_ID = '$project_id' 
	   AND LEAD_IMAGE_PROJECT_FILE_TYPE = 'image_leade_banner' ";
	try {
	   	  $result = mssql_query ( $sql, $GLOBALS ['db_conn'] );
	   	 
	   	  $results = [];
          while ($row = mssql_fetch_object($result)) {
              $results[] = $row;
          }

	   	  return $results;

	   } catch (Exception $e) {
			echo $sql . "<br>" . $e->getMessage ();
	   	  
	   }
}

function getBanner_activity($project_id){
		   $sql = "SELECT LEAD_IMAGE_PROJECT_FILE_ID AS id ,LEAD_IMAGE_PROJECT_FILE_NAME AS name,backgroup_color,
	LEAD_IMAGE_PROJECT_FILE_SEO AS seo , LEAD_IMAGE_PROJECT_FILE_TYPE AS type FROM LH_LEAD_IMAGE_PROJECT_FILE WHERE PROJECT_ID = '$project_id' 
		   AND LEAD_IMAGE_PROJECT_FILE_TYPE = 'banner' ";
   try {
      	  $result = mssql_query ( $sql, $GLOBALS ['db_conn'] );
      	  $result = mssql_fetch_object($result);
   	  return $result;
      } catch (Exception $e) {
   		echo $sql . "<br>" . $e->getMessage ();
      	  
      }
}

function  getBanner_vdo_youtube($project_id){
		 $sql = "SELECT LEAD_IMAGE_PROJECT_FILE_ID AS id ,LEAD_IMAGE_PROJECT_FILE_NAME AS name,banner_img_thum,
		LEAD_IMAGE_PROJECT_FILE_SEO AS seo , LEAD_IMAGE_PROJECT_FILE_TYPE AS type FROM LH_LEAD_IMAGE_PROJECT_FILE WHERE PROJECT_ID = '$project_id' 
			   AND (LEAD_IMAGE_PROJECT_FILE_TYPE = 'vdo' OR LEAD_IMAGE_PROJECT_FILE_TYPE = 'youtube') ";
	   try {
	      	  $result = mssql_query ( $sql, $GLOBALS ['db_conn'] );
	      	  $result = mssql_fetch_object($result);
	   	  return $result;
	      } catch (Exception $e) {
	   		echo $sql . "<br>" . $e->getMessage ();
	      	  
	      }
}

function getBanner_vdo($project_id){
	 $sql = "SELECT LEAD_IMAGE_PROJECT_FILE_ID AS id ,LEAD_IMAGE_PROJECT_FILE_NAME AS name,banner_img_thum,
	LEAD_IMAGE_PROJECT_FILE_SEO AS seo , LEAD_IMAGE_PROJECT_FILE_TYPE AS type FROM LH_LEAD_IMAGE_PROJECT_FILE WHERE PROJECT_ID = '$project_id' 
		   AND LEAD_IMAGE_PROJECT_FILE_TYPE = 'vdo'  ";
   try {
      	  $result = mssql_query ( $sql, $GLOBALS ['db_conn'] );
      	  $result = mssql_fetch_object($result);
   	  return $result;
      } catch (Exception $e) {
   		echo $sql . "<br>" . $e->getMessage ();
      	  
      }
}

function getBanner_youtube($project_id){
	 $sql = "SELECT LEAD_IMAGE_PROJECT_FILE_ID AS id ,LEAD_IMAGE_PROJECT_FILE_NAME AS name,banner_img_thum,
	LEAD_IMAGE_PROJECT_FILE_SEO AS seo , LEAD_IMAGE_PROJECT_FILE_TYPE AS type FROM LH_LEAD_IMAGE_PROJECT_FILE WHERE PROJECT_ID = '$project_id' 
		   AND  LEAD_IMAGE_PROJECT_FILE_TYPE = 'youtube'";
   try {
      	  $result = mssql_query ( $sql, $GLOBALS ['db_conn'] );
      	  $result = mssql_fetch_object($result);
   	  return $result;
      } catch (Exception $e) {
   		echo $sql . "<br>" . $e->getMessage ();
      	  
      }
}
?>











