<?php
/**
 * @author jacky
 * @createdate May 10 2017
 **/
?>
<?php
class BannerHelper {
	public static function toArrayList($result)
	{
		$results = [];
		while ($row = mssql_fetch_object($result)) {
			$results[] = $row;
		}
	
		return $results;
	}
	public static function isLeadMobile($typeMobile) {
		$typeshow = "PC";
		//die(var_dump($typeshowId));
		if (is_null ( $typeMobile ) )
			return $typeshow;
			else
				switch ($typeMobile) {
					case 'm' :
						$typeshow = "Mobile";
						break;
					case 'p' :
						$typeshow = "PC";
						break;
					case 'h' :
						$typeshow = "PC & Mobile";
						break;
					default :
						$typeshow = "";
						break;
			}
			return $typeshow;
	}
	public static function getTypeShowById($typeshowId) {
		$typeshow = "";
		//die(var_dump($typeshowId));
		if (is_null ( $typeshowId ) )
			return $typeshow;
		else
			switch ($typeshowId) {
				case 0 :
					$typeshow = "Lead Image";
					break;
				case 1 :
					$typeshow = "Banner Activity";
					break;
				case 2 :
					$typeshow = "ClipVDO";
					break;
				case 3 :
					$typeshow = "Clip Youtube";
					break;
				default :
					$typeshow = "";
					break;
			}
		return $typeshow;
	}
	public static function getPageTypeById($pageTypeId) {
		$pageType = "";
		if (is_null ( $pageTypeId ) || empty ( $pageTypeId ))
			return $pageType;
		else
			switch ($pageTypeId) {
				case 1 :
					$pageType = "หน้า Home";
					break;
				case 2 :
					$pageType = "บ้านเดี่ยว";
					break;
				case 3 :
					$pageType = "บ้านตกแต่งพร้อมขาย";
					break;
				case 4 :
					$pageType = "ข่าวโครงการใหม่";
					break;
				case 5 :
					$pageType = "ทาว์โฮม";
					break;
				case 6 :
					$pageType = "Ladawan";
					break;
				case 7 :
					$pageType = "โครงการในต่างจังหวัด";
					break;
				case 8 :
					$pageType = "คอนโดมิเนียม";
					break;
				case 9 :
					$pageType = "แบบบ้าน";
					break;
				default :
					$pageType = "";
					break;
			}
		return $pageType;
	}
	
	public static function getSelectListPageTypeAdd(){
		$optiontxt="";
		$listpagetype=getListPageType();
		$arrOptions=array("<option value='1'>หน้า Home</option>",
				          "<option value='2'>บ้านเดี่ยว</option>", 
				          "<option value='3'>บ้านตกแต่งพร้อมขาย</option>",
				          "<option value='4'>ข่าวโครงการใหม่</option>",
				          "<option value='5'>ทาว์โฮม</option>",
				          "<option value='6'>Ladawan</option>",
				          "<option value='7'>โครงการในต่างจังหวัด</option>",
				          "<option value='8'>คอนโดมิเนียม</option>",
				          "<option value='9'>แบบบ้าน</option>"
		);
		if(isset($listpagetype)){
		        foreach($listpagetype as $data){
		        	unset($arrOptions[$data->pageTypeId -1]);
		        }
		 }
		 foreach($arrOptions as $val) {
		 	$optiontxt.=$val;
		 }
		return $optiontxt;
	}
	
	public static function getSelectListPageTypeEdit($curPageType){
		$optiontxt="";
		$listpagetype=getListPageType();
		$arrOptions=array("<option value='1' >หน้า Home</option>",
				"<option value='2'>บ้านเดี่ยว</option>",
				"<option value='3'>บ้านตกแต่งพร้อมขาย</option>",
				"<option value='4'>ข่าวโครงการใหม่</option>",
				"<option value='5'>ทาว์โฮม</option>",
				"<option value='6'>Ladawan</option>",
				"<option value='7'>โครงการในต่างจังหวัด</option>",
				"<option value='8'>คอนโดมิเนียม</option>",
				"<option value='9'>แบบบ้าน</option>"
		);
		if(isset($listpagetype)){
			foreach($listpagetype as $data){
			   if($curPageType<>$data->pageTypeId){
				  unset($arrOptions[$data->pageTypeId -1]);
			   }
			}
		}
		foreach($arrOptions as $val) {
			$optiontxt.=$val;
		}
		return $optiontxt;
	}
	
	public static function genFacebookImg($orgFile,$picName){
      if(isset($orgFile)){
		   $width=600; //*** Fix Width & Heigh (Autu caculate) ***//
		   $size=GetimageSize($orgFile);
		   $height=round($width*$size[1]/$size[0]);
		   $images_orig = ImageCreateFromJPEG($orgFile);
		   $photoX = ImagesX($images_orig);
		   $photoY = ImagesY($images_orig);
		   $images_fin = ImageCreateTrueColor($width, $height);
		   ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
		   ImageJPEG($images_fin,$picName,93);
		   ImageDestroy($images_orig);
		   ImageDestroy($images_fin);
      }
    }
}

?>