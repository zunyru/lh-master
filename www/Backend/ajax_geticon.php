<?php
session_start();

require_once 'include/dbConnect.php';

	try {
		$icon_id = $_GET['icon_id'];

		$conn = (new dbConnect())->getConn();
		$sql = "SELECT * FROM LH_ICON_ACTIVITY
				WHERE icon_id =".$icon_id;
        $result['icon'] = $conn->query($sql);
        $result['icon'] = $result['icon']->fetchAll();

		echo json_encode($result);

	} catch (\Exception $e) {
		return $e->getMessage();
	}
?>