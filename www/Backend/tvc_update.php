<?php
session_start();
include './connect_db.php';
include './include/dbCon_mssql.php';
$group_id=$_SESSION['group_id'];

$_GET['page']='tvc';

if (isset($_SESSION['add'])) {
 if($_SESSION['add'] ==1){
             //header("location:product_add.php");
  $success ="success";
  unset($_SESSION["add"]);
} else if($_SESSION['add'] ==2){

            $error_name="error_name"; //ค่าซ้ำ
            $_SESSION['add']='';

          } else if($_SESSION['add'] ==-1){
            $error_up="error_up";
            unset($_SESSION["add"]);
          }else if($_SESSION['add']== -2){
            $error_youtube="error_youtube";
            $_SESSION['add']='';
          }
        }

        function mssql_escape($str)
        {
          if(get_magic_quotes_gpc())
          {
            $str= stripslashes($str);
          }
          return str_replace("'", "''", $str);
        }

        ?>
        <!DOCTYPE html>
        <html lang="en">
        <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <!-- Meta, title, CSS, favicons, etc. -->
          <meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1">

          <title>LAND & HOUSES</title>

          <!-- Bootstrap -->
          <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
          <!-- Font Awesome -->
          <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
          <!-- NProgress -->
          <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
          <!-- iCheck -->
          <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
          <!-- Custom Theme Style -->
          <link href="../build/css/custom.min.css" rel="stylesheet">
          <!-- Fancybox image popup -->
          <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
          <!-- bootstrap-progressbar -->
          <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
          <!-- jQuery -->
          <script src="../vendors/jquery/dist/jquery.min.js"></script>

          <!-- uploade img -->
          <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
          <!--uploade-->
          <!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
          <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
          <!-- confirm-->
          <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
          <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>


          <style type="text/css">
          th.next.available{
            background-color: #6f9755;
          }
          th.prev.available{
            background-color: #6f9755;
          }
          #play{
            text-align: center;
            height:100px;
          }
          .video-thumbnail {
            position: relative;
          }
          .video-thumbnail .i-view-vdo {
            background: url(../Backend/images/i_play_w.png) no-repeat;
            background-size: 76px;
            position: absolute;
            height: 76px;
            width: 76px;
            margin: -38px 0 0 -38px;
            left: 50%;
            top: 50%;
            z-index: 5;
          }
          .video-thumbnail:hover ..i-view-vdo {
            opacity: 1;
          }

          .file-caption-main .btn-file {
            overflow: visible;
          }

          .file-caption-main .btn-file .error {
            position: absolute;
            bottom: -32px;
            right: 30px;
            }.btn-hidden{
              background: #fff;
            }
            .test{
              overflow: hidden;
              height: 0;
              }#seo_lead_img_file{
                display: none;
              }
              .form-control[readonly] { /* For Firefox */
               background-color: white;
             }

             .form-control[readonly] {
               background-color: white;
             }
           </style>

         </head>

         <body class="nav-md">
          <div class="container body">
            <div class="main_container">
              <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                  <div class="navbar nav_title" style="border: 0;">
                    <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
                  </div>

                  <div class="clearfix"></div>

                  <!-- menu profile quick info -->
                  <!-- sidebar menu -->
                  <?php include './master/navbar.php';?>
                  <!-- /menu footer buttons -->
                </div>
              </div>

              <!-- top navigation -->
              <?php include './master/top_nav.php'; ?>
              <!-- /top navigation -->
              <?php
              $sql="SELECT * FROM LH_TVC WHERE tvc_id = '".$_GET['id']."'";
              $query= mssql_query($sql);
              $row = mssql_fetch_array($query);


              ?>
              <!-- page content -->
              <div class="right_col" role="main">
                <div class="">
                  <div class="clearfix"></div>
                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="x_panel">
                        <div class="x_title">
                          <h2><a href="tvc.php">ข้อมูล TVC</a> > <a href="tvc_add.php">แก้ไขข้อมูล TVC : <?=$row['tvc_name']?></a></h2>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                          <p class="font-gray-dark">
                          </p><br>
                          <?php if(isset($success)){?>
                          <div class="row">
                            <div class="col-md-2"></div>
                            <div class="form-group">
                              <div class="alert alert-success col-md-6" id="alert">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <strong>แก้ไขข้อมูลเรียบร้อยแล้ว</strong>
                              </div>
                            </div>
                            <div class="col-md-6"></div>
                          </div>
                          <?php }else if(isset($error_name)){?>
                          <div class="row">
                            <div class="col-md-2"></div>
                            <div class="form-group">
                              <div class="alert alert-danger col-md-6" id="alert">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <strong>มีชื่อนี้อยู่ในระบบแล้ว</strong>
                              </div>
                            </div>
                            <div class="col-md-4"></div>
                          </div>
                          <?php }else if(isset($error_up)){?>

                          <div class="row">
                            <div class="col-md-2"></div>
                            <div class="form-group">
                              <div class="alert alert-danger col-md-6" id="alert">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <strong>เกิดข้อผิดพลาด !</strong> ไม่สามารถบันทึกข้อมูลได้
                              </div>
                            </div>
                            <div class="col-md-4"></div>
                          </div>
                          <?php }else if(isset($error_youtube)){?>

                          <div class="row">
                            <div class="col-md-2"></div>
                            <div class="form-group">
                              <div class="alert alert-danger col-md-6" id="alert">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <strong>เกิดข้อผิดพลาด !</strong> ใส่ข้อมูล VDO Youtube ไม่ครบ
                              </div>
                            </div>
                            <div class="col-md-4"></div>
                          </div>
                          <?php }?>
                          <form class="form-horizontal form-label-left" action="update_tvc.php" method="post" id="commentForm" enctype="multipart/form-data">
                            <div class="form-group">
                              <label class="control-label col-md-3" for="first-name">ชื่อ TVC <span class="required">*</span>
                              </label>

                              <div class="col-md-6">
                                <input type="text" name="tvc_name" class="form-control col-md-7 col-xs-12" required="required" placeholder="กรอกชื่อนิตยสาร"  value='<?php echo mssql_escape($row['tvc_name']);?>'>
                              </div>
                            </div>
                            <div class="test">
                              <div class="form-group">
                                <label class="control-label col-md-11" for="first-name">&nbsp; <span class="required"></span>
                                </label>
                                <div class="col-md-1">
                                  <input id="logo_series" name="logo_series" type="file" multiple class="file-loading" accept="image/*" >
                                </div>
                              </div>

                              <script>
                                $("#logo_series").fileinput({
                             // uploadUrl: "upload.php", // server upload action
                             maxFileCount: 1,
                             allowedFileExtensions: ["png"],
                             browseLabel: '&nbsp;',
                             removeLabel: 'ลบ',
                             browseClass: 'btn btn-hidden',
                             showUpload: false,
                             showRemove:false,
                             showCaption: false,
                             msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                             msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                             msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                             msgZoomModalHeading: 'ตัวอย่างละเอียด',
                             xxx:'seo_logo_series',
                             dropZoneTitle : 'รูปโลโก้สไตล์บ้าน',
                              minImageWidth: 150, //ขนาด กว้าง ต่ำสุด
                              minImageHeight: 150, //ขนาด ศุง ต่ำสุด
                              maxImageWidth: 150, //ขนาด กว้าง ต่ำสุด
                              maxImageHeight: 150, //ขนาด ศุง ต่ำสุด
                              maxFileSize :300 ,
                              msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                              msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                              msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                              msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                              msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                            });
                          </script>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3" name="tvc_detail" for="first-name">รายละเอียด <span class="required">*</span>
                          </label>
                          <div class="col-md-6">
                           <div class="control-group">
                             <textarea  required class="form-control" name="tvc_detail" rows="7" placeholder="รายละเอียด"><?=$row['tvc_detail']?></textarea>
                           </div>
                         </div>
                       </div>
                       <?php
                       $checked = '';
                       $checked_2 = '';
                       if($row['tvc_type']=="youtube"){
                        $checked = 'checked';
                        $type = 'show();';
                        $type2 = 'hide();';
                      }else{
                        $checked_2 = 'checked';
                        $type = 'hide();';
                        $type2 = 'show();';
                      }
                      ?>

                      <div class="form-group" id="inline_content">
                        <label class="control-label col-md-3" name="tvc_detail" for="first-name">เลือกชนิดการอัปไฟล์ <span class="required"></span>
                        </label>
                        <div class="col-md-6">
                          <div class="radio">
                            <label><input type="radio" id="youtube" name="optradio" <?=$checked;?> value="youtube" class="flat"> VDO Youtube</label>
                          </div>
                          <div class="radio">
                            <label><input type="radio" id="file" name="optradio" <?=$checked_2;?> value="vdo" class="flat"> VDO File</label>
                          </div>
                        </div>
                      </div>

                      <div id="l1">

                        <?php if($row['tvc_type']!='youtube'){ ?>
                        <div class="form-group">
                          <label class="control-label col-md-3" for="first-name">URL Youtube  <span class="required"> *</span>
                          </label>
                          <div class="col-md-6">
                            <div class="control-group">
                              <input type="text" name="url_youtube" id="url_youtube"  class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube" value="">
                            </div>
                          </div>
                        </div>
                        <?php } ?>
                        <?php

                        $req = "*";
                        $required = "required";
                        ?>
                        <?php if($row['tvc_type']=='youtube'){
                          if(isset($row['tvc_youtube_link'])&&($row['tvc_youtube_link'] != "")) {
                            $req = "";
                            $required = "";?>

                            <div class="form-group">
                              <label class="control-label col-md-3" for="first-name">URL Youtube  <?php echo $req ?><span class="required"></span>
                              </label>
                              <div class="col-md-6">
                                <div class="control-group">
                                  <input type="text" name="url_youtube" id="url_youtube"  <?php echo $required; ?> class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube" value="<?=$row['tvc_youtube_link']?>">
                                </div>
                              </div>
                            </div>

                            <?php }
                          }?>
                          <?php

                          $req = "*";
                          $required = "required";
                          ?>
                          <?php

                          if($row['tvc_type']=='youtube') {
                            if(isset($row['tvc_thumbnail'])&&($row['tvc_thumbnail'] != "")) {
                              $req = "";
                              $required = "";?>

                              <div class="form-group">
                                <label class="control-label col-md-3" for="first-name">Thumbnail Youtube <br>(.jpg , .png , .gif <br> ขนาด 1920 x 1080 px) <br> ภาพขนาดไม่เกิน 300 KB<span class="required"></span>
                                </label>
                                <div class="col-md-6">
                                  <div id="map-image-holder-brochure"></div>
                                  <div class="file-preview ">
                                    <div class="close fileinput-remove"></div>
                                    <div class="file-drop-disabled">
                                      <div class="file-preview-thumbnails">
                                        <div class="file-initial-thumbs">
                                          <div class="file-preview-frame file-preview-initial" id="logo"
                                          data-fileindex="init_0" data-template="image">
                                          <div class="kv-file-content">
                                            <a class="fancybox"
                                            href="<?= $row['tvc_thumbnail'] ?>">
                                            <img src="<?= $row['tvc_thumbnail'] ?>"
                                            class="kv-preview-data file-preview-image"
                                            style="width:auto;height:160px;">
                                          </a>
                                        </div>
                                        <div class="file-thumbnail-footer">
                                          <p>Alt text : <?=$row['tvc_thumbnail_youtube_seo']?></p>
                                          <div class="file-actions">
                                            <div class="file-footer-buttons">
                                              <button type="button" id="<?php echo $row['tvc_id']; ?>" onclick="deleteThumbnailTVC(<?php echo $row['tvc_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                              <a class="fancybox" href="<?=$row['tvc_thumbnail']?>">
                                                <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                              </a>
                                            </div>
                                            <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                          </div>
                                        </div>

                                      </div>
                                    </div>
                                  </div>
                                  <div class="clearfix"></div>
                                  <div class="file-preview-status text-center text-success"></div>
                                  <div class="kv-fileinput-error file-error-message"
                                  style="display: none;"></div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <?php }
                        }?>

                        <div class="form-group">

                          <label class="control-label col-md-3" for="first-name">เลือก Thumbnail Youtube <?php echo $req ?> <br>(.jpg , .png , .gif <br> ขนาด 1920 x 1080 px) <br> ภาพขนาดไม่เกิน 300 KB</label><span class="required"></span>

                          <div class="col-md-6">
                            <input type="file" id="img_youtube" name="img_youtube" <?php echo $required; ?> class="file-loading" accept="image/*">

                          </div>
                        </div>
                      </div>
                      <script>
                        $("#img_youtube").fileinput({
                              uploadUrl: "upload.php", // server upload action
                              maxFileCount: 1,
                              allowedFileExtensions: ["jpg", "png", "jpeg","gif"],
                              browseLabel: 'เลือกรูป',
                              removeLabel: 'ลบ',
                              browseClass: 'btn btn-success',
                              showUpload: false,
                              showRemove:false,
                              showCaption: false,
                              msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                              msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                              msgZoomModalHeading: 'ตัวอย่างละเอียด',
                              xxx:'seo_img_youtube',
                              dropZoneTitle : 'รูปภาพ Thumbnail Youtube',
                              minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                              minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                              maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                              maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                              maxFileSize :300 ,
                              msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                              msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                              msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                              msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                              msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                            });
                          </script>


                          <div id="l2">
                            <?php

                            $req = "*";
                            $required = "required";
                            ?>
                            <?php
                            if($row['tvc_type']=='vdo') {
                              if (isset($row['tvc_thumbnail'])&&($row['tvc_thumbnail'] != "")) {
                                $req = "";
                                $required = "";?>



                                <div class="form-group">
                                  <label class="control-label col-md-3" for="first-name">Thumbnail VDO<span class="required"> </span>
                                  </label>
                                  <div class="col-md-6">
                                    <div id="map-image-holder-brochure"></div>

                                    <div class="file-preview ">
                                      <div class="close fileinput-remove"></div>
                                      <div class="file-drop-disabled">
                                        <div class="file-preview-thumbnails">
                                          <div class="file-initial-thumbs">
                                            <div class="file-preview-frame file-preview-initial" id="logo"
                                            data-fileindex="init_0" data-template="image">
                                            <div class="kv-file-content">
                                              <a class="fancybox"
                                              href="<?= $row['tvc_thumbnail'] ?>">
                                              <img src="<?= $row['tvc_thumbnail'] ?>"
                                              class="kv-preview-data file-preview-image"
                                              style="width:auto;height:160px;"
                                              >
                                            </a>
                                          </div>
                                          <div class="file-thumbnail-footer">
                                            <p>Alt text : <?=$row['tvc_thumbnail_vdo_seo']?></p>
                                            <div class="file-actions">
                                              <div class="file-footer-buttons">
                                                <button type="button" id="<?php echo $row['tvc_id']; ?>" onclick="deleteThumbnailTVC(<?php echo $row['tvc_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                <a class="fancybox" href="<?=$row['tvc_thumbnail']?>">
                                                  <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                </a>
                                              </div>
                                              <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                            </div>
                                          </div>

                                        </div>
                                      </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="file-preview-status text-center text-success"></div>
                                    <div class="kv-fileinput-error file-error-message"
                                    style="display: none;"></div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <?php }
                          }?>
                          <div class="form-group">

                            <label class="control-label col-md-3" for="first-name">เลือก Thumbnail VDO <br>(.jpg , .png , .gif  <br> ภาพขนาดไม่เกิน 300 KB<?php echo $req ?>  </label><span class="required"></span>

                            <div class="col-md-6">
                              <input type="file" id="thumbnail_vdo" name="thumbnail_vdo" class="form-control col-md-7 col-xs-12"  <?php echo $required; ?> class="file-loading" accept="image/*">

                            </div>
                          </div>


                          <script>
                            $("#thumbnail_vdo").fileinput({
                                  uploadUrl: "upload.php", // server upload action
                                  maxFileCount: 1,
                                  allowedFileExtensions: ["jpg", "png", "jpeg"],
                                  browseLabel: 'เลือกรูป',
                                  removeLabel: 'ลบ',
                                  browseClass: 'btn btn-success',
                                  showUpload: false,
                                  showRemove:false,
                                  showCaption: false,
                                  msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                  msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                  msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                  xxx:'seo_thumbnail_vdo',
                                  dropZoneTitle : 'รูปภาพ Thumbnail VDO',
                                  minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                  minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                  maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                  maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                  maxFileSize :300 ,
                                  msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                  msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                  msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                  msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                  msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                });
                              </script>
                              <?php

                              $req = "*";
                              $required = "required";
                              ?>
                              <?php
                              if($row['tvc_type']=='vdo') {
                                if (isset($row['tvc_youtube_link'])&&($row['tvc_youtube_link'] != "")) {
                                 $req = "";
                                 $required = "";?>



                                 <div class="form-group">
                                  <label class="control-label col-md-3" for="first-name"> VDO<span class="required"> </span>
                                  </label>
                                  <div class="col-md-6">
                                    <div id="map-image-holder-brochure"></div>
                                    <div class="file-preview ">
                                      <div class="close fileinput-remove"></div>
                                      <div class="file-drop-disabled">
                                        <div class="file-preview-thumbnails">
                                          <div class="file-initial-thumbs">
                                            <div class="file-preview-frame file-preview-initial" id="logo"
                                            data-fileindex="init_0" data-template="image">
                                            <div class="kv-file-content">


                                              <video width="400" controls>
                                                <source src="<?= $row['tvc_youtube_link'] ?>" type="video/mp4">

                                                </video>
                                              </div>
                                              <div class="file-thumbnail-footer">
                                                <div class="file-actions">
                                                  <div class="file-footer-buttons">
                                                    <button type="button" id="<?php echo $row['tvc_id']; ?>" onclick="deleteVdoTVC(<?php echo $row['tvc_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>

                                                  </div>
                                                  <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                                </div>
                                              </div>

                                            </div>
                                          </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="file-preview-status text-center text-success"></div>
                                        <div class="kv-fileinput-error file-error-message"
                                        style="display: none;"></div>
                                      </div>
                                    </div>


                                  </div>
                                </div>
                                <?php }
                              }?>
                              <div class="form-group">

                                <label class="control-label col-md-3" for="first-name">เลือก VDO <?php echo $req ?>  </label><span class="required"></span>

                                <div class="col-md-6">
                                  <input type="file" id="vdo_file" name="vdo_file" class="form-control col-md-7 col-xs-12"  <?php echo $required; ?> class="file-loading" accept="video/mp4">

                                </div>
                              </div>

                              <script>
                                $("#vdo_file").fileinput({
                                  uploadUrl: "upload.php", // server upload action
                                  maxFileCount: 1,
                                  allowedFileExtensions: ["mp4"],
                                  browseLabel: 'เลือกไฟล์',
                                  removeLabel: 'ลบ',
                                  browseClass: 'btn btn-success',
                                  showUpload: false,
                                  showRemove:false,
                                  showCaption: false,
                                  msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                  msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                  msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                  xxx:'seo_lead_img_file',
                                  dropZoneTitle : 'File VDO',
                                });
                              </script>
                            </div>

                            <div class="form-group">
                              <label class="control-label col-md-3" for="first-name">Link URL <span class="required"></span>
                              </label>
                              <div class="col-md-6">
                                <div class="control-group">
                                  <input type="text" name="tvc_url" class="form-control col-md-7 col-xs-12"  placeholder="Link URL" value="<?php echo mssql_escape($row['tvc_url']);?>">
                                </div>
                              </div>
                            </div>

                            <?php

                            $dates=$row['tvc_public_date'];

                            $year = substr($dates, 0, 4);
                            $day = substr($dates, 8, 2);
                            $mont = substr($dates, 5, 2);

                            $date=$day."/".$mont."/".$year;

                            ?>

                            <div class="form-group">
                              <label class="control-label col-md-3" for="first-name">วันที่จะเผยแพร่ <span class="required">*</span>
                              </label>
                              <div class="col-md-3">
                               <div class="control-group">
                                 <div class="controls">
                                   <input type="hidden" name="id" value="<?=$row['tvc_id']?>">
                                   <input type="text" class="form-control has-feedback-left active" id="single_cal2" placeholder="วันที่จะเผยแพร่" aria-describedby="inputSuccess2Status2" name="tvc_public_date" value="<?=$date?>" readonly>
                                   <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>

                                 </div>
                               </div>
                             </div>
                           </div>

                           <br><br>
                           <div class="form-group">
                            <label class="control-label col-md-3" for="first-name"><span class="required"></span>
                            </label>
                            <div class="col-md-6">
                             <div class="control-group">
                              <button type="submit" class="btn btn-success">Update</button>
                              <a class="confirms" data-title="ยืนยันการลบข้อมูล" href="delete_tvc.php?id=<?=$_GET['id']?>">
                                <button type="button" class="btn btn-danger">Delete</button></a>

                                <!--  <button id="example2" type="button" class="example2 btn btn-primary">example confirm</button> -->
                              </div>
                            </div>
                          </div>


                          <script type="text/javascript">

                            $('a.confirms').confirm({
                              content: 'คุณแน่ใจที่จะลบข้อมูลนี้ !',
                              buttons: {
                                Yes: {
                                  text: 'Yes',
                                  btnClass: 'btn-danger',
                                  keys: ['enter', 'a'],
                                  action: function(){
                                    location.href = this.$target.attr('href');
                                  }
                                },
                                No: {
                                  text: 'No',
                                  btnClass: 'btn-default',
                                  keys: ['enter', 'a'],
                                  action: function(){
                                      // button action.
                                    }
                                  },

                                }
                              });
                            </script>



                          </form>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>

              <!-- Bootstrap -->
              <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
              <!-- FastClick -->
              <script src="../vendors/fastclick/lib/fastclick.js"></script>
              <!-- NProgress -->
              <script src="../vendors/nprogress/nprogress.js"></script>
              <!-- iCheck -->
              <script src="../vendors/iCheck/icheck.min.js"></script>
              <script type="text/javascript" src="js/bootstrap-imgupload.js"></script>
              <!-- Validate -->
              <script src="../build/js/jquery.validate.js"></script>

              <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>

              <!-- bootstrap-progressbar -->
              <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>

              <!-- Custom Theme Scripts -->
              <script src="../build/js/custom.min.js"></script>
              <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>
              <!-- date -->
              <link rel="stylesheet" type="text/css" href="../build/css/jquery-ui.css"/>
              <script src="../build/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

              <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>
              <script type="text/javascript">
                $('.fancybox').fancybox();
              </script>
              <!-- Validate -->

              <script type="text/javascript">
                $('.fancybox').fancybox();
              </script>
              <!-- Validate -->

              <script>
                $(document).ready(function() {

                  $("#commentForm").validate({
                    rules: {
                      tvc_name: "required",
                      tvc_detail: "required",
                      url_youtube :"required",
                      tvc_public_date : "required",

                    },
                    messages: {
                      tvc_name: "กรุณากรอกชื่อ TVC !",
                      tvc_detail : "กรุณากรอกรายละเอียด !",
                      url_youtube : "กรุณากรอก url youtube !",
                    // project : "กรุณาเลือกโครงการ",
                    // project_review_dis_th : "กรุณากรอกรายละเอียด Project Review ไทย !",
                    img_youtube : "&nbsp; กรุณาเลือกรูป !",
                    thumbnail_vdo :  "&nbsp; กรุณาเลือกรูป !",
                    vdo_file : "&nbsp; กรุณาเลือก VDO !",
                    tvc_public_date : "กรุณาเลือกวันที่เผยแพร่ !",

                  }
                });



                });
              </script>

              <script type="text/javascript">
                $('#single_cal2').datepicker({
                  autoclose: true,
                  changeMonth: true,
                  showDropdowns: true,
                  changeYear: true,
                        //minDate:  new Date(),
                        onSelect: function(dateText) {
                          var d = new Date($(this).datepicker("getDate"))
                            //$scope.value.posted = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate();
                          }
                        });
                      </script>

                      <script>
                        $(document).ready(function(){
                          $("#alert").show();
                          $("#alert").fadeTo(3000, 400).slideUp(500, function(){
                            $("#alert").alert('close');
                          });
                        });
                      </script>

                      <script>
                        $(document).ready(function(){
                          $("#l1").<?=$type?>
                          $("#l2").<?=$type2?>
                        });
                        $("#youtube").on('ifChecked', function(event){
                          $("#l1").show();
                          $("#l2").hide();
                        });
                        $("#file").on('ifChecked', function(event){
                          $("#l1").hide();
                          $("#l2").show();
                        });

                      </script>
                      <script type="text/javascript">
                        $(document).ready(function(){
                          $("#optradio").val();
                          if($("#optradio").val() == "youtube"){
                            $("#l1").show();
                            $("#l2").hide();
                          }else if($("#optradio_vdo").val() == "vdo"){
                            $("#l2").show();
                            $("#l1").hide();
                          }
                        });
                      </script>

                      <script>
                        function deleteThumbnailTVC(id) {
                          $.confirm({
                            title: 'ลบรูปนี้ !',
                            content: 'คุณแน่ใจที่จะลบรูปนี้!',
                            buttons : {
                              Yes: {
                                text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                              var self = this;
                              return $.ajax({
                                url: 'ajax_deleteThumbnailTVC.php?tvc_id=' + id,
                                dataType: 'json',
                                method: 'get'
                              }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                  }).fail(function () {
                                    self.setContent('Something went wrong.');
                                  });
                                }
                              },
                              No: function () {
                            //$.alert('ยกลเิก!');
                          },
                        }
                      });
                        }
                      </script>
                      <script>
                        function deleteVdoTVC(id) {
                          $.confirm({
                            title: 'ลบรูปนี้ !',
                            content: 'คุณแน่ใจที่จะลบรูปนี้!',
                            buttons : {
                              Yes: {
                                text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                              var self = this;
                              return $.ajax({
                                url: 'ajax_deleteVdoTVC.php?tvc_id=' + id,
                                dataType: 'json',
                                method: 'get'
                              }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                  }).fail(function () {
                                    self.setContent('Something went wrong.');
                                  });
                                }
                              },
                              No: function () {
                            //$.alert('ยกลเิก!');
                          },
                        }
                      });
                        }
                      </script>
                      <script src="js/validate_file_300kb.js"></script>

                    </body>
                    </html>