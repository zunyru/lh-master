<?php
session_start();
include './include/dbCon_mssql.php';
$group_id = $_SESSION['group_id'];

$_GET['page'] = 'managment';

if (isset($_SESSION['add'])) {
	if ($_SESSION['add'] == 1) {
		//header("location:product_add.php");
		$success = "success";
		unset($_SESSION["add"]);
	} else if ($_SESSION['add'] == 2) {

		$error_name = "error_name"; //ค่าซ้ำ
		unset($_SESSION["add"]);

	} else if ($_SESSION['add'] == -1) {
		$error_img = "errors_img";
		unset($_SESSION["add"]);
	} else if ($_SESSION['add'] == -2) {
		$error_up = "error_up";
		unset($_SESSION["add"]);
	}
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <!-- Fancybox image popup -->
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
    <!-- bootstrap-progressbar -->
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>

    <!-- uploade img -->
    <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
    <!--uploade-->
    <!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
    <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
    <!-- confirm-->
    <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
    <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>

    <!-- Include Editor style. -->
    <link href="editor/css/froala_editor.min.css" rel="stylesheet" type="text/css" />
    <link href="editor/css/froala_style.min.css" rel="stylesheet" type="text/css" />

    <!--  froala  -->
    <link href="libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="libs/codemirror/5.3.0/codemirror.min.css">
    <link href="libs/froala-editor/2.6.2/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
    <link href="libs/froala-editor/2.6.2/css/froala_style.min.css" rel="stylesheet" type="text/css" />

    <!-- Include Editor Plugins style. -->
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/char_counter.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/code_view.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/colors.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/emoticons.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/file.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/fullscreen.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/image.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/image_manager.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/line_breaker.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/quick_insert.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/table.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/video.css">

    <!-- confirm-->
    <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
    <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>

    <script id="fr-fek">try{(function (k){localStorage.FEK=k;t=document.getElementById('fr-fek');t.parentNode.removeChild(t);})('inkH3fiA6B-13a==')}catch(e){}</script>

    <style type="text/css">
        .fr-toolbar.fr-desktop.fr-top.fr-basic.fr-sticky.fr-sticky-off {
            background: #f7f7f7  !important;
            border-top: 5px solid #f5f5f5  !important;
        }
        th.next.available {
            background: #6f9755;
        }
        th.next.available:hover {
            background: #9ab688;
        }
        th.prev.available {
            background: #6f9755;
        }
        th.prev.available:hover {
            background: #9ab688;
        }
        .thumbnail {
            height: 100px;
            margin: 5px;
        }
        .fileUpload {
            position: relative;
            overflow: hidden;
        //margin: 10px;
        }
        .fileUpload input.upload {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            padding: 0;
            font-size: 20px;
            cursor: pointer;
            opacity: 0;
            filter: alpha(opacity=0);
        }
        .file-drop-zone-title {
            padding: 0px 0px !important;
        }
        .form-control[readonly] { /* For Firefox */
            background-color: white;
        }

        .form-control[readonly] {
            background-color: white;
        }

        .file-caption-main .btn-file {
            overflow: visible;
        }

        .file-caption-main .btn-file .error {
            position: absolute;
            bottom: -32px;
            right: 30px;
        }
    </style>
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <?php include './master/navbar.php';?>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php include './master/top_nav.php';?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2><a href="managment_web.php?tab=Living">ระบบจัดการข้อมูล Living Tips</a> > <a href="living_tips_add.php">สร้าง Living Tips</a></h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">

                                <?php if (isset($success)) {?>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="form-group">
                                            <div class="alert alert-success col-md-6" id="alert">
                                                <button type="button" class="close" data-dismiss="alert">x</button>
                                                <strong>สร้างข้อมูลเรียบร้อยแล้ว </strong>
                                            </div>
                                        </div>
                                        <div class="col-md-6"></div>
                                    </div>
                                <?php } else if (isset($error_name)) {?>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="form-group">
                                            <div class="alert alert-danger col-md-6" id="alert">
                                                <button type="button" class="close" data-dismiss="alert">x</button>
                                                <strong>มีชื่อนี้อยู่ในระบบแล้ว </strong>
                                            </div>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                <?php } else if (isset($error_img)) {?>

                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="form-group">
                                            <div class="alert alert-danger col-md-6" id="alert">
                                                <button type="button" class="close" data-dismiss="alert">x</button>
                                                <strong>เกิดข้อผิดพลาด !</strong> ใส่รูปภาพไม่ครบ
                                            </div>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                <?php } else if (isset($error_up)) {?>

                                    <div class="row" id="error_">
                                        <div class="col-md-2"></div>
                                        <div class="form-group">
                                            <div class="alert alert-danger col-md-6" id="alert">
                                                <button type="button" class="close" data-dismiss="alert">x</button>
                                                <strong>เกิดข้อผิดพลาด !</strong> ไม่สามารถบันทึกข้อมูลได้
                                            </div>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                <?php }?>
                                <p class="font-gray-dark">
                                </p><br>
                                <form class="form-horizontal form-label-left" action="save_living_tips.php" method="POST"  enctype="multipart/form-data" name="myForm" id="commentForm">
                                    <div class="form-group">
                                        <label class="control-label col-md-3" for="first-name">ชื่อ Living Tips (ไทย)
                                            <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6">
                                            <input type="text" id="first-name1" required class="form-control col-md-7 col-xs-12" value="" name="living_tip_name_th" placeholder="ชื่อ Living Tips (ไทย)">
                                        </div>
                                    </div>

                                        <!-- SEO -->
                                        <div class="form-group">
                                            <label class="control-label col-md-3" for="first-name">Titlt SEO (ไทย)<br>(กรอกได้ไม่เกิน 70 คำ)
                                              <span class="required">*</span>
                                          </label>
                                          <div class="col-md-6">
                                              <input type="text"  required class="form-control col-md-7 col-xs-12" value="" name="landing_page_title_seo_th" placeholder="กรอก Titlt SEO" maxlength="70">
                                          </div>
                                      </div>
                                      <div class="form-group">
                                        <label class="control-label col-md-3" for="first-name">H1<br>(กรอกได้ไม่เกิน 255 คำ)
                                        </label>
                                        <div class="col-md-6">
                                            <input type="text"   class="form-control col-md-7 col-xs-12" value="" name="h1" placeholder="กรอก H1" maxlength="70">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-md-3" for="first-name">Description SEO (ไทย) <br>(กรอกได้ไม่เกิน 160 คำ)
                                      <span class="required">*</span>
                                  </label>
                                  <div class="col-md-6">
                                      <textarea required class="form-control col-md-7 col-xs-12" value="" name="landing_page_description_th" placeholder="กรอก Description SEO" rows="4" maxlength="160"></textarea> 
                                  </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3" for="first-name">Keyword SEO (ไทย)
                                  <span class="required">*</span>
                              </label>
                              <div class="col-md-6">
                                  <textarea required class="form-control col-md-7 col-xs-12" value="" name="keyword_seo" placeholder="กรอก keyword SEO" rows="4"></textarea> 
                              </div>
                          </div>

                              <div class="form-group">
                                <label class="control-label col-md-3" for="first-name">Thumnail SEO <br>นามสกุล .jpg .jpeg<br> ขนาด 600 x 338 px <br> ภาพขนาดไม่เกิน 300 KB <span class="required">*</span>
                                </label><br>
                                <div class="col-md-6">
                                  <input id="fileupload_seo" name="fileupload_th" type="file" multiple class="file-loading" accept="image/*" >
                              </div>
                          </div>

                          <script>
                            $("#fileupload_seo").fileinput({
                              uploadUrl: "upload.php", 
                              maxFileCount: 1,
                              allowedFileExtensions: ["jpg", "jpeg"],
                              browseLabel: 'เลือกรูป',
                              removeLabel: 'ลบ',
                              browseClass: 'btn btn-success',
                              showUpload: false,
                              showRemove:false,
                              showCaption: false, 
                              msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                              minImageWidth: 600, 
                              minImageHeight: 338, 
                              maxImageWidth: 600, 
                              maxImageHeight: 338, 
                              msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                              msgZoomModalHeading: 'ตัวอย่างละเอียด',
                              xxx:'brand_img_seo_th',
                              dropZoneTitle : 'รูป Thumnail SEO',
                              maxFileSize :300 ,
                              msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                              msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                              msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                              msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                              msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',   
                          });
                      </script>

                      <div class="form-group hide-for-th">
                        <label class="control-label col-md-3" for="first-name">ชื่อ Living Tips (อังกฤษ)
                            <span class="required">*</span>
                        </label>
                        <div class="col-md-6">
                            <input type="text" id="first-name2"  class="form-control col-md-7 col-xs-12" value="" name="living_tip_name_en" placeholder="ชื่อ Living Tips (อังกฤษ)">
                        </div>
                    </div>


                                    <!-- url yputube -->
                                    <div class="form-group" id="l1">
                                        <div class="row">
                                            <label class="control-label col-md-3" for="first-name"> </label>
                                            <label class="control-label col-md-3" for="first-name"></label>
                                        </div>
                                        <label class="control-label col-md-3" for="first-name">เลือกรูป Living Tips * <br> (ขนาดรูปต่ำสุด 353 x 240 px - <br> ขนาดรูปสูงสุด 1920 x 1080 px) <br> ภาพขนาดไม่เกิน 300 KB<span class="required"></span>
                                        </label>
                                        <div class="col-md-6">
                                            <input id="living_tip_img" name="living_tip_img" type="file" multiple class="file-loading" accept="image/*" >
                                        </div>
                                    </div>

                                    <script>
                                        $("#living_tip_img").fileinput({
                                            uploadUrl: "upload.php", // server upload action
                                            maxFileCount: 1,
                                            allowedFileExtensions: ["jpg", "gif", "png", "jpeg"],
                                            browseLabel: 'เลือกรูป',
                                            removeLabel: 'ลบ',
                                            browseClass: 'btn btn-success',
                                            showUpload: false,
                                            showRemove:false,
                                            showCaption: false,
                                            msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                            msgFilesTooMany: 'ไฟล์ที่คุณเลือกมีจำนวน <b>({n})</b> ซึ่งเกินกว่าที่ระบบอนุญาตที่ <b>{m}</b>, กรุณาลองใหม่อีกครั้ง!',
                                            msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                            xxx:'seo_living_tip_img',
                                            dropZoneTitle : 'รูป Living Tips',
                                            minImageWidth: 353, //ขนาด กว้าง ต่ำสุด
                                            minImageHeight: 240, //ขนาด ศุง ต่ำสุด
                                            maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                            maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                            maxFileSize :300 ,
                                            msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                            msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                            msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                            msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                            msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                        });
                                    </script>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">เลือกประเภทของ Living Tips <span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-9 col-xs-12">
                                            <div class="col-md-6 col-sm-9 col-xs-12">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" id="homeliving" checked="" name ="living_tip_category"  value="homeliving" class="flat"> Home & Living
                                                    </label>
                                                    <label>
                                                        <input type="radio" id="concept" name ="living_tip_category" value="concept" class="flat"> Living Concept
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">รายละเอียด Living Tips (ไทย)* <br> ใส่ข้อความไม่เกิน 200 คำ <span class="required"></span></label>
                                        <div class="col-md-6 col-sm-9 col-xs-12">
                                            <!-- <div id="edit" class="text-left txt-black fr-box fr-basic fr-top" style="z-index: 1000;"></div>  -->
                                            <textarea name="living_tip_content_th" class="form-control" rows="5" placeholder="กรอกรายละเอียด Living Tips (ไทย)" maxlength="200" required=""></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group hide-for-th">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">รายละเอียด Living Tips (อังกฤษ) <br> ใส่ข้อความไม่เกิน 200 คำ</label>
                                        <div class="col-md-6 col-sm-9 col-xs-12">

                                            <!-- <div id="edit" class="text-left txt-black fr-box fr-basic fr-top" style="z-index: 1000;"></div>  -->
                                            <textarea name="living_tip_content_en" class="form-control" rows="5"  maxlength="200" placeholder="กรอกรายละเอียด Living Tips (อังกฤษ)"></textarea>

                                        </div>
                                    </div>
                                    <!--  editor -->
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-3 col-xs-12">content Living Tips (ไทย)</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">

                                            <!-- <div id="edit" class="text-left txt-black fr-box fr-basic fr-top" style="z-index: 1000;"></div>  -->
                                            <textarea name="living_tip_dis_content" id="edit" rows="15" ></textarea>

                                        </div>
                                    </div>
                                    <!--   END editor-->

                                    <br>
                                    <br>
                                    <div class="form-group hide-for-th">
                                        <label class="control-label col-md-2 col-sm-3 col-xs-12">content Living Tips (อังกฤษ)</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">

                                            <!-- <div id="edit" class="text-left txt-black fr-box fr-basic fr-top" style="z-index: 1000;"></div>  -->
                                            <textarea name="living_tip_dis_content_en" id="edit_en" rows="15" ></textarea>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-2" for="first-name">กำหนด URL พิเศษ <br>(ห้ามใส่ อัขระพิเศษ)<span class="required"></span>
                                      </label>
                                      <div class="col-md-6">
                                        <input type="text"   class="form-control col-md-7 col-xs-12" value="" name="custom_url" placeholder="ชื่อ URL" >
                                    </div>
                                </div>

                                <br>
                                <div class="form-group">
                                    <label class="control-label col-md-2" for="first-name">วันที่จะเผยแพร่ <span class="required">*</span>
                                    </label>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <div class="controls">
                                                <input  id="date_start" type="text" class="form-control has-feedback-left active"  placeholder="วันที่จะเผยแพร่" aria-describedby="inputSuccess2Status2" name="date_start" readonly>
                                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <br>
                                    <div class="form-group">
                                        <label class="control-label col-md-3" for="first-name">
                                        </label>
                                        <div class="col-md-6">

                                            <button type="submit" class="btn btn-success">Submit</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!--   froala JS     -->

<!--        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>-->
        <script type="text/javascript" src="libs/codemirror/5.3.0/codemirror.min.js"></script>
        <script type="text/javascript" src="libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/froala_editor.pkgd.min.js"></script>

        <!-- Include Plugins. -->
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/align.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/char_counter.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/code_beautifier.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/code_view.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/colors.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/emoticons.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/entities.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/file.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/font_family.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/font_size.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/fullscreen.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/image.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/image_manager.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/inline_style.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/line_breaker.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/link.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/lists.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/paragraph_format.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/paragraph_style.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/quick_insert.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/quote.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/table.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/save.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/url.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/video.min.js"></script>
        <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/word_paste.min.js"></script>
         <!-- New -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.9.0/js/plugins/colors.min.js"></script>


        <!-- Bootstrap -->
        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../vendors/nprogress/nprogress.js"></script>
        <!-- iCheck -->
        <script src="../vendors/iCheck/icheck.min.js"></script>
        <script type="text/javascript" src="js/bootstrap-imgupload.js"></script>

        <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>

        <!-- bootstrap-progressbar -->
        <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="../build/js/custom.min.js"></script>

        <!-- date -->
        <link rel="stylesheet" type="text/css" href="../build/css/jquery-ui.css"/>
        <script src="../build/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

        <!-- Validate -->
        <script src="../build/js/jquery.validate.js"></script>

        <script>
            $(document).ready(function() {
                $.validator.setDefaults({ ignore: "[contenteditable='true']" });
                $("#commentForm").validate({
                    rules: {
                        living_tip_name_th: "required",
                        //living_tip_name_en: "required",
                        living_tip_category: "required",
                        living_tip_img : "required",
                        living_tip_content_th : "required",
                        //fileupload_en : "required",
                        date_start : "required",
                        landing_page_title_seo_th: {required: true,maxlength: 70},
                        landing_page_description_th:{required: true,maxlength: 160},
                        fileupload_th: "required",
                        keyword_seo: "required",

                    },
                    messages: {
                        living_tip_name_th: "กรุณากรอกชื่อ Living Tips ไทย !",
                        living_tip_name_en : "กรุณากรอกชื่อ Living Tips อังกฤษ !",
                        living_tip_category : "กรุณาเลือกประเภท",
                        living_tip_img : " &nbsp; กรุณาเลือกรูป !",
                        living_tip_content_th : "กรุณากรอกรายละเอียด Living Tips !",
                        fileupload_en : " &nbsp; กรุณาเลือกรูป",
                        date_start : "กรุณาระบุวันที่เผยแพร่",
                        landing_page_description_th:{
                            required: "กรุณากรอก Description SEO",
                            maxlength:  "กรุณากรอก ไม่เกิน 160 ตัวอักษร !"
                        },
                        landing_page_title_seo_th:{
                            required: "กรุณากรอก Title SEO",
                            maxlength:  "กรุณากรอก ไม่เกิน 70 ตัวอักษร !"
                        },
                        keyword_seo : "กรุณากรอก Keyword SEO ",
                        landing_page_title_seo_th:"กรุณากรอก Title SEO",
                        fileupload_th : " &nbsp; กรุณาเลือกรูป !",
                        h1 : {
                         maxlength:  "กรุณากรอก ไม่เกิน 255 ตัวอักษร !"
                     }
                 }
             });

                $("#living_tip_img").rules("add", {
                    required:true,
                    messages: {
                        required: " &nbsp; ไม่มีรูป !"
                    }
                });

            });
        </script>


        <script>
            $(function() {
                $.FroalaEditor.DefineIcon("imageInfo", { NAME: "info" });
                $.FroalaEditor.RegisterCommand("imageInfo", {
                    title: "Info",
                    focus: false,
                    undo: false,
                    refreshAfterCallback: false,
                    callback: function() {
                        var $img = this.image.get();
                        alert($img.attr("src"));
                    }
                });

                $('#edit').froalaEditor({

                    height: 500,
                    imageUploadURL: 'uploade_highlights.php',
                    imageUploadParams: {
                        id: 'edit'
                    },
                    imageManagerLoadURL: 'images_load.php',

                    fileUploadURL: 'upload_file.php',
                    fileUploadParams: {
                        id: 'edit'
                    },
                    videoUploadURL: 'upload_video.php',
                    videoUploadParams: {
                        id: 'edit'
                    },
                   fontSize: ["8", "9", "10", "11", "12", "14", "18","20","22","24", "30", "36", "48", "60", "72", "96"],
                    fontFamily: {
                        "LHfont": 'Kittithada',
                        "Roboto,sans-serif": 'Roboto',
                        "Oswald,sans-serif": 'Oswald',
                        "Montserrat,sans-serif": 'Montserrat',
                        "'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
                    },

                    imageManagerDeleteURL: "delete_image_highlights.php",
                    imageManagerDeleteMethod: "POST"
                })
                // Catch image removal from the editor.
                    .on('froalaEditor.image.removed', function (e, editor, $img) {
                        $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_image_highlights.php",

                            // Request params.
                            data: {
                                src: $img.attr('src')
                            }
                        })
                            .done (function (data) {
                                console.log ('image was deleted'+$img.attr('src'));
                            })
                            .fail (function (err) {
                                console.log ('image delete problem: ' + JSON.stringify(err));
                            })
                    })

                    // Catch image removal from the editor.
                    .on('froalaEditor.file.unlink', function (e, editor, link) {

                        $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_file.php",

                            // Request params.
                            data: {
                                src: link.getAttribute('href')
                            }
                        })
                            .done (function (data) {
                                console.log ('file was deleted');
                            })
                            .fail (function (err) {
                                console.log ('file delete problem: ' + JSON.stringify(err));
                            })
                    });

                //
                $('#edit_en').froalaEditor({

                    height: 500,
                    imageUploadURL: 'uploade_highlights.php',
                    imageUploadParams: {
                        id: 'edit_en'
                    },
                    imageManagerLoadURL: 'images_load.php',
                    // Set max image size to 300kB.
                    imageMaxSize: 0.3 * 1024 * 1024,
                    fileUploadURL: 'upload_file.php',
                    fileUploadParams: {
                        id: 'edit_en'
                    },
                    videoUploadURL: 'upload_video.php',
                    videoUploadParams: {
                        id: 'edit_en'
                    },
                    fontFamily: {
                        "LHfont": 'Kittithada',
                        "Roboto,sans-serif": 'Roboto',
                        "Oswald,sans-serif": 'Oswald',
                        "Montserrat,sans-serif": 'Montserrat',
                        "'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
                    },

                    imageManagerDeleteURL: "delete_image_highlights.php",
                    imageManagerDeleteMethod: "POST"
                })
                // Catch image removal from the editor.
                    .on('froalaEditor.image.removed', function (e, editor, $img) {
                        $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_image_highlights.php",

                            // Request params.
                            data: {
                                src: $img.attr('src')
                            }
                        })
                            .done (function (data) {
                                console.log ('image was deleted'+$img.attr('src'));
                            })
                            .fail (function (err) {
                                console.log ('image delete problem: ' + JSON.stringify(err));
                            })
                    })

                    // Catch image removal from the editor.
                    .on('froalaEditor.file.unlink', function (e, editor, link) {

                        $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_file.php",

                            // Request params.
                            data: {
                                src: link.getAttribute('href')
                            }
                        })
                            .done (function (data) {
                                console.log ('file was deleted');
                            })
                            .fail (function (err) {
                                console.log ('file delete problem: ' + JSON.stringify(err));
                            })
                    });
            });

        </script>


        <script>
            $(document).ready(function(){
                $("#alert").show();
                $("#alert").fadeTo(3000, 500).slideUp(500, function(){
                    $("#alert").alert('close');
                });
            });
        </script>

        <script>
            $(document).ready(function(){
                $("#l1").show();
                $("#l2").hide();
                $("#l3").hide();
                $("#error_").hide();
            });
        </script>

        <script>

            function getval(sel){
                $("#mySelect").val();
                if($("#mySelect").val() == "image"){
                    $("#l1").show();
                    $("#l2").hide();
                    $("#l3").hide();
                }else if($("#mySelect").val() == "banner"){
                    $("#l2").show();
                    $("#l3").hide();
                    $("#l1").hide();
                }else if($("#mySelect").val() == "vdo"){
                    $("#l3").show();
                    $("#l1").hide();
                    $("#l2").hide();
                }
            }
        </script>

        <script type="text/javascript">

            $('#date_start').datepicker({
                dateFormat : "dd/mm/yy",
                autoclose: true,
                changeMonth: true,
                showDropdowns: true,
                changeYear: true,
                minDate:  new Date(),
                onSelect: function(dateText) {
                    var d = new Date($(this).datepicker("getDate"));

                }
            });

        </script>
         <script src="js/validate_file_300kb.js"></script>

</body>
</html>
