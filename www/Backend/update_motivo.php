<?php
session_start();
include './include/dbCon_mssql.php';
date_default_timezone_set('Asia/Bangkok');
$date = date("Ymd");

$motivo_id     = $_POST['id'];
$motivo_name   = $_POST['motivo_name'];
$motivo_detail = $_POST['motivo_detail'];
$status        = $_POST['status'];
$motivo_detail = $_POST['motivo_detail'];
$month_start   = $_POST['month_start'];
$year_start    = $_POST['year_start'];
$month_end     = $_POST['month_end'];
$year_end      = $_POST['year_end'];

$numrand_img = (mt_rand());
$numrand_pdf = (mt_rand());
$images      = $_FILES['images'];
//print_r($images);
$images = $_FILES['images'];
//print_r($images);
$pdf        = $_FILES['pdf'];
$file_image = $images['name'];
$file_pdf   = $pdf['name'];

$dates        = date("Y-m-d H:i:s");

$check = getimagesize($_FILES["images"]["tmp_name"]);

function mssql_escape($str)
{
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return str_replace("'", "''", $str);
}

$sql   = "SELECT COUNT(*) AS counts FROM LH_MOTIVO WHERE (motivo_name = '" . $_POST['motivo_name'] . "') AND NOT motivo_id = $motivo_id";
$query = mssql_query($sql);
$nums  = mssql_fetch_array($query);

if ($nums['counts'] >= 1) {
    $_SESSION['add'] = '2';
    echo '<script>
                        window.history.go(-1);
                    </script>';
    exit();
}

//if ($motivo_name == "" || $month_start == "" || $year_start == "" || $month_end == "" || $year_end == "") {
//    header('Location: ' . $_SERVER['HTTP_REFERER'] . '&error=0');
//    exit();
//}
if ($status == "") {
    $status = "No";
}

if($month_start != '') {
    $start_date = $year_start . "-" . $month_start . "-" . "01";
    $end_date = $year_end . "-" . $month_end . "-" . "01";

    $sql = "UPDATE LH_MOTIVO SET motivo_name = '" . mssql_escape($motivo_name) . "', "
        . " motivo_detail = '" . mssql_escape($motivo_detail) . "', "
        . " motivo_status = '$status',"
        . " motivo_date_start = '$start_date',"
        . " update_date = '$dates',"
        . " motivo_date_end = '$end_date'"
        . " WHERE motivo_id = '$motivo_id'";
    $query = mssql_query($sql, $db_conn);

}else{
    $start_date = NULL;
    $end_date = NULL;

    $sql = "UPDATE LH_MOTIVO SET motivo_name = '" . mssql_escape($motivo_name) . "', "
        . " motivo_detail = '" . mssql_escape($motivo_detail) . "', "
        . " motivo_status = '$status',"
        . " motivo_date_start = NULL,"
        . " update_date = '$dates',"
        . " motivo_date_end = NULL "
        . " WHERE motivo_id = '$motivo_id'";
    $query = mssql_query($sql, $db_conn);
}



if ($check !== false) {
    //รูป

    $dates_file = date("Y-m-d");
    $path_img   = "fileupload/images/motivo/";
    //print_r($file_image);
    $type_img = strrchr($file_image, ".");

    $newname_img   = $date . $numrand_img . $type_img;
    $path_copy_img = $path_img . $newname_img;

    if (move_uploaded_file($_FILES['images']['tmp_name'], $path_copy_img)) {
        $sql   = "SELECT motivo_img_master FROM LH_MOTIVO WHERE motivo_id = '$motivo_id'";
        $query = mssql_query($sql, $db_conn);
        $row   = mssql_fetch_array($query);
        unlink($path_img . $row['motivo_img_master']);

        $sql = "UPDATE LH_MOTIVO SET motivo_img_master = '$newname_img',update_date = '$dates' "
            . " WHERE motivo_id = '$motivo_id'";
        $query = mssql_query($sql, $db_conn);

    } else {
        $_SESSION['add'] = '-3';
    echo '<script>
                        window.history.go(-1);
                    </script>';
    exit();
    }
}

///file
if (!empty($_FILES['pdf'])) {
//รูป

    $dates_file = date("Y-m-d");
    $path_img   = "fileupload/images/motivo/";

    $type_pdf = strrchr($file_pdf, ".");

    $newname_pdf   = $date . $numrand_pdf . $type_pdf;
    $path_copy_pdf = $path_img . $newname_pdf;

    if (move_uploaded_file($_FILES['pdf']['tmp_name'], $path_copy_pdf)) {
        $sql   = "SELECT motivo_file FROM LH_MOTIVO WHERE motivo_id = '$motivo_id'";
        $query = mssql_query($sql, $db_conn);
        $row   = mssql_fetch_array($query);
        unlink($path_img . $row['motivo_file']);

        $sql = "UPDATE LH_MOTIVO SET motivo_file = '$newname_pdf',update_date = '$dates'  "
            . " WHERE motivo_id = '$motivo_id'";
        $query = mssql_query($sql, $db_conn);

    }
}
$_SESSION['add'] = '1';
header("location:motivo_update.php?id=$motivo_id");
