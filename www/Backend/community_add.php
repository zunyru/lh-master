<?php
session_start();
include './include/dbCon_mssql.php';
$group_id = $_SESSION['group_id'];

$_GET['page'] = 'managment';

if (isset($_SESSION['add'])) {
    if($_SESSION['add'] ==1){
        //header("location:product_add.php");
        $success ="success";
        unset($_SESSION["add"]);
    } else if($_SESSION['add'] ==2){

		$error_name = "error_name"; //ค่าซ้ำ
		unset($_SESSION["add"]);

	} else if ($_SESSION['add'] == -1) {
		$error = "errors";
		unset($_SESSION["add"]);
	} else if ($_SESSION['add'] == -2) {
		$error_up = "error_up";
		unset($_SESSION["add"]);
	}
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>

   <!-- uploade img -->
    <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
     <!--uploade-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">

    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

  <!-- Include Editor style. -->
  <link href="editor/css/froala_editor.min.css" rel="stylesheet" type="text/css" />
  <link href="editor/css/froala_style.min.css" rel="stylesheet" type="text/css" />

  <!-- Include Editor style. -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.3.4/css/froala_editor.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.3.4/css/froala_style.min.css" rel="stylesheet" type="text/css" />

<!-- Include JS file. -->
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.3.4/js/froala_editor.min.js"></script> -->

  <!-- Include Code Mirror style -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">

  <!-- Include Editor Plugins style. -->
  <link rel="stylesheet" href="editor/css/plugins/char_counter.css">
  <link rel="stylesheet" href="editor/css/plugins/code_view.css">
  <link rel="stylesheet" href="editor/css/plugins/colors.css">
  <link rel="stylesheet" href="editor/css/plugins/emoticons.css">
  <link rel="stylesheet" href="editor/css/plugins/file.css">
  <link rel="stylesheet" href="editor/css/plugins/fullscreen.css">
  <link rel="stylesheet" href="editor/css/plugins/image.css">
  <link rel="stylesheet" href="editor/css/plugins/image_manager.css">
  <link rel="stylesheet" href="editor/css/plugins/line_breaker.css">
  <link rel="stylesheet" href="editor/css/plugins/quick_insert.css">
  <link rel="stylesheet" href="editor/css/plugins/table.css">
  <link rel="stylesheet" href="editor/css/plugins/video.css">

 <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.js"></script> -->
 <!-- confirm-->
 <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
 <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>

  <style type="text/css">
    .fr-toolbar.fr-desktop.fr-top.fr-basic.fr-sticky.fr-sticky-off {
        background: #f7f7f7  !important;
        border-top: 5px solid #f5f5f5  !important;
    }
    th.next.available {
        background: #6f9755;
    }
    th.next.available:hover {
        background: #9ab688;
    }
    th.prev.available {
        background: #6f9755;
    }
    th.prev.available:hover {
        background: #9ab688;
    }
      .thumbnail {
          height: 100px;
          margin: 5px;
      }
      .fileUpload {
        position: relative;
        overflow: hidden;
        //margin: 10px;
      }
      .fileUpload input.upload {
          position: absolute;
          top: 0;
          right: 0;
          margin: 0;
          padding: 0;
          font-size: 20px;
          cursor: pointer;
          opacity: 0;
          filter: alpha(opacity=0);
      }
      .file-drop-zone-title {
            padding: 0px 0px !important;
        }

</style>
 <style>
          .file-caption-main .btn-file {
              overflow: visible;
          }

          .file-caption-main .btn-file .error {
              position: absolute;
              bottom: -32px;
              right: 30px;
          }
      </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
           <?php include './master/navbar.php';?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
          <?php include './master/top_nav.php';?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><a href="managment_web.php?tab=Community">ระบบจัดการข้อมูล LH Community</a> > <a href="community_add.php">สร้าง LH Community</a></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                 <?php if (isset($success)) {?>
                   <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>สร้างข้อมูลเรียบร้อยแล้ว </strong>
                      </div>
                    </div>
                    <div class="col-md-6"></div>
                   </div>
                  <?php } else if (isset($error_name)) {?>
                    <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>มีชื่อนี้อยู่ในระบบแล้ว </strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                   <?php } else if (isset($errors)) {?>

                  <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>ลบข้อมูลเรียบร้อยแล้ว !</strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                   <?php } else if (isset($error_up)) {?>
                    <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>เกิดข้อผิดพลาด !</strong> ไม่สามารถบันทึกข้อมูลได้
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                   <?php }?>
                  <p class="font-gray-dark">
                  </p><br>

                  <form class="form-horizontal form-label-left" action="save_lhcommunity.php" method="POST" enctype="multipart/form-data" name="myForm" id="commentForm">
                    <div class="form-group">
                      <label class="control-label col-md-3" for="first-name">ชื่อ LH Community (ไทย)
                        <span class="required">*</span>
                      </label>
                      <div class="col-md-6">
                        <input type="text" id="first-name1" required class="form-control col-md-7 col-xs-12" value="" name="community_name_th" placeholder="ชื่อ LH Community (ไทย)">
                      </div>
                    </div>

                    <div class="form-group hide-for-th">
                      <label class="control-label col-md-3" for="first-name">ชื่อ LH Community (อังกฤษ)
                        <span class="required">*</span>
                      </label>
                      <div class="col-md-6">
                        <input type="text" id="first-name2" required class="form-control col-md-7 col-xs-12" value="" name="community_name_en" placeholder="ชื่อ LH Community (อังกฤษ)">
                      </div>
                    </div>

                    <!-- url yputube -->
                    <div class="form-group" id="l1">

                      <label class="control-label col-md-3" for="first-name">รูป LH Community * <br> (ความกว้างต่ำสุด 500px - ความกว้างสูงสุด 1920px)<br> (ไฟล์ jpg , gif , png) <br> ภาพขนาดไม่เกิน 300 KB<span class="required"></span>
                      </label>
                      <div class="col-md-6">
                          <input id="community_img" name="community_img" type="file" multiple class="file-loading" accept="image/*" >
                      </div>
                    </div>

                     <script>
                        $("#community_img").fileinput({
                            uploadUrl: "upload.php", // server upload action
                            maxFileCount: 1,
                            validateInitialCount: true,
                            overwriteInitial: false,
                            allowedFileExtensions: ["jpg", "gif", "png", "jpeg"],
                            browseLabel: 'เลือกรูป',
                            removeLabel: 'ลบ',
                            browseClass: 'btn btn-success',
                            showUpload: false,
                            showRemove:false,
                            showCaption: false,


                            msgZoomModalHeading: 'ตัวอย่างละเอียด',
                            xxx:'seo',
                            dropZoneTitle : 'รูป LH Community',
                            minImageWidth: 500, //ขนาด กว้าง ต่ำสุด
                            // minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                            maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                            //maxImageHeight: 278, //ขนาด ศุง ต่ำสุด
                            maxFileSize :300 ,
                            msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                            msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                            msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                            msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                            msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                            });
                        </script>

                    <script>
                    $(document).ready(function() {
                        //$("div.desc").hide();
                        $("#page2").attr("disabled", 'disabled');
                        $("#check1").click(function() {
                            var test = $(this).val();
                            //$("div.desc").hide();
                            if(test = "project"){
                              $("#page1").removeAttr('disabled');
                              $("#page1").attr("required", true);
                              $("#page2").attr("disabled", 'disabled');
                              $("#page2").val("");
                              //alert(test);
                            }
                            //$("#" + test).show();
                        });

                        $("#check2").click(function() {
                            var test = $(this).val();
                            //$("div.desc").hide();
                            if(test = "page"){
                              $("#page2").removeAttr('disabled');
                              $("#page2").attr("required", true);
                              $("#page1").attr("disabled", 'disabled');
                              $("#page1").val("");


                              //alert(test);
                            }
                            //$("#" + test).show();
                        });
                    });
                    </script>



                    <div class="form-group ">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">รายละเอียด โดยย่อ (ไทย) <span class="required">*</span></br>ใส่ข้อความไม่เกิน 200 คำ</label>
                      <div class="col-md-6 col-sm-9 col-xs-12">
                       <!-- <div id="edit" class="text-left txt-black fr-box fr-basic fr-top" style="z-index: 1000;"></div>  -->
                       <textarea name="community_dis_th" class="form-control" rows="4" placeholder="กรอกรายละเอียด LH Community (ไทย)" required="" maxlength="200"></textarea>
                     </div>
                   </div>

                   <div class="form-group hide-for-th">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">รายละเอียด โดยย่อ (อังกฤษ)</br>ใส่ข้อความไม่เกิน 200 คำ</label>
                    <div class="col-md-6 col-sm-9 col-xs-12">

                             <!-- <div id="edit" class="text-left txt-black fr-box fr-basic fr-top" style="z-index: 1000;"></div>  -->
                              <textarea name="community_dis_en" class="form-control" rows="4" placeholder="กรอกรายละเอียด LH Community (อังกฤษ)" maxlength="200"></textarea>

                        </div>
                    </div>

                      <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">บทสัมภาษณ์ (ไทย) *</label>
                          <div class="col-md-6 col-sm-9 col-xs-12">

                              <!-- <div id="edit" class="text-left txt-black fr-box fr-basic fr-top" style="z-index: 1000;"></div>  -->
                              <textarea name="community_con_th" required class="form-control" rows="10" placeholder="กรอกรายละเอียด บทสัมภาษณ์ (ไทย)"></textarea>

                          </div>
                      </div>

                <div class="form-group hide-for-th">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">บทสัมภาษณ์ (อังกฤษ)</label>
                  <div class="col-md-6 col-sm-9 col-xs-12">

                              <!-- <div id="edit" class="text-left txt-black fr-box fr-basic fr-top" style="z-index: 1000;"></div>  -->
                              <textarea name="community_con_en" class="form-control" rows="10" placeholder="กรอกรายละเอียด บทสัมภาษณ์ (อังกฤษ)"></textarea>

                          </div>
                      </div>

                      <br>
	                <div class="form-group">
	                  <label class="control-label col-md-3" for="first-name">
	                  </label>
	                  <div class="col-md-6">

	                      <button type="submit" class="btn btn-success">Submit</button>
	                  </div>
	                </div>

                  </form>
                </div>
              </div>
            </div>
          </div>

      </div>
    </div>

    <!-- jQuery -->
    <!-- <script src="../vendors/jquery/dist/jquery.min.js"></script> -->
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
            <!-- bootstrap-daterangepicker -->
    <script src="js/moment/moment.min.js"></script>
    <script src="js/datepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>


   <!--Editor_script-->

   <!-- Include JS files. -->
  <script type="text/javascript" src="editor/js/froala_editor.min.js"></script>
    <!-- Include Code Mirror. -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
     <!-- Include Plugins. -->
  <script type="text/javascript" src="editor/js/plugins/align.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/char_counter.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/code_beautifier.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/code_view.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/colors.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/emoticons.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/entities.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/file.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/font_family.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/font_size.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/fullscreen.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/image.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/image_manager.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/inline_style.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/line_breaker.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/link.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/lists.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/paragraph_format.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/paragraph_style.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/quick_insert.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/quote.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/table.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/save.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/url.min.js"></script>
  <script type="text/javascript" src="editor/js/plugins/video.min.js"></script>

    <!-- Validate -->
    <script src="../build/js/jquery.validate.js"></script>
    <script>
      $(document).ready(function() {

            $("#commentForm").validate({
                rules: {
                    community_name_th: "required",
                    //community_name_en: "required",
                    project: "required",
                    community_img : "required",
                    community_dis_th : "required",
                    community_con_th : "required",
                },
                messages: {
                    community_name_th: "กรุณากรอกชื่อ Community ไทย !",
                    //community_name_en : "กรุณากรอกชื่อ Community อังกฤษ !",
                    project : "กรุณาเลือกโครงการ !",
                    community_img : " &nbsp; กรุณาเลือกรูป !",
                    community_dis_th : "กรุณากรอกรายละเอียด !",
                    community_con_th : "กรุณากรอกบทสัมภาษณ์ !",
                }
            });

//            $("#lead_img_file").rules("add", {
//                required:true,
//                messages: {
//                    required: " &nbsp; ไม่มีรูป !"
//                }
//            });

        });
    </script>

     <!-- Include Language file if we want to u-->
    <script>
    $(function() {
      $('#edit').froalaEditor({
        height: 500,
        imageUploadURL: 'uploade_highlights.php',
        imageUploadParams: {
          id: 'my_editor'
        },

        fileUploadURL: 'upload_file.php',
        fileUploadParams: {
          id: 'my_editor'
        },

        imageManagerLoadURL: 'uploade_highlights.php',
        imageManagerDeleteURL: "delete_image_highlights.php",
        imageManagerDeleteMethod: "POST"
      })
      // Catch image removal from the editor.
      .on('froalaEditor.image.removed', function (e, editor, $img) {
        $.ajax({
          // Request method.
          method: "POST",

          // Request URL.
          url: "delete_image_highlights.php",

          // Request params.
          data: {
            src: $img.attr('src')
          }
        })
        .done (function (data) {
          console.log ('image was deleted'+$img.attr('src'));
        })
        .fail (function (err) {
          console.log ('image delete problem: ' + JSON.stringify(err));
        })
      })

      // Catch image removal from the editor.
      .on('froalaEditor.file.unlink', function (e, editor, link) {

        $.ajax({
          // Request method.
          method: "POST",

          // Request URL.
          url: "delete_file.php",

          // Request params.
          data: {
            src: link.getAttribute('href')
          }
        })
        .done (function (data) {
          console.log ('file was deleted');
        })
        .fail (function (err) {
          console.log ('file delete problem: ' + JSON.stringify(err));
        })
      })
    });

    $(function() {
        $('#edit_en').froalaEditor({
            height: 500,
            imageUploadURL: 'uploade_highlights.php',
            imageUploadParams: {
                id: 'my_editor'
            },

            fileUploadURL: 'upload_file.php',
            fileUploadParams: {
                id: 'my_editor'
            },

            imageManagerLoadURL: 'uploade_highlights.php',
            imageManagerDeleteURL: "delete_image_highlights.php",
            imageManagerDeleteMethod: "POST"
        })
        // Catch image removal from the editor.
            .on('froalaEditor.image.removed', function (e, editor, $img) {
                $.ajax({
                    // Request method.
                    method: "POST",

                    // Request URL.
                    url: "delete_image_highlights.php",

                    // Request params.
                    data: {
                        src: $img.attr('src')
                    }
                })
                    .done (function (data) {
                        console.log ('image was deleted'+$img.attr('src'));
                    })
                    .fail (function (err) {
                        console.log ('image delete problem: ' + JSON.stringify(err));
                    })
            })

            // Catch image removal from the editor.
            .on('froalaEditor.file.unlink', function (e, editor, link) {

                $.ajax({
                    // Request method.
                    method: "POST",

                    // Request URL.
                    url: "delete_file.php",

                    // Request params.
                    data: {
                        src: link.getAttribute('href')
                    }
                })
                    .done (function (data) {
                        console.log ('file was deleted');
                    })
                    .fail (function (err) {
                        console.log ('file delete problem: ' + JSON.stringify(err));
                    })
            })
    });
  </script>

   <script>
      $(document).ready(function(){
        $("#alert").show();
            $("#alert").fadeTo(3000, 500).slideUp(500, function(){
              $("#alert").alert('close');
          });
      });
      </script>

      <script>
        $(document).ready(function(){
            $("#l1").show();
            $("#l2").hide();
            $("#l3").hide();
        });
      </script>

      <script>

            function getval(sel){
               $("#mySelect").val();
                if($("#mySelect").val() == "image"){
                  $("#l1").show();
                  $("#l2").hide();
                  $("#l3").hide();
                }else if($("#mySelect").val() == "banner"){
                  $("#l2").show();
                  $("#l3").hide();
                  $("#l1").hide();
                }else if($("#mySelect").val() == "vdo"){
                  $("#l3").show();
                  $("#l1").hide();
                  $("#l2").hide();
                }
            }
        </script>
         <script src="js/validate_file_300kb.js"></script>


  </body>
</html>