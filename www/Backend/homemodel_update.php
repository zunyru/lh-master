<?php
session_start();
include './include/dbCon_mssql.php';
$group_id = $_SESSION['group_id'];

$_GET['page'] = 'homemodel';
include './include/dbFucntionPlan.php';

$dbFucntionPlan = new dbFucntionPlan();

if (isset($_SESSION['add'])) {
	if ($_SESSION['add'] == 1) {
		//header("location:product_add.php");
		$success = "success";
		unset($_SESSION["add"]);
	} else if ($_SESSION['add'] == 0) {

		$error = "error"; //ค่าซ้ำ
		$_SESSION['add'] = '';

	} else if ($_SESSION['add'] == -1) {
		$errors = "errors";
		unset($_SESSION["add"]);
	} else if ($_SESSION['add'] == -2) {
		$errors2 = "errors";
		unset($_SESSION["add"]);
	} else if ($_SESSION['add'] == -3) {
		$error_youtube = "error_youtube";
		unset($_SESSION["add"]);
	} else if ($_SESSION['add'] == -4) {
		$error_vdo = "error_vdo";
		unset($_SESSION["add"]);
	} else if ($_SESSION['add'] == -5) {
		$error_360 = "error_360";
		unset($_SESSION["add"]);
	}
}

$a = @$_SESSION['array'];

?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <!-- Meta, title, CSS, favicons, etc. -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>LAND & HOUSES</title>
      <!-- Bootstrap -->
      <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome -->
      <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <!-- NProgress -->
      <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
      <!-- iCheck -->
      <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
      <!-- bootstrap-progressbar -->
      <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
      <!-- Custom Theme Style -->
      <link href="../build/css/custom.min.css" rel="stylesheet">

      <!-- jQuery -->
      <script src="../vendors/jquery/dist/jquery.min.js"></script>

      <!-- uploade img -->
      <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
      <!--uploade-->
      <!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
      <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
      <!-- Fancybox image popup -->
      <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
      <!-- confirm-->
      <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
      <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>
      <script>
        $(document).ready(function(){
          $("#alert").show();
          $("#alert").fadeTo(6000, 400).slideUp(500, function(){
            $("#alert").alert('close');
          });
        });
      </script>
      <style type="text/css">
      .thumbnail {
        height: 100px;
        margin: 5px;
      }
      .fileUpload {
        position: relative;
        overflow: hidden;
        //margin: 10px;
      }
      .fileUpload input.upload {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
      }
      .file-drop-zone-title {
        padding: 0px 0px !important;
      }
      #upload_project_galery_seo_edit{
       display: none;
     }
     .file-caption-main .btn-file {
      overflow: visible;
    }

    .file-caption-main .btn-file .error {
      position: absolute;
      bottom: -32px;
      right: 30px;
      }#seo_img_youtube{
        display: none;
        }#seo_vdo_file{
          display: none;
          }#seo_thumbnail_vdo{
            display: none;
          }
          #seo{
            display: none;
          }
          .btn-switch {
           font-size: 14px;
           position: relative;
           display: inline-block;
           -webkit-user-select: none;
           -moz-user-select: none;
           -ms-user-select: none;
           user-select: none;
         }
         .btn-switch__radio {
          display: none;
        }
        .btn-switch__label {
          display: inline-block;
          padding: .75em .5em .75em .75em;
          vertical-align: top;
          font-size: 1em;
          font-weight: 700;
          line-height: 1.5;
          color: #666;
          cursor: pointer;
          transition: color .2s ease-in-out;
        }
        .btn-switch__label + .btn-switch__label {
         padding-right: .75em;
         padding-left: 0;
       }
       .btn-switch__txt {
        font-size: 19px;
        position: relative;
        z-index: 2;
        display: inline-block;
        min-width: 1.5em;
        opacity: 1;
        pointer-events: none;
        transition: opacity .2s ease-in-out;
      }
      .btn-switch__radio_no:checked ~ .btn-switch__label_yes .btn-switch__txt,
      .btn-switch__radio_yes:checked ~ .btn-switch__label_no .btn-switch__txt {
        opacity: 0;
      }
      .btn-switch__label:before {
        content: "";
        position: absolute;
        z-index: -1;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        background: #f0f0f0;
        border-radius: 1.5em;
        box-shadow: inset 0 .0715em .3572em rgba(43,43,43,.05);
        transition: background .2s ease-in-out;
      }
      .btn-switch__radio_yes:checked ~ .btn-switch__label:before {
        background: #1abc9c;
      }
      .btn-switch__label_no:after {
        content: "";
        position: absolute;
        z-index: 2;
        top: .5em;
        bottom: .5em;
        left: .5em;
        width: 2em;
        background: #fff;
        border-radius: 1em;
        pointer-events: none;
        box-shadow: 0 .1429em .2143em rgba(43,43,43,.2), 0 .3572em .3572em rgba(43,43,43,.1);
        transition: left .2s ease-in-out, background .2s ease-in-out;
      }
      .btn-switch__radio_yes:checked ~ .btn-switch__label_no:after {
        left: calc(100% - 2.5em);
        background: #fff;
      }
      .btn-switch__radio_no:checked ~ .btn-switch__label_yes:before,
      .btn-switch__radio_yes:checked ~ .btn-switch__label_no:before {
        z-index: 1;
      }
      .btn-switch__radio_yes:checked ~ .btn-switch__label_yes {
        color: #fff;
      }


    </style>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <?php include './master/navbar.php';?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include './master/top_nav.php';?>
        <!-- /top navigation -->

        <?php

$sql = "SELECT p.*,gp.* ,c_360.c360_plan_url,c.clip_plan_url , c_360.c360_img ,c.thumnail,c.clip_type,p.plan_img  FROM LH_PLANS p "
	. "LEFT JOIN LH_GALERY_PLAN gp ON p.plan_id = gp.plan_id "
	. "LEFT JOIN LH_360_FOR_PLAN c_360 ON c_360.plan_id = p.plan_id "
	. "LEFT JOIN LH_CLIP_FOR_PLAN c ON c.plan_id = p.plan_id "
	. "WHERE p.plan_id = '" . $_GET['id'] . "' ";
$query = mssql_query($sql, $db_conn);
$row3 = mssql_fetch_array($query);
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><a href="homemodel.php">ข้อมูลแบบบ้าน</a> > <a href="homemodel.php">แก้ไขข้อมูลแบบบ้าน : <?=$row3['plan_name_th'];?></a></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="font-gray-dark">
                    </p><br>

                    <?php if (isset($success)) {?>
                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="form-group">
                        <div class="alert alert-success col-md-6" id="alert">
                          <button type="button" class="close" data-dismiss="alert">x</button>
                          <strong>แก้ไขข้อมูลเรียบร้อยแล้ว</strong>
                        </div>
                      </div>
                      <div class="col-md-6"></div>
                    </div>
                    <?php } else if (isset($error)) {?>
                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="form-group">
                        <div class="alert alert-danger col-md-6" id="alert">
                          <button type="button" class="close" data-dismiss="alert">x</button>
                          <strong>มีชื่อ หรือ รหัส อยู่ในระบบแล้ว</strong>
                        </div>
                      </div>
                      <div class="col-md-4"></div>
                    </div>
                    <?php } else if (isset($errors2)) {?>

                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="form-group">
                        <div class="alert alert-danger col-md-6" id="alert">
                          <button type="button" class="close" data-dismiss="alert">x</button>
                          <strong>ไม่สามารถลบข้อมูลได้ !</strong> มีข้อมูลที่ผูกอยู่กับ
                          <?php $i = 0;foreach ($a as $value) {?>
                          <br>
                          <strong><?php $i++; echo $i.". โครงการ ".$value;?></strong>
                          <?php }?>
                        </div>
                      </div>
                      <div class="col-md-4"></div>
                    </div>
                    <?php } else if (isset($error_youtube)) {?>

                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="form-group">
                        <div class="alert alert-danger col-md-6" id="alert">
                          <button type="button" class="close" data-dismiss="alert">x</button>
                          <strong>ไม่สามารถอัพเดทข้อมูลได้ !</strong> ใส่ข้อมูล VDO Youtube ไม่ครบ
                        </div>
                      </div>
                      <div class="col-md-4"></div>
                    </div>
                    <?php } else if (isset($error_vdo)) {?>

                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="form-group">
                        <div class="alert alert-danger col-md-6" id="alert">
                          <button type="button" class="close" data-dismiss="alert">x</button>
                          <strong>ไม่สามารถอัพเดทข้อมูลได้ !</strong> ใส่ข้อมูล VDO File ไม่ครบ
                        </div>
                      </div>
                      <div class="col-md-4"></div>
                    </div>
                    <?php } else if (isset($error_360)) {?>

                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="form-group">
                        <div class="alert alert-danger col-md-6" id="alert">
                          <button type="button" class="close" data-dismiss="alert">x</button>
                          <strong>ไม่สามารถอัพเดทข้อมูลได้ !</strong> ใส่ข้อมูล 360 Virtual Tour ไม่ครบ
                        </div>
                      </div>
                      <div class="col-md-4"></div>
                    </div>
                    <?php }?>


                    <form class="form-horizontal form-label-left" action="update_plan.php" method="post" enctype="multipart/form-data" id="commentForm">
                      <div class="form-group">
                        <label class="control-label col-md-4" for="brand">เลือกสไตล์บ้าน <span class="required">*</span>
                        </label>
                        <?php
$query_3 = "SELECT * FROM LH_SERIES ORDER BY series_name_th ASC;";
$query_result3 = mssql_query($query_3, $db_conn);

?>
                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select class="form-control" name="series">
                            <option value="" >เลือกสไตล์บ้าน</option>
                            <?php
while ($row = mssql_fetch_array($query_result3)) {
	if ($row['series_id'] == $row3['series_id']) {
		$selected = 'selected';
	} else {
		$selected = '';
	}
	?>
                             <option value="<?=$row['series_id'];?>" <?=$selected?>><?=$row['series_name_th'];if ($row['series_name_en'] != '') {echo " / " . $row['series_name_en'];}?></option>
                             <?php }?>

                           </select>
                         </div>
                       </div>
                       <div class="form-group">
                        <label class="control-label col-md-4" for="first-name">รหัสแบบบ้าน <span class="required">*</span>
                        </label>
                        <div class="col-md-4">
                          <input type="text"   name="plan_code" required class="form-control col-md-7 col-xs-12" placeholder=""
                          value="<?=$row3['plan_code'];?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-4" for="first-name">ระบุชื่อแบบบ้าน <span class="required">*</span>
                        </label>
                        <div class="col-md-6">
                          <input type="text"   name="plan_name_th" required class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อแบบบ้าน (ไทย)" value="<?=$row3['plan_name_th'];?>">
                        </div>
                      </div>
                      <div class="form-group hide-for-th">
                        <label class="control-label col-md-4" for="first-name">ระบุชื่อแบบบ้าน <span class="required">*</span>
                        </label>
                        <div class="col-md-6">
                          <input type="text"  name="plan_name_en" required class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อแบบบ้าน (อังกฤษ)" value="<?=$row3['plan_name_en'];?>" >
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-4" for="first-name">พื้นที่ใช้สอย <span class="required">*</span>
                        </label>
                        <div class="col-md-4"  id="staticParent">
                          <input type="text" id="child"  name="plan_useful"  class="form-control col-md-4 col-xs-12" placeholder=""
                          value="<?=$row3['plan_useful'];?>">
                        </div><label class="control-label col-md-2 textleft" >ตารางเมตร
                        </label>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-4" for="first-name">ประเภทบ้าน <span class="required">*</span>
                        </label>
                        <div class="col-md-4"  id="staticParent">
                          <select name="product_id" class="form-control">
                            <option value="">-เลือกประเภทบ้าน-</option>
                            <?php
$sql = "SELECT  * FROM LH_PRODUCTS ORDER BY product_name_th  ASC";
$q = mssql_query($sql);
while ($row = mssql_fetch_array($q)) {
	?>
                              <option value="<?=$row['product_id']?>" <?php if ($row['product_id'] == $row3['product_id']) {echo 'selected';}?> ><?=$row['product_name_th'] . " (" . $row['product_name_en'] . ")"?></option>
                              <?php }?>
                            </select>
                          </div>
                        </label>
                      </div>


                      <br>
                      <div class="form-group">
                        <label class="control-label col-md-4" for="first-name">ระบุฟังก์ชั่นของบ้าน (ถ้าไม่มีไม่ต้องระบุ, ห้ามใส่ 0)
                        </label>
                      </div>
                      <?php
$productsArray = array();
$i = 0;
$spm = $dbFucntionPlan->show_FucntionPlan($group_id);
while ($row_spm = $spm->fetch(PDO::FETCH_ASSOC)) {

	if ($row_spm['function_plan_name_th'] == 'ที่จอดรถ') {
		$span = '<span class="required"> *</span>';
		$required = 'required';
		$_id = 'id="f_id1"';
		$name = 'name="fucn[]1"';
	} elseif ($row_spm['function_plan_name_th'] == 'ห้องนอน') {
		$span = '<span class="required"> *</span>';
		$required = 'required';
		$_id = 'id="f_id2"';
		$name = 'name="fucn[]2"';
	} elseif ($row_spm['function_plan_name_th'] == 'ห้องน้ำ') {
		$span = '<span class="required"> *</span>';
		$required = 'required';
		$_id = 'id="f_id3"';
		$name = 'name="fucn[]3"';
	} else {
		$span = '';
		$required = '';
		$name = 'name="fucn[]"';

	}

	$sql_f = "SELECT  p.plan_id,fp_s.function_plan_plan_id,f.function_plan_id, "
		. "fp_s.function_plan_sub_plan_count_room AS room,f.function_plan_name_th,"
		. "f.function_plan_pronoun  FROM LH_PLANS p "
		. "LEFT JOIN LH_FUNCTION_PLAN_SUB fp_s ON p.plan_id = fp_s.plan_id "
		. "FULL join LH_PLAN_FUNCTION f ON f.function_plan_id = fp_s.function_plan_plan_id "
		. "WHERE p.plan_id = '" . $_GET['id'] . "' ";
	$query_f = mssql_query($sql_f, $db_conn);

	?>

                       <div class="form-group">
                        <label class="control-label col-md-4"> <?=$row_spm['function_plan_name_th']; echo $span;?>
                        </label>
                        <div class="col-md-2">
                          <input type="hidden" name="name_f[]" value="<?=$row_spm['function_plan_id'];?>">
                          <input type="number" <?=$name;?> <?=$_id;?> <?=$required;?> class="form-control wdpercent1" placeholder=""
                          value="<?php
while ($row = mssql_fetch_array($query_f)) {
		if ($row_spm['function_plan_id'] == $row['function_plan_plan_id']) {
			echo $row['room'];
		}
	}
	?>">
                      </div>
                      <div class="col-md-3" style="margin-left: -100px;">
                        <label class="control-label col-md-5" style="text-align: left;" ><?=$row_spm['function_plan_pronoun'];?>
                        </label>
                      </div>
                    </div>

                    <?php $i++;}?>


                    <div class="form-group">
                      <label class="control-label col-md-4" for="first-name"></span>
                      </label>
                      <div class="col-md-3">

                       <!--  <button id="b1" class="btn add-more btn-success" type="button" data-toggle="modal" data-target="#myModal">+ เพิ่มชื่อห้องอื่นๆให้กับแบบบ้าน</button> -->
                     </div>
                   </div>

                   <div class="form-group">
                    <label class="control-label col-md-10"><u>ภาพแบบบ้าน (.JPG , .PNG , .GIF )</u>
                      กรุณาเลือกภาพที่ต้องการเป็นภาพหลัก 1 ภาพ (ขนาดรูป 1920 x 1080 px) ภาพขนาดไม่เกิน 300 KB

                    </label>

                  </div>

                  <label id="model_galery_radio[]-error" class="error" for="model_galery_radio[]"></label>

                  <?php
$sql = "SELECT * FROM LH_PLANS p LEFT JOIN LH_MODEL_IMAGE_PLANS m ON p.plan_id = m.plan_id WHERE p.plan_id = '" . $_GET['id'] . "'";
$query = mssql_query($sql);
$row = mssql_num_rows($query);

if ($row > 0) {

	?>

                    <div class="row">

                     <?php  
                     $i = 0;
                     while($result = mssql_fetch_assoc($query)) { ?>

                     <div class="col-sm-3">
                      <div class="form-group">
                       <div class="file-preview-thumbnails">
                        <div class="file-initial-thumbs">
                          <div class="file-preview-frame file-preview-initial" data-fileindex="init_0" data-template="image">
                            <div class="kv-file-content">
                              <a class="fancybox" rel="group_position_project" href="<?php echo $result['model_img_path']; ?>">
                                <img src="<?php echo $result['model_img_path']; ?>" class="kv-preview-data file-preview-image"  style="width:auto;height:160px;">
                              </a>
                            </div>
                            <p>Alt Text : </p>
                            <input type="type" name="model_seo_edit[]" class="form-control" placeholder="Alt Text" value="<?php echo $result['model_seo']; ?>" />
                            <div class="form-group">
                              <?php
if ($row3['plan_img'] == $result['model_img_path']) {
			$checked = 'checked';
		} else {
			$checked = '';
		}
		?>
                              <div class="col-md-2">
                                <input type="radio" id="<?=$i?>" <?=$checked;?> value="<?=$result['model_img_id']?>"  name="model_galery_radio[]">
                              </div>
                              <label for="<?=$i?>">
                                เลือกเป็นภาพหลัก
                              </label>
                            </div>
                            <input type="hidden" name="id_model[]" value="<?=$result['model_img_path'];?>">
                            <div class="file-thumbnail-footer">

                              <input type="hidden" name="model_img_id[]" value="<?=$result['model_img_id']?>">
                              <input type="file" id="galery_model_edit<?=$i?>" name="model_edit[]" />

                              <script>
                                $("#galery_model_edit<?=$i?>").fileinput({
                                                        uploadUrl: "upload.php", // server upload action
                                                        maxFileCount: 1,
                                                        allowedFileExtensions: ["jpg", "png", "jpeg"],
                                                        browseLabel: 'เลือกรูป',
                                                        removeLabel: 'ลบ',
                                                        browseClass: 'btn btn-success',
                                                        showUpload: false,
                                                        showRemove:false,
                                                        showCaption: false,
                                                        minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                                        minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                                        maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                                        maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                                        msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                        msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                        msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                                                        msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                                        msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                        xxx:'seo',
                                                        dropZoneTitle : 'รูปภาพแบบบ้าน',
                                                        maxFileSize :300 ,
                                                        msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                                        msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                                        msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                                        msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                                        msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                                      });
                                                    </script>



                                                    <div class="file-actions">
                                                      <div class="file-footer-buttons">
                                                        <?php
if ($row3['plan_img'] == $result['model_img_path']) {
			?>
                                                          <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                          <?php  }else{ ?>
                                                          <button type="button" id="<?php echo $result['model_img_id']; ?>" onclick="deleteImageModelPlan2(<?php echo $result['model_img_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                          <a class="fancybox" href="<?=$result['model_img_path'];?>">
                                                            <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                          </a>
                                                          <?php
}
		?>

                                                      </div>
                                                      <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>

                                        </div>
                                        <?php $i++;}?>

                                      </div>

                                      <?php }?>

                                      <!-- Add Model new -->
                                      <div class="row">

                                        <div id="Model_galery"></div>
                                      </div>

                                      <br>
                                      <div class="form-group">
                                        <label class="control-label col-md-3">
                                        </label>
                                        <div class="col-md-4">
                                          <button type="button" class="btn btn-round btn-success" onclick="addModel_galery()" >+
                                            เพิ่มรูปภาพแบบบ้าน
                                          </button>
                                        </div>
                                      </div>



                                      <br>
                                      <hr>
                                      <?php
$floorArray = array();
$sql_floor_paln = "SELECT * FROM  LH_GALERY_FLOOR_PLAN_IMG  WHERE plan_id = '" . $_GET['id'] . "' ORDER BY number_floor_plan ASC";
$query_floor = mssql_query($sql_floor_paln, $db_conn);

while ($floor_img = mssql_fetch_array($query_floor)) {
	//print_r($floor_img[3]);
	//array_push($productsArray, array('id_room' => $row['function_plan_id'],'num_room' => $row['room']));
	array_push($floorArray, array('number' => $floor_img[6], 'img' => $floor_img[2], 'seo' => $floor_img[5], 'dis_th' => $floor_img[3], 'dis_en' => $floor_img[4], 'id' => $floor_img[0]));
}
//print_r($floorArray);
// print_r($floorArray[0]['number']);
// print_r($floorArray[1]['number']);
// echo $floorArray[0]['number'];
?>
                                      <div class="form-group">
                                        <label class="control-label col-md-4">ภาพ Floor Plan ชั้น 1 <br>ภาพขนาดไม่เกิน 300 KB</label>
                                        <div class="col-md-6">
                                          <div id="map-image-holder-brochure"></div>
                                          <?php
if (!empty($floorArray[0]['img']) && ($floorArray[0]['number'] == '1')) {
	?>
                                           <div class="file-preview ">
                                            <div class="close fileinput-remove"></div>
                                            <div class="file-drop-disabled">
                                              <div class="file-preview-thumbnails">
                                                <div class="file-initial-thumbs">
                                                  <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                                    <div class="kv-file-content">
                                                      <a class="fancybox" href="<?=$floorArray[0]['img'];?>">
                                                        <img src="<?=$floorArray[0]['img'];?>" class="kv-preview-data file-preview-image"  style="width:auto;height:160px;">
                                                      </a>
                                                    </div>
                                                    <div class="file-thumbnail-footer">
                                                      <p>Alt Text :</p>
                                                      <input class="form-control" value="<?php echo $floorArray[0]['seo']; ?>" name="floorArray_edit_1" placeholder="Alt SEO">

                                                      <div class="file-actions">
                                                        <div class="file-footer-buttons">
                                                          <button type="button" id="<?php echo $floorArray[0]['id']; ?>" onclick="deleteImageFloorPlan(<?php echo $floorArray[0]['id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                          <a class="fancybox" href="<?=$floorArray[0]['img'];?>">
                                                            <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                          </a>
                                                        </div>
                                                        <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                                      </div>
                                                    </div>
                                                    <div class="file-footer-buttons">
                                                    <!--  <a href="delete_img_floor_plan.php?id=<?=$floorArray[0]['id'];?>">
                                                     <button type="button" class="kv-file-remove btn btn-xs btn-default" title="Remove file"><i class="glyphicon glyphicon-trash text-danger"></i></button></a> -->
                                                   </div>
                                                 </div>
                                               </div>

                                             </div>
                                             <div class="clearfix"></div>
                                             <div class="file-preview-status text-center text-success"></div>
                                             <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                           </div>
                                         </div>

                                         <?php }?>
                                         <input type="file" id="img_plan_1" name="images_plan_1[]" class="file-loading" accept="image/*">
                                       </div>
                                     </div>

                                     <script>
                                      $("#img_plan_1").fileinput(
                            //'enable',
                            {
                            uploadUrl: "upload.php", // server upload action
                            maxFileCount: 1,
                            allowedFileExtensions: ["jpg", "gif", "png", "jpeg"],
                            browseLabel: 'เลือกภาพ Floor Plan',
                            removeLabel: 'ลบ',
                            browseClass: 'btn btn-success',
                            showUpload: false,
                            showRemove:false,
                            showCaption: false,// ปิดช่องแสดงชื่อรูปภาพ
                            msgFilesTooMany: 'ไฟล์ที่คุณเลือกมีจำนวน <b>({n})</b> ซึ่งเกินกว่าที่ระบบอนุญาตที่ <b>{m}</b>, กรุณาลองใหม่อีกครั้ง!',
                            msgZoomModalHeading: 'ตัวอย่างละเอียด',
                            xxx:'seo_img_floor_paln1',
                            dropZoneTitle : 'รูปภาพ Floor Plan ชั้น 1',
                            maxFileSize :300 ,
                            msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                            msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                            msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                            msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                            msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                          });
                        </script>

                        <div class="form-group">
                          <label class="control-label col-md-4" for="first-name">บรรยาย Floor Plan ชั้น 1
                          </label>
                          <div class="col-md-6">
                           <textarea id="message"  class="form-control" name="message1_th" rows="7" placeholder="กรอกคำบรรยาย (ไทย) Floor Plan ชั้น 1"><?php if (!empty($floorArray[0]['number']) && ($floorArray[0]['number'] == '1')) {
	echo htmlspecialchars($floorArray[0]['dis_th']);
}?></textarea>
                         </div>
                       </div>

                       <div class="form-group hide-for-th">
                        <label class="control-label col-md-4" for="first-name">บรรยาย Floor Plan ชั้น 1
                        </label>
                        <div class="col-md-6">
                         <textarea id="message"  class="form-control" name="message1_en" rows="7" placeholder="กรอกคำบรรยาย (อังกฤษ) Floor Plan ชั้น 1"><?php if (!empty($floorArray[0]['number']) && ($floorArray[0]['number'] == '1')) {
	echo $floorArray[0]['dis_en'];
}?></textarea>
                       </div>
                     </div>

                     <div class="form-group">
                      <label class="control-label col-md-4">ภาพ Floor Plan ชั้น 2 <br>ภาพขนาดไม่เกิน 300 KB</label>
                      <div class="col-md-6">
                        <div id="map-image-holder-brochure"></div>
                        <?php
if (!empty($floorArray[1]['img']) && ($floorArray[1]['number'] == '2')) {
	?>
                         <div class="file-preview ">
                          <div class="close fileinput-remove"></div>
                          <div class="file-drop-disabled">
                            <div class="file-preview-thumbnails">
                              <div class="file-initial-thumbs">
                                <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                  <div class="kv-file-content">
                                    <a class="fancybox" href="<?=$floorArray[1]['img'];?>">
                                      <img src="<?=$floorArray[1]['img'];?>" class="kv-preview-data file-preview-image"  style="width:auto;height:160px;">
                                    </a>
                                  </div>
                                  <div class="file-thumbnail-footer">
                                    <p>Alt Text :</p>
                                    <input class="form-control" value="<?php echo $floorArray[1]['seo']; ?>" name="floorArray_edit_2" placeholder="Alt SEO">
                                    <div class="file-actions">
                                      <div class="file-footer-buttons">
                                        <button type="button" id="<?php echo $floorArray[1]['id']; ?>" onclick="deleteImageFloorPlan(<?php echo $floorArray[1]['id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                        <a class="fancybox" href="<?=$floorArray[1]['img'];?>">
                                          <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                        </a>
                                      </div>
                                      <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                    </div>
                                  </div>
                                  <div class="file-footer-buttons">
                                                    <!-- <a href="delete_img_floor_plan.php?id=<?=$floorArray[1]['id'];?>">
                                                     <button type="button" class="kv-file-remove btn btn-xs btn-default" title="Remove file"><i class="glyphicon glyphicon-trash text-danger"></i></button></a> -->
                                                   </div>
                                                 </div>
                                               </div>
                                             </div>
                                             <div class="clearfix"></div>
                                             <div class="file-preview-status text-center text-success"></div>
                                             <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                           </div>
                                         </div>
                                         <?php }?>
                                         <input type="file" id="img_plan_2" name="images_plan_2[]" class="file-loading" accept="image/*">
                                       </div>
                                     </div>


                                     <script>
                                      $("#img_plan_2").fileinput(
                            //'enable',
                            {
                            uploadUrl: "upload.php", // server upload action
                            maxFileCount: 1,
                            allowedFileExtensions: ["jpg", "gif", "png", "jpeg"],
                            browseLabel: 'เลือกภาพ Floor Plan',
                            removeLabel: 'ลบ',
                            browseClass: 'btn btn-success',
                            showUpload: false,
                            showRemove:false,
                            showCaption: false,// ปิดช่องแสดงชื่อรูปภาพ
                            msgFilesTooMany: 'ไฟล์ที่คุณเลือกมีจำนวน <b>({n})</b> ซึ่งเกินกว่าที่ระบบอนุญาตที่ <b>{m}</b>, กรุณาลองใหม่อีกครั้ง!',
                            msgZoomModalHeading: 'ตัวอย่างละเอียด',
                            xxx:'seo_img_floor_paln2',
                            dropZoneTitle : 'รูปภาพ Floor Plan ชั้น 2',
                            maxFileSize :300 ,
                            msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                            msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                            msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                            msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                            msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                          });
                        </script>

                        <div class="form-group">
                          <label class="control-label col-md-4" for="first-name">บรรยาย Floor Plan ชั้น 2
                          </label>
                          <div class="col-md-6">
                           <textarea id="message"  class="form-control" name="message2_th" rows="7" placeholder="กรอกคำบรรยาย Floor Plan (ไทย) ชั้น 2"><?php if (!empty($floorArray[1]['number']) && ($floorArray[1]['number'] == '2')) {
	echo htmlspecialchars($floorArray[1]['dis_th']);
}?></textarea>
                         </div>
                       </div>

                       <div class="form-group hide-for-th">
                        <label class="control-label col-md-4" for="first-name">บรรยาย Floor Plan ชั้น 2
                        </label>
                        <div class="col-md-6">
                         <textarea id="message"  class="form-control" name="message2_en" rows="7" placeholder="กรอกคำบรรยาย Floor Plan (อังกฤษ) ชั้น 2"><?php if (!empty($floorArray[1]['number']) && ($floorArray[1]['number'] == '2')) {
	echo $floorArray[1]['dis_en'];
}?></textarea></textarea>
                       </div>
                     </div>

                     <div class="form-group">
                      <label class="control-label col-md-4">ภาพ Floor Plan ชั้น 3 <br>ภาพขนาดไม่เกิน 300 KB</label>
                      <div class="col-md-6">
                        <div id="map-image-holder-brochure"></div>
                        <?php
if (!empty($floorArray[2]['img']) && ($floorArray[2]['number'] == '3')) {
	?>
                         <div class="file-preview ">
                          <div class="close fileinput-remove"></div>
                          <div class="file-drop-disabled">
                            <div class="file-preview-thumbnails">
                              <div class="file-initial-thumbs">
                                <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                  <div class="kv-file-content">
                                    <a class="fancybox" href="<?=$floorArray[2]['img'];?>">
                                      <img src="<?=$floorArray[2]['img'];?>" class="kv-preview-data file-preview-image"  style="width:auto;height:160px;">
                                    </a>
                                  </div>
                                  <div class="file-thumbnail-footer">
                                    <p>Alt Text :</p>
                                    <input class="form-control" value="<?php echo $floorArray[2]['seo']; ?>" name="floorArray_edit_3" placeholder="Alt SEO">
                                    <div class="file-actions">
                                      <div class="file-footer-buttons">
                                        <button type="button" id="<?php echo $floorArray[2]['id']; ?>" onclick="deleteImageFloorPlan(<?php echo $floorArray[2]['id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                        <a class="fancybox" href="<?=$floorArray[2]['img'];?>">
                                          <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                        </a>
                                      </div>
                                      <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                    </div>
                                  </div>
                                  <div class="file-footer-buttons">
                                                   <!--  <a href="delete_img_floor_plan.php?id=<?=$floorArray[2]['id'];?>">
                                                     <button type="button" class="kv-file-remove btn btn-xs btn-default" title="Remove file"><i class="glyphicon glyphicon-trash text-danger"></i></button></a> -->
                                                   </div>
                                                 </div>
                                               </div>
                                             </div>
                                             <div class="clearfix"></div>
                                             <div class="file-preview-status text-center text-success"></div>
                                             <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                           </div>
                                         </div>
                                         <?php }?>
                                         <input type="file" id="img_plan_3" name="images_plan_3[]" class="file-loading" accept="image/*">
                                       </div>
                                     </div>


                                     <script>
                                      $("#img_plan_3").fileinput(
                            //'enable',
                            {
                            uploadUrl: "upload.php", // server upload action
                            maxFileCount: 1,
                            allowedFileExtensions: ["jpg", "gif", "png", "jpeg"],
                            browseLabel: 'เลือกภาพ Floor Plan',
                            removeLabel: 'ลบ',
                            browseClass: 'btn btn-success',
                            showUpload: false,
                            showRemove:false,
                            showCaption: false,// ปิดช่องแสดงชื่อรูปภาพ
                            msgFilesTooMany: 'ไฟล์ที่คุณเลือกมีจำนวน <b>({n})</b> ซึ่งเกินกว่าที่ระบบอนุญาตที่ <b>{m}</b>, กรุณาลองใหม่อีกครั้ง!',
                            msgZoomModalHeading: 'ตัวอย่างละเอียด',
                            xxx:'seo_img_floor_paln3',
                            dropZoneTitle : 'รูปภาพ Floor Plan ชั้น 3',
                            maxFileSize :300 ,
                            msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                            msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                            msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                            msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                            msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                          });
                        </script>

                        <div class="form-group">
                          <label class="control-label col-md-4" for="first-name">บรรยาย Floor Plan ชั้น 3
                          </label>
                          <div class="col-md-6">
                           <textarea id="message"  class="form-control" name="message3_th" rows="7" placeholder="กรอกคำบรรยาย Floor Plan (ไทย) ชั้น 3"><?php
if (!empty($floorArray[2]['number']) && ($floorArray[2]['number'] == '3')) {
	echo htmlspecialchars($floorArray[2]['dis_th']);
}
?></textarea>
                         </div>
                       </div>

                       <div class="form-group hide-for-th">
                        <label class="control-label col-md-4" for="first-name">บรรยาย Floor Plan ชั้น 3
                        </label>
                        <div class="col-md-6">
                         <textarea id="message"  class="form-control" name="message3_en" rows="7" placeholder="กรอกคำบรรยาย Floor Plan (อังกฤษ) ชั้น 3"><?php
if (!empty($floorArray[2]['number']) && ($floorArray[2]['number'] == '3')) {
	echo $floorArray[2]['dis_en'];
}?></textarea>
                       </div>
                     </div>

                     <div class="form-group">
                      <label class="control-label col-md-4" for="first-name">URL 360 Virtual Tour
                      </label>
                      <div class="col-md-6">
                        <input type="text" id="c_360" name="c_360" class="form-control col-md-7 col-xs-12" placeholder="Url 360 Virtual Tour" value="<?=$row3['c360_plan_url']?>">
                      </div>
                    </div>


                    <?php
$sql = "SELECT * FROM LH_360_FOR_PLAN WHERE plan_id = '" . $_GET['id'] . "'";
$query = mssql_query($sql);
$row = mssql_fetch_array($query);

?>

                    <div class="form-group">
                      <label class="control-label col-md-4">360 Cover image <br>(ขนาดรูป 1920 x 1080) <br>ภาพขนาดไม่เกิน 300 KB</label>
                      <div class="col-md-6">
                        <div id="map-image-holder-brochure"></div>

                        <?php if (isset($row3['c360_img']) && $row3['c360_img'] != '') {?>
                        <script type="text/javascript">
                          $(document).ready(function() {
                            $('#c_img').val("Y");
                          });
                        </script>


                        <div class="file-preview ">
                          <div class="close fileinput-remove"></div>
                          <div class="file-drop-disabled">
                            <div class="file-preview-thumbnails">
                              <div class="file-initial-thumbs">
                                <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                  <div class="kv-file-content">
                                    <a class="fancybox" href="<?=$row3['c360_img'];?>">
                                      <img src="<?=$row3['c360_img'];?>" class="kv-preview-data file-preview-image"  style="width:auto;height:160px;">
                                    </a>
                                  </div>
                                  <div class="file-thumbnail-footer">
                                    <div class="file-actions">
                                      <div class="file-footer-buttons">
                                        <button type="button" id="<?php echo $row['c360_plan_id']; ?>" onclick="delete360Plan(<?php echo $row['c360_plan_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                        <a class="fancybox" href="<?=$row3['c360_img'];?>">
                                          <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                        </a>
                                      </div>
                                      <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="file-preview-status text-center text-success"></div>
                            <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                          </div>
                        </div>
                        <?php }?>
                        <input type="file" id="img" name="c_360_img[]" class="file-loading" accept="image/*">
                        <input type="hidden" id="c_img" value="">
                      </div>
                    </div>


                    <script>
                      $("#img").fileinput({
                              uploadUrl: "upload.php", // server upload action
                              maxFileCount: 1,
                              allowedFileExtensions: ["jpg", "gif", "png", "jpeg"],
                              browseLabel: 'เลือก 360 ',
                              removeLabel: 'ลบ',
                              browseClass: 'btn btn-success',
                              showUpload: false,
                              showRemove:false,
                              showCaption: false,// ปิดช่องแสดงชื่อรูปภาพ
                              msgFilesTooMany: 'ไฟล์ที่คุณเลือกมีจำนวน <b>({n})</b> ซึ่งเกินกว่าที่ระบบอนุญาตที่ <b>{m}</b>, กรุณาลองใหม่อีกครั้ง!',
                              msgZoomModalHeading: 'ตัวอย่างละเอียด',
                              xxx:'seo_img_paln',
                              dropZoneTitle : 'รูปภาพ Cover image 360',
                              minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                              minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                              maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                              maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                              maxFileSize :300 ,
                              msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                              msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                              msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                              msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                              msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                            });
                          </script>

                          <?php
$sql_v = "SELECT * FROM LH_CLIP_FOR_PLAN WHERE plan_id = '" . $_GET['id'] . "'";
$query = mssql_query($sql_v);
$row_v = mssql_fetch_array($query);

$checked = '';
$checked_2 = '';
//echo $row_c['tvc_type'];
$checked = '';
$checked_2 = '';
if ($row_v['clip_type'] == "youtube") {
	$checked = 'checked';
	$type = 'show();';
	$type2 = 'hide();';
} else if ($row_v['clip_type'] == "vdo") {
	$checked_2 = 'checked';
	$type = 'hide();';
	$type2 = 'show();';
} else {
	$checked = 'checked';
	$type = 'show();';
	$type2 = 'hide();';
}
?>

                          <div class="form-group" id="inline_content">
                            <label class="control-label col-md-4" name="tvc_detail" for="first-name">เลือกชนิดการอัปไฟล์ <span class="required"></span>
                            </label>
                            <div class="col-md-6">
                              <div class="radio">
                                <label><input type="radio" id="youtube" name="optradio_vdo" <?=$checked;?> value="youtube" class="flat"> VDO Youtube</label>
                              </div>
                              <div class="radio">
                                <label><input type="radio" id="file" name="optradio_vdo" <?=$checked_2;?> value="vdo" class="flat"> VDO File</label>
                              </div>
                            </div>
                          </div>

                          <div id="v1">
                            <div class="form-group">
                              <label class="control-label col-md-4" for="first-name">URL Youtube  <span class="required"> </span>
                              </label>
                              <div class="col-md-6">
                                <div class="control-group">
                                  <input type="text"  id="url_youtube" name="url_youtube"  class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube" value="<?php if ($row_v['clip_type'] == 'youtube') {echo $row_v['clip_plan_url'];}?>">
                                </div>
                              </div>
                            </div>



                            <div class="form-group">
                              <label class="control-label col-md-4" for="first-name">Thumbnail Youtube <br> ขนาด 1920 x 1080 px) <br>ภาพขนาดไม่เกิน 300 KB<span class="required"></span>
                              </label>
                              <div class="col-md-6">
                                <div id="map-image-holder-brochure"></div>
                                <?php

if ($row_v['clip_type'] == 'youtube') {
	if ((isset($row_v['thumnail'])) && $row_v['thumnail'] != '') {?>
                                  <script type="text/javascript">
                                    $(document).ready(function() {
                                      $('#youtube_img').val("Y");
                                    });
                                  </script>
                                  <div class="file-preview ">
                                    <div class="close fileinput-remove"></div>
                                    <div class="file-drop-disabled">
                                      <div class="file-preview-thumbnails">
                                        <div class="file-initial-thumbs">
                                          <div class="file-preview-frame file-preview-initial" id="logo"
                                          data-fileindex="init_0" data-template="image">
                                          <div class="kv-file-content">
                                            <a class="fancybox" href="<?=$row_v['thumnail']?>">
                                              <img src="<?=$row_v['thumnail']?>"
                                              class="kv-preview-data file-preview-image"
                                              style="width:auto;height:160px;">
                                            </a>
                                          </div>
                                          <div class="file-thumbnail-footer">
                                            <div class="file-actions">
                                              <div class="file-footer-buttons">
                                                <button type="button" id="<?php echo $row_v['clip_plan_id']; ?>" onclick="deleteImageThumbnailPlan(<?php echo $row_v['clip_plan_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                <a class="fancybox" href="<?=$row_v['thumnail'];?>">
                                                  <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                </a>
                                              </div>
                                              <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                            </div>
                                          </div>

                                        </div>
                                      </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="file-preview-status text-center text-success"></div>
                                    <div class="kv-fileinput-error file-error-message"
                                    style="display: none;"></div>
                                  </div>
                                </div>
                                <?php }
}?>
                              <input type="file" name="img_youtube" id="img_youtube" class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube" value="" accept="image/*">
                              <input type="hidden" id="youtube_img" value="">
                            </div>
                          </div>

                        </div>

                        <script>
                          $("#img_youtube").fileinput({
                              uploadUrl: "upload.php", // server upload action
                              maxFileCount: 1,
                              allowedFileExtensions: ["jpg", "png", "jpeg"],
                              browseLabel: 'เลือกรูป',
                              removeLabel: 'ลบ',
                              browseClass: 'btn btn-success',
                              showUpload: false,
                              showRemove:false,
                              showCaption: false,// ปิดช่องแสดงชื่อรูปภาพ
                              msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                              msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                              msgZoomModalHeading: 'ตัวอย่างละเอียด',
                              xxx:'seo_img_youtube',
                              dropZoneTitle : 'รูปภาพ Thumbnail Youtube',
                              minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                              minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                              maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                              maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                              maxFileSize :300 ,
                              msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                              msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                              msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                              msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                              msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                            });
                          </script>


                          <div id="v2">
                            <?php
$sql_v = "SELECT * FROM LH_CLIP_FOR_PLAN WHERE plan_id = '" . $_GET['id'] . "'";
$query = mssql_query($sql_v);
$row_v = mssql_fetch_array($query);
?>

                            <div class="form-group">
                              <label class="control-label col-md-4" for="first-name">Thumbnail VDO <br>(.jpg , .png ,.gif <br> ขนาด 1920 x 1080 px) <br>ภาพขนาดไม่เกิน 300 KB<span class="required"> </span>
                              </label>
                              <div class="col-md-6">
                                <div id="map-image-holder-brochure"></div>
                                <?php
if ($row_v['clip_type'] == 'vdo') {
	if ((isset($row_v['thumnail'])) && $row_v['thumnail'] != '') {?>
                                  <script type="text/javascript">
                                    $(document).ready(function() {
                                      $('#vdo_thumbnail').val("Y");
                                    });
                                  </script>
                                  <div class="file-preview ">
                                    <div class="close fileinput-remove"></div>
                                    <div class="file-drop-disabled">
                                      <div class="file-preview-thumbnails">
                                        <div class="file-initial-thumbs">
                                          <div class="file-preview-frame file-preview-initial" id="logo"
                                          data-fileindex="init_0" data-template="image">
                                          <div class="kv-file-content">
                                            <a class="fancybox"
                                            href="<?=$row_v['thumnail']?>">
                                            <img src="<?=$row_v['thumnail']?>"
                                            class="kv-preview-data file-preview-image"
                                            style="width:auto;height:160px;"
                                            >
                                          </a>
                                        </div>
                                        <div class="file-thumbnail-footer">
                                          <div class="file-actions">
                                            <div class="file-footer-buttons">
                                              <button type="button" id="<?php echo $row_v['clip_plan_id']; ?>" onclick="deleteImageThumbnailPlan(<?php echo $row_v['clip_plan_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                              <a class="fancybox" href="<?=$row_v['thumnail'];?>">
                                                <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                              </a>
                                            </div>
                                            <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="clearfix"></div>
                                  <div class="file-preview-status text-center text-success"></div>
                                  <div class="kv-fileinput-error file-error-message"
                                  style="display: none;"></div>
                                </div>
                              </div>
                              <?php }
}?>
                            <input type="file" name="thumbnail_vdo" id="thumbnail_vdo" class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube" value="" accept="image/*">
                            <input type="hidden" id="vdo_thumbnail" value="">
                          </div>
                        </div>



                        <script>
                          $("#thumbnail_vdo").fileinput({
                                  uploadUrl: "upload.php", // server upload action
                                  maxFileCount: 1,
                                  allowedFileExtensions: ["jpg", "png", "jpeg"],
                                  browseLabel: 'เลือกรูป',
                                  removeLabel: 'ลบ',
                                  browseClass: 'btn btn-success',
                                  showUpload: false,
                                  showRemove:false,
                                  showCaption: false,// ปิดช่องแสดงชื่อรูปภาพ
                                  msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                  msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                  msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                  xxx:'seo_thumbnail_vdo',
                                  dropZoneTitle : 'รูปภาพ Thumbnail VDO',
                                  minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                  minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                  maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                  maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                  maxFileSize :300 ,
                                  msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                  msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                  msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                  msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                  msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                });
                              </script>

                              <div class="form-group">
                                <label class="control-label col-md-4" for="first-name"> VDO <br> (เฉพาะไฟล์ .mp4 ) <span class="required"> </span>
                                </label>
                                <div class="col-md-6">
                                  <div id="map-image-holder-brochure"></div>
                                  <?php
if ($row_v['clip_type'] == 'vdo') {
	if ((isset($row_v['clip_plan_url'])) && $row_v['clip_plan_url'] != '') {?>
                                    <script type="text/javascript">
                                      $(document).ready(function() {
                                        $('#file_vdo').val("Y");
                                      });
                                    </script>
                                    <div class="file-preview ">
                                      <div class="close fileinput-remove"></div>
                                      <div class="file-drop-disabled">
                                        <div class="file-preview-thumbnails">
                                          <div class="file-initial-thumbs">
                                            <div class="file-preview-frame file-preview-initial" id="logo"
                                            data-fileindex="init_0" data-template="image">
                                            <div class="kv-file-content">

                                              <video width="400" controls>
                                                <source src="<?=$row_v['clip_plan_url']?>" type="video/mp4">

                                                </video>

                                              </div>
                                              <div class="file-thumbnail-footer">
                                                <div class="file-actions">
                                                  <div class="file-footer-buttons">
                                                    <button type="button" id="<?php echo $row_v['clip_plan_id']; ?>" onclick="deleteVDOPlan(<?php echo $row_v['clip_plan_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                  </div>
                                                  <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                                </div>
                                              </div>

                                            </div>
                                          </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="file-preview-status text-center text-success"></div>
                                        <div class="kv-fileinput-error file-error-message"
                                        style="display: none;"></div>
                                      </div>
                                    </div>
                                    <?php }
}?>
                                  <input type="file" name="vdo_file" id="vdo_file" class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube" value="" accept="video/mp4">
                                  <input type="hidden" id="file_vdo" value="">
                                </div>
                              </div>

                              <script>
                                $("#vdo_file").fileinput({
                                  uploadUrl: "upload.php", // server upload action
                                  maxFileCount: 1,
                                  allowedFileExtensions: ["mp4"],
                                  browseLabel: 'เลือกไฟล์',
                                  removeLabel: 'ลบ',
                                  browseClass: 'btn btn-success',
                                  showUpload: false,
                                  showRemove:false,
                                  showCaption: false,// ปิดช่องแสดงชื่อรูปภาพ
                                  msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                  msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                  msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                  xxx:'seo_vdo_file',
                                  dropZoneTitle : 'VDO File',

                                  msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                  msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                  msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                                  msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                                });
                              </script>
                            </div>




                            <div class="form-group">
                              <label class="control-label col-md-8"><u>Gallery แบบบ้าน (.JPG , .PNG , .GIF )
                              ใส่ภาพน้อยสุด 5 ภาพ และสูงสุด 15 ภาพ ภาพขนาดไม่เกิน 300 KB</u>
                            </label>

                          </div>

                          <!-- image gallery -->

                          <?php
$sql = "SELECT * FROM LH_GALERY_PLAN WHERE plan_id ='" . $_GET['id'] . "' AND galery_plan_type = 'home_plan'";
$query = mssql_query($sql);
$row = mssql_num_rows($query);

if ($row > 0) {

	?>

                            <div class="row">

                             <?php $i = 0;while ($result = mssql_fetch_assoc($query)) {?>

                             <div class="col-sm-3">
                              <div class="form-group">
                               <div class="file-preview-thumbnails">
                                <div class="file-initial-thumbs">
                                  <div class="file-preview-frame file-preview-initial" data-fileindex="init_0" data-template="image">
                                    <div class="kv-file-content">
                                      <a class="fancybox" rel="group_position_project" href="<?php echo $result['galery_plan_name']; ?>">
                                        <img src="<?php echo $result['galery_plan_name']; ?>" class="kv-preview-data file-preview-image"  style="width:auto;height:160px;">
                                      </a>
                                    </div>
                                    <input type="hidden" name="id_gallery[]" value="<?=$result['galery_plan_id'];?>">
                                    <div class="file-thumbnail-footer">

                                      <input type="hidden" name="galery_plan_id[]" value="<?=$result['galery_plan_id']?>">
                                      <input type="file" id="galery_<?=$i?>" name="project_galery_edit[]" />

                                      <script>
                                        $("#galery_<?=$i?>").fileinput({
                                                  uploadUrl: "upload.php", // server upload action
                                                  maxFileCount: 1,
                                                  allowedFileExtensions: ["jpg", "png", "jpeg"],
                                                  browseLabel: 'เลือกรูป',
                                                  removeLabel: 'ลบ',
                                                  browseClass: 'btn btn-success',
                                                  showUpload: false,
                                                  showRemove:false,
                                                  showCaption: false,
                                                  msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                  msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                                                  msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                                  msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                  xxx:'upload_project_galery_seo_edit',
                                                  dropZoneTitle : 'Gallery แบบบ้าน',
                                                  maxFileSize :300 ,
                                                  msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                                  msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                                  msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                                  msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                                  msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                                });
                                              </script>

                                              <p>Alt Text : </p>
                                              <input type="type" name="galery_plan_img_seo_edit[]" class="form-control" placeholder="Alt Text" value="<?php echo $result['galery_plan_img_seo']; ?>" />

                                              <div class="file-actions">
                                                <div class="file-footer-buttons">
                                                  <button type="button" id="<?php echo $result['galery_plan_id']; ?>" onclick="deleteImageGaleryPlan2(<?php echo $result['galery_plan_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                  <a class="fancybox" href="<?=$result['galery_plan_name'];?>">
                                                    <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                  </a>
                                                </div>
                                                <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="form-group">

                                      <div class="col-md-12">
                                        <input type="text"  class="form-control"  name="project_galery_text_edit[]" placeholder="กรอกรายละเอียด Gallery ไทย " value="<?php echo $result['galery_plan_name_dis']; ?>" />
                                      </div>
                                    </div>

                                    <div class="form-group hide-for-th">

                                      <div class="col-md-12">
                                        <input type="text"  class="form-control"  name="project_galery_text_en_edit[]" placeholder="กรอกรายละเอียด Gallery อังกฤษ " value="<?php echo $result['galery_plan_name_dis_en']; ?>" />
                                      </div>
                                    </div>
                                  </div>
                                  <?php $i++;}?>

                                </div>

                                <?php }?>
                                <!-- end gallery  -->
                                <br>

                                <div class="row">
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      <label class="control-label col-md-4">
                                      </label>
                                      <div class="col-md-12">
                                        <input type="file" id="galery0" name="project_galery[]" />
                                      </div>
                                    </div>
                                    <div class="form-group">

                                      <div class="col-md-12">
                                        <input type="text"  class="form-control"  name="project_galery_text[]" placeholder="กรอกรายละเอียด Gallery ไทย "/>
                                      </div>
                                    </div>

                                    <div class="form-group hide-for-th">

                                      <div class="col-md-12">
                                        <input type="text"  class="form-control"  name="project_galery_text_en[]" placeholder="กรอกรายละเอียด Gallery อังกฤษ "/>
                                      </div>
                                    </div>
                                  </div>

                                  <div id="Project_galery"></div>


                                </div>

                                <br>
                                <div class="form-group">
                                  <label class="control-label col-md-3">
                                  </label>
                                  <div class="col-md-4">
                                    <button type="button" class="btn btn-round btn-success" onclick="addProject_galery()" >+
                                      เพิ่มรูป Gallery แบบบ้าน
                                    </button>
                                  </div>
                                </div>

                                <script>
                                  $("#galery0").fileinput({
                              uploadUrl: "upload.php", // server upload action
                              maxFileCount: 1,
                              allowedFileExtensions: ["jpg", "png", "jpeg"],
                              browseLabel: 'เลือกรูป',
                              removeLabel: 'ลบ',
                              browseClass: 'btn btn-success',
                              showUpload: false,
                              showRemove:false,
                              showCaption: false,
                              msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                              msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                              msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                              msgZoomModalHeading: 'ตัวอย่างละเอียด',
                              xxx:'upload_project_galery_seo',
                              dropZoneTitle : 'รูป Gallery แบบบ้าน',
                              maxFileSize :300 ,
                              msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                              msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                              msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                              msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                              msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                            });
                          </script>


                          <hr>
                          <br>

                          <div class="form-group">
                            <label class="control-label col-md-3">แสดงบนเว็บ
                            </label>
                            <div class="col-md-4">
                             <p class="btn-switch">
                               <input type="radio" <?=$row3['views'] == 'on' ? 'checked' : '';?> id="yes" name="switch" value="on" class="btn-switch__radio btn-switch__radio_yes" />
                               <input type="radio" <?=$row3['views'] == 'off' ? 'checked' : '';?> id="no" value="off" name="switch" class="btn-switch__radio btn-switch__radio_no" />
                               <label for="yes" class="btn-switch__label btn-switch__label_yes"><span class="btn-switch__txt">เปิด</span></label>                  <label for="no" class="btn-switch__label btn-switch__label_no"><span class="btn-switch__txt">ปิด</span></label>
                             </p>
                           </div>
                         </div>

                         <div class="form-group">
                          <label class="control-label col-md-4" for="first-name">
                          </label>
                          <div class="col-md-6">
                           <input type="hidden" id="id" value="<?=$_GET['id'];?>" name="id" >

                           <br>
                           <br>
                           <button type="submit" class="btn btn-success">Update</button>
                           <a class="confirms" data-title="ยืนยันการลบข้อมูล" name="delete" href="delete_plan.php?id=<?=$_GET['id'];?>">
                            <button type="button" class="btn btn-danger">Delete</button></a>
                          </div>
                        </div>

                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /page content -->
              <script type="text/javascript">
                $('a.confirms').confirm({
                  content: 'คุณแน่ใจที่จะลบข้อมูลนี้ !',
                  buttons: {
                    Yes: {
                      text: 'Yes',
                      btnClass: 'btn-danger',
                      keys: ['enter', 'a'],
                      action: function(){
                        location.href = this.$target.attr('href');
                      }
                    },
                    No: {
                      text: 'No',
                      btnClass: 'btn-default',
                      keys: ['enter', 'a'],
                      action: function(){
                          // button action.
                        }
                      },

                    }
                  });
                </script>

                <!-- Bootstrap -->
                <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
                <!-- FastClick -->
                <script src="../vendors/fastclick/lib/fastclick.js"></script>
                <!-- NProgress -->
                <script src="../vendors/nprogress/nprogress.js"></script>
                <!-- iCheck -->
                <script src="../vendors/iCheck/icheck.min.js"></script>
                <script type="text/javascript" src="js/bootstrap-imgupload.js"></script>
                <!-- Validate -->
                <script src="../build/js/jquery.validate.js"></script>

                <!-- Custom Theme Scripts -->
                <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>
                <!-- bootstrap-progressbar -->
                <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
                <!-- Custom Theme Scripts -->
                <script src="../build/js/custom.min.js"></script>
                <script>
                  $(function() {
                    $('#staticParent').on('keydown', '#child', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
                  });
                  $(function() {
                    $('#staticParent2').on('keydown', '#child2', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
                  });
                  $(function() {
                    $('#staticParent3').on('keydown', '#child3', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
                  });
                </script>

                <script type="text/javascript">
                  $('.fancybox').fancybox();
                </script>



                <script>
                  $(document).ready(function() {

                    $("#commentForm").validate({
                      rules: {
                        series: "required",
                        plan_code: "required",
                        plan_name_th : "required",
                        plan_name_en : "required",
                        plan_useful : "required",
                        images_plan : "required",
                        product_id : "required",
                        'model_galery[]' : "required",
                        'model_galery_radio[]' : "required",
                      },
                      messages: {
                        series: "กรุณากรอกเลือก series !",
                        plan_code : "กรุณากรอกรหัสแบบบ้าน !",
                        plan_name_th : " กรุณากรอกชื่อ แบบบ้าน ไทย !",
                        plan_name_en : "กรุณากรอกชื่อ แบบบ้าน อังกฤษ !",
                        plan_useful : " กรุณากรอก พื้นที่ใช้สอย !",
                        'model_galery[]': " &nbsp; กรุณาเลือกรูป",
                        product_id : "กรุณากรอกเลือก ประเภทบ้าน !",
                              // 'fucn[]': "กรุณากรอกข้อมูล !",
                              url_youtube : "กรุณากรอก URL Youtube !",
                              img_youtube : "กรุณาเลือกรูป !",
                              thumbnail_vdo : "กรุณาเลือกรูป !",
                              vdo_file : " &nbsp; กรุณาเลือกไฟล์ !",
                              c_360 : "กรุณากรอก URL 360 Virtual Tour !",
                              'c_360_img[]' : "กรุณาเลือกรูป !",
                              'model_galery_radio[]' : "กรุณาเลือกรูปภาพแบบ้านหลักจำนวน 1 รูปภาพ",
                            }
                          });
                    $("#f_id1").rules("add", {
                      required:true,
                      messages: {
                        required: "กรุณากรอกข้อมูล !"
                      }
                    });
                    $("#f_id2").rules("add", {
                      required:true,
                      messages: {
                        required: "กรุณากรอกข้อมูล !"
                      }
                    });
                    $("#f_id3").rules("add", {
                      required:true,
                      messages: {
                        required: "กรุณากรอกข้อมูล !"
                      }
                    });

                    $("#lead_img_file").rules("add", {
                      required:true,
                      messages: {
                        required: " &nbsp; ไม่มีรูป !"
                      }
                    });

                  });
                </script>
                <script type="text/javascript">
                  $("#commentForm").submit(function() {
                    var c_360 = $("#c_360").val();
                    var img = $("#img").val();
                    var c_img = $("#c_img").val();
                    if(c_360 != "" && img == "" ){
                      if(c_img != ""){
                       $("#img").removeAttr('required','true');
                     }else if (c_img == ""){
                      $("#img").attr('required','true');
                    }
                  }else if(img != "" && c_360 =="" ){
                    $("#c_360").attr('required','true');
                  }else if(c_360 == "" && img == ""){
                    if(c_img != "" ){
                      $("#c_360").attr('required','true');
                    }else if(c_img == ""){
                      $("#img").removeAttr('required','true');
                      $("#c_360").removeAttr('required','true');
                    }
                  }
                });
              </script>
              <script type="text/javascript">
                $("#commentForm").submit(function() {
                  var url_youtube = $("#url_youtube").val();
                  var img_youtube = $("#img_youtube").val();
                  var youtube_img = $("#youtube_img").val();
                  if(url_youtube != "" && img_youtube == "" ){
                    if(youtube_img != ""){
                     $("#img_youtube").removeAttr('required','true');
                   }else if (youtube_img == ""){
                    $("#img_youtube").attr('required','true');
                  }
                }else if(img_youtube != "" && url_youtube =="" ){
                  $("#url_youtube").attr('required','true');
                }else if(url_youtube == "" && img_youtube == ""){
                  if(youtube_img != "" ){
                    $("#url_youtube").attr('required','true');
                  }else if(youtube_img == ""){
                    $("#img_youtube").removeAttr('required','true');
                    $("#url_youtube").removeAttr('required','true');
                  }
                }
              });
            </script>
            <script type="text/javascript">
              $("#commentForm").submit(function() {
                var thumbnail_vdo = $("#thumbnail_vdo").val();
                var vdo_file = $("#vdo_file").val();
                var vdo_thumbnail = $("#vdo_thumbnail").val();
                var file_vdo = $("#file_vdo").val();
                if(thumbnail_vdo != "" && vdo_file == "" ){
                  if(file_vdo != ""){
                   $("#vdo_file").removeAttr('required','true');
                 }else if (file_vdo == ""){
                  $("#vdo_file").attr('required','true');
                }
              }else if(vdo_file != "" && thumbnail_vdo =="" ){
                if(vdo_thumbnail != ""){
                  $("#thumbnail_vdo").removeAttr('required','true');
                }else if(vdo_thumbnail == ""){
                  $("#thumbnail_vdo").attr('required','true');
                }
              }else if(thumbnail_vdo == "" && vdo_file == ""){
                if(vdo_thumbnail != "" && file_vdo == "" ){
                  $("#vdo_file").attr('required','true');
                }else if(vdo_thumbnail == "" && file_vdo != ""){
                  $("#thumbnail_vdo").attr('required','true');
                }else if(vdo_thumbnail == "" && file_vdo == "" ){
                  $("#thumbnail_vdo").removeAttr('required','true');
                  $("#vdo_file").removeAttr('required','true');
                }else if(vdo_thumbnail != "" && file_vdo != "" ){
                  $("#thumbnail_vdo").removeAttr('required','true');
                  $("#vdo_file").removeAttr('required','true');
                }
              }
            });
          </script>

          <script>
            function isNumber(evt) {
              evt = (evt) ? evt : window.event;
              var charCode = (evt.which) ? evt.which : evt.keyCode;
              if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
              }
              return true;
            }
          </script>

          <script>
            $(document).ready(function(){
              $("#v1").<?=$type?>
              $("#v2").<?=$type2?>
            });
//      $('input:radio[name="optradio_vdo"]').change(function() {
//          if ($(this).val() == 'youtube') {
//              $("#v1").show();
//              $("#v2").hide();
//          } else {
//              $("#v1").hide();
//              $("#v2").show();
//          }
//      });

$("#youtube").on('ifChecked', function(event){
  $("#v1").show();
  $("#v2").hide();
});
$("#file").on('ifChecked', function(event){
  $("#v1").hide();
  $("#v2").show();
});

</script>

<script>
                  //galery
                  function addProject_galery()
                  {
                    var index = $('.setIndexProject_galery').length;

                    var form = `<div class="col-sm-3">
                    <div class="form-group setIndexProject_galery">
                    <label class="control-label col-md-4">
                    </label>
                    <div class="col-md-12">
                    <input type="file" id="galery${index + 1}" name="project_galery[]" />
                    </div>
                    </div>
                    <div class="form-group setIndexProject_galery">

                    <div class="col-md-12">
                    <input type="text"  class="form-control"  name="project_galery_text[]" placeholder="กรอกรายละเอียด Gallery ไทย "/>
                    </div>
                    </div>

                    <div class="form-group setIndexProject_galery hide-for-th">

                    <div class="col-md-12">
                    <input type="text"  class="form-control"  name="project_galery_text_en[]" placeholder="กรอกรายละเอียด Gallery อังกฤษ "/>
                    </div>
                    </div>
                    <div class="form-group setIndexProject_galery">
                    <button type="button" class="btn btn-round btn-danger" onclick="removeProject_galery(this)">
                    - ลบ Gallery
                    </button>
                    </div>
                    </div>
                    <div>`;


                    $('#Project_galery').append(form);

                    $("#galery"+(index + 1)).fileinput({
                          uploadUrl: "upload.php", // server upload action
                          maxFileCount: 1,
                          allowedFileExtensions: ["jpg", "png", "jpeg"],
                          browseLabel: 'เลือกรูป',
                          removeLabel: 'ลบ',
                          browseClass: 'btn btn-success',
                          showUpload: false,
                          showRemove:false,
                          showCaption: false,
                          msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                          msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                          msgZoomModalHeading: 'ตัวอย่างละเอียด',
                          xxx:'upload_project_galery_seo',
                          dropZoneTitle : 'รูป Gallery แบบบ้าน',
                          maxFileSize :300 ,
                          msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                          msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                          msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                          msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                          msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                        });
                  }

                  function removeProject_galery(element)
                  {
                    $(element).parent().parent().remove();
                  }


                  //Model
                  function addModel_galery()
                  {
                    var index = $('.setIndexProject_galery').length;
                    var num = $('.num_model').length;

                    var form = `<div class="col-sm-3">
                    <div class="form-group setIndexProject_galery">
                    <label class="control-label col-md-4">
                    </label>
                    <div class="col-md-12">
                    <input type="file" id="galery_model_update${index + 1}" name="model_galery[]" />
                    </div>
                    </div>
                    <div class="form-group num_model">
                    <div class="col-md-2">
                    <input type="radio" class="flat" id="add${num + 1}" value="${num + 1}" name="model_galery_radio[]" >
                    </div>
                    <label for="add${num + 1}">
                    เลือกเป็นภาพหลัก
                    </label>
                    </div>

                    <div class="form-group setIndexProject_galery">
                    <button type="button" class="btn btn-round btn-danger" onclick="removeModel_galery(this)">
                    - ลบรูปภาพ
                    </button>
                    </div>
                    </div>
                    <div>`;


                    $('#Model_galery').append(form);

                    $("#galery_model_update"+(index + 1)).fileinput({
                          uploadUrl: "upload.php", // server upload action
                          maxFileCount: 1,
                          allowedFileExtensions: ["jpg", "png", "jpeg"],
                          browseLabel: 'เลือกรูป',
                          removeLabel: 'ลบ',
                          browseClass: 'btn btn-success',
                          showUpload: false,
                          showRemove:false,
                          showCaption: false,
                           minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                           minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                           maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                           maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                           msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                           msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                           msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                           msgZoomModalHeading: 'ตัวอย่างละเอียด',
                           xxx:'upload_model_galery_seo',
                           dropZoneTitle : 'รูปภาพแบบบ้าน',
                           maxFileSize :300 ,
                           msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                           msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                           msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                           msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                           msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                         });
                  }

                  function removeModel_galery(element)
                  {
                    $(element).parent().parent().remove();
                  }

                </script>


                <script>
                  function deleteImageGaleryPlan(id) {
                    $.confirm({
                      title: 'ลบรูปนี้ !',
                      content: 'คุณแน่ใจที่จะลบรูปนี้!',
                      buttons : {
                        Yes: {
                          text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                              var self = this;
                              return $.ajax({
                                url: 'ajax_deleteImageGaleryPlan.php?galery_plan_img_id=' + id,
                                dataType: 'json',
                                method: 'get'
                              }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                  }).fail(function () {
                                    self.setContent('Something went wrong.');
                                  });
                                }
                              },
                              No: function () {
                            //$.alert('ยกลเิก!');
                          },
                        }
                      });
                  }
                </script>
                <script>
                  function deleteImageGaleryPlan2(id) {
                    $.confirm({
                      title: 'ลบรูปนี้ !',
                      content: 'คุณแน่ใจที่จะลบรูปนี้!',
                      buttons : {
                        Yes: {
                          text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                              var self = this;
                              return $.ajax({
                                url: 'ajax_deleteImageGaleryPlan.php?galery_plan_img_id=' + id,
                                dataType: 'json',
                                method: 'get'
                              }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                  }).fail(function () {
                                    self.setContent('Something went wrong.');
                                  });
                                }
                              },
                              No: function () {
                            //$.alert('ยกลเิก!');
                          },
                        }
                      });
                  }

            //deleteImageModelPlan2
            function deleteImageModelPlan2(id) {
              $.confirm({
                title: 'ลบรูปนี้ !',
                content: 'คุณแน่ใจที่จะลบรูปนี้!',
                buttons : {
                  Yes: {
                    text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                              var self = this;
                              return $.ajax({
                                url: 'ajax_deleteImageModelPlan.php?id=' + id,
                                dataType: 'json',
                                method: 'get'
                              }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                  }).fail(function () {
                                    self.setContent('Something went wrong.');
                                  });
                                }
                              },
                              No: function () {
                            //$.alert('ยกลเิก!');
                          },
                        }
                      });
            }
          </script>
          <script>
            function deleteImagePlan(id) {
              $.confirm({
                title: 'ลบรูปนี้ !',
                content: 'คุณแน่ใจที่จะลบรูปนี้!',
                buttons : {
                  Yes: {
                    text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                              var self = this;
                              return $.ajax({
                                url: 'ajax_deleteImagePlan.php?plan_id=' + id,
                                dataType: 'json',
                                method: 'get'
                              }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                  }).fail(function () {
                                    self.setContent('Something went wrong.');
                                  });
                                }
                              },
                              No: function () {
                            //$.alert('ยกลเิก!');
                          },
                        }
                      });
            }
          </script>
          <script>
            function deleteImageFloorPlan(id) {
              $.confirm({
                title: 'ลบรูปนี้ !',
                content: 'คุณแน่ใจที่จะลบรูปนี้!',
                buttons : {
                  Yes: {
                    text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                              var self = this;
                              return $.ajax({
                                url: 'ajax_deleteImageFloorPlan.php?floor_plan_img_id=' + id,
                                dataType: 'json',
                                method: 'get'
                              }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                  }).fail(function () {
                                    self.setContent('Something went wrong.');
                                  });
                                }
                              },
                              No: function () {
                            //$.alert('ยกลเิก!');
                          },
                        }
                      });
            }
          </script>
          <script>
            function delete360Plan(id) {
              $.confirm({
                title: 'ลบรูปนี้ !',
                content: 'คุณแน่ใจที่จะลบรูปนี้!',
                buttons : {
                  Yes: {
                    text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                              var self = this;
                              return $.ajax({
                                url: 'ajax_delete360Plan.php?c360_plan_id=' + id,
                                dataType: 'json',
                                method: 'get'
                              }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                  }).fail(function () {
                                    self.setContent('Something went wrong.');
                                  });
                                }
                              },
                              No: function () {
                            //$.alert('ยกลเิก!');
                          },
                        }
                      });
            }
          </script>
          <script>
            function deleteImageThumbnailPlan(id) {
              $.confirm({
                title: 'ลบรูปนี้ !',
                content: 'คุณแน่ใจที่จะลบรูปนี้!',
                buttons : {
                  Yes: {
                    text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                              var self = this;
                              return $.ajax({
                                url: 'ajax_deleteImageThumbnailPlan.php?clip_plan_id=' + id,
                                dataType: 'json',
                                method: 'get'
                              }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                  }).fail(function () {
                                    self.setContent('Something went wrong.');
                                  });
                                }
                              },
                              No: function () {
                            //$.alert('ยกลเิก!');
                          },
                        }
                      });
            }
          </script>
          <script>
            function deleteVDOPlan(id) {
              $.confirm({
                title: 'ลบรูปนี้ !',
                content: 'คุณแน่ใจที่จะลบรูปนี้!',
                buttons : {
                  Yes: {
                    text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                              var self = this;
                              return $.ajax({
                                url: 'ajax_deleteVDOPlan.php?clip_plan_id=' + id,
                                dataType: 'json',
                                method: 'get'
                              }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                  }).fail(function () {
                                    self.setContent('Something went wrong.');
                                  });
                                }
                              },
                              No: function () {
                            //$.alert('ยกลเิก!');
                          },
                        }
                      });
            }
          </script>
          <script src="js/validate_file_300kb.js"></script>
        </body>
        </html>