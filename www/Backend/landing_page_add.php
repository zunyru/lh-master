<?php
session_start();
include './include/dbCon_mssql.php';
$group_id = $_SESSION['group_id'];

$_GET['page'] = 'managment';

if (isset($_SESSION['add'])) {
	if ($_SESSION['add'] == 1) {
		//header("location:product_add.php");
		$success = "success";
		unset($_SESSION["add"]);
	} else if ($_SESSION['add'] == 2) {

		$error_name = "error_name"; //ค่าซ้ำ
		unset($_SESSION["add"]);

	} else if ($_SESSION['add'] == -1) {
		$error_img = "errors_img";
		unset($_SESSION["add"]);
	} else if ($_SESSION['add'] == -2) {
		$error_up = "error_up";
		unset($_SESSION["add"]);
	}
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>


    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <!-- Fancybox image popup -->
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
    <!-- bootstrap-progressbar -->
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>

    <!-- uploade img -->
    <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
    <!--uploade-->
    <!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
    <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
    <!-- confirm-->
    <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
    <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>

    <!-- Include Editor style. -->
    <link href="editor/css/froala_editor.min.css" rel="stylesheet" type="text/css" />
    <link href="editor/css/froala_style.min.css" rel="stylesheet" type="text/css" />

    <!--  froala  -->
    <link href="libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="libs/codemirror/5.3.0/codemirror.min.css">
    <link href="libs/froala-editor/2.6.2/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
    <link href="libs/froala-editor/2.6.2/css/froala_style.min.css" rel="stylesheet" type="text/css" />

    <script id="fr-fek">try{(function (k){localStorage.FEK=k;t=document.getElementById('fr-fek');t.parentNode.removeChild(t);})('inkH3fiA6B-13a==')}catch(e){}</script>

    <!-- Include Editor Plugins style. -->
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/char_counter.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/code_view.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/colors.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/emoticons.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/file.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/fullscreen.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/image.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/image_manager.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/line_breaker.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/quick_insert.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/table.css">
    <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/video.css">


    <script id="fr-fek">
        try {(function (k)
            {localStorage.FEK=k;t=document.getElementById('fr-fek');t.parentNode.removeChild(t);})
        ('inkH3fiA6B-13a==')}catch(e){}
    </script>
    <style type="text/css">
    .fr-toolbar.fr-desktop.fr-top.fr-basic.fr-sticky.fr-sticky-off {
        background: #f7f7f7  !important;
        border-top: 5px solid #f5f5f5  !important;
    }
    th.next.available {
        background: #6f9755;
    }
    th.next.available:hover {
        background: #9ab688;
    }
    th.prev.available {
        background: #6f9755;
    }
    th.prev.available:hover {
        background: #9ab688;
    }
    .thumbnail {
        height: 100px;
        margin: 5px;
    }
    .fileUpload {
        position: relative;
        overflow: hidden;
        //margin: 10px;
    }
    .fileUpload input.upload {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
    .file-drop-zone-title {
        padding: 0px 0px !important;
    }
    .form-control[readonly] { /* For Firefox */
        background-color: white;
    }

    .form-control[readonly] {
        background-color: white;
    }

    .file-caption-main .btn-file {
        overflow: visible;
    }

    .file-caption-main .btn-file .error {
        position: absolute;
        bottom: -32px;
        right: 30px;
    }
</style>
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
                    </div>

                    <div class="clearfix"></div>

                    <!-- menu profile quick info -->
                    <?php include './master/navbar.php';?>
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <?php include './master/top_nav.php';?>
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2><a href="landing_page.php">ระบบจัดการข้อมูล Landing Page</a> > <a href="landing_page.php">สร้าง Landing Page</a></h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <?php if (isset($success)) {?>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="form-group">
                                                <div class="alert alert-success col-md-6" id="alert">
                                                    <button type="button" class="close" data-dismiss="alert">x</button>
                                                    <strong>สร้างข้อมูลเรียบร้อยแล้ว</strong>
                                                </div>
                                            </div>
                                            <div class="col-md-6"></div>
                                        </div>
                                    <?php } else if (isset($error_name)) {?>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="form-group">
                                                <div class="alert alert-danger col-md-6" id="alert">
                                                    <button type="button" class="close" data-dismiss="alert">x</button>
                                                    <strong>มีชื่อนี้อยู่ในระบบแล้ว </strong>
                                                </div>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    <?php } else if (isset($errors)) {?>

                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="form-group">
                                                <div class="alert alert-danger col-md-6" id="alert">
                                                    <button type="button" class="close" data-dismiss="alert">x</button>
                                                    <strong>ลบข้อมูลเรียบร้อยแล้ว !</strong>
                                                </div>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    <?php } else if (isset($update)) {?>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="form-group">
                                                <div class="alert alert-success col-md-6" id="alert">
                                                    <button type="button" class="close" data-dismiss="alert">x</button>
                                                    <strong>แก้ไขข้อมูลเรียบร้อยแล้ว</strong>
                                                </div>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    <?php } else if (isset($error_up)) {?>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="form-group">
                                                <div class="alert alert-danger col-md-6" id="alert">
                                                    <button type="button" class="close" data-dismiss="alert">x</button>
                                                    <strong>เกิดข้อผิดพลาด !</strong> ไม่สามารถบันทึกข้อมูลได้
                                                </div>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    <?php }?>
                                    <p class="font-gray-dark">
                                    </p><br>

                                    <form class="form-horizontal form-label-left" action="save_landing_page.php" method="POST" id="commentForm"  enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label class="control-label col-md-2" for="first-name">ชื่อเพจกิจกรรม (ไทย)
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6">
                                                <input type="text" id="first-name1" required class="form-control col-md-7 col-xs-12" value="" name="landing_page_name_th" placeholder="กรอกชื่อเพจกิจกรรม">
                                            </div>
                                        </div>
                                        <!-- SEO -->
                                        <div class="form-group">
                                            <label class="control-label col-md-2" for="first-name">Titlt SEO (ไทย)<br>(กรอกได้ไม่เกิน 160 คำ)
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6">
                                                <input type="text"  required class="form-control col-md-7 col-xs-12" value="" name="landing_page_title_seo_th" placeholder="กรอก Titlt SEO" maxlength="160">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2" for="first-name">H1<br>(กรอกได้ไม่เกิน 255 คำ)
                                            </label>
                                            <div class="col-md-6">
                                                <input type="text"  class="form-control col-md-7 col-xs-12" value="" name="h1" placeholder="กรอก h1" maxlength="255">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2" for="first-name">Description SEO (ไทย) <br>(กรอกได้ไม่เกิน 300 คำ)
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6">
                                                <textarea required class="form-control col-md-7 col-xs-12" value="" name="landing_page_description_th" placeholder="กรอก Description SEO" rows="4" maxlength="300"></textarea> 
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2" for="first-name">Keyword SEO (ไทย)
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6">
                                                <textarea required class="form-control col-md-7 col-xs-12" value="" name="keyword_seo" placeholder="กรอก keyword SEO" rows="4"></textarea> 
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-2" for="first-name">Thumnail SEO <br>นามสกุล .jpg .jpeg<br> ขนาด 600 x 338 px <br> ภาพขนาดไม่เกิน 300 KB <span class="required">*</span>
                                            </label><br>
                                            <div class="col-md-6">
                                              <input id="fileupload_seo" name="fileupload_th" type="file" multiple class="file-loading" accept="image/*" >
                                          </div>
                                      </div>

                                      <script>
                                        $("#fileupload_seo").fileinput({
                                          uploadUrl: "upload.php", // server upload action
                                          maxFileCount: 1,
                                          allowedFileExtensions: ["jpg", "jpeg"],
                                          browseLabel: 'เลือกรูป',
                                          removeLabel: 'ลบ',
                                          browseClass: 'btn btn-success',
                                          showUpload: false,
                                          showRemove:false,
                                          showCaption: false, // ปิดช่องแสดงชื่อไฟล์
                                          msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                         minImageWidth: 600, //ขนาด กว้าง ต่ำสุด
                                         minImageHeight: 338, //ขนาด ศุง ต่ำสุด
                                         maxImageWidth: 600, //ขนาด กว้าง ต่ำสุด
                                         maxImageHeight: 338, //ขนาด ศุง ต่ำสุด
                                         msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                         msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                         xxx:'brand_img_seo_th',
                                         dropZoneTitle : 'รูป Thumnail SEO',
                                         maxFileSize :300 ,
                                         msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                         msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                         msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                         msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                         msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',   
                                     });
                                 </script>


                                 <div class="form-group hide-for-th">
                                    <label class="control-label col-md-2" for="first-name">ชื่อเพจกิจกรรม (อังกฤษ)
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text" id="first-name2"  class="form-control col-md-7 col-xs-12" value="" name="landing_page_name_en" placeholder="กรอกชื่อเพจกิจกรรม">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">สร้างหน้ากิจกรรม </label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <br>
                                        <!-- <div id="edit" class="text-left txt-black fr-box fr-basic fr-top" style="z-index: 1000;"></div>  -->
                                        <textarea name="landing_page_content_th" id="edit" rows="15"></textarea>
                                        <br>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-md-2" for="first-name">กำหนด URL พิเศษ <span class="required"></span>
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text" id="first-name"  class="form-control col-md-7 col-xs-12" value="" name="landing_page_url" placeholder="ชื่อ URL" >
                                    </div>

                                </div>
                                <br>
                                <div class="form-group">
                                    <label class="control-label col-md-2" for="first-name">
                                    </label>
                                    <div class="col-md-6">

                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--   froala JS     -->

    <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
    <script type="text/javascript" src="libs/codemirror/5.3.0/codemirror.min.js"></script>
    <script type="text/javascript" src="libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/froala_editor.pkgd.min.js"></script>

    <!-- Include Plugins. -->
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/align.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/char_counter.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/code_beautifier.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/code_view.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/colors.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/emoticons.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/entities.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/file.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/font_family.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/font_size.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/fullscreen.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/image.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/image_manager.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/inline_style.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/line_breaker.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/link.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/lists.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/paragraph_format.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/paragraph_style.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/quick_insert.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/quote.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/table.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/save.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/url.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/video.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/word_paste.min.js"></script>

    <!-- New -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.9.0/js/plugins/colors.min.js"></script>





    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="js/bootstrap-imgupload.js"></script>
    <!-- Validate -->
    <script src="../build/js/jquery.validate.js"></script>

    <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>

    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

    <!-- date -->
    <link rel="stylesheet" type="text/css" href="../build/css/jquery-ui.css"/>
    <script src="../build/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

    <!-- Validate -->
    <script src="../build/js/jquery.validate.js"></script>

    <script>
        $(document).ready(function() {
           $.validator.setDefaults({ ignore: "[contenteditable='true']" });
           $("#commentForm").validate({
            rules: {
                landing_page_name_th: "required",
                //landing_page_name_en: "required",
                landing_page_title_seo_th: {required: true,maxlength: 160},
                landing_page_description_th:{required: true,maxlength: 300},
                project_id: "required",
                landing_page_date : "required",
                fileupload_th: "required",
                keyword_seo: "required",
            },
            messages: {
                landing_page_description_th:{
                    required: "กรุณากรอก Description SEO",
                    maxlength:  "กรุณากรอก ไม่เกิน 300 ตัวอักษร !"
                },
                landing_page_title_seo_th:{
                    required: "กรุณากรอก Title SEO",
                    maxlength:  "กรุณากรอก ไม่เกิน 160 ตัวอักษร !"
                },
                keyword_seo : "กรุณากรอก Keyword SEO ",
                landing_page_title_seo_th:"กรุณากรอก Title SEO",
                landing_page_name_th: "กรุณากรอกชื่อเพจกิจกรรม ไทย !",
                landing_page_name_en : "กรุณากรอกชื่อเพจกิจกรรม อังกฤษ !",
                project_id : "กรุณาเลือกโครงการ",
                landing_page_date : "กรุณาระบุวันที่ ",
                fileupload_th : " &nbsp; กรุณาเลือกรูป !",
                h1 : {
                   maxlength:  "กรุณากรอก ไม่เกิน 255 ตัวอักษร !"
               },
           }
       });
       });
   </script>

   <script>
    $(function() {
        $.FroalaEditor.DefineIcon("imageInfo", { NAME: "info" });
        $.FroalaEditor.RegisterCommand("imageInfo", {
            title: "Info",
            focus: false,
            undo: false,
            refreshAfterCallback: false,
            callback: function() {
                var $img = this.image.get();
                alert($img.attr("src"));
            }
        });

        $('#edit').froalaEditor({

            height: 500,
            imageUploadURL: 'uploade_highlights.php',
            imageUploadParams: {
                id: 'edit'
            },
            imageManagerLoadURL: 'images_load.php',
                    // Set max image size to 300kB.
                    imageMaxSize: 0.3 * 1024 * 1024,

                    fileUploadURL: 'upload_file.php',
                    fileUploadParams: {
                        id: 'edit'
                    },
                    videoUploadURL: 'upload_video.php',
                    videoUploadParams: {
                        id: 'edit'
                    },
                    fontFamily: {
                        "LHfont": 'Kittithada',
                        "Roboto,sans-serif": 'Roboto',
                        "Oswald,sans-serif": 'Oswald',
                        "Montserrat,sans-serif": 'Montserrat',
                        "'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
                    },

                    imageManagerDeleteURL: "delete_image_highlights.php",
                    imageManagerDeleteMethod: "POST"
                })
                // Catch image removal from the editor.
                .on('froalaEditor.image.removed', function (e, editor, $img) {
                    $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_image_highlights.php",

                            // Request params.
                            data: {
                                src: $img.attr('src')
                            }
                        })
                    .done (function (data) {
                        console.log ('image was deleted'+$img.attr('src'));
                    })
                    .fail (function (err) {
                        console.log ('image delete problem: ' + JSON.stringify(err));
                    })
                })

                    // Catch image removal from the editor.
                    .on('froalaEditor.file.unlink', function (e, editor, link) {

                        $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_file.php",

                            // Request params.
                            data: {
                                src: link.getAttribute('href')
                            }
                        })
                        .done (function (data) {
                            console.log ('file was deleted');
                        })
                        .fail (function (err) {
                            console.log ('file delete problem: ' + JSON.stringify(err));
                        })
                    });

                //
                $('#edit_en').froalaEditor({

                    height: 500,
                    imageUploadURL: 'uploade_highlights.php',
                    imageUploadParams: {
                        id: 'edit'
                    },
                    imageManagerLoadURL: 'images_load.php',
                    // Set max image size to 300kB.
                    imageMaxSize: 0.3 * 1024 * 1024,

                    fileUploadURL: 'upload_file.php',
                    fileUploadParams: {
                        id: 'edit'
                    },
                    fontSize: ["8", "9", "10", "11", "12", "14", "18","20","22","24", "30", "36", "48", "60", "72", "96"],
                    fontFamily: {
                        "LHfont": 'Kittithada',
                        "Roboto,sans-serif": 'Roboto',
                        "Oswald,sans-serif": 'Oswald',
                        "Montserrat,sans-serif": 'Montserrat',
                        "'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
                    },

                    imageManagerDeleteURL: "delete_image_highlights.php",
                    imageManagerDeleteMethod: "POST"
                })
                // Catch image removal from the editor.
                .on('froalaEditor.image.removed', function (e, editor, $img) {
                    $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_image_highlights.php",

                            // Request params.
                            data: {
                                src: $img.attr('src')
                            }
                        })
                    .done (function (data) {
                        console.log ('image was deleted'+$img.attr('src'));
                    })
                    .fail (function (err) {
                        console.log ('image delete problem: ' + JSON.stringify(err));
                    })
                })

                    // Catch image removal from the editor.
                    .on('froalaEditor.file.unlink', function (e, editor, link) {

                        $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_file.php",

                            // Request params.
                            data: {
                                src: link.getAttribute('href')
                            }
                        })
                        .done (function (data) {
                            console.log ('file was deleted');
                        })
                        .fail (function (err) {
                            console.log ('file delete problem: ' + JSON.stringify(err));
                        })
                    });
                });

            </script>


            <script>
                $(document).ready(function(){
                    $("#alert").show();
                    $("#alert").fadeTo(3000, 500).slideUp(500, function(){
                        $("#alert").alert('close');
                    });
                });
            </script>

            <script>
                $(document).ready(function(){
                    $("#l1").show();
                    $("#l2").hide();
                    $("#l3").hide();
                    $("#error_").hide();
                });
            </script>

            <script>

                function getval(sel){
                    $("#mySelect").val();
                    if($("#mySelect").val() == "image"){
                        $("#l1").show();
                        $("#l2").hide();
                        $("#l3").hide();
                    }else if($("#mySelect").val() == "banner"){
                        $("#l2").show();
                        $("#l3").hide();
                        $("#l1").hide();
                    }else if($("#mySelect").val() == "vdo"){
                        $("#l3").show();
                        $("#l1").hide();
                        $("#l2").hide();
                    }
                }
            </script>

            <script type="text/javascript">

                $('#date_start').datepicker({
                    dateFormat : "dd/mm/yy",
                    autoclose: true,
                    changeMonth: true,
                    showDropdowns: true,
                    changeYear: true,
                    minDate:  new Date(),
                    onSelect: function(dateText) {
                        var d = new Date($(this).datepicker("getDate"));

                    }
                });

            </script>

        </body>
        </html>
