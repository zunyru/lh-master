<?php
session_start();
header( 'Content-Type:text/html; charset=utf8');
if(empty($_SESSION['group_status'])){
    header("location:login.php?errors=e");
}
include './include/dbCon_mssql.php';
$_GET['page']='form';

$_SESSION['group_id'];
if (isset($_GET['project_id'])) {
    $sql="SELECT * FROM LH_PROJECTS WHERE project_id ='".$_GET['project_id']."' ";
    $query = mssql_query($sql);
    $row = mssql_fetch_assoc($query);
}

if (isset($_SESSION['add'])) {
    if($_SESSION['add'] ==0){

        $error="error"; //ค่าซ้ำ
        $_SESSION['add']='';

    }
    unset($_SESSION["add"]);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <!-- Fancybox image popup -->
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
    <!-- bootstrap-progressbar -->
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <script src="../build/js/custom.min.js"></script>      <script src="../build/js/custom.min.js"></script>
    <!-- uploade img -->
    <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
    <!--uploade-->
    <!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
    <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
    <!-- confirm-->
    <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
    <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>
    <style>
        .showposition {
            font-size:36 px;
            color:#00ccdd;
        }
    </style>
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="index.html" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
                </div>

                <div class="clearfix"></div>

                <?php include "master/navbar.php"; ?>

            </div>
        </div>

        <!-- top navigation -->
        <?php include './master/top_nav.php'; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <a href="homesales.php?project_id=<?=$_GET['project_id'];?>"><h2>สร้างข้อมูลบ้านพร้อมขาย โครงการ : </a><?=$row['project_name_th']." (".$row['project_name_en'].")"; ?></h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <p class="font-gray-dark">
                                </p><br>
                                <?php if(isset($error)){?>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="form-group">
                                            <div class="alert alert-danger col-md-6" id="alert">
                                                <button type="button" class="close" data-dismiss="alert">x</button>
                                                <strong>มีหมายเลขแปลงซ้ำในระบบ</strong>
                                            </div>
                                        </div>
                                        <div class="col-md-6"></div>
                                    </div>
                                <?php } ?>
                                <?php
                                $sql_plan = "SELECT * FROM LH_PLANS ORDER BY plan_name_th ASC";
                                $query_paln = mssql_query($sql_plan);

                                ?>
                                <form class="form-horizontal form-label-left" action="save_home_sell.php" method="POST" enctype="multipart/form-data" id="form_home_sell" onsubmit="return validateForm()">
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="brand">เลือกชื่อแบบบ้าน <span class="required">*</span>
                                        </label>
                                        <div class="col-md-4 col-sm-9 col-xs-12">
                                            <select class="form-control" id="showPlan" name="plan_id">
                                                <option value="">เลือกแบบบ้าน</option>
                                                <?php while ($row_paln = mssql_fetch_assoc($query_paln)){?>
                                                    <option value="<?=$row_paln['plan_id']?>"><?=$row_paln['plan_name_th']." (".$row_paln['plan_name_en'].")";?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">หมายเลขแปลง <span class="required"></span>
                                        </label>
                                        <div class="col-md-3">
                                            <input type="text" name="number_converter"  class="form-control col-md-7 col-xs-12" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">ระบุลักษณะเด่นของแปลง <span class="required">*
                                            <br>
                                            <span style="font-size: 15px;">(ไม่เกิน 250 ตัวอักษร)</span>
                                        </span>
                                        </label>
                                        <div class="col-md-4">
                                            <textarea maxlength="250"  rows="3" name="features_convert"  required class="form-control col-md-7 col-xs-12" ></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">สร้างบนพื้นที่ตั้งแต่ <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3">
                                            <input type="text" name="built_on" id="built_on"  required class="form-control col-md-7 col-xs-12"  >
                                        </div><label class="control-label col-md-1" for="first-name">ตารางวา</label>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="brand">พื้นที่ใช้สอย <span class="required"></span>
                                        </label>
                                        <div class="col-md-2 col-sm-9 col-xs-12">
                                            <input id="use_full" class="form-control" value="" disabled>
                                        </div><label class="control-label col-md-1" for="first-name">ตารางเมตร</label>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="brand">ระบุไอคอนกิจกรรม (ถ้ามี)
                                        </label>
                                        <div class="col-md-3 col-sm-9 col-xs-12">
                                            <?php
                                            $sql="SELECT * FROM LH_ICON_ACTIVITY ORDER BY icon_id ASC ";
                                            $query =mssql_query($sql);

                                            ?>
                                            <select class="form-control" name="icon" id="showIcon">
                                                <option value="">เลือกไอคอน</option>
                                                <?php  while ($row=mssql_fetch_array($query)){ ?>
                                                    <option value="<?=$row['icon_id']?>"><?=$row['icon_name']?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>

                                    <div id="icon"></div>

                                    <div id="room"></div>

                                    <br>

                                    <div id="func"></div>
                                    <h4>ระบุรายละเอียดเพิ่มเติม (ถ้ามี) </h4>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">
                                        </label>
                                        <div class="col-md-6">
                                            <p style="padding: 5px;">
                                                <?php
                                                $sql_p = "SELECT * FROM LH_PECULIARITY ORDER BY peculiarity_name_th ASC";
                                                $query_p = mssql_query($sql_p);
                                                while ($row_p=mssql_fetch_array($query_p)){
                                                    ?>
                                                    <input type="checkbox" name="pecul[]"  value="<?=$row_p['peculiarity_id'] ?>" class="flat" /> <?=$row_p['peculiarity_name_th'] ?>
                                                    <br />
                                                <?php } ?>
                                            <p>
                                        </div>

                                    </div>
                                    <br>



                                    <div id="series"></div>

                                    <div id="plan"></div>

                                    <div id="plan_model"></div>
                                    <div class="form-group">
                                        <div class="col-md-3">
                                        </div>
                                       <label id="features_convert-error" class="error col-md-9" for="list_model_image[]"></label>
                                    </div>

                                    <div id="foot"></div>

                                    <hr>

                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">ถ้าเป็นบ้านพร้อมตกแต่ง (หากไม่มีรุปกรุณา ติกออก)
                                        </label>
                                        <div class="col-md-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input id="id" name="home" value="" type="hidden"  >
                                                    <input id="home" name="home_sell" value="Y" type="checkbox" class="" > Yes
                                                </label></div>
                                        </div>
                                    </div>
                                    <br>

                                    <div id="check_home">
                                        <div class="form-group" id="inline_content">
                                            <label class="control-label col-md-2" name="tvc_detail" for="first-name">Gallery บ้านพร้อมขาย <span class="required"></span>
                                            </label>
                                            <div class="col-md-6">
                                                <div class="radio">
                                                    <label><input id="up" type="radio" name="optradio_vdo" <?=!empty($checked_2)?$checked_2:'' ;?> checked value="vdo"  class=""> อัปโหลดรูปเอง</label>
                                                </div>
                                                <div class="radio">
                                                    <label><input id="add" type="radio" name="optradio_vdo" <?=!empty($checked)?$checked:'' ?>  value="youtube"  class=""> รูปจากส่วนกลาง (Gallery แบบบ้าน)</label>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div id="master">
                                        <div class="form-group">
                                            <label class="control-label col-md-2" for="brand">รูป Gallery <span class="required"></span>
                                            </label>
                                            <?php
                                            $sql_gallery = "SELECT f.galery_funished_main_id,f.folder_name FROM LH_GALERY_FURNISHED_MAIN f
                                                                WHERE f.project_id =  '".$_GET['project_id']."'";
                                            $query_gallery = mssql_query($sql_gallery);
                                            ?>
                                            <div class="col-md-4 col-sm-9 col-xs-12">
                                                <select name="gallery_master" id="showgallery_master" class="form-control">
                                                    <option value="">เลือก Folder</option>
                                                    <?php
                                                    while ($row_gallery = mssql_fetch_array($query_gallery)){
                                                        ?>

                                                        <option value="<?=$row_gallery['galery_funished_main_id']?>"><?=$row_gallery['folder_name'];?></option>
                                                    <?php }  ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="gallery_master"></div>

                                    <div id="galery_img"></div>

                                    <div id="galery_img2">
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <input type="file" id="galery0" name="project_galery[]" accept="image/*"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <input type="text"  class="form-control"  name="project_galery_text[]" placeholder="กรอกรายละเอียด Gallery (ไทย)"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <input type="text"  class="form-control"  name="project_galery_text_en[]" placeholder="กรอกรายละเอียด Gallery (อังกฤษ)"/>
                                                    </div>
                                                </div>
                                            </div>

                                        <div id="Project_galery"></div>
                                        </div>


                                        <br>
                                        <div class="form-group">
                                            <label class="control-label col-md-2">
                                            </label>
                                            <div class="col-md-4">
                                                <button id="add_gallery" type="button" class="btn btn-round btn-success" onclick="addProject_galery()" >+
                                                    เพิ่มรูป Galery
                                                </button>
                                            </div>
                                        </div>

                                        <script>
                                            $("#galery0").fileinput({
                                                uploadUrl: "upload.php", // server upload action
                                                maxFileCount: 1,
                                                allowedFileExtensions: ["jpg", "png", "jpeg"],
                                                browseLabel: ' เลือก Gallery บ้านพร้อมขาย',
                                                browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                                                removeLabel: 'ลบ',
                                                browseClass: 'btn btn-success',
                                                showUpload: false,
                                                showRemove:false,
                                                showCaption: false,
                                                msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                                msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                xxx:'upload_plan_galery_seo',
                                                dropZoneTitle : 'Gallery',
                                                maxFileSize :300 ,
                                                msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                                msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                                msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                                msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                                msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                            });
                                        </script>
                                    </div>

                                    <hr>
                                    <br>
                                    <h4>ตารางราคาและเงื่อนไขการผ่อน</h4>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">ขนาดที่ดิน <span class="required">*</span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" id="land_size" name="land_size"  required class="form-control col-md-7 col-xs-12" min="1">
                                        </div><label class="control-label col-md-1 textleft" for="first-name">ตารางวา
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">ราคารวมสุทธิ <span class="required">*</span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" id="total_price" required name="total_price" class="form-control col-md-7 col-xs-12" min="1" >
                                        </div><label class="control-label col-md-1 textleft" for="first-name">บาท </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">ดาวน์
                                        </label>
                                        <div class="col-md-2">
                                            <input type="text" id="down"  name="down" class="form-control col-md-7 col-xs-12" placeholder="" min="0.1" max="100" placeholder="" onkeypress="return isNumber(event)">
                                        </div>
                                        <label class="control-label col-md-1 textleft" for="first-name"> % เป็นเงิน
                                        </label>
                                        <div class="col-md-2">
                                            <input type="text" id="money"  name="money"   class="form-control col-md-7 col-xs-12" placeholder="" min="1">
                                        </div><label class="control-label col-md-1 textleft" for="first-name"> บาท<span class="required"></span>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">เงินจอง
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" id="payments"  name="payments"   class="form-control col-md-7 col-xs-12"  >
                                        </div><label class="control-label col-md-1 textleft" for="first-name">บาท </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">ทำสัญญา
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" id="contract"   name="contract"  class="form-control col-md-7 col-xs-12" min="1" >
                                        </div><label class="control-label col-md-1 textleft" for="first-name">บาท </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">เงินโอน * <span class="required"></span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" id="transfer_money" required name="transfer_money"  class="form-control col-md-7 col-xs-12" min="1" >
                                        </div><label class="control-label col-md-1 textleft" for="first-name">บาท </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">ผ่อนเริ่มต้น *
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" id="starting_pay" required name="starting_pay"  class="form-control col-md-7 col-xs-12" min="1" >
                                        </div><label class="control-label col-md-1 textleft" for="first-name">ต่อเดือน </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">ดอกเบี้ย *
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" id="interest"  required name="interest"  class="form-control col-md-7 col-xs-12" min="0.1" max="100" onkeypress="return isNumber(event)">
                                        </div><label class="control-label col-md-1 textleft" for="first-name">% </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="first-name">ระยะเวลาผ่อนชำระ *
                                        </label>
                                        <div class="col-md-2">
                                            <input name="id" type="hidden" value="<?=$_GET['project_id']?>">
                                            <input type="text" id="repayment_period" required name="repayment_period" maxlength="2" class="form-control col-md-7 col-xs-12" min="1">
                                        </div><label class="control-label col-md-1 textleft" for="first-name">ปี </label>
                                    </div>
                                    <br><br>
                                    <input type="hidden" name="selectGallery">
                                    <div align="center">
                                        <!--                                        <button type="button" class="btn btn-warning">Preview</button>-->
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <!-- <footer>
                  <div class="pull-right">
                    Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
                  </div>
                  <div class="clearfix"></div>
                </footer> -->
                <!-- /footer content -->
            </div>
        </div>


        <!-- Bootstrap -->
        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../vendors/nprogress/nprogress.js"></script>

        <script type="text/javascript" src="js/bootstrap-imgupload.js"></script>
        <!-- Validate -->
        <script src="../build/js/jquery.validate.js"></script>
        <!-- iCheck -->
        <script src="../vendors/iCheck/icheck.min.js"></script>

        <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>

        <script src="js/ajax_paln_master.js"></script>

        <!-- bootstrap-progressbar -->
        <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>

        <script type="text/javascript" src="js/jquery.number.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="../build/js/custom.min.js"></script>
        <!-- Fancybox image popup -->
        <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>

        <script type="text/javascript" src="js/jquery.number.js"></script>

        <script type="text/javascript">
            $('.fancybox').fancybox();
        </script>

        <script>
            $(function() {
                // Set up the number formatting.
                $('#built_on').number(true, '');
            });
        </script>


        <script>
            $(document).ready(function() {

                $("#form_home_sell").validate({
                    rules: {
                        plan_id: "required",
                        plan_1: "required",
                        plan_2 : "required",
                        'list_model_image[]' : "required",
                        features_convert : "required",
                        built_on : "required",
                        land_size : "required",
                        total_price : "required",
                        //down :"required",
                        //money : "required",
                        //payments : "required",
                        //contract : "required",
                        starting_pay : "required",
                        interest : "required",
                        repayment_period : "required",
                        transfer_money: "required",
                    },
                    messages: {
                        'list_model_image[]' : "กรุณาเลือกรูปแบบบ้านหลัก",
                        plan_id : "เลือกอย่างน้อย 1 รายการ !",
                        plan_1 : "ระบุ หมายเลขแปลง !",
                        number_converter : "ระบุ หมายเลขแปลง !",
                        features_convert : "ระบุ ลักษณะเด่นของแปลง !",
                        built_on : "ระบุ ขนาดสร้างบนพื้นที่ !",
                        land_size : "ระบุ ขนาดที่ดิน !",
                        total_price : "ระบุ ราคารวมสุทธิ !",
                        down : {
                            required : "ระบุ ดาวน์ ! ",
                            min : "กรุณากรอกค่า มากว่า 0 !",
                            max : "กรุณากรอกค่า ไม่เกิน 100 ! ",
                        },
                        money : "ระบุยอดเงิน !",
                        payments : "ระบุ เงินจอง !",
                        contract : "ระบุ ทำสัญญา !",
                        transfer_money : "ระบุ เงินโอน !",
                        starting_pay : "ระบุ ผ่อนเริ่มต้น ! ",
                        interest : {
                            required : "ระบุ ดอกเบี้ย ! ",
                            min : "กรุณากรอกค่า มากว่า 0 !",
                            max : "กรุณากรอกค่า ไม่เกิน 100 ! ",
                        },
                        repayment_period : "ระบุระยะเวลาผ่อนชำระ !",

                    }
                });

            });
        </script>

        <script>
            $(function() {
                // Set up the number formatting.
                $('#total_price').number(true, 0);
                //$('#down').number(true, 0);
                $('#money').number(true, 0);
                $('#payments').number(true, 0);
                $('#contract').number(true, 0);
                $('#transfer_money').number(true, 0);
                $('#starting_pay').number(true, 0);
                //$('#interest').number(true, 0);
                $('#repayment_period').number(true, 0);
                $('#land_size').number(true, 0);

            });
        </script>

        <script>
            //galery
            function addProject_galery() {
                var index = $('.setIndexProject_galery').length;

                var index = $('.setIndexProject_galery_galley').length;

                var form = `<div><div class="col-sm-3"><div class="form-group setIndexProject_galery">
                        <div class="col-md-12">
                    <input type="file" id="galery_${index + 1}" class="gallery_master" name="project_galery[]" accept="image/*" />
                    </div>
                    </div>

                    <div class="form-group setIndexProject_galery_galley">
                    <div class="col-md-12">
                    <input type="text"  class="form-control"  name="project_galery_text[]" placeholder="กรอกรายละเอียด Gallery (ไทย)" value=""/>
                    </div>
                    </div>

                    <div class="form-group setIndexProject_galery_galley">
                    <div class="col-md-12">
                    <input type="text"  class="form-control"  name="project_galery_text_en[]" placeholder="กรอกรายละเอียด Gallery (อังกฤษ)" value=""/>
                    </div>
                    </div>


                    <button  type="button" class="btn btn-round btn-danger" style="margin-left: 112px;" onclick="removeProject_galery(this)"> - ลบ Gallery</button>

                    </div>
                    </div>`;


                $('#Project_galery').append(form);

                $("#galery_"+(index + 1)).fileinput({
                    uploadUrl: "upload.php", // server upload action
                    maxFileCount: 1,
                    allowedFileExtensions: ["jpg", "png", "jpeg"],
                    browseLabel: ' เลือก Gallery บ้านพร้อมขาย',
                    browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                    removeLabel: 'ลบ',
                    browseClass: 'btn btn-success',
                    showUpload: false,
                    showRemove:false,
                    showCaption: false,
                    msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                    msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                    msgZoomModalHeading: 'ตัวอย่างละเอียด',
                    xxx:'upload_plan_galery_seo',
                    dropZoneTitle : 'Gallery',
                    msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                    msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                    msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                    msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                });


            }

            function removeProject_galery(element)
            {
                $(element).parent().parent().remove();
            }
        </script>

        <script>
            $(document).ready(function(){
                $("#galery_img").hide();
                $("#galery_img2").show();
                $('#master').hide();

                $('#home').prop( "disabled", true );
                $('#up').prop( "disabled", true );
                $('#add').prop( "disabled", true );
                $('#add_gallery').prop( "disabled", true );

                $('#showPlan').change(function () {

                    if($('#showPlan').val() == '') {
                        $('#home').prop( "disabled", true );
                        $('#up').prop( "disabled", true );
                        $('#add').prop( "disabled", true );
                        $('#add_gallery').prop( "disabled", true );
                    }else{
                        $('#home').prop( "disabled", false );
                        $('#up').prop( "disabled", false );
                        $('#add').prop( "disabled", false );
                        $('#add_gallery').prop( "disabled", false );
                    }
                });

            });
            $('input:radio[name="optradio_vdo"]').change(function() {
                if ($(this).val() == 'youtube') {
                    $("#galery_img").show();
                    $("#galery_img2").hide();

                } else {
                    $("#galery_img").hide();
                    $("#galery_img2").show();
                }
            });
            $('#home').change(function () {

                if(this.checked) {
                    $("#galery_img").hide();
                    $("#galery_img2").hide();
                    $('#check_home').hide();
                    $("#galery_img2").hide();
                    $('#master').show();
                } else{
                    $('#check_home').show();
                    $('#master').hide();
                    $('#gallery_master').hide();
                }
            });
        </script>
        <script>
            $(document).ready(function(){
                $("#alert").show();
                $("#alert").fadeTo(6000, 500).slideUp(500, function(){
                    $("#alert").alert('close');
                });

            });
        </script>
        <script>
            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 46 || charCode > 58)|| (charCode == 47)) {
                    return false;
                }
                return true;
            }

            function validateForm() {

                    var selectGallery = $("input[name=selectGallery]");
                   var ch = $('#home').prop('checked');

                    if (($('#home').prop('checked') == true) && ($("#showgallery_master").val()=='')) {
                         $.alert({
                            title: '**หมายเหตุ',
                            content: 'ถ้าระบุเป็นบ้านพร้อมตกแต่ง กรุณาเลือกรูปภาพในกล่องโฟลเดอร์รูป <u>Gallery</u> ถ้าไม่มีกรุณาเอาเครื่องหมายถูกออกค่ะ (แจ้งเพิ่มรูปได้ที่ฝ่ายโคโค่)',
                        });
                         return false;

                    }else{

                        if( (selectGallery.val().length < 1)&&($('#home').prop('checked') == true)){
                        $.alert({
                            title: '**หมายเหตุ',
                            content: 'ถ้าระบุเป็นบ้านพร้อมขาย กรุณาเลือกรูปภาพในรูป <u>Gallery</u> ถ้าไม่มีกรุณาเอาเครื่องหมายถูกออกค่ะ (แจ้งเพิ่มรูปได้ที่ฝ่ายโคโค่',
                        });
                         return false;
                     }
                    }


                }
        </script>


</body>
</html>