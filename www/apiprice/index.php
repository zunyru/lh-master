<?php
header("Access-Control-Allow-Origin: *");
header( 'Content-Type:text/html; charset=utf8');
error_reporting(E_ALL);
ini_set('display_errors', '1');

include  __DIR__.'/../Frontend/include/help/query_function.php';


$arr_id = ['80','36','29','28','35','25','24','26','37'];
//$arr_id = ['80','36'];
//$arr_id = $_POST['param'];
$project_ids = [];

for ($i=0; $i < sizeof($arr_id) ; $i++) { 
	$sql = "SELECT DISTINCT P.project_id,BR.order_seq
	FROM LH_PROJECTS P
	JOIN LH_PROJECT_SUB PS ON P.project_id = PS.project_id
	JOIN LH_PRODUCTS PD ON PS.product_id = PD.product_id
	LEFT JOIN LH_PROJECT_VIEW lh_proj_view ON lh_proj_view.project_id = P.project_id
	LEFT JOIN LH_BRANDS BR ON BR.brand_id = P.brand_id 
	WHERE p.project_id = '".$arr_id[$i]."' 
	ORDER BY BR.order_seq ASC ";
	$result = mssql_query($sql , $GLOBALS['db_conn']);
	while ($row = mssql_fetch_object($result)) {
		$project_ids[] = $row->project_id;
	}
}


Helper::sortByPriceReturnAssoc($project_ids,'ASC');
//print_r($project_ids);
$resultArray = array(); 

foreach ($project_ids as $i => $pro_sub) {
	$project_id = $pro_sub['project_id'];
	$project = getProjectByID($project_id);
	$price   = getProjectPrice($project_id);
	//Zone
	$zone =  getZoneByProject($project_id);	
	$resultArrayZone = array();
	for ($i=0; $i < sizeof($zone['zone_id']) ; $i++) { 
		$resultArrayZone[] = array(
			//"zone_id" => $zone['zone_id'][$i],
			"zone_name_th" => $zone['zone_th'][$i],
			//"zone_name_en" => $zone['zone_en'][$i],
		);  
	}

	 //URL
	$link =  getURLByProject($project_id);	
	$resultArrayLink = array();
	for ($i=0; $i < sizeof($zone['zone_id']) ; $i++) { 
	 	//mode
		if($link['product_id'][$i] == 2){
			$mode = 'townhome/project';
		}else if($link['product_id'][$i] == 3){
			$mode = 'condominium';
		}else{
			$mode = 'singlehome/project';
		}
		$resultArrayLink[] = array(
			"url" => $url_master.$mode."/".str_replace(" ","-",$link['project_name'][$i])."/".$link['product_id'][$i],
		);  
	}

	 //Leade image
	$resultArrayLeadImage = array();
	$banner_project_default = getAllBannerByProjectID_Default($project_id);
	$banner_projects    = getAllBannerByProjectID($project_id);


	if($banner_project_default > 0){
		foreach ($banner_projects as $i => $banner_project) {
			$banner_project->LEAD_IMAGE_PROJECT_FILE_NAME;
			exit();
			if ($banner_project->LEAD_IMAGE_PROJECT_FILE_TYPE == 'image_leade_banner') {
				$resultArrayLeadImage[] = array(
					'LeageImage' => $url_master."www/Backend/". $banner_project->LEAD_IMAGE_PROJECT_FILE_NAME,
					'LeadeLink' => $banner_project->img_url,
					'LeageImageMode' => 'image leade banner',  
				);
			}elseif ($banner_project->LEAD_IMAGE_PROJECT_FILE_TYPE == 'youtube') {
				$lead_img = $url_master."www/Backend/". $banner_project->banner_img_thum;
				$lead_youtube_url = str_replace('watch?v=', 'embed/', $banner_project->LEAD_IMAGE_PROJECT_FILE_NAME);
				$resultArrayLeadImage[] = array(
					'LeageImage' => $lead_img = $url_master."www/Backend/". $banner_project->banner_img_thum,
					'LeadeVideo' => $lead_youtube_url,
					'LeageImageMode' => 'youtube',  
				);
			}elseif ($banner_project->LEAD_IMAGE_PROJECT_FILE_TYPE == 'vdo') {

				$resultArrayLeadImage[] = array(
					'LeageImage' => $url_master."www/Backend/". $banner_project->banner_img_thum,
					'LeadeVideo' =>  $url_master."www/Backend/". $banner_project->LEAD_IMAGE_PROJECT_FILE_NAME ,
					'LeageImageMode' => 'vdo',  
				);
			}elseif ($banner_project->LEAD_IMAGE_PROJECT_FILE_TYPE == 'banner') {
				$resultArrayLeadImage[] = array(
					'LeageImage' => $url_master."www/Backend/". $banner_project->LEAD_IMAGE_PROJECT_FILE_NAME,
					'LeadeLink' => $banner_project->img_url,
					'LeadeText' => $banner_project->banner_text,
					'LeadeColor' => $banner_project->backgroup_color,
					'LeageImageMode' => 'image leade banner',  
				);
			}	

		}
	}else{
		foreach ($banner_projects as $i => $banner_project) {
			if ($banner_project->LEAD_IMAGE_PROJECT_FILE_TYPE == 'image') {
				$resultArrayLeadImage[] = array(
					'LeageImage' => $url_master."www/Backend/". $banner_project->LEAD_IMAGE_PROJECT_FILE_NAME,
					'LeageImageMode' => 'image',  
					'LeadeLink' =>  $banner_project->img_url,
				);
			}
		}
	}
	
	if($price->price_mode == '1'){
		$mode_price = 'ราคาเริ่มต้น';
	}else{
		$mode_price = 'ราคา';
	}

	
	if(!empty($price->project_price)){
		$price = Helper::getProjectPrice($price->project_price,$price->price_mode);
	}else{
		$price = '';
	}
	if(is_object($price)){
		$unit = Helper::getPriceModeName($price->price_mode,$price->project_price);
	}else{
		$unit = 'ล้านบาท';
	}
	
	$resultArray[$i] = [
		'id' => $project->project_id,
		'name' => $project->project_name_th,
		'mode' => $mode_price,
		'price' => (string)$price,
		'unit' => $unit,
		'location' => $resultArrayZone,
		'URL' => $resultArrayLink,
		'LeadeImage' => $resultArrayLeadImage,
	];

}

//print_r($resultArray);

echo json_encode($resultArray);
?>






