<?php

## THIS System root path
$system_path="/apps/www/httpd/vroot/www/flm/";

## Output for xml file
$system_path_output="/apps/www/httpd/vroot/data_flm/";

## Output for map PDF file
$map_pdf_des="/apps/www/httpd/vroot/data_flm/map/";

## Source file from real system
$map_pdf_source="/apps/www/httpd/vroot/www/Backend/fileupload/images/maps/pdf/";

## Define pattern for product ID for comparing old and new system
#  Example.
#  New System is "1" but old is "00001"
#  Will code in "1"=>"00001" (array form)
$product_pattern=array(
	'1'=>'00001',
	'2'=>'00017',
	'4'=>'00002'
);