<?php
/**
 * #####################################
 * CRON SCRIPT FOR GENERATE FLM XML FILE
 * #####################################
 * Create by Yothin MFEC PCL.
 * @uses FluidXml,dbCon_mssql
 * @return XML Files
 */
// die(phpinfo());
//Set show any error
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//Get necessary libs
require_once 'XMLlib/FluidXml.php';
// require_once '../Backend/include/dbCon_mssql.php';
require_once 'config.php';

// define serber path to collect output file correct
define('SYSTEM_PATH', $system_path);
define('OUTPUT_PATH', $system_path_output);
define('MAP_PDF_SOURCE', $map_pdf_source);
define('MAP_PDF_DES', $map_pdf_des);

//define map file that available in system
$map_pdf_file=array();

//DATABASE Configuration
$dvlh = 'lhdb';
$lh = 'lh';

$db_conn = mssql_connect("10.10.10.10","lhadmin","lhadmin@2015")
or die( "<strong>ERROR: Connection to MYSERVER failed</strong>" );
// select database
mssql_select_db( $dvlh, $db_conn ) or die( "<strong>ERROR: Selecting database failed</strong>" );


//Handle use class
use function \FluidXml\fluidxml;
use \FluidXml\FluidXml;

/**
 * ############################
 * 1) SECTION FOR HOME_LOCATION
 * ############################
 * old -> HOME_LOCATION
 * new -> LH_PROVINCES
 */
$query=mssql_query('SELECT PROVINCE_CODE, PROVINCE_NAME, PROVINCE_NAME_ENG, GEO_NAME FROM LH_PROVINCES LEFT JOIN LH_GEOGRAPHY ON LH_GEOGRAPHY.GEO_ID = LH_PROVINCES.GEO_ID');

$HOME_LOCATION = new FluidXml('table');
$HOME_LOCATION->setAttribute('name', 'home_location');
$HOME_LOCATION->add('GeneratedDateTime', date("d/m/Y H:i:s")); // A 'fruit' node with 'orange' as content.

while ($result=mssql_fetch_object($query)) {
	$HOME_LOCATION->add(['rowdata' => [
	        'CODE' => [
	            '@type' => 'varchar',
	            "<![CDATA[$result->PROVINCE_CODE]]>",
	        ],
	        'NAME' => [
	            '@type' => 'varchar',
	            "<![CDATA[{$result->PROVINCE_NAME}]]>",
	        ],
	        'NH_ENABLE' => [
	            '@type' => 'smallint',
	            "<![CDATA[0]]>",
	        ],
	        'HC_ENABLE' => [
	            '@type' => 'smallint',
	            "<![CDATA[0]]>",
	        ],
	        'HR_ENABLE' => [
	            '@type' => 'smallint',
	            "<![CDATA[0]]>",
	        ],
	    ],
	]);
}

$HOME_LOCATION->save(OUTPUT_PATH.'HOME_LOCATION.xml');
$pattern=NULL;
echo "HOME_LOCATION . . . Done<br>";

/**
 * ############################
 * 2) SECTION FOR HOME_SUB_LOCATION
 * ############################
 * old -> HOME_SUB_LOCATION
 * new -> LH_ZONES
 */
$query=mssql_query("SELECT zone_id,LH_PROVINCES.PROVINCE_NAME,zone_name_th,zone_name_en,zone_attribute FROM LH_ZONES LEFT JOIN LH_PROVINCES ON LH_PROVINCES.PROVINCE_ID=LH_ZONES.province_id WHERE LH_ZONES.zone_attribute LIKE '%flm%'");

$HOME_SUB_LOCATION = new FluidXml('table');
$HOME_SUB_LOCATION->setAttribute('name', 'home_sub_location_1198');
$HOME_SUB_LOCATION->add('GeneratedDateTime', date("d/m/Y H:i:s")); // A 'fruit' node with 'orange' as content.

while ($result=mssql_fetch_object($query)) {
	$HOME_SUB_LOCATION->add(['rowdata' => [
	        'CODE' => [
	            '@type' => 'char',
	            "<![CDATA[$result->zone_id]]>",
	        ],
	        'NAME' => [
	            '@type' => 'varchar',
	            "<![CDATA[$result->zone_name_th]]>",
	        ],
	        'HOME_LOCATION_CODE' => [
	            '@type' => 'char',
	            "<![CDATA[$result->zone_id]]>",
	        ],
	        'PIC_MAP_SUBLOCATION' => [
	            '@type' => 'varchar',
	            "<![CDATA[]]>",
	        ],
	        'PIC_MAP_WIDTH' => [
	            '@type' => 'int',
	            "<![CDATA[]]>",
	        ],
	        'PIC_MAP_HEIGHT' => [
	            '@type' => 'int',
	            "<![CDATA[]]>",
	        ]
	    ],
	]);
}

$HOME_SUB_LOCATION->save(OUTPUT_PATH.'HOME_SUB_LOCATION_1198.xml');
$pattern=NULL;
echo "HOME_SUB_LOCATION_1198 . . . Done<br>";

/**
 * ############################
 * 3) SECTION FOR REGION
 * ############################
 * old -> REGION
 * new -> LH_PROJECT_ZONE (District where in project zone )
 */
$query=mssql_query('SELECT LH_AMPHURS.AMPHUR_ID,LH_AMPHURS.AMPHUR_NAME,LH_PROJECT_ZONE.zone_id,LH_PROJECT_ZONE.zone_id_flm FROM LH_AMPHURS LEFT JOIN LH_PROJECT_ZONE ON LH_PROJECT_ZONE.amphur_id=LH_AMPHURS.AMPHUR_ID WHERE LH_AMPHURS.PROVINCE_ID=1');

$REGION = new FluidXml('table');
$REGION->setAttribute('name', 'region');
$REGION->add('GeneratedDateTime', date("d/m/Y H:i:s")); // A 'fruit' node with 'orange' as content.

while ($result=mssql_fetch_object($query)) {
	$REGION->add(['rowdata' => [
	        'REGION_CODE' => [
	            '@type' => 'char',
	            "<![CDATA[$result->AMPHUR_ID]]>",
	        ],
	        'REGION_NAME' => [
	            '@type' => 'varchar',
	            "<![CDATA[{$result->AMPHUR_NAME}]]>",
	        ],
	        'HOME_SUBLOCATION_CODE' => [
	            '@type' => 'char',
	            "<![CDATA[$result->zone_id]]>",
	        ],
	    ],
	]);
}

$REGION->save(OUTPUT_PATH.'REGION.xml');
$pattern=NULL;
echo "REGION . . . Done<br>";

/**
 * ################################
 * 4) SECTION FOR PROMOTION_PROJECT
 * ################################
 * old -> PROMOTION_PROJECT
 * new -> LH_PROMOTION (Related with Project)
 */

//first query for main data
$query=mssql_query('SELECT LH_PROJECTS.project_name_th,LH_PROJECTS.project_url,LH_PROMOTION.promotion_id, LH_PROJECTS.project_id,LH_PRODUCTS.product_id,LH_PRODUCTS.product_name_en FROM LH_PROMOTION LEFT JOIN LH_PROJECTS ON LH_PROJECTS.project_id=LH_PROMOTION.project_id LEFT JOIN LH_PROJECT_SUB ON LH_PROJECT_SUB.project_id = LH_PROJECTS.project_id LEFT JOIN LH_PRODUCTS ON LH_PRODUCTS.product_id = LH_PROJECT_SUB.product_id WHERE LH_PROMOTION.promotion_end_date >= GETDATE()');

$PROMOTION_PROJECT = new FluidXml('table');
$PROMOTION_PROJECT->setAttribute('name', 'promotion_project');
$PROMOTION_PROJECT->add('GeneratedDateTime', date("d/m/Y H:i:s"));

//main loop
$distinct_id=array();
while ($result=mssql_fetch_object($query)) {
	//check for distinct data if not put it to array
	if(!in_array($result->promotion_id,$distinct_id)):
		$distinct_id[]=$result->promotion_id;
		// second query for img data
		$query_img=mssql_query("SELECT LEAD_IMAGE_PROJECT_FILE_NAME as 'img' FROM LH_LEAD_IMAGE_PROJECT_FILE WHERE PROJECT_ID=$result->project_id ");

		$pattern=['rowdata' => [
			        'PROMOTION_ID' => [
			            '@type' => 'char',
			            "<![CDATA[$result->promotion_id]]>",
			        ],
			        'PROJECT_ID' => [
			            '@type' => 'char',
			            "<![CDATA[{$result->project_id}]]>",
			        ],
			        'PROJECT_TYPE' => [
			            '@type' => 'varchar',
			            "<![CDATA[$result->product_id]]>",
			        ],
			        'FLAG_ONLINE' => [
			            '@type' => 'varchar',
			            "<![CDATA[]]>",
			        ],
			        'FLAG_ONLINE_LH' => [
			            '@type' => 'char',
			            "<![CDATA[]]>",
			        ],
			    ],
			];


		if ($result->product_id=='1') {
			$url="www.lh.co.th/th/singlehome/project/".str_replace(" ","-",$result->project_name_th)."/1";
		}elseif ($result->product_id=='2') {
			$url="www.lh.co.th/th/townhome/project/".str_replace(" ","-",$result->project_name_th)."/2";
		}elseif ($result->product_id=='3') {
			$url="https://www.lh.co.th/th/condominium/".str_replace(" ","-",$result->project_name_th);
		}
		$value="<![CDATA[".$url."]]>";
		$pattern['rowdata']+=["PROJECT_URL"=>['@type'=>"varchar","$value"]];

		//loop for getting img data
		$key=0;
		while (($result_img=mssql_fetch_object($query_img)) && ($key <= 2) ) {
			$i=($key==0?'':$key+1);
			$pattern['rowdata']+=["PIC_PROJECT$i"=>['@type'=>"varchar","<![CDATA[$result_img->img]]>"]];
			$pattern['rowdata']+=["PIC_FROM$i"=>['@type'=>"char","<![CDATA[]]>"]];
			$key++;
		}
		$img_num=3-mssql_num_rows($query_img);
		if($img_num <= 3 && $img_num >= 1){
			$x=mssql_num_rows($query_img);
			for ($i=1; $i <= $img_num ; $i++) {
				$z=($x==0?'':$x+1);
				$pattern['rowdata']+=["PIC_PROJECT$z"=>['@type'=>"varchar","<![CDATA[]]>"]];
				$pattern['rowdata']+=["PIC_FROM$z"=>['@type'=>"char","<![CDATA[]]>"]];
				$x++;
			}
		}

		$sort_pattern=['PROMOTION_ID', 'PROJECT_ID', 'PROJECT_TYPE', 'FLAG_ONLINE', 'FLAG_ONLINE_LH', 'PIC_PROJECT', 'PIC_FROM', 'PROJECT_URL', 'PIC_PROJECT2', 'PIC_FROM2', 'PIC_PROJECT3', 'PIC_FROM3'];
		$pattern_sorted['rowdata'] = array_merge(array_flip($sort_pattern), $pattern['rowdata']);

		//add all data to function
		$PROMOTION_PROJECT->add($pattern_sorted);
	endif;
}
$PROMOTION_PROJECT->save(OUTPUT_PATH.'PROMOTION_PROJECT.xml');
$pattern=NULL;
echo "PROMOTION_PROJECT . . . Done<br>";

/**
 * ################################
 * 5) SECTION FOR NEWHOME_PROMOTION
 * ################################
 * old -> NEWHOME_PROMOTION
 * new -> LH_PROMOTION (Main)
 */

$query=mssql_query("SELECT LH_PROMOTION.promotion_id, project_id, promotion_name_th, promotion_name_en, promotion_detail_th, promotion_detail_en, convert(VARCHAR(10), promotion_start_date, 103) promotion_start_date, convert(VARCHAR(10), promotion_end_date, 103) promotion_end_date, promotion_url, promotion_highlights, promotion_approve, LH_IMG_PROMOTION.img_promotion_name,convert(VARCHAR(10), LH_PROMOTION.creat_date, 103) creat_date, convert(VARCHAR(10), LH_PROMOTION.update_date, 103) update_date FROM LH_PROMOTION LEFT JOIN LH_IMG_PROMOTION ON LH_IMG_PROMOTION.promotion_id=LH_PROMOTION.promotion_id WHERE  LH_PROMOTION.promotion_end_date >= GETDATE()");

$NEWHOME_PROMOTION = new FluidXml('table');
$NEWHOME_PROMOTION->setAttribute('name', 'newhome_promotion');
$NEWHOME_PROMOTION->add('GeneratedDateTime', date("d/m/Y H:i:s"));

while ($result=mssql_fetch_object($query)) {
	$pattern=['rowdata' => [
		        'PROMOTION_ID' => [
		            '@type' => 'char',
		            "<![CDATA[$result->promotion_id]]>",
		        ],
		        'ACTIVITY_DATE' => [
		            '@type' => 'varchar',
		            "<![CDATA[]]>",
		        ],
		        'ACTIVITY_DESC' => [
		            '@type' => 'varchar',
		            "<![CDATA[".str_replace(array("\r\n","\r","\n","\\r","\\n","\\r\\n")," ",preg_replace('/\s+/', ' ', $result->promotion_name_th))."]]>",
		        ],
		        'SPECIAL_DESC' => [
		            '@type' => 'varchar',
		            "<![CDATA[".str_replace(array("\r\n","\r","\n","\\r","\\n","\\r\\n")," ",preg_replace('/\s+/', ' ', $result->promotion_detail_th))."]]>",
		        ],
		        'ACTIVITY_DESC_E' => [
		            '@type' => 'varchar',
		            "<![CDATA[".str_replace(array("\r\n","\r","\n","\\r","\\n","\\r\\n")," ",preg_replace('/\s+/', ' ', $result->promotion_name_en))."]]>",
		        ],
		        'SPECIAL_DESC_E' => [
		            '@type' => 'varchar',
		            "<![CDATA[".str_replace(array("\r\n","\r","\n","\\r","\\n","\\r\\n")," ",preg_replace('/\s+/', ' ', $result->promotion_detail_en))."]]>",
		        ],
		        'EFFECT_DATE' => [
		            '@type' => 'date',
		            "<![CDATA[$result->promotion_start_date]]>",
		        ],
		        'END_DATE' => [
		            '@type' => 'date',
		            "<![CDATA[$result->promotion_end_date]]>",
		        ],
		        'CREATE_USER' => [
		            '@type' => 'char',
		            "<![CDATA[]]>",
		        ],
		        'CREATE_DATE' => [
		            '@type' => 'date',
		            "<![CDATA[$result->creat_date]]>",
		        ],
		        'LAST_USER' => [
		            '@type' => 'char',
		            "<![CDATA[]]>",
		        ],
		        'LAST_DATE' => [
		            '@type' => 'date',
		            "<![CDATA[$result->update_date]]>",
		        ],
		        'PROMOTION_THUMB_PIC' => [
		            '@type' => 'varchar',
		            "<![CDATA[]]>",
		        ],
		        'PROMOTION_THUMB_PIC_TEXT' => [
		            '@type' => 'varchar',
		            "<![CDATA[]]>",
		        ],
		        'APPROVED' => [
		            '@type' => 'char',
		            "<![CDATA[$result->promotion_approve]]>",
		        ],
		        'ACTIVITY_DATE_START' => [
		            '@type' => 'date',
		            "<![CDATA[]]>",
		        ],
		        'ACTIVITY_DATE_END' => [
		            '@type' => 'date',
		            "<![CDATA[]]>",
		        ],
		        'PROMOTION_PIC' => [
		            '@type' => 'varchar',
		            "<![CDATA[$result->img_promotion_name]]>",
		        ],
		        'PROMOTION_PIC_TEXT' => [
		            '@type' => 'varchar',
		            "<![CDATA[]]>",
		        ],
		    ],
		];

	//PROMOTION_PIC
	$x=2;
	for ($i=1; $i <= 3 ; $i++) {
		$pattern['rowdata']+=["PROMOTION_PIC$x"=>['@type'=>"varchar","<![CDATA[]]>"]];
		$x++;
	}

	$sort_pattern=['PROMOTION_ID', 'ACTIVITY_DATE', 'ACTIVITY_DESC', 'SPECIAL_DESC', 'EFFECT_DATE', 'END_DATE', 'CREATE_USER', 'CREATE_DATE', 'LAST_USER', 'LAST_DATE', 'PROMOTION_PIC', 'PROMOTION_PIC_TEXT', 'PROMOTION_THUMB_PIC', 'PROMOTION_THUMB_PIC_TEXT', 'APPROVED', 'ACTIVITY_DATE_START', 'ACTIVITY_DATE_END', 'ACTIVITY_DESC_E', 'SPECIAL_DESC_E', 'PROMOTION_PIC2', 'PROMOTION_PIC3', 'PROMOTION_PIC4'];
	$pattern_sorted['rowdata'] = array_merge(array_flip($sort_pattern), $pattern['rowdata']);

	//add all data to function
	$NEWHOME_PROMOTION->add($pattern_sorted);
}
$NEWHOME_PROMOTION->save(OUTPUT_PATH.'NEWHOME_PROMOTION.xml');
$pattern=NULL;
echo "NEWHOME_PROMOTION . . . Done<br>";

/**
 * ################################
 * 6) SECTION FOR NEWHOME_BRAND_PROJECT
 * ################################
 * old -> NEWHOME_BRAND_PROJECT
 * new -> LH_BRANDS
 */

$query=mssql_query("SELECT b.brand_id, b.brand_name_th, b.brand_name_en, b.brand_concep_th, CAST(b.message_th AS varchar(max)) as message_th,CAST(b.message_en AS varchar(max)) as message_en, b.brand_concep_en, b.logo_brand_th, b.logo_brand_en, b.brand_img_seo_th, b.brand_img_seo_en,LH_PRODUCTS.product_name_en FROM LH_BRANDS b LEFT JOIN LH_PROJECTS ON LH_PROJECTS.brand_id=b.brand_id LEFT JOIN LH_PROJECT_SUB ON LH_PROJECT_SUB.project_id = LH_PROJECTS.project_id LEFT JOIN LH_PRODUCTS ON LH_PRODUCTS.product_id = LH_PROJECT_SUB.product_id GROUP BY b.brand_id, b.brand_name_th, b.brand_name_en, b.brand_concep_th,CAST(b.message_th AS varchar(max)),CAST(b.message_en AS varchar(max)),b.brand_concep_en, b.logo_brand_th, b.logo_brand_en, b.brand_img_seo_th, b.brand_img_seo_en,LH_PRODUCTS.product_name_en ORDER BY b.brand_name_en");

$NEWHOME_BRAND_PROJECT = new FluidXml('table');
$NEWHOME_BRAND_PROJECT->setAttribute('name', 'newhome_brand_project');
$NEWHOME_BRAND_PROJECT->add('GeneratedDateTime', date("d/m/Y H:i:s"));

$distinct_id=array();
while ($result=mssql_fetch_object($query)) {
	//check for distinct data if not put it to array
	if(!in_array($result->brand_id,$distinct_id)):
		$distinct_id[]=$result->brand_id;
		$pattern=['rowdata' => [
			        'ID' => [
			            '@type' => 'char',
			            "<![CDATA[$result->brand_id]]>",
			        ],
			        'BRAND_NAME' => [
			            '@type' => 'varchar',
			            "<![CDATA[{$result->brand_name_th}]]>",
			        ],
			        'THUMB_IMAGE' => [
			            '@type' => 'varchar',
			            "<![CDATA[]]>",
			        ],
			        'THUMB_IMAGE_TEXT' => [
			            '@type' => 'varchar',
			            "<![CDATA[]]>",
			        ],
			        'BRAND_ICON' => [
			            '@type' => 'varchar',
			            "<![CDATA[$result->logo_brand_th]]>",
			        ],
			        'BRAND_ICON_TEXT' => [
			            '@type' => 'varchar',
			            "<![CDATA[$result->brand_img_seo_th]]>",
			        ],
			        'PROJECT_TYPE_ID' => [
			            '@type' => 'char',
			            "<![CDATA[$result->product_name_en]]>",
			        ],
			        'IS_FIRST_HOME' => [
			            '@type' => 'char',
			            "<![CDATA[]]>",
			        ],
			        'SHOW_LH' => [
			            '@type' => 'char',
			            "<![CDATA[]]>",
			        ],
			        'PROVINCE_ID' => [
			            '@type' => 'char',
			            "<![CDATA[]]>",
			        ],
			        'DISPLAY_ORDER' => [
			            '@type' => 'smallint',
			            "<![CDATA[]]>",
			        ],
			    ],
			];

		$sort_pattern=['ID', 'BRAND_NAME', 'THUMB_IMAGE', 'THUMB_IMAGE_TEXT', 'BRAND_ICON', 'BRAND_ICON_TEXT', 'PROJECT_TYPE_ID', 'IS_FIRST_HOME', 'SHOW_LH', 'PROVINCE_ID', 'DISPLAY_ORDER'];
		$pattern_sorted['rowdata'] = array_merge(array_flip($sort_pattern), $pattern['rowdata']);

		//add all data to function
		$NEWHOME_BRAND_PROJECT->add($pattern_sorted);
	endif;
}

$NEWHOME_BRAND_PROJECT->save(OUTPUT_PATH.'NEWHOME_BRAND_PROJECT.xml');
$pattern=NULL;
echo "NEWHOME_BRAND_PROJECT . . . Done<br>";

/**
 * ################################
 * 7) SECTION FOR NEWHOME
 * ################################
 * old -> NEWHOME
 * new -> LH_HOME_SELL
 */

//start main query
$query=mssql_query("SELECT LH_HOME_SELL.home_sell_id,LH_PROJECT_SUB.project_id,LH_PLANS.plan_id,LH_PLANS.plan_name_en,LH_HOME_SELL.features_convert,LH_PRODUCTS.product_id,LH_PRODUCTS.product_name_en,LH_HOME_SELL.built_on,LH_PLANS.plan_useful,LH_PLANS.plan_img,LH_PLANS.plan_seo,convert(VARCHAR(10), LH_HOME_SELL.creat_date, 103) creat_date,convert(VARCHAR(10), LH_HOME_SELL.update_date, 103) update_date,LH_HOME_SELL.house,LH_PROJECTS.project_status,LH_HOME_SELL.home_sell_status,

(SELECT TOP 1 LH_GALERY_FLOOR_PLAN_IMG.floor_plan_img_name FROM LH_GALERY_FLOOR_PLAN_IMG WHERE LH_GALERY_FLOOR_PLAN_IMG.plan_id=LH_PLANS.plan_id AND number_floor_plan=1) as 'floor1_img',
(SELECT TOP 1 LH_GALERY_FLOOR_PLAN_IMG.floor_plan_img_name FROM LH_GALERY_FLOOR_PLAN_IMG WHERE LH_GALERY_FLOOR_PLAN_IMG.plan_id=LH_PLANS.plan_id AND number_floor_plan=2) as 'floor2_img',
(SELECT TOP 1 LH_GALERY_FLOOR_PLAN_IMG.floor_plan_img_name FROM LH_GALERY_FLOOR_PLAN_IMG WHERE LH_GALERY_FLOOR_PLAN_IMG.plan_id=LH_PLANS.plan_id AND number_floor_plan=3) as 'floor3_img',

(SELECT TOP 1 LH_FUNCTION_PLAN_SUB.function_plan_sub_plan_count_room FROM LH_FUNCTION_PLAN_SUB WHERE function_plan_plan_id=1 AND LH_FUNCTION_PLAN_SUB.plan_id=LH_PLANS.plan_id) as 'BEDROOM',
(SELECT TOP 1 LH_FUNCTION_PLAN_SUB.function_plan_sub_plan_count_room FROM LH_FUNCTION_PLAN_SUB WHERE function_plan_plan_id=2 AND LH_FUNCTION_PLAN_SUB.plan_id=LH_PLANS.plan_id) as 'TOILET',
(SELECT TOP 1 LH_FUNCTION_PLAN_SUB.function_plan_sub_plan_count_room FROM LH_FUNCTION_PLAN_SUB WHERE function_plan_plan_id=3 AND LH_FUNCTION_PLAN_SUB.plan_id=LH_PLANS.plan_id) as 'PARKING',
(SELECT TOP 1 LH_FUNCTION_PLAN_SUB.function_plan_sub_plan_count_room FROM LH_FUNCTION_PLAN_SUB WHERE function_plan_plan_id=4 AND LH_FUNCTION_PLAN_SUB.plan_id=LH_PLANS.plan_id) as 'WORKING_ROOM',
(SELECT TOP 1 LH_FUNCTION_PLAN_SUB.function_plan_sub_plan_count_room FROM LH_FUNCTION_PLAN_SUB WHERE function_plan_plan_id=5 AND LH_FUNCTION_PLAN_SUB.plan_id=LH_PLANS.plan_id) as 'LIVING_ROOM',
(SELECT TOP 1 LH_FUNCTION_PLAN_SUB.function_plan_sub_plan_count_room FROM LH_FUNCTION_PLAN_SUB WHERE function_plan_plan_id=6 AND LH_FUNCTION_PLAN_SUB.plan_id=LH_PLANS.plan_id) as 'SERVANTROOM',
(SELECT TOP 1 LH_FUNCTION_PLAN_SUB.function_plan_sub_plan_count_room FROM LH_FUNCTION_PLAN_SUB WHERE function_plan_plan_id=7 AND LH_FUNCTION_PLAN_SUB.plan_id=LH_PLANS.plan_id) as 'PEACE_ROOM',
(SELECT TOP 1 LH_FUNCTION_PLAN_SUB.function_plan_sub_plan_count_room FROM LH_FUNCTION_PLAN_SUB WHERE function_plan_plan_id=8 AND LH_FUNCTION_PLAN_SUB.plan_id=LH_PLANS.plan_id) as 'DRAWING_ROOM',
(SELECT TOP 1 LH_FUNCTION_PLAN_SUB.function_plan_sub_plan_count_room FROM LH_FUNCTION_PLAN_SUB WHERE function_plan_plan_id=9 AND LH_FUNCTION_PLAN_SUB.plan_id=LH_PLANS.plan_id) as 'WORKING_CORNER',

(SELECT TOP 1 LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id FROM LH_PECULIARITY_SUB_HOME_SELL WHERE LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id=1 AND LH_PECULIARITY_SUB_HOME_SELL.plan_id=LH_PLANS.plan_id) as 'ตกแต่งห้องน้ำ_พร้อมสุขภัณฑ์',
(SELECT TOP 1 LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id FROM LH_PECULIARITY_SUB_HOME_SELL WHERE LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id=2 AND LH_PECULIARITY_SUB_HOME_SELL.plan_id=LH_PLANS.plan_id) as 'ตกแต่งสวนภายนอกบ้าน_ENG',
(SELECT TOP 1 LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id FROM LH_PECULIARITY_SUB_HOME_SELL WHERE LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id=4 AND LH_PECULIARITY_SUB_HOME_SELL.plan_id=LH_PLANS.plan_id) as 'ติดตั้งเครื่องปรับอากาศ',
(SELECT TOP 1 LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id FROM LH_PECULIARITY_SUB_HOME_SELL WHERE LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id=5 AND LH_PECULIARITY_SUB_HOME_SELL.plan_id=LH_PLANS.plan_id) as 'ตกแต่งพื้นภายใน',
(SELECT TOP 1 LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id FROM LH_PECULIARITY_SUB_HOME_SELL WHERE LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id=6 AND LH_PECULIARITY_SUB_HOME_SELL.plan_id=LH_PLANS.plan_id) as 'ผนังบุวอลล์เปเปอร์',
(SELECT TOP 1 LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id FROM LH_PECULIARITY_SUB_HOME_SELL WHERE LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id=7 AND LH_PECULIARITY_SUB_HOME_SELL.plan_id=LH_PLANS.plan_id) as 'แทงค์น้ำ_ปั๊มน้ำ',
(SELECT TOP 1 LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id FROM LH_PECULIARITY_SUB_HOME_SELL WHERE LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id=8 AND LH_PECULIARITY_SUB_HOME_SELL.plan_id=LH_PLANS.plan_id) as 'ครัวไทย',
(SELECT TOP 1 LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id FROM LH_PECULIARITY_SUB_HOME_SELL WHERE LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id=9 AND LH_PECULIARITY_SUB_HOME_SELL.plan_id=LH_PLANS.plan_id) as 'ครัวไทยพร้อมเฟอร์นิเจอร์',
(SELECT TOP 1 LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id FROM LH_PECULIARITY_SUB_HOME_SELL WHERE LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id=10 AND LH_PECULIARITY_SUB_HOME_SELL.plan_id=LH_PLANS.plan_id) as 'ตกแต่งเฟอร์นิเจอร์__แต่งเพิ่มพิเศษจากมาตรฐานที่มี_เช่น_แต่งเฟอร์ทั้งหลัง',
(SELECT TOP 1 LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id FROM LH_PECULIARITY_SUB_HOME_SELL WHERE LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id=11 AND LH_PECULIARITY_SUB_HOME_SELL.plan_id=LH_PLANS.plan_id) as 'ตกแต่งห้องน้ำ_พร้อมสุขภัณฑ์อัจฉริยะ',
(SELECT TOP 1 LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id FROM LH_PECULIARITY_SUB_HOME_SELL WHERE LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id=12 AND LH_PECULIARITY_SUB_HOME_SELL.plan_id=LH_PLANS.plan_id) as 'ตกแต่งห้องน้ำ_พร้อมสุขภัณฑ์อัตโนมัติ',
(SELECT TOP 1 LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id FROM LH_PECULIARITY_SUB_HOME_SELL WHERE LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id=13 AND LH_PECULIARITY_SUB_HOME_SELL.plan_id=LH_PLANS.plan_id) as 'นวัตกรรม_AIR_PLUS',
(SELECT TOP 1 LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id FROM LH_PECULIARITY_SUB_HOME_SELL WHERE LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id=14 AND LH_PECULIARITY_SUB_HOME_SELL.plan_id=LH_PLANS.plan_id) as 'ระบบกำจัดปลวก',
(SELECT TOP 1 LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id FROM LH_PECULIARITY_SUB_HOME_SELL WHERE LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id=15 AND LH_PECULIARITY_SUB_HOME_SELL.plan_id=LH_PLANS.plan_id) as 'SECURITY_ALARM_SYSTEM',
(SELECT TOP 1 LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id FROM LH_PECULIARITY_SUB_HOME_SELL WHERE LH_PECULIARITY_SUB_HOME_SELL.peculiarity_id=16 AND LH_PECULIARITY_SUB_HOME_SELL.plan_id=LH_PLANS.plan_id) as 'REMOTE_CONTROL_ENTRANCE',

LH_CONDITION_HOME_SELL.land_size, LH_CONDITION_HOME_SELL.total_price, LH_CONDITION_HOME_SELL.down, LH_CONDITION_HOME_SELL.money, LH_CONDITION_HOME_SELL.payments, LH_CONDITION_HOME_SELL.contract, LH_CONDITION_HOME_SELL.transfer_money, LH_CONDITION_HOME_SELL.starting_pay, LH_CONDITION_HOME_SELL.interest, LH_CONDITION_HOME_SELL.repayment_period,LH_HOME_SELL.number_converter

FROM LH_HOME_SELL
LEFT JOIN LH_PROJECT_SUB ON LH_PROJECT_SUB.project_sub_id = LH_HOME_SELL.project_sub_id
LEFT JOIN LH_PROJECTS ON LH_PROJECTS.project_id = LH_PROJECT_SUB.project_id
LEFT JOIN LH_PLANS ON LH_PLANS.plan_id = LH_HOME_SELL.plan_id
LEFT JOIN LH_PRODUCTS ON LH_PRODUCTS.product_id=LH_PROJECT_SUB.product_id
LEFT JOIN LH_CONDITION_HOME_SELL ON LH_CONDITION_HOME_SELL.home_sell_id=LH_HOME_SELL.home_sell_id
WHERE (LH_PROJECTS.project_status != 'SO' OR LH_PROJECTS.project_status is NULL) AND (LH_HOME_SELL.home_sell_status  != 'SO' OR LH_HOME_SELL.home_sell_status is NULL )");

$NEWHOME = new FluidXml('table');
$NEWHOME->setAttribute('name', 'newhome');
$NEWHOME->add('GeneratedDateTime', date("d/m/Y H:i:s"));

//define main query fields & data type
$field_array=[
		["label"=>"NEWHOME_ID","field"=>"home_sell_id","data_type"=>"varchar"],
		["label"=>"PROJECT_ID","field"=>"project_id","data_type"=>"varchar"],
		["label"=>"MODEL_NAME","field"=>"plan_name_en","data_type"=>"varchar"],
		["label"=>"NEWHOME_FEATURE","field"=>"features_convert","data_type"=>"varchar"],
		["label"=>"NEWHOME_FLAG","field"=>"","data_type"=>"char"],
		["label"=>"NEWHOME_TYPE","field"=>"product_id","data_type"=>"char"],
		["label"=>"BUILD_AREA","field"=>"built_on","data_type"=>"int"],
		["label"=>"USEFUL_AREA","field"=>"plan_useful","data_type"=>"int"],
		["label"=>"PIC_NEWHOME","field"=>"plan_img","data_type"=>"varchar"],
		["label"=>"PIC_NEWHOME_TEXT","field"=>"plan_seo","data_type"=>"varchar"],
		["label"=>"PIC_FLOOR1","field"=>"floor1_img","data_type"=>"varchar"],
		["label"=>"PIC_FLOOR2","field"=>"floor2_img","data_type"=>"varchar"],
		["label"=>"PIC_FLOOR3","field"=>"floor3_img","data_type"=>"varchar"],
		["label"=>"NUM_BEDROOM","field"=>"BEDROOM","data_type"=>"smallint"],
		["label"=>"NUM_BATHROOM","field"=>"TOILET","data_type"=>"smallint"],
		["label"=>"NUM_LOBBYROOM","field"=>"DRAWING_ROOM","data_type"=>"smallint"],
		["label"=>"NUM_SERVANTROOM","field"=>"SERVANTROOM","data_type"=>"smallint"],
		["label"=>"NUM_CARPARK","field"=>"PARKING","data_type"=>"smallint"],
		["label"=>"NUM_WORKINGROOM","field"=>"WORKING_ROOM","data_type"=>"smallint"],
		["label"=>"NUM_PRAY_ROOM","field"=>"PEACE_ROOM","data_type"=>"smallint"],
		["label"=>"NUM_GUEST_ROOM","field"=>"LIVING_ROOM","data_type"=>"smallint"],
		["label"=>"REQ_GARDEN","field"=>"ตกแต่งสวนภายนอกบ้าน_ENG","data_type"=>"char"],
		["label"=>"REQ_FLOOR","field"=>"ตกแต่งพื้นภายใน","data_type"=>"char"],
		["label"=>"REQ_BATHROOM","field"=>"ตกแต่งห้องน้ำ_พร้อมสุขภัณฑ์","data_type"=>"char"],
		["label"=>"REQ_FURNITURE","field"=>"ตกแต่งเฟอร์นิเจอร์__แต่งเพิ่มพิเศษจากมาตรฐานที่มี_เช่น_แต่งเฟอร์ทั้งหลัง","data_type"=>"varchar"],
		["label"=>"REQ_AIR_CONDITION","field"=>"ติดตั้งเครื่องปรับอากาศ","data_type"=>"char"],
		["label"=>"REQ_WALLPAPER","field"=>"ผนังบุวอลล์เปเปอร์","data_type"=>"char"],
		["label"=>"REQ_WATER_MACHINE","field"=>"แทงค์น้ำ_ปั๊มน้ำ","data_type"=>"char"],
		["label"=>"REQ_ANTI_TERMITE","field"=>"ระบบกำจัดปลวก","data_type"=>"char"],
		["label"=>"REQ_THAI_KITCHEN","field"=>"ครัวไทย","data_type"=>"char"],
		["label"=>"REQ_THAI_KITCHEN_FURNITURE","field"=>"ครัวไทยพร้อมเฟอร์นิเจอร์","data_type"=>"char"],
		["label"=>"REQ_BATHROOM_INTELLIGENCE","field"=>"ตกแต่งห้องน้ำ_พร้อมสุขภัณฑ์อัจฉริยะ","data_type"=>"char"],
		["label"=>"REQ_AIR_PLUS","field"=>"นวัตกรรม_AIR_PLUS","data_type"=>"char"],
		["label"=>"TOTAL_AREA","field"=>"land_size","data_type"=>"int"],
		["label"=>"NET_PRICE","field"=>"total_price","data_type"=>"int"],
		["label"=>"DOWN_PERCENT","field"=>"down","data_type"=>"int"],
		["label"=>"DOWN_PRICE","field"=>"money","data_type"=>"int"],
		["label"=>"BOOK_MONEY","field"=>"payments","data_type"=>"int"],
		["label"=>"BOOK_CONTACT","field"=>"contract","data_type"=>"int"],
		["label"=>"ORDERING1","field"=>"","data_type"=>"int"],
		["label"=>"ORDERING2","field"=>"","data_type"=>"int"],
		["label"=>"PAY_PER_ORDER","field"=>"","data_type"=>"int"],
		["label"=>"TRANSFER_MONEY","field"=>"transfer_money","data_type"=>"int"],
		["label"=>"LOOSEN_PER_MONTH","field"=>"starting_pay","data_type"=>"int"],
		["label"=>"INTEREST_FOR_BANK","field"=>"interest","data_type"=>"decimal"],
		["label"=>"YEAR_FOR_INTEREST","field"=>"repayment_period","data_type"=>"smallint"],
		["label"=>"CREATE_USER","field"=>"","data_type"=>"char"],
		["label"=>"CREATE_DATE","field"=>"creat_date","data_type"=>"date"],
		["label"=>"LAST_USER","field"=>"","data_type"=>"char"],
		["label"=>"LAST_DATE","field"=>"update_date","data_type"=>"date"],
		["label"=>"APPROVED","field"=>"","data_type"=>"char"],
		["label"=>"NEW_ENTRY_FLAG","field"=>"","data_type"=>"char"],
		["label"=>"SOLD_FLAG","field"=>"home_sell_status","data_type"=>"char"],
		["label"=>"SOLD_EXPIREDATE","field"=>"","data_type"=>"date"],
		["label"=>"BUDGET_CODE","field"=>"","data_type"=>"varchar"],
		["label"=>"STYLEID","field"=>"","data_type"=>"int"],
		["label"=>"HOMEMODEL_ID","field"=>"plan_id","data_type"=>"int"],
		["label"=>"MODEL_SPECIFIC","field"=>"features_convert","data_type"=>"varchar"],
		["label"=>"LAND_TYPE_CODE","field"=>"","data_type"=>"char"],
		["label"=>"LAND_NUMBER","field"=>"number_converter","data_type"=>"char"],
		["label"=>"FIRST_BOOKING_BEGIN","field"=>"","data_type"=>"date"],
		["label"=>"FIRST_BOOKING_END","field"=>"","data_type"=>"date"],
		["label"=>"IS_FIRST_HOME","field"=>"","data_type"=>"char"],
		["label"=>"FIRST_HOME_BUDGET_CODE","field"=>"","data_type"=>"char"],
		["label"=>"SPECIAL_TYPE","field"=>"","data_type"=>"char"],
		["label"=>"SPECIALS4DAY","field"=>"","data_type"=>"char"],
		["label"=>"TOP_BEST","field"=>"","data_type"=>"varchar"],
		["label"=>"IS_HOME_FURNISHED","field"=>"house","data_type"=>"char"],
];
//main loop

while ($result=mssql_fetch_array($query)) {
$pattern['rowdata']=[]; //pattern prepare to blank array
	//put array fields to pattern
	foreach ($field_array as $key => $fields) {
		$field=(string)$fields['field'];
		$label=(string)$fields['label'];
		$type=(string)$fields['data_type'];
		//separate Yes/No
		if ($key >=21 && $key <= 32) {
			$value=($result["$field"]!=''?"<![CDATA[Y]]>":"<![CDATA[N]]>");
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}elseif($label=="NET_PRICE" || $label=="DOWN_PRICE" || $label=="BOOK_MONEY" || $label=="BOOK_CONTACT" || $label=="TRANSFER_MONEY" || $label=="LOOSEN_PER_MONTH"){
			$value="<![CDATA[".(str_replace(',', '', $result["$field"])+0)."]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}elseif($label=="TOTAL_AREA"){
			$value="<![CDATA[".(str_replace(',', '', $result["$field"])+0)."]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}elseif($label=="MODEL_SPECIFIC" || $label=="NEWHOME_FEATURE"){
			$value="<![CDATA[".str_replace(array("\r\n","\r","\n","\\r","\\n","\\r\\n")," ",preg_replace('/\s+/', ' ', $result["$field"]))."]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}elseif($key ==1){
			$value="<![CDATA[".$result["$field"]."]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
			$pattern['rowdata']+=["COMPANY_NAME"=>['@type'=>"varchar","<![CDATA[บริษัท แลนด์ แอนด์ เฮ้าส์ จำกัด (มาหาชน)]]>"]];
		}elseif($label=="DOWN_PERCENT"){
			$value="<![CDATA[".floatval($result["down"]+0)."]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}elseif($fields['field']==''){
			$value="<![CDATA[]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}else{
			$value="<![CDATA[".$result["$field"]."]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}
	}

	//add company
	// $pattern['rowdata']+=["COMPANY_NAME"=>['@type'=>"varchar","<![CDATA[บริษัท แลนด์ แอนด์ เฮ้าส์ จำกัด (มาหาชน)]]>"]];

	//Image query 3 type of home_sell
	#TYPE 1 FROM UPLOAD
	$img_type1_query=mssql_query("SELECT LH_UPLOAD_GALERY_HOME_SELL.path_upload_gallery FROM LH_GALERY_HOME_SELL_MAIN LEFT JOIN LH_UPLOAD_GALERY_HOME_SELL ON LH_UPLOAD_GALERY_HOME_SELL.galery_home_sell_id = LH_GALERY_HOME_SELL_MAIN.galery_home_sell_id WHERE home_sell_id={$result['home_sell_id']} AND LH_GALERY_HOME_SELL_MAIN.type_galery=1");
	if (mssql_num_rows($img_type1_query)>0) {
		$i=1;
		while (($img_type1_r=mssql_fetch_object($img_type1_query)) && ($i<=4)) {

			$pattern['rowdata']+=["PIC_INSIDE$i"=>['@type'=>"varchar","<![CDATA[$img_type1_r->path_upload_gallery]]>"]];
			$i++;
		}
	}

	$img_type1_num=4-mssql_num_rows($img_type1_query);
	if($img_type1_num <= 4 && $img_type1_num >= 1){
		$x=mssql_num_rows($img_type1_query)+1;
		for ($i=1; $i <= $img_type1_num ; $i++) {
			$pattern['rowdata']+=["PIC_INSIDE$x"=>['@type'=>"varchar","<![CDATA[]]>"]];
			$x++;
		}
	}

	#TYPE 2 FROM PLAN
	$img_type2_query=mssql_query("SELECT LH_GALERY_PLAN.galery_plan_name,LH_GALERY_PLAN.galery_plan_img_seo FROM LH_GALERY_HOME_SELL_MAIN LEFT JOIN LH_GALERY_PLAN ON LH_GALERY_PLAN.galery_plan_id = LH_GALERY_HOME_SELL_MAIN.galery_plan_id WHERE home_sell_id={$result['home_sell_id']} AND LH_GALERY_HOME_SELL_MAIN.type_galery=2");
	if (mssql_num_rows($img_type2_query)>0) {
		$i=1;
		while (($img_type2_r=mssql_fetch_object($img_type2_query)) && ($i<=1)) {
			$pattern['rowdata']+=["PIC_NEWHOME_SMALL"=>['@type'=>"varchar","<![CDATA[$img_type2_r->galery_plan_name]]>"]];
			$pattern['rowdata']+=["PIC_NEWHOME_SMALL_TEXT"=>['@type'=>"varchar","<![CDATA[$img_type2_r->galery_plan_img_seo]]>"]];
			$i++;
		}
	}
	$img_type2_num=1-mssql_num_rows($img_type2_query);
	if($img_type2_num == 1){
		$x=mssql_num_rows($img_type2_query)+1;
		for ($i=1; $i <= $img_type2_num ; $i++) {
			$pattern['rowdata']+=["PIC_NEWHOME_SMALL"=>['@type'=>"varchar","<![CDATA[]]>"]];
			$pattern['rowdata']+=["PIC_NEWHOME_SMALL_TEXT"=>['@type'=>"varchar","<![CDATA[]]>"]];
			$x++;
		}
	}

	#TYPE 3 FROM FUNISHED
	$img_type3_query=mssql_query("SELECT LH_GALERY_FURNISHED_SUB.path_funished FROM LH_GALERY_HOME_SELL_MAIN LEFT JOIN LH_GALERY_FURNISHED_SUB ON LH_GALERY_FURNISHED_SUB.galery_funished_sub_id = LH_GALERY_HOME_SELL_MAIN.galery_funished_sub_id WHERE home_sell_id={$result['home_sell_id']} AND LH_GALERY_HOME_SELL_MAIN.type_galery=3");
	if (mssql_num_rows($img_type3_query)>0) {
		$i=1;
		while (($img_type3_r=mssql_fetch_object($img_type3_query)) && ($i<=1)) {
			$pattern['rowdata']+=["PIC_HOME_FURNISHED_HIGHLIGHT"=>['@type'=>"varchar","<![CDATA[$img_type3_r->path_funished]]>"]];
			$i++;
		}
	}

	$img_type3_num=1-mssql_num_rows($img_type3_query);
	if($img_type3_num == 1){
		$x=mssql_num_rows($img_type3_query)+1;
		for ($i=1; $i <= $img_type3_num ; $i++) {
			$pattern['rowdata']+=["PIC_HOME_FURNISHED_HIGHLIGHT"=>['@type'=>"varchar","<![CDATA[]]>"]];
			$x++;
		}
	}

	$sort_pattern=['NEWHOME_ID', 'PROJECT_ID', 'COMPANY_NAME', 'MODEL_NAME', 'NEWHOME_FEATURE', 'NEWHOME_FLAG', 'NEWHOME_TYPE', 'BUILD_AREA', 'USEFUL_AREA', 'PIC_NEWHOME', 'PIC_NEWHOME_TEXT', 'PIC_FLOOR1', 'PIC_FLOOR2', 'PIC_INSIDE1', 'PIC_INSIDE2', 'NUM_BEDROOM', 'NUM_BATHROOM', 'NUM_LOBBYROOM', 'NUM_SERVANTROOM', 'NUM_CARPARK', 'REQ_GARDEN', 'REQ_FLOOR', 'REQ_BATHROOM', 'REQ_FURNITURE', 'REQ_AIR_CONDITION', 'REQ_WALLPAPER', 'REQ_WATER_MACHINE', 'REQ_ANTI_TERMITE', 'REQ_THAI_KITCHEN', 'TOTAL_AREA', 'NET_PRICE', 'DOWN_PERCENT', 'DOWN_PRICE', 'BOOK_MONEY', 'BOOK_CONTACT', 'ORDERING1', 'ORDERING2', 'PAY_PER_ORDER', 'TRANSFER_MONEY', 'LOOSEN_PER_MONTH', 'INTEREST_FOR_BANK', 'YEAR_FOR_INTEREST', 'CREATE_USER', 'CREATE_DATE', 'LAST_USER', 'LAST_DATE', 'PIC_NEWHOME_SMALL', 'PIC_NEWHOME_SMALL_TEXT', 'APPROVED', 'PIC_FLOOR3', 'NEW_ENTRY_FLAG', 'NUM_WORKINGROOM', 'SOLD_FLAG', 'SOLD_EXPIREDATE', 'BUDGET_CODE', 'STYLEID', 'HOMEMODEL_ID', 'MODEL_SPECIFIC', 'PIC_INSIDE3', 'PIC_INSIDE4', 'LAND_TYPE_CODE', 'LAND_NUMBER', 'FIRST_BOOKING_BEGIN', 'FIRST_BOOKING_END', 'IS_FIRST_HOME', 'FIRST_HOME_BUDGET_CODE', 'SPECIAL_TYPE', 'SPECIALS4DAY', 'TOP_BEST', 'REQ_THAI_KITCHEN_FURNITURE', 'REQ_BATHROOM_INTELLIGENCE', 'REQ_AIR_PLUS', 'NUM_PRAY_ROOM', 'IS_HOME_FURNISHED', 'NUM_GUEST_ROOM', 'PIC_HOME_FURNISHED_HIGHLIGHT'];
	$pattern_sorted['rowdata'] = array_merge(array_flip($sort_pattern), $pattern['rowdata']);

	//add all data to function
	$NEWHOME->add($pattern_sorted);
}

$NEWHOME->save(OUTPUT_PATH.'NEWHOME.xml');
$pattern=NULL;
echo "NEWHOME . . . Done<br>";

/**
 * ################################
 * 8) SECTION FOR CONDO_PROJECT_PROFILE_T
 * ################################
 * old -> CONDO_PROJECT_PROFILE_T
 * new -> LH_PROJECTS (Condo)
 */

//start main query
$query=mssql_query("SELECT LH_PROJECTS.project_id,LH_PROJECTS.project_name_th,LH_PROJECT_CONCEPT.concept_th,LH_PROJECTS.area_farm,LH_PROJECTS.area_square_m,LH_PROJECTS.area_square_w,LH_PROJECTS.area_land,LH_PROJECT_AREA_CONDO.building_unit_id,LH_PROJECT_AREA_CONDO.area_condo,LH_PROJECT_PRICES.project_price,LH_PROJECTS.location,LH_PROJECT_CONTACTS.open_day,LH_PROJECT_CONTACTS.open_time_start,LH_PROJECT_CONTACTS.open_time_end,LH_PROJECT_CONTACTS.telephone,LH_PROJECT_CONTACTS.email,LH_PROJECT_MAPS.map_img,LH_PROJECT_MAPS.map_pdf,LH_PROJECTS.latitude,LH_PROJECTS.longtitude,LH_BRANDS.brand_id,LH_BRANDS.brand_name_th,convert(VARCHAR(10), LH_PROJECTS.create_date, 103) + ' '  + convert(VARCHAR(8),  LH_PROJECTS.create_date, 14) create_date,convert(VARCHAR(10), LH_PROJECTS.updated_date, 103) + ' '  + convert(VARCHAR(8),  LH_PROJECTS.updated_date, 14) updated_date,LH_PROJECT_AREA_CONDO.num_plan_condo,LH_PROJECTS.project_status,LH_PROJECT_ZONE.zone_id,LH_PROJECT_ZONE.province_id,LH_PROJECT_ZONE.amphur_id,LH_360_PROJECT.c360_project_url,

(SELECT  TOP 1 SUM(CONVERT(INT, REPLACE(LH_BUILDING_UNIT.building_unit, CHAR(0), ''))) FROM LH_BUILDING_UNIT WHERE LH_BUILDING_UNIT.project_area_condo_id=LH_PROJECT_AREA_CONDO.project_area_condo_id GROUP BY LH_BUILDING_UNIT.project_area_condo_id) as building_unit,

(SELECT TOP 1 LH_BUILDING_UNIT.building_num_layer FROM LH_BUILDING_UNIT WHERE project_area_condo_id=LH_PROJECT_AREA_CONDO.project_area_condo_id ORDER BY building_num_layer DESC) as building_num_layer,

(SELECT TOP 1 LH_PROGRESS_UPDATE_CONDO.progress_update_mount FROM LH_PROGRESS_UPDATE_CONDO WHERE project_sub_id=LH_PROJECT_SUB.project_sub_id ORDER BY progress_update_mount DESC) as progress_update_mount

FROM LH_PROJECTS
LEFT JOIN LH_PROJECT_SUB ON LH_PROJECT_SUB.project_id = LH_PROJECTS.project_id
LEFT JOIN LH_PRODUCTS ON LH_PRODUCTS.product_id=LH_PROJECT_SUB.product_id
LEFT JOIN LH_PROJECT_CONCEPT ON LH_PROJECT_CONCEPT.project_id = LH_PROJECTS.project_id
LEFT JOIN LH_PROJECT_AREA_CONDO ON LH_PROJECT_AREA_CONDO.project_sub_id=LH_PROJECT_SUB.project_sub_id
LEFT JOIN LH_PROJECT_PRICES ON LH_PROJECT_PRICES.project_id = LH_PROJECTS.project_id
LEFT JOIN LH_PROJECT_CONTACTS ON LH_PROJECT_CONTACTS.project_id =LH_PROJECTS.project_id
LEFT JOIN LH_PROJECT_MAPS ON LH_PROJECT_MAPS.project_id=LH_PROJECTS.project_id
LEFT JOIN LH_360_PROJECT ON LH_360_PROJECT.project_id=LH_PROJECTS.project_id
LEFT JOIN LH_BRANDS ON LH_BRANDS.brand_id = LH_PROJECTS.brand_id
LEFT JOIN LH_PROJECT_ZONE ON LH_PROJECT_ZONE.project_id = LH_PROJECTS.project_id
WHERE LH_PRODUCTS.product_name_en='Condominium'
AND LH_PROJECTS.project_status != 'SO'");

$CONDO_PROJECT_PROFILE_T = new FluidXml('table');
$CONDO_PROJECT_PROFILE_T->setAttribute('name', 'condo_project_profile_t');
$CONDO_PROJECT_PROFILE_T->add('GeneratedDateTime', date("d/m/Y H:i:s"));

//define main query fields & data type
$field_array=[
		["label"=>"CONDO_PROJECT_ID","field"=>"project_id","data_type"=>"int"],
		["label"=>"CONDO_PROJECT_NAME","field"=>"project_name_th","data_type"=>"varchar"],
		["label"=>"LOCATION_CODE","field"=>"province_id","data_type"=>"varchar"],
		["label"=>"SUBLOCATION_CODE","field"=>"zone_id","data_type"=>"varchar"],
		["label"=>"PROJECT_CONCEPT","field"=>"concept_th","data_type"=>"varchar"],
		["label"=>"AREA_RAI","field"=>"area_farm","data_type"=>"decimal"],
		["label"=>"AREA_NGAN","field"=>"area_square_m","data_type"=>"decimal"],
		["label"=>"AREA_WA2","field"=>"area_square_w","data_type"=>"decimal"],
		["label"=>"NUM_BUILDING","field"=>"building_unit_id","data_type"=>"smallint"],
		["label"=>"NUM_UNIT_ALL","field"=>"building_unit","data_type"=>"smallint"],
		["label"=>"BEGIN_USEFUL_AREA","field"=>"area_condo","data_type"=>"decimal"],
		["label"=>"END_USEFUL_AREA","field"=>"area_condo","data_type"=>"decimal"],
		["label"=>"BEGIN_PRICE","field"=>"project_price","data_type"=>"decimal"],
		["label"=>"R_AIR_FLAG","field"=>"","data_type"=>"smallint"],
		["label"=>"R_FURNITURE_FLAG","field"=>"","data_type"=>"smallint"],
		["label"=>"R_ELECTRICITY_FLAG","field"=>"","data_type"=>"smallint"],
		["label"=>"R_WATER_FLAG","field"=>"","data_type"=>"smallint"],
		["label"=>"R_VERANDA_FLAG","field"=>"","data_type"=>"smallint"],
		["label"=>"R_INTERNET_CONNECT_FLAG","field"=>"","data_type"=>"smallint"],
		["label"=>"R_CABLE_TV_FLAG","field"=>"","data_type"=>"smallint"],
		["label"=>"R_DIRECT_TEL_FLAG","field"=>"","data_type"=>"smallint"],
		["label"=>"R_WATERWARM_MACHINE_FLAG","field"=>"","data_type"=>"smallint"],
		["label"=>"R_OTHER_FLAG","field"=>"","data_type"=>"smallint"],
		["label"=>"R_OTHER_DETAIL","field"=>"","data_type"=>"varchar"],
		["label"=>"P_SWIMMING_POOL_FLAG","field"=>"","data_type"=>"smallint"],
		["label"=>"P_LAUNDRY_FLAG","field"=>"","data_type"=>"smallint"],
		["label"=>"P_FITNESS_FLAG","field"=>"","data_type"=>"smallint"],
		["label"=>"P_GARDEN_FLAG","field"=>"","data_type"=>"smallint"],
		["label"=>"P_MINIMART_FLAG","field"=>"","data_type"=>"smallint"],
		["label"=>"P_CARPARK_FLAG","field"=>"","data_type"=>"smallint"],
		["label"=>"P_SECURITY_FLAG","field"=>"","data_type"=>"smallint"],
		["label"=>"P_OTHER_FLAG","field"=>"","data_type"=>"smallint"],
		["label"=>"P_OTHER_DETAIL","field"=>"","data_type"=>"varchar"],
		["label"=>"WORKING_DAY_FLAG","field"=>"open_day","data_type"=>"smallint"],
		["label"=>"WORKING_DAY_DETAIL","field"=>"","data_type"=>"varchar"],
		["label"=>"BEGIN_WORKING_TIME","field"=>"open_time_start","data_type"=>"varchar"],
		["label"=>"END_WORKING_TIME","field"=>"open_time_end","data_type"=>"varchar"],
		["label"=>"CONTACT_ADDRESS","field"=>"location","data_type"=>"varchar"],
		["label"=>"CONTACT_TEL","field"=>"telephone","data_type"=>"varchar"],
		["label"=>"CONTACT_EMAIL","field"=>"email","data_type"=>"varchar"],
		["label"=>"PIC_MAP","field"=>"map_img","data_type"=>"varchar"],
		["label"=>"PIC_CONDO","field"=>"","data_type"=>"varchar"],
		["label"=>"STATUS","field"=>"","data_type"=>"char"],
		["label"=>"CREATE_DATE","field"=>"create_date","data_type"=>"datetime"],
		["label"=>"UPDATE_DATE","field"=>"updated_date","data_type"=>"datetime"],
		["label"=>"USER_CODE","field"=>"","data_type"=>"varchar"],
		["label"=>"LAST_USER_CODE","field"=>"","data_type"=>"varchar"],
		["label"=>"COMPANY_NAME","field"=>"","data_type"=>"varchar"],
		["label"=>"REGION_CODE","field"=>"amphur_id","data_type"=>"varchar"],
		["label"=>"PROJECT_BUILD_STATUS","field"=>"","data_type"=>"char"],
		["label"=>"NUM_ROOM_TYPE","field"=>"num_plan_condo","data_type"=>"smallint"],
		["label"=>"MAP_PDF","field"=>"map_pdf","data_type"=>"varchar"],
		["label"=>"URL_360","field"=>"c360_project_url","data_type"=>"varchar"],
		["label"=>"WALK_FLV","field"=>"","data_type"=>"varchar"],
		["label"=>"LATITUDE","field"=>"latitude","data_type"=>"varchar"],
		["label"=>"LONGITUDE","field"=>"longtitude","data_type"=>"varchar"],
		["label"=>"ENVIRONMENT","field"=>"","data_type"=>"char"],
		["label"=>"BRAND_ID","field"=>"brand_id","data_type"=>"char"],
		["label"=>"BEGIN_PRICE_TEXT","field"=>"project_price","data_type"=>"varchar"],
		["label"=>"ORDER_IN_BRAND","field"=>"","data_type"=>"int"],
		["label"=>"ORDER_UPDATE_DATE","field"=>"","data_type"=>"date"],
		["label"=>"NUM_FLOOR","field"=>"building_num_layer","data_type"=>"smallint"],
];
//main loop

while ($result=mssql_fetch_array($query)) {
$pattern['rowdata']=[]; //pattern prepare to blank array
	//put array fields to pattern
	foreach ($field_array as $key => $fields) {
		$field=(string)$fields['field'];
		$label=(string)$fields['label'];
		$type=(string)$fields['data_type'];
		//separate Yes/No
		// if ($key >=21 && $key <= 33) {
		// 	$value=($result["$field"]!=''?"<![CDATA[Y]]>":"<![CDATA[N]]>");
		// 	$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		// }elseif($key==1){
		// 	//add company
		// 	$value="<![CDATA[".$result["$field"]."]]>";
		// 	$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		// 	$pattern['rowdata']+=["COMPANY_NAME"=>['@type'=>"varchar","<![CDATA[บริษัท แลนด์ แอนด์ เฮ้าส์ จำกัด (มาหาชน)]]>"]];
		// }

		$area_condo=$result["area_condo"];
		if (strpos($area_condo, '-') !== false) {
		    $useful_area=explode("-",$area_condo);
		}else{
			$useful_area=[$area_condo,$area_condo];
		}
		if ($label=="BEGIN_USEFUL_AREA") {
			$value="<![CDATA[".$useful_area[0]."]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}elseif($label=="BEGIN_PRICE" || $label=="BEGIN_PRICE_TEXT"){
			$price=explode("-",$result["$field"]);
			if ($label=="BEGIN_PRICE" && count($price)>1) {
				$value="<![CDATA[".(int) preg_replace("/[^0-9]/","",$price[0])."]]>";
			}elseif($label=="BEGIN_PRICE_TEXT" && count($price)>1){
				$value="<![CDATA[".(int) preg_replace("/[^0-9]/","",$price[1])."]]>";
			}else{
				$value="<![CDATA[".(int) preg_replace("/[^0-9]/","",$price[0])."]]>";
			}
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}elseif($label=="MAP_PDF"){
			$map_pdf_file[]=(count(explode("/",$result["$field"]))>1?str_replace(":",":",explode("/",$result["$field"])[4]):'');
			$map_stg=(count(explode("/",$result["$field"]))>1?str_replace(":","_",explode("/",$result["$field"])[4]):'');
			$value="<![CDATA[".$map_stg."]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}elseif($label=="WORKING_DAY_FLAG"){
			$value="<![CDATA[]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}elseif($label=="PROJECT_CONCEPT"){
			$value="<![CDATA[".str_replace(array("\r\n","\r","\n","\\r","\\n","\\r\\n")," ",preg_replace('/\s+/', ' ', $result['concept_th']))."]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}elseif ($label=="END_USEFUL_AREA") {
			$value="<![CDATA[".$useful_area[1]."]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}elseif($label=="END_USEFUL_AREA"){

		}elseif($field=='COMPANY_NAME'){
			//if field blank
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","<![CDATA[บริษัท แลนด์ แอนด์ เฮ้าส์ จำกัด (มาหาชน)]]>"]];
		}elseif($field==''){
			$value="<![CDATA[]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}else{
			$value="<![CDATA[".$result["$field"]."]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}
	}



	//add condo near by
	$near_query=mssql_query("SELECT LH_PROJECT_NEARBY.nearby_name_th,interval FROM LH_PROJECT_NEARBY WHERE project_id={$result['project_id']}");
	if (mssql_num_rows($near_query)>0) {
		$i=1;
		while (($near_r=mssql_fetch_object($near_query)) && ($i <= 4) ) {
			$pattern['rowdata']+=["NEAR_PLACE_NAME$i"=>['@type'=>"varchar","<![CDATA[$near_r->nearby_name_th]]>"]];
			$pattern['rowdata']+=["NEAR_PLACE_RANGE$i"=>['@type'=>"varchar","<![CDATA[$near_r->interval]]>"]];
			$i++;
		}
	}
	$near_by_num=4-mssql_num_rows($near_query);
	if($near_by_num <= 4 && $near_by_num >= 1){
		$x=mssql_num_rows($near_query)+1;
		for ($i=1; $i <= $near_by_num ; $i++) {
			$pattern['rowdata']+=["NEAR_PLACE_NAME$x"=>['@type'=>"varchar","<![CDATA[]]>"]];
			$pattern['rowdata']+=["NEAR_PLACE_RANGE$x"=>['@type'=>"varchar","<![CDATA[]]>"]];
			$x++;
		}
	}

	//add Master Plan IMG
	$master_plan_query=mssql_query("SELECT LH_MASTER_PLAN_CONDO.master_plan_img_name FROM LH_MASTER_PLAN_CONDO WHERE project_id={$result['project_id']}");
	if (mssql_num_rows($master_plan_query)>0) {
		$i=1;
		while (($master_plan_r=mssql_fetch_object($master_plan_query))  && ($i <= 2)  ) {
			$pattern['rowdata']+=["PIC_BIG_PLAN$i"=>['@type'=>"varchar","<![CDATA[$master_plan_r->master_plan_img_name]]>"]];
			$i++;
		}
	}
	$master_plan_num=2-mssql_num_rows($master_plan_query);
	if($master_plan_num <= 2 && $master_plan_num >= 1){
		$x=mssql_num_rows($master_plan_query)+1;
		for ($i=1; $i <= $master_plan_num ; $i++) {
			$pattern['rowdata']+=["PIC_BIG_PLAN$x"=>['@type'=>"varchar","<![CDATA[]]>"]];
			$x++;
		}
	}

	//add Unit Plan IMG
	$unit_plan_query=mssql_query("SELECT LH_UNIT_PLAN_IMG.unit_plan_img_name FROM LH_UNIT_PLAN_IMG LEFT JOIN LH_UNIT_PLAN_CONDO ON LH_UNIT_PLAN_CONDO.unit_plan_id = LH_UNIT_PLAN_IMG.unit_plan_id WHERE LH_UNIT_PLAN_CONDO.project_id={$result['project_id']}");
	if ((mssql_num_rows($unit_plan_query)>0)) {
		$i=1;
		while (($unit_plan_r=mssql_fetch_object($unit_plan_query)) && ($i <= 2)) {
			$pattern['rowdata']+=["PIC_SMALL_PLAN$i"=>['@type'=>"varchar","<![CDATA[$unit_plan_r->unit_plan_img_name]]>"]];
			$i++;
		}
	}
	$unit_plan_num=2-mssql_num_rows($unit_plan_query);
	if($unit_plan_num <= 2 && $unit_plan_num >= 1){
		$x=mssql_num_rows($unit_plan_query)+1;
		for ($i=1; $i <= $unit_plan_num ; $i++) {
			$pattern['rowdata']+=["PIC_SMALL_PLAN$x"=>['@type'=>"varchar","<![CDATA[]]>"]];
			$x++;
		}
	}

	$sort_pattern=['CONDO_PROJECT_ID', 'CONDO_PROJECT_NAME', 'LOCATION_CODE', 'SUBLOCATION_CODE', 'PROJECT_CONCEPT', 'AREA_RAI', 'AREA_NGAN', 'AREA_WA2', 'NUM_BUILDING', 'NUM_UNIT_ALL', 'BEGIN_USEFUL_AREA', 'END_USEFUL_AREA', 'BEGIN_PRICE', 'NEAR_PLACE_NAME1', 'NEAR_PLACE_NAME2', 'NEAR_PLACE_NAME3', 'NEAR_PLACE_NAME4', 'NEAR_PLACE_RANGE1', 'NEAR_PLACE_RANGE2', 'NEAR_PLACE_RANGE3', 'NEAR_PLACE_RANGE4', 'R_AIR_FLAG', 'R_FURNITURE_FLAG', 'R_ELECTRICITY_FLAG', 'R_WATER_FLAG', 'R_VERANDA_FLAG', 'R_INTERNET_CONNECT_FLAG', 'R_CABLE_TV_FLAG', 'R_DIRECT_TEL_FLAG', 'R_WATERWARM_MACHINE_FLAG', 'R_OTHER_FLAG', 'R_OTHER_DETAIL', 'P_SWIMMING_POOL_FLAG', 'P_LAUNDRY_FLAG', 'P_FITNESS_FLAG', 'P_GARDEN_FLAG', 'P_MINIMART_FLAG', 'P_CARPARK_FLAG', 'P_SECURITY_FLAG', 'P_OTHER_FLAG', 'P_OTHER_DETAIL', 'WORKING_DAY_FLAG', 'WORKING_DAY_DETAIL', 'BEGIN_WORKING_TIME', 'END_WORKING_TIME', 'CONTACT_ADDRESS', 'CONTACT_TEL', 'CONTACT_EMAIL', 'PIC_BIG_PLAN1', 'PIC_BIG_PLAN2', 'PIC_SMALL_PLAN1', 'PIC_SMALL_PLAN2', 'PIC_MAP', 'PIC_CONDO', 'PIC_BIG_BUILDING1', 'PIC_BIG_BUILDING2', 'PIC_SMALL_BUILDING1', 'PIC_SMALL_BUILDING2', 'STATUS', 'CREATE_DATE', 'UPDATE_DATE', 'USER_CODE', 'LAST_USER_CODE', 'COMPANY_NAME', 'REGION_CODE', 'PROJECT_BUILD_STATUS', 'NUM_ROOM_TYPE', 'MAP_PDF', 'URL_360', 'WALK_FLV', 'LATITUDE', 'LONGITUDE', 'ENVIRONMENT', 'BRAND_ID', 'BEGIN_PRICE_TEXT', 'ORDER_IN_BRAND', 'ORDER_UPDATE_DATE', 'NUM_FLOOR'];
	$pattern_sorted['rowdata'] = array_merge(array_flip($sort_pattern), $pattern['rowdata']);
	//add all data to function
	$CONDO_PROJECT_PROFILE_T->add($pattern_sorted);
}

$CONDO_PROJECT_PROFILE_T->save(OUTPUT_PATH.'CONDO_PROJECT_PROFILE_T.xml');
$pattern=NULL;
echo "CONDO_PROJECT_PROFILE_T . . . Done<br>";

/**
 * ################################
 * 9) SECTION FOR CONDO_PROJECT_PROFILE_T1
 * ################################
 * old -> CONDO_PROJECT_PROFILE_T1
 * new -> LH_PROJECTS (Condo) con.
 */

//start main query
$query=mssql_query("SELECT LH_PROJECTS.project_id,LH_PROJECT_CONCEPT.distinctive_th,LH_PROJECT_CONCEPT.facilities_th,LH_PROJECT_CONCEPT.security_th,LH_PROJECTS.location,LH_PROJECT_MAPS.brochure,LH_PROJECT_SEO.project_keyword_th,LH_PROJECT_SEO.project_des_seo_th,LH_PROJECT_CONTACTS.telephone,LH_PROJECT_CONTACTS.telephone_line,LH_PROJECT_MAPS.project_logo,LH_PROJECT_MAPS.project_logo_seo,LH_PROJECTS.project_status,LH_PROJECTS.company_no_DL200,LH_PROJECTS.project_no_DL200,LH_PROJECT_CONTACTS.name_staff,LH_PROJECT_CONTACTS.central_value,LH_PROJECT_CLIPS.clip_project_url,LH_BRANDS.brand_name_en
,convert(VARCHAR(10), LH_PROJECT_CONTACTS.prepay, 103) prepay,LH_ZONES.zone_id,LH_PROJECTS.project_url,LH_PROJECT_ZONE.zone_id_flm,LH_PROJECTS.project_name_th

FROM LH_PROJECTS
LEFT JOIN LH_PROJECT_SUB ON LH_PROJECT_SUB.project_id = LH_PROJECTS.project_id
LEFT JOIN LH_PRODUCTS ON LH_PRODUCTS.product_id=LH_PROJECT_SUB.product_id
LEFT JOIN LH_PROJECT_CONCEPT ON LH_PROJECT_CONCEPT.project_id = LH_PROJECTS.project_id
LEFT JOIN LH_PROJECT_AREA_CONDO ON LH_PROJECT_AREA_CONDO.project_sub_id=LH_PROJECT_SUB.project_sub_id
LEFT JOIN LH_PROJECT_PRICES ON LH_PROJECT_PRICES.project_id = LH_PROJECTS.project_id
LEFT JOIN LH_PROJECT_CONTACTS ON LH_PROJECT_CONTACTS.project_id =LH_PROJECTS.project_id
LEFT JOIN LH_PROJECT_MAPS ON LH_PROJECT_MAPS.project_id=LH_PROJECTS.project_id
LEFT JOIN LH_360_PROJECT ON LH_360_PROJECT.project_id=LH_PROJECTS.project_id
LEFT JOIN LH_BRANDS ON LH_BRANDS.brand_id = LH_PROJECTS.brand_id
LEFT JOIN LH_PROJECT_ZONE ON LH_PROJECT_ZONE.project_id = LH_PROJECTS.project_id
LEFT JOIN LH_ZONES ON LH_ZONES.zone_id = LH_PROJECT_ZONE.zone_id
LEFT JOIN LH_PROJECT_SEO ON LH_PROJECT_SEO.project_id = LH_PROJECTS.project_id
LEFT JOIN LH_PROJECT_CLIPS ON LH_PROJECT_CLIPS.project_id = LH_PROJECTS.project_id
WHERE LH_PRODUCTS.product_name_en='Condominium'");

$CONDO_PROJECT_PROFILE_T1 = new FluidXml('table');
$CONDO_PROJECT_PROFILE_T1->setAttribute('name', 'condo_project_profile_t1');
$CONDO_PROJECT_PROFILE_T1->add('GeneratedDateTime', date("d/m/Y H:i:s"));

//define main query fields & data type
$field_array=[
		["label"=>"CONDO_PROJECT_ID","field"=>"project_id","data_type"=>"int"],
		["label"=>"IMPORTANT_POINT","field"=>"distinctive_th","data_type"=>"varchar"],
		["label"=>"ACCOMMODATION","field"=>"facilities_th","data_type"=>"varchar"],
		["label"=>"SAFETY","field"=>"security_th","data_type"=>"varchar"],
		["label"=>"SKB","field"=>"","data_type"=>"varchar"],
		["label"=>"LOCATION","field"=>"location","data_type"=>"varchar"],
		["label"=>"GOOGLE_MAP","field"=>"","data_type"=>"varchar"],
		["label"=>"BROCHURE","field"=>"brochure","data_type"=>"varchar"],
		["label"=>"KEYWORD","field"=>"project_keyword_th","data_type"=>"varchar"],
		["label"=>"DESCRIPTION","field"=>"project_des_seo_th","data_type"=>"varchar"],
		["label"=>"TEL_AUTO_DIAL","field"=>"telephone","data_type"=>"varchar"],
		["label"=>"PROJECT_CONCEPT2","field"=>"","data_type"=>"varchar"],
		["label"=>"PIC_PROJECT_LOGO","field"=>"project_logo","data_type"=>"varchar"],
		["label"=>"NEW_STATUS","field"=>"","data_type"=>"char"],
		["label"=>"NEW_STATUS_DATE","field"=>"","data_type"=>"datetime"],
		["label"=>"PV_CODE","field"=>"","data_type"=>"varchar"],
		["label"=>"PV_SHOW","field"=>"","data_type"=>"char"],
		["label"=>"PROJECT_CODE","field"=>"project_no_DL200","data_type"=>"varchar"],
		["label"=>"SALES_NAME","field"=>"name_staff","data_type"=>"varchar"],
		["label"=>"PRICE_METRE","field"=>"","data_type"=>"decimal"],
		["label"=>"COMMON_FEE_CHARGE","field"=>"central_value","data_type"=>"decimal"],
		["label"=>"URL_PROJECT_DETAIL","field"=>"","data_type"=>"varchar"],
		["label"=>"YOUTUBE_LINK","field"=>"clip_project_url","data_type"=>"varchar"],
		["label"=>"URL_REGISTER","field"=>"","data_type"=>"varchar"],
		["label"=>"SUBLOCATION_CODE_1198","field"=>"zone_id_flm","data_type"=>"varchar"],
		["label"=>"TEL_DIRECT","field"=>"telephone_line","data_type"=>"varchar"],
		["label"=>"I_COMPANY","field"=>"company_no_DL200","data_type"=>"varchar"],
		["label"=>"I_PROJECT","field"=>"project_no_DL200","data_type"=>"varchar"],
		["label"=>"COMMON_FEE_CHARGE_DATE","field"=>"prepay","data_type"=>"date"],
		["label"=>"URL_WEBSITE","field"=>"","data_type"=>"varchar"],
		["label"=>"PIC_PROJECT_LOGO_ALT","field"=>"project_logo_seo","data_type"=>"varcha"],
];

//main loop

while ($result=mssql_fetch_array($query)) {
$pattern['rowdata']=[]; //pattern prepare to blank array
	//put array fields to pattern
	foreach ($field_array as $key => $fields) {
		$field=(string)$fields['field'];
		$label=(string)$fields['label'];
		$type=(string)$fields['data_type'];

		if ($label=="URL_PROJECT_DETAIL") {
			$value="<![CDATA["."www.lh.co.th/th/condominium/".str_replace(" ","-",$result["project_name_th"])."]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}elseif($field==''){
			$value="<![CDATA[]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}else{
			$value="<![CDATA[".str_replace(array("\r\n","\r","\n","\\r","\\n","\\r\\n")," ",$result["$field"])."]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}
	}

	// //add lead img
	$lead_img_query=mssql_query("SELECT LEAD_IMAGE_PROJECT_FILE_NAME FROM LH_LEAD_IMAGE_PROJECT_FILE WHERE PROJECT_ID={$result['project_id']}");
	if (mssql_num_rows($lead_img_query)>0) {
		$i=3;
		while (($lead_img_r=mssql_fetch_object($lead_img_query)) && ($i<=7)) {
			$pattern['rowdata']+=["PIC_BIG_BUILDING$i"=>['@type'=>"varchar","<![CDATA[$lead_img_r->LEAD_IMAGE_PROJECT_FILE_NAME]]>"]];
			$i++;
		}
	}
	$lead_img_num=7-mssql_num_rows($lead_img_query);
	if($lead_img_num <= 7 && $lead_img_num >= 1){
		$x=mssql_num_rows($lead_img_query)+3;
		for ($i=1; $i <= $lead_img_num ; $i++) {
			$pattern['rowdata']+=["PIC_BIG_BUILDING$x"=>['@type'=>"varchar","<![CDATA[]]>"]];
			$x++;
		}
	}

	$sort_pattern=['CONDO_PROJECT_ID', 'IMPORTANT_POINT', 'ACCOMMODATION', 'SAFETY', 'SKB', 'LOCATION', 'GOOGLE_MAP', 'PIC_BIG_BUILDING3', 'PIC_BIG_BUILDING4', 'PIC_BIG_BUILDING5', 'PIC_BIG_BUILDING6', 'PIC_BIG_BUILDING7', 'PIC_BIG_BUILDING8', 'PIC_BIG_BUILDING9', 'BROCHURE', 'KEYWORD', 'DESCRIPTION', 'TEL_AUTO_DIAL', 'PROJECT_CONCEPT2', 'PIC_PROJECT_LOGO', 'NEW_STATUS', 'NEW_STATUS_DATE', 'PV_CODE', 'PV_SHOW', 'PROJECT_CODE', 'SALES_NAME', 'PRICE_METRE', 'COMMON_FEE_CHARGE', 'URL_PROJECT_DETAIL', 'YOUTUBE_LINK', 'URL_REGISTER', 'SUBLOCATION_CODE_1198', 'TEL_DIRECT', 'I_COMPANY', 'I_PROJECT', 'COMMON_FEE_CHARGE_DATE', 'URL_WEBSITE', 'PIC_PROJECT_LOGO_ALT'];
	$pattern_sorted['rowdata'] = array_merge(array_flip($sort_pattern), $pattern['rowdata']);

	//add all data to function
	$CONDO_PROJECT_PROFILE_T1->add($pattern_sorted);
}

$CONDO_PROJECT_PROFILE_T1->save(OUTPUT_PATH.'CONDO_PROJECT_PROFILE_T1.xml');
$pattern=NULL;
echo "CONDO_PROJECT_PROFILE_T1 . . . Done<br>";

/**
 * ################################
 * 10) SECTION FOR CONDO_PROJECT_PROFILE_T2
 * ################################
 * old -> CONDO_PROJECT_PROFILE_T2
 * new -> LH_PROJECTS (Condo) con.
 */

//start main query
$query=mssql_query("SELECT LH_PROJECTS.project_id,LH_PROJECT_SEO.project_title_seo_th,LH_360_PROJECT.c360_project_url
FROM LH_PROJECTS
LEFT JOIN LH_PROJECT_SUB ON LH_PROJECT_SUB.project_id = LH_PROJECTS.project_id
LEFT JOIN LH_PRODUCTS ON LH_PRODUCTS.product_id=LH_PROJECT_SUB.product_id
LEFT JOIN LH_PROJECT_CONCEPT ON LH_PROJECT_CONCEPT.project_id = LH_PROJECTS.project_id
LEFT JOIN LH_PROJECT_AREA_CONDO ON LH_PROJECT_AREA_CONDO.project_sub_id=LH_PROJECT_SUB.project_sub_id
LEFT JOIN LH_PROJECT_PRICES ON LH_PROJECT_PRICES.project_id = LH_PROJECTS.project_id
LEFT JOIN LH_PROJECT_CONTACTS ON LH_PROJECT_CONTACTS.project_id =LH_PROJECTS.project_id
LEFT JOIN LH_PROJECT_MAPS ON LH_PROJECT_MAPS.project_id=LH_PROJECTS.project_id
LEFT JOIN LH_360_PROJECT ON LH_360_PROJECT.project_id=LH_PROJECTS.project_id
LEFT JOIN LH_BRANDS ON LH_BRANDS.brand_id = LH_PROJECTS.brand_id
LEFT JOIN LH_PROJECT_ZONE ON LH_PROJECT_ZONE.project_id = LH_PROJECTS.project_id
LEFT JOIN LH_ZONES ON LH_ZONES.zone_id = LH_PROJECT_ZONE.zone_id
LEFT JOIN LH_PROJECT_SEO ON LH_PROJECT_SEO.project_id = LH_PROJECTS.project_id
LEFT JOIN LH_PROJECT_CLIPS ON LH_PROJECT_CLIPS.project_id = LH_PROJECTS.project_id
WHERE LH_PRODUCTS.product_name_en='Condominium'");

$CONDO_PROJECT_PROFILE_T2 = new FluidXml('table');
$CONDO_PROJECT_PROFILE_T2->setAttribute('name', 'condo_project_profile_t2');
$CONDO_PROJECT_PROFILE_T2->add('GeneratedDateTime', date("d/m/Y H:i:s"));

//define main query fields & data type
$field_array=[
		["label"=>"CONDO_PROJECT_ID","field"=>"project_id","data_type"=>"int"],
		["label"=>"TITLE_TAG","field"=>"project_title_seo_th","data_type"=>"varchar"],
		["label"=>"PIC_CONDO_ALT","field"=>"","data_type"=>"varchar"],
		["label"=>"NEW_STATUS_TEXT","field"=>"","data_type"=>"varchar"],
		["label"=>"NEW_STATUS_DATE_START","field"=>"","data_type"=>"date"],
		["label"=>"NEW_STATUS_DATE_STOP","field"=>"","data_type"=>"date"],
		["label"=>"URL_REGISTER_DATE_START","field"=>"","data_type"=>"date"],
		["label"=>"URL_REGISTER_DATE_STOP","field"=>"","data_type"=>"date"],
		["label"=>"NUM_UNIT_ALL2","field"=>"","data_type"=>"smallint"],
		["label"=>"NUM_FLOOR2","field"=>"","data_type"=>"smallint"],
		["label"=>"NUM_UNIT_ALL3","field"=>"","data_type"=>"smallint"],
		["label"=>"NUM_FLOOR3","field"=>"","data_type"=>"smallint"],
		["label"=>"NUM_UNIT_ALL4","field"=>"","data_type"=>"smallint"],
		["label"=>"NUM_FLOOR4","field"=>"","data_type"=>"smallint"],
		["label"=>"NUM_UNIT_ALL5","field"=>"","data_type"=>"smallint"],
		["label"=>"NUM_FLOOR5","field"=>"","data_type"=>"smallint"],
		["label"=>"URL_360_SKYVIEW","field"=>"c360_project_url","data_type"=>"varchar"],
		["label"=>"PIC_ZONING","field"=>"","data_type"=>"varchar"],
		["label"=>"ZONING_DESC","field"=>"project_id","data_type"=>"varchar"],
		["label"=>"NEAR_BTS","field"=>"","data_type"=>"varchar"],
		["label"=>"NEAR_BTS_DETAIL","field"=>"","data_type"=>"varchar"],
		["label"=>"NEAR_MRT","field"=>"","data_type"=>"varchar"],
		["label"=>"NEAR_MRT_DETAIL","field"=>"","data_type"=>"varchar"],
];

//main loop
while ($result=mssql_fetch_array($query)) {
$pattern['rowdata']=[]; //pattern prepare to blank array
	//put array fields to pattern
	foreach ($field_array as $key => $fields) {
		$field=(string)$fields['field'];
		$label=(string)$fields['label'];
		$type=(string)$fields['data_type'];
		if ( $key >= 8 &&  $key <= 15) {
			$value="<![CDATA["."0"."]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}elseif($field==''){
			$value="<![CDATA[]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}else{
			$value="<![CDATA[".$result["$field"]."]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}
	}

	$x=10;
	for ($i=1; $i <= 20 ; $i++) {
		$pattern['rowdata']+=["PIC_BIG_BUILDING$x"=>['@type'=>"varchar","<![CDATA[]]>"]];
		$x++;
	}

	//add gallery img
	$gal_img_query=mssql_query("SELECT LH_GALERY_IMG_PROJECT.galery_project_img_seo FROM LH_GALERY_IMG_PROJECT LEFT JOIN LH_GALERY_PROJECT ON LH_GALERY_PROJECT.galery_project_id=LH_GALERY_IMG_PROJECT.galery_project_id
WHERE LH_GALERY_PROJECT.project_id={$result['project_id']}");
	if (mssql_num_rows($gal_img_query)>0) {
		$i=1;
		while (($gal_img_r=mssql_fetch_object($gal_img_query)) && ($i<=28)) {
			$img_label="PIC_BIG_BUILDING$i"."_ALT";
			$pattern['rowdata']+=["$img_label"=>['@type'=>"varchar","<![CDATA[$gal_img_r->galery_project_img_seo]]>"]];
			$i++;
		}
	}

	$gal_img_num=28-mssql_num_rows($gal_img_query);
	if($gal_img_num <= 28 && $gal_img_num >= 1){
		$x=mssql_num_rows($gal_img_query)+1;
		for ($i=1; $i <= $gal_img_num ; $i++) {
			$img_label="PIC_BIG_BUILDING$x"."_ALT";
			$pattern['rowdata']+=["$img_label"=>['@type'=>"varchar","<![CDATA[]]>"]];
			$x++;
		}
	}

	$sort_pattern=['CONDO_PROJECT_ID', 'TITLE_TAG', 'PIC_CONDO_ALT', 'PIC_BIG_BUILDING1_ALT', 'PIC_BIG_BUILDING2_ALT', 'PIC_BIG_BUILDING3_ALT', 'PIC_BIG_BUILDING4_ALT', 'PIC_BIG_BUILDING5_ALT', 'PIC_BIG_BUILDING6_ALT', 'PIC_BIG_BUILDING7_ALT', 'PIC_BIG_BUILDING8_ALT', 'PIC_BIG_BUILDING9_ALT', 'NEW_STATUS_TEXT', 'PIC_BIG_BUILDING10', 'PIC_BIG_BUILDING11', 'PIC_BIG_BUILDING12', 'PIC_BIG_BUILDING13', 'PIC_BIG_BUILDING14', 'PIC_BIG_BUILDING15', 'PIC_BIG_BUILDING16', 'PIC_BIG_BUILDING17', 'PIC_BIG_BUILDING18', 'PIC_BIG_BUILDING19', 'PIC_BIG_BUILDING20', 'PIC_BIG_BUILDING21', 'PIC_BIG_BUILDING22', 'PIC_BIG_BUILDING23', 'PIC_BIG_BUILDING24', 'PIC_BIG_BUILDING25', 'PIC_BIG_BUILDING26', 'PIC_BIG_BUILDING27', 'PIC_BIG_BUILDING28', 'PIC_BIG_BUILDING29', 'PIC_BIG_BUILDING10_ALT', 'PIC_BIG_BUILDING11_ALT', 'PIC_BIG_BUILDING12_ALT', 'PIC_BIG_BUILDING13_ALT', 'PIC_BIG_BUILDING14_ALT', 'PIC_BIG_BUILDING15_ALT', 'PIC_BIG_BUILDING16_ALT', 'PIC_BIG_BUILDING17_ALT', 'PIC_BIG_BUILDING18_ALT', 'PIC_BIG_BUILDING19_ALT', 'PIC_BIG_BUILDING20_ALT', 'PIC_BIG_BUILDING21_ALT', 'PIC_BIG_BUILDING22_ALT', 'PIC_BIG_BUILDING23_ALT', 'PIC_BIG_BUILDING24_ALT', 'PIC_BIG_BUILDING25_ALT', 'PIC_BIG_BUILDING26_ALT', 'PIC_BIG_BUILDING27_ALT', 'PIC_BIG_BUILDING28_ALT', 'PIC_BIG_BUILDING29_ALT', 'NEW_STATUS_DATE_START', 'NEW_STATUS_DATE_STOP', 'URL_REGISTER_DATE_START', 'URL_REGISTER_DATE_STOP', 'NUM_UNIT_ALL2', 'NUM_FLOOR2', 'NUM_UNIT_ALL3', 'NUM_FLOOR3', 'NUM_UNIT_ALL4', 'NUM_FLOOR4', 'NUM_UNIT_ALL5', 'NUM_FLOOR5', 'URL_360_SKYVIEW', 'PIC_ZONING', 'ZONING_DESC', 'NEAR_BTS', 'NEAR_BTS_DETAIL', 'NEAR_MRT', 'NEAR_MRT_DETAIL'];
	$pattern_sorted['rowdata'] = array_merge(array_flip($sort_pattern), $pattern['rowdata']);

	//add all data to function
	$CONDO_PROJECT_PROFILE_T2->add($pattern_sorted);
}

$CONDO_PROJECT_PROFILE_T2->save(OUTPUT_PATH.'CONDO_PROJECT_PROFILE_T2.xml');
$pattern=NULL;
echo "CONDO_PROJECT_PROFILE_T2 . . . Done<br>";

/**
 * ################################
 * 11) SECTION FOR CONDO_PROJECT_UNIT_PLAN
 * ################################
 * old -> CONDO_PROJECT_UNIT_PLAN
 * new -> LH_UNIT_PLAN_CONDO
 */

//start main query
$query=mssql_query("SELECT LH_UNIT_PLAN_CONDO.unit_plan_id,LH_UNIT_PLAN_CONDO.project_id,LH_UNIT_PLAN_CONDO.unit_plan_name_th,LH_UNIT_PLAN_CONDO.unit_plan_name_en,LH_UNIT_PLAN_CONDO.unit_plan_size,LH_UNIT_PLAN_CONDO.unit_plan_dis_th,LH_UNIT_PLAN_CONDO.unit_plan_dis_en,LH_UNIT_PLAN_CONDO.unit_plan_price,LH_UNIT_PLAN_IMG.unit_plan_img_name FROM LH_UNIT_PLAN_CONDO LEFT JOIN LH_UNIT_PLAN_IMG ON LH_UNIT_PLAN_IMG.unit_plan_id = LH_UNIT_PLAN_CONDO.unit_plan_id");

$CONDO_PROJECT_UNIT_PLAN = new FluidXml('table');
$CONDO_PROJECT_UNIT_PLAN->setAttribute('name', 'condo_project_unit_plan');
$CONDO_PROJECT_UNIT_PLAN->add('GeneratedDateTime', date("d/m/Y H:i:s"));

//define main query fields & data type
$field_array=[
		["label"=>"ID","field"=>"unit_plan_id","data_type"=>"int"],
		["label"=>"CONDO_PROJECT_ID","field"=>"project_id","data_type"=>"int"],
		["label"=>"UNIT_NAME","field"=>"unit_plan_name_th","data_type"=>"varchar"],
		["label"=>"UNIT_NAME_E","field"=>"unit_plan_name_en","data_type"=>"varchar"],
		["label"=>"UNIT_SIZE","field"=>"unit_plan_size","data_type"=>"varchar"],
		["label"=>"UNIT_SIZE_E","field"=>"","data_type"=>"varchar"],
		["label"=>"DESCRIPTION","field"=>"unit_plan_dis_th","data_type"=>"varchar"],
		["label"=>"DESCRIPTION_E","field"=>"unit_plan_dis_en","data_type"=>"varchar"],
		["label"=>"PICTURE","field"=>"unit_plan_img_name","data_type"=>"varchar"],
		["label"=>"BEGIN_PRICE_UNIT","field"=>"unit_plan_price","data_type"=>"decimal"],
];

//main loop

while ($result=mssql_fetch_array($query)) {
$pattern['rowdata']=[]; //pattern prepare to blank array

	//put array fields to pattern
	foreach ($field_array as $key => $fields) {
		$field=(string)$fields['field'];
		$label=(string)$fields['label'];
		$type=(string)$fields['data_type'];

		if($field==''){
			$value="<![CDATA[]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}else{
			$value="<![CDATA[".$result["$field"]."]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}
	}

	$sort_pattern=['ID', 'CONDO_PROJECT_ID', 'UNIT_NAME', 'UNIT_NAME_E', 'UNIT_SIZE', 'UNIT_SIZE_E', 'DESCRIPTION', 'DESCRIPTION_E', 'PICTURE', 'BEGIN_PRICE_UNIT'];
	$pattern_sorted['rowdata'] = array_merge(array_flip($sort_pattern), $pattern['rowdata']);

	//add all data to function
	$CONDO_PROJECT_UNIT_PLAN->add($pattern_sorted);
}

$CONDO_PROJECT_UNIT_PLAN->save(OUTPUT_PATH.'CONDO_PROJECT_UNIT_PLAN.xml');
$pattern=NULL;
echo "CONDO_PROJECT_UNIT_PLAN . . . Done<br>";

/**
 * ################################
 * 12) SECTION FOR NEWHOME_PROJECT
 * ################################
 * old -> NEWHOME_PROJECT
 * new -> LH_PROJECTS
 */

//start main query
$query=mssql_query("SELECT DISTINCT LH_PROJECTS.project_id
FROM LH_PROJECTS
LEFT JOIN LH_PROJECT_SUB ON LH_PROJECT_SUB.project_id = LH_PROJECTS.project_id
LEFT JOIN LH_PRODUCTS ON LH_PRODUCTS.product_id=LH_PROJECT_SUB.product_id
LEFT JOIN LH_PROJECT_CONCEPT ON LH_PROJECT_CONCEPT.project_id = LH_PROJECTS.project_id
LEFT JOIN LH_PROJECT_AREA_CONDO ON LH_PROJECT_AREA_CONDO.project_sub_id=LH_PROJECT_SUB.project_sub_id
LEFT JOIN LH_PROJECT_PRICES ON LH_PROJECT_PRICES.project_id = LH_PROJECTS.project_id
LEFT JOIN LH_PROJECT_CONTACTS ON LH_PROJECT_CONTACTS.project_id =LH_PROJECTS.project_id
LEFT JOIN LH_PROJECT_MAPS ON LH_PROJECT_MAPS.project_id=LH_PROJECTS.project_id
LEFT JOIN LH_360_PROJECT ON LH_360_PROJECT.project_id=LH_PROJECTS.project_id
LEFT JOIN LH_BRANDS ON LH_BRANDS.brand_id = LH_PROJECTS.brand_id
LEFT JOIN LH_PROJECT_ZONE ON LH_PROJECT_ZONE.project_id = LH_PROJECTS.project_id
LEFT JOIN LH_PROJECT_AREA_HOME ON LH_PROJECT_AREA_HOME.project_sub_id = LH_PROJECT_SUB.project_sub_id
WHERE LH_PRODUCTS.product_id !=3 AND LH_PROJECTS.project_status !='SO'
");

$NEWHOME_PROJECT = new FluidXml('table');
$NEWHOME_PROJECT->setAttribute('name', 'newhome_project');
$NEWHOME_PROJECT->add('GeneratedDateTime', date("d/m/Y H:i:s"));

//define main query fields & data type
$field_array=[
		["label"=>"PROJ_NAME","field"=>"project_name_th","data_type"=>"varchar"],
		["label"=>"PROJ_PROVINCE","field"=>"province_id","data_type"=>"char"],
		["label"=>"PROJ_ID","field"=>"project_id","data_type"=>"char"],
		["label"=>"PROJ_REGION","field"=>"amphur_id","data_type"=>"char"],
		["label"=>"PROJ_SUBLOCATION","field"=>"zone_id","data_type"=>"char"],
		["label"=>"PROJ_AREA_RSIZE","field"=>"area_farm","data_type"=>"decimal"],
		["label"=>"PROJ_AREA_PSIZE","field"=>"area_land","data_type"=>"int"],
		["label"=>"PROJ_VISION","field"=>"concept_th","data_type"=>"varchar"],
		["label"=>"PROJ_FEATURE","field"=>"distinctive_th","data_type"=>"varchar"],
		["label"=>"PROJ_HOME_TYPE","field"=>"product_id","data_type"=>"varchar"],
		["label"=>"NUM_HOMEMODEL","field"=>"num_plan_home","data_type"=>"int"],
		["label"=>"AREA_USEFUL","field"=>"area_home","data_type"=>"int"],
		["label"=>"BUILD_AREA","field"=>"area_square_w_home","data_type"=>"int"],
		["label"=>"BEGIN_PRICE","field"=>"project_price","data_type"=>"int"],
		["label"=>"PIC_PROJ_MAP","field"=>"map_img","data_type"=>"varchar"],
		["label"=>"TELEPHONE","field"=>"telephone","data_type"=>"varchar"],
		["label"=>"BEGIN_WORKDAY","field"=>"","data_type"=>"smallint"],
		["label"=>"END_WORKDAY","field"=>"","data_type"=>"smallint"],
		["label"=>"BEGIN_WORKTIME","field"=>"open_time_start","data_type"=>"char"],
		["label"=>"END_WORKTIME","field"=>"open_time_end","data_type"=>"char"],
		["label"=>"EMAIL","field"=>"email","data_type"=>"varchar"],
		["label"=>"CREATE_USER","field"=>"","data_type"=>"varchar"],
		["label"=>"CREATE_DATE","field"=>"create_date","data_type"=>"date"],
		["label"=>"LAST_USER","field"=>"","data_type"=>"varchar"],
		["label"=>"LAST_DATE","field"=>"updated_date","data_type"=>"date"],
		["label"=>"POSX","field"=>"","data_type"=>"int"],
		["label"=>"POSY","field"=>"","data_type"=>"int"],
		["label"=>"COMPANY_NAME","field"=>"","data_type"=>"varchar"],
		["label"=>"SPECIAL_PROMOTION","field"=>"","data_type"=>"char"],
		["label"=>"PROJ_NAME_E","field"=>"project_name_en","data_type"=>"varchar"],
		["label"=>"POSX_MAIN","field"=>"","data_type"=>"int"],
		["label"=>"POSY_MAIN","field"=>"","data_type"=>"int"],
		["label"=>"HOME_TYPE_PIC","field"=>"logo_brand_th","data_type"=>"varchar"],
		["label"=>"WORK_FLAG","field"=>"","data_type"=>"char"],
		["label"=>"WORK_EXCEPT","field"=>"","data_type"=>"varchar"],
		["label"=>"STATUS","field"=>"project_status","data_type"=>"char"],
		["label"=>"PROJ_LOGO","field"=>"project_logo","data_type"=>"varchar"],
		["label"=>"BRAND_ID","field"=>"brand_id","data_type"=>"char"],
		["label"=>"PIC_MAP_FLASH","field"=>"","data_type"=>"varchar"],
		["label"=>"FLASH_PROJ","field"=>"","data_type"=>"varchar"],
		["label"=>"ORDER_IN_BRAND","field"=>"","data_type"=>"int"],
		["label"=>"ORDER_UPDATE_DATE","field"=>"","data_type"=>"datetime"],
		["label"=>"SHOW_LADAWAN_NO","field"=>"","data_type"=>"int"],
		["label"=>"PROJ_SUBZONE_ID","field"=>"","data_type"=>"varchar"],
];

//main loop
while ($result=mssql_fetch_array($query)) {
$pattern['rowdata']=[]; //pattern prepare to blank array
	$query_detail=mssql_query("SELECT LH_PROJECTS.project_name_th,LH_PROJECT_ZONE.zone_id,LH_PROJECT_ZONE.province_id,LH_PROJECT_ZONE.project_id,LH_PROJECT_ZONE.amphur_id,LH_PROJECTS.area_farm,LH_PROJECTS.area_land,LH_PROJECT_CONCEPT.concept_th,LH_PROJECT_CONCEPT.distinctive_th,LH_PRODUCTS.product_id,LH_PROJECT_AREA_HOME.num_plan_home,LH_PROJECT_AREA_HOME.area_home,LH_PROJECT_AREA_HOME.area_square_w_home,LH_PROJECT_PRICES.project_price,LH_PROJECT_MAPS.map_img,LH_PROJECT_CONTACTS.telephone,LH_PROJECT_CONTACTS.open_time_start,LH_PROJECT_CONTACTS.open_time_end,LH_PROJECT_CONTACTS.open_day,LH_PROJECT_CONTACTS.email,convert(VARCHAR(10), LH_PROJECTS.create_date, 103) create_date,convert(VARCHAR(10), LH_PROJECTS.updated_date, 103) updated_date,LH_PROJECTS.project_name_en,LH_BRANDS.logo_brand_th,LH_PROJECTS.project_status,LH_BRANDS.brand_id,LH_PROJECT_MAPS.project_logo
	FROM LH_PROJECTS
	LEFT JOIN LH_PROJECT_SUB ON LH_PROJECT_SUB.project_id = LH_PROJECTS.project_id
	LEFT JOIN LH_PRODUCTS ON LH_PRODUCTS.product_id=LH_PROJECT_SUB.product_id
	LEFT JOIN LH_PROJECT_CONCEPT ON LH_PROJECT_CONCEPT.project_id = LH_PROJECTS.project_id
	LEFT JOIN LH_PROJECT_AREA_CONDO ON LH_PROJECT_AREA_CONDO.project_sub_id=LH_PROJECT_SUB.project_sub_id
	LEFT JOIN LH_PROJECT_PRICES ON LH_PROJECT_PRICES.project_id = LH_PROJECTS.project_id
	LEFT JOIN LH_PROJECT_CONTACTS ON LH_PROJECT_CONTACTS.project_id =LH_PROJECTS.project_id
	LEFT JOIN LH_PROJECT_MAPS ON LH_PROJECT_MAPS.project_id=LH_PROJECTS.project_id
	LEFT JOIN LH_360_PROJECT ON LH_360_PROJECT.project_id=LH_PROJECTS.project_id
	LEFT JOIN LH_BRANDS ON LH_BRANDS.brand_id = LH_PROJECTS.brand_id
	LEFT JOIN LH_PROJECT_ZONE ON LH_PROJECT_ZONE.project_id = LH_PROJECTS.project_id
	LEFT JOIN LH_PROJECT_AREA_HOME ON LH_PROJECT_AREA_HOME.project_sub_id = LH_PROJECT_SUB.project_sub_id
	WHERE LH_PRODUCTS.product_id !=3 AND LH_PROJECTS.project_status !='SO' AND LH_PROJECTS.project_id = {$result["project_id"]}
	");
	$project_detail=array();
	while ($result_detail=mssql_fetch_array($query_detail)) {
		$project_detail[]=$result_detail;
	}

	$product_list=array();
	if (count($project_detail)>1) {
		foreach ($project_detail as $key => $project_detail_value) {
			// print_r($project_detail_value['product_id']);
			$product_list[]=$product_pattern["{$project_detail_value['product_id']}"];
		}
	}else{
		$product_list[]=$product_pattern["{$project_detail[0]['product_id']}"];
	}
	$product_list=implode(",",$product_list);
	//put array fields to pattern
	foreach ($field_array as $key => $fields) {
		$field=(string)$fields['field'];
		$label=(string)$fields['label'];
		$type=(string)$fields['data_type'];

		if($key>=16 && $key <=17){
			$value="<![CDATA[]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}elseif($label=="BEGIN_PRICE" ){
			$price=explode("-",$project_detail[0]["$field"]);
			if ($label=="BEGIN_PRICE" && count($price)>1) {
				$value="<![CDATA[".(int) preg_replace("/[^0-9]/","",$price[0])."]]>";
			}else{
				$value="<![CDATA[".(int) preg_replace("/[^0-9]/","",$price[0])."]]>";
			}
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}elseif ($label=='PROJ_HOME_TYPE') {
			$value="<![CDATA[".$product_list."]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}elseif ($label=='PROJ_FEATURE' || $label=='PROJ_VISION') {
			$data=str_replace(array("\r\n","\r","\n","\\r","\\n","\\r\\n",'"')," ",preg_replace('/[\x00-\x1f]/', ' ', htmlspecialchars($project_detail[0][$field], ENT_XML1 | ENT_QUOTES, 'UTF-8')));
			$value="<![CDATA[".$data."]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}elseif ($label=='BEGIN_PRICE') {
			$value="<![CDATA[".(int) preg_replace("/[^0-9]/","",$project_detail[0]["$field"])."]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}elseif ($label=='BEGIN_WORKDAY' || $label=='END_WORKDAY') {
			$value="<![CDATA[]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}elseif ($label=='COMPANY_NAME') {
			$value="<![CDATA[บริษัท แลนด์ แอนด์ เฮ้าส์ จำกัด (มาหาชน)]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}elseif($field==''){
			$value="<![CDATA[]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}else{
			$value="<![CDATA[".$project_detail[0]["$field"]."]]>";
			$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
		}
	}

	//add condo near by
	$near_query=mssql_query("SELECT LH_PROJECT_NEARBY.nearby_name_th,interval FROM LH_PROJECT_NEARBY WHERE project_id={$project_detail[0]['project_id']}");
	if (mssql_num_rows($near_query)>0) {
		$i=1;
		while (($near_r=mssql_fetch_object($near_query)) && ($i<=5)) {
			$pattern['rowdata']+=["FEMOUS_PLACE$i"=>['@type'=>"varchar","<![CDATA[$near_r->nearby_name_th]]>"]];
			$pattern['rowdata']+=["RANGE$i"=>['@type'=>"decimal","<![CDATA[$near_r->interval]]>"]];
			$i++;
		}
	}

	$near_num=5-mssql_num_rows($near_query);
	if($near_num <= 5 && $near_num >= 1){
		$x=mssql_num_rows($near_query)+1;
		for ($i=1; $i <= $near_num ; $i++) {
			$pattern['rowdata']+=["FEMOUS_PLACE$x"=>['@type'=>"varchar","<![CDATA[]]>"]];
			$pattern['rowdata']+=["RANGE$x"=>['@type'=>"decimal","<![CDATA[]]>"]];
			$x++;
		}
	}

	//add clip
	$clip_label=['1_1','1_2','2_1','2_2'];
	$clip_query=mssql_query("SELECT clip_project_url FROM LH_PROJECT_CLIPS WHERE project_id={$project_detail[0]['project_id']}");
	if (mssql_num_rows($clip_query)>0) {
		$i=1;$x=0;
		while (($clip_r=mssql_fetch_object($clip_query)) && ($i<=4)) {
			$pattern['rowdata']+=["VIDEO$clip_label[$x]"=>['@type'=>"varchar","<![CDATA[$clip_r->clip_project_url]]>"]];
			$i++;$x++;
		}
	}
	$clip_num=4-mssql_num_rows($clip_query);
	if($clip_num <= 4 && $clip_num >= 1){
		$x=mssql_num_rows($clip_query);
		for ($i=1; $i <= $clip_num ; $i++) {
			$pattern['rowdata']+=["VIDEO$clip_label[$x]"=>['@type'=>"varchar","<![CDATA[]]>"]];
			$x++;
		}
	}

	//add img
	$img_query=mssql_query("SELECT LEAD_IMAGE_PROJECT_FILE_NAME FROM LH_LEAD_IMAGE_PROJECT_FILE WHERE project_id={$project_detail[0]['project_id']}");
	if (mssql_num_rows($img_query)>0) {
		$i=0;
		while (($img_r=mssql_fetch_object($img_query)) && ($i<=3)) {
			$x=($i==0?'':$i);
			$pattern['rowdata']+=["PIC_PROJ$x"=>['@type'=>"varchar","<![CDATA[$img_r->LEAD_IMAGE_PROJECT_FILE_NAME]]>"]];
			$i++;
		}
	}
	$img_num=4-mssql_num_rows($img_query);
	if($img_num <= 4 && $img_num >= 1){
		$x=mssql_num_rows($img_query);
		for ($i=1; $i <= $img_num ; $i++) {
			$z=($x==0?'':$x);
			$pattern['rowdata']+=["PIC_PROJ$z"=>['@type'=>"varchar","<![CDATA[]]>"]];
			$x++;
		}
	}

	$sort_pattern=['PROJ_NAME', 'PROJ_PROVINCE', 'PROJ_ID', 'PROJ_REGION', 'PROJ_SUBLOCATION', 'PROJ_AREA_RSIZE', 'PROJ_AREA_PSIZE', 'PROJ_VISION', 'PROJ_FEATURE', 'PROJ_HOME_TYPE', 'NUM_HOMEMODEL', 'AREA_USEFUL', 'BUILD_AREA', 'BEGIN_PRICE', 'FEMOUS_PLACE1', 'FEMOUS_PLACE2', 'FEMOUS_PLACE3', 'FEMOUS_PLACE4', 'FEMOUS_PLACE5', 'RANGE1', 'RANGE2', 'RANGE3', 'RANGE4', 'RANGE5', 'PIC_PROJ', 'PIC_PROJ_MAP', 'TELEPHONE', 'BEGIN_WORKDAY', 'END_WORKDAY', 'BEGIN_WORKTIME', 'END_WORKTIME', 'EMAIL', 'CREATE_USER', 'CREATE_DATE', 'LAST_USER', 'LAST_DATE', 'POSX', 'POSY', 'COMPANY_NAME', 'SPECIAL_PROMOTION', 'PROJ_NAME_E', 'POSX_MAIN', 'POSY_MAIN', 'HOME_TYPE_PIC', 'WORK_FLAG', 'WORK_EXCEPT', 'STATUS', 'PIC_PROJ1', 'PIC_PROJ2', 'PIC_PROJ3', 'PROJ_LOGO', 'BRAND_ID', 'VIDEO1_1', 'VIDEO1_2', 'VIDEO2_1', 'VIDEO2_2', 'PIC_MAP_FLASH', 'FLASH_PROJ', 'ORDER_IN_BRAND', 'ORDER_UPDATE_DATE', 'SHOW_LADAWAN_NO', 'PROJ_SUBZONE_ID'];
	$pattern_sorted['rowdata'] = array_merge(array_flip($sort_pattern), $pattern['rowdata']);

	//add all data to function
	$NEWHOME_PROJECT->add($pattern_sorted);
}

$NEWHOME_PROJECT->save(OUTPUT_PATH.'NEWHOME_PROJECT.xml');
$pattern=NULL;
echo "NEWHOME_PROJECT . . . Done<br>";

/**
 * ################################
 * 13) SECTION FOR NEWHOME_PROJECT_DETAIL
 * ################################
 * old -> NEWHOME_PROJECT_DETAIL
 * new -> LH_PROJECTS con.
 */

//start main query
$query=mssql_query("SELECT LH_PROJECTS.project_id,LH_PROJECT_MAPS.map_pdf,LH_360_PROJECT.c360_project_url,LH_PROJECT_MAPS.project_logo_seo,LH_PROJECTS.latitude,LH_PROJECTS.longtitude,LH_PROJECT_CONTACTS.telephone_line,LH_PROJECT_SEO.project_keyword_th,LH_PROJECT_SEO.project_des_seo_th,LH_PROJECTS.project_status,LH_PROJECTS.location,LH_PROJECT_PRICES.project_price
FROM LH_PROJECTS
LEFT JOIN LH_PROJECT_SUB ON LH_PROJECT_SUB.project_id = LH_PROJECTS.project_id
LEFT JOIN LH_PRODUCTS ON LH_PRODUCTS.product_id=LH_PROJECT_SUB.product_id
LEFT JOIN LH_PROJECT_CONCEPT ON LH_PROJECT_CONCEPT.project_id = LH_PROJECTS.project_id
LEFT JOIN LH_PROJECT_AREA_CONDO ON LH_PROJECT_AREA_CONDO.project_sub_id=LH_PROJECT_SUB.project_sub_id
LEFT JOIN LH_PROJECT_PRICES ON LH_PROJECT_PRICES.project_id = LH_PROJECTS.project_id
LEFT JOIN LH_PROJECT_CONTACTS ON LH_PROJECT_CONTACTS.project_id =LH_PROJECTS.project_id
LEFT JOIN LH_PROJECT_MAPS ON LH_PROJECT_MAPS.project_id=LH_PROJECTS.project_id
LEFT JOIN LH_360_PROJECT ON LH_360_PROJECT.project_id=LH_PROJECTS.project_id
LEFT JOIN LH_BRANDS ON LH_BRANDS.brand_id = LH_PROJECTS.brand_id
LEFT JOIN LH_PROJECT_ZONE ON LH_PROJECT_ZONE.project_id = LH_PROJECTS.project_id
LEFT JOIN LH_PROJECT_AREA_HOME ON LH_PROJECT_AREA_HOME.project_sub_id = LH_PROJECT_SUB.project_sub_id
LEFT JOIN LH_PROJECT_SEO ON LH_PROJECT_SEO.project_id = LH_PROJECTS.project_id
WHERE LH_PRODUCTS.product_id !=3 AND LH_PROJECTS.project_status !='SO'
");

$NEWHOME_PROJECT_DETAIL = new FluidXml('table');
$NEWHOME_PROJECT_DETAIL->setAttribute('name', 'newhome_project_detail');
$NEWHOME_PROJECT_DETAIL->add('GeneratedDateTime', date("d/m/Y H:i:s"));

//define main query fields & data type
$field_array=[
		["label"=>"PROJ_ID","field"=>"project_id","data_type"=>"char"],
		["label"=>"PIC_MAP_PDF","field"=>"map_pdf","data_type"=>"varchar"],
		["label"=>"URL_MAP_GOOGLE","field"=>"","data_type"=>"varchar"],
		["label"=>"URL_360","field"=>"c360_project_url","data_type"=>"varchar"],
		["label"=>"PROJ_LOGO_TEXT","field"=>"project_logo_seo","data_type"=>"varchar"],
		["label"=>"LATITUDE","field"=>"latitude","data_type"=>"varchar"],
		["label"=>"LONGITUDE","field"=>"longtitude","data_type"=>"varchar"],
		["label"=>"TEL_AUTO_DIAL","field"=>"","data_type"=>"varchar"],
		["label"=>"KEYWORD","field"=>"project_keyword_th","data_type"=>"varchar"],
		["label"=>"DESCRIPTION","field"=>"project_des_seo_th","data_type"=>"varchar"],
		["label"=>"NEW_STATUS","field"=>"project_status","data_type"=>"char"],
		["label"=>"NEW_STATUS_DATE","field"=>"","data_type"=>"datetime"],
		["label"=>"FACTSHEET","field"=>"","data_type"=>"varchar"],
		["label"=>"ADDRESS_NO","field"=>"location","data_type"=>"varchar"],
		["label"=>"BEGIN_PRICE_TEXT","field"=>"project_price","data_type"=>"varchar"],
		["label"=>"NEW_STATUS_DATE_START","field"=>"","data_type"=>"date"],
		["label"=>"NEW_STATUS_DATE_STOP","field"=>"","data_type"=>"date"],
		["label"=>"ORDER_IN_BRAND","field"=>"","data_type"=>"int"],
		["label"=>"ORDER_UPDATE_DATE","field"=>"","data_type"=>"datetime"],
];

//main loop
$distinct_id=array();
while ($result=mssql_fetch_array($query)) {
	//check for distinct data if not put it to array
	if(!in_array($result['project_id'],$distinct_id)):
		$distinct_id[]=$result['project_id'];
		$pattern['rowdata']=[]; //pattern prepare to blank array

		//put array fields to pattern
		foreach ($field_array as $key => $fields) {
			$field=(string)$fields['field'];
			$label=(string)$fields['label'];
			$type=(string)$fields['data_type'];

			if ($label=='DESCRIPTION' || $label=='KEYWORD') {
				$value="<![CDATA[".str_replace(array("\r\n","\r","\n","\\r","\\n","\\r\\n")," ",$result["$field"])."]]>";
				$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
			}elseif($label=="PIC_MAP_PDF" ){
				$map_pdf_file[]=(count(explode("/",$result["$field"]))>1?str_replace(":",":",explode("/",$result["$field"])[4]):'');
				$map_stg=(count(explode("/",$result["$field"]))>1?str_replace(":","_",explode("/",$result["$field"])[4]):'');
				$value="<![CDATA[".$map_stg."]]>";
				$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
			}elseif($label=="BEGIN_PRICE_TEXT" ){
				$price=explode("-",$result["$field"]);
				if ($label=="BEGIN_PRICE_TEXT" && count($price)>1) {
					$value="<![CDATA[".(int) preg_replace("/[^0-9]/","",$price[1])."]]>";
				}else{
					$value="<![CDATA[".(int) preg_replace("/[^0-9]/","",$price[0])."]]>";
				}
				$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
			}elseif($field==''){
				$value="<![CDATA[]]>";
				$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
			}else{
				$value="<![CDATA[".$result["$field"]."]]>";
				$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
			}
		}

		//add img
		$img_query=mssql_query("SELECT LH_IMG_PROMOTION.img_promotion_name FROM LH_IMG_PROMOTION  LEFT JOIN LH_PROMOTION ON LH_PROMOTION.promotion_id = LH_IMG_PROMOTION.promotion_id WHERE LH_PROMOTION.project_id={$result['project_id']}");
		if (mssql_num_rows($img_query)>0) {
			$i=0;
			while (($img_r=mssql_fetch_object($img_query)) && ($i<=10)) {
				$x=($i==0?'':$i);
				$img_label="PIC_PROJ".$x."_PROMOTION";
				$pattern['rowdata']+=["$img_label"=>['@type'=>"varchar","<![CDATA[$img_r->img_promotion_name]]>"]];
				$i++;
			}
		}
		$img_num=11-mssql_num_rows($img_query);
		if($img_num <= 11 && $img_num >= 1){
			$x=mssql_num_rows($img_query);
			for ($i=1; $i <= $img_num ; $i++) {
				$z=($x==0?'':$x);
				$img_label="PIC_PROJ".$z."_PROMOTION";
				$pattern['rowdata']+=["$img_label"=>['@type'=>"varchar","<![CDATA[]]>"]];
				$x++;
			}
		}

		//add fake PIC_PROJ4
		for ($i=4; $i <= 10 ; $i++) {
			$img_label="PIC_PROJ".$i;
			$pattern['rowdata']+=["$img_label"=>['@type'=>"varchar","<![CDATA[]]>"]];
			$x++;
		}

		$sort_pattern=['PROJ_ID', 'PIC_PROJ4', 'PIC_PROJ5', 'PIC_PROJ6', 'PIC_PROJ7', 'PIC_PROJ8', 'PIC_PROJ9', 'PIC_PROJ10', 'PIC_MAP_PDF', 'URL_MAP_GOOGLE', 'URL_360', 'PROJ_LOGO_TEXT', 'LATITUDE', 'LONGITUDE', 'TEL_AUTO_DIAL', 'KEYWORD', 'DESCRIPTION', 'NEW_STATUS', 'NEW_STATUS_DATE', 'FACTSHEET', 'ADDRESS_NO', 'PIC_PROJ_PROMOTION', 'PIC_PROJ2_PROMOTION', 'PIC_PROJ3_PROMOTION', 'PIC_PROJ4_PROMOTION', 'PIC_PROJ5_PROMOTION', 'PIC_PROJ6_PROMOTION', 'PIC_PROJ7_PROMOTION', 'PIC_PROJ8_PROMOTION', 'PIC_PROJ9_PROMOTION', 'PIC_PROJ10_PROMOTION', 'PIC_PROJ1_PROMOTION', 'BEGIN_PRICE_TEXT', 'NEW_STATUS_DATE_START', 'NEW_STATUS_DATE_STOP', 'ORDER_IN_BRAND', 'ORDER_UPDATE_DATE'];
		$pattern_sorted['rowdata'] = array_merge(array_flip($sort_pattern), $pattern['rowdata']);

		//add all data to function
		$NEWHOME_PROJECT_DETAIL->add($pattern_sorted);
	endif;
}

$NEWHOME_PROJECT_DETAIL->save(OUTPUT_PATH.'NEWHOME_PROJECT_DETAIL.xml');
$pattern=NULL;
echo "NEWHOME_PROJECT_DETAIL . . . Done<br>";

/**
 * ################################
 * 14) SECTION FOR NEWHOME_PROJECT_DETAIL2
 * ################################
 * old -> NEWHOME_PROJECT_DETAIL2
 * new -> LH_PROJECTS con.
 */

//start main query
$query=mssql_query("SELECT LH_PROJECTS.project_id,LH_PROJECT_CONCEPT.concept_en,LH_PROJECT_CONCEPT.distinctive_en,LH_PROJECT_CONCEPT.information_th,LH_PROJECT_CONCEPT.information_en,LH_PROJECT_CONTACTS.name_staff,LH_PROJECTS.project_url,LH_PROJECT_ZONE.zone_id,LH_PROJECT_CONTACTS.telephone_line,LH_PROJECT_CONTACTS.central_value,convert(VARCHAR(10),LH_PROJECT_CONTACTS.prepay,103) prepay,LH_PROJECT_SEO.project_title_seo_th,LH_PROJECTS.project_url_register,LH_PROJECT_SUB.product_id,LH_PROJECTS.company_no_DL200,LH_PROJECTS.project_no_DL200,LH_PROJECTS.project_name_th,LH_PROJECT_ZONE.zone_id_flm
FROM LH_PROJECTS
LEFT JOIN LH_PROJECT_SUB ON LH_PROJECT_SUB.project_id = LH_PROJECTS.project_id
LEFT JOIN LH_PRODUCTS ON LH_PRODUCTS.product_id=LH_PROJECT_SUB.product_id
LEFT JOIN LH_PROJECT_CONCEPT ON LH_PROJECT_CONCEPT.project_id = LH_PROJECTS.project_id
LEFT JOIN LH_PROJECT_AREA_CONDO ON LH_PROJECT_AREA_CONDO.project_sub_id=LH_PROJECT_SUB.project_sub_id
LEFT JOIN LH_PROJECT_PRICES ON LH_PROJECT_PRICES.project_id = LH_PROJECTS.project_id
LEFT JOIN LH_PROJECT_CONTACTS ON LH_PROJECT_CONTACTS.project_id =LH_PROJECTS.project_id
LEFT JOIN LH_PROJECT_MAPS ON LH_PROJECT_MAPS.project_id=LH_PROJECTS.project_id
LEFT JOIN LH_360_PROJECT ON LH_360_PROJECT.project_id=LH_PROJECTS.project_id
LEFT JOIN LH_BRANDS ON LH_BRANDS.brand_id = LH_PROJECTS.brand_id
LEFT JOIN LH_PROJECT_ZONE ON LH_PROJECT_ZONE.project_id = LH_PROJECTS.project_id
LEFT JOIN LH_PROJECT_AREA_HOME ON LH_PROJECT_AREA_HOME.project_sub_id = LH_PROJECT_SUB.project_sub_id
LEFT JOIN LH_PROJECT_SEO ON LH_PROJECT_SEO.project_id = LH_PROJECTS.project_id
WHERE LH_PRODUCTS.product_id !=3 AND LH_PROJECTS.project_status !='SO'");

$NEWHOME_PROJECT_DETAIL2 = new FluidXml('table');
$NEWHOME_PROJECT_DETAIL2->setAttribute('name', 'newhome_project_detail2');
$NEWHOME_PROJECT_DETAIL2->add('GeneratedDateTime', date("d/m/Y H:i:s"));

//define main query fields & data type
$field_array=[
		["label"=>"PROJ_ID","field"=>"project_id","data_type"=>"char"],
		["label"=>"PROJ_VISION2","field"=>"","data_type"=>"varchar"],
		["label"=>"PROJ_VISION_E","field"=>"concept_en","data_type"=>"varchar"],
		["label"=>"PROJ_VISION2_E","field"=>"","data_type"=>"varchar"],
		["label"=>"PROJ_FEATURE_E","field"=>"distinctive_en","data_type"=>"varchar"],
		["label"=>"PV_CODE","field"=>"","data_type"=>"varchar"],
		["label"=>"PV_SHOW","field"=>"","data_type"=>"char"],
		["label"=>"SALES_NAME","field"=>"name_staff","data_type"=>"varchar"],
		["label"=>"URL_PROJECT_DETAIL","field"=>"project_url","data_type"=>"varchar"],
		["label"=>"URL_REGISTER","field"=>"project_url_register","data_type"=>"varchar"],
		["label"=>"PROJ_SUBLOCATION_1198","field"=>"zone_id_flm","data_type"=>"varchar"],
		["label"=>"TEL_DIRECT","field"=>"telephone_line","data_type"=>"varchar"],
		["label"=>"I_COMPANY","field"=>"company_no_DL200","data_type"=>"varchar"],
		["label"=>"I_PROJECT","field"=>"project_no_DL200","data_type"=>"varchar"],
		["label"=>"COMMON_FEE_CHARGE","field"=>"central_value","data_type"=>"decimal"],
		["label"=>"COMMON_FEE_CHARGE_DATE","field"=>"prepay","data_type"=>"date"],
		["label"=>"URL_WEBSITE","field"=>"","data_type"=>"varchar"],
		["label"=>"TITLE_TAG","field"=>"project_title_seo_th","data_type"=>"varchar"],
		["label"=>"ACTIVITY_ORDER","field"=>"","data_type"=>"int"],
		["label"=>"URL_REGISTER_DATE_START","field"=>"","data_type"=>"date"],
		["label"=>"URL_REGISTER_DATE_STOP","field"=>"","data_type"=>"date"],
];

//main loop
$distinct_id=array();
while ($result=mssql_fetch_array($query)) {
	//check for distinct data if not put it to array
	if(!in_array($result['project_id'],$distinct_id)):
		$distinct_id[]=$result['project_id'];
		$pattern['rowdata']=[]; //pattern prepare to blank array
		//put array fields to pattern
		foreach ($field_array as $key => $fields) {
			$field=(string)$fields['field'];
			$label=(string)$fields['label'];
			$type=(string)$fields['data_type'];

			if ($label=="PROJ_VISION_E" || $label=="PROJ_FEATURE_E") {
				$value="<![CDATA[".str_replace(array("\r\n","\r","\n","\\r","\\n","\\r\\n")," ",$result["$field"])."]]>";
				$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
			}elseif($label=="COMMON_FEE_CHARGE"){
				$value="<![CDATA[".preg_replace("/[^0-9]/","",$result["$field"])."]]>";
				$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
			}elseif($label=='URL_PROJECT_DETAIL'){
				if ($result["product_id"]=='1') {
					$url="www.lh.co.th/th/singlehome/project/".str_replace(" ","-",$result["project_name_th"])."/1";
				}elseif ($result["product_id"]=='2') {
					$url="www.lh.co.th/th/townhome/project/".str_replace(" ","-",$result["project_name_th"])."/2";
				}elseif ($result["product_id"]=='3') {
					$url="www.lh.co.th/th/condominium/".str_replace(" ","-",$result["project_name_th"]);
				}
				$value="<![CDATA[".$url."]]>";
				$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
			}elseif($field==''){
				$value="<![CDATA[]]>";
				$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
			}else{
				$value="<![CDATA[".$result["$field"]."]]>";
				$pattern['rowdata']+=["$label"=>['@type'=>"$type","$value"]];
			}
		}

		//near by en
		$near_query=mssql_query("SELECT LH_PROJECT_NEARBY.nearby_name_en,interval FROM LH_PROJECT_NEARBY WHERE project_id={$result['project_id']}");
		if (mssql_num_rows($near_query)>0) {
			$i=1;
			while (($near_r=mssql_fetch_object($near_query)) && ($i<=5)) {
				$near_label="FEMOUS_PLACE".$i."_E";
				$pattern['rowdata']+=["$near_label"=>['@type'=>"varchar","<![CDATA[$near_r->nearby_name_en]]>"]];
				$i++;
			}
		}

		$near_by_num=5-mssql_num_rows($near_query);
		if($near_by_num <= 5 && $near_by_num >= 1){
			$x=mssql_num_rows($near_query)+1;
			for ($i=1; $i <= $near_by_num ; $i++) {
				$near_label="FEMOUS_PLACE".$x."_E";
				$pattern['rowdata']+=["$near_label"=>['@type'=>"varchar","<![CDATA[]]>"]];
				$x++;
			}
		}

		//add img alt PIC_PROJ10_ALT
		$img_alt_query=mssql_query("SELECT LH_GALERY_IMG_PROJECT.galery_project_img_seo FROM LH_GALERY_IMG_PROJECT LEFT JOIN LH_GALERY_PROJECT ON LH_GALERY_PROJECT.galery_project_id = LH_GALERY_IMG_PROJECT.galery_project_id WHERE LH_GALERY_PROJECT.project_id={$result['project_id']}");

		if (mssql_num_rows($img_alt_query)>0) {
			$i=0;
			while (($img_alt_r=mssql_fetch_object($img_alt_query)) && ($i<=10)) {
				$z=($i==0?'':$i);
				$img_label="PIC_PROJ".$z."_ALT";
				$pattern['rowdata']+=["$img_label"=>['@type'=>"varchar","<![CDATA[$img_alt_r->galery_project_img_seo]]>"]];
				$i++;
			}
		}

		$img_alt_num=11-mssql_num_rows($img_alt_query);
		if($img_alt_num <= 11 && $img_alt_num >= 1){
			$x=mssql_num_rows($img_alt_query);
			for ($i=1; $i <= $img_alt_num ; $i++) {
				$z=($x==0?'':$x);
				$img_label="PIC_PROJ".$z."_ALT";
				$pattern['rowdata']+=["$img_label"=>['@type'=>"varchar","<![CDATA[]]>"]];
				$x++;
			}
		}

		$sort_pattern=['PROJ_ID', 'PROJ_VISION2', 'PROJ_VISION_E', 'PROJ_VISION2_E', 'PROJ_FEATURE_E', 'FEMOUS_PLACE1_E', 'FEMOUS_PLACE2_E', 'FEMOUS_PLACE3_E', 'FEMOUS_PLACE4_E', 'FEMOUS_PLACE5_E', 'PV_CODE', 'PV_SHOW', 'SALES_NAME', 'URL_PROJECT_DETAIL', 'URL_REGISTER', 'PROJ_SUBLOCATION_1198', 'TEL_DIRECT', 'I_COMPANY', 'I_PROJECT', 'COMMON_FEE_CHARGE', 'COMMON_FEE_CHARGE_DATE', 'URL_WEBSITE', 'TITLE_TAG', 'PIC_PROJ1_ALT', 'PIC_PROJ2_ALT', 'PIC_PROJ3_ALT', 'PIC_PROJ4_ALT', 'PIC_PROJ5_ALT', 'PIC_PROJ6_ALT', 'PIC_PROJ7_ALT', 'PIC_PROJ8_ALT', 'PIC_PROJ9_ALT', 'PIC_PROJ10_ALT', 'PIC_PROJ_ALT', 'ACTIVITY_ORDER', 'URL_REGISTER_DATE_START', 'URL_REGISTER_DATE_STOP'];
		$pattern_sorted['rowdata'] = array_merge(array_flip($sort_pattern), $pattern['rowdata']);

		//add all data to function
		$NEWHOME_PROJECT_DETAIL2->add($pattern_sorted);
	endif;//
}

$NEWHOME_PROJECT_DETAIL2->save(OUTPUT_PATH."NEWHOME_PROJECT_DETAIL2.xml");
$pattern=NULL;
echo "NEWHOME_PROJECT_DETAIL2 . . . Done<br>";

############### Define function for copy map pdf files
/**
 * Copy a file, or recursively copy a folder and its contents
 * @author      Aidan Lister <aidan@php.net>
 * @version     1.0.1
 * @link        http://aidanlister.com/2004/04/recursively-copying-directories-in-php/
 * @param       string   $source    Source path
 * @param       string   $dest      Destination path
 * @param       int      $permissions New folder creation permissions
 * @return      bool     Returns true on success, false on failure
 */
function xcopy($source, $dest, $permissions = 0755)
{
    // Check for symlinks
    if (is_link($source)) {
        return symlink(readlink($source), $dest);
    }

    // Simple copy for a file
    if (is_file($source)) {
        return copy($source, $dest);
    }

    // Make destination directory
    if (!is_dir($dest)) {
        mkdir($dest, $permissions);
    }

    // Loop through the folder
    $dir = dir($source);
    while (false !== $entry = $dir->read()) {
        // Skip pointers
        if ($entry == '.' || $entry == '..') {
            continue;
        }

        // Deep copy directories
        // if (!file_exists("$dest/$entry")) {
        	xcopy("$source/$entry", "$dest/$entry", $permissions);
        // }
    }

    // Clean up
    $dir->close();
    return true;
}
//delete and copy whole new
//clear
array_map('unlink', glob(MAP_PDF_DES."*"));
//copy all
xcopy(MAP_PDF_SOURCE,MAP_PDF_DES);
//delete un-necessary
$all_map_files = scandir(MAP_PDF_DES);
foreach($all_map_files as $all_map_file) {
	if (!in_array($all_map_file,$map_pdf_file)) {
		@unlink ( MAP_PDF_DES.$all_map_file );
	}

}

echo "COPY FILES . . . Done<br>";

## Create Log file
// Function to get the client IP address
function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}


$ipLog = "log.html"; // Your logfiles name here (.txt or .html extensions ok)
// IP logging function by Dave Lauderdale
// Originally published at: www.digi-dl.com
$ip = get_client_ip();
$date = date("l dS of F Y h:i:s A");
$log = fopen(SYSTEM_PATH."$ipLog", "a+");

if (preg_match("/\\bhtm\\b/i", $ipLog) || preg_match("/\\bhtml\\b/i", $ipLog)) {
    fputs($log, "Logged IP address: $ip - Date logged: $date<br>");
} else {
    fputs($log, "Logged IP address: $ip - Date logged: $date\ ");
}
fclose($log);

echo "LOGGED . . . Done<br>";

