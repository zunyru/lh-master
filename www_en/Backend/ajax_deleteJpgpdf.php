<?php
require_once 'include/dbConnect.php';

    try {
        $project_id = $_GET['project_id'];

        $conn = (new dbConnect())->getConn();
        $sql = "UPDATE LH_PROJECT_MAPS SET map_img = ''
                WHERE project_id =".$project_id;
        $result= $conn->query($sql);

        echo json_encode($result->fetchAll());

    } catch (\Exception $e) {
        return $e->getMessage();
    }
?>