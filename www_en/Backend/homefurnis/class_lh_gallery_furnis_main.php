<?php
/**
 * @author jacky
 * @createdate March 8 2007 
 */
?>
<?php
class LH_GALLARY_FURNISHED{

	// database connection and table name
	private $conn;
	private $table_main ="dbo.LH_GALERY_FURNISHED_MAIN";
	private $table_sub  ="dbo.LH_GALERY_FURNISHED_SUB";
	private $table_projects="dbo.LH_PROJECTS";

	public function __construct($db){
		$this->conn = $db;
	}
	
	public function showAllMainProjectGallery(){
		try{
		   $stmt = $this->conn->prepare(" select ROW_NUMBER() OVER (ORDER BY pj.project_name_th ASC) AS Row,"
		   		                        ." gfm.galery_funished_main_id,gfm.project_id,gfm.folder_name,pj.project_name_th "
		   		                        ." FROM " . $this->table_main . " AS gfm left join ".$this->table_projects
		   		                        ." AS pj ON gfm.project_id = pj.project_id " );
		   $stmt->execute();
		   return $resultsCus=$stmt->fetchAll(PDO::FETCH_ASSOC);
		} catch (PDOException $e) {
			echo "Failed to connect:" . $e->getMessage();
		}
	}
	
	public function showMainProjectGalleryById($galleryFunisMainId){
		if(isset($galleryFunisMainId) && !empty($galleryFunisMainId)){
		    try{
		    	$stmt = $this->conn->prepare(" select gfm.galery_funished_main_id,gfm.project_id,gfm.folder_name,pj.project_name_th "
		    			." FROM " . $this->table_main . " AS gfm left join ".$this->table_projects
		    			." AS pj ON gfm.project_id = pj.project_id"
		    			." where gfm.galery_funished_main_id = ".$galleryFunisMainId);
		    	$stmt->execute();
		    	return $resultsCus=$stmt->fetchAll(PDO::FETCH_ASSOC);
		    	} catch (PDOException $e) {
		    		echo "Failed to connect:" . $e->getMessage();
		    	}
		}
	}
	
	public function showProjectList(){
		try{
		    $stmt = $this->conn->prepare("select distinct(project_id) as id ,project_name_th as projectname from ".$this->table_projects);
		    $stmt->execute(); 
		    return $resultsCus=$stmt->fetchAll(PDO::FETCH_ASSOC);
		    } catch (PDOException $e) {
		    	echo "Failed to connect:" . $e->getMessage();
		    }
	}
	
	
}
?>





















