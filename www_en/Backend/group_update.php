<?php
session_start();
$_SESSION['group_id'];
include_once('include/dbGroups.php');
include 'include/dbCon_mssql.php';

$_GET['page']='group';
$group_id = $_GET['id'];
    $funGroup = new dbGroups();
    if(isset($_POST['submit'])){
        $group_name = $_POST['groupsname'];
         $password = $_POST['password'];

        $dates=date("Y-m-d H:i:s");
        $md5_pass=md5($password);
        $password;

        $sql="SELECT COUNT(group_id) AS num FROM LH_GROUPS WHERE group_name = '$group_name' AND NOT group_id = '$group_id'";
        $query_= mssql_query($sql );
        $row = mssql_fetch_array($query_);
        if($row['num'] == 0) {

            $query_up = "UPDATE LH_GROUPS SET group_name='$group_name',group_password='$password',group_password_md5='$md5_pass',group_update = '$dates',group_account_name = '$group_name' WHERE group_id='$group_id'";
            $query_up = mssql_query($query_up);
            $success="success";
        }else{
            $error="error"; //ค่าซ้ำ
        }

    }else if(isset($_POST['delete'])){
        $delete = $funGroup->delete_group($group_id);
        if($delete){
          header("location:group.php?delete=y");
        }else {
          //echo "<script>alert('aaaa')</script>";
          $error="error"; //ค่าซ้ำ

        }
    }

    if(isset($_SESSION['add'])){
        if($_SESSION['add'] == -2){
            $error2 = 'not_delete';
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>


      <!-- Bootstrap -->
      <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome -->
      <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <!-- NProgress -->
      <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
      <!-- Custom Theme Style -->
      <link href="../build/css/custom.min.css" rel="stylesheet">
      <!-- Fancybox image popup -->
      <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
      <!-- bootstrap-progressbar -->
      <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
      <!-- jQuery -->
      <script src="../vendors/jquery/dist/jquery.min.js"></script>

      <script src="../build/js/custom.min.js"></script>      <script src="../build/js/custom.min.js"></script>
      <!-- uploade img -->
      <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
      <!--uploade-->
      <!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
      <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
      <!-- confirm-->
      <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
      <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <?php include './master/navbar.php';?>
            <!-- /sidebar menu -->
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
          <?php include './master/top_nav.php'; ?>
        <!-- /top navigation -->
        <?php
          $qr_id = $funGroup->list_group($group_id);
          $resulft = $qr_id->fetch( PDO::FETCH_ASSOC );

        ?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><a href="group.php">ระบบจัดการข้อมูล Group</a> > <a href="#">แก้ไข Group <?=$resulft['group_name'];?></a></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <p class="font-gray-dark">
                  </p><br>
                   <?php if(isset($success)){?>
                   <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>แก้ไขข้อมูลเรียบร้อยแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                  <?php }else if(isset($error)){?>
                    <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>มี Group นี้แล้ว !</strong> ชื่อ Group ซ้ำกัน
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                  <?php }?>
                    <?php  if(isset($error2)){?>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="form-group">
                                <div class="alert alert-danger col-md-6" id="alert">
                                    <button type="button" class="close" data-dismiss="alert">x</button>
                                    <strong>ไม่สามารถลบข้อมูลนี้ได้ !</strong> ข้อมูลที่ผูกกับ Group นี้อยู่
                                </div>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                    <?php }?>

                  <form class="form-horizontal form-label-left" action="" method="post" id="commentForm">
                    <div class="form-group">
                      <label class="control-label col-md-2"  for="first-name">ระบุชื่อ Group <span class="required">*</span>
                      </label>
                      <div class="col-md-6">
                          <input type="text"  name="groupsname" required="required" class="form-control col-md-7 col-xs-12" value="<?=$resulft['group_name']?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2" for="first-name">Password <span class="required">*</span>
                      </label>
                      <div class="col-md-6">
                          <input type="password" name="password"  required="required" class="form-control col-md-7 col-xs-12" value="<?=$resulft['group_password']?>">
                      </div>
                    </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-8">
                        <center><br>
                          <button type="submit" name="submit" class="btn btn-success">Update</button>
                            <a class="confirms" data-title="ยืนยันการลบข้อมูล" name="delete" href="delete_group.php?id=<?=$_GET['id']?>">
                                <button type="button" class="btn btn-danger">Delete</button></a>

                        </center>
                      </div>
                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
            <script type="text/javascript">
                $('a.confirms').confirm({
                    content: 'คุณแน่ใจที่จะลบข้อมูลนี้ !',
                    buttons: {
                        Yes: {
                            text: 'Yes',
                            btnClass: 'btn-danger',
                            keys: ['enter', 'a'],
                            action: function(){
                                location.href = this.$target.attr('href');
                            }
                        },
                        No: {
                            text: 'No',
                            btnClass: 'btn-default',
                            keys: ['enter', 'a'],
                            action: function(){
                                // button action.
                            }
                        },

                    }
                });
            </script>
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

        <!-- Custom Theme Scripts -->
        <script src="../build/js/custom.min.js"></script>

        <!-- Validate -->
        <script src="../build/js/jquery.validate.js"></script>
        <script>
            $(document).ready(function() {

                $("#commentForm").validate({
                    rules: {
                        groupsname: "required",


                    },
                    messages: {
                        groupsname: "กรุณากรอกชื่อ group !",
                        password : "กรุณากรอก password !",


                    }
                });

                $("#lead_img_file").rules("add", {
                    required:true,
                    'hobbies[]': "เลือกอย่างน้อย 1 รายการ"
                });

            });
        </script>
        <!-- jQuery Tags Input -->
        <!-- jQuery Tags Input -->
    <script>
      function onAddTag(tag) {
        alert("Added a tag: " + tag);
      }

      function onRemoveTag(tag) {
        alert("Removed a tag: " + tag);
      }

      function onChangeTag(input, tag) {
        alert("Changed a tag: " + tag);
      }

      $(document).ready(function() {
        $('#tags_1').tagsInput({
          width: 'auto'
        });
        $('#tags_2').tagsInput({
          width: 'auto'
        });
      });
    </script>
    <!-- /jQuery Tags Input -->
        <script>
            $(document).ready(function(){
                $("#alert").show();
                $("#alert").fadeTo(3000, 500).slideUp(500, function(){
                    $("#alert").alert('close');
                });
            });
        </script>

  </body>
</html>
