<?php
session_start();
$_SESSION['group_id'];
include_once('include/dbBrands.php');
header( 'Content-Type:text/html; charset=utf8');
$_GET['page']='brand';


if (isset($_SESSION['add'])) {
	if ($_SESSION['add'] == 1) {
		//header("location:product_add.php");
		$success = "success";
		unset($_SESSION["add"]);
	} else if ($_SESSION['add'] == 0) {

		$error = "error"; //ค่าซ้ำ
		$_SESSION['add'] = '';

	} else if ($_SESSION['add'] == -1) {
		$errors = "errors";
		unset($_SESSION["add"]);
	}
}

?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <!-- Meta, title, CSS, favicons, etc. -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>LAND & HOUSES</title>

      <!-- Bootstrap -->
      <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome -->
      <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <!-- NProgress -->
      <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
      <!-- bootstrap-progressbar -->
      <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
      <!-- Custom Theme Style -->
      <link href="../build/css/custom.min.css" rel="stylesheet">

      <!-- jQuery -->
      <script src="../vendors/jquery/dist/jquery.min.js"></script>

      <!-- uploade img -->
      <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
      <!--uploade-->
      <!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
      <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
      <!-- confirm-->
      <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
      <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>


      <style>
      .file-caption-main .btn-file {
        overflow: visible;
      }

      .file-caption-main .btn-file .error {
        position: absolute;
        bottom: -32px;
        right: 30px;

      }
    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <?php include './master/navbar.php';?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include './master/top_nav.php';?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><a href="brand.php">ระบบจัดการข้อมูล Brand</a> > สร้าง Brand </h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <p class="font-gray-dark">
                    </p><br>
                    <?php if (isset($success)) {?>
                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="form-group">
                        <div class="alert alert-success col-md-6" id="alert">
                          <button type="button" class="close" data-dismiss="alert">x</button>
                          <strong>สร้างข้อมูลเรียบร้อยแล้ว</strong>
                        </div>
                      </div>
                      <div class="col-md-6"></div>
                    </div>
                    <?php } else if (isset($error)) {?>
                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="form-group">
                        <div class="alert alert-danger col-md-6" id="alert">
                          <button type="button" class="close" data-dismiss="alert">x</button>
                          <strong>มีชื่อนี้อยู่ในระบบแล้ว</strong>
                        </div>
                      </div>
                      <div class="col-md-4"></div>
                    </div>
                    <?php } else if (isset($errors)) {?>

                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="form-group">
                        <div class="alert alert-danger col-md-6" id="alert">
                          <button type="button" class="close" data-dismiss="alert">x</button>
                          <strong>เกิดข้อผิดพลาด !</strong> ไม่สามารถบันทึกข้อมมูลได้
                        </div>
                      </div>
                      <div class="col-md-4"></div>
                    </div>
                    <?php } echo $_SESSION['add'];?>
                    <form class="form-horizontal form-label-left" action="save_brand.php" method="post" enctype="multipart/form-data" id="commentForm">
                      <div class="form-group">
                        <label class="control-label col-md-2" for="first-name">ระบุชื่อ Brand อังกฤษ <span class="required">*</span>
                        </label>
                        <div class="col-md-6">
                          <input type="text"  class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อ Brand (อังกฤษ)" name="brand_name_th" value="">
                        </div>
                      </div>
                      <div class="form-group hide-for-th">
                        <label class="control-label col-md-2" for="first-name">ระบุชื่อ Brand อังกฤษ <span class="required">*</span>
                        </label>
                        <div class="col-md-6">
                          <input type="text"   class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อ Brand (อังกฤษ)" name="brand_name_en" value="">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-2" for="first-name">Brand Logo อังกฤษ<br>(.jpg , .png , .gif <br> ขนาด 150 x 150 px) <br> ภาพขนาดไม่เกิน 300 KB <span class="required">*</span>
                        </label><br>
                        <div class="col-md-6">
                          <input id="fileupload_th" name="fileupload_th" type="file" multiple class="file-loading" accept="image/*" >
                        </div>
                      </div>

                      <script>
                        $("#fileupload_th").fileinput({
                              uploadUrl: "upload.php", // server upload action
                              maxFileCount: 1,
                              allowedFileExtensions: ["jpg", "png", "jpeg" , "gif"],
                              browseLabel: 'เลือกรูป',
                              removeLabel: 'ลบ',
                              browseClass: 'btn btn-success',
                              showUpload: false,
                              showRemove:false,
                              showCaption: false, // ปิดช่องแสดงชื่อไฟล์
                              msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                              msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                              msgZoomModalHeading: 'ตัวอย่างละเอียด',
                              xxx:'brand_img_seo_th',
                              dropZoneTitle : 'รูป Brand Logo อังกฤษ',

//                              minImageWidth: 150, //ขนาด กว้าง ต่ำสุด
//                              minImageHeight: 150, //ขนาด ศุง ต่ำสุด
//                              maxImageWidth: 150, //ขนาด กว้าง ต่ำสุด
//                              maxImageHeight: 150, //ขนาด ศุง ต่ำสุด
maxFileSize :300 ,
msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
});
</script>


<div class="form-group hide-for-th">
  <label class="control-label col-md-2" for="first-name">Brand Logo อังกฤษ<br>(.jpg , .png , .gif <br> ขนาด 150 x 150 px) <br> ภาพขนาดไม่เกิน 300 KB<span class="required">*</span>
  </label><br>
  <div class="col-md-6">
    <input id="fileupload_en" name="fileupload_en" type="file" multiple class="file-loading" accept="image/*" >
  </div>
</div>

<script>
  $("#fileupload_en").fileinput({
                              uploadUrl: "upload.php", // server upload action
                              maxFileCount: 1,
                              allowedFileExtensions: ["jpg", "png", "jpeg" , "gif"],
                              browseLabel: 'เลือกรูป',
                              removeLabel: 'ลบ',
                              browseClass: 'btn btn-success',
                              showUpload: false,
                              showRemove: false,
                              showCaption: false, // ปิดช่องแสดงชื่อภาพ
                              msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                              msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                              msgZoomModalHeading: 'ตัวอย่างละเอียด',
                              xxx:'brand_img_seo_en',
                              dropZoneTitle : 'รูป Brand Logo อังกฤษ',
//                              minImageWidth: 150, //ขนาด กว้าง ต่ำสุด
//                              minImageHeight: 150, //ขนาด ศุง ต่ำสุด
//                              maxImageWidth: 150, //ขนาด กว้าง ต่ำสุด
//                              maxImageHeight: 150, //ขนาด ศุง ต่ำสุด
maxFileSize :300 ,
msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
});
</script>

<div class="form-group">
  <label class="control-label col-md-2" for="first-name">Brand Concept (อังกฤษ) <span class="required"></span>
  </label>
  <div class="col-md-6">
    <textarea id="message"  class="form-control" name="concept_th"  rows="7" placeholder="กรอกBrand Concept (อังกฤษ)"></textarea>
  </div>
</div>
<div class="form-group">
  <label class="control-label col-md-2" for="first-name">รายละเอียด Brand Concept (อังกฤษ)<span class="required"></span>
  </label>
  <div class="col-md-6">
    <textarea id="message" class="form-control" name="message_th"  rows="7"  placeholder="กรอกรายละเอียด Brand Concept (อังกฤษ)"></textarea>
  </div>
</div>
<div class="form-group hide-for-th">
  <label class="control-label col-md-2" for="first-name">Brand Concept (อังกฤษ)<span class="required"></span>
  </label>
  <div class="col-md-6">
    <textarea id="message"  class="form-control" name="concept_en" placeholder="กรอกBrand Concept (อังกฤษ)" rows="7" ></textarea>
  </div>
</div>
<div class="form-group hide-for-th">
  <label class="control-label col-md-2" for="first-name">รายละเอียด Brand Concept (อังกฤษ)<span class="required"></span>
  </label>
  <div class="col-md-6">
    <textarea id="message"  class="form-control" name="message_en"  rows="7"  placeholder="กรอกรายละเอียด Brand Concept (อังกฤษ)"></textarea>
  </div>
</div>
</div>
<center><br>
  <div class="col-md-6">
    <button type="submit" name="submit" class="btn btn-success">Submit</button>
    <!--<button type="button" name="delete" class="btn btn-danger">Delete</button>-->
  </div>
</center>
</form>
</div>
</div>
</div>
</div>
<!-- /page content -->

<!-- footer content -->
        <!-- <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer> -->
        <!-- /footer content -->
      </div>
    </div>


    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <script type="text/javascript" src="js/bootstrap-imgupload.js"></script>
    <!-- Validate -->
    <script src="../build/js/jquery.validate.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>


    <script type="text/javascript">
      $('.fancybox').fancybox();
    </script>
    <script type="text/javascript">
      $('.imgupload').imgupload();
    </script>
    <script type="text/javascript">
      $('#imgupload2').imgupload();
    </script>
    <script type="text/javascript">
      $('#imgupload1').imgupload();
    </script>




    <script>
      $(document).ready(function() {

        $("#commentForm").validate({
          rules: {
            brand_name_th: "required",
            brand_name_en: "required",
            fileupload_th : "required",
            fileupload_en : "required",
          },
          messages: {
            brand_name_th: "กรุณากรอกชื่อ Brand อังกฤษ !",
            brand_name_en : "กรุณากรอกชื่อ Brand อังกฤษ !",
            fileupload_th : " &nbsp; กรุณาเลือกรูป !",
            fileupload_en : " &nbsp; กรุณาเลือกรูป !",
          }
        });

            // $("#fileupload_th").rules("add", {
            //     required:true,
            //     messages: {
            //         required: " &nbsp; กรุณาเลือกรูป !"
            //     }
            // });

          });
        </script>


        <!-- /jQuery Tags Input -->
        <script>
          $(document).ready(function(){
            $("#alert").show();
            $("#alert").fadeTo(6000, 500).slideUp(500, function(){
              $("#alert").alert('close');
            });
          });
        </script>
        <script src="js/validate_file_300kb.js"></script>


      </body>
      </html>