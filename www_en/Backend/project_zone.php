 <?php 
 $sqlzone = "SELECT * FROM LH_PROJECT_ZONE WHERE project_id = '".$_GET['project_id']."' ORDER BY zone_id ";
 $queryzone = mssql_query($sqlzone);

 $i = 0;
 $j= 1;
 while ($zonedata = mssql_fetch_array($queryzone)) {

  ?>
  <div>
   <div class="form-group" id="district" <?=$display?>>
    <label class="control-label col-md-3" for="zone">ระบุ Zone <span class="required">*</span></label>
    <div class="col-md-4 col-sm-9 col-xs-12">
      <select readonly class="form-control detailZone_id zone" name="detailZone_id[]" id="detailZone_id<?=$i?>" onChange="changeZone(value,<?=$i?>);"  required >
        <option value="">เลือกโซน</option>
        <?php
        $sql = "SELECT * FROM LH_ZONES  WHERE zone_attribute = 'websiteflm' OR zone_attribute = 'website' ORDER BY zone_name_th ASC";
        $query = mssql_query($sql);
        while ($value = mssql_fetch_array($query)) {
          ?>
          <?php if ($value['zone_id'] == $detailzone[$i]['zone_id']) { ?>
          <option value="<?php echo $value['zone_id']; ?>" selected>
            <?php echo $value['zone_name_th']; ?>
          </option>
          <?php } else { ?>
          <option value="<?php echo $value['zone_id']; ?>">
            <?php echo $value['zone_name_th']; ?>
          </option>
          <?php } ?>
          <?php }?>
        </select>
      </div>
    </div>

    <?php if($_SESSION['group_status'] == 'sell'){?>

    <div class="form-group" >
      <label class="control-label col-md-3" for="zone">ระบุ Zone <span class="required">*</span></label>
      <div class="col-md-4 col-sm-9 col-xs-12">
        <select disabled class="form-control" >
          <option value="">เลือกโซน</option>
          <?php
          $sql = "SELECT * FROM LH_ZONES  WHERE zone_attribute = 'websiteflm' OR zone_attribute = 'website' ORDER BY zone_name_th ASC";
          $query = mssql_query($sql);
          while ($value = mssql_fetch_array($query)) {
            ?>
            <?php if ($value['zone_id'] == $detailzone[$i]['zone_id']) { ?>
            <option value="<?php echo $value['zone_id']; ?>" selected>
              <?php echo $value['zone_name_th']; ?>
            </option>
            <?php } else { ?>
            <option value="<?php echo $value['zone_id']; ?>">
              <?php echo $value['zone_name_th']; ?>
            </option>
            <?php } ?>
            <?php }?>
          </select>
        </div>
      </div>

      <?php }?>
      <!-- ./ class=form-group -->

      <!-- class=form-group -->

      <div class="form-group">
        <label class="control-label col-md-3" for="zone">ระบุชื่อจังหวัด  <span class="required">*</span></label>
        <div class="col-md-4 col-sm-9 col-xs-12">
          <select name="province_id[]" id="province_id<?=$i?>" class="form-control" onChange="changeProvince(value)" required disabled>
            <option value="">เลือกจังหวัด</option>
            <?php
            $sql_p = "SELECT * FROM LH_ZONES WHERE zone_id = '".$detailzone[$i]['zone_id']."' ";
            $query_p = mssql_query($sql_p);
            $rows = mssql_fetch_array($query_p);
            $id_p=$rows['province_id'];

            ?>
            <?php
            $sql_project_zone = "SELECT p.province_name , p.province_id
            FROM LH_PROJECT_ZONE z LEFT JOIN LH_ZONES s ON z.zone_id = s.zone_id
            LEFT JOIN LH_PROVINCES p ON p.PROVINCE_ID = s.PROVINCE_ID
            WHERE z.project_id = '".$_GET['project_id']."'";
            $res_project_zone = mssql_query($sql_project_zone , $GLOBALS['db_conn']);
            $res_project_zone = mssql_fetch_object($res_project_zone);
            ?>
            <?php foreach (getProvince() as $value) { ?>

            <?php if ($value['PROVINCE_ID'] == $id_p) { ?>
            <option  value="<?php echo $value['PROVINCE_ID']; ?>" selected>
              <?php echo $value['PROVINCE_NAME']; ?>
            </option>
            <?php } else { ?>
            <option  value="<?php echo $value['PROVINCE_ID']; ?>">
              <?php echo $value['PROVINCE_NAME']; ?>
            </option>
            <?php } ?>
            <?php }?>

            <input type="hidden" id="province_choose<?=$i?>" value="<?= $res_project_zone->province_id ?>">
            <input type="hidden" id="amphur_choose<?=$i?>" value="<?= $res_project_zone->amphur_id ?>">
            <input type="hidden" id="district_choose<?=$i?>" value="<?= $res_project_zone->district_id ?>">
          </select>
        </div>
      </div>
      <!-- ./ class=form-group -->

      <!-- class=form-group -->
      <div class="form-group">
        <label class="control-label col-md-3" for="zone">ระบุชื่ออำเภอ <span class="required"></span></label>
        <div class="col-md-4 col-sm-9 col-xs-12">
          <select name="amphur_id[]" id="amphur_id<?=$i?>" class="form-control" onChange="changeAmpur(value,<?=$i?>)">
            <option value="">เลือกอำเภอ</option>
            <?php foreach (getAmphur($rows['province_id']) as $value) { ?>
            <?php if ($value['AMPHUR_ID'] == $detailzone[$i]['amphur_id']) { ?>
            <option value="<?php echo $value['AMPHUR_ID']; ?>" selected>
              <?php echo $value['AMPHUR_NAME']; ?>
            </option>

            <?php } else { ?>
            <option value="<?php echo $value['AMPHUR_ID']; ?>">
              <?php echo $value['AMPHUR_NAME']; ?>
            </option>
            <?php } ?>
            <?php }?>
          </select>
        </div>
      </div>
      <!-- ./ class=form-group -->
      <?php
      $sql_am ="SELECT amphur_id FROM LH_PROJECT_ZONE z WHERE project_id = '".$_GET['project_id']."' ";
      $query_am = mssql_query($sql_am);
      $array_am = mssql_fetch_array($query_am);
      ?>

      <!-- class=form-group -->
      <div class="form-group">
        <label class="control-label col-md-3" for="zone">ระบุชื่อตำบล  <span class="required"></span></label>
        <div class="col-md-4 col-sm-9 col-xs-12">
          <select name="district_id[]" id="district_id<?=$i?>" class="form-control">
            <option value="">เลือกตำบล</option>
            <?php foreach (getDistrict($array_am['amphur_id']) as $value) { ?>
            <?php if ($value['DISTRICT_ID'] == $detailzone[0]['district_id']) { ?>
            <option value="<?php echo $value['DISTRICT_ID']; ?>" selected>
              <?php echo $value['DISTRICT_NAME']; ?>
            </option>
            <?php } else { ?>
            <option value="<?php echo $value['DISTRICT_ID']; ?>">
              <?php echo $value['DISTRICT_NAME']; ?>
            </option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
      </div>

      <?php if($_SESSION['group_status'] != 'sell'){
        if($i != 0){

          ?>
          <div class="form-group">
            <label class="control-label col-md-3" ></label>
            <div class="col-md-4 col-sm-9 col-xs-12">
              <button type="button" class="btn btn-danger" onclick="RemoveZone_master(this,<?=$i?>);">- ลบทำเล</button>
            </div>
          </div>
          <?php }}?>
          <hr>
        </div> 
        <?php  $i++; $j++; }?>   

        <?php if($_SESSION['group_status'] != 'sell'){ ?>
        <div id="divzone"></div> 

        <div class="form-group">
          <label class="control-label col-md-3"></label>
          <label id="error-checkbox-zone" class="col-md-3" style="color: #F44300;"></label>
        </div>

        <div class="form-group">
          <label class="control-label col-md-5"></label>
          <div class="col-md-4 col-sm-9 col-xs-12">
            <button type="button" class="btn btn-success" onclick="addZone()">+ เพิ่มทำเล</button>
          </div>
        </div> 
        <br>
        <?php }?>

        <script type="text/javascript">

          function addZone(){
            var index = $('.zone').length;

            var zone =`<!-- zone -->
            <div>
            <div class="form-group">
            <label class="control-label col-md-3" for="zone">ระบุ Zone <span class="required">*</span>
            </label>
            <div class="col-md-4 col-sm-9 col-xs-12">
            <select name="detailZone_id[]" class="form-control zone zone_add" id="zone_id_add${index}" required >
            <option value="">เลือกโซน</option>`;
            <?php
              $sql = "SELECT * FROM LH_ZONES WHERE zone_attribute = 'websiteflm' OR zone_attribute = 'website' ORDER BY zone_name_th ASC";
              $queryzones = mssql_query($sql);
              while ($value = mssql_fetch_array($queryzones)) {
            ?>

            zone+='<option value="<?php echo $value['zone_id']; ?>"><?=$value['zone_name_th'];?></option>';

            <?php } ?>

            zone+=`</select>
            </div>
            </div>

            <div class="form-group">
            <label class="control-label col-md-3" for="zone">ระบุชื่อจังหวัด   <span class="required">*</span></label>
            <div class="col-md-4 col-sm-9 col-xs-12">
            <select name="province_id[]" id="province_id_add${index}" class="form-control province_id" required disabled onChange="changeProvince2(value,${index})">
            <option value="">เลือกจังหวัด</option></select>
            </div>
            </div>

            <div class="form-group">
            <label class="control-label col-md-3" for="zone">ระบุชื่ออำเภอ  </label>
            <div class="col-md-4 col-sm-9 col-xs-12">
            <select name="amphur_id[]" id="amphur_id_add${index}" class="form-control" onChange="changeAmpur2(value,${index})">
            <option value="">เลือกอำเภอ</option>
            </select>
            </div>
            </div>

            <div class="form-group">
            <label class="control-label col-md-3" for="zone">ระบุชื่อตำบล  </label>
            <div class="col-md-4 col-sm-9 col-xs-12">
            <select name="district_id[]" id="district_id_add${index }" class="form-control district_id">
            <option value="">เลือกตำบล</option>
            </select>
            </div>
            </div>

            <div class="form-group">
            <label class="control-label col-md-3" ></label>
            <div class="col-md-4 col-sm-9 col-xs-12">
            <button type="button" class="btn btn-danger" onclick="RemoveZone(this,${index });">- ลบทำเล</button>
            </div>
            </div>
            <hr>
            </div>

            <!-- end zone -->`;

            $('#divzone').append(zone);

            var flm = `<div class="form-group id="flm${index }">
            <label class="control-label col-md-3" >ทำเลที่ตั้งสำหรับ FLM 
            <span class="required">*</span>
            </label>
            <div class="col-md-4">
            <select name="flm[]" class="form-control flm" required ">
            <option  value="">กรุณาเลือก</option>`;

            <?php foreach (getFLmZone() as $value) : ?>
            
            flm += '<option value="<?=$value['zone_id']; ?>"><?=$value['zone_name_th']; ?></option>';

            <?php endforeach; ?>
            flm += '</select></div> </div>';

            $('#flmform').append(flm);

          }


          function RemoveZone(element,id_flm)
          {
            $(element).parent().parent().parent().remove();
            $('#flm'+id_flm).remove();
          }

          function RemoveZone_master(element,id_flm)
          {
            $(element).parent().parent().parent().remove();
            $('#flm'+id_flm).remove();
          }  
        </script>
        <script>
         $(document).ready(function(){

        // if($(".province_id").val()){
        //   changeProvince($(this).val());
        // }
        // if($(".amphur_id").val()){
        //   changeAmpur($(this).val());
        // }

        $(document).on('change', "select.zone_add", function() {

         var str = $(this).attr('id');
         var id_zone = str.replace("zone_id_add", "");
         console.log(id_zone);

         var params  = {
          zone_id: $('#zone_id_add'+id_zone).val()
        };
        getData2(params,id_zone);
        console.log($("#province_id_add"+id_zone).val());
        changeProvince2($("#province_id_add"+id_zone).val(),id_zone);
        changeAmpur2($("#amphur_id_add"+id_zone).val(),id_zone);
      });

      });


         function getData2(params,id) {
          var id_zone = id;
          console.log(id_zone);
          $.ajax({
            type: "POST",
            url: "search_zone.php",
            data: params,
            async: false,
            cache: false
          }).success(function (data) {
            console.log(data);
            if(data.provinces){
                // $('#province_id'+id_zone).empty();
                console.log(id_zone);
                $('#province_id_add'+id_zone).append('<option value="'+data.provinces.province_id+'" selected >'+data.provinces.province_name+'('+data.provinces.province_name_eng+')'+'</option>')
              }
            });
        }

        function changeProvince2(id,index){
          console.log(id);
          if(id != 0) {
            var url = "ajax_getDataRefProvince.php";
            var link = url+'?province_id='+id;
            $.ajax( link )
            .done(function(response) {
              if (response != 0) {
              //console.log(response)
              if(response != ""){
                if(typeof response === "object"){
                  var json_obj = response;
                }else{
                              var json_obj =  $.parseJSON(response);//parse JSON
                            }
                          }

                      //Clear Elemant <Option>
                      $('#amphur_id_add'+index).empty();
                      $('#district_id_add'+index).empty();
                      $('#amphur_id_add'+index).val(json_obj.result_data[0].PROVINCE_NAME);
                      var htmlOption = '<option value="">เลือกอำเภอ</option>';
                      var htmlOption2 = '<option value="">เลือกตำบล</option>';
                      for (var i = 0; i < json_obj.result_data.length; i++) {
                        var toStr = json_obj.result_data[i].AMPHUR_NAME + '/ ' + json_obj.result_data[i].AMPHUR_NAME_ENG;
                        htmlOption += '<option value="'+json_obj.result_data[i].AMPHUR_ID+'">'+toStr+'</option>';
                      }
                      $('#amphur_id_add'+index).append(htmlOption);
                      $('#district_id+add'+index).append(htmlOption2);
                    }else{

                    }
                  })
            .fail(function() {

                    //Clear Elemant <Option>
                    $('#amphur_id_add'+index).empty();
                    $('#district_id_add'+index).empty();
                    console.error('Backend error');
                  })
            .always(function() {
              console.info( "complete" );
            });
          }else {

          //Clear Elemant <Option>
          $('#amphur_id_add').empty();
          $('#district_id_add').empty();
        }
      }

      function changeAmpur2(id,index){
        if(id != 0) {
          var url = "ajax_getDataRefAmpur.php";
          var link = url+'?ampur_id='+id;
          $.ajax( link )
          .done(function(response) {
            if (response != 0) {
              console.log(response)
              if(response != ""){
                if(typeof response === "object"){
                  var json_obj = response;
                }else{
                              var json_obj =  $.parseJSON(response);//parse JSON
                            }
                          }

                      //Clear Elemant <Option>
                      $('#district_id_add'+index).empty();
                      $('#district_id_add'+index).val(json_obj.result_data[0].PROVINCE_NAME);
                      var htmlOption = '<option value="">เลือกตำบล</option>';
                      for (var i = 0; i < json_obj.result_data.length; i++) {
                        var toStr = json_obj.result_data[i].DISTRICT_NAME + '/ ' + json_obj.result_data[i].DISTRICT_NAME_ENG;
                        htmlOption += '<option value="'+json_obj.result_data[i].DISTRICT_ID+'">'+toStr+'</option>';
                      }
                      $('#district_id_add'+index).append(htmlOption);
                    }else{

                    }
                  })
          .fail(function() {

                    //Clear Elemant <Option>
                    $('#district_id_add'+index).empty();
                    console.error('Backend error');
                  })
          .always(function() {
            console.info( "complete" );
          });
        }else {

          //Clear Elemant <Option>
          $('#district_id_add'+index).empty();
        }
      }

    </script> 
