var elem = document.querySelector('.color-input');
// var hueb = new Huebee( elem, {
//     notation: 'hex'
//             // options
//         });

        //
        $(document ).ready(function() {
            $('#page').on('change', function() {
                console.log( $('#page').val());
            })

            intBanner();

            $('input[name="upvido"]').change(function() {
                $('input[name="chageBannerChoose"]').val("change");
                resetForm('Video');
                upVideoShow(this.value);
            });
        });

        if($('#banner_chek').val() == 'hidden'){
          $("#banner").hide();
          $('#ch_banner').prop('checked', false);
      }else{
         $("#banner").show();
         $('#ch_banner').prop('checked', true);
     }

     $('#ch_banner').click(function() {
        $("#banner").toggle(this.checked);
    });

        //add_lead_image mobile
        function add_lead_image()
        {

            var index = $('.setIndexProject_galery_galley').length;

            var form = `<div><div class="col-sm-3">
            <div class="form-group setIndexProject_galery">
            <div class="col-md-12">
            <input type="file" id="lead_image_m${index + 1}" class="lead_image" name="lead_image_m[]" accept="image/*" />
            </div>
            </div>

            <button  type="button" class="btn btn-round btn-danger" style="margin-left: 112px;" onclick="removeLead_image(this)"> - ลบ Banner Mobile</button>

            </div>
            </div>`;

            $('#lead_image_project_m').append(form);

            $(".lead_image").fileinput({
                uploadUrl: "upload.php", // server upload action
                maxFileCount: 1,
                allowedFileExtensions: ["jpg", "png", "jpeg"],
                browseLabel: ' เลือก Banner Mobile',
                browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                removeLabel: 'ลบ',
                browseClass: 'btn btn-success',
                showUpload: false,
                showRemove:false,
                showCaption: false,
                msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                msgZoomModalHeading: 'ตัวอย่างละเอียด',
                xxx:'lead_image_m',
                minImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
                minImageHeight: 1920, // ขนาด ศุง ต่ำสุด
                maxImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
                maxImageHeight: 1920, // ขนาด ศุง ต่ำสุด
                dropZoneTitle : 'Banner Mobile',
                msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
            });
        }

        function removeLead_image(element)
        {
            //$('#gallery_add').prop("disabled", false);
            $(element).parent().remove();
        }

        //
        //add_lead_image d
        function add_lead_image_d()
        {

            var index = $('.setIndexProject_galery_galley').length;

            var form = `<div><div class="col-sm-3">
            <div class="form-group setIndexProject_galery">
            <div class="col-md-12">
            <input type="file" id="lead_image${index + 1}" class="lead_image" name="lead_image_d[]" accept="image/*" />
            </div>
            </div>

            <button  type="button" class="btn btn-round btn-danger" style="margin-left: 112px;" onclick="removeLead_image(this)"> - ลบ Banner Default </button>

            </div>
            </div>`;

            $('#lead_image_project_d').append(form);

            $(".lead_image").fileinput({
                uploadUrl: "upload.php", // server upload action
                maxFileCount: 1,
                allowedFileExtensions: ["jpg", "png", "jpeg"],
                browseLabel: ' เลือก Banner Default',
                browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                removeLabel: 'ลบ',
                browseClass: 'btn btn-success',
                showUpload: false,
                showRemove:false,
                showCaption: false,
                msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                msgZoomModalHeading: 'ตัวอย่างละเอียด',
                xxx:'lead_image_d',
                minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                dropZoneTitle : 'Banner Default',
                msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
            });
        }

        function removeLead_image(element)
        {
            //$('#gallery_add').prop("disabled", false);
            $(element).parent().remove();
        }

        //add_lead_image p
        function add_lead_image_p()
        {

            var index = $('.setIndexProject_galery_galley').length;

            var form = `<div><div class="col-sm-3">
            <div class="form-group setIndexProject_galery">
            <div class="col-md-12">
            <input type="file" id="lead_image_p${index + 1}" class="lead_image" name="lead_image_p[]" accept="image/*" />
            </div>
            </div>

            <button  type="button" class="btn btn-round btn-danger" style="margin-left: 112px;" onclick="removeLead_image(this)"> - ลบ Banner Lead </button>

            </div>
            </div>`;

            $('#lead_image_project_p').append(form);

            $(".lead_image").fileinput({
                uploadUrl: "upload.php", // server upload action
                maxFileCount: 1,
                allowedFileExtensions: ["jpg", "png", "jpeg"],
                browseLabel: ' เลือก Banner MB',
                browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                removeLabel: 'ลบ',
                browseClass: 'btn btn-success',
                showUpload: false,
                showRemove:false,
                showCaption: false,
                msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                msgZoomModalHeading: 'ตัวอย่างละเอียด',
                xxx:'lead_image_d',
                minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                dropZoneTitle : 'Banner MB',
                msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
            });
        }

        function removeLead_image(element)
        {
            //$('#gallery_add').prop("disabled", false);
            $(element).parent().remove();
        }

        //add_lead_image p
        function add_lead_image_p_m()
        {

            var index = $('.setIndexProject_galery_galley').length;

            var form = `<div><div class="col-sm-3">
            <div class="form-group setIndexProject_galery">
            <div class="col-md-12">
            <input type="file" id="lead_image_p${index + 1}" class="lead_image" name="lead_image_p_m[]" accept="image/*" />
            </div>
            </div>

            <button  type="button" class="btn btn-round btn-danger" style="margin-left: 112px;" onclick="removeLead_image(this)"> - ลบ Banner MB </button>

            </div>
            </div>`;

            $('#lead_image_project_p_m').append(form);

            $(".lead_image").fileinput({
                uploadUrl: "upload.php", // server upload action
                maxFileCount: 1,
                allowedFileExtensions: ["jpg", "png", "jpeg"],
                browseLabel: ' เลือก Banner MB',
                browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                removeLabel: 'ลบ',
                browseClass: 'btn btn-success',
                showUpload: false,
                showRemove:false,
                showCaption: false,
                msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                msgZoomModalHeading: 'ตัวอย่างละเอียด',
                xxx:'lead_image_d',
                minImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
                minImageHeight: 1920, // ขนาด ศุง ต่ำสุด
                maxImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
                maxImageHeight: 1920, // ขนาด ศุง ต่ำสุด
                dropZoneTitle : 'Banner MB',
                msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
            });
        }

        function removeLead_image(element)
        {
            //$('#gallery_add').prop("disabled", false);
            $(element).parent().remove();
        }


        $("#lead_image_p0").fileinput({
            uploadUrl: "upload.php", // server upload action
            maxFileCount: 1,
            allowedFileExtensions: ["jpg", "png", "jpeg"],
            browseLabel: ' เลือก Banner ',
            browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
            removeLabel: 'ลบ',
            browseClass: 'btn btn-success',
            showUpload: false,
            showRemove:false,
            showCaption: false,
            msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
            msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
            msgZoomModalHeading: 'ตัวอย่างละเอียด',
            xxx:'lead_image_d',
            minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
            minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
            maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
            maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
            dropZoneTitle : 'Banner Lead',
            msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
            msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
            msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
            msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
        });

        $("#lead_image_p_m0").fileinput({
            uploadUrl: "upload.php", // server upload action
            maxFileCount: 1,
            allowedFileExtensions: ["jpg", "png", "jpeg"],
            browseLabel: ' เลือก Banner MB',
            browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
            removeLabel: 'ลบ',
            browseClass: 'btn btn-success',
            showUpload: false,
            showRemove:false,
            showCaption: false,
            msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
            msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
            msgZoomModalHeading: 'ตัวอย่างละเอียด',
            xxx:'lead_image_d',
            minImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
            minImageHeight: 1920, // ขนาด ศุง ต่ำสุด
            maxImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
            maxImageHeight: 1920, // ขนาด ศุง ต่ำสุด
            dropZoneTitle : 'Banner MB',
            msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
            msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
            msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
            msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
        });


        $("button[name='delBanner']").confirm({
            title : 'ลบข้อมูล',
            content: 'คุณแน่ใจที่จะลบข้อมูลนี้ !',
            buttons: {
                Yes: {
                    text: 'Yes',
                    btnClass: 'btn-danger',
                    keys: ['enter', 'a'],
                    action: function(){
                        $.ajax({ url: 'save_lhbanner.php',
                            data: {action: 'delByMainId' , bannerMainId :bannerMainId },
                            type: 'post',
                            success: function(e) {
                                window.location.href =  'lhbanner.php?delData=success';
                            }
                        });
                    }
                },
                No: {
                    text: 'No',
                    btnClass: 'btn-default',
                    keys: ['enter', 'a'],
                    action: function(){
                        // button action.
                    }
                },

            }
        });

        /////////////////////

        $('input[name="bannerchoose"]').change(function() {
            $('input[name="chageBannerChoose"]').val("change");
            var pageTypeIdSelect = $(
                'select[name="PageTypeId"]').val();
            if (this.value == 1 && pageTypeIdSelect == 1) {
                // Home & Banner Image
                resetForm('Banner');
                showBannerLead(true);
            } else {
                if (this.value == 1) {
                    resetForm('Banner');
                    showBannerLead(false);
                } else if (this.value == 2) {
                    resetForm('Activity');
                    showActivityBanner();
                } else if (this.value == 3) {
                    resetForm('Video');
                    showVideoBanner();
                }
            }

        });


        function intBanner() {
            if (typeof (pagestate) != "undefined"
                && pagestate != null && pagestate == 'Add') {
                // initial add
            $('input:radio[name=bannerchoose]')[0].checked = true;
                // init banerLead data
                $('div#bannerMB').hide();
                $('div.upVideo').hide();
                $('div.activityimg').hide();
            } else if (typeof (pagestate) != "undefined"
                && pagestate != null && pagestate == 'Edit') {
                // initial edit
                $('input[name="oldBannerImg"]').val(JSON.stringify(defaultbannerImg));
                $('input[name="oldBannerPC"]').val(JSON.stringify(bannerPC));
                $('input[name="oldBannerMB"]').val(JSON.stringify(bannerMB));
                $('select[name="PageTypeId"]').val(pageTypeId);

                initbannerImg("bannerLead",defaultbannerImg);
                $('input[name="defaultBannerMainId"]').val(defaultLeadImageMainId);
                $('input[name="bannerMainId"]').val(bannerMainId);
                switch (bannerChoose) {
                    case "0":{//leadimage
                        $('input:radio[name="bannerchoose"][value="1"]').prop('checked', true);
                        changeBannerChoose(1);
                        if (pageTypeId == 1) {
                            //case home
                            initbannerImg("bannerPc",bannerPC);
                            initbannerImg("bannerMb",bannerMB);
                        }else{
                            //case other
                            initbannerImg("bannerPc",bannerPC);
                        }
                    }
                    break;
                    case "1":{//activity
                        $('input:radio[name="bannerchoose"][value="2"]').prop('checked', true);
                        changeBannerChoose(2);

                    }
                    break;
                    case "2":{//video
                        $('input:radio[name="bannerchoose"][value="3"]').prop('checked', true);
                        $('input:radio[name="upvido"][value="vdo"]').prop('checked', true);
                        changeBannerChoose(3);
                    }
                    break;
                    case "3"://youtube
                    {
                        $('input:radio[name="bannerchoose"][value="3"]').prop('checked', true);
                        $('input:radio[name="upvido"][value="youtube"]').prop('checked', true);
                        changeBannerChoose(3);
                        $("#url_youtube").val(dataBannerMain.url_vdo);
                    }
                    break;
                }
                initEdit=false;
            }
        }

        /////////////////
        $("#activityimg").fileinput({
            uploadUrl : "save_lhbanner.php", // server
            // upload
            // action
            maxFileCount : 1,
            allowedFileExtensions : [ "jpg", "png","jpeg", "gif" ],
            browseLabel : 'เลือกรูป',
            removeLabel : 'ลบ',
            browseClass : 'btn btn-success',
            showUpload : false,
            showRemove : false,
            showCaption : false,
            autoReplace: false,
            msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
            msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
            msgZoomModalHeading : 'ตัวอย่างละเอียด',
            dropZoneTitle : 'Banner Activity Image',
            xxx:'altthumbActivity',
            maxFileSize : 500 ,
            minImageWidth: 1920, // ขนาด กว้าง
            // ต่ำสุด
            minImageHeight: 1080, // ขนาด ศุง
            // ต่ำสุด
            maxImageWidth: 1920, // ขนาด กว้าง
            // ต่ำสุด
            maxImageHeight: 1080, // ขนาด ศุง
            // ต่ำสุด
            msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
            msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
            msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
            msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
        });

        $("#thumbnailYoutube")
        .fileinput(
        {
                    uploadUrl : "save_lhbanner.php", // server
                    // upload
                    // action
                    maxFileCount : 1,
                    allowedFileExtensions : [ "jpg", "png","jpeg", "gif" ],
                    browseLabel : 'เลือกรูป',
                    removeLabel : 'ลบ',
                    browseClass : 'btn btn-success',
                    showUpload : false,
                    showRemove : false,
                    showCaption : false,
                    autoReplace: false,
                    msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                    msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                    msgZoomModalHeading : 'ตัวอย่างละเอียด',
                    dropZoneTitle : 'รูปภาพ Thumbnail Youtube',
                    xxx:'altYoutube',
                    maxFileSize : 500 ,
                    minImageWidth: 1920, // ขนาด กว้าง
                    // ต่ำสุด
                    minImageHeight: 1080, // ขนาด ศุง
                    // ต่ำสุด
                    maxImageWidth: 1920, // ขนาด กว้าง
                    // ต่ำสุด
                    maxImageHeight: 1080, // ขนาด ศุง
                    // ต่ำสุด
                    msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                    msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                    msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                    msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                });

        $("#vdofile")
        .fileinput(
        {
                    uploadUrl : "save_lhbanner.php", // server
                    // upload
                    // action
                    maxFileCount : 1,
                    allowedFileExtensions : [ "mp4" ],
                    browseLabel : 'เลือกไฟล์',
                    removeLabel : 'ลบ',
                    browseClass : 'btn btn-success',
                    showUpload : false,
                    showRemove : false,
                    showCaption : false,
                    autoReplace: false,
                    maxFileSize : 204800 , // 200 MB
                    msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                    msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                    msgZoomModalHeading : 'ตัวอย่างละเอียด',
                    xxx:'altVideo',
                    dropZoneTitle : 'File VDO',
                    msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                    msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                    msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                    msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                });

        $("#thumbnailVDO")
        .fileinput(
        {
                    uploadUrl : "save_lhbanner.php", // server
                    // upload
                    // action
                    maxFileCount : 1,
                    allowedFileExtensions : [ "jpg", "png","jpeg", "gif" ],
                    browseLabel : 'เลือกรูป',
                    removeLabel : 'ลบ',
                    browseClass : 'btn btn-success',
                    showUpload : false,
                    showRemove : false,
                    showCaption : false,
                    autoReplace: false,
                    msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                    msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                    msgZoomModalHeading : 'ตัวอย่างละเอียด',
                    dropZoneTitle : 'รูปภาพ Thumbnail VDO',
                    xxx:'altthumbVideo',
                    maxFileSize : 500 ,
                    minImageWidth: 1920, // ขนาด กว้าง
                    // ต่ำสุด
                    minImageHeight: 1080, // ขนาด ศุง
                    // ต่ำสุด
                    maxImageWidth: 1920, // ขนาด กว้าง
                    // ต่ำสุด
                    maxImageHeight: 1080, // ขนาด ศุง
                    // ต่ำสุด
                    msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                    msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                    msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                    msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                });


        //////////////////////////////////
        function resetForm(resetType){
            if(resetType=='Banner'){
                $("input[name^=bannerPc]").fileinput('clear');
                $("input[name^=bannerMb]").fileinput('clear');
                $('.setIndexBannerPC').each(function() {
                    $(this).parent().parent().remove();
                });
                $('.setIndexBannerMB').each(function() {
                    $(this).parent().parent().remove();
                });

            }else if(resetType=='Activity'){
                $("#activityimg").fileinput('clear');
                $("#editTh").froalaEditor('html.set', '');
                $("#editEn").froalaEditor('html.set', '');
            }else if(resetType=='Video'){
                $("#url_youtube").val('');
                $("#thumbnailYoutube").fileinput('clear');
                $("#vdofile").fileinput('clear');
                $("#thumbnailVDO").fileinput('clear');
            }else{
                // reset form all type
                // $("#bannerForm").trigger('reset');
                $('.setIndexBannerPC').each(function() {
                    $(this).parent().parent().remove();
                });
                $('.setIndexBannerMB').each(function() {
                    $(this).parent().parent().remove();
                });
                $("input[name^=bannerPc]").fileinput('clear');
                $("input[name^=bannerMb]").fileinput('clear');
                $("#activityimg").fileinput('clear');
                $("#editTh").froalaEditor('html.set', '');
                $("#editEn").froalaEditor('html.set', '');
                $("#url_youtube").val('');
                $("#thumbnailYoutube").fileinput('clear');
                $("#vdofile").fileinput('clear');
                $("#thumbnailVDO").fileinput('clear');
            }
        }

        ////////////////////////////////
        function initbannerImg(name,datas){
            if(name!='' && datas!=''){
                for(var i = 0; i < datas.length; i++) {
                    var data= datas[i];
                    //console.log(data.lead_path);
                    var bname="";
                    var msgtext="";
                    if(i==0){
                        bname=name+"_"+i;
                        switch(name){
                            case "bannerLead" :{ msgtext="เพิ่ม Banner Default" };
                            break;
                            case "bannerPc" : { msgtext="เพิ่ม Banner PC" };
                            break;
                            case "bannerMb" :  { msgtext="เพิ่ม Banner MB" };
                            break;
                        }
                        if(name=='bannerMb') {
                            $("#"+bname).fileinput({
                                uploadUrl: "save_lhbanner.php", // server upload
                                maxFileCount: 1,
                                allowedFileExtensions : [ "jpg", "png","jpeg", "gif" ],
                                browseLabel: msgtext,
                                browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                                removeLabel : 'ลบ',
                                browseClass : 'btn btn-success',
                                showUpload : false,
                                showRemove : false,
                                showCaption : false,
                                autoReplace: false,
                                msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                msgZoomModalHeading : 'ตัวอย่างละเอียด',
                                dropZoneTitle : 'Banner Default',
                                xxx:'altMB',
                                maxFileSize : 500 ,
                                minImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
                                minImageHeight: 1920, // ขนาด ศุง ต่ำสุด
                                maxImageWidth: 1080, // ขนาด กว้าง ต่ำสุด
                                maxImageHeight: 1920, // ขนาด ศุง ต่ำสุด
                                msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                                msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.'
                            });
                        }else{

                            $("#"+bname).fileinput({
                                uploadUrl: "save_lhbanner.php", // server upload
                                maxFileCount: 1,
                                allowedFileExtensions : [ "jpg", "png","jpeg", "gif" ],
                                browseLabel: msgtext,
                                browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                                removeLabel : 'ลบ',
                                browseClass : 'btn btn-success',
                                showUpload : false,
                                showRemove : false,
                                showCaption : false,
                                autoReplace: false,
                                msgInvalidFileExtension : 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                msgFilesTooMany : 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                msgZoomModalHeading : 'ตัวอย่างละเอียด',
                                xxx:'altLead',
                                dropZoneTitle : 'Banner Default',
                                maxFileSize : 500 ,
                                minImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                                minImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                                maxImageWidth: 1920, // ขนาด กว้าง ต่ำสุด
                                maxImageHeight: 1080, // ขนาด ศุง ต่ำสุด
                                msgImageWidthSmall : 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                msgImageHeightSmall : 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                msgImageWidthLarge : 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                                msgImageHeightLarge : 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                            });

                        }

                        $("#"+bname).on("filepredelete", function(jqXHR) {
                            var abort = true;
                            if (confirm("คุณต้องการลบรูปนี้?")) {
                                var index=getBannerIndexById(this.id);
                                abort = false;
                                switch(name){
                                    case "bannerLead" :{ delPicData('banner_lead_id', defaultbannerImg[index].banner_lead_id, defaultbannerImg, this.id );  }
                                    break;
                                    case "bannerPc" : { delPicData('banner_lead_id', bannerPC[index].banner_lead_id, bannerPC, this.id ); }
                                    break;
                                    case "bannerMb" :{ delPicData('banner_lead_id', bannerMB[index].banner_lead_id, bannerMB ,this.id ); }
                                    break;
                                }
                            }
                            return abort; // you can also send any data/object that you can receive on `filecustomerror` event
                        });
                    }else{
                        switch(name){
                            case "bannerLead" : addBannerLead_galery(data);
                            break;
                            case "bannerPc" :   addBannerPC_galery(data);
                            break;
                            case "bannerMb" :   addBannerMB_galery(data);
                            break;
                        }
                    }

                }
            }
        }

        ////////////////////////////////////
        function changeBannerChoose(selected){

            var pageTypeIdSelect = $('select[name="PageTypeId"]').val();
            if (selected == 1 && pageTypeIdSelect == 1) {
                // Home & Banner Image
                showBannerLead(true);
            } else {
                if (selected  == 1) {
                    showBannerLead(false);
                } else if (selected  == 2) {
                    showActivityBanner();
                } else if (selected  == 3) {
                    showVideoBanner();
                }
            }
        }

        ////////////////////////////////////////////////////
        function showBannerLead(isHome) {
            if (isHome) {
                $('div#bannerPC').show();
                $('div#bannerMB').show();
            } else {
                $('div#bannerPC').show();
                $('div#bannerMB').hide();
            }
            $('div.upVideo').hide();
            $('div.activityimg').hide();
        }
        /////////////////////////////////

        function showActivityBanner() {
            $('div#bannerPC').hide();
            $('div#bannerMB').hide();
            $('div.upVideo').hide();
            $('div.activityimg').show();
        }

        function showVideoBanner() {
            $('div#bannerPC').hide();
            $('div#bannerMB').hide();
            $('div.activityimg').hide();
            var showVideoType=$("input[name='upvido']:checked").val();
            if(typeof (showVideoType) == "undefined" || showVideoType==''){
                $('input:radio[name=upvido]')[0].checked = true;
                showVideoType=$("input[name='upvido']:checked").val();
            }
            upVideoShow(showVideoType);
        }

        function upVideoShow(typeshow){
            $('div#upVideo').show();
            if(typeshow=='vdo'){
                $('div#upVdo').show();
                $('div#gthumbnailVDO').show();
                $('div#upYoutube').hide();
                $('div#gthumbnailYoutube').hide();
            }else{
                // youtube
                $('div#upYoutube').show();
                $('div#gthumbnailYoutube').show();
                $('div#upVdo').hide();
                $('div#gthumbnailVDO').hide();
            }
        }

        //////////////////////////////

        $('#startdate').datepicker(
        {
            dateFormat : "dd/mm/yy",
            autoclose : true,
            changeMonth : true,
            showDropdowns : true,
            changeYear : true,
            minDate : new Date(),
            onSelect : function(dateText) {
                var d = new Date($(this).datepicker(
                    "getDate"));
                $("input#enddate").datepicker('option',
                    'minDate', dateText);
                $("input#enddate").prop('disabled', false);
            }
        });

        $('#enddate').datepicker(
        {
            dateFormat : "dd/mm/yy",
            autoclose : true,
            changeMonth : true,
            showDropdowns : true,
            changeYear : true,
            minDate : new Date(),
            onSelect : function(dateText) {
                var d = new Date($(this).datepicker(
                    "getDate"))
                $("input#startdate").datepicker('option',
                    'maxDate', dateText);
            }
        });

        ////////////////////
        $.validator.setDefaults({ ignore: "[contenteditable='true']" });

        $('#editBannerForm').validate({
            rules: {
                PageTypeId : "required",
                startdate : "required",
                enddate : "required",
                'lead_image_p[]': "required",
                'lead_image_p_m[]' : "required",
                activityimg : "required",
                thumbnailYoutube : "required",
                url_youtube : "required",
                vdofile : "required",
                thumbnailVDO : "required",
                'lead_image_d[]' : "required",
                'lead_image_m[]'  : "required",

            },
            messages: {
                PageTypeId : "กรุณาเลือกหน้าจอ",
                startdate : "กรุณาเลือกวันที่เริ่มต้น",
                enddate : "กรุณาเลือกวันที่สิ้นสุด",
                'lead_image_p[]': "กรุณาเลือกรูป",
                'lead_image_p_m[]': "กรุณาเลือกรูป",
                activityimg : "กรุณาเลือกรูป",
                thumbnailYoutube : "กรุณาเลือกรูป",
                url_youtube : "กรุณากรอก URL",
                vdofile : "กรุณาเลือก VDO",
                thumbnailVDO : "กรุณาเลือกรูป",
                'lead_image_d[]' : "กรุณาเลือกรูป",
                'lead_image_m[]'  : "กรุณาเลือกรูป",


            }
        });

       ////////////
      
        $(function() {
            $.FroalaEditor.DefineIcon("imageInfo", { NAME: "info" });
            $.FroalaEditor.RegisterCommand("imageInfo", {
                title: "Info",
                focus: false,
                undo: false,
                refreshAfterCallback: false,
                callback: function() {
                    var $img = this.image.get();
                    alert($img.attr("src"));
                }
            });

            $('#editTh').froalaEditor({
                toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'fontFamily', 'fontSize', '|', 'specialCharacters', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', '-', '|', 'undo', 'redo', 'clearFormatting', 'selectAll', 'html', 'applyFormat', 'removeFormat', 'fullscreen', 'print'],    
                height: 200,
                imageUploadURL: 'uploade_highlights.php',
                imageUploadParams: {
                    id: 'edit'
                },
                imageManagerLoadURL: 'images_load.php',

                fileUploadURL: 'upload_file.php',
                fileUploadParams: {
                    id: 'edit'
                },
                fontFamily: {
                    "LHfont": 'Kittithada',
                    "Roboto,sans-serif": 'Roboto',
                    "Oswald,sans-serif": 'Oswald',
                    "Montserrat,sans-serif": 'Montserrat',
                    "'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
                },

                imageManagerDeleteURL: "delete_image_highlights.php",
                imageManagerDeleteMethod: "POST"
            })
                // Catch image removal from the editor.
                .on('froalaEditor.image.removed', function (e, editor, $img) {
                    $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_image_highlights.php",

                            // Request params.
                            data: {
                                src: $img.attr('src')
                            }
                        })
                    .done (function (data) {
                        console.log ('image was deleted'+$img.attr('src'));
                    })
                    .fail (function (err) {
                        console.log ('image delete problem: ' + JSON.stringify(err));
                    })
                })

                    // Catch image removal from the editor.
                    .on('froalaEditor.file.unlink', function (e, editor, link) {

                        $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_file.php",

                            // Request params.
                            data: {
                                src: link.getAttribute('href')
                            }
                        })
                        .done (function (data) {
                            console.log ('file was deleted');
                        })
                        .fail (function (err) {
                            console.log ('file delete problem: ' + JSON.stringify(err));
                        })
                    });

                //
                $('#editEn').froalaEditor({
                    toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'fontFamily', 'fontSize', '|', 'specialCharacters', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', '-', '|', 'undo', 'redo', 'clearFormatting', 'selectAll', 'html', 'applyFormat', 'removeFormat', 'fullscreen', 'print'],    
                    height: 200,
                    imageUploadURL: 'uploade_highlights.php',
                    imageUploadParams: {
                        id: 'edit'
                    },
                    imageManagerLoadURL: 'images_load.php',

                    fileUploadURL: 'upload_file.php',
                    fileUploadParams: {
                        id: 'edit'
                    },
                    fontFamily: {
                        "LHfont": 'Kittithada',
                        "Roboto,sans-serif": 'Roboto',
                        "Oswald,sans-serif": 'Oswald',
                        "Montserrat,sans-serif": 'Montserrat',
                        "'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
                    },

                    imageManagerDeleteURL: "delete_image_highlights.php",
                    imageManagerDeleteMethod: "POST"
                })
                // Catch image removal from the editor.
                .on('froalaEditor.image.removed', function (e, editor, $img) {
                    $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_image_highlights.php",

                            // Request params.
                            data: {
                                src: $img.attr('src')
                            }
                        })
                    .done (function (data) {
                        console.log ('image was deleted'+$img.attr('src'));
                    })
                    .fail (function (err) {
                        console.log ('image delete problem: ' + JSON.stringify(err));
                    })
                })

                    // Catch image removal from the editor.
                    .on('froalaEditor.file.unlink', function (e, editor, link) {

                        $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_file.php",

                            // Request params.
                            data: {
                                src: link.getAttribute('href')
                            }
                        })
                        .done (function (data) {
                            console.log ('file was deleted');
                        })
                        .fail (function (err) {
                            console.log ('file delete problem: ' + JSON.stringify(err));
                        })
                    });
                });
