<?php
require_once 'include/dbConnect.php';

	try {
		$living_tip_id = $_GET['living_tip_id'];

		$conn = (new dbConnect())->getConn();
		$sql = "UPDATE LH_LIVING_TIPS SET living_tip_img = ''
				WHERE living_tip_id =".$living_tip_id;
        $result= $conn->query($sql);

		echo json_encode($result->fetchAll());

	} catch (\Exception $e) {
		return $e->getMessage();
	}
?>