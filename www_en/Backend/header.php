<?php
/**
 * @author jacky
 * @createdate May 10 2017 
 **/
?>
<?php
if (! isset ( $_SESSION )) {
	session_start ();
}
header ( 'Content-Type:text/html; charset=utf8' );

if (! $_SESSION ['login']) {
	$link = "Location: login.php";
	header ( $link );
}
$title_page = isset ( $_GET ['title_page'] ) ? $_GET ['title_page'] : "LAND & HOUSES";
require_once 'include/dbConnect.php';
require_once 'include/dbCon_mssql.php'; // for insert utf8
require_once './include/function_date.php';
$group_id = $_SESSION ['group_id'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php echo $title_page; ?></title>
<!-- jQuery -->
<!-- Include jQuery lib.
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
-->
<script src="../vendors/jquery/dist/jquery.min.js"></script>

<!-- Custom Theme Scripts -->

<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../vendors/jszip/dist/jszip.min.js"></script>
<script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

<script src="../build/js/custom.min.js"></script>
<!-- Bootstrap -->
<link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
<!-- iCheck -->
<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
<!-- Datatables -->
<link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
 <!-- uploade img -->
<link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
      <!--uploade-->
<!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
<script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
<!-- Custom Theme Style -->
<link href="../build/css/custom.min.css" rel="stylesheet">
<!-- Fancybox image popup -->
<link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
<link rel="stylesheet" type="text/css" href="../build/css/jquery-ui.css" />
<script src="../build/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

<!-- Include Editor style. -->
<link href="editor/css/froala_editor.min.css" rel="stylesheet" type="text/css" />
<link href="editor/css/froala_style.min.css" rel="stylesheet" type="text/css" />
<!-- Include Editor Plugins style. -->
<link rel="stylesheet" href="editor/css/plugins/char_counter.css">
<link rel="stylesheet" href="editor/css/plugins/code_view.css">
<link rel="stylesheet" href="editor/css/plugins/colors.css">
<link rel="stylesheet" href="editor/css/plugins/emoticons.css">
<link rel="stylesheet" href="editor/css/plugins/file.css">
<link rel="stylesheet" href="editor/css/plugins/fullscreen.css">
<link rel="stylesheet" href="editor/css/plugins/image.css">
<link rel="stylesheet" href="editor/css/plugins/image_manager.css">
<link rel="stylesheet" href="editor/css/plugins/line_breaker.css">
<link rel="stylesheet" href="editor/css/plugins/quick_insert.css">
<link rel="stylesheet" href="editor/css/plugins/table.css">
<link rel="stylesheet" href="editor/css/plugins/video.css">

<!--Editor_script-->

<!-- Include JS files. -->
<script type="text/javascript" src="editor/js/froala_editor.min.js"></script>
<!-- Include Code Mirror. -->
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
  <!-- Include Plugins. -->
<script type="text/javascript" src="editor/js/plugins/align.min.js"></script>
<script type="text/javascript" src="editor/js/plugins/char_counter.min.js"></script>
<script type="text/javascript" src="editor/js/plugins/code_beautifier.min.js"></script>
<script type="text/javascript" src="editor/js/plugins/code_view.min.js"></script>
<script type="text/javascript" src="editor/js/plugins/colors.min.js"></script>
<script type="text/javascript" src="editor/js/plugins/emoticons.min.js"></script>
<script type="text/javascript" src="editor/js/plugins/entities.min.js"></script>
<script type="text/javascript" src="editor/js/plugins/file.min.js"></script>
<script type="text/javascript" src="editor/js/plugins/font_family.min.js"></script>
<script type="text/javascript" src="editor/js/plugins/font_size.min.js"></script>
<script type="text/javascript" src="editor/js/plugins/fullscreen.min.js"></script>
<script type="text/javascript" src="editor/js/plugins/image.min.js"></script>
<script type="text/javascript" src="editor/js/plugins/image_manager.min.js"></script>
<script type="text/javascript" src="editor/js/plugins/inline_style.min.js"></script>
<script type="text/javascript" src="editor/js/plugins/line_breaker.min.js"></script>
<script type="text/javascript" src="editor/js/plugins/link.min.js"></script>
<script type="text/javascript" src="editor/js/plugins/lists.min.js"></script>
<script type="text/javascript" src="editor/js/plugins/paragraph_format.min.js"></script>
<script type="text/javascript" src="editor/js/plugins/paragraph_style.min.js"></script>
<script type="text/javascript" src="editor/js/plugins/quick_insert.min.js"></script>
<script type="text/javascript" src="editor/js/plugins/quote.min.js"></script>
<script type="text/javascript" src="editor/js/plugins/table.min.js"></script>
<script type="text/javascript" src="editor/js/plugins/save.min.js"></script>
<script type="text/javascript" src="editor/js/plugins/url.min.js"></script>
<script type="text/javascript" src="editor/js/plugins/video.min.js"></script>

    <script id="fr-fek">try{(function (k){localStorage.FEK=k;t=document.getElementById('fr-fek');t.parentNode.removeChild(t);})('inkH3fiA6B-13a==')}catch(e){}</script>
  <!-- Validate -->
        <script src="../build/js/jquery.validate.js"></script>
 <!-- Fancybox image popup -->
        <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>

        <script type="text/javascript" src="js/jquery.number.js"></script>

        <script type="text/javascript">
            $('.fancybox').fancybox();
        </script>
</head>

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<div class="left_col scroll-view">
					<div class="navbar nav_title" style="border: 0;">
						<a href="project.php" class="site_title"> <img
							style="padding-top: 10px;" src="images/lh.jpg" alt="..."><span></span></a>
					</div>

					<div class="clearfix"></div>

					<!-- menu profile quick info -->
            <?php include './master/navbar.php';?>
            <!-- /menu footer buttons -->
				</div>
			</div>

			<!-- top navigation -->
        <?php include './master/top_nav.php'; ?>
        <!-- /top navigation -->