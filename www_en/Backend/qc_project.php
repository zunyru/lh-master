<?php

include("include/FunctionProject.php");

$projectId = $_GET['project_id'];
$serviceProject = new FunctionProject();
$project = $serviceProject->getProject($projectId);

$products =  $serviceProject->getProductshome();
if (sizeof($products) < 0) {
    // Error Project Not Found
//die ;
}

$zone_id = $project[0]['zone_id'];
$band_id = $project[0]['brand_id'];

$detailzone =  $serviceProject->getAboutZone($projectId);
//$detailzone =  $serviceProject->getZone($zone_id);
$dataseo = $serviceProject->getSEO($projectId);
$subproject = $serviceProject->getSubProject($projectId);
$urlClipProject = $serviceProject->getUrlClipByIdProject($projectId);

$detailband = $serviceProject->getBrandByBrandId($band_id);
$detailConcept = $serviceProject->getProjectConcept($projectId);
$detailContact = $serviceProject->getProjectContact($projectId);
$detailUnit = $serviceProject->getlh_unit();

$detailPrice =  $serviceProject->getProjectPrice($projectId);
$detailNearBy =  $serviceProject->get_project_nearby($projectId);
$mapProducts = $serviceProject->mapCheckedProductOfAllProduct($products, $subproject);

$fixvaluebuild = $serviceProject->fixValueBuild();

$detailSubproject = $serviceProject->getDetailSubProject($subproject);
$progressProjectSummany = $serviceProject->getfindValue($subproject);




//get Maps
$projectMap = $serviceProject->getProjectMaps($projectId);
$InitialPreview = $serviceProject->getImagesProjectInitialPreview($projectId);
$InitialPreviewConfig = $serviceProject->getImagesProjectInitialPreviewConfig($projectId);


if (!function_exists('base_url')) {
    function base_url($atRoot=FALSE, $atCore=FALSE, $parse=FALSE){
        if (isset($_SERVER['HTTP_HOST'])) {
            $http = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
            $hostname = $_SERVER['HTTP_HOST'];
            $dir =  str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

            $core = preg_split('@/@', str_replace($_SERVER['DOCUMENT_ROOT'], '', realpath(dirname(__FILE__))), NULL, PREG_SPLIT_NO_EMPTY);
            $core = $core[0];

            $tmplt = $atRoot ? ($atCore ? "%s://%s/%s/" : "%s://%s/") : ($atCore ? "%s://%s/%s/" : "%s://%s%s");
            $end = $atRoot ? ($atCore ? $core : $hostname) : ($atCore ? $core : $dir);
            $base_url = sprintf( $tmplt, $http, $hostname, $end );
        }
        else $base_url = 'http://localhost/';

        if ($parse) {
            $base_url = parse_url($base_url);
            if (isset($base_url['path'])) if ($base_url['path'] == '/') $base_url['path'] = '';
        }

        return $base_url;
    }
}

?>