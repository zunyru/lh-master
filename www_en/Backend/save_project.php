<?php
session_start();
require_once 'include/dbConnect.php';
require_once 'include/dbCon_mssql.php';


$dataParameter = $_REQUEST;


$GLOBALS['conn'] = (new dbConnect())->getConn() ;
$GLOBALS['db_conn']  = $db_conn ;
$GLOBALS['url_master'] =  $url_master;

if(isset($dataParameter["updatefrom"])) {
    if ($dataParameter["updatefrom"] == "preview") {
        //preview

    } else {
        //update
        // Delete

        // VinitDate All DataParameter

        addSubProject($dataParameter);
        updateProject($dataParameter);
        updateNameGaleryProject($dataParameter);
        updateProjectZone($dataParameter);
        updateProjectNearBy($dataParameter);
        (isset($_REQUEST['project_title_seo_th'])?updateSeoProject($dataParameter):'');
        updateProjectPrice($dataParameter);
        updateProjectConcept($dataParameter);
        updateProjectContact($dataParameter);
        updateProjectMap($dataParameter);
        //updateLeadImagesProject($dataParameter);
        updateGaleryProject($dataParameter);
        updateUrlClipProject($dataParameter);
        updateCommunity($dataParameter);
        //updatePositionGaleryProject($dataParameter);
        (isset($_REQUEST['project_360_url'])?updateURL360Project($dataParameter):'');
        updatePreview($dataParameter);
        add_MasterPlanCondo($dataParameter);
        add_Lead_image($dataParameter);

        if($_SESSION['group_status']=="Admin"){
            add_Lead_Banner($dataParameter);
        }

        $link = "Location: " . "qc_projectview.php?project_id=" . $dataParameter['project_id'] . "&de=y";
        
        header($link); /* Redirect browser */
    }
}else{
    echo "<script type='text/javascript'>";
    echo "window.location = 'project.php'; ";
    echo "</script>";
    exit();
}




function addSubProject($data)
{

    // Keep Project Sub Changr lh_home_sell
    $oldSubProjectIdandProductId = array();
    $newSubProjectIdandProductId = array();

    // Check Add Data Sub Project

    $sizeproduct = isset($data['product_home_id']) ? sizeof($data['product_home_id']) : 0;
    $project_id = $data['project_id'];
    
    //Delete Project Area Hgome
    $resultSubProject =  getProjectsubByProjectId($project_id);
    for ($i = 0; $i < sizeof($resultSubProject); $i++) {
        $sub_project_id = $resultSubProject[$i]['project_sub_id'];
        $product_id  = $resultSubProject[$i]['product_id'];

        $oldSubProjectIdandProductId[] = array("subproject"=> $sub_project_id,"productid"=>$product_id);

        remove_project_area_home($sub_project_id);
        remove_project_area_condo($sub_project_id);
    }

    for ($i=0; $i < count($oldSubProjectIdandProductId); $i++) {
        if ($oldSubProjectIdandProductId[$i]['productid'] == 3) {
            // remove_community_feature($oldSubProjectIdandProductId[$i]['subproject']);
            remove_room_feature($oldSubProjectIdandProductId[$i]['subproject']);
        }
    }

    //Delete Subproject
    remove_project_sub($project_id);


    if ($sizeproduct > 0) {

        // echo "Size " . $sizeproduct;
        for ($i = 0; $i < $sizeproduct; $i++) {
            // add product

            $product_home_id = $data['product_home_id'][$i];
            $homearea_m = $data['homearea_m'][$i];
            $homearea_w = $data['homearea_w'][$i];
            $num_plan_home = $data['num_plan_home'][$i];
            $last_project_sub_id = add_project_sub($project_id, $product_home_id,"");
            add_project_area_home($last_project_sub_id, $homearea_m, $homearea_w, $num_plan_home);

            $newSubProjectIdandProductId[]  = array("subproject"=> $last_project_sub_id,"productid"=>$product_home_id);

        }

    }

    //Add Project Condo & SubProject Condo If Check Parameter
    if (isset($data['productcondo_id'])) {
        $product_condo_id = $data['productcondo_id'];
        //$progressSummany = $data['progressSummany'];
        $project_id = $data['project_id'];

        $area_condofrom = trim($data['area_condofrom']); 
        $arra_condoto   = trim($data['area_condoto']);
        $num_plan_condo = trim($data['num_plan_condo']);
        $building_uni   = trim($data['building_uni']);
        $floor          = trim($data['s_floor']);

        $last_project_sub_id = add_project_sub($project_id, $product_condo_id);

        //updateCommunityFeature($data, $last_project_sub_id);
        updateRoomFeature($data, $last_project_sub_id);

        $dataProjectCondo = array(
            "project_sub_id" => $last_project_sub_id,
            "area_condo" => $area_condofrom . '-' . $arra_condoto,
            "num_plan_condo" => $num_plan_condo,
            "building_unit_id" => $building_uni,
            "floor" => $floor);

        $newSubProjectIdandProductId[]  = array("subproject"=> $last_project_sub_id,"productid"=>$product_condo_id);

        $last_project_area_condo_id = add_project_condo($dataProjectCondo);

        // Detail Condo
        $building_name = is_array($data["building_name"]) ? $data["building_name"] : array();
        $building_num_layer = is_array($data["building_num_layer"]) ? $data["building_num_layer"] : array();
        $building_unit = is_array($data["building_unit"]) ? $data["building_unit"] : array();

        $unit_condo_name = is_array($data["unit_condo_name"]) ? $data["unit_condo_name"] : array();
        $size_unit_codo = is_array($data["size_unit_codo"]) ? $data["size_unit_codo"] : array();

        $sizeDetailCondo = sizeof($building_name);
        $sizeUnit_condo_name = sizeof($unit_condo_name);

        for ($i = 0; $i < $sizeDetailCondo; $i++) {
            $dataDetailProjectCondo = array(
                "project_area_condo_id" => $last_project_area_condo_id,
                "building_num_layer" => $building_num_layer[$i],
                "building_unit" => $building_unit[$i],
                "building_name" => $building_name[$i]
            );

            add_DetailProject_condo($dataDetailProjectCondo);

        }

        for ($j = 0; $j < $sizeUnit_condo_name; $j++) {
            $dataDetailProjectCondoUnit = array(
                "project_area_condo_id" => $last_project_area_condo_id,
                "unit_condo_name" => $unit_condo_name[$j],
                "size_unit_codo" => $size_unit_codo[$j]
            );

            add_DetailProject_Unit_condo($dataDetailProjectCondoUnit,$last_project_sub_id);

        }

        // Add UnitPlan Condo


        $list_unit_plan_id = isset($data['unit_plan_id']) ? $data['unit_plan_id'] : array();


        $list_unit_plan_name_th_condo_update = isset($data['unit_plan_name_th_condo_update']) ? $data['unit_plan_name_th_condo_update'] : array() ;
        $list_unit_plan_name_en_condo_update = isset($data['unit_plan_name_en_condo_update']) ? $data['unit_plan_name_en_condo_update'] : array() ;
        $list_unit_plan_size_condo_update = isset($data['unit_plan_size_condo_update']) ? $data['unit_plan_size_condo_update'] : array() ;
        $list_unit_plan_dis_th_condo_update = isset($data['unit_plan_dis_th_condo_update']) ? $data['unit_plan_dis_th_condo_update'] : array() ;
        $list_unit_plan_dis_en_condo_update = isset($data['unit_plan_dis_en_condo_update']) ? $data['unit_plan_dis_en_condo_update'] : array();
        $list_unit_plan_price_condo_update = isset($data['unit_plan_price_condo_update']) ? $data['unit_plan_price_condo_update'] : array() ;

        $list_unit_plan_name_th_condo = isset($data['unit_plan_name_th_condo']) ? $data['unit_plan_name_th_condo'] : array() ;
        $list_unit_plan_name_en_condo = isset($data['unit_plan_name_en_condo']) ? $data['unit_plan_name_en_condo'] : array();
        $list_unit_plan_size_condo = isset($data['unit_plan_size_condo']) ? $data['unit_plan_size_condo'] : array();
        $list_unit_plan_dis_th_condo = isset($data['unit_plan_dis_th_condo']) ? $data['unit_plan_dis_th_condo'] : array();
        $list_unit_plan_dis_en_condo = isset( $data['unit_plan_dis_en_condo']) ? $data['unit_plan_dis_en_condo'] : array() ;
        $list_unit_plan_price_condo = isset($data['unit_plan_price_condo'] ) ? $data['unit_plan_price_condo'] : array() ;

        $list_unit_plan_img_name  = isset($_FILES['unit_plan_img_name']) ? $_FILES['unit_plan_img_name'] : array();
        $list_unit_plan_img_id  = isset($data['unit_plan_img_id']) ? $data['unit_plan_img_id'] : array();
        $list_unit_plan_img_seo = isset($data['unit_plan_img_name_seo']) ? $data['unit_plan_img_name_seo']  : array();

        // ReMove UnitPlan
        $oldSubProjectId = getSubProjectIdByProductId($oldSubProjectIdandProductId,3);
        if($oldSubProjectId != null) {
            remove_UnitPlanCondo($oldSubProjectId, $list_unit_plan_id);
        }




        // Update UnitPlan
        for($i = 0; $i < sizeof($list_unit_plan_id); $i++)
        {
            $dataUnitPlanCode = array(
                "unit_plan_name_th" => $list_unit_plan_name_th_condo_update[$i],
                "unit_plan_name_en" => $list_unit_plan_name_en_condo_update[$i],
                "project_sub_id" => $last_project_sub_id,
                "project_id" =>$project_id,
                "unit_plan_size" => $list_unit_plan_size_condo_update[$i],
                "unit_plan_dis_th" => $list_unit_plan_dis_th_condo_update[$i],
                "unit_plan_dis_en" => $list_unit_plan_dis_en_condo_update[$i],
                "unit_plan_price" => $list_unit_plan_price_condo_update[$i]
            );

            $condition_unit_plan_id = $list_unit_plan_id[$i];

            updateUnitPlan($dataUnitPlanCode,$condition_unit_plan_id);

            $unit_plan_img_id = $list_unit_plan_img_id[$i];

            $keyTempUnitPlanImgSeo = "unit_plan_img_name_seo_temp".$unit_plan_img_id;
            $keyTempUnitPlanImgPath = "unit_plan_img_name_temp".$unit_plan_img_id;

            $keyUnitPlanImg = "unit_plan_img".$unit_plan_img_id;
            $keyUnitPlanImgSeo = "unit_plan_img_seo".$unit_plan_img_id;


            if($_FILES[$keyUnitPlanImg]['name']!= "")
            {
                $unit_image_seo = $data[$keyUnitPlanImgSeo][0];
                $dataName = date("d-m-Y-H:i:s");
                $pathName = "fileupload/images/unitplan/".$dataName.'-'.$_FILES[$keyUnitPlanImg]['name'];
                if( !move_uploaded_file($_FILES[$keyUnitPlanImg]["tmp_name"], $pathName))
                {
                    // die("Case Update UnitPlan Upload image unit is not success.");
                }

                $dataChange = array( "unit_plan_id"=> $condition_unit_plan_id , "unit_plan_img_name"=> $pathName);

                update_ImageUnitPlanCondo($dataChange, $unit_plan_img_id,$unit_image_seo);
            }
            else
            {
                $dataChange = array( "unit_plan_id"=> $condition_unit_plan_id , "unit_plan_img_name"=> $data[$keyTempUnitPlanImgPath]);
                update_ImageUnitPlanCondo($dataChange, $unit_plan_img_id,$data[$keyTempUnitPlanImgSeo]);
            }


        }
        $index = 0 ;
        // Add UnitPlan
        for ($i = 0; $i < sizeof($list_unit_plan_name_th_condo) ; $i++) {

            $dataUnitPlanCode = array(
                "unit_plan_name_th" => $list_unit_plan_name_th_condo[$index],
                "unit_plan_name_en" => $list_unit_plan_name_en_condo[$index],
                "project_sub_id" => $last_project_sub_id,
                "project_id" => $project_id,
                "unit_plan_size" => $list_unit_plan_size_condo[$index],
                "unit_plan_dis_th" => $list_unit_plan_dis_th_condo[$index],
                "unit_plan_dis_en" => $list_unit_plan_dis_en_condo[$index],
                "unit_plan_price" => $list_unit_plan_price_condo[$index]
            );

            if(!empty($dataUnitPlanCode['unit_plan_name_th'])){
             $last_unit_plan_id = add_UnitPlanCondo($dataUnitPlanCode);
         }
            // Add UnitImagePlan Condo

         if($list_unit_plan_img_name['name'][$index] != "")
         {

            $unit_image_seo = $list_unit_plan_img_seo[$index];
            $dataName = date("dmYHis");
            $pathName = "fileupload/images/unitplan/".$dataName.'-'.$list_unit_plan_img_name['name'][$index];
            if(!move_uploaded_file($list_unit_plan_img_name["tmp_name"][$index], $pathName))
            {
                    //die("Case Add UnitPlan Upload image unit is not success.");
            }
            $dataImageUnitPlanCode = array(

                'unit_plan_img_name' => $pathName,
                'unit_plan_id' => $last_unit_plan_id,
                'unit_plan_img_name_seo' => $unit_image_seo
            );


            add_ImageUnitPlanCondo($dataImageUnitPlanCode);

        }
        else
        {

            $dataImageUnitPlanCode = array(

                'unit_plan_img_name' => '',
                'unit_plan_id' => @$last_unit_plan_id,
                'unit_plan_img_name_seo' => ''

            );

            add_ImageUnitPlanCondo($dataImageUnitPlanCode);
        }

        $index = $index+1;

    }

        // End UnitPlan

        //Start Progress

        /* Step : Remove Progress By OldSubProject with condition not in progress_update_id in table Progress
         *  Step  : Find Count Data Progress
              Update SubProjectId In Table Progress with condition progress_update_id  ref name view
           Step  : Add Data Progress
        */

        // Add Progress
           $list_project_progress_id   = isset($data['progress_update_id']) ? $data['progress_update_id'] : array() ;
           $list_progress_update_mount = isset($data['progress_update_mount']) ? $data['progress_update_mount'] : array();
           $list_progress_update_date  = isset($data['progress_update_date']) ? $data['progress_update_date'] : array();


           $list_progressSection       = isset($data['progressSection']) ? $data['progressSection'] : array();


           $sizelist_progressSection   = sizeof($list_progressSection);

           $sizelist_project_progress_id = sizeof($list_project_progress_id);

           $indexRunningProgress = 0 ;


        //   // echo "<BR>";
        //   // echo "Case Remove Data Progress";
        //  // echo "<BR>";

           $oldSubProjectId = getSubProjectIdByProductId($oldSubProjectIdandProductId,3);
           if($oldSubProjectId !=null ) {
            $list_progress_update_id  = removeProgresssProject($list_project_progress_id, $oldSubProjectId);
            removeImgProgressByProgressUpdateId($list_progress_update_id);
        }


        if($sizelist_project_progress_id > 0)
        {
            // Case Update Data Progress";

            for($i=0 ; $i < $sizelist_project_progress_id ; $i++ )
            {

                $progress_update_id =   $list_project_progress_id[$i];


                $dataProgress = array(
                    "project_sub_id"=> $last_project_sub_id,
                    "progress_update"=> $list_progress_update_date[$i],
                    "progress_update_mount"=> $list_progress_update_mount[$i],
                    "progress_update_id"=> $progress_update_id
                );


                updateProgressProject($dataProgress);


                // Case Up Date Img
                $key_progress_update_img_id =  "progress_update_img_id_".$progress_update_id ;
                $key_progress_update_img   = "progress_img_part_".$progress_update_id ;
                $key_progress_update_dis_th  = "progress_update_dis_th_part_".$progress_update_id;
                $key_progress_update_dis_en = "progress_update_dis_en_part_".$progress_update_id ;


                $sizeProgressDesth = sizeof($data["progress_update_dis_th_part_".$progress_update_id]);
                $size_progress_update_img_id =  sizeof($data[$key_progress_update_img_id]);

                removeImgProgressProject($data["progress_update_img_id_".$progress_update_id],$progress_update_id);

                $indexImageOtherAdd = 0 ;

                for($j=0 ; $j<$size_progress_update_img_id ;$j++ )
                {
                    $indexImageOtherAdd = $indexImageOtherAdd+ 1;

                    $progress_update_img_id = $data[$key_progress_update_img_id][$j];

                    $key_progress_update_img_seo = "progress_img_seo_part_".$progress_update_img_id;

                    $progress_update_img = $_FILES[$key_progress_update_img];

                    $progress_update_img_seo = isset($data[$key_progress_update_img_seo]) ? $data[$key_progress_update_img_seo][0] :  $data["progress_seo_temp".$progress_update_img_id] ;

                    $progress_update_dis_th = trim($data[$key_progress_update_dis_th][$j]);
                    $progress_update_dis_en = trim($data[$key_progress_update_dis_en][$j]);

                    $progressImg_img_temp = trim($data["progress_img_temp".$progress_update_img_id]);
                    $progressImg_seo_temp_= trim($data["progress_seo_temp".$progress_update_img_id]);



                    if($progress_update_img['name'][$j] !="")
                    {
                        // Case Update Image
                        $progress_img_type = $progress_update_img['type'][$j];

                        $dataName = date("dmYHis");
                        $pathName = "fileupload/images/progress_img/" . $dataName . '-' . $progress_update_img['name'][$j];

                        if (!move_uploaded_file($progress_update_img["tmp_name"][$j], $pathName)) {
                            //die("Upload image progress is not success.");
                        }

                        $dataUpdateImgProgress = array(
                            "progress_update_img_id"=> $progress_update_img_id,
                            "progress_update_img_name"=> $pathName ,
                            "progress_update_img_type"=>$progress_img_type  ,
                            "progress_update_dis_th"=> $progress_update_dis_th,
                            "progress_update_dis_en"=> $progress_update_dis_en,
                            "progress_update_img_seo" => $progress_update_img_seo
                        );

                        updateImgProgressProject($dataUpdateImgProgress);

                    }
                    else
                    {
                        // Case Only description

                        $dataUpdateImgProgress = array(
                            "progress_update_img_id"=> $progress_update_img_id,
                            "progress_update_img_name"=> '' ,
                            "progress_update_img_type "=>''  ,
                            "progress_update_dis_th"=> $progress_update_dis_th,
                            "progress_update_dis_en"=> $progress_update_dis_en
                        );

                        updateImgProgressProject($dataUpdateImgProgress);
                    }
                }

                // Case Other  Image Add
                $keyLoopProgressSubImgSec_Add  = "progress_update_dis_en_part_add".$progress_update_id;
                $sizeLoopProgressSubImgSec_Add =   isset($data[$keyLoopProgressSubImgSec_Add]) ? sizeof($data[$keyLoopProgressSubImgSec_Add]) : 0  ;
                for( $j=0 ; $j<$sizeLoopProgressSubImgSec_Add ; $j++)
                {
                    $key_progress_update_img_add   =       "progress_img_part_add".$progress_update_id ;
                    $key_progress_update_img_seo_add   =   "progress_img_seo_part_add".$progress_update_id ;
                    $key_progress_update_dis_th_part_add = "progress_update_dis_th_part_add".$progress_update_id;
                    $key_progress_update_dis_en_part_add = "progress_update_dis_en_part_add".$progress_update_id;


                    $progress_update_img = $_FILES[$key_progress_update_img_add];
                    $progress_update_img_seo = $data[$key_progress_update_img_seo_add][$j];
                    $progress_update_dis_th = $data[$key_progress_update_dis_th_part_add][$j];
                    $progress_update_dis_en = $data[$key_progress_update_dis_en_part_add][$j];



                    if($progress_update_img['name'][$j] !="")
                    {
                        // Case Update Image
                        $progress_img_type = $progress_update_img['type'][$j];

                        $dataName = date("dmYHis");
                        $pathName = "fileupload/images/progress_img/" . $dataName . '-' . $progress_update_img['name'][$j];

                        if (!move_uploaded_file($progress_update_img["tmp_name"][$j], $pathName)) {
                            //die("Upload image progress is not success.");
                        }

                        $dataImgProgress = array(
                            "progress_update_id" => trim($progress_update_id),
                            "progress_update_img_name" => trim($pathName),
                            "progress_update_img_type" => trim($progress_img_type),
                            "progress_update_dis_th" => trim($progress_update_dis_th),
                            "progress_update_dis_en" => trim($progress_update_dis_en),
                            "progress_update_img_seo" => trim($progress_update_img_seo)
                        );

                        addimgeProgress($dataImgProgress);

                    }
                    else
                    {
                        // Case Only description

                        $dataImgProgress = array(
                            "progress_update_id" => $progress_update_id,
                            "progress_update_img_name" => '',
                            "progress_update_img_type" => '',
                            "progress_update_dis_th" => trim($progress_update_dis_th),
                            "progress_update_dis_en" => trim($progress_update_dis_en),
                        );

                        addimgeProgress($dataImgProgress);

                    }


                }


                $indexRunningProgress = $indexRunningProgress +1 ;
            }
            // Case After Insert Other Section


        }
        // echo "<BR>";
        // echo "Case Add Data Progress Next Section";
        // echo "<BR>";

        $list_progress_update_mount_add = isset($data['progress_update_mount_add']) ?   $data['progress_update_mount_add'] : array();
        $list_progress_update_date_add =  isset($data['progress_update_date_add']) ?  $data['progress_update_date_add'] : array();


        $list_progressSection_add =  isset($data['progressSection_add']) ? $data['progressSection_add'] : array();

        $sizelist_progressSection_add = sizeof($list_progressSection_add);


        for($index = 0 ; $index < $sizelist_progressSection_add ; $index++) {

            $dataProgress = array(
                "project_sub_id"=> $last_project_sub_id,
                "progress_update"=> trim($list_progress_update_date_add[$index]),
                "progress_update_mount"=>trim($list_progress_update_mount_add[$index])
            );

            $last_progress_update_id = addDataProgress($dataProgress);

            // echo "<BR>";
            // echo "Case Add Imge Progress";
            // echo "<BR>";


            $valueprogressSection = $list_progressSection_add[$index];
            $sizeSubdtailProgress = sizeof($data["progress_update_dis_th_part_add".$valueprogressSection]);

            $indexfileProgress = "progress_img_part_add".$valueprogressSection;
            $indexNameThProgress = "progress_update_dis_th_part_add".$valueprogressSection;
            $indexNameEgProgress = "progress_update_dis_en_part_add".$valueprogressSection;
            $indexImgProgressSeo = "progress_img_seo_part_add".$valueprogressSection;


            for ($i = 0 ; $i < $sizeSubdtailProgress; $i++)
            {

                $objImgProgress    = $_FILES[$indexfileProgress];
                $nameThImgProgress = trim($data[$indexNameThProgress][$i]);
                $nameEgImgProgress = trim($data[$indexNameEgProgress][$i]);
                $seoImgprogress    = trim($data[$indexImgProgressSeo][$i]);


                if ($objImgProgress['name'][$i] != "") {
                    // echo "Case insert detail have image progress ";

                    $dataName = date("dmYHis");
                    $pathName = "fileupload/images/progress_img/" . $dataName . '-' . $objImgProgress['name'][$i];
                    $typeFileImgProgress = $objImgProgress['type'][$i];

                    if (!move_uploaded_file($objImgProgress["tmp_name"][$i], $pathName)) {
                        // die("Upload image progress is not success.");
                    }

                    $dataImgProgress = array(
                        "progress_update_id" => $last_progress_update_id,
                        "progress_update_img_name" => trim($pathName),
                        "progress_update_img_type" => trim($typeFileImgProgress),
                        "progress_update_dis_th" => trim($nameThImgProgress),
                        "progress_update_dis_en" => trim($nameEgImgProgress),
                        "progress_update_img_seo" => trim($seoImgprogress)
                    );


                    addimgeProgress($dataImgProgress);

                } else {

                    // echo "Case insert detail have not image progress ";

                    $dataImgProgress = array(
                        "progress_update_id" => $last_progress_update_id,
                        "progress_update_img_name" => '',
                        "progress_update_img_type" => '',
                        "progress_update_dis_th" => trim($nameThImgProgress),
                        "progress_update_dis_en" => trim($nameEgImgProgress),
                    );

                    addimgeProgress($dataImgProgress);
                }

            } //End Case Add Imge Progress
        }


        //End Progress

    }
    // End  Add About Condo



    // Start Function Change Home Sale
    changeHomeSale($oldSubProjectIdandProductId,$newSubProjectIdandProductId);
    // End Function Change Home Sale

}


function getProjectsubByProjectId($projectId)
{


    $sql =  " SELECT  project_sub_id ,  project_id , product_id
    FROM lh_project_sub
    WHERE project_id = ".$projectId ;
    try {

        // use exec() because no results are returned
        $detail_project_sub = $GLOBALS['conn']->query($sql);

        return $detail_project_sub->fetchAll();

    } catch (PDOException $e)
    {
        // echo $sql . "<br>" . $e->getMessage();
    }
}


function remove_project_area_home($project_sub_id)
{

    $sql = ' DELETE from lh_project_area_home
    where project_sub_id = '.$project_sub_id;

    try {

        $GLOBALS['conn']->query($sql);


    } catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
    }
}


function remove_project_sub($project_id)
{

    $sql = ' DELETE from lh_project_sub
    where project_id = '.$project_id;

    try {

        $GLOBALS['conn']->query($sql);

    }  catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
    }
}

function add_project_sub($project_id, $product_id)
{
    require_once 'include/dbCon_mssql.php';

    $sql = "";

    //if($product_id !=3) {
    $sql = " INSERT INTO lh_project_sub (project_id,product_id) VALUES (" . "'" . $project_id . "'" . ',' . "'" . $product_id . "'" . ")";
    //}


    try {

        // use exec() because no results are returned
        mssql_query($sql,$GLOBALS['db_conn']);
        $project_sub_id = getMaxId('lh_project_sub','project_sub_id')[0][0];

        return $project_sub_id;

    }  catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
    }
}


function add_project_condo($data)
{

    $sql = " INSERT INTO lh_project_area_condo (project_sub_id,area_condo,num_plan_condo,building_unit_id,floor) VALUES ("."'".trim($data['project_sub_id'])."'".','."'".trim($data['area_condo'])."'".','."'".trim($data['num_plan_condo'])."'".','."'".trim($data['building_unit_id'])."',"."'".trim($data['floor'])."'".")";
    try {

        mssql_query($sql,$GLOBALS['db_conn']);
        $lastId = getMaxId('lh_project_area_condo','project_area_condo_id')[0][0];

        return $lastId;
    }  catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
    }
}


function add_DetailProject_condo($datadetail)
{


    $sql = " INSERT INTO LH_BUILDING_UNIT (project_area_condo_id,building_num_layer,building_unit,building_name) VALUES ("."'".trim($datadetail['project_area_condo_id'])."'".','."'".trim($datadetail['building_num_layer'])."'".','."'".trim($datadetail['building_unit'])."'".','."'".trim($datadetail['building_name'])."'".")";

    try {

        mssql_query($sql,$GLOBALS['db_conn']);


    } catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
    }
}

//add_DetailProject_Unit_condo
function add_DetailProject_Unit_condo($datadetail,$project_sub_id)
{


    $sql = " INSERT INTO LH_UNIT_AREA_CONDO (project_area_condo_id,unit_condo_name,size_unit_codo,project_sub_id) VALUES ("."'".trim($datadetail['project_area_condo_id'])."'".','."'".trim($datadetail['unit_condo_name'])."'".','."'".trim($datadetail['size_unit_codo'])."',"."'".$project_sub_id."'".")";

    try {

        mssql_query($sql,$GLOBALS['db_conn']);
      

    } catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
    }
}




function getMaxId($table, $column)
{
    try {
        $sql = 'select max('.$column.') from '.$table;
        $result = $GLOBALS['conn']->query($sql);
        return $result->fetchAll();
    }
    catch(PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
    }

}


function add_project_area_home($project_sub_id, $area_home, $area_square_w_home, $num_plan_home)
{

    $sql = " INSERT INTO lh_project_area_home (project_sub_id,area_home,area_square_w_home,num_plan_home)
    VALUES (".$project_sub_id.','."'".$area_home."'".','."'".$area_square_w_home."'".","."'".$num_plan_home."'".")";

    try {

        mssql_query($sql,$GLOBALS['db_conn']);

    } catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
    }
}


function remove_project_area_condo($project_sub_id)
{



    $sql =  ' select  project_area_condo_id ,  project_sub_id , area_condo , num_plan_condo ,building_uni
    from lh_projects_area_condo
    where project_sub_id = '.$project_sub_id ;


    try {


        $detailcondo = $GLOBALS['conn']->query($sql);


        for ($i = 0; $i < sizeof($detailcondo); $i++)
        {

            $sql_lh_building =  ' DELETE from lh_building
            where project_area_condo_id = '.$detailcondo[$i]['project_area_condo_id'] ;

            $GLOBALS['conn']->query($sql_lh_building);
        }


        $sql_lh_projects_area_condo =  ' DELETE from lh_projects_area_condo  where project_sub_id = '.$project_sub_id ;
        $GLOBALS['conn']->query($sql_lh_projects_area_condo);


    } catch (PDOException $e)
    {
        // echo $sql . "<br>" . $e->getMessage();
    }
}


function remove_UnitPlanCondo($projectSubId,$list_unit_id)
{



    $sql =  ' DELETE  from lh_unit_plan_condo '.
    " where unit_plan_id not in ( ". implode($list_unit_id, ", ") . ")".
    " and  project_sub_id = ".$projectSubId ;



    // echo "<BR>";
    // echo $sql;
    // echo "<BR>";

    try {
        $GLOBALS['conn']->query($sql);

    }  catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
    }

}

function updateUnitPlan($dataUnitPlanCode,$unit_plan_id)
{


    $sql =  ' UPDATE  lh_unit_plan_condo '.
    ' SET unit_plan_name_th = '."'".$dataUnitPlanCode['unit_plan_name_th']."'".','.
    ' unit_plan_name_en = '."'".$dataUnitPlanCode['unit_plan_name_en']."'".','.
    ' project_sub_id = '."'".$dataUnitPlanCode['project_sub_id']."'".','.
    ' unit_plan_size = '."'".$dataUnitPlanCode['unit_plan_size']."'".','.
    ' unit_plan_dis_th = '."'".$dataUnitPlanCode['unit_plan_dis_th']."'".','.
    ' unit_plan_dis_en = '."'".$dataUnitPlanCode['unit_plan_dis_en']."'".','.
    ' unit_plan_price = '."'".$dataUnitPlanCode['unit_plan_price']."'".
    ' where unit_plan_id = '.$unit_plan_id ;


    // echo "<BR>";
    // echo $sql ;
    // echo "<BR>";


    try {


        mssql_query($sql,$GLOBALS['db_conn']);


    }
    catch (PDOException $e)
    {
        // echo $sql . "<br>" . $e->getMessage();
    }


}
function update_ImageUnitPlanCondo($dataChange,$condition_unit_plan_img_id,$seo)
{
    $sql =  ' UPDATE  LH_UNIT_PLAN_IMG '.
    ' SET unit_plan_id = '.$dataChange['unit_plan_id'].','.
    ' unit_plan_img_name = '."'".$dataChange['unit_plan_img_name']."'".",".
    ' unit_plan_img_name_seo = '."'".$seo."'".
    ' where unit_plan_img_id = '.$condition_unit_plan_img_id ;


    // echo "<BR>";
    // echo $sql;
    // echo "<BR>";



    try {


        mssql_query($sql,$GLOBALS['db_conn']);


    }
    catch (PDOException $e)
    {
        // echo $sql . "<br>" . $e->getMessage();
    }

}



function add_UnitPlanCondo($data)
{
    $data['project_id'];
   echo $sql = " INSERT INTO lh_unit_plan_condo (unit_plan_name_th,unit_plan_name_en,project_sub_id,unit_plan_size,unit_plan_dis_th,unit_plan_dis_en,project_id,unit_plan_price) ".
    " VALUES ("."'".$data['unit_plan_name_th']."'".','."'".$data['unit_plan_name_en']."'".','."'".$data['project_sub_id']."'".','."'".$data['unit_plan_size']."'".','."'".$data['unit_plan_dis_th']."'".','."'".$data['unit_plan_dis_en']."'".','."'".$data['project_id']."'".','."'".$data['unit_plan_price']."'".")";


    try {

        mssql_query($sql,$GLOBALS['db_conn']);
        $lastunit_plan_id =  getMaxId('lh_unit_plan_condo','unit_plan_id')[0][0];

        return $lastunit_plan_id;

    } catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
    }

}


function add_ImageUnitPlanCondo($data)
{




    $sql = " INSERT INTO lh_unit_plan_img (unit_plan_img_name,unit_plan_id,unit_plan_img_name_seo) ".
    " VALUES ("."'".$data['unit_plan_img_name']."'".','."'".$data['unit_plan_id']."'".","."'".$data['unit_plan_img_name_seo']."'".")";



    try {

        mssql_query($sql,$GLOBALS['db_conn']);

    } catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
    }


}


function updateMasterPlan($master_plan_id, $dataMasterPlan)
{

    $sql = "";

    if(array_key_exists("master_plan_img_name",$dataMasterPlan))
    {
        $sql = ' UPDATE  lh_master_plan_condo ' .
        ' SET project_sub_id = ' . $dataMasterPlan['project_sub_id'] . ',' .
        ' master_plan_dis_th = ' . "'" . $dataMasterPlan['master_plan_dis_th'] . "'" . ',' .
        ' master_plan_dis_en = ' . "'" . $dataMasterPlan['master_plan_dis_en'] . "'" . ',' .
        ' master_plan_img_name = ' . "'" . $dataMasterPlan['master_plan_img_name'] . "'" .','.
        ' master_plan_img_seo = ' . "'" . $dataMasterPlan['master_plan_img_seo'] . "'" .
        ' where master_plan_id = ' . $master_plan_id;
    }
    else
    {
        $sql = ' UPDATE  lh_master_plan_condo ' .
        ' SET project_sub_id = ' . $dataMasterPlan['project_sub_id'] . ',' .
        ' master_plan_dis_th = ' . "'" . $dataMasterPlan['master_plan_dis_th'] . "'" . ',' .
        ' master_plan_dis_en = ' . "'" . $dataMasterPlan['master_plan_dis_en'] . "'" .','.
        ' master_plan_img_name = ' . "'" . $dataMasterPlan['master_plan_img_name'] . "'" .','.
        ' master_plan_img_seo = ' . "'" . $dataMasterPlan['master_plan_img_seo'] . "'" .
        ' where master_plan_id = ' . $master_plan_id;
    }

    try {

        mssql_query($sql,$GLOBALS['db_conn']);

    } catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
    }

}




function remove_FloorPlan($master_plan_id,$list_floor_plan_img_id)
{



    $sql =  " DELETE from LH_FLOOR_PLAN_IMG ".
    " where master_plan_id = ".$master_plan_id.
    " and floor_plan_img_id not in ( " . implode($list_floor_plan_img_id, ",") . " )";


    // echo "<BR>";
    // echo $sql;
    // echo "<BR>";

    try {

        $GLOBALS['conn']->query($sql);

    } catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
    }

}


function update_ImageFloorPlanCondo($data,$floorr_plan_img_id)
{



    $sql =  ' UPDATE  lh_floor_plan_img '.
    ' SET master_plan_id = '.$data['master_plan_id'].','.
    ' floor_plan_dis_th = '."'".$data['floor_plan_dis_th']."'".','.
    ' floor_plan_dis_en = '."'".$data['floor_plan_dis_en']."'".','.
    ' floor_plan_img_name = '."'".$data['floor_plan_img_name']."'".",".
    ' floor_plan_img_seo =  '."'".$data['floor_plan_img_seo']."'".
    ' where floor_plan_img_id = '.$floorr_plan_img_id ;



    try {



        mssql_query($sql,$GLOBALS['db_conn']);

    } catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
    }
}


function update_NoImageFloorPlanCondo($data,$floorr_plan_img_id)
{


    $sql =  ' UPDATE  lh_floor_plan_img '.
    ' SET master_plan_id = '."'".$data['master_plan_id']."'".','.
    ' floor_plan_dis_th = '."'".$data['floor_plan_dis_th']."'".','.
    ' floor_plan_dis_en = '."'".$data['floor_plan_dis_en']."'".','.
    ' floor_plan_img_name = '."'".$data['floor_plan_img_name']."'".','.
    ' floor_plan_img_seo = '."'".$data['floor_plan_img_seo']."'".
    ' where floor_plan_img_id = '.$floorr_plan_img_id ;

    try {


        mssql_query($sql,$GLOBALS['db_conn']);

    } catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
    }


}

function  add_ImageFloorPlan($data)
{

    $sql = " INSERT INTO lh_floor_plan_img (master_plan_id,floor_plan_dis_th,floor_plan_dis_en,floor_plan_img_name,floor_plan_img_seo) ".
    " VALUES ("."'".$data['master_plan_id']."'".','."'".$data['floor_plan_dis_th']."'".','."'".$data['floor_plan_dis_en']."'".','."'".$data['floor_plan_img_name']."'".","."'".$data['floor_plan_img_seo']."'".")";


    try {


        mssql_query($sql,$GLOBALS['db_conn']);

    } catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
    }

}

function DateDiff($strDate1,$strDate2)
{
    return (strtotime($strDate1) - strtotime($strDate2))/  ( 60 * 60 * 24 );  
}

// updateProject

function updateProject($data){

    //   "project_data_status" => '', ?
    
    $project_data_status = '';
    if($data['public_date'] == "")
    {
        $start_date ='';
        //$data['public_date'] = date('Y-m-d') ;
    }else{
        $date_start  = $data['public_date'];
        $dates = $date_start;
        $year = substr($dates, 6, 4);
        $day = substr($dates, 0, 2);
        $mont = substr($dates, 3, 2);
        $start_date = $year . "-" . $mont . "-" . $day;

        if(DateDiff($start_date." 00:00",date('Y-m-d')." 00:00") > 0 )
        {
            echo $project_data_status = 'FLM';
        }else{
            echo $project_data_status = 'PB';
        }

    }

    //$sql_count = "SELECT COUNT(project_id) AS counts FROM LH_PROJECTS WHERE project_name_th = '".$data['project_name_th']."' OR project_name_en = '".$data['project_name_en']."' AND NOT project_id = '".$data["project_id"]."'";
    if($_SESSION['group_status']=="Admin"){

        if(!isset($data['project_status'])){
            $data['project_status']='';
        }
        $sql =  ' UPDATE  LH_PROJECTS
        SET brand_id = '.$data['detailband'].',
        zone_id = '.$data['detailZone_id'][0].',
        group_id = '.$data['group_id'].',
        project_name_th = '."'".$data['project_name_th']."'".',
        project_name_en = '."'".$data['project_name_en']."'".',
        company_no_DL200 = '."'".$data['company_no_DL200']."'".',
        project_no_DL200 = '."'".$data['project_no_DL200']."'".',
        area_farm = '."'".$data['area_farm']."'".',
        area_square_m = '."'".$data['area_square_m']."'".',
        area_square_w = '."'".$data['area_square_w']."'".',
        area_land = '."'".$data['area_land']."'".',
        public_date = '."'".$start_date."'".',
        updated_date = '."'".date("Y-m-d H:i:s")."'".',
        latitude = '."'".$data['latitude']."'".',
        longtitude = '."'".$data['longtitude']."'".',
        project_status = '."'".$data['project_status']."'".',
        project_data_status = '."'".$project_data_status."'".',
        project_url = '."'".str_replace(" ","-",$data['project_name_th'])."'".',
        location = '."'".$data['location_th']."'".',
        location_en = '."'".$data['location_en']."'".',
        paking = '."'".$data['paking_th']."'".',
        paking_en = '."'".$data['paking_en']."'".',
        project_url_register = '."'".$data['project_url_register']."'".'
        where project_id = '.$data["project_id"];
    }else{
        $sql =  $sql =  ' UPDATE  LH_PROJECTS
        SET area_farm = '."'".$data['area_farm']."'".',
        area_square_m = '."'".$data['area_square_m']."'".',
        area_square_w = '."'".$data['area_square_w']."'".',
        area_land = '."'".$data['area_land']."'".',
        public_date = '."'".$start_date."'".',
        updated_date = '."'".date("Y-m-d H:i:s")."'".',
        latitude = '."'".$data['latitude']."'".',
        longtitude = '."'".$data['longtitude']."'".',
        location = '."'".$data['location_th']."'".',
        location_en = '."'".$data['location_en']."'".',
        paking = '."'".$data['paking_th']."'".',
        paking_en = '."'".$data['paking_en']."'".',
        company_no_DL200 = '."'".$data['company_no_DL200']."'".',
        project_no_DL200 = '."'".$data['project_no_DL200']."'".',
        project_url_register = '."'".$data['project_url_register']."'".'
        where project_id = '.$data["project_id"];

    }           
// echo $sql."ASDASDASDASD";
    mssql_query($sql,$GLOBALS['db_conn']);

    updatePreview($data);

}

function updatePreview($data){

    if(isset($data['preview_page'])) {
        try {
            $sql_d = "DELETE FROM LH_PROJECT_VIEW WHERE project_id = '" . $data["project_id"] . "'";
            $result_d = mssql_query($sql_d, $GLOBALS['db_conn']);

            $size2 = count($data['preview_page']);
            for ($i = 0; $i < $size2; $i++) {
             $sql = "INSERT INTO LH_PROJECT_VIEW (project_id, product_view_id)
             VALUES ('" . $data["project_id"] . "', '" . $data['preview_page'][$i] . "')";
             $result = mssql_query($sql, $GLOBALS['db_conn']);
         }
     } catch (PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}
}



//updateNameGaleryProject
function updateNameGaleryProject($data) {

    if($_SESSION['group_status']=="Admin") {
        $sql = "UPDATE LH_GALERY_PROJECT
        SET galery_project_name = '".$data['project_name_th']."'";
        $sql .= " WHERE project_id =".$data["project_id"];
        $result = mssql_query($sql, $GLOBALS['db_conn']);
    }  

}

function updateProjectZone($request) {
    try {
        $zone_id = 0;
        $province_id = 0;
        $amphur_id = 0;
        $district_id = 0;
        $zone_id_flm = 0;


        $sql_del_zone ="DELETE FROM LH_PROJECT_ZONE WHERE project_id = '".$request['project_id']."' "; 
        $result_del = mssql_query($sql_del_zone, $GLOBALS['db_conn']);

        $i = 0;
        foreach ($request['detailZone_id'] as $key => $value_z) { 
           $sql1 = "SELECT * FROM LH_ZONES WHERE zone_id = $value_z";
           $result1 = mssql_query($sql1, $GLOBALS['db_conn']);
           $row = mssql_fetch_array($result1);

           if($value_z == ''){
              $value_z = 0;
          }
          if($request['amphur_id'][$i] == ''){
              $request['amphur_id'][$i] = 0;
          }
          if($request['district_id'][$i] == ''){
              $request['district_id'][$i] = 0; 
          }

          $sql = "INSERT INTO LH_PROJECT_ZONE (project_id, zone_id, province_id, amphur_id, district_id,zone_id_flm)
          VALUES ('".$request['project_id']."', '".$value_z."', '".$row['province_id']."', '".$request['amphur_id'][$i]."', '".$request['district_id'][$i]."','".$request['flm'][$i]."')";
          $result = mssql_query($sql, $GLOBALS['db_conn']);
          $i++; 
      }



  }
  catch(PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
  }

}

function  updateProjectNearBy($data)
{
    $projectId = $data["project_id"];
    remove_project_nearby($projectId);
    for($i=0 ;$i<sizeof($data["nearby_name_th"]) ;$i++)
    {
        $dataInsert = array(
            'nearby_name_th'=>mssql_escape($data["nearby_name_th"][$i]),
            'nearby_name_en'=>mssql_escape($data["nearby_name_en"][$i]),
            'nearby_unit'=>$data["nearby_unit"][$i],
            'interval'=>$data["interval"][$i],
            "project_id" => $projectId
        );

        $sql = "INSERT INTO LH_PROJECT_NEARBY (nearby_name_th, nearby_name_en, interval, project_id,nearby_unit)
                VALUES ('".$dataInsert['nearby_name_th']."', '".$dataInsert['nearby_name_en']."',
                        '".$dataInsert['interval']."', ".$dataInsert['project_id'].",'".$dataInsert['nearby_unit']."')";

        try {
            mssql_query($sql,$GLOBALS['db_conn']);
        }  catch (PDOException $e) {
            // echo $sql . "<br>" . $e->getMessage();
        }

    }

}



function remove_project_nearby($project_id) {

    $sql =  "DELETE from lh_project_nearby
    where project_id = ".$project_id;
    try {
        $GLOBALS['conn']->query($sql);
    } catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
    }
}




///updateSeoProject

function updateSeoProject($data)
{


    $sql =  ' UPDATE  LH_PROJECT_SEO '.
    ' SET project_title_seo_th = '."'".mssql_escape($data['project_title_seo_th'])."'".','.
    ' project_title_seo_en = '."'".$data['project_title_seo_en']."'".','.
    ' project_des_seo_th = '."'".mssql_escape($data['project_des_seo_th'])."'".','.
    ' project_des_seo_en = '."'".$data['project_des_seo_en']."'".','.
    ' project_keyword_th = '."'".mssql_escape($data['project_keyword_seo_th'])."'".','.
    ' project_keyword_en  = '."'".$data['project_keyword_seo_en']."'".
    ' where project_id = '. $data["project_id"] ;

    try {
        mssql_query($sql,$GLOBALS['db_conn']);

        //check seo
        for($i=0  ;$i< sizeof($data['url_web']); $i++){

            $sql_lead = "SELECT * FROM LH_LEAD_IMAGE_PROJECT_FILE WHERE PROJECT_ID = '" . $data['project_id'] . "' AND LEAD_IMAGE_PROJECT_FILE_TYPE = 'image' ";
            $query_lead = mssql_query($sql_lead,$GLOBALS['db_conn']);
            $image = mssql_fetch_array($query_lead);
            $data['url_web'][$i];
            $GLOBALS['url_master'];
            $url_ = str_replace($GLOBALS['url_master'],"",$data['url_web'][$i]);
            $url_ = str_replace("%2F","/",urlencode($url_));
            $url_page =  $GLOBALS['url_master'].$url_;
            $url_page2 =  $GLOBALS['url_master'].$data['url_web'][$i];
            $sql_check_seo = "SELECT COUNT(*) AS cont FROM LH_SEO WHERE url_page = '".$url_page."' ";
            $query_seo = mssql_query($sql_check_seo,$GLOBALS['db_conn']);

            $seo_num = mssql_fetch_array($query_seo);
            $seo_num['cont'];
            if($seo_num['cont'] > 0){
                $image_ = str_replace('fileupload/images/project_img/','fileupload/images/project_img/Thumbnails_',$image['LEAD_IMAGE_PROJECT_FILE_NAME']); 

                $sql_update = "UPDATE LH_SEO SET title_seo = '".mssql_escape($data['project_title_seo_th'])."', "
                ." description_seo = '".mssql_escape($data['project_des_seo_th'])."',"
                ." keyword_seo = '".mssql_escape($data['project_keyword_seo_th'])."',"
                ." thumnail_seo = '".$image_."'"
                ." WHERE url_page = '".$url_page."' ";

                mssql_query($sql_update,$GLOBALS['db_conn']);   
            }

            $url_page2 =  $data['url_web'][$i];
            $sql_check_seo2 = "SELECT COUNT(*) AS cont FROM LH_SEO WHERE url_page = '".$url_page2."' ";
            $query_seo2 = mssql_query($sql_check_seo2,$GLOBALS['db_conn']);

            $seo_num2 = mssql_fetch_array($query_seo2);
            $seo_num2['cont'];
            if($seo_num2['cont'] > 0){
                $image_ = str_replace('fileupload/images/project_img/','fileupload/images/project_img/Thumbnails_',$image['LEAD_IMAGE_PROJECT_FILE_NAME']); 

                $sql_update = "UPDATE LH_SEO SET title_seo = '".mssql_escape($data['project_title_seo_th'])."', "
                ." description_seo = '".mssql_escape($data['project_des_seo_th'])."',"
                ." keyword_seo = '".mssql_escape($data['project_keyword_seo_th'])."',"
                ." title_seo_en = '".$data['project_title_seo_en']."', "
                ." description_seo_en = '".$data['project_des_seo_en']."',"
                ." keyword_seo_en = '".$data['project_keyword_seo_en']."',"
                ." thumnail_seo = '".$image_."'"
                ." WHERE url_page = '".$url_page2."' ";

                mssql_query($sql_update,$GLOBALS['db_conn']);   
            }

        }
       
    }catch (PDOException $e){

    }
}


//updateProjectPrice

function updateProjectPrice($data)
{

    if (!isset($data["pire_mode"])) {
        return;
    }


    $projectId = $data["project_id"];

    // Clear Data By ProjectId
    remove_project_price($projectId);

    //  //   "project_data_status" => '', ?

    $modeId = $data["pire_mode"];

    $sizmodeId = sizeof($modeId);
    $indexModeId = 0 ;
    for($i=0  ;$i< $sizmodeId ; $i++)
    {
        if($modeId[$i] != "")
        {
            $indexModeId =   $i;
            break;
        }
    }



    $dataInsert = array(
        "project_id" => $projectId,
        "price_mode" => strval($modeId[$indexModeId])
    );



    if ($modeId[$indexModeId] == "1") {
        $dataInsert = array_merge($dataInsert, ["project_price" => $data["value_pire_mod"][0]]);
    } else if ($modeId[$indexModeId] == "2") {
        $value = $data["value_pire_mod_begine"] . "-" . $data["value_pire_mod_end"];
        $dataInsert = array_merge($dataInsert, ["project_price" => $value]);
    } else if ($modeId[$indexModeId] == "3") {
        $dataInsert = array_merge($dataInsert, ["project_price" => $data["value_pire_mod_condo"][0]]);
    }

    // echo "<BR/>";
    // echo "Data Insert  :". print_r($dataInsert) ."<BR/>";
    // echo "Size Data". sizeof($dataInsert);
    // echo "<BR/>";


    $sql = "";
    $culumn = "";
    $valueCulumn = "";
    $i = 0;
    foreach($dataInsert as $key => $value  )
    {
        if($i>0)
        {
            $culumn = $culumn . ",";
            $valueCulumn = $valueCulumn .",";
        }
        $culumn = $culumn.$key ;
        $valueCulumn  = $valueCulumn."'".$value."'" ;
        $i = $i+1;
    }



    $sql = " INSERT INTO LH_PROJECT_PRICES (".$culumn.") VALUES (".$valueCulumn.") ";


    // echo "<BR/>";
    // echo "SQL".$sql;
    // echo "<BR/>";

    try {

        mssql_query($sql,$GLOBALS['db_conn']);

    } catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
    }


}



function remove_project_price($project_id)
{

    $sql =  " DELETE from LH_PROJECT_PRICES ".
    " where project_id = ".$project_id;

    try {

        $GLOBALS['conn']->query($sql);

    } catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
    }
}


//updateProjectConcept
function mssql_escape($str)
{
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return str_replace("'", "''", $str);
}

function updateProjectConcept($data)
{
    $projectId = $data["project_id"];

    remove_project_concept($projectId);

    $dataInsert = array(
        "concept_th" => trim($data["concept_th"]),
        "concept_en" => trim(mssql_escape($data["concept_en"])),
        "distinctive_th" => trim($data["distinctive_th"]),
        "distinctive_en" => trim($data["distinctive_en"]),
        "facilities_th" => trim($data["facilities_th"]),
        "facilities_en" => trim($data["facilities_en"]),
        "security_th" => trim($data["security_th"]),
        "security_en" => trim($data["security_en"]),
        "information_th" => trim($data["information_th"]),
        "information_en" => trim($data["information_en"]),
        "project_id" => $projectId
    );



    $sql = " INSERT INTO lh_project_concept (concept_th,concept_en,distinctive_th,distinctive_en,facilities_th,facilities_en,security_th,security_en,information_th,information_en,project_id) ".
    " VALUES ("."'".mssql_escape($dataInsert['concept_th'])."'".','.
    "'".mssql_escape($dataInsert['concept_en'])."'".','.
    "'".mssql_escape($dataInsert['distinctive_th'])."'".','.
    "'".mssql_escape($dataInsert['distinctive_en'])."'".','.
    "'".mssql_escape($dataInsert['facilities_th'])."'".','.
    "'".mssql_escape($dataInsert['facilities_en'])."'".','.
    "'".mssql_escape($dataInsert['security_th'])."'".','.
    "'".mssql_escape($dataInsert['security_en'])."'".','.
    "'".mssql_escape($dataInsert['information_th'])."'".','.
    "'".mssql_escape($dataInsert['information_en'])."'".','.
    "'".$dataInsert['project_id']."'".")";

    try {

        mssql_query($sql,$GLOBALS['db_conn']);


    }  catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
    }


}

function remove_project_concept($project_id)
{

    $sql =  " DELETE from lh_project_concept ".
    " where project_id = ".$project_id;

    try {

        $GLOBALS['conn']->query($sql);

    }catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
    }

}

//updateProjectContact


function updateProjectContact($data)
{
    $projectId = $data["project_id"];

    remove_project_contact($projectId);

    //check type open day
    if ($data["open-day"] == 'sum-day') {
        $str = '';
        for ($i=0; $i < count($data["close-day"]); $i++) {
            if ($i > 0) {
                $str .= ',';
            }
            $str .= trim($data["close-day"][$i]);
        }

        $data["open-day"] = $str;
    }

    $mail = explode("@", $data["email"]);

    $dataInsert = array(
        'open_day'=> $data["open-day"],
        'open_time_start'=>$data["open_time_start"],
        'open_time_end'=>$data["open_time_end"],
        'telephone'=>$data["telephone"],
        'email'=>$mail[0],
        'telephone_line'=>$data["telephone_line"],
        'name_staff'=>$data["name_staff"],
        'central_value'=>$data["central_value"],
        "prepay"=>$data["prepay"],
        "unit_id"=>$data["unit_id"],
        "project_id" => $projectId
    );

    if($dataInsert['prepay'] == "")
    {
        $start_date = date("Y-m-d") ;
    }else{
        $date_start  = $dataInsert['prepay'];
        $dates = $date_start;
        $year = substr($dates, 6, 4);
        $day = substr($dates, 0, 2);
        $mont = substr($dates, 3, 2);
        $start_date = $year . "-" . $mont . "-" . $day;
    }




    $sql = " INSERT INTO LH_PROJECT_CONTACTS  (open_day,open_time_start,open_time_end,telephone,email,telephone_line,name_staff,central_value,prepay,unit_id,project_id) ".
    " VALUES ("."'".$dataInsert['open_day']."'".','.
    "'".$dataInsert['open_time_start']."'".','.
    "'".$dataInsert['open_time_end']."'".','.
    "'".$dataInsert['telephone']."'".','.
    "'".$dataInsert['email']."@lh.co.th'".','.
    "'".$dataInsert['telephone_line']."'".','.
    "'".$dataInsert['name_staff']."'".','.
    "'".$dataInsert['central_value']."'".','.
        //"'".$dataInsert['prepay']."'".','.
    "'".$start_date."'".','.
    "'".$dataInsert['unit_id']."'".','.
    "'".$projectId."'".")";


    try
    {
        mssql_query($sql,$GLOBALS['db_conn']);

    }  catch (PDOException $e)
    {
        // echo "<br/>".$sql . "<br/>" . $e->getMessage();
    }


}


function remove_project_contact($project_id)
{

    $sql =  " DELETE from lh_project_contacts ".
    " where project_id = ".$project_id;

    try {
        $GLOBALS['conn']->query($sql);

    }catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
    }
}

//updateProjectMaps
function updateProjectMap($data) {

    $sql = "SELECT COUNT(*) AS NUM
    FROM LH_PROJECT_MAPS
    WHERE project_id =".$data["project_id"];
    $result = $GLOBALS['conn']->query($sql);
    $issetProject = $result->fetchAll();

    if ($issetProject[0]['NUM'] == 0) {
        $sql = "INSERT INTO LH_PROJECT_MAPS (project_id)
        VALUES (".$data["project_id"].")";
        $result = mssql_query($sql, $GLOBALS['db_conn']);
    }

    $dataName = date("d-m-Y-H:i:s");
    if(isset($_FILES["map-image"]["name"])) {
        if ($_FILES["map-image"]["name"] != "" && $_FILES["map-image"]["name"] != null) {

            $pathName = "fileupload/images/maps/image/" . $dataName . '-' . $_FILES["map-image"]["name"];
            if (move_uploaded_file($_FILES["map-image"]["tmp_name"], $pathName)) {
                $sql = "UPDATE LH_PROJECT_MAPS SET map_img = " . "'" . $pathName . "'";
                $sql .= ", map_img_seo ='" . $_POST['map-image-seo'][0] . "'";
                $sql .= " WHERE project_id =" . $data["project_id"];
                $result = mssql_query($sql, $GLOBALS['db_conn']);

                // echo $sql;
            }
        }
    }
    if(isset($_FILES["map-pdf"]["name"])) {
        if ($_FILES["map-pdf"]["name"] != "" && $_FILES["map-pdf"]["name"] != null) {
            $pathName = "fileupload/images/maps/pdf/" . $dataName . '-' . $_FILES["map-pdf"]["name"];
            if (move_uploaded_file($_FILES["map-pdf"]["tmp_name"], $pathName)) {
                $sql = "UPDATE LH_PROJECT_MAPS SET map_pdf = " . "'" . $pathName . "'";
                $sql .= " WHERE project_id =" . $data["project_id"];
                $result = mssql_query($sql, $GLOBALS['db_conn']);
            }
        }
    }
    if(isset($_FILES["map_image_logo"]["name"])) {
        if ($_FILES["map_image_logo"]["name"] != "" && $_FILES["map_image_logo"]["name"] != null) {
            $pathName = "fileupload/images/logo_project/" . $dataName . '-' . $_FILES["map_image_logo"]["name"];
            if (move_uploaded_file($_FILES["map_image_logo"]["tmp_name"], $pathName)) {
                $sql = "UPDATE LH_PROJECT_MAPS SET project_logo = " . "'" . $pathName . "'";
                $sql .= ", project_logo_seo ='" . $_POST['map-image-logo-seo'][0] . "'";
                $sql .= " WHERE project_id =" . $data["project_id"];
                $result = mssql_query($sql, $GLOBALS['db_conn']);
            }
        }
    }
    if(isset($_FILES["map-brochure"]["name"])) {
        if ($_FILES["map-brochure"]["name"] != "" && $_FILES["map-brochure"]["name"] != null) {
            $pathName = "fileupload/images/maps/" . $dataName . '-' . $_FILES["map-brochure"]["name"];
            if (move_uploaded_file($_FILES["map-brochure"]["tmp_name"], $pathName)) {
                $sql = "UPDATE LH_PROJECT_MAPS SET brochure = " . "'" . $pathName . "'";
                $sql .= ", brochure_seo ='" . $_POST['map-image-brochure-seo'][0] . "'";
                $sql .= " WHERE project_id =" . $data["project_id"];
                $result = mssql_query($sql, $GLOBALS['db_conn']);
            }
        }
    }

    // $sql = "INSERT INTO LH_PROJECT_MAPS  (project_id, map_img, map_pdf, project_logo, brochure)
    //         VALUES (".$data["project_id"].", "."'".$data["map-image"]."'".", "."'".$data["map-pdf"]."'".", "."'".$data["map-image-logo"]."'".", "."'".$data["map-image-brochure"]."'".",)";
    // try {
    //     mssql_query($sql,$GLOBALS['db_conn']);
    // }  catch (PDOException $e) {
    //     // echo "<br/>".$sql . "<br/>" . $e->getMessage();
    // }
}


function checkMaxLeadImage($project_id){
    $sql = "SELECT * FROM LH_LEAD_IMAGE_PROJECT_FILE
    WHERE project_id =".$project_id;
    $query = mssql_query($sql, $GLOBALS['db_conn']);

    if ($query) {
        if (mssql_num_rows($query)){
            return mssql_num_rows($query);
        }
    }
    return 0;
}

//updateGaleryProject
function updateGaleryProject($data) {

    $dataName = date("d-m-Y-H:i:s");
    //upadte
   //print_r($_FILES['project_galery_edit']);
    if(isset($_FILES['project_galery_edit']) && count($_FILES['project_galery_edit']['error']) == 1 && $_FILES['project_galery_edit']['error'][0] > 0){
        //file not selected
    } else if(isset($_FILES['project_galery_edit'])){ //this is just to check if isset($_FILE). Not required.
        //file selected


        $numSize = count($_FILES['project_galery_edit']["name"]);
        if($numSize > 0) {
            for ($i = 0; $i < $numSize; $i++) {
            //resize
                if ($_FILES['project_galery_edit']["name"][$i] != '') {
                    $pathName = "fileupload/images/project_galery/" . $dataName . '-' . $_FILES["project_galery_edit"]["name"][$i];
                    if (move_uploaded_file($_FILES["project_galery_edit"]["tmp_name"][$i], $pathName)) {
                        if(isset($data['project_galery_text_en_edit'][$i])){
                           $project_galery_text_en_edit = $data['project_galery_text_en_edit'][$i];
                       }else{
                        $project_galery_text_en_edit = '';
                    }

                    if(isset($data['galery_project_img_id'][$i])){
                       $galery_project_img_id = $data['galery_project_img_id'][$i];
                   }else{
                    $galery_project_img_id = '';
                }

                $data['galery_project_img_id'][$i] ;
                echo $sql_update_gallery = "UPDATE LH_GALERY_IMG_PROJECT SET galery_project_img_seo ='" . $data['alt_gallery'][$i] . "' ,
                galery_project_img_desc = '" . $data['project_galery_text_edit'][$i] . "',
                galery_project_img_name = '".$pathName."',
                galery_project_img_desc_en = '" .$project_galery_text_en_edit. "'
                WHERE galery_project_img_id = '" .$galery_project_img_id. "'";
                $result_gallery_update = mssql_query($sql_update_gallery, $GLOBALS['db_conn']);
            }
        }
    }
}
}

    //add
if(isset($_FILES["project_galery"]["name"])) {

    if ($_FILES["project_galery"]["name"][0] != "" && $_FILES["project_galery"]["name"][0] != null) {

        $sql_get = "SELECT galery_project_id
        FROM LH_GALERY_PROJECT
        WHERE project_id =" . $data['project_id'];
        $query = mssql_query($sql_get, $GLOBALS['db_conn']);

        $result = array();
            //case update
        if (mssql_num_rows($query)) {
            while ($row = mssql_fetch_assoc($query)) {
                $result[] = $row;
            }
            $galeryProjectId = $result[0]['galery_project_id'];
            $numOld = checkMaxFileGalery($galeryProjectId);
            $numSize = count($_FILES['project_galery']["name"]);
            if ($numOld > 0) {
                $numNew = 30 - $numOld;
                if ($numNew > $numSize) {
                    $numNew = $numSize;
                }
            } else {
                $numNew = $numSize;
            }
                // echo '######################### updateGaleryProject ###########################';
                // echo 'in DB ='.$numOld.'| count upload='.$numSize.' | calculate'.$numNew;
            for ($i = 0; $i < $numNew; $i++) {
                $pathName = "fileupload/images/project_galery/" . $dataName . '-' . $_FILES["project_galery"]["name"][$i];
                if (move_uploaded_file($_FILES["project_galery"]["tmp_name"][$i], $pathName)) {

                    echo $sql = "INSERT INTO LH_GALERY_IMG_PROJECT
                    (GALERY_PROJECT_ID, GALERY_PROJECT_IMG_NAME, GALERY_PROJECT_IMG_SEO,galery_project_img_desc,galery_project_img_desc_en)
                    VALUES (" . $galeryProjectId . ", '" . $pathName . "', '" . $_POST['upload-project-galery-seo'][$i] . "','" . $_POST['project_galery_text'][$i] . "','" . $_POST['project_galery_text_en'][$i] . "')";
                    $result = mssql_query($sql, $GLOBALS['db_conn']);
                }
            }
        } else {
            echo $sql = "INSERT INTO LH_GALERY_PROJECT (project_id, galery_project_name)
            VALUES (" . $data['project_id'] . ", '" . $data['project_name_th'] . "')";
            $result = mssql_query($sql, $GLOBALS['db_conn']);
            $maxId = getMaxId('LH_GALERY_PROJECT', 'galery_project_id');

            $numSize = count($_FILES['project_galery']["name"]);
            for ($i = 0; $i < $numSize; $i++) {
                $pathName = "fileupload/images/project_galery/" . $dataName . '-' . $_FILES["project_galery"]["name"][$i];
                if (move_uploaded_file($_FILES["project_galery"]["tmp_name"][$i], $pathName)) {

                    $sql = "INSERT INTO LH_GALERY_IMG_PROJECT
                    (GALERY_PROJECT_ID, GALERY_PROJECT_IMG_NAME, GALERY_PROJECT_IMG_SEO,galery_project_img_desc,galery_project_img_desc_en)
                    VALUES (" . $maxId[0][0] . ", '" . $pathName . "', '" . $_POST['upload-project-galery-seo'][$i] . "','" . $_POST['project_galery_text'][$i] . "','" . $_POST['project_galery_text_en'][$i] . "')";
                    $result = mssql_query($sql, $GLOBALS['db_conn']);
                }
            }
        }
    } else {
            // แก้ไขรายละเอียดโครงการ
        $sql_gallery = "SELECT * FROM LH_GALERY_PROJECT p LEFT JOIN LH_GALERY_IMG_PROJECT img ON p.galery_project_id = img.galery_project_id WHERE p.project_id = '" . $data['project_id'] . "'";
        $result_gallery = mssql_query($sql_gallery, $GLOBALS['db_conn']);
            //echo $data['gallery_id'];
        $i = 0;
        while ($row_gallery = mssql_fetch_array($result_gallery)) {
            if ($data['gallery_id'][$i] == $row_gallery['galery_project_img_id']) {
                $sql_update_gallery = "UPDATE LH_GALERY_IMG_PROJECT SET galery_project_img_seo ='" . $data['alt_gallery'][$i] . "' ,
                galery_project_img_desc = '" . $data['project_galery_text_edit'][$i] . "',
                galery_project_img_desc_en = '" . $data['project_galery_text_en_edit'][$i] . "'
                WHERE galery_project_img_id = '" . $data['gallery_id'][$i] . "'";
                $result_gallery_update = mssql_query($sql_update_gallery, $GLOBALS['db_conn']);
                    //echo $data["project_galery_text_edit"][$i];
            }
            $i++;
        }
    }
}

else{

        // แก้ไขรายละเอียดโครงการ
    $sql_gallery = "SELECT * FROM LH_GALERY_PROJECT p LEFT JOIN LH_GALERY_IMG_PROJECT img ON p.galery_project_id = img.galery_project_id WHERE p.project_id = '" . $data['project_id'] . "'";
    $result_gallery = mssql_query($sql_gallery, $GLOBALS['db_conn']);
        //echo $data['gallery_id'];
    $i = 0;
    while ($row_gallery = mssql_fetch_array($result_gallery)) {
        if ($data['gallery_id'][$i] == $row_gallery['galery_project_img_id']) {
            $sql_update_gallery = "UPDATE LH_GALERY_IMG_PROJECT SET galery_project_img_seo ='" . $data['alt_gallery'][$i] . "' ,
            galery_project_img_desc = '" . $data['project_galery_text_edit'][$i] . "',
            galery_project_img_desc_en = '" . $data['project_galery_text_en_edit'][$i] . "'
            WHERE galery_project_img_id = '" . $data['gallery_id'][$i] . "'";
            $result_gallery_update = mssql_query($sql_update_gallery, $GLOBALS['db_conn']);
                //echo $data["project_galery_text_edit"][$i];
        }
        $i++;
    }

}
}

function checkMaxFileGalery($galery_project_id){
    $sql = "SELECT * FROM LH_GALERY_IMG_PROJECT
    WHERE galery_project_id =".$galery_project_id;
    $query = mssql_query($sql, $GLOBALS['db_conn']);

    if ($query) {
        if (mssql_num_rows($query)){
            return mssql_num_rows($query);
        }
    }
    return 0;
}

//updateUrlClipProject
function updateUrlClipProject($data) {
    $dataName = date("dmYHis");

    if (isset($data['optradio_vdo'])){

        if($data['optradio_vdo']=="youtube") {
            if (trim($data['url_youtube']) != "") {

               $sql= "SELECT * FROM LH_PROJECT_CLIPS c WHERE c.project_id = '".$data["project_id"]."' and c.tvc_type = 'youtube'";
               $query = mssql_query($sql);
               $num=mssql_num_rows($query);
               if($num == 0) {
                $dataName = date("dmYHis");
                if ($_FILES["img_youtube"]["name"] != "" && $_FILES["img_youtube"]["name"] != null) {
                    $pathName = "fileupload/images/tvc/" . $dataName . '-' . $_FILES["img_youtube"]["name"];
                    if (move_uploaded_file($_FILES["img_youtube"]["tmp_name"], $pathName)) {

                        $sql_d="DELETE FROM LH_PROJECT_CLIPS WHERE PROJECT_ID = '".$data["project_id"]."'";
                        $query_d = mssql_query($sql_d);

                        echo $sql_c = "INSERT INTO LH_PROJECT_CLIPS
                        (PROJECT_ID, CLIP_PROJECT_URL,thumnail,tvc_type)
                        VALUES ('" . $data["project_id"] . "', '" . $data['url_youtube'] . "','" . $pathName . "' ,'" . $data['optradio_vdo'] . "')";
                        $query = mssql_query($sql_c, $GLOBALS['db_conn']);
                    }
                }
            }else{

                $dataName = date("dmYHis");
                $sql_c = "UPDATE LH_PROJECT_CLIPS SET CLIP_PROJECT_URL = '". $data['url_youtube']. "', tvc_type = 'youtube' WHERE PROJECT_ID = '" . $data["project_id"] . "' ";
                $query = mssql_query($sql_c, $GLOBALS['db_conn']);
                if(isset($_FILES["img_youtube"]["name"])){
                    if ($_FILES["img_youtube"]["name"] != "" && $_FILES["img_youtube"]["name"] != null) {
                        $pathName = "fileupload/images/tvc/" . $dataName . '-' . $_FILES["img_youtube"]["name"];
                        if (move_uploaded_file($_FILES["img_youtube"]["tmp_name"], $pathName)) {
                            $sql_c = "UPDATE LH_PROJECT_CLIPS SET CLIP_PROJECT_URL = '". $data['url_youtube']. "',thumnail = '".$pathName."',tvc_type = 'youtube' 
                            WHERE PROJECT_ID = '" . $data["project_id"] . "'";
                            $query = mssql_query($sql_c, $GLOBALS['db_conn']);
                        }
                    }
                }

            }
        }else{
            $sql_d="DELETE FROM LH_PROJECT_CLIPS WHERE PROJECT_ID = '".$data["project_id"]."'";
            $query_d = mssql_query($sql_d);

        }
    }else if($data['optradio_vdo']=="vdo"){
        if(isset($_FILES["thumbnail_vdo"]["name"])) {
            if ($_FILES["thumbnail_vdo"]["name"] != "" && $_FILES["thumbnail_vdo"]["name"] != null) {
                $sql = "SELECT * FROM LH_PROJECT_CLIPS c WHERE c.project_id = '" . $data["project_id"] . "' and c.tvc_type = 'vdo'";
                $query = mssql_query($sql);
                $num = mssql_num_rows($query);
                if ($num == 0) {
                    $dataName = date("dmYHis");
                    $dataName_vdo = date("dmYHis") . "_vdo";
                    if ($_FILES["thumbnail_vdo"]["name"] != "" && $_FILES["thumbnail_vdo"]["name"] != null) {
                        if ($_FILES["vdo"]["name"] != "" && $_FILES["vdo"]["name"] != null) {

                            $pathName = "fileupload/images/tvc/" . $dataName . '-' . $_FILES["thumbnail_vdo"]["name"];
                            $pathNamevdo = "fileupload/images/tvc/" . $dataName_vdo . '-' . $_FILES["vdo"]["name"];
                            if (move_uploaded_file($_FILES["thumbnail_vdo"]["tmp_name"], $pathName)) {
                                if (move_uploaded_file($_FILES["vdo"]["tmp_name"], $pathNamevdo)) {
                                    $sql_del = "DELETE FROM LH_PROJECT_CLIPS "
                                    . "WHERE project_id = " . $data["project_id"];
                                    mssql_query($sql_del, $GLOBALS['db_conn']);

                                    $sql_c = "INSERT INTO LH_PROJECT_CLIPS
                                    (PROJECT_ID, CLIP_PROJECT_URL,thumnail,tvc_type)
                                    VALUES ('" . $data["project_id"] . "', '" . $pathNamevdo . "','" . $pathName . "' ,'" . $data['optradio_vdo'] . "')";
                                    $query = mssql_query($sql_c, $GLOBALS['db_conn']);
                                }
                            }
                        }
                    }
                } else {
                    $sql = "SELECT * FROM LH_PROJECT_CLIPS c WHERE c.project_id = '" . $data["project_id"] . "' AND c.tvc_type = 'vdo'";
                    $query = mssql_query($sql);
                    $row_s = mssql_fetch_array($query);
                    $thumnail = $row_s['thumnail'];
                    $vdo = $row_s['CLIP_PROJECT_URL'];
                    unlink($thumnail);
                    unlink($vdo);

                    $dataName = date("dmYHis");
                    $dataName_vdo = date("dmYHis") . "_vdo";

                    if ($_FILES["thumbnail_vdo"]["name"] != "" && $_FILES["thumbnail_vdo"]["name"] != null) {
                        if ($_FILES["vdo"]["name"] != "" && $_FILES["vdo"]["name"] != null) {

                            $pathName = "fileupload/images/tvc/" . $dataName . '-' . $_FILES["thumbnail_vdo"]["name"];
                            $pathNamevdo = "fileupload/images/tvc/" . $dataName_vdo . '-' . $_FILES["vdo"]["name"];
                            if (move_uploaded_file($_FILES["thumbnail_vdo"]["tmp_name"], $pathName)) {
                                if (move_uploaded_file($_FILES["vdo"]["tmp_name"], $pathNamevdo)) {
                                    $sql_c = "UPDATE LH_PROJECT_CLIPS SET CLIP_PROJECT_URL = '" . $pathNamevdo . "',thumnail = '" . $pathName . "',tvc_type = 'vdo'
                                    WHERE PROJECT_ID = '" . $data["project_id"] . "'";
                                    $query = mssql_query($sql_c, $GLOBALS['db_conn']);
                                }
                            }
                        }
                    }else{
                        if ($_FILES["vdo"]["name"] != "" && $_FILES["vdo"]["name"] != null) {

                            $pathNamevdo = "fileupload/images/tvc/" . $dataName_vdo . '-' . $_FILES["vdo"]["name"];
                            if (move_uploaded_file($_FILES["thumbnail_vdo"]["tmp_name"], $pathNamevdo)) {
                                if (move_uploaded_file($_FILES["vdo"]["tmp_name"], $pathNamevdo)) {
                                    $sql_c = "UPDATE LH_PROJECT_CLIPS SET CLIP_PROJECT_URL = '" . $pathNamevdo . "',tvc_type = 'vdo'
                                    WHERE PROJECT_ID = '" . $data["project_id"] . "'";
                                    $query = mssql_query($sql_c, $GLOBALS['db_conn']);
                                }
                            }
                        }

                    }

                }
            }
        }else{
                //vdo only
            if(isset($_FILES["vdo"]["name"])) {
                if ($_FILES["vdo"]["name"] != "" && $_FILES["vdo"]["name"] != null) {
                    $dataName_vdo = date("dmYHis") . "_vdo";
                    $pathNamevdo = "fileupload/images/tvc/" . $dataName_vdo . '-' . $_FILES["vdo"]["name"];

                    if (move_uploaded_file($_FILES["vdo"]["tmp_name"], $pathNamevdo)) {
                        $sql_c = "UPDATE LH_PROJECT_CLIPS SET CLIP_PROJECT_URL = '" . $pathNamevdo . "',tvc_type = 'vdo'
                        WHERE PROJECT_ID = '" . $data["project_id"] . "'";
                        $query = mssql_query($sql_c, $GLOBALS['db_conn']);
                    }
                }
            }
        }
    }

}
}

//updatePositionGaleryProject
function updatePositionGaleryProject($data) {

    $dataName = date("dmYHis");
    if ($_FILES["project_position_img"]["name"][0] != "" && $_FILES["project_position_img"]["name"][0] != null){

        $numSize = count($_FILES['project_position_img']["name"]);
        for ($i=0; $i < $numSize; $i++) {
            $pathName = "fileupload/images/project_position_img/".$dataName.'-'.$_FILES["project_position_img"]["name"][$i];
            if(move_uploaded_file($_FILES["project_position_img"]["tmp_name"][$i], $pathName)) {

                $sql = "INSERT INTO LH_GALERY_PROJECT
                (PROJECT_ID, GALERY_PROJECT_NAME)
                VALUES (".$data["project_id"].", '".$pathName."' )";
                $result = mssql_query($sql, $GLOBALS['db_conn']);
            }
        }
    }
}
//updateURL360Project
function updateURL360Project($data) {

    if (isset($data["project_360_url"]) && $data["project_360_url"] != "") {
        $sql="UPDATE LH_360_PROJECT SET C360_PROJECT_URL = '".$data["project_360_url"]."' WHERE PROJECT_ID = '".$data["project_id"]."'";
        mssql_query($sql, $GLOBALS['db_conn']);
        $i = 0 ;
        $dataName =date('dmYHis');
        if(isset($_FILES["c_360_img"]["name"])) {
            if ($_FILES["c_360_img"]["name"] != "" && $_FILES["c_360_img"]["name"] != null) {

                $numrand = mt_rand();
                $type_img = strrchr($_FILES["c_360_img"]["name"],".");
                $images = $_FILES["360_thumnail"]["tmp_name"];
                $images_name = $_FILES["360_thumnail"]["name"];
                $new_images = "Thumbnails_"."360_".$dataName.$numrand.$type_img;
                        //copy($_FILES["images_plan"]["tmp_name"][0],"fileupload/images/galery_plan/".$_FILES["images_plan"]["name"][0]);
                        $width=600; //*** Fix Width & Heigh (Autu caculate) ***//
                        $size=GetimageSize($images);
                        $height=round($width*$size[1]/$size[0]);
                        $images_orig = ImageCreateFromJPEG($images);
                        $photoX = ImagesX($images_orig);
                        $photoY = ImagesY($images_orig);
                        $images_fin = ImageCreateTrueColor($width, $height);
                        ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
                        ImageJPEG($images_fin,"fileupload/images/360_thumnail/".$new_images,93);
                        ImageDestroy($images_orig);
                        ImageDestroy($images_fin); 

                        $pathName = "fileupload/images/360_thumnail/" ."360_".$dataName.$numrand.$type_img;
                        if (move_uploaded_file($_FILES["c_360_img"]["tmp_name"], $pathName)) {
                            $sql_del = "DELETE FROM LH_360_PROJECT
                            WHERE PROJECT_ID = " . $data["project_id"];
                            mssql_query($sql_del, $GLOBALS['db_conn']);

                            $sql = "INSERT INTO LH_360_PROJECT
                            (PROJECT_ID, C360_PROJECT_URL,thumnail)
                            VALUES (" . $data["project_id"] . ", '" . $data["project_360_url"] . "' ,'$pathName')";
                            $result = mssql_query($sql, $GLOBALS['db_conn']);
                        }
                    }
                }
            }else{
               $sql_del = "DELETE FROM LH_360_PROJECT
               WHERE PROJECT_ID = " . $data["project_id"];
               mssql_query($sql_del, $GLOBALS['db_conn']);    
           }

       }



       function findHomeSale($projectSubId){

        $sub_projectid = array();

        for($i = 0 ; $i < sizeof($projectSubId) ; $i++)
        {
            $sub_projectid[] = $projectSubId[$i]["project_sub_id"] ;

        }


        $sql = " SELECT LH_HOME_SELL.home_sell_id , LH_PRODUCTS.product_id
        from LH_PROJECT_SUB
        INNER JOIN LH_HOME_SELL ON LH_PROJECT_SUB.project_sub_id = LH_HOME_SELL.project_sub_id
        INNER JOIN LH_PRODUCTS ON LH_PRODUCTS.product_id = LH_PROJECT_SUB.product_id ".
        " WHERE  LH_HOME_SELL.project_sub_id = ( '" . implode($sub_projectid, "', '") . "' )  ";

        try {
            $GLOBALS['conn']->query($sql);

        }catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
        }

    }



    function changeHomeSale($oldSubProjectIdandProductId,$newSubProjectIdandProductId)
    {
        $arrayDeleteSubProject = array();
        $arrayUpdateSubProject = array();


        $sizeoldSubProjectIdandProductId = sizeof($oldSubProjectIdandProductId);
        $sizenewSubProjectIdandProductId = sizeof($newSubProjectIdandProductId);


    // echo "<BR>";
    // echo "changeHomeSale";
    // echo "<BR>";
    // echo "Size Old : ".$sizeoldSubProjectIdandProductId ;
    // echo "<BR>";
    //print_r($oldSubProjectIdandProductId);
    // echo "<BR>";

    // echo "Size New : ".$sizenewSubProjectIdandProductId ;
    // echo "<BR>";
    //print_r($newSubProjectIdandProductId);

    // echo "<BR>";



    //Case Update
        for($i=0 ;$i <$sizeoldSubProjectIdandProductId;$i++)
        {
            $oldsubprojectId =  $oldSubProjectIdandProductId[$i]['subproject'];
            $oldproductId = $oldSubProjectIdandProductId[$i]['productid'];
        // echo "<BR>";
        // echo "Old SubprojectId : ".$oldsubprojectId;


            $obj =  getnewProductIdAndSubprojectByProductId($newSubProjectIdandProductId,$oldproductId);
        // echo "<BR>";
        // echo print_r($obj);
        // echo "<BR>";
            if($obj !=null)
            {
            // Update
                $newsubprojectId = $obj['subproject'] ;

                $objHomeSale =   getHomeSaleBySubProjectId($oldsubprojectId);
                if($objHomeSale !=null)
                {
                    $sizeObjHomeSale = sizeof($objHomeSale);
                    for($indexobjHomeSale = 0 ; $indexobjHomeSale < $sizeObjHomeSale ; $indexobjHomeSale++)
                    {
                        $home_sell_id  =  $objHomeSale[$indexobjHomeSale]['home_sell_id'];
                        $plan_id = $objHomeSale[$indexobjHomeSale]['plan_id'];

                        updateHomeSale($home_sell_id,$plan_id,$newsubprojectId);
                    }
                }

            }else
            {
            // Remove

            }

        }

    //Case Add
        for($i = 0 ; $i <$sizenewSubProjectIdandProductId ;$i++ )
        {
            $newsubproject = $newSubProjectIdandProductId[$i]['subproject'];
            $newproductId = $newSubProjectIdandProductId[$i]['productid'];

            $obj  = getnewProductIdAndSubprojectByProductId($oldSubProjectIdandProductId,$newproductId);

            if($obj == null)
            {
            // Case Add
            }


        }



    }


    function getnewProductIdAndSubprojectByProductId($list, $conditionSubprojectId)
    {
        $size = sizeof($list);
        for($i=0 ;$i< $size ; $i++)
        {
            $productid =   $list[$i]['productid'];
            if($conditionSubprojectId == $productid )
            {
                return $list[$i];
            }

        }
        return null ;
    }

    function updateHomeSale($home_sell_id,$plan_id,$newsubprojectId)
    {
        $sql =  ' UPDATE  LH_HOME_SELL '.
        ' SET project_sub_id = '.$newsubprojectId.
        ' where home_sell_id = '.$home_sell_id.
        ' and plan_id = '.$plan_id;

    // echo "<BR>";
    // echo $sql ;
    // echo "<BR>";

        try {

            mssql_query($sql,$GLOBALS['db_conn']);

        }  catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
        }

    }

    function getHomeSaleBySubProjectId($subprojectId)
    {

        $sql = " select home_sell_id,plan_id
        from LH_HOME_SELL
        where project_sub_id = ".$subprojectId;

    // echo "<BR>";
    // echo $sql ;
    // echo "<BR>";

        try
        {
            $result = $GLOBALS['conn']->query($sql);
            if($result !=null)
            {
                return   $result->fetchAll();
            }

            return null ;
        }
        catch (PDOException $e)
        {
        // echo $sql . "<br>" . $e->getMessage();
        }

    }


    function addDataProgress($data)
    {
        $date = $data['progress_update'];
    //30/03/2017
    //2017-03-30
        $day=substr($date, 0, 2);
        $mount = substr($date, 3, 2);
        $year = substr($date, 6, 4);
        $progress_update_new = $year."-".$mount."-".$day;
        if( $data['progress_update'] == null && $data['progress_update_mount'] == null )
        {
            $sql = " INSERT INTO LH_PROGRESS_UPDATE_CONDO (project_sub_id) ".
            " VALUES (".$data['project_sub_id'].")";

        }
        else
            if($data['progress_update'] == null)
            {


                $sql = " INSERT INTO LH_PROGRESS_UPDATE_CONDO (project_sub_id,progress_update_mount) ".
                " VALUES (".$data['project_sub_id'].','."'".$data['progress_update_mount']."'".")";

            }
            else if($data['progress_update_mount'] == null)
            {

                $sql = " INSERT INTO LH_PROGRESS_UPDATE_CONDO (project_sub_id,progress_update) ".
                " VALUES (".$data['project_sub_id'].','."'".$progress_update_new."'".")";

            }

            else
            {

                $sql = " INSERT INTO LH_PROGRESS_UPDATE_CONDO (project_sub_id,progress_update,progress_update_mount) ".
                " VALUES (".$data['project_sub_id'].','."'".$progress_update_new ."'".','."'".$data['progress_update_mount']."'".")";

            }

    // echo "<BR>";
    // echo $sql;
    // echo "</BR>";


            try {

                mssql_query($sql,$GLOBALS['db_conn']);
                $lastId_master_plan = getMaxId('LH_PROGRESS_UPDATE_CONDO','progress_update_id')[0][0];

                return $lastId_master_plan;

            }  catch (PDOException $e) {
                echo $sql . "<br>" . $e->getMessage();
            }
        }

        function addimgeProgress($data)
        {

            $sql = "";

            if(array_key_exists('progress_update_img_seo',$data))
            {
        // Have Img
                $sql = " INSERT INTO LH_PROGRESS_UPDATE_IMG (progress_update_img_name,progress_update_id,progress_update_dis_th,progress_update_dis_en,progress_update_img_seo) " .
                " VALUES (" . "'" . $data['progress_update_img_name'] . "'" . ',' . $data['progress_update_id'] . ',' . "'" . $data['progress_update_dis_th'] . "'" . ',' . "'" . $data['progress_update_dis_en'] . "'".','."'".$data['progress_update_img_seo']."'". ")";

            }
            else
            {
                $sql = " INSERT INTO LH_PROGRESS_UPDATE_IMG (progress_update_img_name,progress_update_id,progress_update_dis_th,progress_update_dis_en) " .
                " VALUES (" . "'" . $data['progress_update_img_name'] . "'" . ',' . $data['progress_update_id'] . ',' . "'" . $data['progress_update_dis_th'] . "'" . ',' . "'" . $data['progress_update_dis_en'] . "'" . ")";

            }

            try {

                mssql_query($sql,$GLOBALS['db_conn']);

            }  catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
            }

        }



        function updateRoomFeature($data, $projectSubId) {
            if (isset($data["productcondo_id"]) && $data["productcondo_id"] == 3) {

                if (isset($data["room_features"])) {
                    for($i=0 ;$i<sizeof($data["room_features"]) ;$i++) {

                        $roomID = $data['room_features'][$i];
                        $roomDesc = $data['room-des-'.$roomID];
                        $fileName = trim($_FILES['room-img-'.$roomID]["name"]);
                        if ($fileName != "" && $fileName != null){

                            $fileImgName = $_FILES['room-img-'.$roomID]["name"];
                            $fileImgTmp  = $_FILES['room-img-'.$roomID]["tmp_name"];
                            $fileImgSeo  = $data['room-img-seo-'.$roomID][0];
                            $dataName = date("d-m-Y-H:i:s");
                            $pathName = "fileupload/images/room/".$dataName."-".$fileImgName;
                            if(move_uploaded_file($fileImgTmp, $pathName)) {

                            } else{
                                $pathName = '';
                            }

                            $sql = "INSERT INTO lh_features (project_sub_id, room_features_id,
                            room_features_img, room_features_img_seo, room_features_desc)
                            VALUES ('".$projectSubId."', '".$roomID."', '".$pathName."', '".$fileImgSeo."', '".$roomDesc."')";
                    // echo $sql.' |||||||| ADD dsfsSDAKSLKD';
                        }else{
                            $pathName   = $data['roomImgTemp-'.$roomID];
                            $fileImgSeo = $data['roomImgSeoTemp-'.$roomID];

                            $sql = "INSERT INTO lh_features (project_sub_id, room_features_id,
                            room_features_img, room_features_img_seo, room_features_desc)
                            VALUES ('".$projectSubId."', '".$roomID."', '".$pathName."', '".$fileImgSeo."', '".$roomDesc."')";
                    // echo $sql.' |||||||| Update sAKDLAJSdl';
                        }

                        try {
                            mssql_query($sql,$GLOBALS['db_conn']);
                        } catch (PDOException $e) {
                    // echo $sql . "<br>" . $e->getMessage();
                        }

                    }
                }
            }
        }


        function remove_room_feature($projectSubId) {

            if ($projectSubId != null && $projectSubId != "") {

                $sql = "DELETE FROM lh_features
                WHERE project_sub_id = ".$projectSubId;

                try {
                    mssql_query($sql,$GLOBALS['db_conn']);
                } catch (PDOException $e) {
            // echo $sql . "<br>" . $e->getMessage();
                }
            }
        }



        function removeProgresssProject($list_progress_update_id,$old_subprojectId)
        {


            $sqlselect = ' SELECT progress_update_id '.
            " FROM LH_PROGRESS_UPDATE_CONDO ".
            " where progress_update_id not in (" . implode($list_progress_update_id, ",") ." )".
            " and project_sub_id =".$old_subprojectId ;


            $sql =  ' DELETE  from LH_PROGRESS_UPDATE_CONDO '.
            " where progress_update_id not in (" . implode($list_progress_update_id, ",") ." )".
            " and project_sub_id =".$old_subprojectId ;

    // echo "<BR>";
    // echo $sql ;
    // echo "<BR>";

            try {

                $result = $GLOBALS['conn']->query($sqlselect);

                $obj_delete_progress_update_id  = null ;

                if($result !=null)
                {
                    $obj_delete_progress_update_id = $result->fetchAll();
                }


                $GLOBALS['conn']->query($sql);


                $list_delete_progress_update_id =  array();

                if($obj_delete_progress_update_id == null )
                {
                    return null ;
                }

                for($i=0 ; $i<sizeof($obj_delete_progress_update_id) ;$i++)
                {
                    $list_delete_progress_update_id [] = $obj_delete_progress_update_id[$i]['progress_update_id'];
                }

                return $list_delete_progress_update_id;

            }  catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
            }


        }

        function removeImgProgressProject($list_progress_update_img_id,$progress_update_id)
        {

    // echo "<BR>";
    // echo print_r($list_progress_update_img_id);
    // echo "<BR>";


            $sql =  ' DELETE  from LH_PROGRESS_UPDATE_IMG '.
            " where progress_update_img_id not in (" . implode($list_progress_update_img_id, ",") ." )".
            " and progress_update_id =".$progress_update_id ;

    // echo "<BR>";
    // echo $sql ;
    // echo "<BR>";

            try {
                $GLOBALS['conn']->query($sql);

            }  catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
            }


        }

        function removeImgProgressByProgressUpdateId($progress_update_id)
        {

    // echo "<BR>";
            print_r($progress_update_id);
    // echo "<BR>";

            if($progress_update_id == null || $progress_update_id == "" )
            {
                return "";
            }


            $sql =  ' DELETE  from LH_PROGRESS_UPDATE_IMG '.
            " where progress_update_id  in (" . implode($progress_update_id, ",") ." )";


    // echo "<BR>";
    // echo $sql ;
    // echo "<BR>";

            try {
                $GLOBALS['conn']->query($sql);

            }  catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
            }
        }




        function getSubProjectIdByProductId($data,$produntId)
        {
            $size = sizeof($data);

            for($i = 0 ; $i<$size ; $i++)
            {
                if($data[$i]['productid'] == $produntId)
                {
                    return $data[$i]['subproject'];
                }
            }

            return null;

        }


        function updateProgressProject($data)
        {


            $progress_update =  $data['progress_update'];
            $progress_update_mount = $data['progress_update_mount'];

            $date = $progress_update;
    //30/03/2017
    //2017-03-30
            $day=substr($date, 0, 2);
            $mount = substr($date, 3, 2);
            $year = substr($date, 6, 4);
            $progress_update = $year."-".$mount."-".$day;

            $sql =  ' UPDATE  LH_PROGRESS_UPDATE_CONDO '.
            ' SET project_sub_id = '.$data['project_sub_id'].',';

            if($progress_update == null || $progress_update == "" || $progress_update == "--")
            {
                $progress_update = 'NULL' ;
            }
            else
            {
                $progress_update = "'".$progress_update."'" ;
            }

            if($progress_update_mount == null ||  $progress_update_mount == "")
            {
                $progress_update_mount = 'NULL' ;
            }


            $sql .=  'progress_update = '.$progress_update.','.
            ' progress_update_mount = '.$progress_update_mount.
            ' where progress_update_id = '. $data["progress_update_id"] ;

            try {

                mssql_query($sql,$GLOBALS['db_conn']);

            }  catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
            }

        }


        function updateImgProgressProject($data){

            $sql = "";

            if($data["progress_update_img_name"] != "") {

                $sql = ' UPDATE  LH_PROGRESS_UPDATE_IMG ' .
                ' SET progress_update_img_name = ' . "'" . $data['progress_update_img_name'] ."'" . ',' .
                ' progress_update_img_seo = ' . "'" . $data['progress_update_img_seo'] . "'" . ',' .
                ' progress_update_dis_th = ' . "'" . $data['progress_update_dis_th'] . "'" . ',' .
                ' progress_update_dis_en = ' . "'" . $data['progress_update_dis_en'] . "'" .
                ' where progress_update_img_id = ' . $data["progress_update_img_id"];
            }
            else
            {
                $sql = ' UPDATE  LH_PROGRESS_UPDATE_IMG ' .
                ' SET progress_update_dis_th = ' . "'" . $data['progress_update_dis_th'] . "'" . ',' .
                ' progress_update_dis_en = ' . "'" . $data['progress_update_dis_en'] . "'" .
                ' where progress_update_img_id = ' . $data["progress_update_img_id"];
            }

            try {

                mssql_query($sql,$GLOBALS['db_conn']);

            }  catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
            }

        }


        function updateCommunity($data) {
    //เช็คใหม่

            if(isset($_FILES["project_pacu"]["name"][0])) {
                if ($_FILES["project_pacu"]["name"] != "" && $_FILES["project_pacu"]["name"] != null) {
                    $numSize = count($_FILES['project_pacu']["name"]);
                    for ($i = 0; $i < $numSize; $i++) {
                        $dataName = date("d-m-Y-H:i:s");
                        $pathName = "fileupload/images/community/" . $dataName . '_' . $_FILES["project_pacu"]["name"][$i];
                        if (move_uploaded_file($_FILES["project_pacu"]["tmp_name"][$i], $pathName)) {

                            $sql2 = "INSERT INTO LH_COMMUNITY (project_id,community_features_img,community_features_img_seo,community_features_desc,community_features_desc_en,community_name,community_name_en)
                            VALUES ('" . $data["project_id"] . "','" . $pathName . "','" . $_POST['upload_project_pacu_seo'][$i] . "','" . $_POST['project_pacu_text'][$i] . "','" . $_POST['project_pacu_text_en'][$i] . "','" . $_POST['pacu_text'][$i] . "','" . $_POST['pacu_text_en'][$i] . "')";
                            $result = mssql_query($sql2, $GLOBALS['db_conn']);
                        }
                    }
                }
            }

            if(isset($_FILES["project_pacu_edit"]["name"][0])) {
                if ($_FILES["project_pacu_edit"]["name"][0] != "" && $_FILES["project_pacu_edit"]["name"][0] != null) {
                    $numSize = count($_FILES['project_pacu_edit']["name"]);
                    $array=array();
                    for ($i = 0; $i < $numSize; $i++) {

                        $dataName = date("d-m-Y-H:i:s");
                        $pathName = "fileupload/images/community/" . $dataName . '_' . $_FILES["project_pacu_edit"]["name"][$i];
                        if (move_uploaded_file($_FILES["project_pacu_edit"]["tmp_name"][$i], $pathName)) {

                         $sql3 = "UPDATE LH_COMMUNITY SET community_features_img = '$pathName',
                         community_features_img_seo = '" . $_POST['upload_project_pacu_seo'][$i] . "'
                         WHERE community_features_id = '" . $_POST['community_features_id'][$i] . "'";
                         $result = mssql_query($sql3, $GLOBALS['db_conn']);

                     }
                 }
             }
         }


         if(isset($_POST['pacu_text_th_edit'][0])){
            $numSize = count($_POST['pacu_text_th_edit']);
            for ($i = 0; $i < $numSize; $i++) {
                $community_features_id = $_POST['community_features_id'][$i];
                $sql_updateCommunity = "SELECT * FROM LH_COMMUNITY WHERE project_id  = '" . $data['project_id'] . "'";
                $qury_updateCommunity = mssql_query($sql_updateCommunity, $GLOBALS['db_conn']);
                while ($row = mssql_fetch_array($qury_updateCommunity)) {
                    if ($community_features_id == $row['community_features_id']) {

                        $sql2 = "UPDATE LH_COMMUNITY SET community_name = '".$_POST['pacu_text_th_edit'][$i]."',
                        community_name_en = '".$_POST['pacu_text_en_edit'][$i]."',
                        community_features_desc = '".$_POST['project_pacu_text_edit'][$i]."',
                        community_features_desc_en = '".$_POST['project_pacu_text_en_edit'][$i]."'
                        WHERE community_features_id = '" . $_POST['community_features_id'][$i] . "'";
                        $result = mssql_query($sql2, $GLOBALS['db_conn']);
                    }
                }
            }
        }

    //master paln

        function  add_MasterPlanCondo($data)
        {
            $sql = "SELECT s.project_sub_id,s.project_id FROM LH_PROJECTS p LEFT JOIN LH_PROJECT_SUB s 
            ON p.project_id = s.project_id WHERE p.project_id = '".$data['project_id']."' AND s.product_id = 3";
            $resul = mssql_query($sql, $GLOBALS['db_conn']);
            $row = mssql_fetch_array($resul);

            $update ="UPDATE lh_master_plan_condo SET project_sub_id = '".$row['project_sub_id']."' WHERE project_id = '".$data['project_id']."' ";
            $resul2 = mssql_query($update, $GLOBALS['db_conn']);

            if(isset($_FILES["master_plan_img_name"]["name"][0])) {
                if ($_FILES["master_plan_img_name"]["name"] != "" && $_FILES["master_plan_img_name"]["name"] != null) {
                    $numSize = count($_FILES['master_plan_img_name']["name"]);
                    for ($i = 0; $i < $numSize; $i++) {
                        $dataName = date("d-m-Y-H:i:s");
                        $pathName = "fileupload/images/master_plan/" . $dataName . '_' . $_FILES["master_plan_img_name"]["name"][$i];
                        if (move_uploaded_file($_FILES["master_plan_img_name"]["tmp_name"][$i], $pathName)) {

                            $sql2 = " INSERT INTO lh_master_plan_condo (master_plan_dis_th,master_plan_dis_en,project_sub_id,master_plan_img_name,master_plan_img_seo,project_id) ".
                            " VALUES ("."'".mssql_escape($data['master_plan_condo_dis_th'][$i])."'".','."'".mssql_escape($data['master_plan_condo_dis_en'][$i])."'".','."'".$row['project_sub_id']."'".','."'".$pathName."'".","."'".$data['master_seo'][$i]."'".",'".$data['project_id']."')";
                            $result = mssql_query($sql2, $GLOBALS['db_conn']);
                        }
                    }
                }
            }

            if(isset($_POST['master_plan_condo_dis_th_edit'][0])){
                $numSize = count($_POST['master_plan_condo_dis_th_edit']);
                for ($i = 0; $i < $numSize; $i++) {
                    $master_plan_id = $_POST['master_plan_id'][$i];
                    $sql_updateCommunity = "SELECT * FROM LH_MASTER_PLAN_CONDO m LEFT JOIN LH_PROJECTS j 
                    ON m.project_id = j.project_id WHERE j.project_id = '" . $data['project_id'] . "'";
                    $qury_updateCommunity = mssql_query($sql_updateCommunity, $GLOBALS['db_conn']);
                    while ($row = mssql_fetch_array($qury_updateCommunity)) {
                        if ($master_plan_id == $row['master_plan_id']) {

                            $sql2 = "UPDATE LH_MASTER_PLAN_CONDO SET master_plan_dis_th = '".mssql_escape($_POST['master_plan_condo_dis_th_edit'][$i])."',
                            master_plan_dis_en = '".mssql_escape($_POST['master_plan_condo_dis_en_edit'][$i])."',
                            master_plan_img_seo = '".$_POST['master_seo_edit'][$i]."'
                            WHERE master_plan_id = '" .$master_plan_id. "'";
                            $result = mssql_query($sql2, $GLOBALS['db_conn']);
                        }
                    }
                }
            }
        }

    //add_Lead_image
        function add_Lead_image ($data){
            $dataName = date("dmYHis");
       // print_r($_FILES['lead_image_edit']);

            if(isset($_FILES['lead_image_edit']) && count($_FILES['lead_image_edit']['error']) == 1 && $_FILES['lead_image_edit']['error'][0] > 0){
            //file not selected
        } else if(isset($_FILES['lead_image_edit'])){ //this is just to check if isset($_FILE). Not required.
            //file selected

            $numSize = count($_FILES['lead_image_edit']["name"]);
            if($numSize > 0){
                for ($i = 0; $i < $numSize; $i++) {
                    //resize
                    if($_FILES['lead_image_edit']["name"][$i] != ''){
                        $numrand = mt_rand();
                        $images = $_FILES["lead_image_edit"]["tmp_name"][$i];
                        $images_name = $_FILES["lead_image_edit"]["name"][$i];
                        $type_img = strrchr($images_name,".");
                        $new_images = "Thumbnails_"."LD_".$dataName.$numrand.$type_img;
                        //copy($_FILES["edit"]["tmp_name"][0],"fileupload/galery_master/".$_POST['folder_name_old'].$_FILES["edit"]["name"][0]);
                        $width=600; //*** Fix Width & Heigh (Autu caculate) ***//
                        $size=GetimageSize($images);
                        $height=round($width*$size[1]/$size[0]);
                        $images_orig = ImageCreateFromJPEG($images);
                        $photoX = ImagesX($images_orig);
                        $photoY = ImagesY($images_orig);
                        $images_fin = ImageCreateTrueColor($width, $height);

                        ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);

                        ImageJPEG($images_fin,"fileupload/images/project_img/".$new_images,93);

                        ImageDestroy($images_orig);
                        ImageDestroy($images_fin);

                        $pathName = "fileupload/images/project_img/" ."LD_" .$dataName.$numrand.$type_img;
                        if (move_uploaded_file($_FILES["lead_image_edit"]["tmp_name"][$i], $pathName)) {
                            if($_FILES['lead_image_edit']["name"][$i] != '') {

                            //echo "ok".$new_images;

                                $sql = "UPDATE LH_LEAD_IMAGE_PROJECT_FILE SET
                                LEAD_IMAGE_PROJECT_FILE_NAME ='".$pathName."',
                                LEAD_IMAGE_PROJECT_FILE_SEO = '".$data['lead_image_edit_seo'][$i]."'
                                WHERE LEAD_IMAGE_PROJECT_FILE_ID = '".$data['lead_image_id'][$i]."' ";

                                $result = mssql_query($sql, $GLOBALS['db_conn']);


                            }

                        }
                    }

                }
            }  
            //seo edit
            $numSize_seo = count($_POST['seo_lead_edit']);
            for ($i = 0; $i < $numSize_seo; $i++) {
             $sql = "UPDATE LH_LEAD_IMAGE_PROJECT_FILE SET
             seo_lead = '".$data['seo_lead_edit'][$i]."'
             WHERE LEAD_IMAGE_PROJECT_FILE_ID = '".$data['lead_image_id'][$i]."' ";

             $result = mssql_query($sql, $GLOBALS['db_conn']);

         } 

              //url edit
         $numSize_url = count($_POST['lead_image_edit']);
         for ($i = 0; $i < $numSize_url; $i++) {
             $sql = "UPDATE LH_LEAD_IMAGE_PROJECT_FILE SET
             img_url = '".$data['lead_image_edit'][$i]."'
             WHERE LEAD_IMAGE_PROJECT_FILE_ID = '".$data['lead_image_id'][$i]."' ";

             $result = mssql_query($sql, $GLOBALS['db_conn']);

         }  

     }

        //edit
     if(isset($_FILES["lead_image"]["name"][0])) {
        if ($_FILES["lead_image"]["name"][0] != "" && $_FILES["lead_image"]["name"][0] != null) {

            $numSize = count($_FILES['lead_image']["name"]);
            for ($i = 0; $i < $numSize; $i++) {

                $numrand = mt_rand();
                $images = $_FILES["lead_image"]["tmp_name"][$i];
                $images_name = $_FILES["lead_image"]["name"][$i];
                $type_img = strrchr($images_name,".");
                    //resize 
                $new_images = "Thumbnails_"."LD_".$dataName.$numrand.$type_img;
                    //copy($_FILES["lead_image"]["tmp_name"][0],"fileupload/images/project_img/".$_FILES["lead_image"]["name"][0]);
                    $width=600; //*** Fix Width & Heigh (Autu caculate) ***//
                    $size=GetimageSize($images);
                    $height=round($width*$size[1]/$size[0]);
                    $images_orig = ImageCreateFromJPEG($images);
                    $photoX = ImagesX($images_orig);
                    $photoY = ImagesY($images_orig);
                    $images_fin = ImageCreateTrueColor($width, $height);
                    ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
                    ImageJPEG($images_fin,"fileupload/images/project_img/".$new_images,93);
                    ImageDestroy($images_orig);
                    ImageDestroy($images_fin);     

                    $pathName = "fileupload/images/project_img/" ."LD_".$dataName .$numrand.$type_img;
                    if (move_uploaded_file($_FILES["lead_image"]["tmp_name"][$i], $pathName)) {

                        $sql = "INSERT INTO LH_LEAD_IMAGE_PROJECT_FILE
                        (PROJECT_ID, LEAD_IMAGE_PROJECT_FILE_NAME,LEAD_IMAGE_PROJECT_FILE_TYPE,seo_lead,img_url)
                        VALUES (" . $data["project_id"] . ", '" . $pathName . "','image','".$_POST['seo_lead_add'][$i]."','".$_POST['lead_image_url_add'][$i]."' )";
                        $result = mssql_query($sql, $GLOBALS['db_conn']);
                    }
                }
            }
        }

    }


    function add_Lead_Banner($data){
        // Lead image Banner
       if(isset($data['banner'])){ 
        if($data['banner'] == 'on'){

            $dataName = date("dmYHis");
            $startdate=$data['date_start'];
            $enddate= $data['date_end'];

            $sql = "UPDATE LH_LEAD_IMAGE_PROJECT_FILE SET 
            date_start = '$startdate',date_end = '$enddate' 
            WHERE PROJECT_ID = '".$data['project_id']."' AND LEAD_IMAGE_PROJECT_FILE_TYPE != 'image' ";
            $result = mssql_query($sql, $GLOBALS['db_conn']);

            if($data['banner_type']=='image_lead'){

                $dataName = date("dmYHis");
                $startdate=$data['date_start'];
                $enddate= $data['date_end'];

                if (! empty ( $startdate ) && ! empty ( $enddate )) {
                    $startdate = DateTime::createFromFormat ( 'd/m/Y', $startdate );
                    $enddate = DateTime::createFromFormat ( 'd/m/Y', $enddate );
                            // startdate->format("Y-m-d")
                    $startdate = $startdate->format('Y-m-d');
                    $enddate = $enddate->format('Y-m-d');

                    $sql = "UPDATE LH_LEAD_IMAGE_PROJECT_FILE SET 
                    date_start = '$startdate',date_end = '$enddate' 
                    WHERE PROJECT_ID = '".$data['project_id']."' AND LEAD_IMAGE_PROJECT_FILE_TYPE != 'image' ";
                    $result = mssql_query($sql, $GLOBALS['db_conn']);

                }
                if(isset($_FILES["lead_img_banner"]["name"][0])){
                    if ($_FILES["lead_img_banner"]["name"][0] != "" && $_FILES["lead_img_banner"]["name"][0] != null){
                        //check 
                        $projectId = $data['project_id'];


                        $sql ="SELECT COUNT(LEAD_IMAGE_PROJECT_FILE_ID) AS num from LH_LEAD_IMAGE_PROJECT_FILE 
                        where PROJECT_ID = '$projectId' AND LEAD_IMAGE_PROJECT_FILE_TYPE = 'image_leade_banner'";
                        $query = mssql_query($sql);    
                        $row = mssql_fetch_array($query);

                        if($row['num'] <= 0 ){
                           Check_BannerLead($projectId);
                       }

                       $numSize = count($_FILES['lead_img_banner']["name"]);
                       for ($i = 0; $i < $numSize; $i++) {

                        $images = $_FILES["lead_img_banner"]["name"][$i];
                        $numrand = (mt_rand());
                        $type_img = strrchr($images,".");

                        $pathName = "fileupload/images/project_img/" ."LD_banner_". $dataName . $numrand .$type_img;
                        $fname = "fileupload/images/project_img/Thumbnails_" ."LD_banner_". $dataName . $numrand .$type_img;
                        if (move_uploaded_file($_FILES["lead_img_banner"]["tmp_name"][$i], $pathName)) {
                         $sql = "INSERT INTO LH_LEAD_IMAGE_PROJECT_FILE
                         (PROJECT_ID, LEAD_IMAGE_PROJECT_FILE_NAME, LEAD_IMAGE_PROJECT_FILE_TYPE, LEAD_IMAGE_PROJECT_FILE_SEO,date_start,date_end,seo_lead,img_url)
                         VALUES (".$data["project_id"].", '".$pathName."', 'image_leade_banner', NULL,'$startdate','$enddate','".$data['seo_lead_banner_add'][$i]."','".$data['lead_img_banner_url_add'][$i]."')";
                         $result = mssql_query($sql, $GLOBALS['db_conn']);
                         Resize_Thumbnail($pathName,$fname);
                     }
                 }
             }
         }   

                    // Edit Lead image
         if(isset($_FILES["lead_img_banner_edit"]["name"][0])){  
            if ($_FILES["lead_img_banner_edit"]["name"][0] != "" && $_FILES["lead_img_banner_edit"]["name"][0] != null){

                $numSize = count($_FILES['lead_img_banner_edit']["name"]);
                for ($i = 0; $i < $numSize; $i++) {

                    $images = $_FILES["lead_img_banner_edit"]["name"][$i];
                    $numrand = mt_rand();
                    $type_img = strrchr($images,".");

                    $pathName = "fileupload/images/project_img/" ."LD_banner_". $dataName . $numrand .  $type_img;
                    $fname = "fileupload/images/project_img/Thumbnails_" ."LD_banner_". $dataName . $numrand .  $type_img;
                    if (move_uploaded_file($_FILES["lead_img_banner_edit"]["tmp_name"][$i], $pathName)) {
                     $sql = "UPDATE LH_LEAD_IMAGE_PROJECT_FILE SET 
                     LEAD_IMAGE_PROJECT_FILE_NAME = '$pathName'
                     WHERE LEAD_IMAGE_PROJECT_FILE_ID = '".$data['lead_image_banner_id_edit'][$i]."' ";
                     $result = mssql_query($sql, $GLOBALS['db_conn']);
                     Resize_Thumbnail($pathName,$fname);
                 }

                 $sql = "UPDATE LH_LEAD_IMAGE_PROJECT_FILE SET 
                 date_start = '$startdate',date_end = '$enddate' 
                 WHERE LEAD_IMAGE_PROJECT_FILE_ID = '".$data['lead_image_banner_id_edit'][$i]."'";
                 $result = mssql_query($sql, $GLOBALS['db_conn']);        
             }

         }
     }  
                    //END Edit Lead image

                  //edit seo
     if(isset($data['seo_lead_banner_edit'][0])){
      $numSize = count($data['seo_lead_banner_edit']);
      for ($i = 0; $i < $numSize; $i++) {
         $sql = "UPDATE LH_LEAD_IMAGE_PROJECT_FILE SET 
         seo_lead = '".$data['seo_lead_banner_edit'][$i]."' 
         WHERE LEAD_IMAGE_PROJECT_FILE_ID = '".$data['lead_image_banner_id_edit'][$i]."'";
         $result = mssql_query($sql, $GLOBALS['db_conn']);  
     }
 }

                  //edit url
 if(isset($data['lead_img_banner_url_edit'][0])){
  $numSize = count($data['lead_img_banner_url_edit']);
  for ($i = 0; $i < $numSize; $i++) {
     $sql = "UPDATE LH_LEAD_IMAGE_PROJECT_FILE SET 
     img_url = '".$data['lead_img_banner_url_edit'][$i]."' 
     WHERE LEAD_IMAGE_PROJECT_FILE_ID = '".$data['lead_image_banner_id_edit'][$i]."'";
     $result = mssql_query($sql, $GLOBALS['db_conn']);  
 }
}

}elseif($data['banner_type']=='activity'){
    $dataName = date("dmYHis");
    $startdate=$data['date_start'];
    $enddate= $data['date_end'];

    $text = $data['banner_text'];

    if (! empty ( $startdate ) && ! empty ( $enddate )) {
        $startdate = DateTime::createFromFormat ( 'd/m/Y', $startdate );
        $enddate = DateTime::createFromFormat ( 'd/m/Y', $enddate );
                            // startdate->format("Y-m-d")
        $startdate = $startdate->format('Y-m-d');
        $enddate = $enddate->format('Y-m-d');

    }

                     //Update data Banner Activity
    $sql = "UPDATE LH_LEAD_IMAGE_PROJECT_FILE SET 
    banner_text = '".mssql_escape($text)."',
    backgroup_color = '".$data['favcolor']."',
    date_start = '$startdate',date_end = '$enddate'
    WHERE LEAD_IMAGE_PROJECT_FILE_ID = '".$data['banner_activity_id_edit']."'"; 
    $result = mssql_query($sql, $GLOBALS['db_conn']);         

                    //Add Banner Activity
    if(isset($_FILES["banner_image"]["name"])){
        if ($_FILES["banner_image"]["name"] != "" && $_FILES["banner_image"]["name"] != null){
            $projectId = $data['project_id'];
            Check_BannerLead($projectId);
            $file_name = $_FILES["banner_image"]["name"];
            $dataName  = date("dmYHis");
            $images    = $_FILES["banner_image"]["tmp_name"];
            $pathName  = "fileupload/images/project_img/" . $dataName . '-' . $file_name;

            if (move_uploaded_file($images, $pathName)) {
                $sql = "INSERT INTO LH_LEAD_IMAGE_PROJECT_FILE
                (PROJECT_ID, LEAD_IMAGE_PROJECT_FILE_NAME, LEAD_IMAGE_PROJECT_FILE_TYPE, LEAD_IMAGE_PROJECT_FILE_SEO,banner_text,backgroup_color,date_start,date_end)
                VALUES ('".$data["project_id"]."', '".$pathName."', 'banner', NULL ,'".mssql_escape($text)."','".$data['favcolor']."','$startdate','$enddate')";
                $result = mssql_query($sql, $GLOBALS['db_conn']);
            }
        }
    }

                    //Edit Banner activity
    if ($_FILES["banner_activity_edit"]["name"] != "" && $_FILES["banner_activity_edit"]["name"] != null){
        $projectId = $data['project_id'];
        $file_name = $_FILES["banner_activity_edit"]["name"];
        $dataName  = date("dmYHis");
        $images    = $_FILES["banner_activity_edit"]["tmp_name"];
        $pathName  = "fileupload/images/project_img/" . $dataName . '-' . $file_name;

        if (move_uploaded_file($images, $pathName)) {
         $sql = "UPDATE LH_LEAD_IMAGE_PROJECT_FILE SET 
         LEAD_IMAGE_PROJECT_FILE_NAME = '$pathName'
         WHERE LEAD_IMAGE_PROJECT_FILE_ID = '".$data['banner_activity_id_edit']."'";
         $result = mssql_query($sql, $GLOBALS['db_conn']);
     }
 }




}elseif($data['banner_type']=='vdo'){

    $dataName = date("dmYHis");
    $startdate=$data['date_start'];
    $enddate= $data['date_end'];

    if (! empty ( $startdate ) && ! empty ( $enddate )) {
        $startdate = DateTime::createFromFormat ( 'd/m/Y', $startdate );
        $enddate = DateTime::createFromFormat ( 'd/m/Y', $enddate );
                            // startdate->format("Y-m-d")
        $startdate = $startdate->format('Y-m-d');
        $enddate = $enddate->format('Y-m-d');

    }

                    //youtube
    if($data['upvido'] == 'youtube'){

                        //Add youtube
      if(isset($_FILES["img_youtube_banner"]["name"])){ 
                        //check 
        $projectId = $data['project_id'];
        Check_BannerLead($projectId);

        $file_name = $_FILES["img_youtube_banner"]["name"];
        $dataName  = date("dmYHis");
        $images    = $_FILES["img_youtube_banner"]["tmp_name"];
        $pathName  = "fileupload/images/project_img/" . $dataName . '-' . $file_name;
        if (move_uploaded_file($images, $pathName)) {
         $sql = "INSERT INTO LH_LEAD_IMAGE_PROJECT_FILE
         (PROJECT_ID,banner_img_thum,  LEAD_IMAGE_PROJECT_FILE_TYPE, LEAD_IMAGE_PROJECT_FILE_SEO,LEAD_IMAGE_PROJECT_FILE_NAME,date_start,date_end)
         VALUES ('".$data["project_id"]."', '".$pathName."', 'youtube', NULL,'".$data['url_youtube2']."','$startdate','$enddate')";
         $result = mssql_query($sql, $GLOBALS['db_conn']);
     }
 }  

                        //Edit Youtube 
 if(isset($_FILES["banner_youtube_edit"]["name"])){  
    $file_name = $_FILES["banner_youtube_edit"]["name"];
    $dataName  = date("dmYHis");
    $images    = $_FILES["banner_youtube_edit"]["tmp_name"];
    $pathName  = "fileupload/images/project_img/" . $dataName . '-' . $file_name;
    if (move_uploaded_file($images, $pathName)) {

     $sql = "UPDATE LH_LEAD_IMAGE_PROJECT_FILE SET 
     banner_img_thum = '$pathName' ,
     LEAD_IMAGE_PROJECT_FILE_NAME = '".$data['url_youtube2']."' 
     WHERE LEAD_IMAGE_PROJECT_FILE_ID = '".$data['banner_youtube_id']."'";
     $result = mssql_query($sql, $GLOBALS['db_conn']);
 }
}

$sql = "UPDATE LH_LEAD_IMAGE_PROJECT_FILE SET 
LEAD_IMAGE_PROJECT_FILE_NAME = '".$data['url_youtube2']."', 
date_start = '$startdate',date_end = '$enddate' 
WHERE PROJECT_ID = '".$data['project_id']."' AND LEAD_IMAGE_PROJECT_FILE_TYPE = 'youtube' ";
$result = mssql_query($sql, $GLOBALS['db_conn']);   


}elseif($data['upvido']== 'vdo'){
    $dataName  = date("dmYHis");

                        //vdo file
    if(isset($_FILES["vdo_img"]["name"])){
      if(isset($_FILES["vdo_file"]["name"])){
        $projectId = $data['project_id'];
        Check_BannerLead($projectId);

        $file_name = $_FILES["vdo_img"]["name"];

        $images    = $_FILES["vdo_img"]["tmp_name"];
        $pathName  = "fileupload/images/project_img/" . $dataName . '-' . $file_name;

        $name_vdo = $_FILES["vdo_file"]["name"];
        $pathNamevdo = "fileupload/images/project_img/" . $dataName . '-' . $name_vdo;
        $vdo = $_FILES["vdo_file"]["tmp_name"];

        if (move_uploaded_file($images, $pathName)) {
            if(move_uploaded_file($vdo, $pathNamevdo)){
             $sql = "INSERT INTO LH_LEAD_IMAGE_PROJECT_FILE
             (PROJECT_ID,banner_img_thum,  LEAD_IMAGE_PROJECT_FILE_TYPE, LEAD_IMAGE_PROJECT_FILE_SEO,LEAD_IMAGE_PROJECT_FILE_NAME,date_start,date_end)
             VALUES ('".$data["project_id"]."', '".$pathName."', 'vdo', NULL,'".$pathNamevdo."','$startdate','$enddate')";
             $result = mssql_query($sql, $GLOBALS['db_conn']);
         }
     }
 }
}

                        //Edit img VDO
if(isset($_FILES['banner_img_vdo_edit'])){
   $images    = $_FILES["banner_img_vdo_edit"]["tmp_name"];
   $file_name = $_FILES["banner_img_vdo_edit"]["name"];
   $pathName  = "fileupload/images/project_img/" . $dataName . '-' . $file_name;
   if (move_uploaded_file($images, $pathName)) {
       $sql = "UPDATE LH_LEAD_IMAGE_PROJECT_FILE SET 
       banner_img_thum = '$pathName' 
       WHERE LEAD_IMAGE_PROJECT_FILE_ID = '".$data['banner_img_vdo_id_edit']."'";
       $result = mssql_query($sql, $GLOBALS['db_conn']);
   }
}

                        //Edit VDO file
if(isset($_FILES['banner_vdo_file_edit'])){
   $vdo    = $_FILES["banner_vdo_file_edit"]["tmp_name"];
   $file_name = $_FILES["banner_vdo_file_edit"]["name"];
   $pathNamevdo  = "fileupload/images/project_img/" . $dataName . '-' . $file_name;
   if (move_uploaded_file($vdo, $pathNamevdo)) {
       $sql = "UPDATE LH_LEAD_IMAGE_PROJECT_FILE SET 
       LEAD_IMAGE_PROJECT_FILE_NAME = '$pathNamevdo' 
       WHERE LEAD_IMAGE_PROJECT_FILE_ID = '".$data['banner_vdo_file_id_edit']."'";
       $result = mssql_query($sql, $GLOBALS['db_conn']);
   }
}

$sql = "UPDATE LH_LEAD_IMAGE_PROJECT_FILE SET 
date_start = '$startdate',date_end = '$enddate' 
WHERE PROJECT_ID = '".$data['project_id']."' AND LEAD_IMAGE_PROJECT_FILE_TYPE = 'vdo' ";
$result = mssql_query($sql, $GLOBALS['db_conn']); 
}

}

            //END if banner
}else{

   $sql ="DELETE from LH_LEAD_IMAGE_PROJECT_FILE 
   where PROJECT_ID = '".$data['project_id']."' AND LEAD_IMAGE_PROJECT_FILE_TYPE != 'image'";
   $query = mssql_query($sql);  

}
}else{
 $sql ="DELETE from LH_LEAD_IMAGE_PROJECT_FILE 
 where PROJECT_ID = '".$data['project_id']."' AND LEAD_IMAGE_PROJECT_FILE_TYPE != 'image'";
 $query = mssql_query($sql); 
}


}

    //resize thumnail facebook
function Resize_Thumbnail($images,$picName){

    if(isset($images)){
           $width       =600; //*** Fix Width & Heigh (Autu caculate) ***//
           $size        =GetimageSize($images);
           $height      =round($width*$size[1]/$size[0]);
           $images_orig = ImageCreateFromJPEG($images);
           $photoX      = ImagesX($images_orig);
           $photoY      = ImagesY($images_orig);
           $images_fin  = ImageCreateTrueColor($width, $height);
           ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
           ImageJPEG($images_fin,$picName,93);
           ImageDestroy($images_orig);
           ImageDestroy($images_fin);
       }
   }

    //fucntion check banner lead to project id
   function Check_BannerLead($id){
       $sql ="DELETE from LH_LEAD_IMAGE_PROJECT_FILE 
       where PROJECT_ID = '$id' AND LEAD_IMAGE_PROJECT_FILE_TYPE != 'image'";
       $query = mssql_query($sql);            
   }


}