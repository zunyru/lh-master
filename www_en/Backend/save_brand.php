<?php
session_start();
include './include/dbCon_mssql.php';
include './include/UpdateSlug.php';

function mssql_escape($str)
{
 if(get_magic_quotes_gpc())
 {
     $str= stripslashes($str);
 }
 return str_replace("'", "''", $str);
}
date_default_timezone_set('Asia/Bangkok');
$date       = date("Ymd");
$numrand_th = (mt_rand());
$numrand_en = (mt_rand());

$brand_name_th = mssql_escape($_POST['brand_name_th']);
$brand_name_en = mssql_escape($_POST['brand_name_en']);
$fileupload_th = $_FILES['fileupload_th'];
$fileupload_en = $_FILES['fileupload_en'];
$concept_th    = mssql_escape($_POST['concept_th']);
$message_th    = mssql_escape($_POST['message_th']);
$concept_en    = mssql_escape($_POST['concept_en']);
$message_en    = mssql_escape($_POST['message_en']);

$fileupload_th = $_FILES['fileupload_th']['name'];
$fileupload_en = $_FILES['fileupload_en']['name'];

$dates        = date("Y-m-d H:i:s");
$query        = "SELECT COUNT(*) AS counts FROM dbo.LH_BRANDS WHERE brand_name_th = '$brand_name_th' ";
$query_result = mssql_query($query, $db_conn);

$count = mssql_fetch_row($query_result);
$count[0];
if ($count[0] >= 1) {
    $_SESSION['add'] = '0';
    echo '<script>window.history.go(-1);</script>';
    exit();
}

if ($fileupload_th != '') {

    $fileupload_en = $_FILES['fileupload_th']['name'];
    $array_last_th = explode(".", $fileupload_en);
    $c_th          = count($array_last_th) - 1;
    $lastname_th   = strtolower($array_last_th[$c_th]);

    //echo $lastname_th;
    if ($lastname_th == "gif" or $lastname_th == "jpg" or $lastname_th == "jpeg" or $lastname_th == "png") {
        //ตรวจสอบนามสกุล
    } else {
        echo "<script type='text/javascript'>";
        echo "window.location = 'brand.php?error=2'; ";
        echo "</script>";
    }
}


if ($brand_name_th != '') {
    if ($fileupload_th != '') {
            //โฟลเดอร์ที่จะ upload file เข้าไป
        $path = "fileupload/images/brand_img/";

        $seo_th = $_POST['brand_img_seo_th'][0];
        $seo_en = $_POST['brand_img_seo_en'][0];

        $type_th = strrchr($_FILES['fileupload_th']['name'], ".");
        $type_en = strrchr($_FILES['fileupload_en']['name'], ".");

        $newname_th = $date . $numrand_th . $type_th;
        $newname_en = $date . $numrand_en . $type_en;

        $path_copy_th = $path . $newname_th;
        $path_copy_en = $path . $newname_en;

        if (move_uploaded_file($_FILES['fileupload_th']['tmp_name'], $path_copy_th)) {
            if ($count[0] == 0) {
                $sql = "SELECT  COUNT(brand_id) AS num_id FROM LH_BRANDS";
                $query_result1 = mssql_query($sql, $db_conn);
                $num = mssql_fetch_array($query_result1);
                $query_2 = "INSERT INTO dbo.LH_BRANDS (brand_name_th,brand_name_en,brand_concep_th,message_th,brand_concep_en,message_en,logo_brand_th,logo_brand_en,brand_update,brand_img_seo_th,brand_img_seo_en,order_seq)
                VALUES ('$brand_name_th','$brand_name_en','$concept_th','$message_th','$concept_en','$message_en','$newname_th','$newname_en','$dates','$seo_th','$seo_en','".$num['num_id']."');";

                $query_result2 = mssql_query($query_2, $db_conn);
                        //update slug
                UpdateSlug::run(['brand']);
                if ($query_result2) {
                    $_SESSION['add'] = '1';
                    header("location:brand_add.php");
                }
            } else {
                $_SESSION['add'] = '0';
                echo '<script>window.history.go(-1);</script>';
                exit();
            }
        }
    }
} else {
    $_SESSION['add'] = '-1';
    header("location:brand_add.php");
    echo '<script>window.history.go(-1);</script>';
    exit();
}

