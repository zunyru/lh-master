<?php
require_once 'include/dbConnect.php';

	try {
		$home_sell_id = $_GET['home_sell_id'];

		$conn = (new dbConnect())->getConn();
		$sql = "SELECT HSIF.home_sell_img_file_id, HSIF.home_sell_img_name, HSIF.home_sell_img_seo_name FROM LH_HOME_SELL_IMG HSI
					LEFT JOIN LH_HOME_SELL HS
					ON HSI.home_sell_img_id = HS.image_in_home_sell_id
					LEFT JOIN LH_HOME_SELL_IMG_FILE HSIF
					ON HSI.home_sell_img_id = HSIF.home_sell_img_id
					WHERE HS.home_sell_id = ".$home_sell_id;
        $result= $conn->query($sql);

		echo json_encode($result->fetchAll());

	} catch (\Exception $e) {
		return $e->getMessage();
	}
?>