<?php
include './include/dbCon_mssql.php';
date_default_timezone_set('Asia/Bangkok');
$date = date("Ymd");

function mssql_escape($str)
{
    if(get_magic_quotes_gpc())
    {
        $str= stripslashes($str);
    }
    return str_replace("'", "''", $str);
}

$banner_name = $_POST['banner_name'];
$page = $_POST['page'];
$banner_url = $_POST['banner_url'];
$project = $_POST['project'];
//image
//banner
//youtube
//vdo

 if($page  ==  "image"){

     $sql_r = "INSERT INTO LH_BANNER (banner_name,banner_type,banner_link) "
         . " VALUES ('" . mssql_escape($banner_name) . "','$page','$banner_url')";
     $query_r = mssql_query($sql_r, $db_conn);

     $sql="SELECT MAX(banner_id) AS id FROM LH_BANNER";
     $query = mssql_query($sql, $db_conn);
     $row = mssql_fetch_array($query);
     $id=$row['id'];

     $numrand= (mt_rand());
     $images = $_FILES['lead_img_file'];
     $file_image = $images['name'];

     $total = count($_FILES['lead_img_file']['name']);

// Loop through each file
     for($i=0; $i<$total; $i++) {
         if (!empty($_FILES['lead_img_file'])) {
             $dates_file = date("Y-m-d");
             $path_img = "fileupload/banner_file/";

             $type_img = strrchr($file_image[$i], ".");

             $newname_img = $date .$i. $numrand . $type_img;
             $path_copy_img = $path_img . $newname_img;

             if (move_uploaded_file($_FILES['lead_img_file']['tmp_name'][$i], $path_copy_img)) {
                    //echo $newname_img;
                    $sql_imag="INSERT INTO LH_BANNER_SUB (banner_id,banner) VALUES ('$id' , '$path_copy_img' )";
                    $query_img = mssql_query($sql_imag, $db_conn);
             }

         }
     }
     foreach ($_POST['spam'] as  $key_n => $spam_n){
            //echo $spam_n;
         $sql_imag="INSERT INTO LH_BANNER_PAGE (banner_id,banner_page,banner_project_id) VALUES ('$id' , '$spam_n','$project' )";
         $query_img = mssql_query($sql_imag, $db_conn);
     }

 }elseif ($page  ==  "banner"){
     $banner_text = $_POST['banner_text'];
     $favcolor = $_POST['favcolor'];

     $sql_r = "INSERT INTO LH_BANNER (banner_name,banner_type,banner_link) "
         . " VALUES ('" . mssql_escape($banner_name) . "','$page','$banner_url')";
     $query_r = mssql_query($sql_r, $db_conn);

     $sql="SELECT MAX(banner_id) AS id FROM LH_BANNER";
     $query = mssql_query($sql, $db_conn);
     $row = mssql_fetch_array($query);
     $id=$row['id'];

     $numrand= (mt_rand());
     $images = $_FILES['banner_image'];
     $file_image = $images['name'];

     $total = count($_FILES['banner_image']['name']);

// Loop through each file
  //   for($i=0; $i<$total; $i++) {
         if (!empty($_FILES['banner_image'])) {
             $dates_file = date("Y-m-d");
             $path_img = "fileupload/banner_file/";

             $type_img = strrchr($file_image, ".");

             $newname_img = $date . $numrand . $type_img;
             $path_copy_img = $path_img . $newname_img;

             if (move_uploaded_file($_FILES['banner_image']['tmp_name'], $path_copy_img)) {
                 //echo $newname_img;
                 $sql_imag="INSERT INTO LH_BANNER_SUB (banner_id,banner,banner_text,banner_backgroup) VALUES ('$id' , '$path_copy_img','$banner_text','$favcolor' )";
                 $query_img = mssql_query($sql_imag, $db_conn);
             }

         }
   //  }
     foreach ($_POST['spam'] as  $key_n => $spam_n){
         //echo $spam_n;
         $sql_imag="INSERT INTO LH_BANNER_PAGE (banner_id,banner_page,banner_project_id) VALUES ('$id' , '$spam_n' ,'$project')";
         $query_img = mssql_query($sql_imag, $db_conn);
     }
 }
 elseif ($page  ==  "youtube"){
     $url_youtube = $_POST['url_youtube'];

     $sql_r = "INSERT INTO LH_BANNER (banner_name,banner_type,banner_link) "
         . " VALUES ('" . mssql_escape($banner_name) . "','$page','$banner_url')";
     $query_r = mssql_query($sql_r, $db_conn);

     $sql="SELECT MAX(banner_id) AS id FROM LH_BANNER";
     $query = mssql_query($sql, $db_conn);
     $row = mssql_fetch_array($query);
     $id=$row['id'];

     $numrand= (mt_rand());
     $images = $_FILES['img_youtube'];
     $file_image = $images['name'];

     $total = count($_FILES['img_youtube']['name']);

// Loop through each file
 //    for($i=0; $i<$total; $i++) {
         if (!empty($_FILES['img_youtube'])) {
             $dates_file = date("Y-m-d");
             $path_img = "fileupload/banner_file/";

             $type_img = strrchr($file_image, ".");

             $newname_img = $date.  $numrand . $type_img;
             $path_copy_img = $path_img . $newname_img;

             if (move_uploaded_file($_FILES['img_youtube']['tmp_name'], $path_copy_img)) {
                 //echo $newname_img;
                echo $sql_imag="INSERT INTO LH_BANNER_SUB (banner_id,banner_img_thum,banner) VALUES ('$id' , '$path_copy_img','$url_youtube' )";
                 $query_img = mssql_query($sql_imag, $db_conn);
             }

         }
     //}
     foreach ($_POST['spam'] as  $key_n => $spam_n){
         //echo $spam_n;
         $sql_imag="INSERT INTO LH_BANNER_PAGE (banner_id,banner_page,banner_project_id) VALUES ('$id' , '$spam_n','$project' )";
         $query_img = mssql_query($sql_imag, $db_conn);
     }
 }elseif ($page  ==  "vdo"){

     $sql_r = "INSERT INTO LH_BANNER (banner_name,banner_type,banner_link) "
         . " VALUES ('" . mssql_escape($banner_name) . "','$page','$banner_url')";
     $query_r = mssql_query($sql_r, $db_conn);

     $sql="SELECT MAX(banner_id) AS id FROM LH_BANNER";
     $query = mssql_query($sql, $db_conn);
     $row = mssql_fetch_array($query);
     $id=$row['id'];

     $numrand= (mt_rand());
     $images = $_FILES['vdo_img'];
     $vdo = $_FILES['vdo_file'];

     //print_r($vdo );
     $file_image = $images['name'];
     $file_vdo = $vdo['name'];

     $total = count($_FILES['vdo_file']['name']);

     //for($i=0; $i<$total; $i++) {
         if (!empty($_FILES['vdo_file'])) {
             $dates_file = date("Y-m-d");
             $path_img = "fileupload/banner_file/";

             $type_img = strrchr($file_image, ".");
             $type_vdo = strrchr($file_vdo, ".");

             $newname_img = $date .$numrand . $type_img;
             $newname_vdo = $date .$numrand ."_vdo". $type_vdo;

             $path_copy_img = $path_img . $newname_img ;
             $path_copy_vdo = $path_img . $newname_vdo;

             if (move_uploaded_file($_FILES['vdo_img']['tmp_name'], $path_copy_img)) {
                 if (move_uploaded_file($_FILES['vdo_file']['tmp_name'], $path_copy_vdo)) {
                     //echo $newname_img;
                     $sql_imag = "INSERT INTO LH_BANNER_SUB (banner_id,banner,banner_img_thum) VALUES ('$id' , '$path_copy_vdo','$path_copy_img' )";
                     $query_img = mssql_query($sql_imag, $db_conn);
                 }
             }
        // }
             foreach ($_POST['spam'] as  $key_n => $spam_n){
                 //echo $spam_n;
                 $sql_imag="INSERT INTO LH_BANNER_PAGE (banner_id,banner_page,banner_project_id) VALUES ('$id','$spam_n','$project')";
                 $query_img = mssql_query($sql_imag, $db_conn);
             }

         }


 }

