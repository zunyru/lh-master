<?php
require_once 'include/dbConnect.php';

	try {
		$home_sell_img_file_id = $_GET['home_sell_img_file_id'];

		$conn = (new dbConnect())->getConn();
		$sql = "DELETE FROM LH_HOME_SELL_IMG_FILE
				WHERE home_sell_img_file_id =".$home_sell_img_file_id;
        $result= $conn->query($sql);

		echo json_encode($result->fetchAll());

	} catch (\Exception $e) {
		return $e->getMessage();
	}
?>