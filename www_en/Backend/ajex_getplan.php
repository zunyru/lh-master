<?php

include 'include/dbCon_mssql.php';
$q          = isset( $_POST['id_plan'])?$_POST['id_plan']:'';
$check_hs   = isset($_POST['check_homesell'])? $_POST['check_homesell']:'';
$icon   = isset($_POST['id_icon'])?$_POST['id_icon']:'';
$master  = isset($_POST['id_master'])?$_POST['id_master']:'';
$id_home_sell  = isset($_POST['id_home_sell'])?$_POST['id_home_sell']:'';

$sql="SELECT * FROM LH_PLANS p LEFT JOIN LH_GALERY_PLAN g ON p.plan_id = g.plan_id WHERE p.plan_id = '".$q."'";

$query = mssql_query($sql);
$row1=mssql_fetch_assoc($query);

$sql_model="SELECT * FROM LH_PLANS p LEFT JOIN LH_MODEL_IMAGE_PLANS m ON p.plan_id = m.plan_id WHERE p.plan_id ='".$q."'";
$quer_model = mssql_query($sql_model);
$array_model =[];
while ($row_model=mssql_fetch_assoc($quer_model)){
    array_push($array_model,$row_model);
}

$sql_model_ch ="SELECT model_image FROM LH_HOME_SELL WHERE home_sell_id = '".$id_home_sell."'";

$query_ch_model = mssql_query($sql_model_ch);
$row_ch_model =mssql_fetch_assoc($query_ch_model);


$sql_f ="SELECT * FROM LH_FUNCTION_PLAN_SUB fb LEFT JOIN LH_PLAN_FUNCTION f ON fb.function_plan_plan_id = f.function_plan_id WHERE fb.plan_id = '".$q."'";
$query_f = mssql_query($sql_f);
$array =[];
while ($row2=mssql_fetch_assoc($query_f)){
    array_push($array,$row2);
}



$sql_s ="SELECT sm.series_img_name,s.series_logo,s.series_logo_seo FROM LH_PLANS p LEFT JOIN LH_SERIES s ON p.series_id = s.series_id LEFT JOIN LH_SERIES_IMG sm ON sm.series_id = s.series_id WHERE p.plan_id = '".$q."'";
$query_s = mssql_query($sql_s);
$row3=mssql_fetch_assoc($query_s);

$sql_floor_paln="SELECT * FROM  LH_GALERY_FLOOR_PLAN_IMG fimg  WHERE plan_id = '".$q."'";
$query_floor = mssql_query($sql_floor_paln);

$array_f =[];
while ($row4=mssql_fetch_assoc($query_floor)){
    array_push($array_f,$row4);
}

$array2 = [];
if(isset($check_hs)) {
    if (empty($check_hs)) {
        $sql2 = "SELECT * FROM LH_GALERY_PLAN WHERE plan_id ='" . $q . "' AND galery_plan_type = 'home_plan'";
    } else {
        $sql2 = "SELECT * FROM LH_GALERY_PLAN WHERE plan_id ='" . $q . "' AND galery_plan_type = 'home_sell'";
    }
    $query2 = mssql_query($sql2);
    $row2 = mssql_num_rows($query2);


    while ($row2 = mssql_fetch_assoc($query2)) {
        array_push($array2, $row2);
    }
}

$array_check = [];
$sql_c = "SELECT m.galery_plan_id FROM LH_HOME_SELL h 
        LEFT JOIN LH_GALERY_HOME_SELL_MAIN m ON m.home_sell_id = h.home_sell_id
        LEFT JOIN LH_GALERY_PLAN p ON m.galery_plan_id = p.galery_plan_id
        WHERE h.home_sell_id = '$id_home_sell'";
$query_c = mssql_query($sql_c);

while ($row_c = mssql_fetch_assoc($query_c)) {
    array_push($array_check, $row_c);
}

$array_icon=[];
if(isset($icon)) {
    if (!empty($icon)) {
        $sql_icon = "SELECT * FROM  LH_ICON_ACTIVITY   WHERE icon_id = '" . $icon . "' AND  CONVERT(date, getdate()) < icon_date_end";
        $query_icon = mssql_query($sql_icon);
        $array_icon = mssql_fetch_array($query_icon);
    }
}

$array_ms = [];
if(isset($master)) {
    if (!empty($master)) {
        $sql_master = "SELECT * FROM LH_GALERY_FURNISHED_MAIN m 
LEFT JOIN LH_GALERY_FURNISHED_SUB s 
ON m.galery_funished_main_id = s.galery_funished_main_id 
WHERE m.galery_funished_main_id = '" . $master . "'";
        $query_master = mssql_query($sql_master);


        while ($row5 = mssql_fetch_assoc($query_master)) {
            array_push($array_ms, $row5);
        }


    }
}


$response['qalery']=$array;
$response['gallery_plan_ch'] = $array_check;

$response['q1']=$row1;
$response['q2']=$array;
$response['q3']=$row3;
$response['q4']=$array_f;
$response['qalery']=$array2;
$response['icon']=$array_icon;
$response['master'] = $array_ms;
$response['model'] = $array_model;
$response['ch_model'] = $row_ch_model ;


header('Content-Type: application/json');
echo json_encode($response);

?>