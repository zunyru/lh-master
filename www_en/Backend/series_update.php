<?php
session_start();
$_SESSION['group_id'];
include_once('include/dbSeries.php');
include './include/dbCon_mssql.php';
header('Content-Type:text/html; charset=utf8');
$_GET['page'] = 'homemodel';
$id = $_GET['id'];
$funBrabd = new dbSeries();

if (isset($_SESSION['add'])) {
	if ($_SESSION['add'] == 1) {
		//header("location:product_add.php");
		$success = "success";
		$_SESSION['add'] = '';
	} else if ($_SESSION['add'] == 2) {

		$yet = "yet"; //ค่าซ้ำ
		$_SESSION['add'] = '';

	} else if ($_SESSION['add'] == -1) {
		$error = "errors";
		$_SESSION['add'] = '';
	} else if ($_SESSION['add'] == -2) {
		$error_360 = "error_360";
		$_SESSION['add'] = '';
	} else if ($_SESSION['add'] == -3) {
		$error_vdo = "error_vdo";
		$_SESSION['add'] = '';
	} else if ($_SESSION['add'] == -4) {
		$error_youtube = "error_youtube";
		$_SESSION['add'] = '';
	} else if ($_SESSION['add'] == -5) {
		$error2 = "error";
		$_SESSION['add'] = '';
	}
}

?>
            <!DOCTYPE html>
            <html lang="en">
            <head>
              <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
              <!-- Meta, title, CSS, favicons, etc. -->
              <meta charset="utf-8">
              <meta http-equiv="X-UA-Compatible" content="IE=edge">
              <meta name="viewport" content="width=device-width, initial-scale=1">

              <title>LAND & HOUSES</title>

              <!-- Bootstrap -->
              <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
              <!-- Font Awesome -->
              <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
              <!-- NProgress -->
              <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
              <!-- iCheck -->
              <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
              <!-- bootstrap-progressbar -->
              <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
              <!-- Custom Theme Style -->
              <link href="../build/css/custom.min.css" rel="stylesheet">
              <!-- Fancybox image popup -->
              <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
              <!-- jQuery -->
              <script src="../vendors/jquery/dist/jquery.min.js"></script>

              <!-- uploade img -->
              <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
              <!--uploade-->
              <!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
              <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
              <!-- confirm-->
              <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
              <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>
              <style>
              .file-caption-main .btn-file {
                overflow: visible;
              }

              .file-caption-main .btn-file .error {
                position: absolute;
                bottom: -32px;
                right: 30px;
              }
              #seo_img_paln{
                display: none;
              }
              #seo_lead_img_file{
                display: none;
              }
              .btn-switch {
               font-size: 14px;
               position: relative;
               display: inline-block;
               -webkit-user-select: none;
               -moz-user-select: none;
               -ms-user-select: none;
               user-select: none;
             }
             .btn-switch__radio {
              display: none;
            }
            .btn-switch__label {
              display: inline-block;
              padding: .75em .5em .75em .75em;
              vertical-align: top;
              font-size: 1em;
              font-weight: 700;
              line-height: 1.5;
              color: #666;
              cursor: pointer;
              transition: color .2s ease-in-out;
            }
            .btn-switch__label + .btn-switch__label {
             padding-right: .75em;
             padding-left: 0;
           }
           .btn-switch__txt {
            font-size: 19px;
            position: relative;
            z-index: 2;
            display: inline-block;
            min-width: 1.5em;
            opacity: 1;
            pointer-events: none;
            transition: opacity .2s ease-in-out;
          }
          .btn-switch__radio_no:checked ~ .btn-switch__label_yes .btn-switch__txt,
          .btn-switch__radio_yes:checked ~ .btn-switch__label_no .btn-switch__txt {
            opacity: 0;
          }
          .btn-switch__label:before {
            content: "";
            position: absolute;
            z-index: -1;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background: #f0f0f0;
            border-radius: 1.5em;
            box-shadow: inset 0 .0715em .3572em rgba(43,43,43,.05);
            transition: background .2s ease-in-out;
          }
          .btn-switch__radio_yes:checked ~ .btn-switch__label:before {
            background: #1abc9c;
          }
          .btn-switch__label_no:after {
            content: "";
            position: absolute;
            z-index: 2;
            top: .5em;
            bottom: .5em;
            left: .5em;
            width: 2em;
            background: #fff;
            border-radius: 1em;
            pointer-events: none;
            box-shadow: 0 .1429em .2143em rgba(43,43,43,.2), 0 .3572em .3572em rgba(43,43,43,.1);
            transition: left .2s ease-in-out, background .2s ease-in-out;
          }
          .btn-switch__radio_yes:checked ~ .btn-switch__label_no:after {
            left: calc(100% - 2.5em);
            background: #fff;
          }
          .btn-switch__radio_no:checked ~ .btn-switch__label_yes:before,
          .btn-switch__radio_yes:checked ~ .btn-switch__label_no:before {
            z-index: 1;
          }
          .btn-switch__radio_yes:checked ~ .btn-switch__label_yes {
            color: #fff;
          }
        </style>

      </head>

      <body class="nav-md">
        <div class="container body">
          <div class="main_container">
            <div class="col-md-3 left_col">
              <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                  <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <?php include './master/navbar.php';?>
                <!-- /menu footer buttons -->
              </div>
            </div>

            <!-- top navigation -->
            <?php include './master/top_nav.php';?>
            <!-- /top navigation -->
            <?php

// $qr_id = $funBrabd->list_serie($id );
// $resulft = $qr_id->fetch( PDO::FETCH_ASSOC );
//print_r($resulft);

$sql = "SELECT * FROM LH_SERIES s LEFT JOIN LH_SERIES_IMG sm ON s.series_id=sm.series_id  WHERE s.series_id = '" . $id . "' ";
$query_result3 = mssql_query($sql, $db_conn);
$resulft = mssql_fetch_array($query_result3);
//print_r($row);
?>
            <!-- page content -->
            <div class="right_col" role="main">
              <div class="">
                <div class="clearfix"></div>
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2><a href="series.php">ระบบจัดการข้อมูล Series</a> > แก้ไขข้อมูล Series : <?=$resulft['series_name_th'];if ($resulft['series_name_en'] == '') {echo "";} else {echo " ( " . $resulft['series_name_en'] . ")";}?></h2>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <!-- <h4>Step1 : สร้างข้อมูล Series ได้ทั้งภาษาอังกฤษ และ ภาษาอังกฤษ(*ไม่บังคับ)</h4> -->
                        <p class="font-gray-dark">
                        </p><br>
                        <?php if (isset($success)) {?>
                        <div class="row">
                          <div class="col-md-2"></div>
                          <div class="form-group">
                            <div class="alert alert-success col-md-6" id="alert">
                              <button type="button" class="close" data-dismiss="alert">x</button>
                              <strong>แก้ไขข้อมูลเรียบร้อยแล้ว</strong>
                            </div>
                          </div>
                          <div class="col-md-6"></div>
                        </div>
                        <?php } else if (isset($yet)) {?>
                        <div class="row">
                          <div class="col-md-2"></div>
                          <div class="form-group">
                            <div class="alert alert-danger col-md-6" id="alert">
                              <button type="button" class="close" data-dismiss="alert">x</button>
                              <strong>มีชื่อนี้อยู่ในระบบแล้ว</strong>
                            </div>
                          </div>
                          <div class="col-md-4"></div>
                        </div>
                        <?php } else if (isset($error2)) {?>
                        <div class="row">
                          <div class="col-md-2"></div>
                          <div class="form-group">
                            <div class="alert alert-danger col-md-6" id="alert">
                              <button type="button" class="close" data-dismiss="alert">x</button>
                              <strong>ไม่สามารถลบได้ ! </strong>มีแบบบ้านที่ผูกกับ Series นี้อยู่
                            </div>
                          </div>
                          <div class="col-md-4"></div>
                        </div>
                        <?php } else if (isset($error_360)) {?>
                        <div class="row">
                          <div class="col-md-2"></div>
                          <div class="form-group">
                            <div class="alert alert-danger col-md-6" id="alert">
                              <button type="button" class="close" data-dismiss="alert">x</button>
                              <strong>เกิดข้อผิดพลาด !</strong> ใส่ข้อมูล 360 Virtual Tour ไม่ครบ
                            </div>
                          </div>
                          <div class="col-md-4"></div>
                        </div>
                        <?php } else if (isset($error_vdo)) {?>
                        <div class="row">
                          <div class="col-md-2"></div>
                          <div class="form-group">
                            <div class="alert alert-danger col-md-6" id="alert">
                              <button type="button" class="close" data-dismiss="alert">x</button>
                              <strong>เกิดข้อผิดพลาดเนื่องจากใส่ข้อมูล VDO File ไม่ครบ !</strong>
                            </div>
                          </div>
                          <div class="col-md-4"></div>
                        </div>
                        <?php } else if (isset($error_youtube)) {?>
                        <div class="row">
                          <div class="col-md-2"></div>
                          <div class="form-group">
                            <div class="alert alert-danger col-md-6" id="alert">
                              <button type="button" class="close" data-dismiss="alert">x</button>
                              <strong>เกิดข้อผิดพลาดเนื่องจากใส่ข้อมูล VDO Youtube ไม่ครบ !</strong>
                            </div>
                          </div>
                          <div class="col-md-4"></div>
                        </div>
                        <?php } else if (isset($error2)) {?>
                        <div class="row">
                          <div class="col-md-2"></div>
                          <div class="form-group">
                            <div class="alert alert-danger col-md-6" id="alert">
                              <button type="button" class="close" data-dismiss="alert">x</button>
                              <strong>ไม่สามารถลบได้ ! </strong>มีโครงการที่ผูกกับประเภทนี้อยู่
                            </div>
                          </div>
                          <div class="col-md-4"></div>
                        </div>
                        <?php }?>

                        <form class="form-horizontal form-label-left" action="update_series.php" method="post" enctype="multipart/form-data" id="commentForm">
                          <div class="form-group">
                            <label class="control-label col-md-2" for="first-name">ชื่อสไตล์บ้าน <span class="required">*</span>
                            </label>
                            <div class="col-md-6">
                              <input type="text" id="first-name2" required class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อ Series (อังกฤษ)" name="series_name_th" value="<?=$resulft['series_name_th']?>">
                            </div>
                          </div>
                          <div class="form-group hide-for-th">
                            <label class="control-label col-md-2" for="first-name">ชื่อสไตล์บ้าน <span class="required">*</span>
                            </label>
                            <div class="col-md-6">
                              <input type="text" id="first-name2"  class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อ Series (อังกฤษ)" name="series_name_en" value="<?php if ($resulft['series_name_en'] != ' ') {echo $resulft['series_name_en'];} else {echo '';}?>">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-2" for="first-name">รายละเอียดสไตล์บ้าน <span class="required">*</span>
                            </label>
                            <div class="col-md-6">
                              <textarea name="series_des_th" rows="4" class="form-control" placeholder="รายละเอียดสไตล์บ้าน (อังกฤษ)" ><?php if ($resulft['series_des_th'] != ' ') {echo $resulft['series_des_th'];} else {echo '';}?></textarea>
                            </div>
                          </div>
                          <div class="form-group hide-for-th">
                            <label class="control-label col-md-2" for="first-name">รายละเอียดสไตล์บ้าน <span class="required">*</span>
                            </label>
                            <div class="col-md-6">
                              <textarea name="series_des_en" rows="4" class="form-control" placeholder="รายละเอียดสไตล์บ้าน (อังกฤษ)" ><?php if ($resulft['series_des_en'] != ' ') {echo $resulft['series_des_en'];} else {echo '';}?></textarea>
                            </div>
                          </div>


                          <?php

$req = "*";
$required = "required";
?>
                          <?php if (isset($resulft['series_logo']) && ($resulft['series_logo'] != "")) {
	$req = "";
	$required = "";?>

                            <div class="form-group">
                              <label class="control-label col-md-2" for="first-name">โลโก้สไตล์บ้าน <br>(.png <br> ขนาด 150 x 150 px) <br> ภาพขนาดไม่เกิน 300 KB <span class="required"></span>
                              </label>
                              <div class="col-md-6">
                                <div id="map-image-holder-brochure"></div>

                                <div class="file-preview ">
                                  <div class="close fileinput-remove"></div>
                                  <div class="file-drop-disabled">
                                    <div class="file-preview-thumbnails">
                                      <div class="file-initial-thumbs">
                                        <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                          <div class="kv-file-content">
                                            <a class="fancybox" href="fileupload/images/series_img/series_img_logo/<?php echo $resulft['series_logo']; ?>">
                                              <img src="fileupload/images/series_img/series_img_logo/<?php echo $resulft['series_logo']; ?>" class="kv-preview-data file-preview-image"  style="width:150px;height:auto;">
                                            </a>
                                          </div>
                                          <div class="file-thumbnail-footer">
                                            <p>Alt text : <?=$resulft['series_logo_seo']?></p>
                                            <div class="file-actions">
                                              <div class="file-footer-buttons">
                                                <button type="button" id="<?php echo $resulft['series_id']; ?>" onclick="deleteLogoSeries(<?php echo $resulft['series_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                <a class="fancybox" href="fileupload/images/series_img/series_img_logo/<?=$resulft['series_logo']?>">
                                                  <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                </a>
                                              </div>
                                              <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                            </div>
                                          </div>

                                        </div>
                                      </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="file-preview-status text-center text-success"></div>
                                    <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <?php }?>
                            <div class="form-group">

                              <label class="control-label col-md-2" for="first-name">เลือกรูปโลโก้สไตล์บ้าน  <br>(.png <br> ขนาด 150 x 150 px) <br> ภาพขนาดไม่เกิน 300 KB <?php echo $req ?></label><span class="required"></span>

                              <div class="col-md-6">
                               <input type="file" id="logo_series" name="logo_series" <?php echo $required; ?> class="file-loading" accept="image/png">

                             </div>
                           </div>




                           <script>
                            $("#logo_series").fileinput({
                              uploadUrl: "upload.php", // server upload action
                              maxFileCount: 1,
                              allowedFileExtensions: ["png"],
                              browseLabel: 'เลือกรูป',
                              removeLabel: 'ลบ',
                              browseClass: 'btn btn-success',
                              showUpload: false,
                              showRemove:false,
                              showCaption: false,//ปิดช่องแสดงชื่อภาพ
                              msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                              msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                              msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                              msgZoomModalHeading: 'ตัวอย่างละเอียด',
                              xxx:'series_logo_seo',
                              dropZoneTitle : 'รูปโลโก้สไตล์บ้าน',
//                              minImageWidth: 150, //ขนาด กว้าง ต่ำสุด
//                              minImageHeight: 150, //ขนาด ศุง ต่ำสุด
//                              maxImageWidth: 150, //ขนาด กว้าง ต่ำสุด
//                              maxImageHeight: 150, //ขนาด ศุง ต่ำสุด
maxFileSize :300 ,
msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
});
</script>

<?php

$req = "*";
$required = "required";
?>
<?php if (isset($resulft['series_img_name']) && ($resulft['series_img_name'] != "")) {
	$req = "";
	$required = "";?>

  <div class="form-group">
    <label class="control-label col-md-2" for="first-name">ภาพตัวแทนสไตล์บ้าน <br>(.png <br> ขนาด 1920 x 1080 px) <br> ภาพขนาดไม่เกิน 300 KB <span class="required"></span>
    </label>
    <div class="col-md-6">
      <div id="map-image-holder-brochure"></div>
      <div class="file-preview ">
        <div class="close fileinput-remove"></div>
        <div class="file-drop-disabled">
          <div class="file-preview-thumbnails">
            <div class="file-initial-thumbs">
              <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                <div class="kv-file-content">
                  <a class="fancybox" href="fileupload/images/series_img/<?php echo $resulft['series_img_name']; ?>">
                    <img src="fileupload/images/series_img/<?php echo $resulft['series_img_name']; ?>" class="kv-preview-data file-preview-image"  style="width:150px;height:auto;">
                  </a>
                </div>
                <div class="file-thumbnail-footer">
                  <p>Alt text : <?=$resulft['series_img_seo']?></p>
                  <div class="file-actions">
                    <div class="file-footer-buttons">
                      <button type="button" id="<?php echo $resulft['series_id']; ?>" onclick="deleteImageSeries(<?php echo $resulft['series_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                      <a class="fancybox" href="fileupload/images/series_img/<?=$resulft['series_img_name']?>">
                        <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                      </a>
                    </div>
                    <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                  </div>
                </div>

              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="file-preview-status text-center text-success"></div>
          <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
        </div>
      </div>
    </div>
  </div>
  <?php }?>
  <div class="form-group">

    <label class="control-label col-md-2" for="first-name">เลือกภาพตัวแทนสไตล์บ้าน <br>(.png <br> ขนาด 1920 x 1080 px) <br> ภาพขนาดไม่เกิน 300 KB <?php echo $req ?></label><span class="required"></span>

    <div class="col-md-6">
     <input type="file" id="images" name="images" <?php echo $required; ?> class="file-loading" accept="image/*">

   </div>
 </div>

 <script>
  $("#images").fileinput({
//                              initialPreview: [
//                                  'http://devwww2.lh.co.th/devlh/Backend/fileupload/images/series_img/<?//=$resulft['series_img_name']?>//',
//                              ],
//                              initialPreviewAsData: true,
//                              initialPreviewConfig: [
//                                  {caption: "ทดสอบ", size: 1000, xxx: "zunyru" , width: "120px", key: 1},
//                              ],
//                              deleteUrl: "ajax_deleteImageSeries.php?series_id=" + 0,
//                              overwriteInitial: false,
                              uploadUrl: "upload.php", // server upload action
                              maxFileCount: 1,
                              allowedFileExtensions: ["png","jpg"],
                              browseLabel: 'เลือกภาพตัวแทนสไตล์บ้าน',
                              removeLabel: 'ลบ',
                              browseClass: 'btn btn-success',
                              showUpload: false,
                              showRemove:false,
                              showCaption: false,
                              msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                              msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                              msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                              msgZoomModalHeading: 'ตัวอย่างละเอียด',
                              xxx:'seo_img_series',
                              dropZoneTitle : 'รูปภาพตัวแทนสไตล์บ้าน',
                              minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                              minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                              maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                              maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                              maxFileSize :300 ,
                              msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                              msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                              msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                              msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                              msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                            });
                          </script>

                          <div class="form-group">
                            <label class="control-label col-md-2" for="first-name">URL 360 Virtual Tour
                            </label>
                            <div class="col-md-6">
                              <input type="text" id="c_360" name="c_360"  class="form-control col-md-7 col-xs-12" placeholder="Url 360 Virtual Tour" value="<?=$resulft['series_360']?>">
                            </div>
                          </div>


                          <?php if (isset($resulft['series_360_thumbnail']) && ($resulft['series_360_thumbnail'] != "")) {?>
                          <script type="text/javascript">
                            $(document).ready(function() {
                              $('#c_img').val("Y");
                            });
                          </script>

                          <div class="form-group">
                            <label class="control-label col-md-2">ภาพตัวอย่าง 360 Virtual Tour <br> ภาพขนาดไม่เกิน 300 KB </label>

                            <div class="col-md-6">
                              <div id="map-image-holder-brochure"></div>
                              <div class="file-preview ">
                                <div class="close fileinput-remove"></div>
                                <div class="file-drop-disabled">
                                  <div class="file-preview-thumbnails">
                                    <div class="file-initial-thumbs">
                                      <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                        <div class="kv-file-content">
                                          <a class="fancybox" href="<?=$resulft['series_360_thumbnail'];?>">
                                            <img src="<?=$resulft['series_360_thumbnail'];?>" class="kv-preview-data file-preview-image"   style="width:150px;height:auto;">
                                          </a>
                                        </div>
                                        <div class="file-thumbnail-footer">
                                          <div class="file-actions">
                                            <div class="file-footer-buttons">
                                              <button type="button" id="<?php echo $resulft['series_id']; ?>" onclick="delete360ThumbnailSeries(<?php echo $resulft['series_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                              <a class="fancybox" href="<?=$resulft['series_360_thumbnail']?>">
                                                <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                              </a>
                                            </div>
                                            <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="clearfix"></div>
                                  <div class="file-preview-status text-center text-success"></div>
                                  <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <?php }?>


                          <div class="form-group">

                            <label class="control-label col-md-2" for="first-name">เลือกภาพประกอบ 360 Virtual Tour <br>(.jpg , .png , .gif <br> ขนาด 1920 x 1080 px) <br> ภาพขนาดไม่เกิน 300 KB</label><span class="required"></span>

                            <div class="col-md-6">
                             <input type="file" id="img" name="c_360_img[]" class="form-control col-md-7 col-xs-12"  value="" accept="image/*">
                             <input type="hidden" id="c_img" value="">

                           </div>
                         </div>

                         <script>
                          $("#img").fileinput({
                              uploadUrl: "upload.php", // server upload action
                              maxFileCount: 1,
                              allowedFileExtensions: ["jpg", "gif", "png", "jpeg"],
                              browseLabel: 'เลือก 360 ',
                              removeLabel: 'ลบ',
                              browseClass: 'btn btn-success',
                              showUpload: false,
                              showRemove:false,
                              showCaption: false,//ปิดช่องแสดงชื่อภาพ
                              msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                              msgFilesTooMany: 'ไฟล์ที่คุณเลือกมีจำนวน <b>({n})</b> ซึ่งเกินกว่าที่ระบบอนุญาตที่ <b>{m}</b>, กรุณาลองใหม่อีกครั้ง!',
                              msgZoomModalHeading: 'ตัวอย่างละเอียด',
                              xxx:'seo_img_paln',
                              dropZoneTitle : 'รูปภาพ Cover Image 360',
                              minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                              minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                              maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                              maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                              maxFileSize :300 ,
                              msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                              msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                              msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                              msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                              msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                            });
                          </script>

                          <?php
$sql_v = "SELECT * FROM LH_SERIES WHERE series_id = '" . $_GET['id'] . "'";
$query = mssql_query($sql_v);
$row_v = mssql_fetch_array($query);

$checked = '';
$checked_2 = '';
//echo $row_c['tvc_type'];
$checked = '';
$checked_2 = '';
if ($row_v['series_youtube_type'] == "youtube") {
	$checked = 'checked';
	$type = 'show();';
	$type2 = 'hide();';
} else if ($row_v['series_youtube_type'] == "vdo") {
	$checked_2 = 'checked';
	$type = 'hide();';
	$type2 = 'show();';
} else {
	$checked = 'checked';
	$type = 'show();';
	$type2 = 'hide();';
}
?>

                          <div class="form-group" id="inline_content">
                            <label class="control-label col-md-2" name="tvc_detail" for="first-name">เลือกชนิดการอัปไฟล์ <span class="required"></span>
                            </label>
                            <div class="col-md-6">
                              <div class="radio">
                                <label><input type="radio" id="youtube_" name="optradio_vdo" <?=$checked;?> value="youtube" class="flat"> VDO Youtube</label>
                              </div>
                              <div class="radio">
                                <label><input type="radio" id="file_" name="optradio_vdo" <?=$checked_2;?> value="vdo" class="flat"> VDO File</label>
                              </div>
                            </div>
                          </div>

                          <div id="v1">
                            <div class="form-group">
                              <label class="control-label col-md-2" for="first-name">URL Youtube <?php echo $req ?> <span class="required"> </span>
                              </label>
                              <div class="col-md-6">
                                <div class="control-group">
                                  <input type="text"  id="url_youtube" name="url_youtube" class="form-control col-md-7 col-xs-12" <?php echo $required; ?> placeholder="URL Youtube" value="<?php if ($row_v['series_youtube_type'] == 'youtube') {echo $row_v['series_youtube'];}?>">
                                </div>
                              </div>
                            </div>


                            <?php if ($row_v['series_youtube_type'] == 'youtube') {

	if (isset($row_v['series_youtube_thumbnail']) && ($row_v['series_youtube_thumbnail'] != "")) {
		$req = "";
		$required = "";

		?>
                               <script type="text/javascript">
                                $(document).ready(function() {
                                  $('#youtube_img').val("Y");
                                });
                              </script>
                              <div class="form-group">
                                <label class="control-label col-md-2" for="first-name">Thumbnail Youtube <br>(.jpg , .png , .gif <br> ขนาด 1920 x 1080 px) <br> ภาพขนาดไม่เกิน 300 KB<span class="required"></span>
                                </label>
                                <div class="col-md-6">
                                  <div id="map-image-holder-brochure"></div>


                                  <div class="file-preview ">
                                    <div class="close fileinput-remove"></div>
                                    <div class="file-drop-disabled">
                                      <div class="file-preview-thumbnails">
                                        <div class="file-initial-thumbs">
                                          <div class="file-preview-frame file-preview-initial" id="logo"
                                          data-fileindex="init_0" data-template="image">
                                          <div class="kv-file-content">
                                            <a class="fancybox" href="<?=$row_v['series_youtube_thumbnail']?>">
                                              <img src="<?=$row_v['series_youtube_thumbnail']?>"
                                              class="kv-preview-data file-preview-image"
                                              style="width:150px;height:auto;">
                                            </a>
                                          </div>
                                          <div class="file-thumbnail-footer">
                                            <div class="file-actions">
                                              <div class="file-footer-buttons">
                                                <button type="button" id="<?php echo $row_v['series_id']; ?>" onclick="deleteYoutubeThumbnailSeries(<?php echo $row_v['series_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                <a class="fancybox" href="<?=$row_v['series_youtube_thumbnail']?>">
                                                  <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                                </a>
                                              </div>
                                              <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                            </div>
                                          </div>

                                        </div>
                                      </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="file-preview-status text-center text-success"></div>
                                    <div class="kv-fileinput-error file-error-message"
                                    style="display: none;"></div>
                                  </div>
                                </div>


                              </div>
                            </div>
                            <?php }?>
                            <?php }?>
                            <div class="form-group">


                              <label class="control-label col-md-2" for="first-name" style="padding-left: 0px">เลือกภาพ Thumbnail Youtube <?php echo $req ?> <br>(.jpg , .png ,.gif <br> ขนาด 1920 x 1080 px) <br> ภาพขนาดไม่เกิน 300 KB </label><span class="required"></span>

                              <div class="col-md-6">
                               <input type="file" id="img_youtube" name="img_youtube" class="form-control col-md-7 col-xs-12"  <?php echo $required; ?> placeholder="URL Youtube" value=""  accept="image/*">
                               <input type="hidden" id="youtube_img" value="">

                             </div>
                           </div>



                           <script>
                            $("#img_youtube").fileinput({
                              uploadUrl: "upload.php", // server upload action
                              maxFileCount: 1,
                              allowedFileExtensions: ["jpg", "png", "jpeg"],
                              browseLabel: 'เลือกรูป',
                              removeLabel: 'ลบ',
                              browseClass: 'btn btn-success',
                              showUpload: false,
                              showRemove:false,
                              msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                              msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                              msgZoomModalHeading: 'ตัวอย่างละเอียด',
                              xxx:'seo_lead_img_file',
                              dropZoneTitle : 'รูปภาพ Thumbnail Youtube',
                              minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                              minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                              maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                              maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                              maxFileSize :300 ,
                              msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                              msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                              msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                              msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                              msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                            });
                          </script>
                        </div>


                        <div id="v2">


                          <?php if ($row_v['series_youtube_type'] == 'vdo') {

	if (isset($row_v['series_youtube_thumbnail']) && ($row_v['series_youtube_thumbnail'] != "")) {
		$req = "";
		$required = "";?>

                              <script type="text/javascript">
                                $(document).ready(function() {
                                  $('#vdo_thumbnail').val("Y");
                                });
                              </script>
                              <div class="form-group">
                                <label class="control-label col-md-2" for="first-name">Thumbnail VDO <br>(.jpg , .png , .gif <br> ขนาด 1920 x 1080 px) <br> ภาพขนาดไม่เกิน 300 KB <span class="required"> </span>
                                </label>
                                <div class="col-md-6">
                                  <div id="map-image-holder-brochure"></div>
                                  <div class="file-preview ">
                                    <div class="close fileinput-remove"></div>
                                    <div class="file-drop-disabled">
                                      <div class="file-preview-thumbnails">
                                        <div class="file-initial-thumbs">
                                          <div class="file-preview-frame file-preview-initial" id="logo"
                                          data-fileindex="init_0" data-template="image">
                                          <div class="kv-file-content">
                                            <a class="fancybox"
                                            href="<?=$row_v['series_youtube_thumbnail']?>">
                                            <img src="<?=$row_v['series_youtube_thumbnail']?>"
                                            class="kv-preview-data file-preview-image"
                                            style="width:150px;height:auto;"
                                            >
                                          </a>
                                        </div>
                                        <div class="file-thumbnail-footer">
                                          <div class="file-actions">
                                            <div class="file-footer-buttons">
                                              <button type="button" id="<?php echo $row_v['series_id']; ?>" onclick="deleteYoutubeThumbnailSeries(<?php echo $row_v['series_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                              <a class="fancybox" href="<?=$row_v['series_youtube_thumbnail']?>">
                                                <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                              </a>
                                            </div>
                                            <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                          </div>
                                        </div>

                                      </div>
                                    </div>
                                  </div>
                                  <div class="clearfix"></div>
                                  <div class="file-preview-status text-center text-success"></div>
                                  <div class="kv-fileinput-error file-error-message"
                                  style="display: none;"></div>
                                </div>
                              </div>


                            </div>
                          </div>
                          <?php }
}?>
                        <div class="form-group">
                          <label class="control-label col-md-2" for="first-name" style="padding-left: 0px">เลือกภาพ Thumbnail VDO <?php echo $req ?><br>(.jpg , .png , .gif <br> ขนาด 1920 x 1080 px) <br> ภาพขนาดไม่เกิน 300 KB </label><span class="required"></span>

                          <div class="col-md-6">
                           <input type="file" id="thumbnail_vdo" name="thumbnail_vdo" <?php echo $required; ?> class="form-control col-md-7 col-xs-12"  placeholder="URL Youtube" value=""  accept="image/*">
                           <input type="hidden" id="vdo_thumbnail" value="">

                         </div>
                       </div>

                       <script>
                        $("#thumbnail_vdo").fileinput({
                                  uploadUrl: "upload.php", // server upload action
                                  maxFileCount: 1,
                                  allowedFileExtensions: ["jpg", "png", "jpeg"],
                                  browseLabel: 'เลือกรูป',
                                  removeLabel: 'ลบ',
                                  browseClass: 'btn btn-success',
                                  showUpload: false,
                                  showRemove:false,
                                  showCaption: false,
                                  msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                  msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                  msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                  xxx:'seo_lead_img_file',
                                  dropZoneTitle : 'รูปภาพ Thumbnail VDO',
                                  minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                  minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                  maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                  maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                  maxFileSize :300 ,
                                  msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                  msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                  msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                  msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                  msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                });
                              </script>

                              <?php
if ($row_v['series_youtube_type'] == 'vdo') {
	if (isset($row_v['series_youtube']) && ($row_v['series_youtube'] != "")) {
		$req = "";
		$required = "";?>

                                 <script type="text/javascript">
                                  $(document).ready(function() {
                                    $('#file_vdo').val("Y");
                                  });
                                </script>

                                <div class="form-group">
                                  <label class="control-label col-md-2" for="first-name"> VDO <br> (เฉพาะไฟล์ .mp4 ) <span class="required"> </span>
                                  </label>
                                  <div class="col-md-6">
                                    <div id="map-image-holder-brochure"></div>

                                    <div class="file-preview ">
                                      <div class="close fileinput-remove"></div>
                                      <div class="file-drop-disabled">
                                        <div class="file-preview-thumbnails">
                                          <div class="file-initial-thumbs">
                                            <div class="file-preview-frame file-preview-initial" id="logo"
                                            data-fileindex="init_0" data-template="image">
                                            <div class="kv-file-content">
                                              <video width="400" controls>
                                                <source src="<?=$row_v['series_youtube']?>" type="video/mp4">
                                                </video>
                                              </div>
                                              <div class="file-thumbnail-footer">
                                                <div class="file-actions">
                                                  <div class="file-footer-buttons">
                                                    <button type="button" id="<?php echo $row_v['series_id']; ?>" onclick="deleteVdoSeries(<?php echo $row_v['series_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                                  </div>
                                                  <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                                </div>
                                              </div>

                                            </div>
                                          </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="file-preview-status text-center text-success"></div>
                                        <div class="kv-fileinput-error file-error-message"
                                        style="display: none;"></div>
                                      </div>
                                    </div>


                                  </div>
                                </div>
                                <?php }
}?>
                              <div class="form-group">
                                <label class="control-label col-md-2" for="first-name" style="padding-left: 0px">เลือก VDO <?php echo $req ?><br>(.mp4) </label><span class="required"></span>

                                <div class="col-md-6">
                                 <input type="file" id="vdo_file" name="vdo" <?php echo $required; ?> class="form-control col-md-7 col-xs-12"   value="" accept="video/mp4">
                                 <input type="hidden" id="file_vdo" value="">

                               </div>
                             </div>

                             <script>
                              $("#vdo_file").fileinput({
                                  uploadUrl: "upload.php", // server upload action
                                  maxFileCount: 1,
                                  allowedFileExtensions: ["mp4"],
                                  browseLabel: 'เลือกไฟล์',
                                  removeLabel: 'ลบ',
                                  browseClass: 'btn btn-success',
                                  showUpload: false,
                                  showRemove:false,
                                  showCaption: false,
                                  msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                  msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                  msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                  xxx:'seo_lead_img_file',
                                  dropZoneTitle : 'VDO File',

                                  msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                  msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีขนาด {size} px.',
                                  msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                                  msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกินขนาด {size} พิกเซล.',
                                });
                              </script>
                            </div>

                             <br>

                             <div class="form-group">
                               <label class="control-label col-md-2">แสดงบนเว็บ
                               </label>
                               <div class="col-md-4">
                                <p class="btn-switch">
                                  <input type="radio" <?=$resulft['views'] == 'on' ? 'checked' : '';?> id="yes" name="switch" value="on" class="btn-switch__radio btn-switch__radio_yes" />
                                  <input type="radio" <?=$resulft['views'] == 'off' ? 'checked' : '';?> id="no" value="off" name="switch" class="btn-switch__radio btn-switch__radio_no" />
                                  <label for="yes" class="btn-switch__label btn-switch__label_yes"><span class="btn-switch__txt">เปิด</span></label>                  <label for="no" class="btn-switch__label btn-switch__label_no"><span class="btn-switch__txt">ปิด</span></label>
                                </p>
                              </div>
                            </div>


                            <center><br>
                              <div class="col-md-8">
                                <input name="id" type="hidden" value="<?=$_GET['id']?>">
                                <button type="submit" name="submit"  class="btn btn-success">Update</button>
                                <!-- <a href="series_update.php?delete=1&id=<?=$id?>"><button type="button" name="delete" class="btn btn-danger">Delete</button> -->
                                  <a class="confirms" data-title="ยืนยันการลบข้อมูล" name="delete" href="delete_series.php?id=<?=$id?>">
                                    <button type="button" class="btn btn-danger">Delete</button>
                                  </a>
                                </div>
                              </center>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                    <script type="text/javascript">
                      $('a.confirms').confirm({
                        content: 'คุณแน่ใจที่จะลบข้อมูลนี้ !',
                        buttons: {
                          Yes: {
                            text: 'Yes',
                            btnClass: 'btn-danger',
                            keys: ['enter', 'a'],
                            action: function(){
                              location.href = this.$target.attr('href');
                            }
                          },
                          No: {
                            text: 'No',
                            btnClass: 'btn-default',
                            keys: ['enter', 'a'],
                            action: function(){
                          // button action.
                        }
                      },

                    }
                  });
                </script>
                <!-- /page content -->

                <!-- footer content -->
        <!-- <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer> -->
        <!-- /footer content -->
      </div>
    </div>

    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="js/bootstrap-imgupload.js"></script>
    <!-- Validate -->
    <script src="../build/js/jquery.validate.js"></script>
    <!-- Fancybox image popup -->
    <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>
    <!-- Custom Theme Scripts -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

    <script type="text/javascript">
      $('.fancybox').fancybox();
    </script>

    <script>
      $(document).ready(function() {
        $("#commentForm").validate({
          rules: {
            series_name_th: "required",
            series_name_en: "required",
            series_des_th: "required",
            series_des_en: "required",
//                        c_360 : {
//                            required: function(element){
//                                return $("#c_360").val()!="aaa";
//                            }
//                        },
//                        c_360 : {
//                            required :depends: function(){
//                                return  $('#c_img').src.length > 0;
//                        },
//                        logo_series : "required",
//                        images : "required",
},
messages: {
  series_name_th: "กรอกชื่อ series อังกฤษ !",
  series_name_en: "กรอกชื่อ series อังกฤษ !",
  series_des_th: "กรอกรายละเอียดสไตล์บ้าน อังกฤษ !",
  series_des_en: "กรอกรายละเอียดสไตล์บ้าน อังกฤษ !",
  logo_series :" &nbsp; ไม่มีรูป !",
  images : " &nbsp; ไม่มีรูป !",
  img_youtube : " &nbsp; ไม่มีรูป !",
  thumbnail_vdo : " &nbsp; ไม่มีรูป !",
  vdo : "&nbsp; ไม่มี Vdo !",
  url_youtube : "กรอก Url Youtube !",
  img : " &nbsp; ไม่มีรูป !",
  'c_360_img[]' : " &nbsp; ไม่มีรูป !",
  c_360 : "กรอก Url 360 Virtual Tour !",

}
});
//                $('#myform').validate({
//                    rules: {
//                            required: $('#c_img').each(function () {
//                                if (this.src.length > 0) {
//
//                                }
//                            });
//                    }
//                });

});
</script>
<!-- Custom Theme Scripts -->
<script>
  $(document).ready(function(){
    $("#alert").show();
    $("#alert").fadeTo(6000, 500).slideUp(500, function(){
      $("#alert").alert('close');
    });
  });
</script>

<script>
  $(document).ready(function(){
    $("#v1").<?=$type?>
    $("#v2").<?=$type2?>
  });

  $("#youtube_").on('ifChecked', function(event){
    $("#v1").show();
    $("#v2").hide();
  });
  $("#file_").on('ifChecked', function(event){
    $("#v1").hide();
    $("#v2").show();
  });

</script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#optradio_vdo").val();
    if($("#optradio_vdo").val() == "youtube"){
      $("#v1").show();
      $("#v2").hide();
    }else if($("#optradio_vdo").val() == "vdo"){
      $("#v2").show();
      $("#v1").hide();
    }
  });
</script>
<script type="text/javascript">
  $("#commentForm").submit(function() {
    var c_360 = $("#c_360").val();
    var img = $("#img").val();
    var c_img = $("#c_img").val();
    if(c_360 != "" && img == "" ){
      if(c_img != ""){
       $("#img").removeAttr('required','true');
     }else if (c_img == ""){
      $("#img").attr('required','true');
    }
  }else if(img != "" && c_360 =="" ){
    $("#c_360").attr('required','true');
  }else if(c_360 == "" && img == ""){
    if(c_img != "" ){
      $("#c_360").attr('required','true');
    }else if(c_img == ""){
      $("#img").removeAttr('required','true');
      $("#c_360").removeAttr('required','true');
    }
  }
});
</script>
<script type="text/javascript">
  $("#commentForm").submit(function() {
    var url_youtube = $("#url_youtube").val();
    var img_youtube = $("#img_youtube").val();
    var youtube_img = $("#youtube_img").val();
    if(url_youtube != "" && img_youtube == "" ){
      if(youtube_img != ""){
       $("#img_youtube").removeAttr('required','true');
     }else if (youtube_img == ""){
      $("#img_youtube").attr('required','true');
    }
  }else if(img_youtube != "" && url_youtube =="" ){
    $("#url_youtube").attr('required','true');
  }else if(url_youtube == "" && img_youtube == ""){
    if(youtube_img != "" ){
      $("#url_youtube").attr('required','true');
    }else if(youtube_img == ""){
      $("#img_youtube").removeAttr('required','true');
      $("#url_youtube").removeAttr('required','true');
    }
  }
});
</script>

<script type="text/javascript">
  $("#commentForm").submit(function() {
    var thumbnail_vdo = $("#thumbnail_vdo").val();
    var vdo_file = $("#vdo_file").val();
    var vdo_thumbnail = $("#vdo_thumbnail").val();
    var file_vdo = $("#file_vdo").val();
    if(thumbnail_vdo != "" && vdo_file == "" ){
      if(file_vdo != ""){
       $("#vdo_file").removeAttr('required','true');
     }else if (file_vdo == ""){
      $("#vdo_file").attr('required','true');
    }
  }else if(vdo_file != "" && thumbnail_vdo =="" ){
    if(vdo_thumbnail != ""){
      $("#thumbnail_vdo").removeAttr('required','true');
    }else if(vdo_thumbnail == ""){
      $("#thumbnail_vdo").attr('required','true');
    }
  }else if(thumbnail_vdo == "" && vdo_file == ""){
    if(vdo_thumbnail != "" && file_vdo == "" ){
      $("#vdo_file").attr('required','true');
    }else if(vdo_thumbnail == "" && file_vdo != ""){
      $("#thumbnail_vdo").attr('required','true');
    }else if(vdo_thumbnail == "" && file_vdo == "" ){
      $("#thumbnail_vdo").removeAttr('required','true');
      $("#vdo_file").removeAttr('required','true');
    }else if(vdo_thumbnail != "" && file_vdo != "" ){
      $("#thumbnail_vdo").removeAttr('required','true');
      $("#vdo_file").removeAttr('required','true');
    }
  }
});
</script>

<script>
  function deleteLogoSeries(id) {
    $.confirm({
      title: 'ลบรูปนี้ !',
      content: 'คุณแน่ใจที่จะลบรูปนี้!',
      buttons : {
        Yes: {
          text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                              var self = this;
                              return $.ajax({
                                url: 'ajax_deleteLogoSeries.php?series_id=' + id,
                                dataType: 'json',
                                method: 'get'
                              }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                  }).fail(function () {
                                    self.setContent('Something went wrong.');
                                  });
                                }
                              },
                              No: function () {
                            //$.alert('ยกลเิก!');
                          },
                        }
                      });
  }
</script>
<script>
  function deleteImageSeries(id) {
    $.confirm({
      title: 'ลบรูปนี้ !',
      content: 'คุณแน่ใจที่จะลบรูปนี้!',
      buttons : {
        Yes: {
          text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                              var self = this;
                              return $.ajax({
                                url: 'ajax_deleteImageSeries.php?series_id=' + id,
                                dataType: 'json',
                                method: 'get'
                              }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                  }).fail(function () {
                                    self.setContent('Something went wrong.');
                                  });
                                }
                              },
                              No: function () {
                            //$.alert('ยกลเิก!');
                          },
                        }
                      });
  }
</script>
<script>
  function deleteYoutubeThumbnailSeries(id) {
    $.confirm({
      title: 'ลบรูปนี้ !',
      content: 'คุณแน่ใจที่จะลบรูปนี้!',
      buttons : {
        Yes: {
          text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                              var self = this;
                              return $.ajax({
                                url: 'ajax_deleteYoutubeThumbnailSeries.php?series_id=' + id,
                                dataType: 'json',
                                method: 'get'
                              }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                  }).fail(function () {
                                    self.setContent('Something went wrong.');
                                  });
                                }
                              },
                              No: function () {
                            //$.alert('ยกลเิก!');
                          },
                        }
                      });
  }
</script>
<script>
  function delete360ThumbnailSeries(id) {
    $.confirm({
      title: 'ลบรูปนี้ !',
      content: 'คุณแน่ใจที่จะลบรูปนี้!',
      buttons : {
        Yes: {
          text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                              var self = this;
                              return $.ajax({
                                url: 'ajax_delete360ThumbnailSeries.php?series_id=' + id,
                                dataType: 'json',
                                method: 'get'
                              }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                  }).fail(function () {
                                    self.setContent('Something went wrong.');
                                  });
                                }
                              },
                              No: function () {
                            //$.alert('ยกลเิก!');
                          },
                        }
                      });
  }
</script>
<script>
  function deleteVdoSeries(id) {
    $.confirm({
      title: 'ลบรูปนี้ !',
      content: 'คุณแน่ใจที่จะลบรูปนี้!',
      buttons : {
        Yes: {
          text: 'Yes',
            btnClass: 'btn-red any-other-class', // multiple classes.
            action: function () {
              var self = this;
              return $.ajax({
                url: 'ajax_deleteVdoSeries.php?series_id=' + id,
                dataType: 'json',
                method: 'get'
              }).done(function () {
                    //$.alert('Confirmed!');
                    location.reload();
                  }).fail(function () {
                    self.setContent('Something went wrong.');
                  });
                }
              },
              No: function () {
            //$.alert('ยกลเิก!');
          },
        }
      });
  }
</script>
<script src="js/validate_file_300kb.js"></script>



</body>
</html>