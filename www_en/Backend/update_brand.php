<?php
session_start();
include './include/dbCon_mssql.php';
include './include/UpdateSlug.php';

function mssql_escape($str)
{
 if(get_magic_quotes_gpc())
 {
     $str= stripslashes($str);
 }
 return str_replace("'", "''", $str);
}

date_default_timezone_set('Asia/Bangkok');
$date       = date("Ymd");
$numrand_th = (mt_rand());
$numrand_en = (mt_rand());

$brand_name_th = mssql_escape($_POST['brand_name_th']);
$brand_name_en = mssql_escape($_POST['brand_name_en']);

$concept_th = mssql_escape($_POST['concept_th']);
$message_th = mssql_escape($_POST['message_th']);
$concept_en = mssql_escape($_POST['concept_en']);
$message_en = mssql_escape($_POST['message_en']);
$brand_id   = $_POST['id'];

$fileupload_th = $_FILES['fileupload_th']['name'];
$fileupload_en = $_FILES['fileupload_en']['name'];

// print_r($fileupload_th);
$query        = "SELECT COUNT(*) AS counts FROM dbo.LH_BRANDS WHERE ((brand_name_th = '$brand_name_th') OR (brand_name_en = '$brand_name_en')) AND NOT brand_id = '$brand_id'";
$query_result = mssql_query($query, $db_conn);

$count = mssql_fetch_row($query_result);
$count[0];
if ($count[0] >= 1) {
    $_SESSION['add'] = '2';
    echo '<script>window.history.go(-1);</script>';
    exit();
}

if(isset($_POST['brand_seo_th_edit'])){
    $sql    = "UPDATE LH_BRANDS SET  brand_img_seo_th = '".$_POST['brand_seo_th_edit']."' WHERE brand_id = '$brand_id' ";
    $result = mssql_query($sql);
}

if(isset($_POST['brand_seo_en_edit'])){
    $sql    = "UPDATE LH_BRANDS SET  brand_img_seo_en = '".$_POST['brand_seo_en_edit']."' WHERE brand_id = '$brand_id' ";
    $result = mssql_query($sql);
}

if ($fileupload_th != '') {
    //not select file
    //โฟลเดอร์ที่จะ upload file เข้าไป
    $path   = "fileupload/images/brand_img/";
    $seo_th = $_POST['brand_img_seo_th'][0];

    //เอาชื่อไฟล์เก่าออกให้เหลือแต่นามสกุล
    $type_th = strrchr($_FILES['fileupload_th']['name'], ".");

    //ตั้งชื่อไฟล์ใหม่โดยเอาเวลาไว้หน้าชื่อไฟล์เดิม
    $newname_th   = $date . $numrand_th . $type_th;
    $path_copy_th = $path . $newname_th;
    $path_lin_th  = "fileupload/images/brand_img/" . $newname_th;

    if (move_uploaded_file($_FILES['fileupload_th']['tmp_name'], $path_copy_th)) {
        //echo "Copy/Upload Complete b1";
        $query2        = "SELECT logo_brand_th  FROM dbo.LH_BRANDS WHERE brand_id = '$brand_id' ";
        $query_result2 = mssql_query($query2, $db_conn);
        $line2         = mssql_fetch_array($query_result2, MSSQL_ASSOC);

        $newname_th2 = $line2['logo_brand_th'];
        $flgDelete   = unlink($path . $newname_th2);

        $sql    = "UPDATE LH_BRANDS SET logo_brand_th = '$path_lin_th', brand_img_seo_th = '$seo_th' WHERE brand_id = '$brand_id' ";
        $result = mssql_query($sql);
    }

} else {
    $query2        = "SELECT logo_brand_th  FROM dbo.LH_BRANDS WHERE brand_id = '$brand_id' ";
    $query_result2 = mssql_query($query2, $db_conn);
    $line2         = mssql_fetch_array($query_result2, MSSQL_ASSOC);

    $newname_th = $line2['logo_brand_th'];
    //$flgDelete = unlink($path.$newname_th);
}

if ($fileupload_en != '') {
    //not select file
    //โฟลเดอร์ที่จะ upload file เข้าไป
    $path   = "fileupload/images/brand_img/";
    $seo_en = $_POST['brand_img_seo_en'][0];

    //เอาชื่อไฟล์เก่าออกให้เหลือแต่นามสกุล
    $type_en = strrchr($_FILES['fileupload_en']['name'], ".");

    $newname_en   = $date . $numrand_en . $type_en;
    $path_copy_en = $path . $newname_en;
    $path_link_en = "fileupload/images/brand_img/" . $newname_en;

    if (move_uploaded_file($_FILES['fileupload_en']['tmp_name'], $path_copy_en)) {
        //echo "Copy/Upload Complete b2";
        $query3        = "SELECT logo_brand_en FROM dbo.LH_BRANDS WHERE brand_id = '$brand_id' ";
        $query_result3 = mssql_query($query3, $db_conn);
        $line3         = mssql_fetch_array($query_result3, MSSQL_ASSOC);
        //echo "b2";
        $newname_en3 = $line3['logo_brand_en'];
        $flgDelete   = unlink($path . $newname_en3);
        $dates = date("Y-m-d H:i:s");

        $sql    = "UPDATE LH_BRANDS SET logo_brand_en = '$path_link_en',brand_update = '$dates', brand_img_seo_en = '$seo_en' WHERE brand_id = '$brand_id' ";
        $result = mssql_query($sql);

    }
    //echo "b1";

} else {
    $query3        = "SELECT logo_brand_en FROM dbo.LH_BRANDS WHERE brand_id = '$brand_id' ";
    $query_result3 = mssql_query($query3, $db_conn);
    $line3         = mssql_fetch_array($query_result3, MSSQL_ASSOC);
    //echo "b2";
    $newname_en = $line3['logo_brand_en'];
    //$flgDelete = unlink($path.$newname_en);
}

if ($brand_name_th != '') {
    $dates        = date("Y-m-d H:i:s");
    $query        = "SELECT COUNT(*) AS counts  FROM dbo.LH_BRANDS WHERE brand_name_th = '$brand_name_th' AND brand_name_en = '$brand_name_en'";
    $query_result = mssql_query($query, $db_conn);

    $count = mssql_fetch_row($query_result);
    //echo $count[0];

    echo "<br>" . $newname_en;
    $query4 = "UPDATE LH_BRANDS SET brand_name_th='$brand_name_th',brand_name_en='$brand_name_en',brand_concep_th = '$concept_th',brand_concep_en='$concept_en',message_th='$message_th',message_en='$message_en', "
        . "logo_brand_th='$newname_th'"
        . ",logo_brand_en='$newname_en',"
        . "brand_update = '$dates' WHERE brand_id='$brand_id'";

    $query_result4 = mssql_query($query4, $db_conn);
    //update slug
    UpdateSlug::run(['brand']);
    if ($query_result4) {
        $_SESSION['add'] = '1';
        header("location:brand_update.php?id=$brand_id&add=1");
    }

} else {
    $_SESSION['add'] = '-1';
    header("location:brand_add.php?add=-1");
}
