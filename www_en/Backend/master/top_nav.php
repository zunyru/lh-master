<?php if(!isset($_SESSION['group_status'])){ echo '<script type="text/javascript">window.location.replace("'.$url_master.'www_en/Backend/login.php");</script>'; exit(); };?>
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>
        </nav>
        <ul class="nav navbar-nav navbar-right">
            <li class="">
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <?php if($_SESSION['group_status']=='Admin'){?>
                        <img src="images/img.jpg" alt="">Admin
                    <?php }else if($_SESSION['group_status']=='sell'){?>
                        <img src="images/picture.jpg" alt="">Seller
                    <?php }else if($_SESSION['group_status']=='job'){?>
                        <img src="images/hr.png" alt="">Job
                    <?php }?>
                    <span class=" fa fa-angle-down"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="logout.php" style="font-size: 20px;"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                </ul>
            </li>

        </ul>
    </div>
</div>