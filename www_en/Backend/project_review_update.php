<?php
session_start();
include './include/dbCon_mssql.php';
$group_id = $_SESSION['group_id'];

$_GET['page'] = 'managment';
$id = $_GET['id'];

if (isset($_SESSION['add'])) {
	if ($_SESSION['add'] == 1) {
		//header("location:product_add.php");
		$success = "success";
		unset($_SESSION["add"]);
	} else if ($_SESSION['add'] == 2) {

		$error = "error"; //ค่าซ้ำ
		unset($_SESSION["add"]);

	} else if ($_SESSION['add'] == 3) {

		$date_yum = "error"; //ค่าซ้ำ
		$_SESSION['add'] = '';

	} else if ($_SESSION['add'] == -1) {
		$errors = "errors";
		unset($_SESSION["add"]);
	}
}
?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <!-- Meta, title, CSS, favicons, etc. -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>LAND & HOUSES</title>

      <!-- Bootstrap -->
      <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome -->
      <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <!-- NProgress -->
      <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
      <!-- iCheck -->
      <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
      <!-- Custom Theme Style -->
      <link href="../build/css/custom.min.css" rel="stylesheet">
      <!-- Fancybox image popup -->
      <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
      <!-- bootstrap-progressbar -->
      <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
      <!-- jQuery -->
      <script src="../vendors/jquery/dist/jquery.min.js"></script>

      <!-- uploade img -->
      <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
      <!--uploade-->
      <!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
      <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
      <!-- confirm-->
      <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
      <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>

      <!-- Include Editor style. -->
      <link href="editor/css/froala_editor.min.css" rel="stylesheet" type="text/css" />
      <link href="editor/css/froala_style.min.css" rel="stylesheet" type="text/css" />

      <!--  froala  -->
      <link href="libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" href="libs/codemirror/5.3.0/codemirror.min.css">
      <link href="libs/froala-editor/2.6.2/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
      <link href="libs/froala-editor/2.6.2/css/froala_style.min.css" rel="stylesheet" type="text/css" />

      <!-- Include Editor Plugins style. -->
      <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/char_counter.css">
      <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/code_view.css">
      <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/colors.css">
      <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/emoticons.css">
      <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/file.css">
      <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/fullscreen.css">
      <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/image.css">
      <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/image_manager.css">
      <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/line_breaker.css">
      <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/quick_insert.css">
      <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/table.css">
      <link rel="stylesheet" href="libs/froala-editor/2.6.2/css/plugins/video.css">
      <!-- Include JS file. -->

      <script id="fr-fek">try{(function (k){localStorage.FEK=k;t=document.getElementById('fr-fek');t.parentNode.removeChild(t);})('inkH3fiA6B-13a==')}catch(e){}</script>

      <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.3.4/js/froala_editor.min.js"></script> -->

      <!-- Fancybox image popup -->
      <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />

      <!-- Custom Theme Style -->
      <link href="../build/css/custom.min.css" rel="stylesheet">
      <style type="text/css">

      th.next.available{
        background-color: #6f9755;
      }
      th.prev.available{
        background-color: #6f9755;
      }
      .fr-toolbar.fr-desktop.fr-top.fr-basic.fr-sticky.fr-sticky-off {
        background: #f7f7f7  !important;
        border-top: 5px solid #f5f5f5  !important;
      }
      th.next.available {
        background: #6f9755;
      }
      th.next.available:hover {
        background: #9ab688;
      }
      th.prev.available {
        background: #6f9755;
      }
      th.prev.available:hover {
        background: #9ab688;
      }
      .thumbnail {
        height: 100px;
        margin: 5px;
      }
      .fileUpload {
        position: relative;
        overflow: hidden;
        //margin: 10px;
      }
      .fileUpload input.upload {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
      }
      .file-drop-zone-title {
        padding: 0px 0px !important;
      }
      th.next.available{
        background-color: #6f9755;
      }
      th.prev.available{
        background-color: #6f9755;
      }
      .form-control[readonly] { /* For Firefox */
        background-color: white;
      }

      .form-control[readonly] {
        background-color: white;
      }

      .file-caption-main .btn-file {
        overflow: visible;
      }

      .file-caption-main .btn-file .error {
        position: absolute;
        bottom: -32px;
        right: 30px;
      }
    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <?php include './master/navbar.php';?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include './master/top_nav.php';?>
        <!-- /top navigation -->
        <?php
$sql = "SELECT * FROM LH_PROJECT_REVIEW WHERE project_review_id = '$id'";
$query = mssql_query($sql);
$row = mssql_fetch_array($query);
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><a href="managment_web.php?tab=Project">ระบบจัดการข้อมูล Project Review</a> > แก้ไข Project Review :
                      <?php
$row['project_review_name_th'] . " (" . $row['project_review_name_en'] . ")";
$str = strlen($row['project_review_name_th']);
if ($str >= 67) {
	echo iconv_substr($row['project_review_name_th'], 0, 66, "UTF-8") . "...";
} else {
	echo $row['project_review_name_th'];
}

?> </h2>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                     <?php if (isset($success)) {?>
                     <div class="row">
                      <div class="col-md-2"></div>
                      <div class="form-group">
                        <div class="alert alert-success col-md-6" id="alert">
                          <button type="button" class="close" data-dismiss="alert">x</button>
                          <strong>แก้ไขข้อมูลเรียบร้อย</strong>
                        </div>
                      </div>
                      <div class="col-md-6"></div>
                    </div>
                    <?php } else if (isset($error)) {?>
                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="form-group">
                        <div class="alert alert-danger col-md-6" id="alert">
                          <button type="button" class="close" data-dismiss="alert">x</button>
                          <strong>มีชื่อนี้อยู่ในระบบแล้ว </strong>
                        </div>
                      </div>
                      <div class="col-md-4"></div>
                    </div>
                    <?php } else if (isset($errors)) {?>

                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="form-group">
                        <div class="alert alert-danger col-md-6" id="alert">
                          <button type="button" class="close" data-dismiss="alert">x</button>
                          <strong>ลบข้อมูลเรียบร้อยแล้ว !</strong>
                        </div>
                      </div>
                      <div class="col-md-4"></div>
                    </div>
                    <?php } else if (isset($error2)) {?>
                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="form-group">
                        <div class="alert alert-danger col-md-6" id="alert">
                          <button type="button" class="close" data-dismiss="alert">x</button>
                          <strong>ระยะเวลาทับซ้อนกับข้อมูลที่มีอยู่ !</strong>
                        </div>
                      </div>
                      <div class="col-md-4"></div>
                    </div>
                    <?php } else if (isset($date_yum)) {?>
                    <div class="row" id="error_">
                      <div class="col-md-2"></div>
                      <div class="form-group">
                        <div class="alert alert-danger col-md-6" id="alert">
                          <button type="button" class="close" data-dismiss="alert">x</button>
                          <strong>ระยะเวลาทับซ้อนกับข้อมูลที่มีอยู่ !</strong>
                        </div>
                      </div>
                      <div class="col-md-4"></div>
                    </div>
                    <?php }?>
                    <p class="font-gray-dark">
                    </p><br>

                    <form class="form-horizontal form-label-left" action="update_project_review.php" method="POST" enctype="multipart/form-data" id="commentForm">
                      <input type="hidden" name="id" value="<?=$row['project_review_id'] ?>">
                      <div class="form-group">
                        <label class="control-label col-md-3" for="first-name">ชื่อ Project Review (อังกฤษ)
                          <span class="required">*</span>
                        </label>
                        <div class="col-md-6">
                          <input type="text" id="first-name1" required class="form-control col-md-7 col-xs-12"  name="project_review_name_th" placeholder="ชื่อ Project Review (อังกฤษ)" value="<?=$row['project_review_name_th']?>">
                        </div>
                      </div>

                      <!-- SEO -->
                      <div class="form-group">
                        <label class="control-label col-md-3" for="first-name">Titlt SEO (อังกฤษ)<br>(กรอกได้ไม่เกิน 70 คำ)
                          <span class="required">*</span>
                        </label>
                        <div class="col-md-6">
                          <input type="text"  required class="form-control col-md-7 col-xs-12" value="<?php echo $row['title_seo'];?>" name="landing_page_title_seo_th" placeholder="กรอก Titlt SEO" maxlength="70">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3" for="first-name">H1<br>(กรอกได้ไม่เกิน 255 คำ)
                        </label>
                        <div class="col-md-6">
                          <input type="text"   class="form-control col-md-7 col-xs-12" value="<?php echo $row['h1'];?>" name="h1" placeholder="กรอก H1" maxlength="255">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3" for="first-name">Description SEO (อังกฤษ) <br>(กรอกได้ไม่เกิน 160 คำ)
                          <span class="required">*</span>
                        </label>
                        <div class="col-md-6">
                          <textarea required class="form-control col-md-7 col-xs-12"  name="landing_page_description_th" placeholder="กรอก Description SEO" rows="4" maxlength="160"><?php echo $row['description_seo'];?></textarea> 
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3" for="first-name">Keyword SEO (อังกฤษ)
                          <span class="required">*</span>
                        </label>
                        <div class="col-md-6">
                          <textarea required class="form-control col-md-7 col-xs-12"  name="keyword_seo" placeholder="กรอก keyword SEO" rows="4"><?php echo $row['keyword_seo'];?></textarea> 
                        </div>
                      </div>


                    <?php
                    $req = "*";
                    $required = "required";
                    ?>

                    <?php if(isset($row['thumnail_seo'])&&($row['thumnail_seo'] != "")) {
                      $req = "";
                      $required = "";?>

                      <div class="form-group">
                        <label class="control-label col-md-3" for="first-name">ตัวอย่าง Thumnail SEO <br>นามสกุล .jpg .jpeg<br> ขนาด 600 x 338 px <br> ภาพขนาดไม่เกิน 300 KB <span class="required">*</span>
                        </label>
                        <div class="col-md-6">
                          <div id="map-image-holder-brochure"></div>
                          <div class="file-preview ">
                            <div class="close fileinput-remove"></div>
                            <div class="file-drop-disabled">
                              <div class="file-preview-thumbnails">
                                <div class="file-initial-thumbs">
                                  <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                    <div class="kv-file-content">
                                      <a class="fancybox" href="<?php echo $row['thumnail_seo'];?>">
                                        <img src="<?php echo $row['thumnail_seo'];?>" class="kv-preview-data file-preview-image"  style="max-height: 150px; width: auto">
                                      </a>
                                    </div>
                                    <div class="file-thumbnail-footer">
                                      <div class="file-actions">
                                        <div class="file-footer-buttons">
                              
                                          <a class="fancybox" href="<?=$row['thumnail_seo']?>">
                                            <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                          </a>
                                        </div>
                                        <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                      </div>
                                    </div>

                                  </div>
                                </div>
                              </div>
                              <div class="clearfix"></div>
                              <div class="file-preview-status text-center text-success"></div>
                              <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <?php } ?>

                      <div class="form-group">
                        <label class="control-label col-md-3" for="first-name">Thumnail SEO <br>นามสกุล .jpg .jpeg<br> ขนาด 600 x 338 px <br> ภาพขนาดไม่เกิน 300 KB <span class="required">*</span>
                        </label><br>
                        <div class="col-md-6">
                          <input id="fileupload_seo" name="fileupload_th" type="file" multiple class="file-loading" accept="image/*" >
                        </div>
                      </div>

                      <script>
                        $("#fileupload_seo").fileinput({
                        uploadUrl: "upload.php", // server upload action
                        maxFileCount: 1,
                        allowedFileExtensions: ["jpg", "jpeg"],
                        browseLabel: 'เลือกรูป',
                        removeLabel: 'ลบ',
                        browseClass: 'btn btn-success',
                        showUpload: false,
                        showRemove:false,
                        showCaption: false, // ปิดช่องแสดงชื่อไฟล์
                        msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                       minImageWidth: 600, //ขนาด กว้าง ต่ำสุด
                       minImageHeight: 338, //ขนาด ศุง ต่ำสุด
                       maxImageWidth: 600, //ขนาด กว้าง ต่ำสุด
                       maxImageHeight: 338, //ขนาด ศุง ต่ำสุด
                       msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                       msgZoomModalHeading: 'ตัวอย่างละเอียด',
                       xxx:'landing_img_seo',
                       dropZoneTitle : 'รูป Thumnail SEO',
                       maxFileSize :300 ,
                       msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                       msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                       msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                       msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                       msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',   
                     });
                   </script>


                      <div class="form-group hide-for-th">
                        <label class="control-label col-md-3" for="first-name">ชื่อ Project Review (อังกฤษ)
                          <span class="required">*</span>
                        </label>
                        <div class="col-md-6">
                          <input type="text" id="first-name2"  class="form-control col-md-7 col-xs-12"  name="project_review_name_en" placeholder="ชื่อ Project Review (อังกฤษ)" value="<?=$row['project_review_name_en'] ?>">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3" for="first-name">ชื่อโครงการ <span class="required">*</span>
                        </label>

                        <?php

$query_3 = "SELECT * FROM dbo.LH_PROJECTS order by project_name_th asc;";
$query_result3 = mssql_query($query_3, $db_conn);

?>
                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select class="form-control" name="project" onchange="getval(this);" required>
                            <option value="" >เลือกโครงการ</option>
                            <?php
while ($row1 = mssql_fetch_array($query_result3)) {
	if ($row1['project_id'] == $row['project_id']) {
		$selected = 'selected';
	} else {
		$selected = '';
	}
	?>
                             <option value="<?=$row1['project_id'];?>" <?=$selected?>><?=$row1['project_name_th'];?></option>
                             <?php }?>
                           </select>
                         </div>
                       </div>
                       <div class="form-group">
                        <label class="control-label col-md-3" for="first-name">เลือกลำดับ <span class="required"></span></label>

                        <?php
$total_item = 6;
$query_4 = "SELECT DISTINCT TOP {$total_item} order_seq FROM LH_PROJECT_REVIEW WHERE order_seq !='' ORDER BY order_seq ASC ";
$query_result4 = mssql_query($query_4, $db_conn);
$query_result5 = mssql_query($query_4, $db_conn);
//$order_num_row = mssql_num_rows($query_result4);

// for ($i=2; $i <=$total_item ;$i+=3 ) {
// 	$mid_pos[]=$i;
// }
$mid_pos_str = implode(",", [2]);
while ($order_list = mssql_fetch_array($query_result4)) {
	if ($order_list['order_seq'] != '') {
		$dump_arr[] = $order_list['order_seq'];
	}
}
if (!in_array($row['order_seq'], [2])) {
	$select_img_text = 'กรุณาเลือกรูปขนาด 1920 * 1080';
} else {
	$select_img_text = 'กรุณาเลือกรูปขนาด 720 * 800';
}

$own_selected = [$row['order_seq']];
$selected_stg = implode(',', array_diff($dump_arr, $own_selected));
?>

                        <div class="col-md-6 col-sm-9 col-xs-12">
                         <select id='order_seq' class="form-control" name="order_seq"  >
                          <option value="">เลือกข้อมูล</option>
                          <?php
$i = 1;
while ($i <= $total_item):
?>
                            <option value="<?=$i;?>"><?=$i;?>
                             <?php
if (in_array($i, $dump_arr)) {
	$val = "(ลำดับนี้ถูกใช้งานแล้ว)";
	if ($i == $row['order_seq']) {
		$val = '';
	}
} else {
	$val = '';
}
echo $val;
?>
                        </option>
                        <?php
$i++;
endwhile;
?>
                    </select>

                  </div>
                </div>
                <?php
$sql = "SELECT * FROM LH_PROJECT_REVIEW WHERE project_review_id = '$id'";
$query = mssql_query($sql);
$row = mssql_fetch_array($query);
$req = "*";
$required = "required";
?>
                <?php if (isset($row['project_review_img']) && ($row['project_review_img'] != "")) {
	$req = "";
	$required = "";?>

                  <div class="form-group" id="img_none">
                    <label id="label_pic" class="control-label col-md-3" for="first-name"><?=$select_img_text;?> <br>ภาพขนาดไม่เกิน 300 KB
                    </label>
                    <div class="col-md-6">
                      <div id="map-image-holder-brochure"></div>
                      <div class="file-preview ">
                        <div class="close fileinput-remove"></div>
                        <div class="file-drop-disabled">
                          <div class="file-preview-thumbnails">
                            <div class="file-initial-thumbs">
                              <div class="file-preview-frame file-preview-initial" id="logo" data-fileindex="init_0" data-template="image">
                                <div class="kv-file-content">
                                  <a class="fancybox" href="<?=$row['project_review_img']?>">
                                    <img src="<?=$row['project_review_img']?>" class="kv-preview-data file-preview-image"  style="width:auto;height:160px;">
                                  </a>
                                </div>

                                <div class="file-thumbnail-footer">
                                 <p>Alt text : <?=$row['project_review_img_seo'] . $_SESSION['add'];?></p>
                                 <div class="file-actions">
                                  <div class="file-footer-buttons">
                                    <button type="button" id="<?php echo $row['project_review_id']; ?>" onclick="deleteImageProjectReview(<?php echo $row['project_review_id']; ?>)" class="kv-file-remove btn btn-xs btn-default" title="Remove file" data-url="" data-key="1"><i class="glyphicon glyphicon-trash text-danger"></i></button>
                                    <a class="fancybox" href="<?=$row['project_review_img']?>">
                                      <button type="button" class="kv-file-zoom btn btn-xs btn-zoom" title="View Details"><i class="glyphicon glyphicon-zoom-in"></i></button>
                                    </a>
                                  </div>
                                  <span class="file-drag-handle drag-handle-init text-info" title="Move / Rearrange"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="file-preview-status text-center text-success"></div>
                        <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <?php }?>

                <!-- url yputube -->
                <div class="form-group" id="l1">
                  <div class="row">
                    <label class="control-label col-md-3" for="first-name"> </label>
                    <label class="control-label col-md-3" for="first-name"></label>
                  </div>
                  <label id="label_pic2" class="control-label col-md-3" for="first-name"><?=$select_img_text;?> <br>ภาพขนาดไม่เกิน 300 KB <?php echo $req; ?><br> <!-- (ความกว้างต่ำสุด 357 px - <br> ความกว้างสูงสุด 1920 px) -->
                  </label>
                  <div id='new-pic' class="col-md-6">
                    <input id="project_review_img" name="project_review_img" <?php echo $required; ?> type="file" multiple class="file-loading" accept="image/*" >
                  </div>
                </div>

                <script type="text/javascript">
                  var img_vid_w=1920;
                  var img_vid_h=1080;
                  function reset_img_vid(w,h){
                    $("#project_review_img").fileinput('destroy');
                    $("#project_review_img").val('');
                    img_vid_w = w;
                    img_vid_h = h;
                    int_img_vid();
                  }
                  function int_img_vid() {
                   $("#project_review_img").fileinput({
                                 uploadUrl: "upload.php", // server upload action
                                 maxFileCount: 1,
                                 allowedFileExtensions: ["jpg", "gif", "png", "jpeg"],
                                 browseLabel: 'เลือกรูป',
                                 removeLabel: 'ลบ',
                                 browseClass: 'btn btn-success',
                                 showUpload: false,
                                 showRemove:false,
                                 showCaption: false,
                                 msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                                 msgFilesTooMany: 'ไฟล์ที่คุณเลือกมีจำนวน <b>({n})</b> ซึ่งเกินกว่าที่ระบบอนุญาตที่ <b>{m}</b>, กรุณาลองใหม่อีกครั้ง!',
                                 msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                 xxx:'seo_lead_img_file',
                                 dropZoneTitle : 'รูปProject Review',
                                 minImageWidth: img_vid_w, //ขนาด กว้าง ต่ำสุด
                                 minImageHeight: img_vid_h, //ขนาด ศุง ต่ำสุด
                                 maxImageWidth: img_vid_w, //ขนาด กว้าง ต่ำสุด
                                 maxImageHeight: img_vid_h, //ขนาด ศุง ต่ำสุด
                                 maxFileSize :300 ,
                                 msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                 msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                 msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                 msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                 msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                               });
                 }

                 $('#new-pic #project_review_img').on('change', function(event) {
                  $("input[name='pic_err']").val(1);
                });
                 $('#project_review_img').on('fileuploaderror', function(event, data, msg) {
                  $("input[name='pic_err']").val(0);
                });
                 var int_val=<?=($row['order_seq'] == '' ? '""' : $row['order_seq']);?>;
                 var order_selected=[<?=$selected_stg;?>];
                 var current_val=int_val;
                 var confirmx=true;
                 var midpos=[2];
                 int_img_vid();
                 $("select[name=order_seq]").val(int_val);
                 $("#order_seq").on('change',function(){
                  var choose_val=$(this).val();
                  if (midpos.indexOf(Number(choose_val))>=0) {
                    $("#project_review_img").fileinput('destroy');
                    reset_img_vid(720,800);
                    $("#label_pic").text("กรุณาเลือกรูปขนาด 720 * 800 <br>ภาพขนาดไม่เกิน 300 KB");
                    $("#label_pic2").text("กรุณาเลือกรูปขนาด 720 * 800 <br>ภาพขนาดไม่เกิน 300 KB");
                    $('#img_none').show();
                  }else{
                    $("#project_review_img").fileinput('destroy');
                    reset_img_vid(1920,1080);
                    $("#label_pic").text("กรุณาเลือกรูปขนาด 1920 * 1080 <br>ภาพขนาดไม่เกิน 300 KB");
                    $("#label_pic2").text("กรุณาเลือกรูปขนาด 1920 * 1080 <br>ภาพขนาดไม่เกิน 300 KB");
                    $('#img_none').show();
                  }
                  if (order_selected.indexOf(Number(choose_val))>=0 && confirmx==true) {
                              // if (confirm("ลำดับนี้ถูกใช้งานแล้ว ต้องการดำเนินการต่อหรือไม่")) {
                              //   current_val=$(this).val();
                              //   return true;
                              //  }else{
                              //    $("#order_seq").val(current_val);
                              //    confirmx=false;
                              //    $('#order_seq').change();
                              //  }
                              $.confirm({
                                title: 'แจ้งเตือน',
                                content: 'ลำดับนี้ถูกใช้งานแล้ว ต้องการดำเนินการต่อหรือไม่',
                                buttons: {
                                  confirm: {
                                    action: function(){
                                            // current_val=$(this).val();
                                            // return true;
                                            if ((current_val!=2 && choose_val ==2) || (current_val==2 && choose_val !=2)) {
                                              $('#img_none').hide();
                                            }
                                          },
                                          btnClass: 'btn btn-success',
                                        },
                                        cancel: function () {
                                         $("#order_seq").val(current_val);
                                         confirmx=false;
                                         $('#order_seq').change();
                                         $('#img_none').show();
                                       },

                                     }
                                   });
                            }else{
                              current_val=$(this).val();
                            }
                            setTimeout(function() {confirmx=true;}, 700);
                          });
                 $('#order_seq').change();
               </script>
               <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">รายละเอียด Project Review (อังกฤษ) * <br> ใส่ข้อความไม่เกิน 150 คำ <span class="required"></span></label>
                <div class="col-md-6 col-sm-9 col-xs-12">
                 <!-- <div id="edit" class="text-left txt-black fr-box fr-basic fr-top" style="z-index: 1000;"></div>  -->
                 <textarea name="project_review_dis_th" class="form-control" rows="5" required="" maxlength="150" placeholder="กรอกรายละเอียด Project Review (อังกฤษ)"><?=$row['project_review_dis_th']?></textarea>
               </div>
             </div>



             <div class="form-group hide-for-th">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">รายละเอียด Project Review (อังกฤษ) <br> ใส่ข้อความไม่เกิน 150 คำ </label>
              <div class="col-md-6 col-sm-9 col-xs-12">

               <!-- <div id="edit" class="text-left txt-black fr-box fr-basic fr-top" style="z-index: 1000;"></div>  -->
               <textarea name="project_review_dis_en" class="form-control" rows="5" maxlength="150" placeholder="กรอกรายละเอียด Project Review (อังกฤษ)"><?=$row['project_review_dis_en']?></textarea>

             </div>
           </div>


           <div class="form-group">
            <label class="control-label col-md-2 col-sm-3 col-xs-12">content Project Review (อังกฤษ) </label>
            <div class="col-md-9 col-sm-9 col-xs-12">

             <!-- <div id="edit" class="text-left txt-black fr-box fr-basic fr-top" style="z-index: 1000;"></div>  -->
             <textarea name="project_review_dis_content" id="edit" rows="15"><?=$row['project_review_dis_content']?></textarea>

           </div>
         </div>

         <br>
         <br>

         <div class="form-group hide-for-th">
          <label class="control-label col-md-2 col-sm-3 col-xs-12">content Project Review (อังกฤษ) </label>
          <div class="col-md-9 col-sm-9 col-xs-12">

            <!-- <div id="edit" class="text-left txt-black fr-box fr-basic fr-top" style="z-index: 1000;"></div>  -->
            <textarea name="project_review_dis_content_en" id="edit_en" rows="15"><?=$row['project_review_dis_content_en']?></textarea>

          </div>
        </div>

    <div class="form-group">
      <label class="control-label col-md-2" for="first-name">กำหนด URL พิเศษ <br>(ห้ามใส่ อัขระพิเศษ)<span class="required"></span>
      </label>
      <div class="col-md-6">
        <input type="text"   class="form-control col-md-7 col-xs-12" value="<?=$row['custom_url']?>" name="custom_url" placeholder="ชื่อ URL" >
      </div>
    </div>

    <br>
    <?php
                      //2017-03-30
    $date_start=  $row['project_update'];

$day = substr($date_start, 8, 2);
$mount = substr($date_start, 5, 2);
$year = substr($date_start, 0, 4);

$date_start = $day . "/" . $mount . "/" . $year;

?>

        <div class="form-group">
          <label class="control-label col-md-2" for="first-name">วันที่จะเผยแพร่ <span class="required">*</span>
          </label>
          <div class="col-md-3">
            <div class="control-group">
              <div class="controls">

                <input  id="date_start" type="text" class="form-control has-feedback-left active"  placeholder="วันที่จะเผยแพร่" aria-describedby="inputSuccess2Status2" name="date_start"
                value="<?=$date_start?>" readonly>
                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>

              </div>
            </div>
          </div>
        </div>

    <?php
    if($row['custom_url']==''){
      $url=$url_master.'en/review/'.str_replace(" ", "-", $row['project_review_name_th']);
    }else{
      $url=$url_master.'en/review/'.str_replace(" ", "-", $row['custom_url']);
    }

    ?>

    <br>
    <div class="form-group">
     <label class="control-label col-md-3" for="first-name">
     </label>
     <div class="col-md-6">
      <input type='hidden' name='pic_err' value='1'>
      <a href="<?=$url;?>" target="_blank"><button type="button" class="btn btn-warning">Preview</button></a>
      <a class="confirms" data-title="ยืนยันการลบข้อมูล" name="delete" href="delete_project_review.php?id=<?=$id;?>">
        <button type="button" class="btn btn-danger">Delete</button></a>
        <button type="submit" class="btn btn-success">Update</button>
      </div>
    </div>

      </form>
    </div>
  </div>
</div>
</div>
<!-- /page content -->
<script type="text/javascript">
  $('a.confirms').confirm({
    content: 'คุณแน่ใจที่จะลบข้อมูลนี้ !',
    buttons: {
      Yes: {
        text: 'Yes',
        btnClass: 'btn-danger',
        keys: ['enter', 'a'],
        action: function(){
          location.href = this.$target.attr('href');
        }
      },
      No: {
        text: 'No',
        btnClass: 'btn-default',
        keys: ['enter', 'a'],
        action: function(){
                          // button action.
                        }
                      },

                    }
                  });
                </script>
                <!-- footer content -->
        <!-- <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer> -->
        <!-- /footer content -->
      </div>
    </div>


    <script type="text/javascript" src="libs/codemirror/5.3.0/codemirror.min.js"></script>
    <script type="text/javascript" src="libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/froala_editor.pkgd.min.js"></script>


    <!-- Include Plugins. -->
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/align.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/char_counter.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/code_beautifier.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/code_view.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/colors.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/emoticons.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/entities.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/file.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/font_family.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/font_size.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/fullscreen.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/image.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/image_manager.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/inline_style.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/line_breaker.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/link.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/lists.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/paragraph_format.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/paragraph_style.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/quick_insert.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/quote.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/table.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/save.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/url.min.js"></script>
    <script type="text/javascript" src="libs/froala-editor/2.6.2/js/plugins/video.min.js"></script>


    <!-- Custom Theme Scripts -->
    <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <script type="text/javascript" src="js/bootstrap-imgupload.js"></script>
    <!-- Validate -->
    <script src="../build/js/jquery.validate.js"></script>


    <!--Editor_script-->
    <!-- date -->
    <link rel="stylesheet" type="text/css" href="../build/css/jquery-ui.css"/>
    <script src="../build/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

    <!-- Validate -->
    <script src="../build/js/jquery.validate.js"></script>


    <script>
      $(document).ready(function() {
        $("#commentForm").on("submit",function(){
          if ($("#new-pic #project_review_img").val()!='' && $("input[name='pic_err']").val()==0) {
            return false;
          }
        });
        $.validator.setDefaults({ ignore: "[contenteditable='true']" });
        $("#commentForm").validate({
          rules: {
            project_review_name_th: "required",
            //project_review_name_en: "required",
            project: "required",
            project_review_dis_th: "required",
            date_start : "required",
            landing_page_title_seo_th: {required: true,maxlength: 70},
            landing_page_description_th:{required: true,maxlength: 160},
            //fileupload_th: "required",
            keyword_seo: "required",
          },
          messages: {
            project_review_name_th: "กรุณากรอกชื่อ Project Review อังกฤษ !",
            project : "กรุณาเลือกโครงการ !",
            project_review_dis_th : "กรุณากรอกรายละเอียด Project Review อังกฤษ !",
            project_review_img : "&nbsp; กรุณาเลือกรูป !",
            date_start : "กำหนดวันที่เผยแพร่ !",
            landing_page_description_th:{
              required: "กรุณากรอก Description SEO",
              maxlength:  "กรุณากรอก ไม่เกิน 160 ตัวอักษร !"
            },
            landing_page_title_seo_th:{
              required: "กรุณากรอก Title SEO",
              maxlength:  "กรุณากรอก ไม่เกิน 70 ตัวอักษร !"
            },
            keyword_seo : "กรุณากรอก Keyword SEO ",
            landing_page_title_seo_th:"กรุณากรอก Title SEO",
            fileupload_th : " &nbsp; กรุณาเลือกรูป !",
            h1 : {
               maxlength:  "กรุณากรอก ไม่เกิน 255 ตัวอักษร !"
            }
          },
        });



        // $("#project_review_img").rules("add", {
        //   required:true,
        //   messages: {
        //     required: " &nbsp; ไม่มีรูป !"
        //   }
        // });

      });
    </script>

    <!-- Fancybox image popup -->


    <script type="text/javascript">
      $('.fancybox').fancybox();
    </script>
    <!-- Include Language file if we want to u-->
    <script>
      $(function() {
        $('#edit').froalaEditor({

          height: 500,
          imageUploadURL: 'uploade_highlights.php',
          // Set max image size to 300kB.
          imageMaxSize: 0.3 * 1024 * 1024,
          imageUploadParams: {
            id: 'edit'
          },
          imageManagerLoadURL: 'images_load.php',

          fileUploadURL: 'upload_file.php',
          fileUploadParams: {
            id: 'edit'
          },
          videoUploadURL: 'upload_video.php',
          videoUploadParams: {
            id: 'edit'
          },
          fontFamily: {
            "LHfont": 'Kittithada',
            "Roboto,sans-serif": 'Roboto',
            "Oswald,sans-serif": 'Oswald',
            "Montserrat,sans-serif": 'Montserrat',
            "'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
          },

          imageManagerDeleteURL: "delete_image_highlights.php",
          imageManagerDeleteMethod: "POST"
        })
      // Catch image removal from the editor.
      .on('froalaEditor.image.removed', function (e, editor, $img) {
        $.ajax({
          // Request method.
          method: "POST",

          // Request URL.
          url: "delete_image_highlights.php",

          // Request params.
          data: {
            src: $img.attr('src')
          }
        })
        .done (function (data) {
          console.log ('image was deleted'+$img.attr('src'));
        })
        .fail (function (err) {
          console.log ('image delete problem: ' + JSON.stringify(err));
        })
      })

      // Catch image removal from the editor.
      .on('froalaEditor.file.unlink', function (e, editor, link) {

        $.ajax({
          // Request method.
          method: "POST",

          // Request URL.
          url: "delete_file.php",

          // Request params.
          data: {
            src: link.getAttribute('href')
          }
        })
        .done (function (data) {
          console.log ('file was deleted');
        })
        .fail (function (err) {
          console.log ('file delete problem: ' + JSON.stringify(err));
        })
      })
    });

      $(function() {
        $('#edit_en').froalaEditor({
          height: 500,
          imageUploadURL: 'uploade_highlights.php',
          // Set max image size to 300kB.
          imageMaxSize: 0.3 * 1024 * 1024,
          imageUploadParams: {
            id: 'edit_en'
          },

          fileUploadURL: 'upload_file.php',
          fileUploadParams: {
            id: 'edit_en'
          },
          videoUploadURL: 'upload_video.php',
          videoUploadParams: {
            id: 'edit_en'
          },
          fontFamily: {
            "LHfont": 'Kittithada',
            "Roboto,sans-serif": 'Roboto',
            "Oswald,sans-serif": 'Oswald',
            "Montserrat,sans-serif": 'Montserrat',
            "'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
          },

          imageManagerLoadURL: 'uploade_highlights.php',
          imageManagerDeleteURL: "delete_image_highlights.php",
          imageManagerDeleteMethod: "POST"
        })
        // Catch image removal from the editor.
        .on('froalaEditor.image.removed', function (e, editor, $img) {
          $.ajax({
                    // Request method.
                    method: "POST",

                    // Request URL.
                    url: "delete_image_highlights.php",

                    // Request params.
                    data: {
                      src: $img.attr('src')
                    }
                  })
          .done (function (data) {
            console.log ('image was deleted'+$img.attr('src'));
          })
          .fail (function (err) {
            console.log ('image delete problem: ' + JSON.stringify(err));
          })
        })

            // Catch image removal from the editor.
            .on('froalaEditor.file.unlink', function (e, editor, link) {

              $.ajax({
                    // Request method.
                    method: "POST",

                    // Request URL.
                    url: "delete_file.php",

                    // Request params.
                    data: {
                      src: link.getAttribute('href')
                    }
                  })
              .done (function (data) {
                console.log ('file was deleted');
              })
              .fail (function (err) {
                console.log ('file delete problem: ' + JSON.stringify(err));
              })
            })
          });
        </script>

        <script>
          $(document).ready(function(){
            $("#alert").show();
            $("#alert").fadeTo(3000, 500).slideUp(500, function(){
              $("#alert").alert('close');
            });
          });
        </script>

        <script>
          $(document).ready(function(){
            $("#l1").show();
            $("#l2").hide();
            $("#l3").hide();
          });
        </script>

        <script>

          function getval(sel){
           $("#mySelect").val();
           if($("#mySelect").val() == "image"){
            $("#l1").show();
            $("#l2").hide();
            $("#l3").hide();
          }else if($("#mySelect").val() == "banner"){
            $("#l2").show();
            $("#l3").hide();
            $("#l1").hide();
          }else if($("#mySelect").val() == "vdo"){
            $("#l3").show();
            $("#l1").hide();
            $("#l2").hide();
          }
        }
      </script>
      <script>
        function deleteImageProjectReview(id) {
          $.confirm({
            title: 'ลบรูปนี้ !',
            content: 'คุณแน่ใจที่จะลบรูปนี้!',
            buttons : {
              Yes: {
                text: 'Yes',
                            btnClass: 'btn-red any-other-class', // multiple classes.
                            action: function () {
                              var self = this;
                              return $.ajax({
                                url: 'ajax_deleteImageProjectReview.php?project_review_id=' + id,
                                dataType: 'json',
                                method: 'get'
                              }).done(function () {
                                    //$.alert('Confirmed!');
                                    location.reload();
                                  }).fail(function () {
                                    self.setContent('Something went wrong.');
                                  });
                                }
                              },
                              No: function () {
                            //$.alert('ยกลเิก!');
                          },
                        }
                      });
        }

      </script>
      <script type="text/javascript">

        $('#date_start').datepicker({
          dateFormat : "dd/mm/yy",
          autoclose: true,
          changeMonth: true,
          showDropdowns: true,
          changeYear: true,
          minDate:  new Date(),
          onSelect: function(dateText) {
            var d = new Date($(this).datepicker("getDate"));

          }
        });

      </script>
      <script src="js/validate_file_300kb.js"></script>
    </body>
    </html>