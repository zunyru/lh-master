<?php
require_once 'include/dbConnect.php';

try {
    $id = $_GET['id'];

    $conn = (new dbConnect())->getConn();
    $sql = "UPDATE LH_PROJECT_CLIPS SET thumnail = ''
                WHERE clip_project_id =".$id;
    $result= $conn->query($sql);

    echo json_encode($result->fetchAll());

} catch (\Exception $e) {
    return $e->getMessage();
}
?>