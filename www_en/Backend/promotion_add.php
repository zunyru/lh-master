<?php
session_start();
include './include/dbCon_mssql.php';
$group_id = $_SESSION['group_id'];

$_GET['page'] = 'promotion';

if (isset($_SESSION['add'])) {
	if ($_SESSION['add'] == 1) {
		//header("location:product_add.php");
		$success = "success";
		$_SESSION['add'] = '';
	} else if ($_SESSION['add'] == 2) {

		$error_name = "error_name"; //ค่าซ้ำ
		$_SESSION['add'] = '';

	} else if ($_SESSION['add'] == -1) {
		$errors_date = "errors_date";
		$_SESSION['add'] = '';
	} else if ($_SESSION['add'] == -2) {
		$error_img = "error_img";
		$_SESSION['add'] = '';
	} else if ($_SESSION['add'] == -3) {
		$error_up = "error_up";
		$_SESSION['add'] = '';
	}
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>

        <!-- Bootstrap -->
      <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome -->
      <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <!-- NProgress -->
      <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
      <!-- Custom Theme Style -->
      <link href="../build/css/custom.min.css" rel="stylesheet">
      <!-- Fancybox image popup -->
      <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
      <!-- bootstrap-progressbar -->
      <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
      <!-- jQuery -->
      <script src="../vendors/jquery/dist/jquery.min.js"></script>

      <script src="../build/js/custom.min.js"></script>      <script src="../build/js/custom.min.js"></script>
      <!-- uploade img -->
      <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
      <!--uploade-->
      <!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
      <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
      <!-- confirm-->
      <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
      <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>


     <style type="text/css">

      th.next.available{
        background-color: #6f9755;
      }
      th.prev.available{
        background-color: #6f9755;
      }
      .file-caption-main .btn-file {
          overflow: visible;
      }

      .file-caption-main .btn-file .error {
          position: absolute;
          bottom: -32px;
          right: 30px;
      }
      .form-control[readonly] { /* For Firefox */
          background-color: white;
      }

      .form-control[readonly] {
          background-color: white;
      }
      </style>

    <!-- Custom Theme Style -->


  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="project.php" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <?php include './master/navbar.php';?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
          <?php include './master/top_nav.php';?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><a href="promotion.php">ระบบจัดการข้อมูลโปรโมชั่น :</a> หน้าสร้างโปรโมชั่น </h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <p class="font-gray-dark">
                  </p><br>
                   <?php if (isset($success)) {?>
                   <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-success col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>สร้างข้อมูลเรียบร้อยแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-6"></div>
                   </div>
                  <?php } else if (isset($error_name)) {?>
                    <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>มีชื่อนี้อยู่ในระบบแล้ว</strong>
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                   <?php } else if (isset($errors_date)) {?>

                  <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>ไม่สามารถสร้างข้อมูลได้ !</strong> วันที่ดำเนินการ ทับซ้อนกัน
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                  <?php } else if (isset($error_img)) {?>
                  <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>คุณยังไม่ได้เลือกรูปในโครงการ !</strong> กรุณาเลือกรูปโปรโมชัน
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                   <?php } else if (isset($error_up)) {?>
                  <div class="row">
                    <div class="col-md-2"></div>
                     <div class="form-group">
                      <div class="alert alert-danger col-md-6" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>เกิดข้อผิดพลาด !</strong> ไม่สามารถบันทึกข้อมูลได้
                      </div>
                    </div>
                    <div class="col-md-4"></div>
                   </div>
                   <?php }?>

                  <form class="form-horizontal form-label-left" action="save_promotion.php" method="post" id="commentForm">
                    <div class="form-group">
                      <label class="control-label col-md-2" for="first-name">ชื่อโปรโมชั่น (อังกฤษ) <span class="required">*</span></label>
                      <div class="col-md-6">
                        <input type="text" id="first-name" name="promotion_name_th" class="form-control col-md-7 col-xs-12" value="" required placeholder="กรอกชื่อโปรโมชัน (อังกฤษ)">
                      </div>
                    </div>
                    <div class="form-group hide-for-th">
                      <label class="control-label col-md-2" for="first-name">ชื่อโปรโมชั่น (อังกฤษ) <span class="required"></span>
                      </label>
                      <div class="col-md-6">
                        <input type="text" id="first-name2" name="promotion_name_en" class="form-control col-md-7 col-xs-12" value="" placeholder="กรอกชื่อโครงการ (อังกฤษ)">
                      </div>
                    </div>
                     <div class="form-group">
                      <label class="control-label col-md-2" for="first-name">รายละเอียดโปรโมชั่น (อังกฤษ)
                      </label>
                      <div class="col-md-6">
                        <textarea id="message" class="form-control" name="promotion_detail_th" rows="7" placeholder="ระบุรายละเอียดโปรโมชั่น (อังกฤษ)"></textarea>
                      </div>
                    </div>
                    <div class="form-group hide-for-th">
                      <label class="control-label col-md-2" for="first-name">รายละเอียดโปรโมชั่น (อังกฤษ)
                      </label>
                      <div class="col-md-6">
                        <textarea id="message" class="form-control" name="promotion_detail_en" rows="7" placeholder="ระบุรายละเอียดโปรโมชั่น (อังกฤษ)"></textarea>
                      </div>
                    </div>
                    <?php
$sql = "SELECT project_id,project_name_th,project_name_en FROM LH_PROJECTS WHERE group_id = '" . $group_id . "'";
$query_result3 = mssql_query($sql, $db_conn);
?>
                    <div class="form-group">
                      <label class="control-label col-md-2" for="brand">ระบุโครงการ <span class="required">*</span>
                      </label>
                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select id="select" class="form-control" name="project_id" required onchange="showUser(this.value)">
                            <option value="">เลือกโครงการ</option>
                            <?php while ($row_r = mssql_fetch_array($query_result3)) {
	?>
                            <option value="<?=$row_r['project_id']?>"><?=$row_r['project_name_th'];
	echo '' . (!empty($row_r['project_name_en']) ? ' (' . $row_r['project_name_en'] . ")" : "") . '';?></option>
                            <?php }?>
                          </select>
                        </div>
                      </div>

                      <script>
                          function showUser(str) {
                            if (str=="") {
                              document.getElementById("txtHint").innerHTML="";
                              return;
                            }
                            if (window.XMLHttpRequest) {
                              // code for IE7+, Firefox, Chrome, Opera, Safari
                              xmlhttp=new XMLHttpRequest();
                            } else { // code for IE6, IE5
                              xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                            }
                            xmlhttp.onreadystatechange=function() {
                              if (this.readyState==4 && this.status==200) {
                                document.getElementById("img").innerHTML=this.responseText;
                              }
                            }
                            xmlhttp.open("GET","ajaxselect_img_galery_project.php?id="+str,true);
                            xmlhttp.send();
                          }
                    </script>


                     <div class="form-group">
                        <label class="control-label col-md-2 col-sm-3 col-xs-12"></label>
                        <div class="col-md-8">
                          <label for="iCheck"  class="error"></label>
                      </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-3 col-xs-12"></label>
                        <div class="col-md-8">
                          <div id="map-image-holder-brochure"></div>
                          <div id="img"></div>
                          <!-- name = iCheck-->
                      </div>
                    </div>



                    <div class="form-group">
                      <label class="control-label col-md-2" for="first-name">ระบุวันเริ่ม <span class="required">*</span>
                      </label>
                      <div class="col-md-2">
                        <fieldset>
                        <div class="input-prepend input-group">
                           <input type="text" style="width: 180px" name="date_start" required id="date_start" class="form-control" value="" readonly/>
                         </div>
                        </fieldset>
                    </div>
                    <label class="control-label col-md-2" for="first-name">ระบุวันสิ้นสุด <span class="required" aria-required="true">*</span></label>
                        <div class="col-md-2">
                        <fieldset>
                        <div class="input-prepend input-group">
                           <input type="text" style="width: 180px" name="date" required="" id="date" class="form-control " value="" aria-required="true" readonly>
                         </div>
                        </fieldset>
                    </div>
                  </div>

                    <div class="form-group">
                      <label class="control-label col-md-2" for="first-name">กำหนด URL พิเศษ
                      </label>
                      <div class="col-md-6">
                        <input type="text" name="promotion_url" id="first-name2" class="form-control col-md-7 col-xs-12" placeholder="EX : http://lh.co.th or https://lh.co.th">
                      </div>
                      <p style="color:#929292;">**ถ้าไม่กำหนดจะลิงค์ไปยังหน้าโครงการอัตโนมัติ</p>
                    </div>

                    <br>

                    <center>
                    <button type="submit" class="btn btn-success">Submit</button>
                  </center>
                  </form>
                  <br>
                   <br>
                    <br>
                     <br>
                      <br>
                       <br>
                </div>
              </div>
            </div>
          </div>
        <!-- /page content -->

        <!-- footer content -->
        <!-- <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer> -->
        <!-- /footer content -->
      </div>
    </div>

         <!-- jQuery -->
        <!-- <script src="../vendors/jquery/dist/jquery.min.js"></script> -->
        <!-- Bootstrap -->
        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../vendors/nprogress/nprogress.js"></script>
        <!-- iCheck -->
        <script src="../vendors/iCheck/icheck.min.js"></script>
        <!-- date -->
        <link rel="stylesheet" type="text/css" href="../build/css/jquery-ui.css"/>
        <script src="../build/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
        <!-- jQuery Tags Input -->
        <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
        <!-- Fancybox image popup -->
        <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>
        <script src="../build/js/jquery.validate.js"></script>
        <script src="../build/js/custom.min.js"></script>

        <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>


        <!-- jQuery Tags Input -->
    <script>
      function onAddTag(tag) {
        alert("Added a tag: " + tag);
      }

      function onRemoveTag(tag) {
        alert("Removed a tag: " + tag);
      }

      function onChangeTag(input, tag) {
        alert("Changed a tag: " + tag);
      }

      $(document).ready(function() {
        $('#tags_1').tagsInput({
          width: 'auto'
        });
        $('#tags_2').tagsInput({
          width: 'auto'
        });
      });
    </script>


    <script>
        $(document).ready(function() {

            $("#commentForm").validate({
                rules: {
                    promotion_name_th: "required",
                    //promotion_name_en : "required",
                    project_id: "required",
                    date_start : "required",
                    date : "required",
                    iCheck : "required",

                },
                messages: {
                    promotion_name_th: "กรุณากรอกชื่อโปรโมชั่น อังกฤษ !",
                    //promotion_name_en : "กรุณากรอกชื่อโปรโมชั่น อังกฤษ !",
                    project_id : "กรุณาเลือกโครงการ !",
                    date_start : "กรุณาระบุวันที่เริ่มต้น !",
                    date : "กรุณาระบุวันที่สิ้นสุด !",
                    iCheck : "กรุณาเลือกรูปอย่างน้อย 1 รูป",
                }
            });


        });
    </script>
          <script type="text/javascript">

              $('#date_start').datepicker({
                  dateFormat : "dd/mm/yy",
                  autoclose: true,
                  changeMonth: true,
                  showDropdowns: true,
                  changeYear: true,
                  minDate:  new Date(),
                  onSelect: function(dateText) {
                      var d = new Date($(this).datepicker("getDate"));
                      $("input#date").datepicker('option', 'minDate', dateText);
                      $("input#date").prop('disabled', false);
                  }
              });
              $('#date').datepicker({
                  dateFormat : "dd/mm/yy",
                  autoclose: true,
                  changeMonth: true,
                  showDropdowns: true,
                  changeYear: true,
                  minDate:  new Date(),
                  onSelect: function(dateText) {
                      var d = new Date($(this).datepicker("getDate"))
                      $("input#date_start").datepicker('option', 'maxDate', dateText);
                  }
              });
          </script>


    <script>
        $(document).ready(function(){
            $("#alert").show();
            $("#alert").fadeTo(6000, 500).slideUp(500, function(){
                $("#alert").alert('close');
            });
        });
    </script>
  </body>
</html>