<?php
require_once 'include/dbConnect.php';

try {
    $community_id = $_GET['id'];

    $conn = (new dbConnect())->getConn();
    $sql = "UPDATE LH_COMMUNITY SET community_features_img = '' , community_features_img_seo = ''
				WHERE community_features_id =".$community_id;
    $result= $conn->query($sql);

    echo json_encode($result->fetchAll());

} catch (\Exception $e) {
    return $e->getMessage();
}
?>