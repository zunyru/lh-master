<?php
session_start();
include './include/dbCon_mssql.php';

$landing_page_name_th = $_POST['landing_page_name_th'];
$landing_page_name_en = $_POST['landing_page_name_en'];
$landing_page_content_th = $_POST['landing_page_content_th'];
$landing_page_url = $_POST['landing_page_url'];
$fileupload_th = $_FILES['fileupload_th']['name'];
$title_seo_th = $_POST['landing_page_title_seo_th'];
$description = $_POST['landing_page_description_th'];
$keyword_seo = $_POST['keyword_seo'];
$h1 = $_POST['h1'];

$dates=date("Y-m-d H:i:s");
$date = date("YmdHis");


//http://devwww2.lh.co.th/Land_and_house/Frontend/landing-page-details.php?landing_page_id=4
$url_master;
$url_landing = "Frontend/landing-page-details.php?landing_page_id=";
$url = str_replace(" ", "-", $landing_page_name_en, $var);
$url_cus = str_replace(" ", "-", $landing_page_url);

function mssql_escape($str) {
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return str_replace("'", "''", $str);
}


$query = "SELECT COUNT(*) AS counts FROM LH_LANDING_PAGE WHERE landing_page_name_th='$landing_page_name_th'  ";
$query_result = mssql_query($query, $db_conn);
$count = mssql_fetch_row($query_result);
$count[0];
if ($count[0] >= 1) {
    $_SESSION['add'] = '2';
    echo '<script>
    window.history.go(-1);
    </script>';
    exit();
}

if ($landing_page_url != "") {

  $path = "fileupload/images/landingpage/";

  $type_th = strrchr($_FILES['fileupload_th']['name'], ".");

  $newname_th = $date . $numrand_th . $type_th;

  $path_copy_th = $path . $newname_th;

  if (move_uploaded_file($_FILES['fileupload_th']['tmp_name'], $path_copy_th)) {

     $sql = "INSERT INTO LH_LANDING_PAGE (landing_page_name_th,landing_page_name_en,landing_page_url,landing_page_content_th,update_date,title_seo,description_seo,thumnail_seo,keyword_seo,h1)"
     . " VALUES ('" . mssql_escape($landing_page_name_th) . "','" . mssql_escape($landing_page_name_en) . "','".mssql_escape($url_cus)."','" . mssql_escape($landing_page_content_th) . "','$dates','".mssql_escape($title_seo_th)."','".mssql_escape($description)."','".$path_copy_th."','".$keyword_seo."','$h1')";
     $query = mssql_query($sql);
 }

 if ($query) {
     $_SESSION['add'] = '-2';
     header("location:landing_page_add.php?add=1");
 } else {
    $_SESSION['add'] = '-1';
    header("location:landing_page_add.php?add=-1");
}
} else {
 $sql_l = "SELECT MAX(landing_page_id) as max_id FROM LH_LANDING_PAGE";
 $query_l = mssql_query($sql_l);
 $row = mssql_fetch_array($query_l);
 $max_id=$row['max_id'];
 $max_id = $max_id + 1;

 $path = "fileupload/images/landingpage/";

 $type_th = strrchr($_FILES['fileupload_th']['name'], ".");

 $newname_th = $date . $numrand_th . $type_th;

 $path_copy_th = $path . $newname_th;

 if (move_uploaded_file($_FILES['fileupload_th']['tmp_name'], $path_copy_th)) {

     $sql = "INSERT INTO LH_LANDING_PAGE (landing_page_name_th,landing_page_name_en,landing_page_url,landing_page_content_th,update_date,title_seo,description_seo,thumnail_seo,keyword_seo,h1)"
     . " VALUES ('" . mssql_escape($landing_page_name_th) . "','" . mssql_escape($landing_page_name_en) . "','','" . mssql_escape($landing_page_content_th) . "','$dates','".mssql_escape($title_seo_th)."','".mssql_escape($description)."','".$path_copy_th."','".$keyword_seo."','$h1')";
     $query = mssql_query($sql);
 }

 if ($query) {
    $_SESSION['add'] = '1';
    header("location:landing_page_add.php?add=1");
} else {
    $_SESSION['add'] = '-1';
    header("location:landing_page_add.php?add=-1");
}
}
?>