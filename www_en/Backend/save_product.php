<?php
session_start();
include './include/dbCon_mssql.php';
include './include/UpdateSlug.php';
$product_name_th = $_POST['productname_th'];
$product_name_en = $_POST['productname_en'];

 $query = "SELECT COUNT(*) AS counts FROM dbo.LH_PRODUCTS WHERE product_name_th = '$product_name_th' ";
$stmt = mssql_query($query);
$row =mssql_fetch_array($stmt);
if($row['counts']<= 0){
    $dates=date("Y-m-d H:i:s");
    $query_result = "INSERT INTO LH_PRODUCTS (product_name_th, product_name_en, product_update) VALUES ('$product_name_th','$product_name_en','$dates')";
    $stmt = mssql_query($query_result);
    //update slug
    UpdateSlug::run(['product']);
    $_SESSION['add']='1';
    header("location:product_add.php");
    exit();
}else{
    $_SESSION['add']='0';
    echo '<script>window.history.go(-1);</script>';
    exit();
}