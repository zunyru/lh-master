<?php
require_once 'include/dbConnect.php';

    try {
        $floor_plan_img_id = $_GET['floor_plan_img_id'];

        $conn = (new dbConnect())->getConn();
        $sql = "UPDATE LH_GALERY_FLOOR_PLAN_IMG SET floor_plan_img_name = ''
                WHERE floor_plan_img_id =".$floor_plan_img_id;
        $result= $conn->query($sql);

        echo json_encode($result->fetchAll());

    } catch (\Exception $e) {
        return $e->getMessage();
    }
?>