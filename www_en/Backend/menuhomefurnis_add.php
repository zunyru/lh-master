<?php
/**
 * @author jacky
 * @createdate March 8 2007
 */
?>
<?php
$page_title = "LAND & HOUSES";
$page = "homemodel";
include ("header.php");
include ("include/dbConnect.php");
require_once ("homefurnis/class_lh_gallery_furnis_main.php");
?>
<?php
$db = new dbConnect ();
$homefur = new LH_GALLARY_FURNISHED ( $db->getConn () );
$result = $homefur->showProjectList ();
?>
<!-- page content -->
<link href="homefurnis/css/homefurnis.css" rel="stylesheet">
<script src="homefurnis/js/homefurnis.js"></script>
<?php include_once ("homefurnis/header_upload_image.php"); ?>
<div class="right_col" role="main">
	<div class="">
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>ระบบจัดการรูปภาพบ้านตกแต่งพร้อมขาย</h2>
						<div class="clearfix"></div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<form class="form-horizontal form-label-left" method="post"
								enctype="multipart/form-data" id="addFurForm">
								<div class="form-group">
									<label class="control-label col-md-3" for="project">ระบุชื่อโครงการของภาพ
										<span class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-9 col-xs-12">
										<select class="form-control" name="projectid" required>
											<option value="">เลือกโครงการ</option>
                             <?php
																													foreach ( $result as $row ) {
																														?>
                                 <option value="<?=$row['id']?>"><?=$row['projectname']?></option>
                             <?php } ?>
                          </select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3" for="first-name">ตั้งชื่อ
										folder <span class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-9 col-xs-12">
										<input type="text" value="" name="foldername" required
											class="form-control col-md-7 col-xs-12" placeholder="">
									</div>
								</div>
								<div class="form-group ">
									<label class="control-label col-md-3" for="first-name">รูปแกลอรี่
										<span class="required">*</span>
									</label>
									<div class="col-md-9 ">
										<div id="dropzone">
											<input type="file" id="uploader" multiple />
										</div>
									</div>
								</div>

								<div class="div-space-height"></div>
								<div class="form-group">
									<div class="col-md-12 div-center">
										<button type="button" name="savedata" class="btn btn-success">Submit</button>
										<button type="button" class="btn btn-danger" name="deldata">Delete</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!--end x_content -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- for upload image -->
<script>

              function fileUploaded(u, p) {
	             //alert(u.uploadList[p].name+' has been uploaded');
              }

              function fileUploadFailed(u, p, e) {
	               alert(u.uploadList[p].name+' couldn\'t be uploaded: '+e);
              }

			window.onload = function() {
				var uploadSettings = {
					'extension': ['png', 'jpg', 'jpeg', 'gif'],
					'callbackSuccess': fileUploaded,
					'callbackError': fileUploadFailed,
					'maxWidth':2000,
					'maxHeight':1500,
					'maxFileSize':2048,
					'thumbnails': true,
					'dropzone':document.getElementById("dropzone"),
					'debug':false,
					'concurrentUploads':15,
					'uploadButton':true,
					'autoUpload':false
				};
				var uploader = document.getElementById('uploader');
				var uploadForm = new RealTimeUpload(uploadSettings, uploader);
			};
		</script>
<!-- end for upload image -->
<!-- /page content -->
<?php include_once("footer.php"); ?>