<?php
/**
 * Class UpdateSlug
 * @package httpd\vroot\devlh\Backend\include
 */
class UpdateSlug
{
	/**
	 * Yothin
	 * @param array $method [method that want to execute]
	 */
	public static function notAllow($url_page)
	{
		$not_allow_list = array('landing-page','lh-living-concept/tips','review/');
		$not_allow=1;
		foreach ($not_allow_list as $not_allow_i) {
		    //if (strstr($string, $url)) { // mine version
		    if (strpos($url_page, $not_allow_i) == true) { // Yoshi version
		        $not_allow++;
		    }
		}
		return ($not_allow>1?false:true);
	}
	/**
	 * For updating slug for URL in Search function
	 * @param array $method [method that want to execute]
	 */
	public static function run(array $method = array())
	{
		if (in_array("product",$method)) {
			$product_q=mssql_query("SELECT product_id,product_name_en FROM LH_PRODUCTS");
			while ($product_r=mssql_fetch_object($product_q)) {
				$great_slug=self::slugify($product_r->product_name_en);
				mssql_query("UPDATE LH_PRODUCTS SET slug='{$great_slug}' WHERE product_id=$product_r->product_id");
			}
		}

		if (in_array("zone",$method)) {
			$zone_q=mssql_query("SELECT zone_id,zone_name_en FROM LH_ZONES");
			while ($zone_r=mssql_fetch_object($zone_q)) {
				$great_slug=self::slugify($zone_r->zone_name_en);
				mssql_query("UPDATE LH_ZONES SET slug='{$great_slug}' WHERE zone_id=$zone_r->zone_id");
			}
		}

		if (in_array("brand",$method)) {
			$brand_q=mssql_query("SELECT brand_id,brand_name_en FROM LH_BRANDS");
			while ($brand_r=mssql_fetch_object($brand_q)) {
				$great_slug=self::slugify($brand_r->brand_name_en);
				mssql_query("UPDATE LH_BRANDS SET slug='{$great_slug}' WHERE brand_id=$brand_r->brand_id");
			}
		}
	}

	/**
	 * Helper for generate pretty slug URL
	 * @param text $text [text to convert]
	 * @return  text $text [converted slug text]
	 */
	private static function slugify($text)
	{
	  // replace non letter or digits by -
	  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

	  // transliterate
	  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

	  // remove unwanted characters
	  $text = preg_replace('~[^-\w]+~', '', $text);

	  // trim
	  $text = trim($text, '-');

	  // remove duplicate -
	  $text = preg_replace('~-+~', '-', $text);

	  // lowercase
	  $text = strtolower($text);

	  if (empty($text)) {
	    return 'n-a';
	  }
	  return $text;
	}

}
