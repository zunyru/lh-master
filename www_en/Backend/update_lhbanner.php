<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (! isset ( $_SESSION )) {
    session_start ();
}

$group_id = $_SESSION ['group_id'];
$group_name=$_SESSION['group_name'];
$action=isset ( $_POST['action'])?$_POST['action']:'';

include './include/dbCon_mssql.php';
require_once './include/function_query.php';
require_once './include/BannerHelper.php';
date_default_timezone_set ( 'Asia/Bangkok' );


if($action=='EDIT'){
    $pageType = isset ( $_POST ['PageTypeId'] ) ? $_POST ['PageTypeId'] : '';
    $startdate = isset ( $_POST ['startdate'] ) ? $_POST ['startdate'] : '';
    $enddate = isset ( $_POST ['enddate'] ) ? $_POST ['enddate'] : '';
    $date = new DateTime ();

    //$banner = $_POST['banner'];
    $banner=isset($_POST['banner'])?$_POST['banner']:'';

    $sql ="SELECT * FROM LH_BANNER_MAIN WHERE type_page_id  = '$pageType' AND lead_img_mobile = 'd'";
    $qeury = mssql_query($sql);
    $result = mssql_fetch_assoc($qeury);
    $banner_main_id =  $result['banner_main_id'];

    $bannerMainId=isset($_POST['bannerMainId'])?$_POST['bannerMainId']:'';
    $changeFlag=isset ( $_POST ['chageBannerChoose'] ) ? $_POST ['chageBannerChoose'] : '';

    $today =date('Y-m-d H:i:s');

    if (! empty ( $startdate ) && ! empty ( $enddate )) {
        $startdate = DateTime::createFromFormat ( 'd/m/Y', $startdate );
        $enddate = DateTime::createFromFormat ( 'd/m/Y', $enddate );
        // startdate->format("Y-m-d")
        $startdate = $startdate->format('Y-m-d');
        $enddate = $enddate->format('Y-m-d');

    }
    if(isset($_FILES['lead_image_d'])){
        $numSize = count($_FILES['lead_image_d']["name"]);
        if($numSize > 0){
            for ($i = 0; $i < $numSize; $i++) {

                $seo_add = isset ($_POST ['banner_lead_seo'][$i] ) ? $_POST ['banner_lead_seo'][$i] : '';
                $url_add = isset ($_POST ['lead_image_p_url'][$i] ) ? $_POST ['lead_image_p_url'][$i] : '';

                $type_img = strrchr($_FILES["lead_image_d"]["name"][$i],".");
                $picname = "BL_". mt_rand().date('dmY').$type_img; 
                $pathName = "fileupload/banner_file/" . $picname ;
                $fname = "fileupload/banner_file/Thumbnails_" . $picname;

                if (move_uploaded_file($_FILES["lead_image_d"]["tmp_name"][$i], $pathName)) {

                    $sql2 = "select seq_leade_img from LH_BANNER_MAIN m left join LH_BANNER_LEAD l on m.banner_main_id = l.banner_main_id where m.type_page_id= '$pageType' and l.lead_img_mobile = 'd' order by l.seq_leade_img desc";
                    $qeury = mssql_query($sql2);
                    $row = mssql_fetch_array($qeury);
                    $seq_leade_img=  $row['seq_leade_img']+1;

                    $sql = "INSERT INTO LH_BANNER_LEAD (banner_main_id,lead_path,lead_img_mobile,seq_leade_img,seo_lead,img_url)  ";
                    $sql .= "  values ($banner_main_id,'$pathName','d','$seq_leade_img','$seo_add','$url_add')";
                    exeQuery($sql);
                    BannerHelper::genFacebookImg($pathName,$fname);
                }
            }
        }
    }

    //edit banner d
    if(isset($_FILES['lead_image_d_edit'])){
        $numSize = count($_FILES['lead_image_d_edit']["name"]);
        if($numSize > 0){
            for ($i = 0; $i < $numSize; $i++) {
                
                
                $seo_edit = isset ($_POST ['banner_lead_seo_edit'][$i] ) ? $_POST ['banner_lead_seo_edit'][$i] : '';
                $url_edit = isset ($_POST ['lead_image_d_url_edit'][$i] ) ? $_POST ['lead_image_d_url_edit'][$i] : '';

                $type_img = strrchr($_FILES["lead_image_d_edit"]["name"][$i],".");
                $picname = "BL_". mt_rand().date('dmY').$type_img;
                $pathName = "fileupload/banner_file/" . $picname ;
                $fname = "fileupload/banner_file/Thumbnails_" .$picname;
                if (move_uploaded_file($_FILES["lead_image_d_edit"]["tmp_name"][$i], $pathName)) {

                    $sql=" update LH_BANNER_LEAD set lead_path = '$pathName',img_url = '$url_edit',seo_lead = '$seo_edit' ";
                    $sql.=" where banner_lead_id = ".$_POST['banner_lead_id'][$i] ;
                   
                    exeQuery($sql);
                    BannerHelper::genFacebookImg($pathName,$fname);
                }
            }
        }
    }
   

    //seo banner d edit
    if(isset($_POST['banner_lead_seo_edit'])){
         $numSize = count($_POST['banner_lead_seo_edit']);
         for ($i = 0; $i < $numSize; $i++) {
            
            $seo_edit = isset ($_POST ['banner_lead_seo_edit'][$i] ) ? $_POST ['banner_lead_seo_edit'][$i] : '';
            $url_edit = isset ($_POST ['lead_image_d_url_edit'][$i] ) ? $_POST ['lead_image_d_url_edit'][$i] : '';

            $sql=" update LH_BANNER_LEAD set img_url = '$url_edit',seo_lead = '$seo_edit' ";
            $sql.=" where lead_img_mobile = 'd' AND banner_lead_id = ".$_POST['banner_lead_id'][$i] ;
            
            exeQuery($sql);    
         }   
    }
    // url image d edit
    if(isset($_POST['lead_image_d_url_edit'])){
         $numSize = count($_POST['lead_image_d_url_edit']);
         for ($i = 0; $i < $numSize; $i++) {
            
            $url_edit = isset ($_POST ['lead_image_d_url_edit'][$i] ) ? $_POST ['lead_image_d_url_edit'][$i] : '';

            $sql=" update LH_BANNER_LEAD set img_url = '$url_edit' ";
            $sql.=" where lead_img_mobile = 'd' AND  banner_lead_id = ".$_POST['banner_lead_id'][$i] ;
           
            exeQuery($sql);    
         }   
    }

    ##########################

    //add banner m
    if(isset($_FILES['lead_image_m'])){
         $numSize = count($_FILES['lead_image_m']["name"]);

        if($numSize > 0){
            for ($i = 0; $i < $numSize; $i++) {
                $seo_add = isset ($_POST ['banner_lead_m_seo'][$i] ) ? $_POST ['banner_lead_m_seo'][$i] : '';
                $url_add = isset ($_POST ['lead_image_p_m_url'][$i] ) ? $_POST ['lead_image_p_m_url'][$i] : '';

                $type_img = strrchr($_FILES["lead_image_m"]["name"][$i],".");
                $picname = "BL_". mt_rand().date('dmY').$type_img;
                $pathName = "fileupload/banner_file/" . $picname ;
                $fname = "fileupload/banner_file/Thumbnails_" . $picname;

                if (move_uploaded_file($_FILES["lead_image_m"]["tmp_name"][$i], $pathName)) {

                    $sql2 = "select seq_leade_img from LH_BANNER_MAIN m left join LH_BANNER_LEAD l on m.banner_main_id = l.banner_main_id where m.type_page_id= '$pageType' and l.lead_img_mobile = 'm' and m.lead_img_mobile = 'd' order by l.seq_leade_img desc";
                    $qeury = mssql_query($sql2);
                    $row = mssql_fetch_array($qeury);
                    $seq_leade_img=  $row['seq_leade_img']+1;

                    $sql = "INSERT INTO LH_BANNER_LEAD (banner_main_id,lead_path,lead_img_mobile,seq_leade_img,seo_lead,img_url)  ";
                    $sql .= "  values ($banner_main_id,'$pathName','m',$seq_leade_img,'$seo_add','$url_add')";
                    exeQuery($sql);
                    BannerHelper::genFacebookImg($pathName,$fname);
                   
                }
            }
        }
    }

    //edit banner m
    if(isset($_FILES['lead_image_m_edit'])){
        $numSize = count($_FILES['lead_image_m_edit']["name"]);
        if($numSize > 0){
            for ($i = 0; $i < $numSize; $i++) {
                $seo_edit = isset ($_POST ['banner_lead_d_m_seo_edit'][$i] ) ? $_POST ['banner_lead_d_m_seo_edit'][$i] : '';
                $url_edit = isset ($_POST ['lead_image_d_m_url_edit'][$i] ) ? $_POST ['lead_image_d_m_url_edit'][$i] : '';
                $type_img = strrchr($_FILES["lead_image_m_edit"]["name"][$i],".");
                $picname = "BL_". mt_rand().date('dmY').$type_img;
                $pathName = "fileupload/banner_file/" . $picname ;
                $fname = "fileupload/banner_file/Thumbnails_" .$picname;
                if (move_uploaded_file($_FILES["lead_image_m_edit"]["tmp_name"][$i], $pathName)) {

                    $sql=" update LH_BANNER_LEAD set lead_path = '$pathName',seo_lead = '$seo_edit',img_url = '$url_edit' ";
                    $sql.=" where banner_lead_id = ".$_POST['lead_image_m_id'][$i] ;
                    
                    exeQuery($sql);
                    BannerHelper::genFacebookImg($pathName,$fname);
                }
            }
        }
    }


    //sdit seo mb   
    if(isset($_POST['banner_lead_d_m_seo_edit'])){
         $numSize = count($_POST['banner_lead_d_m_seo_edit']);
         for ($i = 0; $i < $numSize; $i++) {
            
            $seo_edit_m = isset ($_POST ['banner_lead_d_m_seo_edit'][$i] ) ? $_POST ['banner_lead_d_m_seo_edit'][$i] : '';
            $url_edit_m = isset ($_POST ['lead_image_p_m_url_edit'][$i] ) ? $_POST ['lead_image_p_m_url_edit'][$i] : '';

            $sql=" update LH_BANNER_LEAD set img_url = '$url_edit_m',seo_lead = '$seo_edit_m' ";
            $sql.=" where banner_lead_id = ".$_POST['lead_image_m_id'][$i] ;
            exeQuery($sql);  

         }   
    }
  
 

    // url image d edit
    if(isset($_POST['lead_image_d_m_url_edit'])){
         $numSize = count($_POST['lead_image_d_m_url_edit']);
         for ($i = 0; $i < $numSize; $i++) {
            
            $url_edit = isset ($_POST ['lead_image_d_m_url_edit'][$i] ) ? $_POST ['lead_image_d_m_url_edit'][$i] : '';

            $sql=" update LH_BANNER_LEAD set img_url = '$url_edit' ";
            $sql.=" where banner_lead_id = ".$_POST['lead_image_m_id'][$i] ;
            exeQuery($sql);    
         }   
    }
   

    // END defuail
    ##################### NOT Banner Lead 
   // Banner Lead
    if($banner != 'on'){
         $sql2 ="SELECT * FROM LH_BANNER_MAIN WHERE type_page_id  = '$pageType' AND lead_img_mobile != 'd'";
        $qeury = mssql_query($sql2);
        $result = mssql_fetch_assoc($qeury);
        $banner_main_id =  $result['banner_main_id'];

       $sql2=" update LH_BANNER_MAIN set type_show_id = '0' ,color_code_activity = '',
                 pic_activity  =NULL ,url_activity =NULL ,url_lead =NULL,url_vdo =NULL,
                 text_activity_en =NULL,text_activity_th =NULL,thumnail_path =NULL 
                 where banner_main_id = ".$banner_main_id ;
        exeQuery($sql2);

         $sql3="DELETE FROM LH_BANNER_LEAD WHERE banner_main_id = '$banner_main_id' ";
        exeQuery($sql3);

        $sql=" update LH_BANNER_MAIN set type_page_id= $pageType ";
        $sql.=" ,startdate='1900-01-01 00:00:00',enddate='1900-01-01 00:00:00',updateby=$group_id ,updatedate='$today' ";
        $sql.=" where lead_img_mobile != 'd' AND  banner_main_id = '$banner_main_id' ";
        exeQuery($sql);

        header("Location:lhbanner.php?saveEdit=success");
        exit();

    }else{
        $bannerChoose = isset ( $_POST ['bannerchoose'] ) ? $_POST ['bannerchoose'] : '';
        $bannerChoose;
        if($bannerChoose == 1){
             //add banner p
            $sql=" update LH_BANNER_MAIN set type_show_id=0 ,type_page_id= $pageType ,text_activity_th = NULL , text_activity_en = NULL,pic_activity =NULL,color_code_activity=NULL";
            $sql.=" ,startdate='$startdate',enddate='$enddate',updateby=$group_id ,updatedate='$today' WHERE banner_main_id = '$bannerMainId' ";
            exeQuery($sql);

                if(isset($_FILES['lead_image_p'])){
                    $numSize = count($_FILES['lead_image_p']["name"]);
                    if($numSize > 0){
                        for ($i = 0; $i < $numSize; $i++) {
         

                            $type_img = strrchr($_FILES["lead_image_p"]["name"][$i],".");
                            $picname = "BL_". mt_rand().date('dmY').$type_img;
                            $pathName = "fileupload/banner_file/" . $picname ;
                            $fname = "fileupload/banner_file/Thumbnails_" . $picname;

                            if (move_uploaded_file($_FILES["lead_image_p"]["tmp_name"][$i], $pathName)) {

                              $sql2 = "select seq_leade_img from LH_BANNER_MAIN m left join LH_BANNER_LEAD l on m.banner_main_id = l.banner_main_id where m.type_page_id= '$pageType' and l.lead_img_mobile = 'p' order by l.seq_leade_img desc";
                              $qeury = mssql_query($sql2);
                              $row = mssql_fetch_array($qeury);
                              $seq_leade_img=  $row['seq_leade_img']+1;

                              $seo = $_POST['lead_image_p_seo'][$i];
                              $url = $_POST['lead_image_p_url'][$i];

                              $sql = "INSERT INTO LH_BANNER_LEAD (banner_main_id,lead_path,lead_img_mobile,seq_leade_img,seo_lead,img_url)  ";
                              $sql .= "  values ($bannerMainId,'$pathName','p',$seq_leade_img,'$seo','$url')";
                                exeQuery($sql);
                                BannerHelper::genFacebookImg($pathName,$fname);
                            }
                        }
                    }
                }

            //add banner p M
            if(isset($_FILES['lead_image_p_m'])){
                $numSize = count($_FILES['lead_image_p_m']["name"]);
                if($numSize > 0){
                    for ($i = 0; $i < $numSize; $i++) {
                        $type_img = strrchr($_FILES["lead_image_p_m"]["name"][$i],".");
                        $picname = "BM_". mt_rand().date('dmY').$type_img;
                        $pathName = "fileupload/banner_file/" . $picname ;
                        $fname = "fileupload/banner_file/Thumbnails_" . $picname;

                        if (move_uploaded_file($_FILES["lead_image_p_m"]["tmp_name"][$i], $pathName)) {

                            $sql2 = "select seq_leade_img from LH_BANNER_MAIN m left join LH_BANNER_LEAD l on m.banner_main_id = l.banner_main_id where m.type_page_id= '$pageType' and l.lead_img_mobile = 'm' and m.lead_img_mobile = 'h' order by l.seq_leade_img desc";
                            $qeury = mssql_query($sql2);
                            $row = mssql_fetch_array($qeury);
                            $seq_leade_img=  $row['seq_leade_img']+1;

                            $seo  =$_POST['lead_image_p_m_seo'][$i];
                            $img_url = $_POST['lead_image_p_m_url'][$i];

                            $sql = "INSERT INTO LH_BANNER_LEAD (banner_main_id,lead_path,lead_img_mobile,seq_leade_img,seo_lead,img_url)  ";
                            $sql .= "  values ($bannerMainId,'$pathName','m',$seq_leade_img,'$seo','$img_url' )";
                            exeQuery($sql);
                            BannerHelper::genFacebookImg($pathName,$fname);
                        }
                    }
                }
            }

            //edit banner p
            if(isset($_FILES['lead_image_p_edit'])){
                $numSize = count($_FILES['lead_image_p_edit']["name"]);
                if($numSize > 0){
                    for ($i = 0; $i < $numSize; $i++) {

                        
                        $seo_p_edit = isset ($_POST ['lead_image_p_seo_edit'][$i] ) ? $_POST ['lead_image_p_seo_edit'][$i] : '';
                        $url_p_edit = isset ($_POST ['lead_image_p_url_edit'][$i] ) ? $_POST ['lead_image_p_url_edit'][$i] : '';

                        $type_img = strrchr($_FILES["lead_image_p_edit"]["name"][$i],".");
                        $picname = "BL_". mt_rand().date('dmY').$type_img;
                        $pathName = "fileupload/banner_file/BL_" . $picname ;
                        $fname = "fileupload/banner_file/Thumbnails_BL_" .$picname;
                        if (move_uploaded_file($_FILES["lead_image_p_edit"]["tmp_name"][$i], $pathName)) {

                            $sql=" update LH_BANNER_LEAD set lead_path = '$pathName',seo_lead = '$seo_p_edit',img_url = '$url_p_edit' ";
                            $sql.=" where banner_lead_id = ".$_POST['banner_lead_p_id'][$i] ;

                            exeQuery($sql);
                            BannerHelper::genFacebookImg($pathName,$fname);
                        }
                    }
                }
            }

            //edit seo p 
            if(isset($_POST['lead_image_p_seo_edit'])){
                 $numSize = count($_POST['lead_image_p_seo_edit']);
                 for ($i = 0; $i < $numSize; $i++) {
                    
                    $seo_edit = isset ($_POST ['lead_image_p_seo_edit'][$i] ) ? $_POST ['lead_image_p_seo_edit'][$i] : '';
                    $url_edit = isset ($_POST ['lead_image_p_url_edit'][$i] ) ? $_POST ['lead_image_p_url_edit'][$i] : '';

                    $sql=" update LH_BANNER_LEAD set img_url = '$url_edit',seo_lead = '$seo_edit' ";
                    $sql.=" where lead_img_mobile = 'p' AND banner_lead_id = ".$_POST['banner_lead_p_id'][$i] ;
                    
                    exeQuery($sql);    
                 }   
            }
                      // url image p edit
            if(isset($_POST['lead_image_p_url_edit'])){
                 $numSize = count($_POST['lead_image_p_url_edit']);
                 for ($i = 0; $i < $numSize; $i++) {
                    
                    $url_edit = isset ($_POST ['lead_image_p_url_edit'][$i] ) ? $_POST ['lead_image_p_url_edit'][$i] : '';

                    $sql=" update LH_BANNER_LEAD set img_url = '$url_edit' ";
                    $sql.=" where lead_img_mobile = 'p' AND  banner_lead_id = ".$_POST['banner_lead_p_id'][$i] ;
                   
                    exeQuery($sql);    
                 }   
            }

            //edit banner p M
            if(isset($_FILES['lead_image_p_m_edit'])){
                $numSize = count($_FILES['lead_image_p_m_edit']["name"]);
                if($numSize > 0){
                    for ($i = 0; $i < $numSize; $i++) {
                        $type_img = strrchr($_FILES["lead_image_p_m_edit"]["name"][$i],".");
                        $picname = "BL_". mt_rand().date('dmY').$type_img;
                        $pathName = "fileupload/banner_file/BL_" . $picname ;
                        $fname = "fileupload/banner_file/Thumbnails_BL_" .$picname;
                        if (move_uploaded_file($_FILES["lead_image_p_m_edit"]["tmp_name"][$i], $pathName)) {
                            
                            $seo = $_POST['lead_image_p_m_seo_edit'][$i];
                            $url = $_POST['lead_image_p_m_url_edit'][$i];

                            $sql=" update LH_BANNER_LEAD set lead_path = '$pathName',seo_lead = '$seo',img_url='$url' ";
                            $sql.=" where banner_lead_id = ".$_POST['banner_lead_p_m_id'][$i] ;

                            exeQuery($sql);
                            BannerHelper::genFacebookImg($pathName,$fname);
                        }
                    }
                }
            }

            //edit seo p M
            if(isset($_POST['banner_lead_p_m_seo_edit'])){
                 $numSize = count($_POST['banner_lead_p_m_seo_edit']);
                 for ($i = 0; $i < $numSize; $i++) {
                    
                    $seo_edit = isset ($_POST ['banner_lead_p_m_seo_edit'][$i] ) ? $_POST ['banner_lead_p_m_seo_edit'][$i] : '';
                    $url_edit = isset ($_POST ['lead_image_p_m_url_edit'][$i] ) ? $_POST ['lead_image_p_m_url_edit'][$i] : '';

                    $sql=" update LH_BANNER_LEAD set img_url = '$url_edit',seo_lead = '$seo_edit' ";
                    $sql.=" where lead_img_mobile = 'm' AND banner_lead_id = ".$_POST['banner_lead_p_m_id'][$i] ;
                    exeQuery($sql);    
                 }   
            }
            // url image p edit M
            if(isset($_POST['lead_image_p_m_url_edit'])){
                 $numSize = count($_POST['lead_image_p_m_url_edit']);
                 for ($i = 0; $i < $numSize; $i++) {
                    
                    $url_edit = isset ($_POST ['lead_image_p_m_url_edit'][$i] ) ? $_POST ['lead_image_p_m_url_edit'][$i] : '';

                    $sql=" update LH_BANNER_LEAD set img_url = '$url_edit' ";
                    $sql.=" where lead_img_mobile = 'm' AND  banner_lead_id = ".@$_POST['banner_lead_p_m_id'][$i] ;
                    exeQuery($sql);    
                 }   
            }
            

        }else if($bannerChoose == 2) {
            //Delete_leadeBannerAll($bannerMainId);
            $sql = "DELETE FROM LH_BANNER_LEAD WHERE banner_main_id = '$bannerMainId'";
            exeQuery ( $sql );
            //page 1-9 set banner activity
            $color = isset ( $_POST['activitycolor'] ) ? $_POST['activitycolor'] : '';
            $editTh = isset ( $_POST['editTh'] ) ? $_POST['editTh'] : '';
            $editEn = isset ( $_POST['editEn'] ) ? $_POST['editEn'] : '';
            $picPath='';
            //Add 
            if (! empty ($_FILES ['activityimg']["tmp_name"] )) {
                $picname = "AV_". mt_rand ();
                $savePath = "fileupload/banner_file/" . $picname . ".jpg";
                $fname = "fileupload/banner_file/Thumbnails_" .$picname;
                move_uploaded_file ( $_FILES ['activityimg']["tmp_name"], $savePath );
                $picPath=$savePath;
                BannerHelper::genFacebookImg($savePath,$fname);
            }
            //Edit 
            if (! empty ($_FILES ['activityimg_edit']["tmp_name"] )) {
                $picname = "AV_". mt_rand ();
                $savePath = "fileupload/banner_file/" . $picname . ".jpg";
                $fname = "fileupload/banner_file/Thumbnails_" .$picname;
                move_uploaded_file ( $_FILES ['activityimg_edit']["tmp_name"], $savePath );
                $picPath=$savePath;
                BannerHelper::genFacebookImg($savePath,$fname);
            }

            $seo = isset($_POST['seo_lead_banner'][0] ) ? $_POST['seo_lead_banner'][0] : '';

            $sql=" update LH_BANNER_MAIN set type_show_id=1 ,type_page_id= $pageType,seo_main = '$seo' ";
            $sql.=" ,startdate='$startdate',enddate='$enddate',updateby=$group_id ,updatedate='$today' ";
            $sql.=" ,color_code_activity='$color' ";
            $sql.=" ,text_activity_th='".mssql_escape($editTh)."' ";
            $sql.=" ,text_activity_en='".mssql_escape($editEn)."' ";
            if (! empty ($_FILES ['activityimg_edit']["tmp_name"] )) {
                 $sql.=" , pic_activity='$picPath' ";
            }
            if (! empty ($_FILES ['activityimg']["tmp_name"] )) {
                 $sql.=" , pic_activity='$picPath' ";
            }
            $sql.=" where banner_main_id =".$bannerMainId;
          
            exeQuery ( $sql );



        }else if($bannerChoose == 3){
            //Delete_leadeBannerAll($bannerMainId);
            $sql = "DELETE FROM LH_BANNER_LEAD WHERE banner_main_id = '$bannerMainId'";
            exeQuery ( $sql );

            $upvido = isset ( $_POST ['upvido'] ) ? $_POST ['upvido'] : '';
            if(!empty($upvido) && $upvido=='youtube'){
                $urlyoutube= isset ( $_POST ['url_youtube'] ) ? $_POST ['url_youtube'] : '';
                $picPath='';
                if (! empty ($_FILES ["thumbnailYoutube"] ["tmp_name"] )) { //url_youtube,thumbnailYoutube
                    $picname = "TY_". mt_rand ();
                    $savePath = "fileupload/banner_file/" . $picname . ".jpg";
                    $fname = "fileupload/banner_file/Thumbnails_" .$picname;
                    move_uploaded_file ( $_FILES ["thumbnailYoutube"] ["tmp_name"], $savePath );
                    $picPath=$savePath;
                    BannerHelper::genFacebookImg($savePath,$fname);
                }

                $seo = isset($_POST['youtube_img_seo'][0] ) ? $_POST['youtube_img_seo'][0] : '';

                $sql=" update LH_BANNER_MAIN set type_show_id=3 ,type_page_id= $pageType ,text_activity_th = NULL , text_activity_en = NULL,pic_activity =NULL,color_code_activity=NULL,seo_main = '$seo' ";
                $sql.=",startdate='$startdate',enddate='$enddate',updateby=$group_id ,updatedate='$today' ";
                $sql.=",url_vdo='$urlyoutube' ";
                if(!empty($picPath))
                    $sql.=" , thumnail_path='$picPath' ";
                $sql.=" where banner_main_id =".$bannerMainId;
                  //echo $sql;
                exeQuery ( $sql );
            }else if(!empty($upvido) && $upvido=='vdo'){

                $seo = isset($_POST['vdo_img_seo'][0] ) ? $_POST['vdo_img_seo'][0] : '';

                $sql=" update LH_BANNER_MAIN set type_show_id=2 ,type_page_id= $pageType ,text_activity_th = NULL , text_activity_en = NULL,pic_activity =NULL,color_code_activity=NULL,seo_main = '$seo'";
                $sql.=" ,startdate='$startdate',enddate='$enddate',updateby=$group_id ,updatedate='$today' ";
                $picVDO='';
                $fileVDO='';
                if (! empty ($_FILES ["vdofile"] ["tmp_name"] )){
                    //video
                    $videoname = "VDO_". mt_rand ();
                    $savePath = "fileupload/banner_file/" . $videoname . ".mp4";
                    move_uploaded_file ( $_FILES ["vdofile"] ["tmp_name"], $savePath );
                    $fileVDO=$savePath;

                }
                if(! empty ( $_FILES ["thumbnailVDO"] ["tmp_name"] )){
                    //image
                    $picname = "TV_". mt_rand ();
                    $savePic = "fileupload/banner_file/" . $picname . ".jpg";
                    $fname = "fileupload/banner_file/Thumbnails_" .$picname;
                    move_uploaded_file ( $_FILES ["thumbnailVDO"] ["tmp_name"], $savePic );
                    $picVDO=$savePic;
                    BannerHelper::genFacebookImg($savePic,$fname);
                }
                if(!empty($fileVDO))
                    $sql.=" , url_vdo='$fileVDO' ";
                if(!empty($picVDO))
                    $sql.=" , thumnail_path='$picVDO' ";

                $sql.=" where banner_main_id =".$bannerMainId;
                exeQuery ( $sql );
            }

            //edit youtube_edit
            if(! empty ($_FILES ["thumbnailYoutube_edit"] ["tmp_name"] )){
                $urlyoutube= isset ( $_POST ['url_youtube'] ) ? $_POST ['url_youtube'] : '';
                $picPath='';
               
                    $picname = "TY_". mt_rand ();
                    $savePath = "fileupload/banner_file/" . $picname . ".jpg";
                    $fname = "fileupload/banner_file/Thumbnails_" .$picname;
                    move_uploaded_file ( $_FILES ["thumbnailYoutube_edit"] ["tmp_name"], $savePath );
                    $picPath=$savePath;
                    BannerHelper::genFacebookImg($savePath,$fname);

                    $sql=" update LH_BANNER_MAIN set type_show_id=3 ,type_page_id= $pageType ,text_activity_th = NULL , text_activity_en = NULL,pic_activity =NULL,color_code_activity=NULL";
                    $sql.=",startdate='$startdate',enddate='$enddate',updateby=$group_id ,updatedate='$today' ";
                    $sql.=",url_vdo='$urlyoutube' ";
                    if(!empty($picPath))
                        $sql.=" , thumnail_path='$picPath' ";
                    $sql.=" where banner_main_id =".$bannerMainId;
                    exeQuery ( $sql );
                
            }

            //edit vdofile_edit
            if (! empty ($_FILES ["vdofile_edit"] ["tmp_name"] )){

                $sql=" update LH_BANNER_MAIN set type_show_id=2 ,type_page_id= $pageType ,text_activity_th = NULL , text_activity_en = NULL,pic_activity =NULL,color_code_activity=NULL";
                $sql.=" ,startdate='$startdate',enddate='$enddate',updateby=$group_id ,updatedate='$today' ";
                $picVDO='';
                $fileVDO='';
                if (! empty ($_FILES ["vdofile_edit"] ["tmp_name"] )){
                    //video
                    $videoname = "VDO_". mt_rand ();
                    $savePath = "fileupload/banner_file/" . $videoname . ".mp4";
                    move_uploaded_file ( $_FILES ["vdofile_edit"] ["tmp_name"], $savePath );
                    $fileVDO=$savePath;

                }
                if(! empty ( $_FILES ["thumbnailVDO_edit"] ["tmp_name"] )){
                    //image
                    $picname = "TV_". mt_rand ();
                    $savePic = "fileupload/banner_file/" . $picname . ".jpg";
                    $fname = "fileupload/banner_file/Thumbnails_" .$picname;
                    move_uploaded_file ( $_FILES ["thumbnailVDO_edit"] ["tmp_name"], $savePic );
                    $picVDO=$savePic;
                    BannerHelper::genFacebookImg($savePic,$fname);
                }
                if(!empty($fileVDO))
                    $sql.=" , url_vdo='$fileVDO' ";
                if(!empty($picVDO))
                    $sql.=" , thumnail_path='$picVDO' ";

                $sql.=" where banner_main_id =".$bannerMainId;
                exeQuery ( $sql );

                
            }


        }

    

    }

    

    header("Location:lhbanner.php?saveEdit=success");
    exit;

}