<?php
session_start();
$_SESSION['group_id'];
include_once('include/dbBrands.php');
header( 'Content-Type:text/html; charset=utf8');
$_GET['page']='seo';


if (isset($_SESSION['add'])) {
  if($_SESSION['add'] ==1){
        //header("location:product_add.php");
    $success ="success";
    unset($_SESSION["add"]);
  } else if($_SESSION['add'] ==0){

        $error="error"; //ค่าซ้ำ
        $_SESSION['add']='';

      }elseif ($_SESSION['add'] == -2) {
         $error_url="error_url";
         $_SESSION['add']='';
      }else if($_SESSION['add'] ==-1){
        $errors="errors";
        unset($_SESSION["add"]);
      }else if($_SESSION['add'] ==-4){//ไม่สามารถ save ได้
        $error_url="notallow";
        unset($_SESSION["add"]);
      }
    }

    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <!-- Meta, title, CSS, favicons, etc. -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>LAND & HOUSES</title>

      <!-- Bootstrap -->
      <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome -->
      <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <!-- NProgress -->
      <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
      <!-- bootstrap-progressbar -->
      <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
      <!-- Custom Theme Style -->
      <link href="../build/css/custom.min.css" rel="stylesheet">

      <!-- jQuery -->
      <script src="../vendors/jquery/dist/jquery.min.js"></script>

      <!-- uploade img -->
      <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
      <!--uploade-->
      <!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
      <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
      <!-- confirm-->
      <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
      <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>


      <style>
      .file-caption-main .btn-file {
        overflow: visible;
      }

      .file-caption-main .btn-file .error {
        position: absolute;
        bottom: -32px;
        right: 30px;

      }
    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/lh.jpg" alt="..." ><span></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <?php include './master/navbar.php';?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include './master/top_nav.php'; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><a href="seo.php">ระบบจัดการข้อมูล SEO</a> > สร้าง SEO </h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <p class="font-gray-dark">
                    </p><br>
                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="form-group">
                        <div class="alert alert-<?=($error_url=='notallow'?'danger':'info');?>  col-md-6" id="alert">
                          <button type="button" class="close" data-dismiss="alert">x</button>
                          <strong> ระบบไม่รองรับสำหรับ Project Review, Living Tip และ Landing Page กรุณาแก้ไขข้อมูลในแต่ละบทความได้โดยตรง </strong>
                        </div>
                      </div>
                      <div class="col-md-4"></div>
                    </div>
                    <?php if(isset($success)){?>
                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="form-group">
                        <div class="alert alert-success col-md-6" id="alert">
                          <button type="button" class="close" data-dismiss="alert">x</button>
                          <strong>สร้างข้อมูลเรียบร้อยแล้ว</strong>
                        </div>
                      </div>
                      <div class="col-md-6"></div>
                    </div>
                    <?php }else if(isset($error)){?>
                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="form-group">
                        <div class="alert alert-danger col-md-6" id="alert">
                          <button type="button" class="close" data-dismiss="alert">x</button>
                          <strong>มีชื่อนี้อยู่ในระบบแล้ว</strong>
                        </div>
                      </div>
                      <div class="col-md-4"></div>
                    </div>
                    <?php }else if($error_url=="error_url"){?>
                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="form-group">
                        <div class="alert alert-danger col-md-6" id="alert">
                          <button type="button" class="close" data-dismiss="alert">x</button>
                          <strong>มี URL นี้อยู่ในระบบแล้ว</strong>
                        </div>
                      </div>
                      <div class="col-md-4"></div>
                    </div>
                    <?php }else if(isset($errors)){?>

                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="form-group">
                        <div class="alert alert-danger col-md-6" id="alert">
                          <button type="button" class="close" data-dismiss="alert">x</button>
                          <strong>เกิดข้อผิดพลาด !</strong> ไม่สามารถบันทึกข้อมมูลได้
                        </div>
                      </div>
                      <div class="col-md-4"></div>
                    </div>
                    <?php } echo $_SESSION['add'];?>
                    <form class="form-horizontal form-label-left" action="save_seo.php" method="post" enctype="multipart/form-data" id="commentForm">
                      <div class="form-group">
                        <label class="control-label col-md-2" for="first-name">Name<span class="required">*</span>
                        </label>
                        <div class="col-md-6">
                          <input type="text"  class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อ Name" name="name_tag" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-2" for="first-name">URL<span class="required">*</span>
                        </label>
                        <div class="col-md-6">
                          <input type="url"   class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อ URL" name="url_page" value="">
                        </div>
                      </div>

                      <!-- SEO -->
                      <div class="form-group">
                        <label class="control-label col-md-2" for="first-name">Titlt SEO (อังกฤษ)<br>(กรอกได้ไม่เกิน 70 คำ)
                          <span class="required">*</span>
                        </label>
                        <div class="col-md-6">
                          <input type="text"  required class="form-control col-md-7 col-xs-12" value="" name="landing_page_title_seo_th" placeholder="กรอก Titlt SEO" maxlength="70">
                        </div>
                      </div>
                      <div class="form-group hide-for-th">
                        <label class="control-label col-md-2" for="first-name">Titlt SEO (อังกฤษ)<br>(กรอกได้ไม่เกิน 70 คำ)
                          <span class="required">*</span>
                        </label>
                        <div class="col-md-6">
                          <input type="text"   class="form-control col-md-7 col-xs-12" value="" name="landing_page_title_seo_en" placeholder="กรอก Titlt SEO" maxlength="70">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-2" for="first-name">H1 <br>(กรอกได้ไม่เกิน 255 คำ)
                        </label>
                        <div class="col-md-6">
                          <input type="text"   class="form-control col-md-7 col-xs-12" value="" name="h1" placeholder="กรอก h1" maxlength="255">
                        </div>
                      </div> 


                      <div class="form-group">
                        <label class="control-label col-md-2" for="first-name">Description SEO  (อังกฤษ)<br>(กรอกได้ไม่เกิน 160 คำ)
                          <span class="required">*</span>
                        </label>
                        <div class="col-md-6">
                          <textarea required class="form-control col-md-7 col-xs-12" value="" name="landing_page_description_th" placeholder="กรอก Description SEO" rows="4" maxlength="160"></textarea> 
                        </div>
                      </div>
                      <div class="form-group hide-for-th">
                        <label class="control-label col-md-2" for="first-name">Description SEO (อังกฤษ) <br>(กรอกได้ไม่เกิน 160 คำ)
                          <span class="required">*</span>
                        </label>
                        <div class="col-md-6">
                          <textarea  class="form-control col-md-7 col-xs-12" value="" name="landing_page_description_en" placeholder="กรอก Description SEO" rows="4" maxlength="160"></textarea> 
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-2" for="first-name">Keyword SEO (อังกฤษ)
                          <span class="required">*</span>
                        </label>
                        <div class="col-md-6">
                          <textarea required class="form-control col-md-7 col-xs-12" value="" name="keyword_seo_th" placeholder="กรอก keyword SEO" rows="4"></textarea> 
                        </div>
                      </div>
                      <div class="form-group hide-for-th">
                        <label class="control-label col-md-2" for="first-name">Keyword SEO (อังกฤษ)
                          <span class="required">*</span>
                        </label>
                        <div class="col-md-6">
                          <textarea  class="form-control col-md-7 col-xs-12" value="" name="keyword_seo_en" placeholder="กรอก keyword SEO" rows="4"></textarea> 
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-2" for="first-name">Thumnail SEO <br>นามสกุล .jpg .jpeg<br> ขนาด 600 x 338 px <br> ภาพขนาดไม่เกิน 300 KB <span class="required">*</span>
                        </label><br>
                        <div class="col-md-6">
                          <input id="fileupload_seo" name="fileupload_th" type="file" multiple class="file-loading" accept="image/*" >
                        </div>
                      </div>

                      <script>
                        $("#fileupload_seo").fileinput({
                        uploadUrl: "upload.php", // server upload action
                        maxFileCount: 1,
                        allowedFileExtensions: ["jpg", "jpeg"],
                        browseLabel: 'เลือกรูป',
                        removeLabel: 'ลบ',
                        browseClass: 'btn btn-success',
                        showUpload: false,
                        showRemove:false,
                        showCaption: false, // ปิดช่องแสดงชื่อไฟล์
                        msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                       minImageWidth: 600, //ขนาด กว้าง ต่ำสุด
                       minImageHeight: 338, //ขนาด ศุง ต่ำสุด
                       maxImageWidth: 600, //ขนาด กว้าง ต่ำสุด
                       maxImageHeight: 338, //ขนาด ศุง ต่ำสุด
                       msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                       msgZoomModalHeading: 'ตัวอย่างละเอียด',
                       xxx:'landing_img_seo',
                       dropZoneTitle : 'รูป Thumnail SEO',
                       maxFileSize :300 ,
                       msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                       msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                       msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                       msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                       msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                     });
                   </script>


                   <center><br>
                    <div class="col-md-6">
                      <button type="submit" name="submit" class="btn btn-success">Submit</button>
                      <!--<button type="button" name="delete" class="btn btn-danger">Delete</button>-->
                    </div>
                  </center>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

      </div>
    </div>


    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <script type="text/javascript" src="js/bootstrap-imgupload.js"></script>
    <!-- Validate -->
    <script src="../build/js/jquery.validate.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>


    <script type="text/javascript">
      $('.fancybox').fancybox();
    </script>
    <script type="text/javascript">
      $('.imgupload').imgupload();
    </script>
    <script type="text/javascript">
      $('#imgupload2').imgupload();
    </script>
    <script type="text/javascript">
      $('#imgupload1').imgupload();
    </script>




    <script>
      $(document).ready(function() {

        $("#commentForm").validate({
          rules: {
            name_tag : "required",
            url_page : {required : true,url: true},
            fileupload_th : "required",
            landing_page_title_seo_th: {required: true,maxlength: 70},
            //landing_page_title_seo_en: {required: true,maxlength: 70},
            landing_page_description_th:{required: true,maxlength: 160},
            //landing_page_description_en:{required: true,maxlength: 160},
            keyword_seo_th: "required",
            //keyword_seo_en: "required",
          },
          messages: {
            name_tag : "กรุณากรอกชื่อ SEO",
            url_page : {required : "กรุณากรอก Url", url : "Url ไม่ถูกต้อง"},
            landing_page_title_seo_th:"กรุณากรอก Title SEO",
            fileupload_th : " &nbsp; กรุณาเลือกรูป !",
            keyword_seo_th : "กรุณากรอก Keyword SEO ",
            keyword_seo_en : "กรุณากรอก Keyword SEO ",
            landing_page_description_th:{
              required: "กรุณากรอก Description SEO",
              maxlength:  "กรุณากรอก ไม่เกิน 160 ตัวอักษร !"
            },
            landing_page_description_en:{
              required: "กรุณากรอก Description SEO",
              maxlength:  "กรุณากรอก ไม่เกิน 160 ตัวอักษร !"
            },
            landing_page_title_seo_th:{
              required: "กรุณากรอก Title SEO",
              maxlength:  "กรุณากรอก ไม่เกิน 70 ตัวอักษร !"
            },
            landing_page_title_seo_en:{
              required: "กรุณากรอก Title SEO",
              maxlength:  "กรุณากรอก ไม่เกิน 70 ตัวอักษร !"
            },
            h1:{
              maxlength:  "กรุณากรอก ไม่เกิน 255 ตัวอักษร !"
            }
          }
        });

          });
        </script>


        <!-- /jQuery Tags Input -->
        <script>
          $(document).ready(function(){
            $("#alert").show();
            window.scrollTo(0, 0);
            $("#alert").fadeTo(50000, 500).slideUp(500, function(){
              $("#alert").alert('close');
            });
          });
        </script>
        <script src="js/validate_file_300kb.js"></script>


      </body>
      </html>