<?php
session_start();
require_once 'include/dbConnect.php';
include './include/dbCon_mssql.php';
include './include/UpdateSlug.php';

function mssql_escape($str)
{
 if(get_magic_quotes_gpc())
 {
   $str= stripslashes($str);
 }
 return str_replace("'", "''", $str);
}

date_default_timezone_set('Asia/Bangkok');
$date       = date("YmdHis");
$numrand_th = (mt_rand());
$numrand_en = (mt_rand());

$name_tag    = mssql_escape($_POST['name_tag']);
$url_page    = mssql_escape($_POST['url_page']);

$fileupload_th = $_FILES['fileupload_th'];

$title_seo    = mssql_escape($_POST['landing_page_title_seo_th']);
$description    = mssql_escape($_POST['landing_page_description_th']);
$keyword_seo = mssql_escape($_POST['keyword_seo_th']);

$title_seo_en    = mssql_escape($_POST['landing_page_title_seo_en']);
$description_en    = mssql_escape($_POST['landing_page_description_en']);
$keyword_seo_en = mssql_escape($_POST['keyword_seo_en']);

$h1    = mssql_escape($_POST['h1']);

$dates        = date("Y-m-d H:i:s");
$id = $_POST['id'];

$query        = "SELECT COUNT(*) AS counts FROM LH_SEO WHERE name_tag = '$name_tag' AND NOT title_seo_id = '$id' ";
$query_result = mssql_query($query, $db_conn);

$count = mssql_fetch_row($query_result);
$count[0];
if ($count[0] >= 1) {
  $_SESSION['add'] = -1;
  echo '<script>window.history.go(-1);</script>';
  exit();
}

$query        = "SELECT COUNT(*) AS counts FROM LH_SEO WHERE url_page = '$url_page'  AND NOT title_seo_id = '$id'";
$query_result = mssql_query($query, $db_conn);


$count = mssql_fetch_row($query_result);
$count[0];
if ($count[0] >= 1) {
  $_SESSION['add'] = -3;
  echo '<script>window.history.go(-1);</script>';
  exit();
}

if(UpdateSlug::notAllow($url_page)==false){
  $_SESSION['add'] = '-4';
  echo '<script>window.history.go(-1);</script>';
  exit();
}


$sql    = "UPDATE LH_SEO SET  name_tag = '$name_tag',"
." title_seo = '$title_seo',"
." description_seo = '$description',"
." keyword_seo = '$keyword_seo',"
." title_seo_en = '$title_seo_en',"
." description_seo_en = '$description_en',"
." keyword_seo_en = '$keyword_seo_en',"
." url_page = '$url_page',"
." h1 = '$h1',"
." update_date = '$dates'"
." WHERE title_seo_id = '$id' ";
$result = mssql_query($sql);

$url = explode("/",$url_page);

$url_master;
// print_r($url);
// exit();


$url_ = urldecode($url[5]);
$url_2 = urldecode($url[6]);

$sel_project = "SELECT pr.project_id FROM LH_PROJECT_SEO seo 
LEFT JOIN LH_PROJECTS pr ON seo.project_id = pr.project_id 
LEFT JOIN LH_PROJECT_SUB sub ON sub.project_id = pr.project_id
WHERE pr.project_url = '".$url_."' OR pr.project_url = '".$url_2."'" ;

$resul_check_project = mssql_query($sel_project);
$row= mssql_fetch_array($resul_check_project);
$row_= mssql_num_rows($resul_check_project);


if($row_ > 0){
  $sql_ = "UPDATE LH_PROJECT_SEO SET project_title_seo_th = '$title_seo', ";
  $sql_ .=" project_des_seo_th = '$description', ";
  $sql_ .=" project_keyword_th = '$keyword_seo', ";
  $sql_ .=" project_title_seo_en = '$title_seo_en', ";
  $sql_ .=" project_des_seo_en = '$description_en', ";
  $sql_ .=" project_keyword_en = '$keyword_seo_en' ";
  $sql_ .= " WHERE project_id = '".$row['project_id']."' ";
  
}
$resul_ = mssql_query($sql_);

// echo $sql_;
// exit();

if ($fileupload_th != '') {

  $path = "fileupload/images/seo/";

  $type_th = strrchr($_FILES['fileupload_th']['name'], ".");

  $newname_th = $date . $numrand_th . $type_th;

  $path_seo = $path . $newname_th;

  if (move_uploaded_file($_FILES['fileupload_th']['tmp_name'], $path_seo)) {

    echo $sql    = "UPDATE LH_SEO SET thumnail_seo = '$path_seo' WHERE title_seo_id = '$id' ";
    $result = mssql_query($sql);
  }

}



if(!$result){
  $_SESSION['add'] = -2;
  header("location:seo_update.php?id=$id");
}else{
  $_SESSION['add'] = 1;
  header("location:seo_update.php?id=$id");
}
