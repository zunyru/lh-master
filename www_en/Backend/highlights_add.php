<?php
session_start();
$_SESSION['group_id'];
include './include/dbCon_mssql.php';
header('Content-Type:text/html; charset=utf8');
$_GET['page'] = 'lead_page';

if (isset($_SESSION['add'])) {
	if ($_SESSION['add'] == 1) {
		$success = "success";
		$_SESSION['add'] = '';
	} else if ($_SESSION['add'] == 2) {

		$error_name = "error_name"; //ค่าซ้ำ
		$_SESSION['add'] = '';

	} else if ($_SESSION['add'] == -1) {
		$error_up = "error_up";
		$_SESSION['add'] = '';
	} else if ($_SESSION['add'] == -2) {
		$update = "update";
		$_SESSION['add'] = '';
	} else if ($_SESSION['add'] == -3) {
		$error_vdo = "error_vdo";
		$_SESSION['add'] = '';
	} else if ($_SESSION['add'] == -4) {
		$error_youtube = "error_youtube";
		$_SESSION['add'] = '';
	}
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAND & HOUSES</title>

     <!-- Bootstrap -->
      <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome -->
      <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <!-- NProgress -->
      <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
      <!-- bootstrap-progressbar -->
      <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
      <!-- Custom Theme Style -->
      <!-- <link href="../build/css/custom.min.css" rel="stylesheet"> -->

      <!-- jQuery -->
      <script src="../vendors/jquery/dist/jquery.min.js"></script>

      <!-- uploade img -->
      <link href="./js/kartik-v-bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
      <!--uploade-->
<!--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
      <script src="./js/kartik-v-bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>

    <!-- Include Editor style. -->
    <link href="editor/css/froala_editor.min.css" rel="stylesheet" type="text/css" />
    <link href="editor/css/froala_style.min.css" rel="stylesheet" type="text/css" />

    <!-- Include Editor style. -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.3.4/css/froala_editor.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.3.4/css/froala_style.min.css" rel="stylesheet" type="text/css" />

    <!-- Include JS file. -->
    <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.3.4/js/froala_editor.min.js"></script> -->

    <!-- Include Code Mirror style -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">

    <!-- Include Editor Plugins style. -->
    <link rel="stylesheet" href="editor/css/plugins/char_counter.css">
    <link rel="stylesheet" href="editor/css/plugins/code_view.css">
    <link rel="stylesheet" href="editor/css/plugins/colors.css">
    <link rel="stylesheet" href="editor/css/plugins/emoticons.css">
    <link rel="stylesheet" href="editor/css/plugins/file.css">
    <link rel="stylesheet" href="editor/css/plugins/fullscreen.css">
    <link rel="stylesheet" href="editor/css/plugins/image.css">
    <link rel="stylesheet" href="editor/css/plugins/image_manager.css">
    <link rel="stylesheet" href="editor/css/plugins/line_breaker.css">
    <link rel="stylesheet" href="editor/css/plugins/quick_insert.css">
    <link rel="stylesheet" href="editor/css/plugins/table.css">
    <link rel="stylesheet" href="editor/css/plugins/video.css">

     <!-- confirm-->
    <link rel="stylesheet" type="text/css" href="../build/libs/css/jquery-confirm.css"/>
    <script type="text/javascript" src="../build/libs/js/jquery-confirm.js"></script>

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <style>
        .file-caption-main .btn-file {
            overflow: visible;
        }

        .file-caption-main .btn-file .error {
            position: absolute;
            bottom: -32px;
            right: 30px;
        }
    </style>

    <style>
        #myProgress {
            position: fixed;
            bottom: 0;
            left: 0;
            right: 0;
            top: 0;
            background: rgba(0,0,0,0.5);
            z-index: 999;
        }

        #myCenter {
            margin-top: 230px;
            color: #fff;
        }
        .btn-hidden{
            background: #fff;
        }
        /*.col-md-1 .kv-fileinput-caption{*/
            /*display: none !important;*/
        /*}*/
        /*.col-md-1 .kv-fileinput-caption + .input-group-btn {*/
            /*display: none;*/
        /*}*/
        .test{
            overflow: hidden;
            height: 0;
        }#seo_lead_img_file_vdo{
          display: none;
        }
        .form-control[readonly] { /* For Firefox */
            background-color: white;
        }

        .form-control[readonly] {
            background-color: white;
        }
     </style>



</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="project.php" class="site_title"> <img src="./images/lh.jpg" alt="..." ><span></span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <?php include './master/navbar.php';?>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php include './master/top_nav.php';?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2><a href="banner.php?tab=High">ระบบจัดการ Highlights</a> > สร้าง Highlights </h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <!-- <h4>Step1 : สร้างข้อมูล Series ภาษาอังกฤษ(*ไม่บังคับ)</h4> -->
                                <p class="font-gray-dark">
                                </p><br>
                                <?php if (isset($success)) {?>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="form-group">
                                            <div class="alert alert-success col-md-6" id="alert">
                                                <button type="button" class="close" data-dismiss="alert">x</button>
                                                <strong>สร้างข้อมูลเรียบร้อยแล้ว</strong>
                                            </div>
                                        </div>
                                        <div class="col-md-6"></div>
                                    </div>
                                <?php } else if (isset($error_name)) {?>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="form-group">
                                            <div class="alert alert-danger col-md-6" id="alert">
                                                <button type="button" class="close" data-dismiss="alert">x</button>
                                                <strong>มีชื่อนี้อยู่ในระบบแล้ว</strong>
                                            </div>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                <?php } else if (isset($error_up)) {?>

                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="form-group">
                                            <div class="alert alert-danger col-md-6" id="alert">
                                                <button type="button" class="close" data-dismiss="alert">x</button>
                                                <strong>เกิดข้อผิดพลาด !</strong> ไม่สามารถบันทึกข้อมูลได้
                                            </div>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                <?php } else if (isset($error_youtube)) {?>
                                <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="form-group">
                                            <div class="alert alert-danger col-md-6" id="alert">
                                                <button type="button" class="close" data-dismiss="alert">x</button>
                                                <strong>เกิดข้อผิดพลาด !</strong> ใส่ข้อมูลสื่อ VDO Youtube ไม่ครบ
                                            </div>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                    <?php } else if (isset($error_vdo)) {?>
                                <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="form-group">
                                            <div class="alert alert-danger col-md-6" id="alert">
                                                <button type="button" class="close" data-dismiss="alert">x</button>
                                                <strong>เกิดข้อผิดพลาด !</strong> ใส่ข้อมูลสื่อ VDO File ไม่ครบ
                                            </div>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                    <?php }?>
                                <form  data-toggle="validator" class="form-horizontal form-label-left" action="save_highlights.php" method="post" enctype="multipart/form-data" id="commentForm">
                                    <div class="form-group">
                                        <label class="control-label col-md-3" for="first-name">ชื่อ Highlights (อังกฤษ) <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6">
                                            <input type="text"   class="form-control col-md-7 col-xs-12" placeholder="กรอกชื่อ Highlights (อังกฤษ) " name="highlights_name_th" value=""
                                                   data-rule-required="true">
                                        </div>
                                    </div>

                                    <div class="form-group hide-for-th">
                                    <label class="control-label col-md-3" for="first-name">ชื่อ Highlights (อังกฤษ) <span class="required">*</span>
                                     </label>
                                    <div class="col-md-6">
                                        <input type="text" id="first-name2" required class="form-control col-md-7 col-xs-12" value="" name="highlights_name_en" placeholder="กรอกชื่อ Highlights (อังกฤษ)">
                                      </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">รายละเอียด Highlights (อังกฤษ)<br> ใส่ข้อความไม่เกิน 300 คำ <span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-9 col-xs-12">
                                             <!-- <div id="edit" class="text-left txt-black fr-box fr-basic fr-top" style="z-index: 1000;"></div>  -->
                                              <textarea name="highlights_dis_th" class="form-control" rows="5" maxlength="300" placeholder="กรอกรายละเอียด Highlights (อังกฤษ)" required=""></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group hide-for-th">
                                      <label class="control-label col-md-3 ">รายละเอียด Highlights (อังกฤษ) <br> ใส่ข้อความไม่เกิน 300 คำ </label>
                                        <div class="col-md-6 col-sm-9 col-xs-12">

                                             <!-- <div id="edit" class="text-left txt-black fr-box fr-basic fr-top" style="z-index: 1000;"></div>  -->
                                              <textarea name="highlights_dis_en" class="form-control" rows="5" maxlength="300" placeholder="กรอกรายละเอียด Highlights (อังกฤษ)"></textarea>

                                        </div>
                                    </div>

                        <div class="test">
                        <div class="form-group">
                          <label class="control-label col-md-11" for="first-name">&nbsp; <span class="required"></span>
                          </label>
                          <div class="col-md-1">
                              <input id="logo_series" name="logo_series" type="file" multiple class="file-loading" accept="image/*" >
                          </div>
                      </div>

                      <script>
                          $("#logo_series").fileinput({
                             // uploadUrl: "upload.php", // server upload action
                              maxFileCount: 1,
                              allowedFileExtensions: ["png"],
                              browseLabel: '&nbsp;',
                              removeLabel: 'ลบ',
                              browseClass: 'btn btn-hidden',
                              showUpload: false,
                              showRemove:false,
                              showCaption: false,
                              msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                              msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
                              msgInvalidFileExtension: 'ไฟล์ "{name}" เป็น ชนิดไฟล์ ที่ไมถูกต้อง, กรุณาลือกไฟล์ "{extensions}"',
                              msgZoomModalHeading: 'ตัวอย่างละเอียด',
                              xxx:'seo_logo_series',
                              dropZoneTitle : 'รูปโลโก้สไตล์บ้าน',
                              minImageWidth: 150, //ขนาด กว้าง ต่ำสุด
                              minImageHeight: 150, //ขนาด ศุง ต่ำสุด
                              maxImageWidth: 150, //ขนาด กว้าง ต่ำสุด
                              maxImageHeight: 150, //ขนาด ศุง ต่ำสุด
                              maxFileSize :300 ,
                             msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                             msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                             msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                             msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                             msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                          });
                      </script>
                      </div>



                                    <div class="form-group">
                                        <label class="control-label col-md-3" for="first-name">เลือกประเภทสื่อ <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6">
                                            <select id="mySelect"  class="form-control" name="page" onchange="getval(this);">
                                                <option value="image" selected>Lead Image</option>
                                                <!-- <option value="banner">Banner Activity</option> -->
                                                <option value="youtube">VDO youtube</option>
                                                <option value="vdo">VDO file</option>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Lead Image -->

                                    <div id="l1">
                                    <div class="form-group" >
                                            <label class="control-label col-md-3" for="first-name">Lead Image <span class="required">*</span>  <br> สูงสุด 1 รูป <br> (รูปขนาด 1920 x 1080 )</label>


                                        <div class="col-md-6">
                                            <input id="lead_img_file" name="lead_img_file" type="file" multiple class="file-loading" accept="image/*" >
                                        </div>
                                    </div>
                                    <script>
                                        $("#lead_img_file").fileinput({
                                            uploadUrl: "upload.php", // server upload action
                                            maxFileCount: 1,
                                            allowedFileExtensions: ["jpg", "png", "jpeg"],
                                            browseLabel: 'เลือกรูป',
                                            removeLabel: 'ลบ',
                                            browseClass: 'btn btn-success',
                                            showUpload: false,
                                            showRemove:false,
											showCaption: false,
                                            //msgFilesTooMany: 'ไฟล์ที่คุณเลือกมีจำนวน <b>({n})</b> ซึ่งเกินกว่าที่ระบบอนุญาตที่ <b>{m}</b>, กรุณาลองใหม่อีกครั้ง!',
                                            msgFilesTooMany: 'เลือกได้ สูงสุด <b>{m}</b> ไฟล์',
                                            msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                            xxx:'seo_lead_img_file',
                                            dropZoneTitle : 'รูป Lead Image',
                                            minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                            minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                            maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                            maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                            maxFileSize :300 ,
                                           msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                           msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                           msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                           msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                           msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                        });
                                    </script>


                                </div>


                                    <!-- url yputube -->
                                    <div id="l3">
                                        <div class="form-group" >
                                            <label class="control-label col-md-3" for="first-name"> Thumbnail youtube <span class="required">*</span> <br> สูงสุด 1 url   <br> (รูปขนาด 1920 x 1080 ) </label>

                                            <div class="col-md-6">
                                                <input id="img_youtube" name="img_youtube" type="file" multiple class="file-loading" accept="image/*" >
                                            </div>
                                        </div>

                                        <script>
                                            $("#img_youtube").fileinput({
                                                uploadUrl: "upload.php", // server upload action
                                                maxFileCount: 1,
                                                allowedFileExtensions: ["jpg", "png", "jpeg"],
                                                browseLabel: 'เลือกรูป',
                                                removeLabel: 'ลบ',
                                                browseClass: 'btn btn-success',
                                                showUpload: false,
                                                showRemove:false,

                                                msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                                msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                                xxx:'seo_lead_img_file',
                                                dropZoneTitle : 'ลากและวางรูป',
                                                minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                                minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                                maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                                maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                                maxFileSize :300 ,
                                               msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                               msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                               msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                               msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                               msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                            });
                                        </script>
                                        <div class="form-group" >
                                                <label class="control-label col-md-3" for="first-name">URL VDO Leade image <span class="required">*</span></label>

                                            <div class="col-md-6">
                                                <input id="url_youtube" class="form-control" name="url_youtube" type="text"  placeholder="URL Youtube" >
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Vdo file -->
                                <div id="l4">
                                    <div class="form-group" >
                                        <label class="control-label col-md-3" for="first-name">Thumbnail VDO <span class="required">*</span> <br> สูงสุด 1 ไฟล์ <br> (รูปขนาด 1920 x 1080 ) </label>


                                        <div class="col-md-6">
                                            <input id="vdo_img" name="vdo_img" type="file" multiple class="file-loading" accept="image/*" >
                                        </div>
                                    </div>


                                       <div class="form-group" >
                                           <label class="control-label col-md-3" for="first-name">VDO File  <span class="required">*</span> สูงสุด 1 ไฟล์ <br> (ไฟล์ .mp4)</label>

                                           <div class="col-md-6">
                                               <input id="vdo_file" name="vdo_file" type="file" multiple class="file-loading" accept="video/mp4" />
                                           </div>
                                       </div>
                                </div>

                                    <script>
                                        $("#vdo_img").fileinput({
                                            uploadUrl: "upload.php", // server upload action
                                            maxFileCount: 1,
                                            allowedFileExtensions: ["jpg", "png", "jpeg"],
                                            browseLabel: 'เลือกรูป',
                                            removeLabel: 'ลบ',
                                            browseClass: 'btn btn-success',
                                            showUpload: false,
                                            showRemove:false,

                                            msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                            msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                            xxx:'seo_lead_img_file',
                                            dropZoneTitle : 'ลากและวางรูป',
                                            minImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                            minImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                            maxImageWidth: 1920, //ขนาด กว้าง ต่ำสุด
                                            maxImageHeight: 1080, //ขนาด ศุง ต่ำสุด
                                            maxFileSize :300 ,
                                           msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                           msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                           msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                           msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                           msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                        });
                                    </script>
                                    <script>
                                           $("#vdo_file").fileinput({
                                               uploadUrl: "upload.php", // server upload action
                                               maxFileCount: 1,
                                               allowedFileExtensions: ["mp4"],
                                               browseLabel: 'เลือกไฟล์',
                                               removeLabel: 'ลบ',
                                               browseClass: 'btn btn-success',
                                               showUpload: false,
                                               showRemove:false,

                                               msgFilesTooMany: 'เลือกได้แค่ <b>{m}</b> ไฟล์',
                                               msgZoomModalHeading: 'ตัวอย่างละเอียด',
                                               xxx:'seo_lead_img_file_vdo',
                                               dropZoneTitle : 'ลากและวางไฟล์',

                                               //maxFileSize :300 ,
                                              msgSizeTooLarge: 'ขนาดไฟล์เกิน {maxSize} KB.',
                                              msgImageWidthSmall: 'ความกว้างน้อยกว่า {size} PX.',
                                              msgImageHeightSmall: 'ความสูงน้อยกว่า {size} PX.',
                                              msgImageWidthLarge: 'ความกว้างเกินขนาด {size} PX.',
                                              msgImageHeightLarge: 'ความสูงเกินขนาด {size} PX.',
                                           });
                                       </script>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">content Highlights (อังกฤษ) </label>
                                        <div class="col-md-6">

                                             <!-- <div id="edit" class="text-left txt-black fr-box fr-basic fr-top" style="z-index: 1000;"></div>  -->
                                              <textarea name="highlights_content" id="edit" rows="15" ></textarea>

                                        </div>
                                    </div>

                                    <div class="form-group hide-for-th">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">content Highlights (อังกฤษ) </label>
                                        <div class="col-md-6">

                                             <!-- <div id="edit" class="text-left txt-black fr-box fr-basic fr-top" style="z-index: 1000;"></div>  -->
                                              <textarea name="highlights_content_en" id="edit_en" rows="15" ></textarea>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3" for="first-name">ระบุวันเริ่ม <span class="required">*</span>
                                        </label>
                                        <div class="col-md-2">
                                            <fieldset>
                                                <div class="input-prepend input-group">
                                                    <input type="text" style="width: 180px" name="date_start" required id="date_start" class="form-control" value="" readonly/>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <label class="control-label col-md-3" for="first-name" style="margin-left: -111px;">ระบุวันสิ้นสุด <span class="required" aria-required="true" >*</span></label>
                                        <div class="col-md-2">
                                            <fieldset>
                                                <div class="input-prepend input-group">
                                                    <input type="text" style="width: 180px" name="date" required="" id="date" class="form-control " value="" aria-required="true" readonly>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="form-group" >
                                        <label class="control-label col-md-3" for="first-name">Link url <span class="required"></span></label>

                                        <div class="col-md-6">
                                            <input name="highlights_url" placeholder="EX : http://lh.co.th or https://lh.co.th" class="form-control" rows="3">
                                        </div>
                                    </div>

                                    <center><br>
                                        <div class="col-md-8">
                                            <button type="submit" name="submit" class="btn btn-success" >Submit</button>
                                            <!--<button type="button" name="delete" class="btn btn-danger">Delete</button>-->
                                        </div>
                                    </center>
                                </form>

                                <div class="form-group">
                                    <label class="control-label col-md-4" > <span class="required"></span></label>
                                    <div class="col-md-3">
                                        <div id="myProgress">
                                            <!-- <div id="myBar"></div> -->
                                            <center id="myCenter"><img  src="fileupload/images/motivo/cloud_upload_256.gif" width="50" height="50">
                                                <p>กำลังโหลด...</p></center>
                                        </div>
                                    </div>
                                </div>

                             </div>

                        </div>
                    </div>
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <!-- <footer>
                  <div class="pull-right">
                    Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
                  </div>
                  <div class="clearfix"></div>
                </footer> -->
                <!-- /footer content -->
            </div>
        </div>

     <!-- Bootstrap -->
        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../vendors/nprogress/nprogress.js"></script>
        <script type="text/javascript" src="js/bootstrap-imgupload.js"></script>
        <!-- Validate -->
        <script src="../build/js/jquery.validate.js"></script>
        <!-- date -->
        <link rel="stylesheet" type="text/css" href="../build/css/jquery-ui.css"/>
        <script src="../build/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="js/fancybox/jquery.fancybox.js" type="text/javascript"></script>
        <!-- bootstrap-progressbar -->
        <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
        <!-- Custom Theme Scripts -->


            <!-- bootstrap-daterangepicker -->
    <script src="js/moment/moment.min.js"></script>
    <script src="js/datepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

        <!--Editor_script-->

        <!-- Include JS files. -->
        <script type="text/javascript" src="editor/js/froala_editor.min.js"></script>
        <!-- Include Code Mirror. -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
        <!-- Include Plugins. -->
        <script type="text/javascript" src="editor/js/plugins/align.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/char_counter.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/code_beautifier.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/code_view.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/colors.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/emoticons.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/entities.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/file.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/font_family.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/font_size.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/fullscreen.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/image.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/image_manager.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/inline_style.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/line_breaker.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/link.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/lists.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/paragraph_format.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/paragraph_style.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/quick_insert.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/quote.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/table.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/save.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/url.min.js"></script>
        <script type="text/javascript" src="editor/js/plugins/video.min.js"></script>


        <!-- Include Language file if we want to u-->
        <script>
            $(function() {
                $('#edit').froalaEditor({
                    toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'fontFamily', 'fontSize', '|', 'specialCharacters', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', '-', '|', 'undo', 'redo', 'clearFormatting', 'selectAll', 'html', 'applyFormat', 'removeFormat', 'fullscreen', 'print'],
                    pluginsEnabled: null,
                    colorsText: [
                        '#ff00ff', '#1ABC9C', '#54ACD2', '#2C82C9', '#9365B8', '#475577', '#CCCCCC',
                        '#61BD6D', '#1ABC9C', '#54ACD2', '#2C82C9', '#9365B8', '#475577', '#CCCCCC',
                        '#41A85F', '#00A885', '#3D8EB9', '#2969B0', '#553982', '#28324E', '#000000',
                        '#F7DA64', '#FBA026', '#EB6B56', '#E25041', '#A38F84', '#EFEFEF', '#FFFFFF',
                        '#FAC51C', '#F37934', '#D14841', '#B8312F', '#7C706B', '#D1D5D8', 'REMOVE'
                    ],
                    height: 300,
                    imageUploadURL: 'uploade_highlights.php',
                    imageUploadParams: {
                        id: 'my_editor'
                    },

                    fileUploadURL: 'upload_file.php',
                    fileUploadParams: {
                        id: 'my_editor'
                    },
                    imagePaste: false,

                    imageManagerLoadURL: 'uploade_highlights.php',
                    imageManagerDeleteURL: "delete_image_highlights.php",
                    imageManagerDeleteMethod: "POST"
                })
                // Catch image removal from the editor.
                    .on('froalaEditor.image.removed', function (e, editor, $img) {
                        $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_image_highlights.php",

                            // Request params.
                            data: {
                                src: $img.attr('src')
                            }
                        })
                            .done (function (data) {
                                console.log ('image was deleted'+$img.attr('src'));
                            })
                            .fail (function (err) {
                                console.log ('image delete problem: ' + JSON.stringify(err));
                            })
                    })

                    // Catch image removal from the editor.
                    .on('froalaEditor.file.unlink', function (e, editor, link) {

                        $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_file.php",

                            // Request params.
                            data: {
                                src: link.getAttribute('href')
                            }
                        })
                            .done (function (data) {
                                console.log ('file was deleted');
                            })
                            .fail (function (err) {
                                console.log ('file delete problem: ' + JSON.stringify(err));
                            })
                    })
            });
        </script>
        <script>
            $(function() {
                $('#edit_en').froalaEditor({
                    toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'fontFamily', 'fontSize', '|', 'specialCharacters', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', '-', '|', 'undo', 'redo', 'clearFormatting', 'selectAll', 'html', 'applyFormat', 'removeFormat', 'fullscreen', 'print'],
                    pluginsEnabled: null,
                    colorsText: [
                        '#ff00ff', '#1ABC9C', '#54ACD2', '#2C82C9', '#9365B8', '#475577', '#CCCCCC',
                        '#61BD6D', '#1ABC9C', '#54ACD2', '#2C82C9', '#9365B8', '#475577', '#CCCCCC',
                        '#41A85F', '#00A885', '#3D8EB9', '#2969B0', '#553982', '#28324E', '#000000',
                        '#F7DA64', '#FBA026', '#EB6B56', '#E25041', '#A38F84', '#EFEFEF', '#FFFFFF',
                        '#FAC51C', '#F37934', '#D14841', '#B8312F', '#7C706B', '#D1D5D8', 'REMOVE'
                    ],
                    height: 300,
                    imageUploadURL: 'uploade_highlights.php',
                    imageUploadParams: {
                        id: 'my_editor'
                    },

                    fileUploadURL: 'upload_file.php',
                    fileUploadParams: {
                        id: 'my_editor'
                    },
                    imagePaste: false,

                    imageManagerLoadURL: 'uploade_highlights.php',
                    imageManagerDeleteURL: "delete_image_highlights.php",
                    imageManagerDeleteMethod: "POST"
                })
                // Catch image removal from the editor.
                    .on('froalaEditor.image.removed', function (e, editor, $img) {
                        $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_image_highlights.php",

                            // Request params.
                            data: {
                                src: $img.attr('src')
                            }
                        })
                            .done (function (data) {
                                console.log ('image was deleted'+$img.attr('src'));
                            })
                            .fail (function (err) {
                                console.log ('image delete problem: ' + JSON.stringify(err));
                            })
                    })

                    // Catch image removal from the editor.
                    .on('froalaEditor.file.unlink', function (e, editor, link) {

                        $.ajax({
                            // Request method.
                            method: "POST",

                            // Request URL.
                            url: "delete_file.php",

                            // Request params.
                            data: {
                                src: link.getAttribute('href')
                            }
                        })
                            .done (function (data) {
                                console.log ('file was deleted');
                            })
                            .fail (function (err) {
                                console.log ('file delete problem: ' + JSON.stringify(err));
                            })
                    })
            });
        </script>



        <!-- Validate -->
        <script src="../build/js/jquery.validate.js"></script>
        <script>
            $(document).ready(function() {

                $("#commentForm").validate({
                    rules: {
                        highlights_name_th: "required",
                        highlights_name_en: "required",
                        highlights_dis_th: "required",
                        url_youtube : "required",
                        img_youtube : "required",
                        images : "required",
                        vdo_img : "required",
                        vdo_file : "required",
                        project : "required",
                    },
                    messages: {
                        highlights_name_th: "กรุณากรอกชื่อ Highlights อังกฤษ !",
                        highlights_name_en: "กรุณากรอกชื่อ Highlights อังกฤษ !",
                        highlights_dis_th: "กรุณกรอกรายละเอียด Highlights !",
                        url_youtube : "กรุณากรอก URL Youtube",
                        img_youtube  :" &nbsp; กรุณาเลือกรูป !",
                        banner_image :" &nbsp; กรุณาเลือกรูป !",
                        images : " &nbsp; กรุณาเลือกรูป !",
                        vdo_img : " &nbsp; กรุณาเลือกรูป",
                        vdo_file : "&nbsp; กรุณาเลือกไฟล์",
                        'spam[]': "เลือกอย่างน้อย 1 รายการ",
                        project : "กรุณาเลือกโครงการ",
                        date_start : "กรุณาระบุวันที่เริ่มต้น !",
                        date : "กรุณาระบุวันที่สิ้นสุด !",
                    }
                });

                $("#lead_img_file").rules("add", {
                    required:true,
                    messages: {
                        required: " &nbsp; กรุณาเลือกรูป !"
                    }
                });

            });
        </script>
        <script type="text/javascript">

            function move() {

                    if (window.ActiveXObject) {
                        var fso = new ActiveXObject("Scripting.FileSystemObject");
                        var filepath = document.getElementById('vdo_file').value;
                        var thefile = fso.getFile(filepath);
                        var sizeinbytes = thefile.size;
                    } else {
                        var sizeinbytes = document.getElementById('vdo_file').files[0].size;

                    }
                    alert(sizeinbytes);

                    var fSExt = new Array('Bytes', 'KB', 'MB', 'GB');
                    fSize = sizeinbytes;
                    i = 0;
                    while (fSize > 900) {
                        fSize /= 1024;
                        i++;
                    }

                    var progess2 = (Math.round(fSize * 100) * 100) + ' ' + fSExt[i];

                    var progess = (Math.round(fSize * 100) / 100);
                    parseInt(progess);
                    //alert(progess);
                    var elem = document.getElementById("myBar");
                    var width = 1;
                    var id = setInterval(frame, 1000);

                    function frame() {
                        if (width >= 100) {
                            clearInterval(id);
                            //alert("OK");
                        } else {
                            width++;
                            //elem.style.width = width + '%';
                        }
                    }

                    $("#myProgress").show();

                }

        </script>

        <script type="text/javascript">
            $('.fancybox').fancybox();
        </script>

        <script type="text/javascript">
            $('.imgupload').imgupload();
        </script>
        <script type="text/javascript">
            $('#imgupload2').imgupload();
        </script>
        <script type="text/javascript">
            $('#imgupload1').imgupload();
        </script>




        <script>
            $(document).ready(function(){
                $("#alert").show();
                $("#alert").fadeTo(3000, 500).slideUp(500, function(){
                    $("#alert").alert('close');
                });
            });
        </script>



        <!-- /jQuery Tags Input -->
        <script>
            $(document).ready(function(){
                $("#l1").show();
                $("#l2").hide();
                $("#l3").hide();
                $("#l4").hide();
                $("#l5").hide();
                $("#l1_2").show();
            });
        </script>

        <script>

            function getval(sel){
                $("#mySelect").val();
                if($("#mySelect").val() == "image"){
                    $("#l1").show();
                    $("#l2").hide();
                    $("#l3").hide();
                    $("#l4").hide();
                }else if($("#mySelect").val() == "banner"){
                    $("#l2").show();
                    $("#l3").hide();
                    $("#l1").hide();
                    $("#l4").hide();
                }else if($("#mySelect").val() == "youtube"){
                    $("#l3").show();
                    $("#l1").hide();
                    $("#l2").hide();
                    $("#l4").hide();
                }else if($("#mySelect").val() == "vdo"){
                    $("#l4").show();
                    $("#l1").hide();
                    $("#l2").hide();
                    $("#l3").hide();
                }
            }

            // $('#project').click(function() {
            //     console.log($('#project').val());
            // });

        </script>
        <script type="text/javascript">

            $('#date_start').datepicker({
                dateFormat : "dd/mm/yy",
                autoclose: true,
                changeMonth: true,
                showDropdowns: true,
                changeYear: true,
                minDate:  new Date(),
                onSelect: function(dateText) {
                    var d = new Date($(this).datepicker("getDate"));
                    $("input#date").datepicker('option', 'minDate', dateText);
                    $("input#date").prop('disabled', false);
                }
            });
            $('#date').datepicker({
                dateFormat : "dd/mm/yy",
                autoclose: true,
                changeMonth: true,
                showDropdowns: true,
                changeYear: true,
                minDate:  new Date(),
                onSelect: function(dateText) {
                    var d = new Date($(this).datepicker("getDate"))
                    $("input#date_start").datepicker('option', 'maxDate', dateText);
                }
            });
        </script>
        <script>
            $(document).ready(function(){
                $("#myProgress").hide();
            });
        </script>
        <script src="js/validate_file_300kb.js"></script>


</body>
</html>