$(document).ready(function () {

	//Page Load Start
    preloader();
    menu();
    //centerMainNav();
    menuHomeSeries();
    submenuInnerpage();
    responsiveMenu();
    searchDropdown();
    getdirectionDropdown();
    contactDropdown()
    sortBlock();
    scrollNext();
    scrollTop();
    tabMenu();

    hideMenu();

    //radionAppoint();

    var winW = $(window).width();
    var winH = $(window).height();

    var pageH = winH - 60;
    $('.page-banner, .topdropdown-container, .page-banner-content').css('height',pageH);
    // reponsive
    if( winW < 992 ) {
        var funBlockH = winH;
        $('.topdropdown-container').css('height', funBlockH);
        $('.topdropdown-container').css('height',funBlockH);
    }

    $(window).resize(function(){
        var winW = $(window).width();
        var winH = $(window).height();
        var pageH = winH - 60;
        $('.page-banner, .topdropdown-container, .page-banner-content').css('height',pageH);


        if( winW < 992 ) {
            var funBlockH = winH;
            $('.topdropdown-container').css('height',funBlockH);
        }

    });

    // var winH = $(window).height();
    // var pageH = winH - 70

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= pageH) {
            $('.scroll-top').addClass('fadeIn');
            $('.float-share').css('bottom','90px');
            $('.ficon-shares').css('bottom','170px');
            if(winW < 992 ) {
                $('.float-share').css('bottom','115px');
                $('.ficon-shares').css('bottom','175px');
            }
        }
        else {
            $('.scroll-top').removeClass('fadeIn');
            $('.float-share').css('bottom','20px');
            $('.ficon-shares').css('bottom','100px');
            if(winW < 992 ) {
                $('.float-share').css('bottom','55px');
                $('.ficon-shares').css('bottom','115px');
            }
                //$('.main-navigation').css({'opacity': 0, 'visibility':'hidden', 'display':'none' });
            // $('.main-navigation, .top-icons, .menu-home-series').fadeOut();
        }


        if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
            $('.page-banner').css({'opacity': 0, 'visibility':'hidden' });
        }
        else{
            $('.page-banner').css({'opacity': 1, 'visibility':'visible' });
        }
    });

    var pos = $('.content').offset().top;
    //console.log(pos);

    $('.view-block li a').click(function(e){
        $('.view-block li a').removeClass('active');
        $(this).addClass('active');
    });

    $('.float-share').click(function(e){
        var status = $(this).data('status');
        if(status == 0) {
            $(this).data('status', 1)
            $('.ficon-shares').css({'opacity': 1, 'visibility':'visible' });
        }
        else {
            $(this).data('status', 0)
            $('.ficon-shares').css({'opacity': 0, 'visibility':'hidden' });
        }

        e.preventDefault();
    });

    //Page Load End
});


//Function Start

function hideMenu()
{
    $(document).click(function (event) {
        if (!$(event.target).closest(".main-navigation").length) {
            $('.m-submenu-innerpage').each(function () {
                $(this).removeClass('active');
                $(this).attr('data-status', 0);
                $(this).parent().find('.submenu-innerpage').removeClass('animate');
                $(this).parent().find('.submenu-list').removeClass('animate');
            });
        }
    });
}

function preloader() {
    $(window).on('load', function() {
        $('#preloadLogo').fadeOut();
        $('#preloader').delay(350).fadeOut('slow');
        $('body').delay(350).css({'overflow':'visible'});
    })
}

//Menu
function menu() {
    $('.top-menu .menu').click(function(e){

        $(this).removeClass('show-menu');
        $('.menu-close').addClass('show-menu');
        $('.submenu-bg').addClass('animate');
        $('.submenu-list').addClass('animate');
        $('.submenu-tool').addClass('animate');
        $('body').css('overflow', 'hidden');
        $('.main-navigation, .menu-home-series-close').css({'opacity': 0, 'visibility':'hidden' });
        $('.menu-home-series').removeClass('animate');
        $('#main-nav a.m-home-series, .m-submenu-innerpage').attr('data-status', 0).removeClass('active');

        e.preventDefault();
    });

    $('.menu-close').click(function(e){

        $(this).removeClass('show-menu');
        $('.top-menu .menu').addClass('show-menu');
        $('.submenu-bg').removeClass('animate');
        $('.submenu-list').removeClass('animate');
        $('.submenu-tool').removeClass('animate');
        $('body').css('overflow', 'auto');
        $('.main-navigation').css({'opacity': 1, 'visibility':'visible' });
        $('#main-nav a.m-home-series, .m-submenu-innerpage').attr('data-status', 0).removeClass('active');


        e.preventDefault();
    });

    $('.panel-close').click(function(e){

        $('.top-icons li a#topSearch, .top-icons li a#topGetdirection').data('status', 0).removeClass('active');
        $('.search-container, .getdirection-container, .contact-container').fadeOut(200);

        e.preventDefault();
    });


    // $('.top-menu a.menu').hover(function(){
    //     $('.top-icons li a#topSearch').data('status', 0).removeClass('active');
    //     $('.search-container').fadeOut(200);
    //     $('.top-icons li a#topGetdirection').data('status', 0).removeClass('active');
    //     $('.getdirection-container').fadeOut(200);
    // });
}

function menuHomeSeries() {
    $('#main-nav a.m-home-series').click(function(e){
        $('#main-nav a.m-home-series').removeClass('active');
        var status = $(this).attr('data-status');
        if(status == 0) {
            $(this).attr('data-status', 1).addClass('active');
            $('.menu-home-series').addClass('animate');
            $('.menu-col-left').addClass('animate');
            $('.menu-col-right').addClass('animate');
            $('.submenu-bg').removeClass('animate');
            $('.menu-home-series-close').css({'opacity': 1, 'visibility':'visible' });
        }
        else {
            $(this).attr('data-status', 0).removeClass('active');
            $('.menu-home-series').removeClass('animate');
            $('.menu-col-left').removeClass('animate');
            $('.menu-col-right').removeClass('animate');
            $('.menu-home-series-close').css({'opacity': 0, 'visibility':'hidden' });
        }

        e.preventDefault();
    });

    $('.menu-home-series-close').click(function(e) {
        $('#main-nav a.m-home-series').attr('data-status', 0).removeClass('active');
        $('.menu-home-series').removeClass('animate');
        $('.menu-col-left').removeClass('animate');
        $('.menu-col-right').removeClass('animate');
        $('.menu-home-series-close').css({'opacity': 0, 'visibility':'hidden' });
    });

}

function submenuInnerpage() {
    $('.m-submenu-innerpage').click(function(e){

        var status = $(this).attr('data-status');
        var menuName = $(this).attr('data-name');
        var parentMenu = $(this);

        $('.m-submenu-innerpage').each(function () {
            if($(this).attr('data-name') && menuName == $(this).attr('data-name')){
                if(status == 0){
                    $(this).attr('data-status', 1).addClass('active');
                    $(this).parent().find('.submenu-innerpage').addClass('animate');
                    $(this).parent().find('.submenu-list').addClass('animate');
                }else if(status == 1){
                    $(this).attr('data-status', 0).removeClass('active');
                    $(this).parent().find('.submenu-innerpage').removeClass('animate');
                    $(this).parent().find('.submenu-list').removeClass('animate');
                }
            }else{
                $(this).removeClass('active');
                if($(this).attr('data-name') && menuName != $(this).attr('data-name')){
                    $(this).attr('data-status', 0);
                    $(this).parent().find('.submenu-innerpage').removeClass('animate');
                    $(this).parent().find('.submenu-list').removeClass('animate');
                }

            }
        });



        // if(status == 0) {
        //     $(this).attr('data-status', 1).addClass('active');
        //     $( '.submenu-innerpage.'+menuName ).addClass('animate');
        //     $('.'+menuName+' .submenu-list').addClass('animate');
        // }
        // else {
        //     $(this).attr('data-status', 0).removeClass('active');
        //     $('.submenu-innerpage').removeClass('animate');
        //     $('.submenu-list').removeClass('animate');
        //     // alert('zz');
        // }

        e.preventDefault();
    });

}

function centerMainNav() {
    var mainNavW = $('#main-nav').outerWidth();
    var mainNav = -(mainNavW/2);
    $('.main-navigation').css('margin-left', mainNav+'px' );
}

function responsiveMenu() {
    var winH = $(window).height();
    $('.rps-menu').css({ 'height' : winH + 60 , 'overflow-y': 'scroll' });

    $('a.burger-menu').click(function(e){
        var active = $(this).data('active');
        if ( $(this).hasClass('menu-open') ) {
            $(this).removeClass('menu-open');
            $(this).css('overflow','auto');
            $('body').css('overflow', 'hidden');
        }
        else {
            $(this).addClass('menu-open');
            $(this).css('overflow','hidden');
            $('body').css('overflow', 'auto');
        }
        if(active == 0) {
            $(this).addClass('active');
            $('.rps-menu').fadeIn(300);
            $(this).data('active', 1);
            $('body').css('overflow', 'hidden');
        }
        else if(active == 1) {
            $(this).removeClass('active');
            $('.rps-menu').fadeOut(300);
            $(this).data('active', 0);
            $('body').css('overflow', 'auto');
        }
        e.preventDefault();

    });


    $('.rps-close').click(function(e){
        $('.rps-menu').fadeOut(300);
        $('a.burger-menu').data('active', 0);
    });

    // var tags = $(".responsive-menu li > ul").addClass("hasSubmenu");

    $('.responsive-menu li').each(function(){
        if( $(this).has('ul').length ){
            $(this).addClass('hasSubmenu');
        }
    });


    $('.responsive-menu ul li a').removeClass("active");

    $('.responsive-menu ul li').click(function() {
        if( $(this).has('ul').length ){
            $(this).children('ul').slideToggle(300);
            $(this).addClass('i-active');
            //$(this).children('a').addClass('active');

            if($(this).children('a').hasClass("active")){
                $(this).children('a').removeClass("active");
            }
            else {
                $(this).children('a').addClass("active");
            }

            return false;
        }

    });
}

function scrollNext() {
    $("#scrollNext").click(function() {
        $('html, body').animate({
            scrollTop: $("#content").offset().top - 60
        }, 800);
    });
}

function scrollTop() {

    $('.scroll-top').click(function (){
        $('html, body').animate({
            scrollTop: $('#page').offset().top
        }, 1400);
    });



}

function sortBlock() {
    $('.sort-block .sort').click(function(e){
        e.stopImmediatePropagation();
        var status = $(this).data('status');
        if( status == 0 ) {
            $('.sort-block ul').fadeIn(200);
            $(this).data('status', 1);
        }
        else if( status == 1 ) {
            $('.sort-block ul').fadeOut(200);
            $(this).data('status', 0);
        }
    });

    $('.sort-block ul li').click(function(e){
        var value = $(this).data('value');
        $('.sort-block .sort span').text(value);
        $('input[name="sortSelect"]').val(value);

        $('.sort-block ul').fadeOut(200);
        $('.sort-block .sort').data('status', 0);
    });

    $(document).click(function(e) {
        var target = $(e.target);
        var sortLists = $('#sortLists');
        if(!(target.is(sortLists) || sortLists.find(target).length )) {
            sortLists.fadeOut(200);
            $('.sort-block .sort').data('status', 0);
        }
    });
}


function searchDropdown() {
    $('#topSearch, .top-icons li a#topSearch, .rps-footer-sticky li a#topSearch, #searchResult').click(function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var status = $(this).data('status');

        if( status == 0 ) {
            $('.search-container').fadeIn(200);
            $(this).data('status', 1).addClass('active');
        }
        else if( status == 1 ) {
            $('.search-container').fadeOut(200);
            $(this).data('status', 0).removeClass('active');
        }

        $('.getdirection-container').fadeOut(200);
        $('.contact-container').fadeOut(200);
        $('#topGetdirection, .top-icons li a#topGetdirection, .rps-footer-sticky li a#topGetdirection').data('status', 0).removeClass('active');

        $('.menu-close').removeClass('show-menu');
        $('.top-menu .menu').addClass('show-menu');
        $('.submenu-bg').removeClass('animate');
        $('.submenu-list').removeClass('animate');
        $('.submenu-tool').removeClass('animate');


    })

    $(document).click(function(e) {
        var target = $(e.target);
        var block = $('.search-container');
        if(!(target.is(block) || block.find(target).length )) {
            block.fadeOut(200);
            $('#topSearch, .top-icons li a#topSearch, .rps-footer-sticky li a#topSearch').data('status', 0).removeClass('active');
            $('body').css('overflow', 'auto');
        }
    });

    // Project type
    $('.projecttype-block .searchBlock').click(function(e){
        e.stopImmediatePropagation();
        var status = $(this).data('status');
        if( status == 0 ) {
            $('.projecttype-block ul').fadeIn(200);
            $(this).data('status', 1);
        }
        else if( status == 1 ) {
            $('.projecttype-block ul').fadeOut(200);
            $(this).data('status', 0);
        }

        $('.projectlocation-block ul').hide();
        $('.projectlocation-block .searchBlock').data('status', 0);
        $('.projectbrand-block ul').hide();
        $('.projectbrand-block .searchBlock').data('status', 0);
        $('.projectname-block ul').hide();
        $('.projectname-block .searchBlock').data('status', 0);
    });

    $('.projecttype-block ul li').click(function(e){
        var value = $(this).data('value');
        $('.projecttype-block .searchBlock span').text(value);
        $('input[name="projecttypeSelect"]').val(value);

        $('.projecttype-block ul').fadeOut(200);
        $('.projecttype-block .searchBlock').data('status', 0);
    });

    $(document).click(function(e) {
        var target = $(e.target);
        var searchLists = $('#searchLists');
        if(!(target.is(searchLists) || searchLists.find(target).length )) {
            searchLists.fadeOut(200);
            $('.projecttype-block .searchBlock').data('status', 0);
        }
    });

    // Project location
    $('.projectlocation-block .searchBlock').click(function(e){
        e.stopImmediatePropagation();
        var status = $(this).data('status');
        if( status == 0 ) {
            $('.projectlocation-block ul').fadeIn(200);
            $(this).data('status', 1);
        }
        else if( status == 1 ) {
            $('.projectlocation-block ul').fadeOut(200);
            $(this).data('status', 0);
        }

        $('.projecttype-block ul').hide();
        $('.projecttype-block .searchBlock').data('status', 0);
        $('.projectbrand-block ul').hide();
        $('.projectbrand-block .searchBlock').data('status', 0);
        $('.projectname-block ul').hide();
        $('.projectname-block .searchBlock').data('status', 0);
    });

    $('.projectlocation-block ul li').click(function(e){
        var value = $(this).data('value');
        $('.projectlocation-block .searchBlock span').text(value);
        $('input[name="projectlocationSelect"]').val(value);

        $('.projectlocation-block ul').fadeOut(200);
        $('.projecttype-block .searchBlock').data('status', 0);
    });

    $(document).click(function(e) {
        var target = $(e.target);
        var searchLists = $('#projectlocationLists');
        if(!(target.is(searchLists) || searchLists.find(target).length )) {
            searchLists.fadeOut(200);
            $('.projectlocation-block .searchBlock').data('status', 0);
        }
    });

    // Project Brand
    $('.projectbrand-block .searchBlock').click(function(e){
        e.stopImmediatePropagation();
        var status = $(this).data('status');
        if( status == 0 ) {
            $('.projectbrand-block ul').fadeIn(200);
            $(this).data('status', 1);
        }
        else if( status == 1 ) {
            $('.projectbrand-block ul').fadeOut(200);
            $(this).data('status', 0);
        }

        $('.projecttype-block ul').hide();
        $('.projecttype-block .searchBlock').data('status', 0);
        $('.projectlocation-block ul').hide();
        $('.projectlocation-block .searchBlock').data('status', 0);
        $('.projectname-block ul').hide();
        $('.projectname-block .searchBlock').data('status', 0);
    });

    $('.projectbrand-block ul li').click(function(e){
        var value = $(this).data('value');
        $('.projectbrand-block .searchBlock span').text(value);
        $('input[name="projectbrandSelect"]').val(value);

        $('.projectbrand-block ul').fadeOut(200);
        $('.projectbrand-block .searchBlock').data('status', 0);
    });

    $(document).click(function(e) {
        var target = $(e.target);
        var searchLists = $('#projectbrandLists');
        if(!(target.is(searchLists) || searchLists.find(target).length )) {
            searchLists.fadeOut(200);
            $('.projectbrand-block .searchBlock').data('status', 0);
        }
    });

    // Project name
    $('.projectname-block .searchBlock').click(function(e){
        e.stopImmediatePropagation();
        var status = $(this).data('status');
        if( status == 0 ) {
            $('.projectname-block ul').fadeIn(200);
            $(this).data('status', 1);
        }
        else if( status == 1 ) {
            $('.projectname-block ul').fadeOut(200);
            $(this).data('status', 0);
        }

        $('.projecttype-block ul').hide();
        $('.projecttype-block .searchBlock').data('status', 0);
        $('.projectlocation-block ul').hide();
        $('.projectlocation-block .searchBlock').data('status', 0);
        $('.projectbrand-block ul').hide();
        $('.projectbrand-block .searchBlock').data('status', 0);
    });

    $('.projectname-block ul li').click(function(e){
        var value = $(this).data('value');
        $('.projectname-block .searchBlock span').text(value);
        $('input[name="projectnameSelect"]').val(value);

        $('.projectname-block ul').fadeOut(200);
        $('.projectname-block .searchBlock').data('status', 0);
    });

    $(document).click(function(e) {
        var target = $(e.target);
        var searchLists = $('#projectnameLists');
        if(!(target.is(searchLists) || searchLists.find(target).length )) {
            searchLists.fadeOut(200);
            $('.projectname-block .searchBlock').data('status', 0);
        }
    });
}

function getdirectionDropdown() {
    $('#topGetdirection, .top-icons li a#topGetdirection, .rps-footer-sticky li a#topGetdirection').click(function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var status = $(this).data('status');

        if( status == 0 ) {
            $('.getdirection-container').fadeIn(200);
            $(this).data('status', 1).addClass('active');
        }
        else if( status == 1 ) {
            $('.getdirection-container').fadeOut(200);
            $(this).data('status', 0).removeClass('active');
        }

        $('.search-container').fadeOut(200);
        $('.contact-container').fadeOut(200);
        $('#topSearch, .top-icons li a#topSearch, .rps-footer-sticky li a#topSearch').data('status', 0).removeClass('active');
        $('.menu-close').removeClass('show-menu');
        $('.top-menu .menu').addClass('show-menu');
        $('.submenu-bg').removeClass('animate');
        $('.submenu-list').removeClass('animate');
        $('.submenu-tool').removeClass('animate');
    })

    $(document).click(function(e) {
        var target = $(e.target);
        var block = $('.getdirection-container');
        if(!(target.is(block) || block.find(target).length )) {
            block.fadeOut(200);
            $('#topGetdirection, .top-icons li a#topGetdirection, .rps-footer-sticky li a#topGetdirection').data('status', 0).removeClass('active');
            $('body').css('overflow', 'auto');
        }
    });

    // Get Direct type
    $('.directlocation-block .searchBlock').click(function (e) {
        e.stopImmediatePropagation();
        var status = $(this).data('status');
        if (status == 0) {
            $('.directlocation-block ul').fadeIn(200);
            $(this).data('status', 1);
        }
        else if (status == 1) {
            $('.directlocation-block ul').fadeOut(200);
            $(this).data('status', 0);
        }

        $('.directproject-block ul').hide();
        $('.directproject-block .searchBlock').data('status', 0);
    });

    $('.directlocation-block ul li').click(function (e) {
        var value = $(this).data('value');
        $('.directlocation-block .searchBlock span').text(value);
        $('input[name="directlocationSelect"]').val(value);

        $('.directlocation-block ul').fadeOut(200);
        $('.directlocation-block .searchBlock').data('status', 0);
    });

    $(document).click(function (e) {
        var target = $(e.target);
        var searchLists = $('#directlocationLists');
        if (!(target.is(searchLists) || searchLists.find(target).length )) {
            searchLists.fadeOut(200);
            $('.projecttype-block .searchBlock').data('status', 0);
        }
    });

    // Get Direct location
    $('.directproject-block .searchBlock').click(function (e) {
        e.stopImmediatePropagation();
        var status = $(this).data('status');
        if (status == 0) {
            $('.directproject-block ul').fadeIn(200);
            $(this).data('status', 1);
        }
        else if (status == 1) {
            $('.directproject-block ul').fadeOut(200);
            $(this).data('status', 0);
        }

        $('.directlocation-block ul').hide();
        $('.directlocation-block .searchBlock').data('status', 0);
    });

    $('.directproject-block ul li').click(function (e) {
        var value = $(this).data('value');
        $('.directproject-block .searchBlock span').text(value);
        $('input[name="directprojectSelect"]').val(value);

        $('.directproject-block ul').fadeOut(200);
        $('.directproject-block .searchBlock').data('status', 0);
    });

    $(document).click(function (e) {
        var target = $(e.target);
        var searchLists = $('#directprojectLists');
        if (!(target.is(searchLists) || searchLists.find(target).length )) {
            searchLists.fadeOut(200);
            $('.directproject-block .searchBlock').data('status', 0);
        }
    });
}

function contactDropdown() {
    $('#topContact, .top-icons li a#topContact, .rps-footer-sticky li a#topContact').click(function(e) {

        radionAppoint();
        e.preventDefault();
        e.stopImmediatePropagation();
        var status = $(this).data('status');

        if( status == 0 ) {
            $('.contact-container').fadeIn(200);
            $(this).data('status', 1).addClass('active');
        }
        else if( status == 1 ) {
            $('.contact-container').fadeOut(200);
            $(this).data('status', 0).removeClass('active');
        }

        $('.search-container').fadeOut(200);
        $('.getdirection-container').fadeOut(200);
        $('#topContact, .top-icons li a#topContact, .rps-footer-sticky li a#topContact').data('status', 0).removeClass('active');
        $('.menu-close').removeClass('show-menu');
        $('.top-menu .menu').addClass('show-menu');
        $('.submenu-bg').removeClass('animate');
        $('.submenu-list').removeClass('animate');
        $('.submenu-tool').removeClass('animate');
    });

    $('.contact-container-ft .radio-checkmark input[type="radio"]').click(function() {
        
        if($('#check-appoint-ft').is(':checked')) {
            $('input#date_appointment_ft').css('display','block');
        }
        else {
            $('input#date_appointment_ft').css('display','none');
        }
    });

    $('#date_appointment_ft').datepicker();

    $(document).click(function(e) {
        var target = $(e.target);
        var block = $('.contact-container');
        if(!(target.is(block) || block.find(target).length )) {
            block.fadeOut(200);
            $('#topContact, .top-icons li a#topContact, .rps-footer-sticky li a#topContact').data('status', 0).removeClass('active');
            $('body').css('overflow', 'auto');
        }
    });
}

function radionAppoint() {

    $('.contact-container-ft .radio-checkmark input[type="radio"]').click(function() {

        if($('#check-appoint-ft').is(':checked')) {
            $('input#date_appointment_ft').css('display','block');
        }
        else {
            $('input#date_appointment_ft').css('display','none');
        }
    });

    $('#date_appointment_ft').datepicker();
}

function tabMenu() {
    $('.tabs-menu a').click(function(e) {
        e.preventDefault();
        $(this).parent().addClass('current');
        $(this).parent().siblings().removeClass('current');
        var tab = $(this).attr('href');
        $('.tab-content').not(tab).css('display', 'none');
        $(tab).fadeIn();
    });

}
//Function End