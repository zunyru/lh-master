<?php
require_once 'include/dbConnect.php';

try {
    $galery_banner_img_id = $_GET['galery_banner_img_id'];

    $conn = (new dbConnect())->getConn();
    $sql = "DELETE FROM LH_BANNER_SUB
				WHERE banner_sub_id =".$galery_banner_img_id;
    $result= $conn->query($sql);

    echo json_encode($result->fetchAll());

} catch (\Exception $e) {
    return $e->getMessage();
}
?>