<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>LAND & HOUSES</title>

  <!-- Bootstrap -->
  <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- NProgress -->
  <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
  <!-- Animate.css -->
  <link href="../vendors/gentelella/css/animate.min.css" rel="stylesheet">

  <!-- Custom Theme Style -->
  <link href="../build/css/custom.min.css" rel="stylesheet">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>



  <script>
    $(document).ready(function(){
      $("#alert").show();
      $("#alert").fadeTo(3000, 500).slideUp(500, function(){
        $("#alert").alert('close');
      });
    });
  </script>

</head>

<body class="login">

  <div>
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>

    <div class="login_wrapper">
      <div class="animate form login_form">
        <section class="login_content">

          <form action="check_login.php" name="login" method="post" id="commentForm">
            <div style="margin-top: 3%;">
              <center><img src="images/lh.jpg" alt="..." ></center>
            </div>
            <br>
            <h1>Login Form  <img src="https://www.lh.co.th/www_th/Frontend/images/global/lang-en.png"></h1>
            <?php  if(isset($_GET['error'])){ ?>
              <div class="alert alert-danger" id="alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                <strong>Username หรือ Password ! </strong>
                ไม่ถูกต้อง.
              </div>
            <?php }else if(isset($_GET['errors'])){ ?>
              <div class="alert alert-danger" id="alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                <strong>กรุณา Login เข้าสู่ระบบก่อน ! </strong>
              </div>
            <?php  } ?>


            <div>
              <input type="text" name="username" class="form-control" placeholder="Username" required="" />
            </div>
            <div>
              <input type="password" name="password" class="form-control" placeholder="Password" required="" />
            </div>

            <div>
              <button type="submit" name="login" class="btn btn-default submit"  >Login</button>
            </div>

            <br>
            <h3>เข้าสู่ระบบหลังบ้านของแต่ละระบบ ดังนี้</h3>

            <div style="text-align: center;">
              <div>
                <!-- <input type="text" name="username" class="form-control" placeholder="Username" required="" /> -->
                <a class="btn btn-default" href="https://www.lh.co.th/www_th/Backend/" style="width: 100%;">เข้าสู่ระบบ หลังบ้านไทย
                  <img src="https://www.lh.co.th/www_th/Frontend/images/global/lang-th.png" style="
                  float: right;
                  width: 10%;
                  ">
                </a>
              </div>
              <div>
                <!-- <input type="password" name="password" class="form-control" placeholder="Password" required="" /> -->
                <a class="btn btn-default" href="https://www.lh.co.th/www_en/Backend/" style="width: 100%;">เข้าสู่ระบบ หลังบ้านอังกฤษ
                  <img src="https://www.lh.co.th/www_th/Frontend/images/global/lang-en.png" style="
                  float: right;
                  width: 10%;
                  ">
                </a>
              </div>

              <div>
                <!-- <input type="password" name="password" class="form-control" placeholder="Password" required="" /> -->
                <a class="btn btn-default" href="https://www.lh.co.th/www_cn/Backend/" style="width: 100%;">เข้าสู่ระบบ หลังบ้านจีน
                  <img src="https://www.lh.co.th/www_th/Frontend/images/global/lang-cn.png" style="
                  float: right;
                  width: 10%;
                  ">
                </a>
              </div>
            </div>
            <br>

            <p>รองรับ Browser Chome รุ่น 56.0.2924.76 <a href="https://www.google.com/intl/th/chrome/browser/desktop/index.html" target="_blank">ดาว์โหลด</a></p>
            <p>รองรับ Browser Internet Explorer 11 <a href="https://www.microsoft.com/en-us/download/Internet-Explorer-11-for-Windows-7-details.aspx" target="_blank">ดาว์โหลด</a></p>
            <div class="clearfix"></div>





          </form>
        </section>
        <!-- box -->


        <div class="well" style="display:none;max-width: 699px;
        margin: auto;
        text-align: left;
        line-height: 20px;
        position: absolute;
        margin-top: -10px;
        width: 139%;
        margin-left: -102px;
        color: #000 !important;
        background: #FF0;
        padding-bottom: 20px;
        margin-bottom: 20px;">

        <h3> เเนื่องจากจากระบบควบรวม Database ยังพัฒนาไม่สมบูรณ์จึงขอให้กรอกข้อมูล Back web ทั้ง 2 เว็บต่อออกไปจนถึง 15 ส.ค. 60 </h3>
        <h3>คลิกกรอกข้อมูล URL ระบบเดิม ที่ <a href="http://www1.lh.co.th/backweb" target="_blank" style="font-size: 24px;padding: 0px;  margin: 0;">www1.lh.co.th/backweb</a></h3>
      </div>





    </div>


  </div>
</body>
<!-- Validate -->
<script src="../build/js/jquery.validate.js"></script>
<script>
  $(document).ready(function() {

    $("#commentForm").validate({

      </section>

      </div>



      rules: {
        username: "required",
        password: "required",

      },
      messages: {
        username: "กรุณากรอก user name !",
        password : "กรุณากรอก Password !",
      }
    });
  });
</script>

</html>