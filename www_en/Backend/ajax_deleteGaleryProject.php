<?php
require_once 'include/dbConnect.php';

	try {
		$galery_project_id = $_GET['galery_project_id'];

		$conn = (new dbConnect())->getConn();
		$sql = "DELETE FROM LH_GALERY_PROJECT
				WHERE galery_project_id =".$galery_project_id;
        $result= $conn->query($sql);

		echo json_encode($result->fetchAll());

	} catch (\Exception $e) {
		return $e->getMessage();
	}
?>