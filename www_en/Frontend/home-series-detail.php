<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

 $plan_id = $_GET['plan_id'];
 $series_id = $_GET ['series_id'];
 //var_dump($series_id);

if(empty($plan_id) || getPlanByPlanID($plan_id) == false){
    echo 'not have a home series for this series id';
}

$plan = getPlanByPlanID($plan_id);
$series = getHomeSeriesByID($series_id);
@$galleryFloorPlans = getGalleryFloorPLans($plan_id);

function mssql_escape($str) {
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return str_replace("'", "''", $str);
}
$description = 'Land & Houses บริษัทธุรกิจอสังหาริมทรัพย์ในประเทศไทย LHเปิดขายทั้งโครงการบ้านเดี่ยว ทาวน์โฮม คอนโดมิเนียมในกรุงเทพฯ ปริมณฑลและต่างจังหวัด';
// $title_page = "House style  ".$plan->plan_name_th ." From Land & Houses";
// $description_page = "Home Design ".$plan->plan_name_th ." From Land & Houses";
// $image_page = str_replace('fileupload/images/galery_plan/', 'http://www.lh.co.th/www/Backend/fileupload/images/galery_plan/Thumbnails_',$plan->plan_img);
$title_page = $plan->plan_name_th ;
$description_page = isset($galleryFloorPlans[0]) ? mssql_escape($galleryFloorPlans[0]->floor_plan_dis_th) : $description; 
$image_page = str_replace('fileupload/images/galery_plan/', 'http://www.lh.co.th/www/Backend/fileupload/images/galery_plan/Thumbnails_',$plan->plan_img);
$keywords =  $plan->plan_name_th ;
?>
<?php include('header.php'); ?>
    <!-- CSS -->
    <link rel="stylesheet" href="<?= file_path('css/home-series-detail.css') ?>" type="text/css">
    <!-- JS -->
    <script src="<?= file_path('js/home-series-detail.js') ?>"></script>


    <div class="page-banner page-scroll">
        <span id="scrollNext" class="i-scroll-next"></span>

        <div id="bannerSlide">
            <?php
            $galleryPlan = getGalleryFirstPlan($plan_id);
            ?>
            <div class="item">
                <div class="banner-container banner-parallax">
                    <img src="<?= backend_url('base',$galleryPlan->plan_img) ?>" alt="">
                </div>
            </div>
        </div>
    </div>

        <?php
        $series = getHomeSeriesByID($plan->series_id);
        ?>

<div id="content" class="content project-info-furnished-page">
    <div class="container">
        <div class="project-info-detail-block">
            <!-- H1 -->
            <?php 
            $SEO = getSEOUrl($actual_link);
            if($actual_link == @$SEO->url_page){
                echo '<h1 class="heading-title header-margin">'.$SEO->h1.'</h1>'; 
            }
            ?>
            <!-- End H1 -->
            <div class="row">
                <div class="project-sale-logo">
                    <img src="<?= backend_url('series_logo', $series->series_logo) ?>" alt="">
                    <p class="heading-title grey">House style <?= $plan->plan_name_th ?></p>
                </div>

                    <?php
                    $planFunctions     = getPlanFunction_paln($plan_id);
                    $planFunctions_praking  = getPlanFunction_paln_praking($plan_id);
                    $leftPlanFunction  = [];
                    $rightPlanFunction = [];
                    $lhPlan            = getPlanByPlanID($plan_id);
                    $leftPlanFunction[] = $plan->plan_useful;
                    $leftPlanFunction[] = $planFunctions_praking[0];
                    //$conditionHomeSell->land_size;
                    foreach ($planFunctions as $i => $planFunction)
                    {
                        $leftPlanFunction[] = $planFunction;
                    }
                    //print_r($leftPlanFunction);
                    ?>
                    <div class="col-md-6">
                        <div class="content-style">
                            <p class="heading-title mb0 mbr-15">Table of functions in the house</p>
<!--                            <p class="title grey">โครงการ  บ้านชัยพฤกษ์ ปิ่นเกล้า-กาญจนาฯ</p>-->
<!--                            <p class="title grey mbr-15">ทำเล ราชพฤกษ์-ปิ่นเกล้า</p>-->
                            <div class="row">
                                <div class="tb-2col">

                                    <div class="col-md-12 cl">
                                        <table class="content-style tbs1" id="project-table" width="100%">
                                            <tbody>
                                            <?php  foreach ($leftPlanFunction as $i => $planFunction) { ?>
                                                <tr>
                                                    <td><?php if($i == 0){echo 'Utility area'; }elseif($i==1){echo 'Car parking';}else{ echo $planFunction->function_plan_name_th;}?></td>
                                                    <td><?php if($i == 0){echo $planFunction; }elseif($i==1){echo $planFunctions_praking[0]->function_plan_sub_plan_count_room;}else{echo $planFunction->function_plan_sub_plan_count_room;;}?></td>
                                                    <td><?php if($i == 0){echo 'sq.m.';}elseif($i==1){echo 'cars';}else{ echo $planFunction->function_plan_pronoun; }?></td>
                                                </tr>
                                            <?php }?>
                                            </tbody>
                                        </table>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $peculalityPlans = getPeculalityPlan($plan_id);
                    if($peculalityPlans != false){
                    ?>
                    <div class="col-md-6">
                        <p class="title grey mtr-15">Other informations</p>
                        <div class="content-style">

                            <div class="row">

                                <?php
                                $leftPeculality  = [];
                                $rightPeculality = [];
                                foreach ($peculalityPlans as $i => $peculality)
                                {
                                    if($i%2 == 0){
                                        $leftPeculality[] = $peculality;
                                    }elseif ($i%2 == 1){
                                        $rightPeculality[] = $peculality;
                                    }
                                }
                                ?>

                                <div class="col-md-6">
                                    <ul class="tbs1">
                                        <?php
                                        foreach ($leftPeculality as $peculality){
                                            ?>
                                            <li><?= $peculality->peculiarity_name_th ?></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>

                                <div class="col-md-6">
                                    <ul class="tbs1">
                                        <?php
                                        foreach ($rightPeculality as $peculality){
                                            ?>
                                            <li><?= $peculality->peculiarity_name_th ?></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                    <?php
                    }
                    ?>
                </div>
            </div>

            <div class="project-info-detail-block">
                <p class="heading-title">House plan</p>

                <div id="homePlanSlide">

                    <?php
                    $galleryFloorPlans = getGalleryFloorPLans($plan_id);
                    if($galleryFloorPlans != false){
                        foreach ($galleryFloorPlans as $i => $galleryFloorPlan){
                            ?>
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="#" data-featherlight="<?= backend_url('base',$galleryFloorPlan->floor_plan_img_name)?>">
                                            <img src="<?= backend_url('base',$galleryFloorPlan->floor_plan_img_name)?>" alt="">
                                            <span class="i-zoom"></span>
                                        </a>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="tab-dd dropdown">
                                            <div class="row">
                                                <ul class="tabs-menu-plan-slide dropdown-menu" aria-labelledby="dropdownMenu1">
                                                    <?php
                                                    if(count($galleryFloorPlans) >= 1) {
                                                        ?>
                                                        <li class="<?= $i == 0 ? 'current' : '' ?>">
                                                            <a data-value="0">
                                                                First floor
                                                            </a>
                                                        </li>
                                                        <?php
                                                    }
                                                    if(count($galleryFloorPlans) >= 2) {
                                                        ?>
                                                        <li class="<?= $i == 1 ? 'current' : '' ?>">
                                                            <a data-value="1">
                                                                Second floor
                                                            </a>
                                                        </li>
                                                        <?php
                                                    }
                                                    if(count($galleryFloorPlans) >= 3) {
                                                        ?>
                                                        <li class="<?= $i == 2 ? 'current' : '' ?>">
                                                            <a data-value="2">
                                                                Second floor
                                                            </a>
                                                        </li>
                                                        <?php
                                                    }
                                                    ?>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="tab">
                                            <div id="tab-1" class="tab-content">
                                                <div class="tab-block content-style">
                                                    <ul class="desc_floor<?= $i ?>">

                                                            <script type="text/javascript">
                                                                $('.desc_floor<?=$i?>').html( '<li>'+`<?= $galleryFloorPlan->floor_plan_dis_th?>`.replace(/\r?\n/g, '</li><li>') + '</li>')
                                                            </script>

                                                    </ul>
                                                    <ul style="color: #999999;font-size: 15px;">
                                                        Note: the figure in the plan is the estimated approximately. The details of plan and house plan for consideration You can consider the real details of the real house at the project site. The company also reserves the rights to change the materials and detail without prior notice 
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                    <?php
                        }
                    }
                    ?>
                </div>

            </div>

            <?php
            $projectIdPlans = getProjectIDPlan($plan_id);
            if($projectIdPlans != false) {
                ?>

                <div class="project-info-detail-block project-sample">
                    <p class="heading-title">Other projects</p>
                    <div class="sort-container">
                        <div class="sort-block">
                            <input type="hidden" name="sortSelect" value="">
                            <div class="sort" data-status="0">
                                <span>
                                    <?php
                                    $otherProjectPlans = getProjectIDPlan($plan_id);

                                    if(!empty($_GET['zone_id'])){

                                         $zone_name = getZoneByID($_GET['zone_id']);
                                         echo $zone_name->zone_name_th;

                                    }else{
                                        echo 'Select All Locations';
                                    }
                                    ?>
                                </span> <i class="i-sort"></i>
                            </div>


                            <ul id="sortLists">
                                <li data-value="เลือกทำเลที่สนใจทั้งหมด" onclick="location.href='<?= $router->generate('home-series-detail',[
                                    'plan_name'  =>  str_replace(' ','-',$plan->plan_name_th),
                                    'series_name'  =>  str_replace(' ','-',$series->series_name_th),
                                ]) ?>'">
                                    Select All Locations
                                </li>
                                <?php
                                $zone = getZoneByID_Dis($plan->plan_id);
                                foreach($zone as $zones) {
//                                    $project = getProjectByID($otherProjectPlan->project_id);
//                                    if($project != false) {
//                                        $zone_id = $project->zone_id;

                                        ?>
                                        <li data-value="<?= $zones->zone_name_th ?>"
                                            onclick="location.href='<?= $router->generate('home-series-detail-by-zone',[
                                                'series_name'  =>  str_replace(' ','-',$series->series_name_th),
                                                'plan_name'  =>  str_replace(' ','-',$plan->plan_name_th),
                                                'zone_name'  =>  str_replace(' ','-',$zones->zone_name_th),
                                                
                                            ]) ?>'">
                                            <?= $zones->zone_name_th ?>
                                        </li>
                                        <?php
                                   // }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div id="colSlide" class="col-slide">
                            <?php
                            foreach ($projectIdPlans as $i => $projectIDPlann) {
                                $projectID = $projectIDPlann->project_id;
                                $project = getProjectByID($projectID);
                                $banner = getBannerByProjectID($projectID);

                                if( empty($_GET['zone_id']) || (!empty($_GET['zone_id']) && $_GET['zone_id'] == $project->zone_id ) ) {
                                    ?>
                                    <div class="item">
                                        <div class="project-onsale">
                                            <div class="project-info-other-img">

                                                <?php
                                                if ($banner != false && $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'image') {
                                                    ?>
                                                <img src="<?= backend_url('base', $banner->LEAD_IMAGE_PROJECT_FILE_NAME) ?>"
                                                     alt="">
                                                <?php
                                                } elseif ($banner != false && $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'youtube') {
                                                $lead_img = backend_url('base', $banner->banner_img_thum);
                                                $lead_youtube_url = str_replace('watch?v=', 'embed/', $banner->LEAD_IMAGE_PROJECT_FILE_NAME);
                                                ?>

                                                    <a href="#" data-featherlight="#lbVdo<?= $i ?>">
                                                        <span class="i-view-vdo"></span>
                                                        <img src="<?= $lead_img ?>"
                                                             alt="<?= $banner->LEAD_IMAGE_PROJECT_FILE_SEO ?>"
                                                             class="hm-highl">
                                                    </a>
                                                    <iframe class="lightbox"
                                                            src="<?= $lead_youtube_url . "?autoplay=0" ?>"
                                                            width="1000"
                                                            height="560" id="lbVdo<?= $i ?>" style="border:none;"
                                                            webkitallowfullscreen
                                                            mozallowfullscreen allowfullscreen></iframe>

                                                <?php
                                                } elseif ($banner != false && $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'vdo') {
                                                ?>

                                                    <a id="ban_vdo<?= $i ?>">
                                                        <span class="i-view-vdo"></span>
                                                        <img src="<?= backend_url('base', $banner->banner_img_thum) ?>"
                                                             alt="<?= $banner->LEAD_IMAGE_PROJECT_FILE_SEO ?>"
                                                             class="hm-highl">
                                                    </a>
                                                    <script>
                                                        $(document).ready(function () {
                                                            $('#ban_vdo<?= $i ?>').click(function () {
                                                                $('#lbVdo<?= $i ?>').show();
                                                                $.featherlight($('#lbVdo<?= $i ?>'), {});
                                                                $('.featherlight-content #video_player<?= $i ?>')[0].play();
                                                                $('#lbVdo<?= $i ?>').hide();
                                                            });
                                                        })
                                                    </script>
                                                    <div id="lbVdo<?= $i ?>"
                                                         style="position:relative;width: 100%;display: none;">
                                                        <video id='video_player<?= $i ?>' preload='none' controls>
                                                            <source src="<?= backend_url('base', $banner->LEAD_IMAGE_PROJECT_FILE_NAME) ?>"
                                                                    type="video/mp4">
                                                        </video>
                                                    </div>

                                                    <?php
                                                }
                                                ?>
                                            </div>

                                            <?php
                                               if($projectIDPlann->product_id == 1){
                                                  $type = 'singlehome';
                                                  $url_path = 'single-home-detail'; 
                                               }else{
                                                  $type = 'townhome';
                                                  $url_path = 'town-home-detail'; 
                                               }
                                            ?>
                                            <a href="<?= $router->generate($url_path,[
                                                'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                                'product_id'    =>  $projectIDPlann->product_id
                                            ]); ?>">
                                                <p class="title">
                                                    Project <?php if ($project != false) echo $project->project_name_th ?></p>
                                            </a>
                                            <a href="<?= $router->generate($url_path,[
                                                'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                                'product_id'    =>  $projectIDPlann->product_id
                                            ]); ?>"
                                               class="btn btn-seemore">See more</a>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <?php

            $plan360   = get360Plan($plan_id);
            $videoPlan = getVideoPlan($plan_id);

            if($plan360 != false || $videoPlan != false){
                ?>
                <div class="project-info-detail-block">
                    <div class="row">
                        <?php
                        if($plan360 != false){
                            ?>
                            <div class="col-md-6">
                                <p class="heading-title">360-degree image</p>
                                <div class="project-info-block">
                                    <div class="project-info-other-img">
                                        <a href="#" data-featherlight="#lb360">
                                            <span class="i-view-360"></span>
                                            <img src="<?= backend_url('base',$plan360->c360_img) ?>" alt="">
                                        </a>
                                        <iframe class="lightbox vdoview" src="<?= $plan360->c360_plan_url ?>" width="1000"
                                                height="560" id="lb360" style="border:none;" webkitallowfullscreen
                                                mozallowfullscreen allowfullscreen></iframe>
                                    </div>
<!--                                    <p class="title">EMPERY</p>-->
                                </div>
                            </div>
                        <?php
                        }
                        if($videoPlan != false){
                            if($videoPlan->clip_type == 'youtube') {
                                if($videoPlan->thumnail != '') {
                                    ?>
                                    <div class="col-md-6">
                                        <p class="heading-title">VDO</p>
                                        <div class="project-info-block">
                                            <div class="project-info-other-img">
                                                <?php
                                                if ($videoPlan->clip_type == 'youtube') {
                                                    $youtube_img = backend_url('base', $videoPlan->thumnail);
                                                    $youtube_url = str_replace('watch?v=', 'embed/', $videoPlan->clip_plan_url);
                                                    ?>
                                                    <a href="#" data-featherlight="#lbVdo1">
                                                        <span class="i-view-vdo"></span>
                                                        <img src="<?= $youtube_img ?>" alt="">
                                                    </a>
                                                    <iframe class="lightbox" src="<?= $youtube_url . "?autoplay=0" ?>"
                                                            width="1000"
                                                            height="560" id="lbVdo1" style="border:none;"
                                                            webkitallowfullscreen
                                                            mozallowfullscreen allowfullscreen></iframe>
                                                <?php
                                                }elseif ($videoPlan->clip_type == 'vdo'){
                                                $youtube_img = backend_url('base', $videoPlan->thumnail);
                                                ?>
                                                    <a id="ban_vdo">
                                                        <span class="i-view-vdo"></span>
                                                        <img src="<?= $youtube_img ?>"
                                                             alt="" class="hm-highl">
                                                    </a>
                                                    <script>
                                                        $(document).ready(function () {
                                                            $('#ban_vdo').click(function () {
                                                                $('#lbVdo').show();
                                                                $.featherlight($('#lbVdo'), {});
                                                                $('.featherlight-content #video_player')[0].play();
                                                                $('#lbVdo').hide();
                                                            });
                                                        })
                                                    </script>
                                                    <div id="lbVdo"
                                                         style="position:relative;width: 100%;display: none;">
                                                        <video id='video_player' preload='none' controls>
                                                            <source src="<?= backend_url('base', $videoPlan->clip_plan_url) ?>"
                                                                    type="video/mp4">
                                                        </video>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
            <?php
            }
            ?>

            <?php
            $gallerys = getGallerysPlanByPlanID($plan_id);
            if($gallerys != false){
                ?>
                <div class="gallery-block">
                    <p class="heading-title">GALLERY</p>
                    <div class="tpl5-img">
                        <div id="tpl5-img">
                            <?php
                            foreach ($gallerys as $i => $gallery){
                                ?>
                                <div class="item tpl5-img-<?= $i+1 ?>">
                                    <a class="gallery" href="<?= backend_url('base',$gallery->galery_plan_name) ?>">
                                        <img src="<?= backend_url('base',$gallery->galery_plan_name) ?>"
                                             alt="<?= $gallery->galery_plan_img_seo ?>">
                                        <p class="title-gallery"><?= $gallery->galery_plan_name_dis ?></p>
                                    </a>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>


<?php include('footer.php'); ?>