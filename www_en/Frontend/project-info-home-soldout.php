<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>
<?php include('header.php'); ?>
    <!-- CSS -->
    <link rel="stylesheet" href="css/project-info-home.css" type="text/css">
    <link rel="stylesheet" href="bxslider/jquery.bxslider.css" type="text/css">
    <!-- JS -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKN03V-Oz7X7nlSgAj9w55RrUvv82j5yA&callback=initMap"
async defer></script>
    <script>
        function initMap() {
            // Create a map object and specify the DOM element for display.
            $(document).ready(function () {
                var lat_proj = 13.7341307;
                var lng_proj = 100.5808519;
                var pos = {lat: lat_proj, lng: lng_proj};
                var map = new google.maps.Map(document.getElementById('map'), {
                    center: pos,
                    scrollwheel: false,
                    zoom: 15,
                    zoomControlOptions: {
                        position: google.maps.ControlPosition.TOP_RIGHT
                    },
                    streetViewControl: false,
                    mapTypeControl: false,
                    styles: [{"elementType": "geometry", "stylers": [{"color": "#f5f5f5"}]}, {
                        "elementType": "labels.icon",
                        "stylers": [{"visibility": "off"}]
                    }, {
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#616161"}]
                    }, {
                        "elementType": "labels.text.stroke",
                        "stylers": [{"color": "#f5f5f5"}]
                    }, {
                        "featureType": "administrative.land_parcel",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#bdbdbd"}]
                    }, {
                        "featureType": "administrative.locality",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#c19838"}]
                    }, {
                        "featureType": "administrative.neighborhood",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#c19838"}]
                    }, {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [{"color": "#eeeeee"}]
                    }, {
                        "featureType": "poi",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#757575"}]
                    }, {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [{"color": "#e5e5e5"}]
                    }, {
                        "featureType": "poi.park",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#9e9e9e"}]
                    }, {
                        "featureType": "road",
                        "elementType": "geometry",
                        "stylers": [{"color": "#ffffff"}]
                    }, {
                        "featureType": "road",
                        "elementType": "labels.icon",
                        "stylers": [{"visibility": "simplified"}]
                    }, {
                        "featureType": "road",
                        "elementType": "labels.text",
                        "stylers": [{"color": "#878787"}]
                    }, {
                        "featureType": "road",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#9f720a"}, {"visibility": "off"}]
                    }, {
                        "featureType": "road.arterial",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#757575"}]
                    }, {
                        "featureType": "road.highway",
                        "elementType": "geometry",
                        "stylers": [{"color": "#dadada"}]
                    }, {
                        "featureType": "road.highway",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#616161"}]
                    }, {
                        "featureType": "transit.line",
                        "elementType": "geometry",
                        "stylers": [{"color": "#e5e5e5"}]
                    }, {
                        "featureType": "transit.station",
                        "elementType": "geometry",
                        "stylers": [{"color": "#eeeeee"}]
                    }, {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [{"color": "#c9c9c9"}]
                    }, {"featureType": "water", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]}]
                });
                var icon = {
                    url: 'images/global/icon_google_map.png',
                    scaledSize: new google.maps.Size(30, 42), // scaled size
                    origin: new google.maps.Point(0, 0), // origin
                    anchor: new google.maps.Point(0, 0) // anchor
                };
                var marker = new google.maps.Marker({
                    position: pos,
                    map: map,
                    icon: icon
                });
                google.maps.event.addListener(marker, 'click', function() {
                    window.open('https://maps.google.com?saddr=Current+Location&daddr='+lat_proj+','+lng_proj,'_blank');
                });
            });
        }
    </script>
    <script src="bxslider/jquery.bxslider.min.js"></script>
    <script src="js/project-info-home.js"></script>


    <div class="page-banner page-scroll slider">
        <div id="pjHomeBanner" class="owl-carousel owl-theme">

            <div class="item slide">
                <div class="banner-container banner-parallax banner-dktp"
                     style="background-image: url(http://devwww2.lh.co.th/Land_and_house/Backend/fileupload/images/project_img/12-01-2017-10:33:16-1412837514221_LH_SLV_57.jpg);">
                </div>
            </div>
        </div>
        <span id="scrollNextInfoHome" class="i-scroll-next"></span>
    </div>

    <div id="sec-information" class="content project-info-page">
        <div class="project-info-container">
            <div class="container">
                <div class="tagbox2_sold soldout-tag">
                    <div class="tag2">sold<br>out</div>
                </div>
                <div class="row">
                    <div class="info-logo-img">
                        <img src="http://devwww2.lh.co.th/Land_and_house/Backend/fileupload/images/maps/12-01-2017-11:48:57-1408008326994_Logo2.jpg" alt="" style="width: 100px;">
                    </div>
                </div>
                <div class="row mb20">
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                        <p class="subtitle green">ข้อมูลโครงการ</p>
                    </div>
                    <div class="col-md-6">
                        <p>สีวลี เลควิว-เชียงใหม่<br>
                           พื้นที่โครงการ 67.3 ไร่<br>
                           ประเภท บ้านเดี่ยว มีแบบบ้านให้เลือกทั้งหมด 5 แบบ พื้นที่ใช้สอยตั้งแต่ 148 ตร.ม.<br>
                           บนขนาดที่ดินตั้งแต่ 72 ตารางวา
                        </p>
                        <p class="subtitle">ราคาเริ่มต้น 6.39 ล้านบาท</p>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <div class="row mb20">
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                        <p class="subtitle green">แนวคิดโครงการ</p>
                    </div>
                    <div class="col-md-6">
                        <p class="txt-slogan">IDENTITY OF YOUR PRESTIGE</p>
                        <p class="subtitle">เติมเต็มสิ่งที่ดีที่สุดสำหรับชีวิตครอบครัว ด้วยไลฟ์สไตล์ที่โดดเด่น พร้อมความรู้สึกพิเศษท่ามกลางพื้นที่ความสุขกว้างๆ บรรยากาศใหม่ๆ</p>
                    </div>
                    <div class="col-md-2"></div>
                </div>

                <div class="row mb20">
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                        <p class="subtitle green">จุดเด่นโครงการ</p>
                    </div>
                    <div class="col-md-6">
                        <p>การออกแบบผังโครงให้สัมผัสบรรยากาศธรรมชาติได้อย่างทั่วถึง ทั้งพื้นที่ด้านหน้าและภายในโครงการ - สถาปัตยกรรมบริเวณทางเข้าออกหน้าโครงการได้รับการสร้างสรรค์ให้เป็นจุดชมวิวที่สวยที่สุดเปิดรับวิวดอยสุเทพทอดขนานทะเลสาบกว่า 100 ไร่ - สวนพักผ่อนในโครงการ ทอดยาวขนานแนวบ้านให้บรรยกาศสไตล์ Valley of Nature</p>
                    </div>
                    <div class="col-md-2"></div>
                </div>

            </div>
        </div>

        <div class="container">
            <div id="sec-gallery" class="gallery-block block-margin">
                <p class="heading-title header-margin">แกลอรี</p>
                <div class="tpl5-img">
                    <div id="tpl5-img">
                        <div class="item tpl5-img-1">
                            <a class="gallery" href="http://devwww2.lh.co.th/Land_and_house/Backend/fileupload/images/project_galery/12-01-2017-10:33:16-1393410246021_LH-02-04.jpg">
                                <img src="http://devwww2.lh.co.th/Land_and_house/Backend/fileupload/images/project_galery/12-01-2017-10:33:16-1393410246021_LH-02-04.jpg" alt="">
                            </a>
                        </div>
                        <div class="item tpl5-img-2">
                            <a class="gallery" href="http://devwww2.lh.co.th/Land_and_house/Backend/fileupload/images/project_galery/12-01-2017-10:33:16-1412837514221_LH_SLV_59.jpg">
                                <img src="http://devwww2.lh.co.th/Land_and_house/Backend/fileupload/images/project_galery/12-01-2017-10:33:16-1412837514221_LH_SLV_59.jpg" alt="">
                            </a>
                        </div>
                        <div class="item tpl5-img-3">
                            <a class="gallery" href="http://devwww2.lh.co.th/Land_and_house/Backend/fileupload/images/project_galery/12-01-2017-10:33:16-1393410246024_LH-03-05_b.jpg">
                                <img src="http://devwww2.lh.co.th/Land_and_house/Backend/fileupload/images/project_galery/12-01-2017-10:33:16-1393410246024_LH-03-05_b.jpg" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div id="other-home-sec" class="other-home-sec">
                <p class="heading-title header-margin">โครงการอื่นๆ ที่น่าสนใจ <span>1/5 แปลง</span></p>
                <div class="row">
                    <div id="otherHome" class="col-slide">
                        <div class="item">
                            <div class="">
                                <div class="other-home-img">
                                    <img src="images/temp/pj_home_sale.jpg" alt="">
                                </div>
                                <a>
                                    <p class="title">นันทวัน บางนา กม7</p>
                                    <p class="gray mb0">รสนิยมเห่งการอยู่อาศัยที่สมบูรณ์แบบ</p>
                                    <p class="title green"><b>ราคา 5.49 ล้านบาท</b></p>
                                    <div id="data-slider" style="display: none">
                                        <input type="hidden" id="title_home" value="CEDAR A">
                                        <input type="hidden" id="format_home" value="แบบ บ้านๆ">
                                        <input type="hidden" id="id_home" value="01I07">
                                        <input type="hidden" id="identity_home" value="เจ๋งเฟร่อ">
                                        <input type="hidden" id="price_home" value="6.88 ล้านบาทจร้า">
                                        <div id="img_list">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale.jpg">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale.jpg">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale.jpg">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="">
                                <div class="other-home-img">
                                    <img src="images/temp/pj_home_sale.jpg" alt="">
                                </div>
                                <a>
                                    <p class="title">มัณฑนา Lak Watcharapol</p>
                                    <p class="gray mb0">รสนิยมเห่งการอยู่อาศัยที่สมบูรณ์แบบ</p>
                                    <p class="title green"><b>ราคา 5.49 ล้านบาท</b></p>
                                    <div id="data-slider" style="display: none">
                                        <input type="hidden" id="title_home" value="WALNUT">
                                        <input type="hidden" id="format_home" value="WALNUT MODERN">
                                        <input type="hidden" id="id_home" value="01U07">
                                        <input type="hidden" id="identity_home" value="บ้านทรง โมเดิร์น">
                                        <input type="hidden" id="price_home" value="ราคา 7.99 ล้านบาท">
                                        <div id="img_list">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale.jpg">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale.jpg">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="">
                                <div class="other-home-img">
                                    <img src="images/temp/pj_home_sale.jpg" alt="">
                                </div>
                                <a>
                                    <p class="title">ชัยพฤกษ์</p>
                                    <p class="gray mb0">รสนิยมเห่งการอยู่อาศัยที่สมบูรณ์แบบ</p>
                                    <p class="title green"><b>ราคา 5.49 ล้านบาท</b></p>
                                    <div id="data-slider" style="display: none">
                                        <input type="hidden" id="title_home" value="CEDAR B">
                                        <input type="hidden" id="format_home" value="CEDAR B">
                                        <input type="hidden" id="id_home" value="01H07">
                                        <input type="hidden" id="identity_home" value="Loof Of Title">
                                        <input type="hidden" id="price_home" value="ราคา 7.98 ล้านบาท">
                                        <div id="img_list">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale.jpg">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="">
                                <div class="">
                                    <img src="images/temp/pj_home_sale.jpg" alt="">
                                </div>
                                <a>
                                    <p class="title">ชัยพฤกษ์</p>
                                    <p class="gray mb0">รสนิยมเห่งการอยู่อาศัยที่สมบูรณ์แบบ</p>
                                    <p class="title green"><b>ราคา 5.49 ล้านบาท</b></p>
                                    <div id="data-slider" style="display: none">
                                        <input type="hidden" id="title_home" value="CEDAR C">
                                        <input type="hidden" id="format_home" value="CEDAR C Custom">
                                        <input type="hidden" id="id_home" value="01I07">
                                        <input type="hidden" id="identity_home" value="CEDAR C Custom Private">
                                        <input type="hidden" id="price_home" value="ราคา 16.88 ล้านบาท">
                                        <div id="img_list">
                                            <input type="hidden" class="img_url" value="images/temp/pj_home_sale.jpg">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="project-info-other-block block-margin">
                <div class="row">
                    <div id="sec-virtual" class="col-md-6">
                        <p class="heading-title header-margin">ภาพ 360 องศา</p>
                        <div class="project-info-other-img">
                            <a href="#" data-featherlight="#lb360">
                                <span class="i-view-360"></span>
                                <img src="http://devwww2.lh.co.th/Land_and_house/Backend/fileupload/images/360_thumnail/11-01-2017-10-28-05-MRC01.jpg" alt="">
                            </a>
                            <iframe class="lightbox" src="http://www.lh.co.th/360file/VBA/" width="1000"
                                    height="560" id="lb360" style="border:none;" webkitallowfullscreen
                                    mozallowfullscreen allowfullscreen></iframe>
                        </div>
<!--                        <p class="title">Good Moment</p>-->
                    </div>
                    <div id="sec-tvc" class="col-md-6">
                        <p class="heading-title header-margin">VDO</p>
                        <div class="project-info-other-img">
                            <a href="#" data-featherlight="#lbVdo1">
                                <span class="i-view-vdo"></span>
                                <img src="http://devwww2.lh.co.th/Land_and_house/Backend/fileupload/images/tvc/banner.jpg" alt="">
                            </a>
                            <iframe class="lightbox" src="https://www.youtube.com/embed/GJkPSkZ80bo?autoplay=0" width="1000"
                                    height="560" id="lbVdo1" style="border:none;" webkitallowfullscreen
                                    mozallowfullscreen allowfullscreen></iframe>

                        </div>
<!--                        <p class="title">Good Moment</p>-->
                    </div>
                </div>
            </div>
        </div>

        <div id="sec-location" class="project-location-block block-margin">
            <div class="container">
                <p class="heading-title header-margin">Location</p>


                <div class="map-container">
                    <div id="map"></div>
                    <span class="i-getdirection"></span>
                    <span class="i-getdirection-car"></span>
                </div>
                <div id="info-window" style="display: none;">
                    <div style="width: auto;">
                        <img src="images/logo/logo_mtn.jpg" class="background-logo" style="display: inline-block"/>
                        <p style="display: inline-block;top: 10%;">ราคาเริ่มต้น <br> <span>3.79 - 5 ล้านบาท</span></p>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p class="title">สิ่งอำนวยความสะดวกโดยรอบโครงการ</p>
                        <div class="content-style">
                            <div class="col-md-6">
                                <ul class="tbs1">
                                    <li>ใกล้ม.มหิดล ศาลายา 4.00 กม.</li>
                                    <li>ใกล้ Central ศาลายา 4.00 กม.</li>
                                    <li>ไป Central ปิ่นเกล้า 20.00 กม.จากเชิงสะพานปิ่นเกล้า</li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul class="tbs1">
                                    <li>ใกล้ม.มหิดล ศาลายา 4.00 กม.</li>
                                    <li>ใกล้ Central ศาลายา 4.00 กม.</li>
                                    <li>ไป Central ปิ่นเกล้า 20.00 กม.จากเชิงสะพานปิ่นเกล้า</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div id="sec-contact" class="block-margin">
                <p class="heading-title header-margin">Contact & Appointment</p>
                <span></span>
                <div>
                    <form action="" class="project-form form-container">
                        <p class="title-label"></p>
                        <div class="">
                            <div class="form-group radio-checkmark">
                                <input type="radio" id="check-contact" name="check-type" checked/>
                                <label for="check-contact" class="title-label"><span></span>ติดต่อสอบถาม</label>
                                <input type="radio" id="check-appoint" name="check-type"/>
                                <label for="check-appoint"
                                       class="title-label"><span></span>นัดหมายเยี่ยมชมโครงการ</label>
                            </div>
                            <div class="form-group check-type-radio">
                                <input type="text" id="date_appointment" name="" class="form-control"
                                       placeholder="*วันที่ต้องการนัดหมาย">
                            </div>
                            <div class="form-group">
                                <input type="text" name="" class="form-control" placeholder="ccr@lh.co.th">
                            </div>
                            <div class="form-group">
                                <input type="text" name="" class="form-control" placeholder="*ระบุชื่อ - นามสกุล">
                            </div>
                            <div class="form-group">
                                <input type="text" name="" class="form-control" placeholder="*หมายเลขโทรศัพท์">
                            </div>
                            <div class="form-group">
                                <input type="text" name="" class="form-control" placeholder="*อีเมล์">
                            </div>
                            <div class="form-group">
                                    <textarea name="" placeholder="*ระบุข้อความ" id="" class="form-control" cols="30"
                                              rows="10"></textarea>
                            </div>
                            <div class="title-block">
                                <p class="title">ต้องการให้ติดต่อกลับทาง</p>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" id="check-tel" name="check-tel"/>
                                <label for="check-tel" class="title-label"><span></span>หมายเลขโทรศัพท์</label>
                                <input type="checkbox" id="check-mail" name="check-mail"/>
                                <label for="check-mail" class="title-label"><span></span>อีเมล์</label>
                            </div>
                            <button class="btn btn-submit">ส่งข้อมูล</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>


<?php include('footer.php'); ?>