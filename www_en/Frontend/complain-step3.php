<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>
<?php include('header.php'); ?>
    <!-- CSS -->
<link rel="stylesheet" href="<?= file_path('css/complain.css') ?>" type="text/css">

    <div class="page-banner-md banner-hide">
        <div id="bannerSlide">
            <div class="item">
                <div class="banner-parallax">
                    <img src="<?= file_path('images/temp2/banner_complain.jpg') ?>" alt="">
                </div>
            </div>
        </div>
    </div>

    <div id="content" class="content complain-step3 rps-content-pd">
        <div class="container">
            <h1 class="heading-title">ร้องเรียนเรื่องบ้านและคอนโด</h1>

            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div>
<!--                       <p class="title banner-hide">คุณสามารถค้นหาชื่อโครงการที่คุณอาศัย และส่งข้อความร้องเรียนบ้านและคอนโดมาที่ แลนด์ แอนด์เฮ้าส์</p>-->
                        <div class="complain-form-block">
                            <form action="" class="complain-form form-container">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="step-label">
                                            step 3
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <p class="title">ร้องเรียนเรื่องบ้านและคอนโด</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10">
                                        <p class="pmargin">กรุณากรอกข้อมูลด้านล่างเพื่อร้องเรียนเกี่ยวกับเรื่องต่างๆ ที่คุณต้องการ</p>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="project-name">
                                            <p class="title">
                                                ชื่อโครงการ<br>
                                                <span>ชัยพฤกษ์ วัชรพล</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="">
                                            <div class="form-group">
                                                <input type="text" name="" class="form-control" placeholder="ชื่อ-นามสกุล">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" name="" class="form-control" placeholder="อีเมล์">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" name="" class="form-control" placeholder="โทรศัพท์">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" name="" class="form-control" placeholder="บ้านเลขที่">
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-offset-3 col-md-8">
                                        <button type="reset" class="btn btn-submit clear">Clear</button>
                                        <a href="<?= $router->generate('complain-step',['step' => 4]) ?>" class="btn btn-submit next">Next</a>
                                    </div>
                                </div>

                            </form>

                            <a href="<?= $router->generate('complain-step',['step' => 2]) ?>" class="step-back">< Back</a>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


<?php include('footer.php'); ?>
