<!-- <script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script> -->
<script src="https://www.google.com/recaptcha/api.js?onload=ReCaptchaCallback&render=explicit" async defer></script>
<script type="text/javascript">
    //         var CaptchaCallback = function() {
        
    //     grecaptcha.render('RecaptchaField1', 
    //                     {'sitekey' : '6LdzoRYUAAAAAMiP6KQwKoy-PS8mhON700qVex_a',
    //                     callback: function () {
    //                         $('#chacheck1').hide();
    //                     }
    //                 });

    //     grecaptcha.render('RecaptchaField2', {'sitekey' : '6LdzoRYUAAAAAMiP6KQwKoy-PS8mhON700qVex_a',
    //                     callback: function () {   
    //                          $('#chacheck2').hide();
    //                     }
    //     });
    // };
    
            var ReCaptchaCallback = function() {
    	       $('.g-recaptcha').each(function(){
    		      var el = $(this);
    		          grecaptcha.render(el.get(0), {'sitekey' : el.data("sitekey"),
    				         callback: function () {
                               
                                    $('#chacheck').hide();
                                    $('#chacheck1').hide();
                           
                             }
    			     });
    	        });  
            };
</script>
<style>
    .error{
        color: red;
    }
</style>
 <script src="../build/js/jquery.validate.js"></script>
<script>
  
  $(document).ready(function() {
            
            var email = $('#mailcont').val();
            if(email === undefined || email  === null){              
                
                $('#emailpro').val('info@lh.co.th'); 
   
            }else{              
                
                $('#emailpro').val(email); 
            }
 
            $("#CheckContForm").validate({
                rules: {
                    fullname: "required",
                    phonecon: "required",
                    email: "required",
                    detail : "required",
                    dateappointment : "required",
                },
                messages: {
                    fullname: "กรุณากรอกชื่อ-นามสกุล !",
                    phonecon : "กรุณากรอกหมายเลขโทรศัพท์ !",
                    email : "กรุณากรอกอีเมล์ !",
                    detail : "กรุณาระบุข้อความ !",
                    dateappointment : "กรุณาระบุวันที่ต้องการนัดหมาย !"

                }
            });

            $('#phonecon').keydown(function(event){
                var max = 10;
                var phone = $(this).val().length;
                
                if(phone >= max){

                    if(event.keyCode == 8 || event.keyCode == 46){
                        return true;                    
                    }                        
                        return false;
                    
                }
            });

            $('#CheckContForm').on('submit', function(){
                var googleResponse = $('#g-recaptcha-response').val();
                
                if(googleResponse==''){   
                    $('#chacheck').show();

                     return false;
           
                }

            });

            $('#popcontact').click(function(){
                
                    $('#date_appointment_ft-error').hide();
                

            })
             $('#popappoint').click(function(){
                

                   $('#date_appointment_ft-error').show();

            })

 

     //        	if(word == 'snippet'){
            		
     //        		contact = $("input[name='check-type']:checked").val();
     //        		email_lh = $('#emailpro').val();
     //        		customer_name = $('#fullname').val();
     //        		tell = $('#phonecon').val();
     //        		email_cus = $('#email').val();
     //        		content = $('#detail').val();

     //        		var data = {'contact': contact, 
     //        					'email_lh': email_lh, 
     //        					'customer_name': customer_name,
     //        					'tell': tell, 
     //        					'email_cus': email_cus,
     //        					'content': content, 
     //        					};
     //        					console.log(data);
     //        	}else{
            		
     //        		contact2 = $("input[name='check-type-home']:checked").val();
					// email_lh2 = $('#mailcont').val();
     //        		customer_name2 = $('#namecont').val();
     //        		tell2 = $('#phonecont').val();
     //        		email_cus2 = $('#emailcust').val();
     //        		content2 = $('#detailcont').val();
            		
     //        		var data = {'contact': contact2, 
     //        					'email_lh': email_lh2, 
     //        					'customer_name': customer_name2,
     //        					'tell': tell2, 
     //        					'email_cus': email_cus2,
     //        					'content': content2, 
     //        					};
     //        					console.log(data);
     //        	}

     //        	$.ajax({
       					 	
     //   					 	type: "POST",
     //    					url: "#",
     //    					data: data,
     //    					dataType: "json",
        				
     //    				success: function(data) {
            				
     //    				}
    	// 		})
 

        });

</script>

<div class="contact-container contact-container-ft topdropdown-container">
    <span class="panel-close">close</span>

    <h3>Contact & Appointment</h3>
    <div>
        <form class="project-form form-container" id="CheckContForm" name="CheckContForm" action="mail-snippet-contact.php" method="POST">
            <p class="title-label"></p>
            <div class="">
                <div class="form-group radio-checkmark">
                    <input type="radio" id="check-contact-ft" name="check-type" value="contact" checked/>
                    <label for="check-contact-ft" id="popcontact" class="title-label" style="color: black"><span></span>ติดต่อสอบถาม</label>
                    <input type="radio" id="check-appoint-ft" name="check-type"/ value="appointment">
                    <label for="check-appoint-ft" id="popappoint" class="title-label" style="display: none; color: black;"><span></span>นัดหมายเยี่ยมชมโครงการ</label>
                </div>
                <div class="form-group check-type-radio">
                    <input type="text" id="date_appointment_ft" name="dateappointment" class="form-control"
                           placeholder="*วันที่ต้องการนัดหมาย" style="background-color: #ffffff;" readonly required>
                </div>
                <div class="form-group">
                    <input type="text" name="emailpro" id="emailpro" class="form-control" 
                    style="background-color: #ffffff;" readonly>
                </div>
                <div class="form-group">
                    <input type="text" name="fullname" id="fullname" class="form-control" placeholder="*ระบุชื่อ - นามสกุล" required>
                </div>
                <div class="form-group">
                    <input type="number" id="phonecon" name="phonecon" class="form-control" min="0" 
                    placeholder="*หมายเลขโทรศัพท์" required>
                </div>
                <div class="form-group">
                    <input type="email" name="email" id="email" class="form-control" placeholder="*อีเมล์" required>
                </div>
                <div class="form-group">
                    <textarea name="detail" id="detail" placeholder="*ระบุข้อความ" id="" class="form-control" cols="30"
                              rows="3" required></textarea>
                </div>
                <div class="title-block backcontact" style="display: none;">
                    <p class="title">ต้องการให้ติดต่อกลับทาง</p>
                </div>
                <div class="form-group backcontact" style="display: none;">
                    <input type="checkbox" id="check-tel-ft" name="check-tel-ft" value="tell">
                    <label for="check-tel-ft" class="title-label" style="color: black"><span></span>หมายเลขโทรศัพท์</label>
                    <input type="checkbox" id="check-mail-ft" name="check-mail-ft" value="email">
                    <label for="check-mail-ft" class="title-label"style="color: black"><span></span>อีเมล์</label>
                </div>
                <!-- <div id="RecaptchaField1"></div> -->
                <div class="g-recaptcha" data-sitekey="6LcSzSEUAAAAAPTUl4maci5Pl4SyWVMmnNhly0Qw"></div>
                <span id="chacheck" style="display: none; color: red;">พิสูจน์ว่าท่านไม่ใช่บอท !</span>
                <br><br>
                <input type="submit" class="btn-search" id="btnsendmail"  name="submit" value="ส่งข้อมูล">
            </div>
        </form>
    </div>
</div>
