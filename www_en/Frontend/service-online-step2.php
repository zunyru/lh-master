<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>
<?php include('header.php'); ?>
    <!-- CSS -->
    <link rel="stylesheet" href="<?= file_path('css/service-online.css') ?>" type="text/css">
    <!-- JS -->
<!--    <script src="js/service-online.js"></script>-->

    <div id="content" class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-11 col-md-offset-1">
                    <h1 class="heading-title">แจ้งซ่อมออนไลน์</h1>
                    <div class="customer-info">
                        <p class="title">ข้อมูลลูกค้า</p>

                        <div class="row">
                            <div class="col-md-4">
                                <p>ชื่อ-สกุล ลูกค้า : นายสมชาย สมชื่อ</p>
                            </div>
                            <div class="col-md-4">
                                <p>โทรศัพท์ติดต่อ : 087-927-6955</p>
                            </div>
                            <div class="col-md-4">
                                <p>Email : somsak11@hotmail.com</p>
                            </div>
                        </div>
                    </div>

                    <div class="customer-info">
                        <p class="title">รายละเอียดบ้าน</p>
                        <p>หลังที่: 1</p>
                        <p>โครงการ: นันทวัน - เชียงใหม่</p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="">
                                    <p>
                                        บ้านหลังที่: 176/33<br>
                                        แบบบ้าน: คาลลา ลิลลี่<br>
                                        สถานะของงานรับประกัน 1 ปี: หมดประกัน <a href="" class="customer-view-btn">View</a><br>
                                        สถานะของงานรับประกัน 5 ปี: หมดประกัน<br>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="">
                                    <p>
                                        แปลง: 06A03<br>
                                        วันที่โอน: 30/12/2553<br>
                                        พื้นที่บ้าน: 109.90 ตร.ว.<br>
                                        อัตราจัดเก็บค่าบริการสาธารณะ: บาท / ต่อหลัง สิ้นสุด ณ<br>
                                    </p>

                                </div>
                            </div>
                        </div>
<!--                        <a href="" class="btn btn-submit">ดูประวัติการแจ้งซ่อมทั้งหมด</a>-->

                        <div class="service-history-block">
                            <p class="title">ประวัติการแจ้งซ่อมทั้งหมด</p>
                            <table class="tb-history" width="100%">
                                <thead>
                                <tr>
                                    <th>โครงการ</th>
                                    <th>บ้านเลขที่</th>
                                    <th>วันที่แจ้งซ่อม</th>
                                    <th>วันที่นัดตรวจ</th>
                                    <th>สถานะ</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>นันทวัน - เชียงใหม่</td>
                                    <td>176/33</td>
                                    <td><a href="" class="lnk-detail">9/02/2016 11:38 น.</a></td>
                                    <td>รอนัดหมาย</td>
                                    <td>ยกเลิกรายการแล้ว</td>
                                </tr>
                                <tr>
                                    <td>นันทวัน - เชียงใหม่</td>
                                    <td>176/33</td>
                                    <td><a href="" class="lnk-detail">9/02/2016 11:38 น.</a></td>
                                    <td>16/02/2017</td>
                                    <td>ดำเนินการแก้ไขเรียบร้อย</td>
                                </tr>
                                <tr>
                                    <td>นันทวัน - เชียงใหม่</td>
                                    <td>176/33</td>
                                    <td><a href="" class="lnk-detail">9/02/2016 11:38 น.</a></td>
                                    <td>รอนัดหมาย</td>
                                    <td>รอดำเนินการแก้ไข</td>
                                </tr>
                                <tr>
                                    <td>นันทวัน - เชียงใหม่</td>
                                    <td>176/33</td>
                                    <td><a href="" class="lnk-detail">9/02/2016 11:38 น.</a></td>
                                    <td>16/02/2017</td>
                                    <td>ยกเลิกรายการแล้ว</td>
                                </tr>
                                <tr>
                                    <td>นันทวัน - เชียงใหม่</td>
                                    <td>176/33</td>
                                    <td><a href="" class="lnk-detail">9/02/2016 11:38 น.</a></td>
                                    <td>รอนัดหมาย</td>
                                    <td>ดำเนินการแก้ไขเรียบร้อย</td>
                                </tr>
                                <tr>
                                    <td>นันทวัน - เชียงใหม่</td>
                                    <td>176/33</td>
                                    <td><a href="" class="lnk-detail">9/02/2016 11:38 น.</a></td>
                                    <td>16/02/2017</td>
                                    <td>รอดำเนินการแก้ไข</td>
                                </tr>
                                <tr>
                                    <td>นันทวัน - เชียงใหม่</td>
                                    <td>176/33</td>
                                    <td><a href="" class="lnk-detail">9/02/2016 11:38 น.</a></td>
                                    <td>รอนัดหมาย</td>
                                    <td>รอดำเนินการแก้ไข</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <a href="" class="step-back">< Back</a>

                </div>
            </div>


        </div>
    </div>


<?php include('footer.php'); ?>
