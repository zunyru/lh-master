<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>
<?php include('header.php'); ?>
<!-- CSS -->
<link rel="stylesheet" href="css/corporate-all.css" type="text/css">
<link rel="stylesheet" href="css/corporate-menu.css" type="text/css">

<!-- JS -->
<script src="js/corporate-menu.js"></script>

<div id="content" class="content committee-page corp-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div id="corpMenu" class="corp-menu-tab">
                    <ul>
                        <li class="current"><a href="" onclick="location.href='<?php url('/corporate-committee.php'); ?>';">คณะกรรมการบริหาร</a></li>
                        <li><a href="" onclick="location.href='<?php url('/corporate-committee.php'); ?>';">คณะกรรมการตรวจสอบ</a></li>
                        <li><a href="" onclick="location.href='<?php url('/corporate-committee.php'); ?>';">คณะกรรมการสรรหาและพิจารณาคำตอบแทน</a></li>
                        <li><a href="" onclick="location.href='<?php url('/corporate-committee.php'); ?>';">คณะกรรมการบริหารความเสี่ยง</a></li>
                        <li><a href="" onclick="location.href='<?php url('/corporate-committee.php'); ?>';">คลังรูปผู้บริหาร</a></li>
                    </ul>
                </div>

                <div id="corpBlock1" class="corp-block current">
                    <h1>คณะกรรมการบริหาร</h1>

                    <div class="committee-detail-block">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="committee-img">
                                    <img src="images/temp6/cmtee_1.jpg" alt="">
                                </div>
                                <p class="cmtee-title-stg">นายอนันต์ อัศวโภคิน</p>
                                <p>อายุ 65 ปี</p>
                            </div>
                            <div class="col-sm-6">
                                <div class="committee-detail">
                                    <p class="cmtee-title">ตำแหน่ง</p>
                                    <p>
                                        ประธานคณะกรรมการบริษัท<br>
                                        ประธานกรรมการบริหาร<br>
                                        กรรมการผู้มีอำนาจลงนาม<br>
                                        วันที่ได้รับการแต่งตั้ง 12 ธันวาคม 2535<br>
                                    </p>

                                    <p class="cmtee-title-stg">สัดส่วนการถือหุ้นในบริษัท</p>
                                    <p><span class="cmtee-title-stg">24.40</span>% ณ 31 ธ.ค. 2558</p>

                                    <p class="cmtee-title">คุณวุฒิทางการศึกษา</p>
                                    <ul>
                                        <li>วิศวกรรมศาสตร์บัณฑิต (โยธา) จุฬาลงกรณ์มหาวิทยาลัย</li>
                                        <li>M.S. Industrial Engineering, Illinois Institute of technology, Chicago, USA</li>
                                        <li>M.B.A มหาวิทยาลัยธรรมศาสตร์</li>
                                        <li>การอบรมจากสมาคมส่งเสริมสถาบันกรรมการบริษัทไทย (IOD)
                                            หลักสูตร DCP Program รุ่นที่ 52/2004</li>
                                    </ul>

                                </div>
                            </div>
                        </div>

                        <div class="work-exp-block">
                            <p class="cmtee-title">ประวัติการทำงาน</p>

                            <table class="tableprofile" cellpadding="0" cellspacing="0" width="100%">
                                <tbody><tr>
                                    <th colspan="2">
                                        <div align="left"><strong>การดำรงตำแหน่งในบริษัทที่จดทะเบียนในตลาดหลักทรัพย์แห่งประเทศไทย</strong></div>
                                    </th>
                                </tr>
                                <tr>
                                    <td width="15%">พ.ค.56-ปัจจุบัน</td>
                                    <td>ประธานคณะกรรมการบริษัท, ประธานกรรมการบริหาร, บมจ. แลนด์ แอนด์ เฮ้าส์ (พัฒนาอสังหาริมทรัพย์)</td>
                                </tr>
                                <tr>
                                    <td>2528-เม.ย.56</td>
                                    <td>ประธานกรรมการบริหาร บมจ. แลนด์ แอนด์ เฮ้าส์ (พัฒนาอสังหาริมทรัพย์)</td>
                                </tr>
                                <tr>
                                    <td>2531-เม.ย.56</td>
                                    <td>ประธานกรรมการบริหาร และกรรมการผู้จัดการ บมจ. แลนด์ แอนด์ เฮ้าส์ (พัฒนาอสังหาริมทรัพย์)</td>
                                </tr>
                                <tr>
                                    <td>2548-ปัจจุบัน</td>
                                    <td>ประธานกรรมการ บมจ. ธนาคารแลนด์ แอนด์ เฮ้าส์ (ธนาคารพาณิชย์)</td>
                                </tr>
                                <tr>
                                    <td>2538-ปัจจุบัน</td>
                                    <td>กรรมการ บมจ. โฮมโปรดักส์เซ็นเตอร์ (ค้าปลีกวัสดุก่อสร้าง)</td>
                                </tr>
                                <tr>
                                    <td>2537-ปัจจุบัน</td>
                                    <td>กรรมการ บมจ. ควอลิตี้คอนสตรัคชั่นโปรดัคส์ (ผลิตวัสดุก่อสร้าง)</td>
                                </tr>
                                <tr>
                                    <td>2526-ปัจจุบัน</td>
                                    <td>กรรมการ บมจ. ควอลิตี้เฮ้าส์ (พัฒนาอสังหาริมทรัพย์)</td>
                                </tr>
                                <tr>
                                    <th colspan="2" class="tb-row2">
                                        <div align="left"><strong>การดำรงตำแหน่งในบริษัทที่ไม่ได้จดทะเบียน ในตลาดหลักทรัพย์แห่งประเทศไทยกรรมการ</strong></div>
                                    </th>
                                </tr>
                                <tr>
                                    <td>2548-ปัจจุบัน</td>
                                    <td>กรรมการ บริษัท แอล แอนด์ เอช พร็อพเพอร์ตี้ จำกัด (พัฒนาอสังหาริมทรัพย์)</td>
                                </tr>
                                <tr>
                                    <td>2547-ปัจจุบัน</td>
                                    <td>กรรมการ บริษัท แอล เอช เรียลเอสเตท จำกัด (พัฒนาอสังหาริมทรัพย์)</td>
                                </tr>
                                <tr>
                                    <td>2547-ปัจจุบัน</td>
                                    <td>กรรมการ บริษัท แอลเอช แอสเซท จำกัด (พัฒนาอสังหาริมทรัพย์)</td>
                                </tr>
                                <tr>
                                    <td>2543-ปัจจุบัน</td>
                                    <td>กรรมการ บจ. เอเซียแอสเซทแอดไวเซอรี่ (ที่ปรึกษา)</td>
                                </tr>
                                </tbody></table>
                        </div>

                        <a href="javascript:history.back()" class="btn-backpage">< Back</a>
                    </div>

                </div>

            </div>
        </div>

    </div>
</div>


<?php include('footer.php'); ?>
