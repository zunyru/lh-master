<?php
header('Location: //'.$_SERVER['SERVER_NAME'].'/'.'en');
exit();

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

$promotions = getPromotions();
$highlights = getAllHighlight();
$page = 'promotion';
$img = '';
$page_index = 0;
?>
<?php
include('header.php');
elementMetaTitleDisTag($page, $page_index ,$img);
?>
    <!-- JS -->
    <link rel="stylesheet" href="<?= file_path('css/promotion.css') ?>" type="text/css">
    <link rel="stylesheet" href="<?= file_path('css/masonry-block.css') ?>" type="text/css">
    <script src="<?= file_path('js/promotion.js') ?>"></script>

    <div id="content" class="content promotion-page">

    <div class="container">
        <?php 
        if(count($highlights) != 0 ){
            $SEO = getSEOUrl($actual_link);
            if($actual_link == @$SEO->url_page){
                echo "<h1>".$SEO->h1."</h1>";
            }else{
               echo "<h1>Promotion and extra privileges</h1>";  
           }

           ?>

           <div class="activity-block">
            <h2 class="grey-h">Meet new <?= count($highlights) ?> interesting activities  from LH</h2>
            <div class="row">
                <div id="col2Slide" class="col-slide">
                    <?php
                    foreach ($highlights as $i => $highlight){
                        ?>

                            <?php
                            $highlight_url = strpos($highlight->highlight_link, 'https://') !== false || strpos($highlight->highlight_link, 'http://') !== false ? $highlight->highlight_link : "http://$highlight->highlight_link";
                            $highlight_url = str_replace(' ','',$highlight_url);
                            ?>



                            <div class="item">
                                <div class="">
                                    <div class="activity-list">
                                        <?php
                                        if ($highlight->highlights_lead_img_type == 'image') {
                                            ?>
                                            <div class="list-item-img">
                                                <a <?php if(!empty($highlight_url)) { ?>href="<?= $highlight_url ?>" target="_blank" <?php } ?> >
                                                    <img src="<?= backend_url('base', $highlight->highlights_lead_img) ?>" alt="">
                                                </a>
                                            </div>
                                        <?php
                                        }elseif ($highlight->highlights_lead_img_type == 'youtube') {
                                            $highlight_thumbnail    = backend_url('base',$highlight->highlight_thumbnail);
                                            $highlight_url          = str_replace('watch?v=', 'embed/', $highlight->highlights_lead_img);
                                            ?>
                                            <div class="list-item-img">
                                                <a href="#" data-featherlight="#lbVdo<?= $i ?>">
                                                    <span class="i-view-vdo"></span>
                                                    <img src="<?= $highlight_thumbnail ?>" alt="" class="hm-highl">
                                                </a>
                                                <iframe class="lightbox" src="<?= $highlight_url . "?autoplay=0" ?>"
                                                        width="1000"
                                                        height="560" id="lbVdo<?= $i ?>" style="border:none;" webkitallowfullscreen
                                                        mozallowfullscreen allowfullscreen></iframe>
                                            </div>
                                        <?php
                                        }elseif($highlight->highlights_lead_img_type == 'vdo'){
                                            $highlight_thumbnail    = backend_url('base',$highlight->highlight_thumbnail);
                                            $highlight_vdo_file     = backend_url('base',$highlight->highlights_lead_img);
                                            ?>
                                            <div class="list-item-img">

                                                <a id="ban_vdo<?= $i ?>">
                                                    <span class="i-view-vdo"></span>
                                                    <img src="<?= $highlight_thumbnail ?>"
                                                         alt="" class="hm-highl">
                                                </a>
                                                <script>
                                                    $(document).ready(function () {
                                                        $('#ban_vdo<?= $i ?>').click(function () {
                                                            $('#lbVdo<?= $i ?>').show();
                                                            $.featherlight($('#lbVdo<?= $i ?>'),{});
                                                            $('.featherlight-content #video_player<?= $i ?>')[0].play();
                                                            $('#lbVdo<?= $i ?>').hide();
                                                        });
                                                    })
                                                </script>
                                                <div id="lbVdo<?= $i ?>" style="position:relative;width: 100%;display: none;">
                                                    <video id='video_player<?= $i ?>' preload='none' controls>
                                                        <source src="<?= $highlight_vdo_file ?>" type="video/mp4">
                                                    </video>
                                                </div>

                                            </div>
                                        <?php
                                        }
                                        ?>


                                        <a <?php if(!empty($highlight->highlight_link)) { ?>href="<?= $highlight->highlight_link ?>" target="_blank" <?php } ?> >
                                            <div class="activity-detail">
                                                <div>
                                                    <p class="title"><?= $highlight->highlights_name_th ?></p>
                                                    <p class="concept<?= $i ?> concept-in-list">
                                                     <?=!empty($highlight->highlights_dis_th) ? str_replace(array("\n\r", "\n","n/g"), '<br/>', $highlight->highlights_dis_th) : ''?>
                                                    </p>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </a>

                                        <?php if (!empty($highlight->highlight_link)) {
                                                            ?>
                                                            <a href="<?= $highlight->highlight_link ?>"
                                                               target="_blank"
                                                               class="btn btn-seemore">See
                                                                more</a>
                                                            <?php
                                                        } ?>

                                        

                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php
            $countPromotion = 0;
            foreach ($promotions as $promotion){
                $project_id = $promotion->project_id;
                $project = getProjectByID($project_id);
                if( empty($_GET['zone_id']) || (!empty($_GET['zone_id']) && $_GET['zone_id'] == $project->zone_id ) ) {
                    $countPromotion++;
                }
            }
            ?>

            <?php if($countPromotion > 0){
                ?>
                <div class="promotion-block">
                    <h2 class="grey-h">Introduce<?= $countPromotion ?> project with interesting & attractive promotion </h2>
                    <div class="sort-container">
                        <div class="sort-block">
                            <input type="hidden" name="sortSelect" value="">
                            <div class="sort" data-status="0">
                            <span>
                                <?php
                                $zones = getPromotionProjectZones();
                                if(!empty($_GET['zone_id'])){
                                    foreach($zones as $zone) {
                                        if($_GET['zone_id'] == $zone->zone_id){
                                            echo $zone->zone_name_th;
                                        }
                                    }
                                }else{
                                    echo 'Select All interested locations';
                                }
                                ?>
                            </span> <i class="i-sort"></i>
                            </div>
                            <ul id="sortLists">
                                <li data-value="เลือกทำเลที่สนใจทั้งหมด" onclick="location.href='<?= $router->generate('promotion') ?>'">
                                    Select All interested locations
                                </li>
                                <?php
                                foreach($zones as $zone) {  $zone_names =$zone->zone_name_th ; $zone_names= str_replace(' ', '', $zone_names);?>
                                    <li data-value="<?= $zone->zone_name_th ?>" onclick="location.href='<?= $router->generate('promotion',['zone_name'=>$zone_names]) ?>'">
                                        <?= $zone->zone_name_th ?>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <div class="masonry-lists">
                        <?php
                        if($promotions != false){
                            foreach ($promotions as $i => $promotion){
                                $project_id = $promotion->project_id;
                                $project = getProjectByID($project_id);
                                $banner  = getBannerByProjectID($project_id);
                                $brand   = getBrandByID($project->brand_id);

                                if( empty($_GET['zone_id']) || (!empty($_GET['zone_id']) && $_GET['zone_id'] == $project->zone_id ) ) {
                                    ?>
                                    <div class="list-item">

                                        <?php

                                        $project_id  = $promotion->project_id;
                                        $project     = getProjectByID($project_id);
                                        $project_sub = getProjectSub($project_id,1);
                                        $product_id  = 1;
                                        if($project_sub == false){
                                            $project_sub = getProjectSub($project_id,2);
                                            $product_id  = 2;
                                        }
                                        if($project_sub == false){
                                            $project_sub = getProjectSub($project_id,3);
                                            $product_id  = 3;
                                        }

                                        $url_see_more = "";
                                        if($product_id == 1 ){
                                            $url_see_more = isLadawan($project->project_id) ?
                                                $router->generate('ladawan-detail',[
                                                    'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                                    'product_id'    =>  1
                                                ]) :
                                                $router->generate('single-home-detail',[
                                                    'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                                    'product_id'    =>  1
                                                ]);
                                        }elseif( $product_id == 2){
                                            $url_see_more = isLadawan($project->project_id) ?
                                                $router->generate('town-home-detail',[
                                                    'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                                    'product_id'    =>  2
                                                ]) :
                                                $router->generate('town-home-detail',[
                                                    'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                                    'product_id'    =>  2
                                                ]);
                                        }
                                        elseif($product_id == 3){
                                            $url_see_more = isLadawan($project->project_id) ?
                                                $router->generate('ladawan-detail',[
                                                    'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                                    'product_id'    =>  3
                                                ]) :
                                                $router->generate('condominium-detail',[
                                                    'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                                    'lang'          =>  'th'
                                                ]);
                                        }else{
                                            $url_see_more = isLadawan($project->project_id) ?
                                                $router->generate('ladawan-detail',[
                                                    'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                                    'product_id'    =>  $product_id
                                                ]) :
                                                $router->generate('single-home-detail',[
                                                    'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                                    'product_id'    =>  $product_id
                                                ]);
                                        }

                                        $promotion_url = str_replace(' ','',$promotion->promotion_url);
                                        if(!empty($promotion_url)){
                                            $url_see_more = strpos($promotion_url, 'https://') !== false || strpos($promotion_url, 'http://') !== false ? $promotion_url : "http://$promotion_url";
                                        }
                                        ?>

                                        <div class="list-item-img">
                                            <a href="<?= $url_see_more ?>">
                                                <?php
                                                $img_promotion = getImagePromotion($promotion->promotion_id);
                                                if ($img_promotion != false && $img_promotion->img_promotion_name) {
                                                    ?>
                                                <img src="<?= backend_url('base', $img_promotion->img_promotion_name)  ?>"
                                                     alt="">
                                                <?php
                                                } ?>
                                            </a>
                                        </div>

                                        <a href="<?= $url_see_more ?>">
                                            <p class="title"><?= $project->project_name_th ?></p>
                                            <p class="title">Promotions : <span
                                                        class="green"><?= $promotion->promotion_name_th ?></span></p>
                                            <p><?=!empty($promotion->promotion_detail_th) ? str_replace(array("\n\r", "\n","n/g"), '<br/>', $promotion->promotion_detail_th) : ''?></p>
                                        </a>
                                    </div>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>


<?php include('footer.php'); ?>