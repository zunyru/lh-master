<?php

$logo_homes      = getLogoByProductID(1);
$logo_townhomes  = getLogoByProductID(2);
$logo_condos     = getLogoByProductID(3);
$logo_homesells  = getLogoByProductID(1);

$zone_homes         = getZoneBkkByProductID(1);
$zone_home_countrys = getZoneCountryByProductID(1);

$zone_townhomes         = getZoneBkkByProductID(2);
$zone_townhome_countrys = getZoneCountryByProductID(2);

$zone_condos         = getZoneBkkByProductID(3);
$zone_condo_countrys = getZoneCountryByProductID(3);

$zone_homesells         = getZoneBkkByProductID(1);
$zone_homesell_countrys = getZoneCountryByProductID(1);

?>

<!--home-->
<div class="menu-home-series home">
    <div class="menu-home-series-close">CLOSE</div>
    <div class="container">
        <div class="row">
            <div class="menu-col-left home">
                <div class="col-md-5 col-md-offset-1">
                    <p class="project-menu-title">เลือกบ้านเดี่ยวบนทำเลที่สนใจ</p>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="project-menu-subtitle">กรุงเทพและปริมณฑล</p>
                            <ul>
                                <?php
                                if($zone_homes != false){
                                    foreach ($zone_homes as $zone_home){
                                ?>
                                        <li><a href="<?php url('/project-type.php?zone_id='.$zone_home->zone_id) ?>"><?= $zone_home->zone_name ?></a></li>
                                <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <p class="project-menu-subtitle">ต่างจังหวัด</p>
                            <ul>
                                <?php
                                if($zone_home_countrys != false){
                                    foreach ($zone_home_countrys as $zone_home){
                                        ?>
                                        <li><a href="<?php url('/project-type.php?zone_id='.$zone_home->zone_id) ?>"><?= $zone_home->zone_name ?></a></li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="menu-col-right home">
                <div class="col-md-6">
                    <p class="project-menu-title">เลือกบ้านเดี่ยวในแบรนด์ที่สนใจ</p>
                    <?php
                    $logo_home1 = $logo_homes[0];
                    $logo_home2 = $logo_homes[1];
                    $logo_home3 = $logo_homes[2];
                    $logo_home4 = $logo_homes[3];
                    $logo_home5 = $logo_homes[4];
                    $logo_home6 = $logo_homes[5];
                    ?>
                    <ul class="project-menu-logo">
                        <li>
                            <a href="<?php if(is_object($logo_home1)) url('/search-by-brand.php?brand_id='.$logo_home1->brand_id.'&product_id=1'); ?>">
                                <img src="<?= is_object($logo_home1) ? backend_url('logo',$logo_home1->logo_brand_th) : '' ?>" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="<?php if(is_object($logo_home2)) url('/search-by-brand.php?brand_id='.$logo_home2->brand_id.'&product_id=1');  ?>" style="width: 100px;">
                                <img src="<?= is_object($logo_home2) ? backend_url('logo',$logo_home2->logo_brand_th) : ''  ?>" alt="">
                            </a>
                        </li>
                    </ul>
                    <ul class="project-menu-logo m-line-2">
                        <li>
                            <a href="<?php if(is_object($logo_home3)) url('/search-by-brand.php?brand_id='.$logo_home3->brand_id.'&product_id=1');  ?>">
                                <img src="<?= is_object($logo_home3) ? backend_url('logo',$logo_home3->logo_brand_th) : '' ?>" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="<?php if(is_object($logo_home4)) url('/search-by-brand.php?brand_id='.$logo_home4->brand_id.'&product_id=1'); ?>">
                                <img src="<?= is_object($logo_home4) ? backend_url('logo',$logo_home4->logo_brand_th) : '' ?>" alt="">
                            </a>
                        </li>
                        <li class="l-inizio">
                            <a href="<?php if(is_object($logo_home5)) url('/search-by-brand.php?brand_id='.$logo_home5->brand_id.'&product_id=1'); ?>">
                                <img src="<?= is_object($logo_home5) ? backend_url('logo',$logo_home5->logo_brand_th) : '' ?>" alt="">
                            </a>
                        </li>
                        <li class="l-inizio">
                            <a href="<?php if(is_object($logo_home5)) url('/search-by-brand.php?brand_id='.$logo_home6->brand_id.'&product_id=1'); ?>">
                                <img src="<?= is_object($logo_home5) ? backend_url('logo',$logo_home6->logo_brand_th) : '' ?>" alt="">
                            </a>
                        </li>
                    </ul>
                    <a href="<?php url('/project-type.php') ?>" class="project-menu-title">ดูบ้านเดี่ยวทั้งหมด ></a>
                </div>
            </div>
        </div>
    </div>
</div>

<!--town home-->
<div class="menu-home-series townhome">
    <div class="menu-home-series-close">CLOSE</div>
    <div class="container">
        <div class="row">
            <div class="menu-col-left townhome">
                <div class="col-md-5 col-md-offset-1">
                    <p class="project-menu-title">เลือกทาวน์โฮมบนทำเลที่สนใจ</p>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="project-menu-subtitle">กรุงเทพและปริมณฑล</p>
                            <ul>
                                <?php
                                if($zone_townhomes != false){
                                    foreach ($zone_townhomes as $zone_home){
                                        ?>
                                        <li><a href="<?php url('/project-type-townhome.php?zone_id='.$zone_home->zone_id) ?>"><?= $zone_home->zone_name ?></a></li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <p class="project-menu-subtitle">ต่างจังหวัด</p>
                            <ul>
                                <?php
                                if($zone_townhome_countrys != false){
                                    foreach ($zone_townhome_countrys as $zone_home){
                                        ?>
                                        <li><a href="<?php url('/project-type-townhome.php?zone_id='.$zone_home->zone_id) ?>"><?= $zone_home->zone_name ?></a></li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="menu-col-right townhome">
                <div class="col-md-6">
                    <p class="project-menu-title">เลือกทาวน์โฮมในแบรนด์ที่สนใจ</p>
                    <?php
                    $logo_townhome1 = $logo_townhomes[0];
                    $logo_townhome2 = $logo_townhomes[1];
                    $logo_townhome3 = $logo_townhomes[2];
                    $logo_townhome4 = $logo_townhomes[3];
                    $logo_townhome5 = $logo_townhomes[4];
                    $logo_townhome6 = $logo_townhomes[5];
                    ?>
                    <ul class="project-menu-logo">
                        <li><a href="<?php if(is_object($logo_townhome1)) url('/search-by-brand.php?brand_id='.$logo_townhome1->brand_id.'&product_id=2'); ?>"><img src="<?= is_object($logo_townhome1) ? backend_url('logo',$logo_townhome1->logo_brand_th) : '' ?>" alt=""></a></li>
                        <li><a href="<?php if(is_object($logo_townhome2)) url('/search-by-brand.php?brand_id='.$logo_townhome2->brand_id.'&product_id=2'); ?>" style="width: 100px;"><img src="<?= is_object($logo_townhome2) ? backend_url('logo',$logo_townhome2->logo_brand_th) : '' ?>" alt=""></a></li>
                    </ul>
                    <ul class="project-menu-logo m-line-2">
                        <li><a href="<?php if(is_object($logo_townhome3)) url('/search-by-brand.php?brand_id='.$logo_townhome3->brand_id.'&product_id=2'); ?>"><img src="<?= is_object($logo_townhome3) ? backend_url('logo',$logo_townhome3->logo_brand_th) : '' ?>" alt=""></a></li>
                        <li><a href="<?php if(is_object($logo_townhome4)) url('/search-by-brand.php?brand_id='.$logo_townhome4->brand_id.'&product_id=2'); ?>"><img src="<?= is_object($logo_townhome4) ? backend_url('logo',$logo_townhome4->logo_brand_th) : '' ?>" alt=""></a></li>
                        <li class="l-inizio"><a href="<?php if(is_object($logo_townhome5)) url('/search-by-brand.php?brand_id='.$logo_townhome5->brand_id.'&product_id=2'); ?>"><img src="<?= is_object($logo_townhome5) ? backend_url('logo',$logo_townhome5->logo_brand_th) : '' ?>" alt=""></a></li>
                        <li class="l-inizio"><a href="<?php if(is_object($logo_townhome6)) url('/search-by-brand.php?brand_id='.$logo_townhome6->brand_id.'&product_id=2'); ?>"><img src="<?= is_object($logo_townhome6) ? backend_url('logo',$logo_townhome6->logo_brand_th) : '' ?>" alt=""></a></li>
                    </ul>
                    <a href="<?php url('/project-type-townhome.php') ?>" class="project-menu-title">ดูทาวน์โฮมทั้งหมด </a>
                </div>
            </div>
        </div>
    </div>
</div>

<!--condominium-->
<div class="menu-home-series condo">
    <div class="menu-home-series-close">CLOSE</div>
    <div class="container">
        <div class="row">
            <div class="menu-col-left condo">
                <div class="col-md-5 col-md-offset-1">
                    <p class="project-menu-title">เลือกคอนโดมิเนียมบนทำเลที่สนใจ</p>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="project-menu-subtitle">กรุงเทพและปริมณฑล</p>
                            <ul>
                                <?php
                                if($zone_condos != false){
                                    foreach ($zone_condos as $zone_home){
                                        ?>
                                        <li><a href="<?php url('/project-type-condominium.php?zone_id='.$zone_home->zone_id) ?>"><?= $zone_home->zone_name ?></a></li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <?php
                        if($zone_condo_countrys != false) {
                            ?>
                            <div class="col-md-6">
                                <p class="project-menu-subtitle">ต่างจังหวัด</p>
                                <ul>
                                    <?php
                                    if ($zone_condo_countrys != false) {
                                        foreach ($zone_condo_countrys as $zone_home) {
                                            ?>
                                            <li>
                                                <a href="<?php url('/project-type-condominium.php?zone_id=' . $zone_home->zone_id) ?>"><?= $zone_home->zone_name ?></a>
                                            </li>
                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="menu-col-right condo">
                <div class="col-md-6">
                    <p class="project-menu-title">เลือกคอนโดมิเนียมในแบรนด์ที่สนใจ</p>
                    <?php
                    $logo_condo1 = $logo_condos[0];
                    $logo_condo2 = $logo_condos[1];
                    $logo_condo3 = $logo_condos[2];
                    $logo_condo4 = $logo_condos[3];
                    $logo_condo5 = $logo_condos[4];
                    $logo_condo6 = $logo_condos[5];
                    ?>
                    <ul class="project-menu-logo">
                        <li><a href="<?php if(is_object($logo_condo1)) url('/search-by-brand.php?brand_id='.$logo_condo1->brand_id.'&product_id=3'); ?>"><img src="<?= is_object($logo_condo1) ? backend_url('logo',$logo_condo1->logo_brand_th) : '' ?>" alt=""></a></li>
                        <li><a href="<?php if(is_object($logo_condo2)) url('/search-by-brand.php?brand_id='.$logo_condo2->brand_id.'&product_id=3'); ?>" style="width: 100px;"><img src="<?= is_object($logo_condo2) ? backend_url('logo',$logo_condo2->logo_brand_th) : '' ?>" alt=""></a></li>
                    </ul>
                    <ul class="project-menu-logo m-line-2">
                        <li><a href="<?php if(is_object($logo_condo3)) url('/search-by-brand.php?brand_id='.$logo_condo3->brand_id.'&product_id=3'); ?>"><img src="<?= is_object($logo_condo3) ? backend_url('logo',$logo_condo3->logo_brand_th) : '' ?>" alt=""></a></li>
                        <li><a href="<?php if(is_object($logo_condo4)) url('/search-by-brand.php?brand_id='.$logo_condo4->brand_id.'&product_id=3'); ?>"><img src="<?= is_object($logo_condo4) ? backend_url('logo',$logo_condo4->logo_brand_th) : '' ?>" alt=""></a></li>
                        <li class="l-inizio"><a href="<?php if(is_object($logo_condo5)) url('/search-by-brand.php?brand_id='.$logo_condo5->brand_id.'&product_id=3'); ?>"><img src="<?= is_object($logo_condo5) ? backend_url('logo',$logo_condo5->logo_brand_th) : '' ?>" alt=""></a></li>
                        <li class="l-inizio"><a href="<?php if(is_object($logo_condo6)) url('/search-by-brand.php?brand_id='.$logo_condo6->brand_id.'&product_id=3'); ?>"><img src="<?= is_object($logo_condo6) ? backend_url('logo',$logo_condo6->logo_brand_th) : '' ?>" alt=""></a></li>
                    </ul>
                    <a href="<?php url('/project-type-condominium.php') ?>" class="project-menu-title">ดูคอนโดมิเนียมทั้งหมด ></a>
                </div>
            </div>
        </div>
    </div>
</div>

<!--home sell-->
<div class="menu-home-series homesell">
    <div class="menu-home-series-close">CLOSE</div>
    <div class="container">
        <div class="row">
            <div class="menu-col-left homesell">
                <div class="col-md-5 col-md-offset-1">
                    <p class="project-menu-title">เลือกบ้านตกแต่งพร้อมขายบนทำเลที่สนใจ</p>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="project-menu-subtitle">กรุงเทพและปริมณฑล</p>
                            <ul>
                                <?php
                                if($zone_homesells != false){
                                    foreach ($zone_homesells as $zone_home){
                                        ?>
                                        <li><a href="<?php url('/furnished-home.php?zone_id='.$zone_home->zone_id) ?>"><?= $zone_home->zone_name ?></a></li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <p class="project-menu-subtitle">ต่างจังหวัด</p>
                            <ul>
                                <?php
                                if($zone_homesell_countrys != false){
                                    foreach ($zone_homesell_countrys as $zone_home){
                                        ?>
                                        <li><a href="<?php url('/furnished-home.php?zone_id='.$zone_home->zone_id) ?>"><?= $zone_home->zone_name ?></a></li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="menu-col-right homesell">
                <div class="col-md-6">
                    <p class="project-menu-title">เลือกบ้านตกแต่งพร้อมขายในแบรนด์ที่สนใจ</p>
                    <?php
                    $logo_homesell1 = $logo_homesells[0];
                    $logo_homesell2 = $logo_homesells[1];
                    $logo_homesell3 = $logo_homesells[2];
                    $logo_homesell4 = $logo_homesells[3];
                    $logo_homesell5 = $logo_homesells[4];
                    $logo_homesell6 = $logo_homesells[5];
                    ?>
                    <ul class="project-menu-logo">
                        <li><a href="<?php if(is_object($logo_homesell1)) url('/search-by-brand.php?brand_id='.$logo_homesell1->brand_id.'&product_id=4'); ?>"><img src="<?= is_object($logo_homesell1) ? backend_url('logo',$logo_homesell1->logo_brand_th) : '' ?>" alt=""></a></li>
                        <li><a href="<?php if(is_object($logo_homesell2)) url('/search-by-brand.php?brand_id='.$logo_homesell2->brand_id.'&product_id=4'); ?>" style="width: 100px;"><img src="<?= is_object($logo_homesell2) ? backend_url('logo',$logo_homesell2->logo_brand_th) : '' ?>" alt=""></a></li>
                    </ul>
                    <ul class="project-menu-logo m-line-2">
                        <li><a href="<?php if(is_object($logo_homesell3)) url('/search-by-brand.php?brand_id='.$logo_homesell3->brand_id.'&product_id=4'); ?>"><img src="<?= is_object($logo_homesell3) ? backend_url('logo',$logo_homesell3->logo_brand_th) : '' ?>" alt=""></a></li>
                        <li><a href="<?php if(is_object($logo_homesell4)) url('/search-by-brand.php?brand_id='.$logo_homesell4->brand_id.'&product_id=4'); ?>"><img src="<?= is_object($logo_homesell4) ? backend_url('logo',$logo_homesell4->logo_brand_th) : '' ?>" alt=""></a></li>
                        <li class="l-inizio"><a href="<?php if(is_object($logo_homesell5)) url('/search-by-brand.php?brand_id='.$logo_homesell5->brand_id.'&product_id=4'); ?>"><img src="<?= is_object($logo_homesell5) ? backend_url('logo',$logo_homesell5->logo_brand_th) : '' ?>" alt=""></a></li>
                        <li class="l-inizio"><a href="<?php if(is_object($logo_homesell6)) url('/search-by-brand.php?brand_id='.$logo_homesell6->brand_id.'&product_id=4'); ?>"><img src="<?= is_object($logo_homesell6) ? backend_url('logo',$logo_homesell6->logo_brand_th) : '' ?>" alt=""></a></li>
                    </ul>
                    <a href="<?php url('/furnished-home.php') ?>" class="project-menu-title">ดูบ้านตกแต่งพร้อมขายทั้งหมด ></a>
                </div>
            </div>
        </div>
    </div>
</div>