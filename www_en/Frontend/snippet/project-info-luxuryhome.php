<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

$project_id = $_GET['project_id'];
$product_id = $_GET['product_id'];

if (empty($project_id) || getProjectByID($project_id) == false) {
    header('Location: '.Helper::url_string('project-type-luxury.php'));
}

$lang = !empty($_GET['lang']) ? $_GET['lang'] : 'th';

?>
<?php include('header.php'); ?>
    <!-- CSS -->
    <link rel="stylesheet" href="css/project-info-luxuryhome.css" type="text/css">
    <link rel="stylesheet" href="bxslider/jquery.bxslider.css" type="text/css">
    <link rel="stylesheet" href="css/video.css" type="text/css">
    <!-- JS -->
    <?php
    $project            = getProjectByID($project_id);
    if(!empty($project->latitude) && !empty($project->longtitude) && $project->project_status != 'SO'){
        ?>
        <script>
            function initMap() {
                // Create a map object and specify the DOM element for display.
                $(document).ready(function () {
                    var lat_proj = parseFloat($('#lat').val());
                    var lng_proj = parseFloat($('#lng').val());
                    var pos = {lat: lat_proj, lng: lng_proj};
                    var map = new google.maps.Map(document.getElementById('map'), {
                        center: pos,
                        scrollwheel: false,
                        zoom: 15,
                        zoomControlOptions: {
                            position: google.maps.ControlPosition.TOP_RIGHT
                        },
                        streetViewControl: false,
                        mapTypeControl: false,
                        styles: [{"elementType": "geometry", "stylers": [{"color": "#f5f5f5"}]}, {
                            "elementType": "labels.icon",
                            "stylers": [{"visibility": "off"}]
                        }, {
                            "elementType": "labels.text.fill",
                            "stylers": [{"color": "#616161"}]
                        }, {
                            "elementType": "labels.text.stroke",
                            "stylers": [{"color": "#f5f5f5"}]
                        }, {
                            "featureType": "administrative.land_parcel",
                            "elementType": "labels.text.fill",
                            "stylers": [{"color": "#bdbdbd"}]
                        }, {
                            "featureType": "administrative.locality",
                            "elementType": "labels.text.fill",
                            "stylers": [{"color": "#c19838"}]
                        }, {
                            "featureType": "administrative.neighborhood",
                            "elementType": "labels.text.fill",
                            "stylers": [{"color": "#c19838"}]
                        }, {
                            "featureType": "poi",
                            "elementType": "geometry",
                            "stylers": [{"color": "#eeeeee"}]
                        }, {
                            "featureType": "poi",
                            "elementType": "labels.text.fill",
                            "stylers": [{"color": "#757575"}]
                        }, {
                            "featureType": "poi.park",
                            "elementType": "geometry",
                            "stylers": [{"color": "#e5e5e5"}]
                        }, {
                            "featureType": "poi.park",
                            "elementType": "labels.text.fill",
                            "stylers": [{"color": "#9e9e9e"}]
                        }, {
                            "featureType": "road",
                            "elementType": "geometry",
                            "stylers": [{"color": "#ffffff"}]
                        }, {
                            "featureType": "road",
                            "elementType": "labels.icon",
                            "stylers": [{"visibility": "simplified"}]
                        }, {
                            "featureType": "road",
                            "elementType": "labels.text",
                            "stylers": [{"color": "#878787"}]
                        }, {
                            "featureType": "road",
                            "elementType": "labels.text.fill",
                            "stylers": [{"color": "#9f720a"}, {"visibility": "off"}]
                        }, {
                            "featureType": "road.arterial",
                            "elementType": "labels.text.fill",
                            "stylers": [{"color": "#757575"}]
                        }, {
                            "featureType": "road.highway",
                            "elementType": "geometry",
                            "stylers": [{"color": "#dadada"}]
                        }, {
                            "featureType": "road.highway",
                            "elementType": "labels.text.fill",
                            "stylers": [{"color": "#616161"}]
                        }, {
                            "featureType": "transit.line",
                            "elementType": "geometry",
                            "stylers": [{"color": "#e5e5e5"}]
                        }, {
                            "featureType": "transit.station",
                            "elementType": "geometry",
                            "stylers": [{"color": "#eeeeee"}]
                        }, {
                            "featureType": "water",
                            "elementType": "geometry",
                            "stylers": [{"color": "#c9c9c9"}]
                        }, {"featureType": "water", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]}]
                    });
                    var icon = {
                        url: 'images/global/icon_google_map_db.png',
                        scaledSize: new google.maps.Size(30, 42), // scaled size
                        origin: new google.maps.Point(0, 0), // origin
                        anchor: new google.maps.Point(0, 0) // anchor
                    };
                    var marker = new google.maps.Marker({
                        position: pos,
                        map: map,
                        icon: icon
                    });
                    google.maps.event.addListener(marker, 'click', function() {
                        window.open('https://maps.google.com?saddr=Current+Location&daddr='+lat_proj+','+lng_proj,'_blank');
                    });
                });
            }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC0Itod-fczPlmFpGySrnvOJ15khTzQTd4&callback=initMap"
                async defer></script>

        <?php
    }
    ?>

    <script src="bxslider/jquery.bxslider.min.js"></script>
    <script src="js/project-info-home.js"></script>


    <div class="page-banner page-scroll slider">
        <div id="pjHomeBanner" class="owl-carousel owl-theme" style="position: relative!important;top:0!important;">

            <?php
            $project            = getProjectByID($project_id);
            $logo               = getProjectMaps($project_id);
            $project_concept    = getProjectConcept($project_id);

            $banner_projects    = getAllBannerByProjectID($project_id);
            $banner_homes       = getBannerByBannerProjectID($project_id);

            foreach ($banner_projects as $i => $banner_project) {
                    ?>

                    <?php
                    if ($banner_project->LEAD_IMAGE_PROJECT_FILE_TYPE == 'image') {
                        ?>
                        <div class="item slide">
                            <div class="banner-container banner-parallax">
                                <img class="<?= $i == 0 ? 'background-show' : '' ?>" src="<?= backend_url('base', $banner_project->LEAD_IMAGE_PROJECT_FILE_NAME) ?>" alt="">
                            </div>
                            <?php if($i == 0) {
                                ?>
                               <!--  <div class="banner-logo">
                                    <div class="bg-slide-home" style="margin-top: 8%">
                                        <img id="img_logo" src="<?= is_object($logo) ? backend_url('base', $logo->project_logo) : '' ?>" class="background-logo"
                                             style="margin: 0 auto;width: 180px;"/>
                                        <div id="desc_logo" style="margin: 0 auto;text-align: center;">
                                            <h1 style="col"><?= !empty($project->project_name_en) ? $project->project_name_en : '' ?></h1>
                                            <p class="p-in-banner"><?= !empty($project_concept->concept_th) ? $project_concept->concept_th : '' ?></p>
                                        </div>
                                    </div>
                                </div> -->
                                <?php
                            }?>
                        </div>
                        <?php
                    } elseif ($banner_project->LEAD_IMAGE_PROJECT_FILE_TYPE == 'youtube') {
                        $lead_img = backend_url('base', $banner_project->banner_img_thum);
                        $lead_youtube_url = str_replace('watch?v=', 'embed/', $banner_project->LEAD_IMAGE_PROJECT_FILE_NAME);
                        ?>
                        <div class="item slide">
                            <div class="item-video">
                                <div class="banner-container banner-parallax" id="bannerVideo<?= $i ?>">
                                    <div class="rps-vdobg">
                                        <img src="<?= $lead_img ?>" alt="">
                                    </div>

                                    <div class="vdo-control">
                                        <div class="play-block">
                                            <span id="vdoPlay<?= $i ?>" class="vdo-icon vdoControl vdoPlay"></span>
                                        </div>
                                    </div>

                                    <script type="text/javascript">
                                        var iframe_src = '<?= $lead_youtube_url ?>';
                                        var youtube_video_id = iframe_src.match(/youtube\.com.*(\?v=|\/embed\/)(.{11})/).pop();
                                        if (youtube_video_id.length == 11) {
                                            $('#vdoPlay<?=$i?>').click(function () {
                                                $("#slideVdo<?=$i?>").remove();
                                                var video_iframe = $('' +
                                                    '<iframe id="video" width="100%" height="720px"' +
                                                    ' src="<?= $lead_youtube_url . "?autoplay=0" ?>">' +
                                                    '</iframe>');
                                                $('#bannerVideo<?=$i?>').find('.vdo-control').remove();
                                                $('.rps-vdobg').remove();
                                                $('#bannerVideo<?=$i?>').append(video_iframe);
                                                $(this).trigger('stop.autoplay.owl');
                                                $('#scrollNext').hide();
                                            });
                                        }
                                    </script>
                                </div>
                            </div>

                        </div>

                        <?php
                    } elseif ($banner_project->LEAD_IMAGE_PROJECT_FILE_TYPE == 'vdo') {
                        ?>
                        <div class="item slide">
                            <script>
                                $(document).ready(function () {
                                    $('#vdoPlay<?= $i ?>').click(function () {
                                        $('#slideVdo<?= $i ?>').css('display', 'block');
                                        $('#slideVdo<?= $i ?>')[0].play();
                                        $(this).fadeOut(200);
                                        $('#vdoPause<?= $i ?>').fadeIn(200);
                                        $('.vid_bg').remove();

                                        $(".vdo-control").mouseenter(function (event) {
                                            event.stopPropagation();
                                            $('#vdoPause<?= $i ?>').addClass("btnshown");
                                        }).mouseleave(function (event) {
                                            event.stopPropagation();
                                            $('#vdoPause<?= $i ?>').removeClass("btnshown");
                                        });
                                        $('#scrollNext').hide();
                                    });

                                    $("#vdoPause<?= $i ?>").click(function () {
                                        $('#slideVdo<?= $i ?>').get(0).pause();
                                        $(this).fadeOut(200);
                                        $('#vdoPlay<?= $i ?>').fadeIn(200);
                                        $('#scrollNext').show();
                                    });
                                });
                            </script>
                            <div class="banner-container banner-parallax">
                                <img class="vid_bg" src="<?= backend_url('base', $banner_project->banner_img_thum) ?>" alt="">
                                <video loop="loop" id="slideVdo<?= $i ?>" class="" style="display:none;">
                                    <source src="<?= backend_url('base', $banner_project->LEAD_IMAGE_PROJECT_FILE_NAME) ?>" type="video/mp4">
                                </video>

                                <div class="vdo-control">
                                    <div class="play-block">
                                        <span id="vdoPlay<?= $i ?>" class="vdo-icon vdoControl vdoPlay"></span>
                                        <span id="vdoPause<?= $i ?>" class="vdo-icon vdoControl vdoPause"></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php
                    } elseif ($banner_project->LEAD_IMAGE_PROJECT_FILE_TYPE == 'banner') {
                        ?>
                        <div class="item slide" <?php if (!empty($banner_project->lead_img_url)) echo "onclick=\"location.href='" . $banner_project->lead_img_url . "'\""; ?>>
                            <div class="banner-container banner-parallax">
                                <img class="background-show" src="<?= backend_url('base',$banner_project->LEAD_IMAGE_PROJECT_FILE_NAME)?>" alt="">

                                <?php
                                if(!empty($banner_project->banner_text)){
                                    ?>
                                    <a <?= !empty($banner_project->banner_link) ? 'href="'.$banner_project->banner_link.'"' : '' ?>
                                            class="banner-activity"
                                        <?php if(!empty($banner_project->banner_backgroup)) {
                                            ?>
                                            style="background-color: <?= $banner_project->banner_backgroup ?>;opacity: 0.70;"
                                            <?php
                                        }?>
                                    >
                                        <?= $banner_project->banner_text ?>
                                        <?php if(!empty($banner_project->banner_link)) {
                                            ?>
                                            <span class="link-acty"></span>
                                            <?php
                                        }?>
                                    </a>
                                    <?php
                                }
                                ?>

                            </div>
                            <?php if($i == 0) {
                                ?>
                                <!-- <div class="banner-logo">
                                    <div class="bg-slide-home" style="margin-top: 8%">
                                        <img id="img_logo" src="<?= is_object($logo) ? backend_url('base', $logo->project_logo) : '' ?>" class="background-logo"
                                             style="margin: 0 auto;width: 180px;"/>
                                        <div id="desc_logo" style="margin: 0 auto;text-align: center;">
                                            <h1 style="col"><?= !empty($project->project_name_en) ? $project->project_name_en : '' ?></h1>
                                            <p class="p-in-banner"><?= !empty($project_concept->concept_th) ? $project_concept->concept_th : '' ?></p>
                                        </div>
                                    </div>
                                </div> -->
                                <?php
                            }?>
                        </div>
                        <?php
                    }
                    ?>

                    <?php
                }

            ?>

        </div>
        <span id="scrollNext" class="i-scroll-next"></span>
    </div>

    <?php
    $banner_homes = !empty($banner_homes) ? $banner_homes : $banner_projects;
    if(!empty($banner_homes)){
        foreach ($banner_homes as $i => $banner){
            if(!empty($banner->banner_text)) {
                ?>
                <a <?= !empty($banner->banner_link) ? 'href="'.$banner->banner_link.'"' : '' ?>
                        class="banner-activity banner-rsp"
                    <?php if(!empty($banner->banner_backgroup)) {
                        ?>
                        style="background-color: <?= $banner->banner_backgroup ?>;opacity: 0.70;"
                        <?php
                    }?>
                >
                    <?= $banner->banner_text ?>
                    <?php if(!empty($banner->banner_link)) {
                        ?>
                        <span class="link-acty"></span>
                        <?php
                    }?>
                    <span class="close-acty"></span>
                </a>
                <?php
                break;
            }
        }
    }
    ?>

<?php
$projectMap = getProjectMaps($project_id);
$project = getProjectByID($project_id);
$project_price = getProjectPrice($project_id);
$project_concept = getProjectConcept($project_id);
$project_promotion = getProjectPromotion($project_id);
$brand = getBrandByID($project->brand_id);
$project_areas = getProjectAreaByProjectID($project_id);
$logo          = getProjectMaps($project_id);
?>
    <div id="sec-information" class="content project-info-page">

        <div class="project-info-container">
            <div class="container">

                <div class="row">
                    <div class="info-logo-img">
                        <img src="<?= is_object($logo) ? backend_url('base', $logo->project_logo) : '' ?>" alt="" >
                    </div>
                </div>

                <?php if($project->project_status == 'SO') {
                    ?>
                    <div class="tagbox2_sold soldout-tag">
                        <div class="tag2">sold<br>out</div>
                    </div>
                    <?php
                }?>

                <?php if($project->project_status != 'SO'){ ?>

                    <div class="row mb15">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <p class="subtitle green">ข้อมูลโครงการ</p>
                        </div>
                        <div class="col-md-6">
                            <p><b><span class="line-space "><?php if($project != false) echo $project->project_name_th ?></span></b><br>
                                <?php if(!empty($project->area_farm)) { ?>
                                    <span class="line-space">พื้นที่โครงการ
                                        <?php if(!empty($project->area_farm))     echo $project->area_farm.' ไร่' ?>
                                        <?php if(!empty($project->area_square_m)) echo $project->area_square_m.' งาน' ?>
                                        <?php if(!empty($project->area_square_w)) echo $project->area_square_w.' ตารางวา' ?>
                                        </span><br>
                                <?php } ?>
                                <?php if(!empty($project_concept->area_land)) { ?>
                                    <span class="line-space">จำนวนแปลงขาย <?= $project_concept->area_land ?> แปลง</span><br>
                                <?php } ?>
                                <span class="line-space">ประเภทที่อยู่อาศัยในโครงการ</span><br>

                                <?php foreach ($project_areas as $project_area){
                                    $product_name       = Helper::getProductNameTHByType($project_area->product_id);
                                    $num_plan_home      = str_replace(' ','',$project_area->num_plan_home);
                                    $area_home          = str_replace(' ','',$project_area->area_home);
                                    $area_square_w_home = str_replace(' ','',$project_area->area_square_w_home);
                                    ?>


                                    <span class="line-space">ประเภท <?= $product_name ?>
                                        <?php if(!empty($project_area->num_plan_home)) {
                                            ?>
                                            มีแบบบ้านให้เลือกทั้งหมด <?= $num_plan_home ?> แบบ
                                            <?php
                                        }?>
                                        </span><br>

                                    <?php if(!empty($area_home) || !empty($area_square_w_home)) { ?>
                                        <span class="line-space">
                                            <?php if(!empty($area_home)) {
                                                ?>
                                                พื้นที่ใช้สอยตั้งแต่ <?= $area_home ?> ตร.ม.
                                                <?php
                                            }?>
                                            <?php if(!empty($product_name)) {
                                                ?>
                                                บนขนาดที่ดินตั้งแต่ <?= $area_square_w_home ?> ตารางวา
                                                <?php
                                            }?>
                                        </span><br>
                                    <?php } ?>
                                    <?php
                                }
                                ?>
                            </p>
                            <?php if(!empty($project_price->project_price) || !empty($project_price->project_price)) { ?>
                                <p class="subtitle">ราคาเริ่มต้น <?php if($project_price != false) echo Helper::getProjectPrice($project_price->project_price,$project_price->price_mode) ?> ล้านบาท</p>
                            <?php } ?>
                        </div>
                        <div class="col-md-2"></div>
                    </div>

                    <?php if (!empty($project_concept->concept_en) || !empty($project_concept->concept_th)) { ?>
                        <div class="row mb15">
                            <div class="col-md-2"></div>
                            <div class="col-md-2">
                                <p class="subtitle green">แนวคิดโครงการ</p>
                            </div>
                            <div class="col-md-6">

                                <?php if(!empty($project_concept->concept_th)) { ?>
                                    <p class="subtitle distinctive1">
                                        <?php if($project_concept != false)  {
                                            ?>
                                             <?=!empty($project_concept->concept_th) ? str_replace(array("\n\r", "\n","n/g"), '<br/>', $project_concept->concept_th) : ''?>
                                           
                                            <?php
                                        }?>
                                    </p>
                                <?php } ?>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                    <?php } ?>

                    <?php if (!empty($project_concept->distinctive_th)) { ?>
                        <div class="row mb15">
                            <div class="col-md-2"></div>
                            <div class="col-md-2">
                                <p class="subtitle green">จุดเด่นโครงการ</p>
                            </div>
                            <div class="col-md-6 distinctive2" >
                                <ol>
                                    <p><?php if($project_concept != false) ?>
                                     <?=!empty($project_concept->distinctive_th) ? str_replace(array("\n\r", "\n","n/g"), '<br/>', $project_concept->distinctive_th) : ''?>
                                      

                                    </p>
                                </ol>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                    <?php } ?>

                    <?php if (!empty($project_concept->facilities_th)) { ?>
                        <div class="row mb15">
                            <div class="col-md-2"></div>
                            <div class="col-md-2">
                                <p class="subtitle green">สิ่งอำนวยความสะดวก</p>
                            </div>
                            <div class="col-md-6">
                                <p class="distinctive3"><?php if($project_concept != false)  ?>
                                 <?=!empty($project_concept->facilities_th) ? str_replace(array("\n\r", "\n","n/g"), '<br/>', $project_concept->facilities_th) : ''?>
                                  
                                </p>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                    <?php } ?>

                    <?php if (!empty($project_concept->security_th)) { ?>
                        <div class="row mb15">
                            <div class="col-md-2"></div>
                            <div class="col-md-2">
                                <p class="subtitle green">ระบบรักษาความปลอดภัย</p>
                            </div>
                            <div class="col-md-6">
                                <p class="distinctive4"><?php if($project_concept != false)  ?>
                                 <?=!empty($project_concept->security_th) ? str_replace(array("\n\r", "\n","n/g"), '<br/>', $project_concept->security_th) : ''?>
                                   
                                </p>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                    <?php } ?>

                    <?php
                    if($projectMap != false && !empty($projectMap->brochure)){
                        ?>
                        <div class="row mb15">
                            <div class="col-md-2"></div>
                            <div class="col-md-2">
                                <p class="subtitle green">ดาวน์โหลดโบรชัวร์</p>
                            </div>
                            <div class="col-md-6">
                                <a href="<?= backend_url('base',$projectMap->brochure) ?>"
                                   target="_blank"
                                   class="dl-brochure">
                                    <i class="i-download" style="background: url(images/global/i_download_LW.png) no-repeat;"></i>
                                    <p>
                                        <?php if($project != false) echo $project->project_name_th ?>
                                    </p>
                                </a>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <?php
                    }
                    ?>

                <?php } ?>

            </div>
        </div>

        <div class="container">

            <?php if ($project_promotion != false && $project->project_status != 'SO') {
            $img_promotion = getImagePromotion($project_promotion->promotion_id);
            ?>
            <div id="sec-promotion" class="block-margin">
                <p class="heading-title">โปรโมชั่น</p>
                <div class="row">
                    <div id="projectPromoSlide">
                        <div class="item">
                            <div class="">
                                <div class="col-md-8">
                                    <div class="review-img">
                                        <img src="<?php if ($img_promotion != false) echo backend_url('base', $img_promotion->img_promotion_name) ?>"
                                             alt="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="promo-descrp">
                                        <p class="title"><?= $project_promotion->promotion_name_th ?></p>
                                        <p><?= $project_promotion->promotion_detail_th ?></p>
                                        <?php
                                        if(!empty($project_promotion->promotion_url)){
                                            $promotion_url = $project_promotion->promotion_url;
                                            $promotion_url = strpos($promotion_url, 'https://') !== false || strpos($promotion_url, 'http://') !== false ? $promotion_url : "http://$promotion_url";
                                            ?>
                                            <a href="<?= $promotion_url ?>" class="btn btn-seemore" style="text-align:center">
                                                See more
                                            </a>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <?php }

                $homeSells = getHomeSell($project_id, $product_id);
                if ($homeSells != false && $project->project_status != 'SO') {

                    ?>

                    <div id="sec-home-sale" class="block-margin">
                        <p class="heading-title">บ้านพร้อมขาย</p>

                        <?php

                        //first item
                        $planImg = getGalleryPlanByPlanID($homeSells[0]->plan_id);
                        $lhPlan = getPlanByPlanID($homeSells[0]->plan_id);
                        $anotherImgs = getHomeSellImgByHomeSellID($homeSells[0]->home_sell_id);
                        $conditionHomeSell = getConditionHomeSellByHomeSellID($homeSells[0]->home_sell_id);

                        ?>
                        <div class="row">
                            <div id="projectModelSlide">
                                <div class="col-md-8">
                                    <div class="project-model-block">
                                        <div id="tag_sold">
                                            <?php if($homeSells[0]->home_sell_status == 'SO') {
                                                ?>
                                                <div class="tagbox2_sold soldout-tag">
                                                    <div class="tag2">sold<br>out</div>
                                                </div>
                                                <?php
                                            }?>
                                        </div>
                                        <div id="projectSlide" class="owl-carousel owl-theme">
                                            <div class="gal-img item"><img
                                                        src="<?php if ($planImg != false) echo backend_url('plan_img', $planImg->galery_plan_name) ?>"/>
                                            </div>
                                            <?php
                                            foreach ($anotherImgs as $anotherImg) {
                                                ?>

                                                <div class="gal-img item"><img
                                                            src="<?= backend_url('base', $anotherImg->home_sell_img_name) ?>"/>
                                                </div>

                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>

                                    <div id="imgThumb" class="slide-thumb owl-thumbs owl-carousel owl-theme">
                                        <a class="item"><img
                                                    src="<?php if ($planImg != false) echo backend_url('plan_img', $planImg->galery_plan_name) ?>"
                                                    alt=""></a>
                                        <?php
                                        foreach ($anotherImgs as $anotherImg) {
                                            ?>
                                            <a class="item"><img
                                                        src="<?= backend_url('base', $anotherImg->home_sell_img_name) ?>"
                                                        alt=""></a>
                                            <?php
                                        }
                                        ?>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="promo-descrp" id="desc_home">
                                        <p class="title green">แบบบ้าน <?= $lhPlan->plan_name_th ?></p>
                                        <p class="title dsktp">หมายเลขแปลง <?= $lhPlan->plan_code ?></p>
                                        <?php  ?>
                                        <p class="title dsktp">ลักษณะแปลงที่ดิน</p>
                                        <p><?= $homeSells[0]->features_convert ?></p>
                                        <p class="title green">
                                            ราคา <?= Helper::getProjectPrice($conditionHomeSell->total_price) ?>
                                            ล้านบาท</p>
                                        <a href="<?php url('/project-info-sale.php?home_sell_id=' . $homeSells[0]->home_sell_id); ?>"
                                           class="btn btn-seemore">See more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="other-home-sec" class="other-home-sec">
                        <p class="heading-title">บ้านพร้อมขายทั้งหมด <span id="index_plan"><span id="index_homesell"
                                                                                                 style="float: left;">1</span>/<?= count($homeSells) ?>
                                แปลง</span></p>
                        <input type="hidden" id="amount_value" value="<?= count($homeSells) ?>">
                        <div class="row">
                            <div id="otherHome" class="col-slide">

                                <?php
                                foreach ($homeSells as $i => $homeSell) {

                                    $subPlanImg = getGalleryPlanByPlanID($homeSell->plan_id);
                                    $lhSubPlan = getPlanByPlanID($homeSell->plan_id);
                                    $anotherSubImgs = getHomeSellImgByHomeSellID($homeSell->home_sell_id);
                                    $conditionSubHomeSell = getConditionHomeSellByHomeSellID($homeSell->home_sell_id);

                                    ?>

                                    <div class="item">
                                        <div class="">
                                            <div class="other-home-img">
                                                <img src="<?= backend_url('plan_img', $subPlanImg->galery_plan_name) ?>"
                                                     alt="">
                                                <!--                                    <img src="-->
                                                <?php //if($planImg != false) echo base_url('plan_img',$planImg->galery_plan_name)
                                                ?><!--" alt="">-->
                                            </div>
                                            <a>
                                                <p class="title">แบบบ้าน <?= $lhSubPlan->plan_name_th ?></p>
                                                <p class="gray mb0">หมายเลขแปลง <?= $lhSubPlan->plan_code ?></p>
                                                <p class="title green">
                                                    <b>ราคา <?= Helper::getProjectPrice($conditionSubHomeSell->total_price) ?>
                                                        ล้านบาท</b></p>
                                                <div id="data-slider" style="display: none">
                                                    <input type="hidden" id="home_sell_status" value="<?= $homeSell->home_sell_status ?>">
                                                    <input type="hidden" id="index_value" value="<?= $i + 1 ?>">
                                                    <input type="hidden" id="title_home"
                                                           value="<?= $lhSubPlan->plan_name_th ?>">
                                                    <input type="hidden" id="format_home"
                                                           value="<?= $lhSubPlan->plan_name_th ?>">
                                                    <input type="hidden" id="id_home" value="<?= $lhSubPlan->plan_code ?>">
                                                    <input type="hidden" id="identity_home"
                                                           value="<?= $homeSell->features_convert ?>">
                                                    <input type="hidden" id="price_home"
                                                           value="<?= Helper::getProjectPrice($conditionSubHomeSell->total_price) ?>  ล้านบาท">
                                                    <input type="hidden" id="home_sell_id"
                                                           value="<?php url('/project-info-sale.php?home_sell_id=' . $homeSell->home_sell_id); ?>">
                                                    <div id="img_list">
                                                        <input type="hidden" class="img_url"
                                                               value="<?= backend_url('plan_img', $subPlanImg->galery_plan_name) ?>">
                                                        <?php
                                                        foreach ($anotherSubImgs as $anotherSubImg) {
                                                            ?>
                                                            <input type="hidden" class="img_url"
                                                                   value="<?= backend_url('base', $anotherSubImg->home_sell_img_name) ?>">
                                                            <?php
                                                        }
                                                        ?>

                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <?php
                                } ?>

                            </div>
                        </div>
                    </div>

                <?php } ?>

                <?php
                $galleryImgProjects = getGalleryImgProject($project_id);
                if($galleryImgProjects != false && $project->project_status != 'SO'){
                    ?>
                    <div id="sec-gallery" class="gallery-block block-margin">
                        <p class="heading-title">แกลอรี</p>
                        <div class="tpl5-img">
                            <div id="tpl5-img">
                                <?php
                                $i = 1;
                                foreach ($galleryImgProjects as $galleryImgProject) {
                                    ?>
                                    <div class="item tpl5-img-<?= $i ?>">
                                        <a class="gallery"
                                           href="<?= backend_url('base', $galleryImgProject->galery_project_img_name) ?>">
                                            <img src="<?= backend_url('base', $galleryImgProject->galery_project_img_name) ?>"
                                                 alt="">
                                            <p class="title-gallery"><?= $galleryImgProject->galery_project_img_desc ?></p>
                                        </a>
                                    </div>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>

                <?php if($project->project_status == 'SO'){

                    $projectNearByZones = getProjectRelate($project_id,$project->zone_id,$product_id);
                    if($projectNearByZones != false){
                        ?>
                        <div class="project-info-detail-block">
                            <p class="heading-title">โครงการอื่นๆ ที่น่าสนใจ</p>
                            <div class="row">
                                <div id="colSlide" class="col-slide">
                                    <?php

                                    foreach ($projectNearByZones as $projectNearByZone) {
                                        $project_near_id = $projectNearByZone->project_id;
                                        $project_relate = getProjectByID($project_near_id);
                                        $banner  = getBannerByProjectID($project_near_id);
                                        $project_concept = getProjectConcept($project_near_id);
                                        $project_price = getProjectPrice($project_near_id);
                                        ?>
                                        <div class="item">
                                            <div class="project-onsale">
                                                <div class="project-info-other-img">
                                                    <?php
                                                    if($banner != false && ( $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'image' || $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'banner' ) ){
                                                        ?>
                                                    <img src="<?= backend_url('base',$banner->LEAD_IMAGE_PROJECT_FILE_NAME)?>"
                                                         alt="<?= $banner->LEAD_IMAGE_PROJECT_FILE_SEO ?>">
                                                    <?php
                                                    }elseif($banner != false && $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'youtube'){
                                                    $lead_img = backend_url('base', $banner->banner_img_thum);
                                                    $lead_youtube_url = str_replace('watch?v=', 'embed/', $banner->LEAD_IMAGE_PROJECT_FILE_NAME);
                                                    ?>

                                                        <a href="#" data-featherlight="#lbVdo<?= $i ?>">
                                                            <span class="i-view-vdo"></span>
                                                            <img src="<?= $lead_img ?>"
                                                                 alt="<?= $banner->LEAD_IMAGE_PROJECT_FILE_SEO ?>" class="hm-highl">
                                                        </a>
                                                        <iframe class="lightbox" src="<?= $lead_youtube_url . "?autoplay=0" ?>"
                                                                width="1000"
                                                                height="560" id="lbVdo<?= $i ?>" style="border:none;" webkitallowfullscreen
                                                                mozallowfullscreen allowfullscreen></iframe>

                                                    <?php
                                                    } elseif ($banner != false && $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'vdo') {
                                                    ?>
                                                        <a id="ban_vdo<?= $i ?>">
                                                            <span class="i-view-vdo"></span>
                                                            <img src="<?= backend_url('base', $banner->banner_img_thum) ?>"
                                                                 alt="<?= $banner->LEAD_IMAGE_PROJECT_FILE_SEO ?>" class="hm-highl">
                                                        </a>
                                                        <script>
                                                            $(document).ready(function () {
                                                                $('#ban_vdo<?= $i ?>').click(function () {
                                                                    $('#lbVdo<?= $i ?>').show();
                                                                    $.featherlight($('#lbVdo<?= $i ?>'),{});
                                                                    $('.featherlight-content #video_player<?= $i ?>')[0].play();
                                                                    $('#lbVdo<?= $i ?>').hide();
                                                                });
                                                            })
                                                        </script>
                                                        <div id="lbVdo<?= $i ?>" style="position:relative;width: 100%;display: none;">
                                                            <video id='video_player<?= $i ?>' preload='none' controls>
                                                                <source src="<?= backend_url('base',$banner->LEAD_IMAGE_PROJECT_FILE_NAME) ?>" type="video/mp4">
                                                            </video>
                                                        </div>

                                                    <?php } ?>
                                                </div>
                                                <a href="<?php url("/project-info-home.php?project_id=$project_near_id&product_id=$product_id") ?>">
                                                    <p class="title"><?= $project_relate->project_name_th ?></p>
                                                    <p class="gray mb0"><?= !empty($project_concept->concept_th) ? $project_concept->concept_th : '' ?></p>
                                                    <p class="title green">
                                                        <b>ราคา
                                                            <?php if($project_price != false) echo Helper::getProjectPrice($project_price->project_price,$project_price->price_mode) ?>
                                                            ล้านบาท</b>
                                                    </p>
                                                </a>
                                            </div>
                                        </div>
                                        <?php

                                    }

                                    ?>
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <?php
                    }
                } ?>

                <?php if($project->project_status != 'SO'){ ?>

                    <div class="project-info-other-block block-margin">
                        <div class="row">

                            <?php
                            $project360 = get360Project($project_id);
                            if ($project360 != false) {
                                ?>

                                <div id="sec-virtual" class="col-md-6">
                                    <p class="heading-title">ภาพ 360 องศา</p>
                                    <div class="project-info-other-img">
                                        <a href="#" data-featherlight="#lb360">
                                            <span class="i-view-360"></span>
                                            <img src="<?= backend_url('base', $project360->thumnail) ?>"
                                                 alt="">
                                        </a>
                                        <iframe class="lightbox vdoview" src="<?= $project360->c360_project_url ?>"
                                                width="1000"
                                                height="560" id="lb360" style="border:none;" webkitallowfullscreen
                                                mozallowfullscreen allowfullscreen></iframe>
                                    </div>
                                    <!--                            <p class="title">Good Moment</p>-->
                                </div>

                                <?php
                            }
                            $vdoProject = getVDOProject($project_id);
                            if ($vdoProject != false) {
                                if($vdoProject->tvc_type == 'youtube'){
                                    $youtube_img = backend_url('base',$vdoProject->thumnail);
                                    $youtube_url = str_replace('watch?v=', 'embed/', $vdoProject->clip_project_url);
                                    $parameterPef= strpos($youtube_url, '?') !== false ? '&' : '?';
                                    ?>
                                    <div id="sec-tvc" class="col-md-6">
                                        <p class="heading-title">VDO</p>
                                        <div class="project-info-other-img">
                                            <a href="#" data-featherlight="#lbVdo1">
                                                <span class="i-view-vdo"></span>
                                                <img src="<?= $youtube_img ?>" alt="" style="">
                                            </a>
                                            <iframe class="lightbox" src="<?= $youtube_url . $parameterPef ."autoplay=0" ?>" width="1000"
                                                    height="560" id="lbVdo1" style="border:none;" webkitallowfullscreen
                                                    mozallowfullscreen allowfullscreen></iframe>

                                        </div>
                                        <!--                            <p class="title">Good Moment</p>-->
                                    </div>
                                    <?php
                                }elseif($vdoProject->tvc_type == 'vdo'){
                                    ?>
                                    <div id="sec-tvc" class="col-md-6">
                                        <p class="heading-title">VDO</p>
                                        <div class="project-info-other-img">
                                            <a id="tvc_vdo">
                                                <span class="i-view-vdo"></span>
                                                <img src="<?= backend_url('base',$vdoProject->thumnail) ?>" alt="" style="">
                                            </a>
                                            <script>
                                                $(document).ready(function () {
                                                    $('#tvc_vdo').click(function () {
                                                        $('#tvcVdo').show();
                                                        $.featherlight($('#tvcVdo'),{});
                                                        $('.featherlight-content #tvc_video_player')[0].play();
                                                        $('#tvcVdo').hide();
                                                    });
                                                })
                                            </script>
                                            <div id="tvcVdo" style="position:relative;width: 100%;display: none;">
                                                <video id='tvc_video_player' preload='none' controls>
                                                    <source src="<?= backend_url('base',$vdoProject->clip_project_url) ?>" type="video/mp4">
                                                </video>
                                            </div>

                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            <?php } ?>
                        </div>
                    </div>

                <?php } ?>

            </div>
        </div>

        <?php if($project->project_status != 'SO'){ ?>

            <input type="hidden" id="lat" value="<?= $project->latitude ?>">
            <input type="hidden" id="lng" value="<?= $project->longtitude ?>">
            <div id="sec-location" class="project-location-block block-margin">

                <?php
                $project->latitude = str_replace(' ','',$project->latitude);
                $project->longtitude = str_replace(' ','',$project->longtitude);
                if(!empty($project->latitude) && $project->longtitude){
                    ?>
                    <div class="container">
                        <p class="heading-title">Location</p>
                    </div>

                    <div class="map-container">
                        <div id="map"></div>
                        <span class="i-getdirection" onclick="toLocationByWalk();"></span>
                        <span class="i-getdirection-car" onclick="toLocation();"></span>
                        <?php if(!empty($projectMap) && (!empty($projectMap->map_img) || !empty($projectMap->map_pdf)) ){
                            ?>

                            <?php
                            if(!empty($projectMap->map_pdf)){
                                ?>

                                <a href="<?php echo backend_url('base',$projectMap->map_pdf); ?>" class="map-download-pdf" target="_blank"><i class="map-pdf"></i><span class="cal-txt">Map PDF</span></a>

                                <?php
                            }
                            if(!empty($projectMap->map_img)){
                                ?>
                                
                                <a href="<?php echo backend_url('base',$projectMap->map_img); ?>" class="map-download-jpg" target="_blank"><i class="map-jpg"></i><span class="cal-txt">Map JPG</span></a>

                                <?php
                            }
                            ?>
                            <?php
                        } ?>

                    </div>
                    <div id="info-window" style="display:none;">
                        <div style="width: auto;">
                            <img src="images/logo/logo_indy.jpg" class="background-logo" style="float:left;"/>
                            <p style="float:left; padding-top: 20px;margin-left: 20px;">ราคาเริ่มต้น <br> <span>3.79 - 5 ล้านบาท</span>
                            </p>
                        </div>
                    </div>

                    <?php
                }
                ?>


                <?php
                $projectNearBys = getProjectNearBy($project_id);
                if ($projectNearBys != false && !empty($projectNearBys[0]->nearby_name_th) ) {
                    ?>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="title">สิ่งอำนวยความสะดวกโดยรอบโครงการ</p>
                                <?php
                                $leftProjectNearBys  = [];
                                $rightProjectNearBys = [];
                                foreach ($projectNearBys as $i => $projectNearBy)
                                {
                                    if($i%2 == 0){
                                        $leftProjectNearBys[] = $projectNearBy;
                                    }elseif ($i%2 == 1){
                                        $rightProjectNearBys[] = $projectNearBy;
                                    }
                                }
                                ?>
                                <div class="content-style">
                                    <div class="col-md-6">
                                        <ul class="tbs1">
                                            <?php
                                            foreach ($leftProjectNearBys as $projectNearBy) {
                                                if (!empty($projectNearBy->nearby_name_th) && !empty($projectNearBy->interval)) {
                                                    ?>
                                                    <li>
                                                        <?= $projectNearBy->nearby_name_th ?> <?= $projectNearBy->interval ?>
                                                        กม.
                                                    </li>
                                                    <?php
                                                }
                                                ?>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <div class="col-md-6">
                                        <ul class="tbs1">
                                            <?php
                                            foreach ($rightProjectNearBys as $projectNearBy) {
                                                if (!empty($projectNearBy->nearby_name_th) && !empty($projectNearBy->interval)) {
                                                    ?>
                                                    <li>
                                                        <?= $projectNearBy->nearby_name_th ?> <?= $projectNearBy->interval ?>
                                                        กม.
                                                    </li>
                                                    <?php
                                                }
                                                ?>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>

                <?php
                $project_contact    = getProjectContactByProjectID($project_id);
                $contact_day        = [
                    'all'   =>  'ทุกวัน',
                    '0'     =>  'วันหยุดนักขัตฤกษ์',
                    '1'     =>  'วันจันทร์',
                    '2'     =>  'วันอังคาร',
                    '3'     =>  'วันพุธ',
                    '4'     =>  'วันพฤหัสบดี',
                    '5'     =>  'วันศุกร์',
                    '6'     =>  'วันเสาร์',
                    '7'     =>  'วันอาทิตย์',
                ];
                $day_str  = 'ทุกวัน';
                $start_time_contact     = '';
                $end_time_contact       = '';
                $tell_contact           = '';
                $email_contact          = '';
                $arr_days = [];
                if($project_contact != false){
                    $open_day = str_replace(' ','',$project_contact->open_day);
                    if (strpos($open_day, ',') !== false) {
                        $arr_days = explode(',',$open_day);
                        if(count($arr_days) > 0 ){
                            $day_str .= ' ยกเว้น ';
                        }
                        foreach ($arr_days as $i => $arr_day){
                            $day_str .= ' '.$contact_day[(string)$arr_day];
                            if(count($arr_days) != $i+1 ){
                                $day_str .= ',';
                            }
                        }
                    }
                    $start_time_contact     = $project_contact->open_time_start;
                    $end_time_contact       = $project_contact->open_time_end;
                    $tell_contact           = $project_contact->telephone;
                    $email_contact          = $project_contact->email;
                }

                ?>

                <div class="container">
                    <div id="sec-contact" class="block-margin">
                        <p class="heading-title">Contact & Appointment</p>
                        <?php if($project_contact != false){
                            ?>
                            <span> ติดต่อสำนักงานขาย <?= $day_str ?> เวลาทำการ <?= $start_time_contact ?> - <?= $end_time_contact ?> น. โทร. <?= $tell_contact ?> EMAIL : <?= $email_contact ?></span>
                            <?php
                        }?>
                        <div>
                            <form action="" class="project-form form-container" id="Projectcontact">
                                <p class="title-label"></p>
                                <div class="">
                                    <div class="form-group radio-checkmark">
                                        <input type="radio" id="check-contact" name="check-type" checked/>
                                        <label for="check-contact" id="checkcont" class="title-label"><span></span>
                                        ติดต่อสอบถาม</label>
                                        <input type="radio" id="check-appoint" name="check-type"/>
                                        <label for="check-appoint" id="checkappoint" 
                                               class="title-label"><span></span>นัดหมายเยี่ยมชมโครงการ</label>
                                    </div>
                                    <div class="form-group check-type-radio">
                                        <input type="text" id="date_appointment" name="datecont" class="form-control"
                                               placeholder="*วันที่ต้องการนัดหมาย" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="mailcont" class="form-control" value="<?= $email_contact ?>" readonly style="background-color: #ffffff;">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="namecont" class="form-control" placeholder="*ระบุชื่อ - นามสกุล" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" min="0" name="phonecont" id="phonecont" class="form-control" 
                                        placeholder="*หมายเลขโทรศัพท์" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="email" name="emailcust" class="form-control" placeholder="*อีเมล์" required>
                                    </div>
                                    <div class="form-group">
                                        <textarea name="detailcont" placeholder="*ระบุข้อความ" id="" class="form-control" cols="30"
                                                  rows="10" required></textarea>
                                    </div>
                                    <div class="title-block">
                                        <p class="title">ต้องการให้ติดต่อกลับทาง</p>
                                    </div>
                                    <div class="form-group">
                                        <input type="checkbox" id="check-tel" name="check-tel"/>
                                        <label for="check-tel" class="title-label"><span></span>หมายเลขโทรศัพท์</label>
                                        <input type="checkbox" id="check-mail" name="check-mail"/>
                                        <label for="check-mail" class="title-label"><span></span>อีเมล์</label>
                                    </div>
                                    <div class="g-recaptcha" data-sitekey="6LcSzSEUAAAAAPTUl4maci5Pl4SyWVMmnNhly0Qw"></div>
                                    <span id="chacheck1" style="display: none; color: red;">พิสูจน์ว่าท่านไม่ใช่บอท !</span>
                                    <button class="btn btn-submit">ส่งข้อมูล</button>
                                </div>
                            </form>
                        </div>
                    </div>

            <script>
  
                $(document).ready(function() {

                     $('#checkcont').click(function(){

                            $('#date_appointment-error').hide();
                     })

                    $('#checkappoint').click(function(){

                            $('#date_appointment-error').show();
                    })

                    $("#Projectcontact").validate({
                        rules: {
                            mailcont: "required",
                            namecont: "required",
                            phonecont: "required",
                            emailcust : "required",
                            detailcont : "required",
                            datecont : "required",
                    },
                        messages: {
                            namecont: "กรุณากรอกชื่อ-นามสกุล !",
                            phonecont : "กรุณากรอกหมายเลขโทรศัพท์ !",
                            emailcust : "กรุณากรอกอีเมล์ !",
                            detailcont : "กรุณาระบุข้อความ !",
                            datecont : "กรุณาระบุวันที่ต้องการนัดหมาย !"

                    }
                });

                $('#Projectcontact').on('submit', function(){
                 var googleResponse1 = $('#g-recaptcha-response-1').val();
                    
                    if(googleResponse1 == ''){   
                            
                            $('#chacheck1').show();
                             return false;
                }

                });
               
                $('#phonecont').keydown(function(event){
                        var max = 10;
                        var phone = $(this).val().length;
                
                         if(phone >= max){

                            if(event.keyCode == 8 || event.keyCode == 46){
                                
                                return true;                    
                        
                            }                        
                                return false;
                    
                        }
             });

         });

        </script>


    </div>

                </div>

        <?php } ?>

        <div style="color: #999999;font-size: 15px;" class="container block-margin">
                        <?php
                            if($project_concept != false) {
                                $information = Helper::checkLangEnglish($lang) ? $project_concept->information_en : $project_concept->information_th;
                                echo $information;
                            }
                       ?>
            </div>

<?php include('footer.php'); ?>