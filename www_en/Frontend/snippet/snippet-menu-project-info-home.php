<?php

require_once dirname(dirname(__FILE__)).'/include/help/begin.php';
require_once dirname(dirname(__FILE__)).'/include/help/query_function.php';


$promo = getProjectPromotion($_GET['project_id']);
$proj  = getProjectByID($_GET['project_id']);
$homeS = getHomeSell($_GET['project_id'], $_GET['product_id']);
$galls = getGalleryImgProject($_GET['project_id']);
$pr360 = get360Project($_GET['project_id']);
$prVdo = getVDOProject($_GET['project_id']);

?>
<div class="main-navigation">
    <ul id="main-nav">
        <li><a class="m-submenu-innerpage" data-name="info" data-status="0">Project Details</a>
            <div class="submenu-innerpage info">
                <div class="container">
                    <div class="row">
                        <div class="submenu-list">
                            <ul>
                                <li><a id="information" data-family="child">Project Details</a></li>
                                <?php
                                if($galls != false) {
                                    ?>
                                    <li><a id="gallery" data-family="child">GALLERY</a></li>
                                    <?php
                                }
                                ?>
                                <?php
                                if ($pr360 != false) {
                                ?>
                                    <li><a id="virtual" data-family="child">360-degree image</a></li>
                                    <?php
                                }
                                ?>
                                <?php
                                if ($prVdo != false) {
                                ?>
                                    <li><a id="tvc" data-family="child">VDO</a></li>
                                    <?php
                                }
                                ?>
                                <?php
                                $proj->latitude = str_replace(' ','',$proj->latitude);
                                $proj->longtitude = str_replace(' ','',$proj->longtitude);
                                if(!empty($proj->map_link)){
                                ?>
                                    <li><a id="location" data-family="child">Project's location</a></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <?php
            if($homeS != false && $proj->project_status != 'SO'){
                ?>
                    <li><a id="home-sale" class="m-submenu-innerpage" >House for sale</a></li>
                <?php
            }
        ?>
        <?php
            if($promo != false && $proj->project_status != 'SO'){
                ?>
                <li><a id="promotion" class="m-submenu-innerpage" >Promotion</a></li>
        <?php
            }
        ?>
        <li><a id="" class="m-submenu-innerpage" data-name="contact" data-status="0">Contact the Project site</a>
            <div class="submenu-innerpage contact">
                <div class="container">
                    <div class="row">
                        <div class="submenu-list">
                            <ul>
                                 <li>
                                <a id="contact" data-family="child"><i class="i-subm-email"></i>Project inquiry or to schedule a  project visit</a>
                                 <?php
                                     //$edm = get_Emd_Project($project_id);

                                 ?>
                               
                                 
                                 
                               


                                </li>
                                <?php
                                $proj->latitude = str_replace(' ','',$proj->latitude);
                                $proj->longtitude = str_replace(' ','',$proj->longtitude);
                                   if(!empty($proj->map_link)){
                                ?>
                                    <li><a id="location" data-family="child"><i class="i-subm-direction"></i>Directions / Maps to the projects</a></li>
                                <?php } ?>
                                <li><a href="call:1198" data-family="child" style="cursor: text;"><i class="i-subm-call"></i>Call 1198 ( from 9 a.m. - 5.30 p.m)</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </li>
    </ul>
</div>