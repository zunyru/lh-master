<?php
    $products   = getSearchSelectProduct();
    $zones      = getSearchSelectZone();
    $brands     = getSearchSelectBrand();
    $projects   = getSearchSelectProject();

?>
<div class="search-container topdropdown-container">
    <span class="panel-close">close</span>
    <script>
        $(document).ready(function () {
            $('#search_form').submit(function () {
                var product_id  = $('#product_id').val();
                var zone_id     = $('#zone_id').val();
                var brand_id    = $('#brand_id').val();
                if(product_id == '' && zone_id == '' && brand_id == ''){
                    return false;
                }
            });
        });
    </script>
    <form action="<?= $router->generate('search') ?>" id="search_form">
        <h3>Search</h3>
        <p>Find the project you are interested in.</p>
        <div class="">
            <div class="projecttype-block search-block">
                <span id="validateSearchFilter" class="txt-error" style="position: relative;display: none;" >
                    Please select a condition to search.
                </span>
                <input type="hidden" id="product_id" name="product_id" value="">
                <div class="searchBlock" data-status="0">
                    <span>Select project type</span>
                    <div class="search-icon-dropdown"><i class="i-search-triangle"></i></div>
                </div>
                <ul id="searchLists">
                    <?php
                    foreach ($products as $product){
                        ?>
                        <li data-id="<?= $product->product_id ?>" data-slug="<?= $product->slug ?>"  data-value="<?= $product->product_name_th ?>"><?= $product->product_name_th ?></li>
                    <?php
                    }
                    ?>
                </ul>
            </div>

            <div class="projectlocation-block search-block">
                <input type="hidden" id="zone_id" name="zone_id" value="">
                <div class="searchBlock" data-status="0">
                    <span>Select Location</span>
                    <div class="search-icon-dropdown"><i class="i-search-triangle"></i></div>
                </div>
                <ul id="projectlocationLists">
                    <?php
                    foreach ($zones as $zone){
                        ?>
                        <li data-id="<?= $zone->zone_id ?>" data-slug="<?= $zone->slug ?>" data-value="<?= $zone->zone_name ?>"><?= $zone->zone_name ?></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>

            <div class="projectbrand-block search-block">
                <input type="hidden" id="brand_id" name="brand_id" value="">
                <div class="searchBlock" data-status="0">
                    <span>Select Brand</span>
                    <div class="search-icon-dropdown"><i class="i-search-triangle"></i></div>
                </div>
                <ul id="projectbrandLists">
                    <?php
                    foreach ($brands as $brand){
                        ?>
                        <li data-id="<?= $brand->brand_id ?>" data-slug="<?= $brand->slug ?>" data-value="<?= $brand->brand_name_th ?>"><?= $brand->brand_name_th ?></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>

            <div class="projectname-block search-block">
                <input type="hidden" id="project_id" name="project_id" value="">
                <div class="searchBlock" data-status="0">
                    <span>Select Project</span>
                    <div class="search-icon-dropdown"><i class="i-search-triangle"></i></div>
                </div>
                <ul id="projectnameLists">
                    <?php
                    foreach ($projects as $project){
                        $product_arr= Helper::getArrayProductOfProject($project->project_id);
                        if(in_array(1,$product_arr) || in_array(2,$product_arr) || !in_array(3,$product_arr)){
                            $project_url = isLadawan($project->project_id) ?
                                $router->generate('ladawan-detail',[
                                    'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                    'product_id'    =>  1
                                ]) :
                                $router->generate('single-home-detail',[
                                    'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                    'product_id'    =>  1
                                ]);
                        }
                        else{
                            $project_url = isLadawan($project->project_id) ?
                                $router->generate('ladawan-detail',[
                                    'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                    'product_id'    =>  3
                                ]) :
                                $router->generate('condominium-detail',[
                                    'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                    'lang'          =>  'th'
                                ]);
                        }
                        ?>
                        <li data-url="<?= $project_url ?>" data-value="<?= $project->project_name_th ?>"><?= $project->project_name_th ?></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>

            <button class="btn-search"><i></i> Search</button>
        </div>

    </form>
</div>
<script>
    $('#')
</script>