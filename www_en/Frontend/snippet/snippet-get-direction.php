<?php

$zones      = getSearchSelectZoneBydirection();
$projects   = getSearchSelectProjectBydirection();

?>
<div class="getdirection-container topdropdown-container">
    <span class="panel-close">close</span>
    <form id="getDirection" action="<?= $router->generate('get-direction') ?>">
        <h3>Get Direction</h3>
        <p>Find directions to projects you are interested in.</p>

        <div class="">
            <div class="directlocation-block topdropdown-block">
                <input type="hidden" id="project_zone_id" name="zone_id" value="">
                <div class="searchBlock" data-status="0">
                    <span>Location</span>
                    <div class="search-icon-dropdown"><i class="i-search-triangle"></i></div>
                </div>
                <ul id="directlocationLists">
                    <?php
                    foreach ($zones as $zone){
                        ?>
                        <li data-id="<?= $zone->zone_id ?>" data-slug="<?= $zone->slug ?>" data-value="<?= $zone->zone_name ?>"><?= $zone->zone_name ?></li>
                        <?php
                    }
                    ?>
                </ul>
                <span id="validateZone" class="txt-error" style="display: none;">Please specify location.</span>
            </div>

            <div class="directproject-block topdropdown-block">
                <input type="hidden" id="project_direction_id" name="project_id" value="">
                <input type="hidden" id="project_real_id" name="project_real_id" value="">
                <div class="searchBlock" data-status="0">
                    <span>Project</span>
                    <div class="search-icon-dropdown"><i class="i-search-triangle"></i></div>
                </div>
                <ul id="directprojectLists">
                    <?php
                    foreach ($projects as $project){
                        ?>
                        <li data-id="<?= $project->project_id ?>" data-slug="<?= $project->latitude ?>" data-value="<?= $project->project_name_th ?>"><?= $project->project_name_th ?></li>
                        <?php
                    }
                    ?>
                </ul>
                <span id="validateProjectName" class="txt-error" style="display: none;">Please specify project.</span>
            </div>

            <button class="btn-search"><i></i> Search</button>

        </div>

    </form>
</div>