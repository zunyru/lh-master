<div class="submenu-bg">
    <div class="submenu-container">
        <div class="container">
            <div class="row">
                <div class="submenu-list">
                    <div class="col-md-7 col-md-offset-1">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="submenu-block">
                                    <p class="submenu-title">Project Details</p>
                                    <ul>
                                       <!--  <li><a href="<?= $router->generate('furnished-home-list') ?>">Fully Furnished House for sale</a></li> -->
                                        <li><a href="<?= $router->generate('single-home-list') ?>">Single detached  house</a></li>
                                        <li><a href="<?= $router->generate('town-home-list') ?>">Townhome</a></li>
                                        <li><a href="<?= $router->generate('condominium-list',[
                                                 'lang'         =>  'th'
                                        ]) ?>">Condominium</a></li>
                                        <li><a href="<?= $router->generate('ladawan-list') ?>">LADAWAN</a></li>
					                    <!-- <li><a href="https://www.lh.co.th/vive">VIVE</a></li> -->
                                        <li><a href="<?= $router->generate('country-list') ?>">Projects in the provinces</a></li>
                                       <!--  <li><a href="<?= $router->generate('new-project-list') ?>">New Project news</a></li> -->
                                        <!-- <li><a href="<?= $router->generate('promotion') ?>">Promotion and extra privileges</a></li> -->
                                        <!-- <li><a href="<?= $router->generate('review') ?>">Project Review</a></li> -->
                                        <li><a href="<?= $router->generate('lh-project') ?>">Foreign Buyers</a></li>
                                        <!-- <li><a href="<?= $router->generate('tvc') ?>">TVC</a></li> -->
                                    </ul>
                                </div>
                            </div>

                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="submenu-block">
                                            <p class="submenu-title">Design concept</p>
                                            <ul>
                                                <li><a href="<?= $router->generate('home-series') ?>">House Plan</a></li>
                                                <li><a href="<?= $router->generate('360-virtual') ?>">360 degree photos / images</a></li>
                                                <li><a href="<?= $router->generate('air-plus') ?>">Air Plus Technology </a></li>
                                                <!--  
                                                <li><a href="<?= $router->generate('concept') ?>">แนวคิดบ้านสบาย</a></li>
                                                -->
                                                <!-- <li><a href="<?= $router->generate('living-tips') ?>">LH Living Tips</a></li> -->
                                                <!-- <li  ><a href="<?= $router->generate('motivo') ?>">Motivo</a></li> -->
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="submenu-block">
                                            <p class="submenu-title">Company Information</p>
                                            <ul>
                                                <li><a href="http://lh.listedcompany.com/corporate_information.html">Company Profile</a></li>
                                                <li><a href="http://lh.listedcompany.com/right_shareholder.html">Corporate  Governance </a></li>
                                                <li><a href="http://lh.listedcompany.com/home.html">Investor Info</a></li>
                                                <li><a href="http://lh.listedcompany.com/company_news.html">Corporate News</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="submenu-block">
                                            <p class="submenu-title">Contact us</p>
                                            <ul>
                                                <!-- <li><a href="<?= $router->generate('job-with-us') ?>">Applying for a job</a></li>
                                                <li><a href="<?= $router->generate('land-offer') ?>">Land offering for sale</a></li> -->
                                                <li><a href="<?= $router->generate('report') ?>">Report a problem on the website</a></li>
                                                <li hidden><a href="<?= $router->generate('contact-us') ?>">ติดต่อสอบถามข้อมูลทั่วไป</a></li>
                                            </ul>
                                        </div>
                                </div>
                                <div class="row">
                                    <!-- <div class="col-md-6">
                                        <div class="submenu-block">
                                            <p class="submenu-title">Customer / Family  Relationship</p>
                                            <ul>
                                                <li><a href="<?= $router->generate('community') ?>">LH Community</a></li>
                                                <li><a href="http://www7.lh.co.th/EService/LoginForm.do">Online home repair service</a></li>
                                                <li><a href="http://www7.lh.co.th/LHComplaint/index.jsp">Home & Condo complaints</a></li>
                                            </ul>
                                        </div>
                                    </div> -->
                                    <div class="col-md-6">
                                        <!-- <div class="submenu-block">
                                            <p class="submenu-title">Contact us</p>
                                            <ul>
                                                <li><a href="<?= $router->generate('job-with-us') ?>">Applying for a job</a></li>
                                                <li><a href="<?= $router->generate('land-offer') ?>">Land offering for sale</a></li>
                                                <li><a href="<?= $router->generate('report') ?>">Report a problem on the website</a></li>
                                                <li hidden><a href="<?= $router->generate('contact-us') ?>">ติดต่อสอบถามข้อมูลทั่วไป</a></li>
                                            </ul>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="submenu-tool">
                    <div class="col-md-4">
                        <div class="submenu-tools">
                            <a href="" id="topSearch" class="menu-tool-list" data-status="0">
                                <i class="i-subm-search"></i>
                                Find directions to your interested  Location
                            </a>
                            <a href="" id="topGetdirection" class="menu-tool-list" data-status="0">
                                <i class="i-subm-direction"></i>
                                Find directions to your interested  Projects
                            </a>
                           <!--  <a href="" id="topContact" class="menu-tool-list"> -->
                            <a href="<?= $router->generate('contact-us') ?>" class="menu-tool-list">
                                <i class="i-subm-email"></i>
                                Project information inquiry
                            </a>
                            <a href="#" class="menu-tool-list" id="call" style="cursor:text;">
                                <i class="i-subm-call"></i>
                                Tel 1198<br>
                                <span>( every day from 9.00 a.m. to 5.30 p.m)</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script  type="text/javascript" >
    $('#call').click(function(){

        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            window.location.href='tel:1198';
        }else{
            //alert('กรุณาติดต่อ : 1198')
        }

    });
</script>