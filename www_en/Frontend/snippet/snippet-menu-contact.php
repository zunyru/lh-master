<!-- <script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script> -->
<script src="https://www.google.com/recaptcha/api.js?onload=ReCaptchaCallback&render=explicit&hl=<?= !empty($_GET['lang']) ? $_GET['lang'] : 'en' ?>" async defer>
</script>
<script type="text/javascript">
    //         var CaptchaCallback = function() {
        
    //     grecaptcha.render('RecaptchaField1', 
    //                     {'sitekey' : '6LcOTx4UAAAAAIludSpeNgoh68Bwt4nbWK8Ksyy2',
    //                     callback: function () {
    //                         $('#chacheck1').hide();
    //                     }
    //                 });

    //     grecaptcha.render('RecaptchaField2', {'sitekey' : '6LcOTx4UAAAAAIludSpeNgoh68Bwt4nbWK8Ksyy2',
    //                     callback: function () {   
    //                          $('#chacheck2').hide();
    //                     }
    //     });
    // };
    
            var ReCaptchaCallback = function() {
    	       $('.g-recaptcha').each(function(){
    		      var el = $(this);
    		          grecaptcha.render(el.get(0), {'sitekey' : el.data("sitekey"),
    				         callback: function () {
                               
                                    $('#chacheck').hide();
                                    $('#chacheck1').hide();
                           
                             }
    			     });
    	        });  
            };
</script>
<style>
    .error{
        color: red;
    }
</style>
<script src="<?= file_path('../build/js/jquery.validate.js') ?>" /></script>

<div class="contact-container contact-container-ft topdropdown-container">
    <span class="panel-close">close</span>

    <h3>Contact & Appointment</h3>
    <div>
        <form class="project-form form-container" id="CheckContForm" name="CheckContForm" action="http://uat.lh.co.th/en/mail-snippet-contact" method="POST">
            <p class="title-label"></p>
            <div class="">
                <div class="form-group radio-checkmark">
                    <input type="radio" id="check-contact-ft" name="check-type" checked/>
                    <label for="check-contact-ft" id="popcontact" class="title-label" style="color: black"><span></span>Contact</label>
                    <input type="radio" id="check-appoint-ft" name="check-type"/>
                    <label for="check-appoint-ft" id="popappoint" class="title-label" style="display: none; color: black;"><span></span>Appointment</label>
                </div>
                <div class="form-group check-type-radio">
                    <input type="text" id="date_appointment_ft" name="dateappointment" class="form-control"
                           placeholder="*Appointment Date" style="background-color: #ffffff;" readonly required>
                </div>
                <div class="form-group">
                    <input type="text" name="emailpro" id="emailpro" class="form-control" 
                    style="background-color: #ffffff;" readonly>
                </div>
                <div class="form-group">
                    <input type="text" name="fullname" class="form-control" placeholder="*Full name" required>
                </div>
                <div class="form-group">
                    <input type="number" id="phonecon" name="phonecon" class="form-control" min="0" 
                    placeholder="*Tel No" required>
                </div>
                <div class="form-group">
                    <input type="text" name="email" class="form-control" placeholder="*Email" required>
                </div>
                <div class="form-group">
                    <textarea name="detail" placeholder="*Remark" id="" class="form-control" cols="30"
                              rows="3" required></textarea>
                </div>
                <div class="title-block">
                    <p class="title">Call back by</p>
                </div>
                <div class="form-group">
                    <input type="checkbox" id="check-tel-ft" name="check-tel-ft"/>
                    <label for="check-tel-ft" class="title-label" style="color: black"><span></span>Phone</label>
                    <input type="checkbox" id="check-mail-ft" name="check-mail-ft"/>
                    <label for="check-mail-ft" class="title-label"style="color: black"><span></span>Email</label>
                </div>
                <!-- <div id="RecaptchaField1"></div> -->
                <div class="g-recaptcha" data-sitekey="6LcSzSEUAAAAAPTUl4maci5Pl4SyWVMmnNhly0Qw"></div>
                <span id="chacheck" style="display: none; color: red;">to prove that you are not a robot !</span>
                <br><br>
                <input type="submit" class="btn-search"  name="submit" value="Send">
            </div>
        </form>
    </div>
</div>

<script>

  $(document).ready(function() {

            var email = $('#mailcont').val();
            // var email2;
            // $('#submenu').click(function(){
                    
            //         alert('dddd');
             
            //     })
  			
            
            if(email === undefined || email  === null && email2 ==""){
                 
                    $('#emailpro').val('info@lh.co.th'); 
            
            }else{
                
                $('#emailpro').val(email); 
   
            }
            
            $("#CheckContForm").validate({
                rules: {
                    fullname: "required",
                    phonecon: "required",
                    email: "required",
                    detail : "required",
                    dateappointment : "required",
                },
                messages: {
                    fullname: "Please enter your full name. !",
                    phonecon : "Please enter your phone number. !",
                    email : "Please enter your email !",
                    detail : "Please enter a message. !",
                    dateappointment : "Please specify the date of your appointment. !"

                }
            });

            $('#phonecon').keydown(function(event){
                var max = 10;
                var phone = $(this).val().length;
                
                if(phone >= max){

                    if(event.keyCode == 8 || event.keyCode == 46){
                        return true;                    
                    }                        
                        return false;
                    
                }
            });

            $('#CheckContForm').on('submit', function(){
                var googleResponse = $('#g-recaptcha-response').val();
                if(googleResponse==''){   
                    $('#chacheck').show();

                     return false;
           
                }

            });

            $('#popcontact').click(function(){
                
                    $('#date_appointment_ft-error').hide();
                

            })
             $('#popappoint').click(function(){
                

                   $('#date_appointment_ft-error').show();

            })


        });

</script>
