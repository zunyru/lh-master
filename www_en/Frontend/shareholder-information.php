<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>

<?php include('header.php'); ?>
<!-- CSS -->
<link rel="stylesheet" href="css/corporate-all.css" type="text/css">
<link rel="stylesheet" href="css/corporate-menu.css" type="text/css">
<link rel="stylesheet" href="css/governance.css" type="text/css">
<link rel="stylesheet" href="css/investor.css" type="text/css">

<!-- JS -->
<script src="js/corporate-menu.js"></script>
<script src="js/corporate-menu.js"></script>

<div id="content" class="content corp-page share-info-page">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                <h1>ข้อมูลสำหรับผู้ถือหุ้น</h1>
                <div id="corpMenu" class="corp-menu-tab">
                    <ul>
                        <li class="current"><a href="#corpBlock1">หนังสือเชิญประชุมผู้ถือหุ้น</a></li>
                        <li><a href="#corpBlock2">โครงสร้างการถือหุ้น</a></li>
                        <li><a href="#corpBlock3">ประวัติการเพิ่มทุน</a></li>
                        <li><a href="#corpBlock4">ข้อบังคับบริษัทเรื่องหุ้นของบุคคลต่างด้าว</a></li>
                        <li><a href="#corpBlock5">จำนวนหุ้นที่จดทะเบียนและชำระแล้ว</a></li>
                        <li><a href="#corpBlock6">รายงานการประชุมผู้ถือหุ้น</a></li>
                        <li><a href="#corpBlock7">การจ่ายเงินปันผล</a></li>
                        <li><a href="#corpBlock8">ปฏิทินนักลงทุน</a></li>
                    </ul>
                </div>

                <div id="corpBlock1" class="corp-block current list-style-none">
                    <div>
                        <div class="">
                            <div class="col-govern-50">
                                <h3 class="title-gov">หนังสือเชิญประชุมผู้ถือหุ้น</h3>
                                <p>ดาวน์โหลดเอกสารการเสนอวาระการประชุมผู้ถือหุ้นครั้งล่าสุด</p>
                                <ul>
                                    <li><a href=""><i class="download"></i>หลักเกณฑ์การให้สิทธิแก่ผู้ถือหุ้นส่วนน้อยในการเสนอวาระการประชุมและการเสนอชื่อบุคคลเพื่อรับเลือกตั้งเป็นกรรมการบริษัทฯเป็นการล่วงหน้า</a></li>
                                    <li><a href=""><i class="download"></i>การเสนอชื่อบุคคลเพื่อรับเลือกตั้งเป็นกรรมการ</a></li>
                                </ul>
                            </div>
                            <div class="col-govern-50 col-govern-right">
                                <img src="images/investor/inv_shareholder_1.jpg" alt="" class="">
                            </div>
                        </div>
                    </div>
                </div>

                <div id="corpBlock2" class="corp-block">
                    <div>
                        <h3 class="title-gov">โครงสร้างการถือหุ้น</h3>
                        <p>รายชื่อผู้ถือหุ้นใหญ่ 10 อันดับ ณ วันที่ 29 สิงหาคม 2559 </p>
                        <table class="table-historical-price table-shareholder" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                            <tr class="">
                                <th class="tb-title">ลำดับ</th>
                                <th class="tb-left">ชื่อบุคคล / นิติบุคคล</th>
                                <th>ร้อยละของจำนวนหุ้น</th>
                                <th>ที่จำหน่ายแล้วทั้งหมด</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="tb-center">1.</td>
                                <td class="tb-left">นายอนันต์ อัศวโภคิน</td>
                                <td class="tb-pdr40">2,859,167,547 </td>
                                <td class="tb-pdr40">24.27%</td>
                            </tr>
                            <tr>
                                <td class="tb-center">2.</td>
                                <td class="tb-left">บริษัท ไทยเอ็นวีดีอาร์ จำกัด</td>
                                <td class="tb-pdr40">2,082,727,289 </td>
                                <td class="tb-pdr40">17.68%</td>
                            </tr>
                            <tr>
                                <td class="tb-center">3.</td>
                                <td class="tb-left">GIC Private Limited</td>
                                <td class="tb-pdr40">1,932,798,432 </td>
                                <td class="tb-pdr40">16.41%</td>
                            </tr>
                            <tr>
                                <td class="tb-center">4.</td>
                                <td class="tb-left">บริษัท เมย์แลนด์ จำกัด</td>
                                <td class="tb-pdr40">676,289,269 </td>
                                <td class="tb-pdr40">5.74%</td>
                            </tr>
                            <tr>
                                <td class="tb-center">5.</td>
                                <td class="tb-left">Chase Nominees Limited</td>
                                <td class="tb-pdr40">631,964,432 </td>
                                <td class="tb-pdr40">5.37%</td>
                            </tr>
                            <tr>
                                <td class="tb-center">6.</td>
                                <td class="tb-left">สำนักงานประกันสังคม</td>
                                <td class="tb-pdr40">311,815,300 </td>
                                <td class="tb-pdr40">2.65%</td>
                            </tr>
                            <tr>
                                <td class="tb-center">7.</td>
                                <td class="tb-left">State Street Bank and Trust Company</td>
                                <td class="tb-pdr40">208,555,778 </td>
                                <td class="tb-pdr40">1.77%</td>
                            </tr>
                            <tr>
                                <td class="tb-center">8.</td>
                                <td class="tb-left">น.ส.เพียงใจ หาญพาณิชย์</td>
                                <td class="tb-pdr40">134,900,000 </td>
                                <td class="tb-pdr40">1.15%</td>
                            </tr>
                            <tr>
                                <td class="tb-center">9.</td>
                                <td class="tb-left">State Street Bank Europe Limited</td>
                                <td class="tb-pdr40">130,391,836 </td>
                                <td class="tb-pdr40">1.11%</td>
                            </tr>
                            <tr>
                                <td class="tb-center">10.</td>
                                <td class="tb-left">The Bank of New York Mellon</td>
                                <td class="tb-pdr40">121,010,839 </td>
                                <td class="tb-pdr40">1.03%</td>
                            </tr>
                            <tr>
                                <td class="tb-center">&nbsp;</td>
                                <td class="tb-left">&nbsp;</td>
                                <td class="tb-pdr40 tb-summary"><strong>9,089,620,722 </strong></td>
                                <td class="tb-pdr40 tb-summary"><strong>77.17%</strong></td>
                            </tr>
                            </tbody></table>
                    </div>
                </div>

                <div id="corpBlock3" class="corp-block">
                    <div>
                        <h3></h3>

                        <table class="table-historical-price table-shareholder2" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th width="223">ผู้ถือหุ้นสัดส่วนการถือหุ้น (ร้อยละ)</th>
                                <th width="73">ธ.ค. 2554</th>
                                <th width="73">ธ.ค. 2555</th>
                                <th width="73">ธ.ค. 2556 </th>
                                <th width="73">ธ.ค. 2557</th>
                                <th width="73">ธ.ค. 2558</th>
                                <th width="73">ส.ค. 2559</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="tb-title tb-left"><strong>1. กลุ่มนายอนันต์ อัศวโภคิน</strong></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="tb-left">1.1 นายอนันต์ อัศวโภคิน</td>
                                <td>23.76%</td>
                                <td>23.76%</td>
                                <td>23.76%</td>
                                <td>22.07%</td>
                                <td>24.37%</td>
                                <td>24.27%</td>
                            </tr>
                            <tr class="row-highlight">
                                <td class="tb-left">1.2 บริษัท เมย์แลนด์ จำกัด</td>
                                <td>5.62%</td>
                                <td>5.62%</td>
                                <td>5.62%</td>
                                <td>5.22%</td>
                                <td>5.77%</td>
                                <td>5.74%</td>
                            </tr>
                            <tr>
                                <td class="tb-left">1.3 น.ส. เพียงใจ หาญพาณิชย์</td>
                                <td>1.14%</td>
                                <td>1.14%</td>
                                <td>1.14%</td>
                                <td>1.03%</td>
                                <td>1.15%</td>
                                <td>1.15%</td>
                            </tr>
                            <tr class="row-highlight">
                                <td class="tb-summary tb-left"><strong>รวมกลุ่มนายอนันต์ อัศวโภคิน</strong></td>
                                <td class="tb-summary">30.52%</td>
                                <td class="tb-summary">30.52%</td>
                                <td class="tb-summary">30.53%</td>
                                <td class="tb-summary">28.32%</td>
                                <td class="tb-summary">31.29%</td>
                                <td class="tb-summary">31.16%</td>
                            </tr>
                            <tr class="line">
                                <td class="tb-title tb-left"><strong>2. Government of Singapore Investment Corporation (GIC)</strong></td>
                                <td>13.69%</td>
                                <td>15.97%</td>
                                <td>16.11%</td>
                                <td>14.96%</td>
                                <td>16.51%</td>
                                <td>16.41%</td>
                            </tr>
                            <tr class="row-highlight">
                                <td class="tb-left">อื่นๆ</td>
                                <td>55.79%</td>
                                <td>53.50%</td>
                                <td>53.36%</td>
                                <td>56.72%</td>
                                <td>52.20%</td>
                                <td>52.43%</td>
                            </tr>
                            <tr>
                                <td class="tb-left">รวม</td>
                                <td>100.00%</td>
                                <td>100.00%</td>
                                <td>100.00%</td>
                                <td>100.00%</td>
                                <td>100.00%</td>
                                <td>100.00%</td>
                            </tr>
                            <tr class="row-highlight">
                                <td class="tb-summary tb-left"><strong>ทุนที่ชำระแล้ว (ล้านบาท)</strong></td>
                                <td class="tb-summary">10,025.92 </td>
                                <td class="tb-summary">10,025.92 </td>
                                <td class="tb-summary">10,025.92 </td>
                                <td class="tb-summary">10,985.57 </td>
                                <td class="tb-summary">11,730.03 </td>
                                <td class="tb-summary">11,779.07 </td>
                            </tr>
                            </tbody></table>
                    </div>
                </div>

                <div id="corpBlock4" class="corp-block">
                    <div>
                        <h3 class="title-gov">ข้อบังคับบริษัทเรื่องหุ้นของบุคคลต่างด้าว</h3>
                        <div class="col-govern-50">
                            <p><strong>ข้อ 10 : หุ้นของบริษัทสามารถโอนได้โดยไม่มีข้อจำกัด</strong></p>
                            <p>เว้นแต่การโอนหุ้นนั้นเป็นการโอนหุ้นจากผู้ถือหุ้นสัญชาติไทยให้แก่คน ต่างด้าว ในขณะที่มีคนต่างด้าวถือหุ้นอยู่ในบริษัทเป็นจำนวนถึงร้อยละ 30 ของหุ้นที่ออกจำหน่ายแล้วทั้งหมดของบริษัท

                            นอกเหนือจากการรับโอนหุ้นตามอัตราที่กำหนดในวรรคแรกแล้ว คนต่างด้าวอาจเข้ามาถือหุ้นใหม่ที่ออกโดยบริษัท อันเนื่อง มาจาก (ก) การใช้สิทธิซื้อหุ้นเพิ่มทุน ซึ่งออกและเสนอขายให้แก่ผู้ถือหุ้นเดิม ตามมติที่ประชุมวิสามัญผู้ถือหุ้น ครั้งที่ 1/2542 เมื่อวันที่ 28 ตุลาคม 2542 หรือ</p>

                            <p>การเสนอขายให้แก่ผู้ลง ทุนที่มีลักษณะหรือจัดอยู่ในประเภทตามข้อ 2 ของประกาศสำนักงานคณะกรรมการกำกับตลาด หลักทรัพย์เรื่อง หลักเกณฑ์ เงื่อนไข และวิธีการในการออกและเสนอขายหุ้นที่ออกใหม่และการขออนุญาตฉบับลงวันที่ 18 พฤษภาคม 2535 หรือ</p>
                            <p>การใช้สิทธิแปลงสภาพ ของหุ้นกู้แปลงสภาพ ที่เสนอขายให้แก่นักลงทุนในต่างประเทศทั้งจำนวน ซึ่งออกตามมติที่ประชุมวิสามัญผู้ถือหุ้น ครั้งที่ 2/2535 เมื่อวันที่ 15 ธันวาคม 2535 แม้ว่าอัตราส่วนการถือหุ้นของคนต่างด้าวจะครบร้อยละ 30 ของหุ้นที่ออกจำหน่ายแล้วทั้งหมดของบริษัท แต่ทั้งนี้ การเข้าถือหุ้นดังกล่าวจะต้องไม่ทำให้อัตราส่วนการถือหุ้นของคนต่างด้าวทั้ง หมดเกินกว่าร้อยละ 49 ของหุ้นที่ออกจำหน่ายแล้วทั้งหมดของบริษัท การถือหุ้นโดยคนต่างด้าวเกินร้อยละ 30 แต่ไม่เกินร้อยละ 49 ของหุ้นที่ออกจำหน่ายแล้วทั้งหมดของบริษัทนี้ จะใช้บังคับกรณีการโอนหุ้นของคนต่างด้าวที่ได้หุ้นของบริษัท อันเนื่องมาจากการเข้าถือหุ้นที่ออกใหม่ หรืออันเนื่องมาจากการใช้สิทธิแปลงสภาพของหุ้นกู้แปลงสภาพตามวรรคสองนี้ใน ทุกๆ ทอดของการโอน จนกว่าคนต่างด้าวจะได้โอนหุ้นที่ตนถือดังกล่าวให้แก่บุคคลที่มีสัญชาติไทย</p>
                        </div>
                        <div class="col-govern-50 col-govern-right">
                            <img src="images/investor/inv_shareholder_4.jpg" alt="" class="">
                        </div>
                    </div>
                </div>

                <div id="corpBlock5" class="corp-block">
                    <div>
                        <div class="col-govern-50">
                            <h3 class="title-gov">จำนวนหุ้นที่จดทะเบียนและชำระแล้ว</h3>
                            <p class="title-share"><strong>ทุนจดทะเบียน ณ วันที่ 29 สิงหาคม 2559</strong></p>
                            <p class="txt-grey">หุ้นสามัญ 12,031,105,828 หุ้น มูลค่า 12,031,105,828 บาท</p>

                            <p class="title-share"><strong>ทุนที่ออกและชำระแล้ว ณ วันที่ 29 สิงหาคม 2559</strong></p>
                            <p class="txt-grey">หุ้นสามัญ 11,779,071,429 หุ้น มูลค่า 11,779,071,429 บาท</p>

                            <p class="title-share"><strong>อัตราส่วนการถือหุ้นของคนต่างด้าว</strong></p>
                            <p class="txt-grey">ณ วันที่ 29 สิงหาคม 2559 เท่ากับ 30.00%</p>
                        </div>
                        <div class="col-govern-50 col-govern-right">
                            <img src="images/investor/inv_shareholder_5.jpg" alt="" class="">
                        </div>
                    </div>
                </div>

                <div id="corpBlock6" class="corp-block">
                    <div>
                        <h3 class="title-gov">รายงานการประชุมผู้ถือหุ้น</h3>
                        <p class="invest-p-txt">คุณสามารถคลิกเพื่ออ่านหนังสือเชิญประชุมผู้ถือหุ้นครั้งล่าสุดในรูปแบบไฟล์ PDF ได้ที่นี่ค่ะ</p>
                        <div class="inv-report-block">

                            <div class="inv-report-list list-first">
                                <div class="ic-report"></div>
                                <div class="inv-report-detail">
                                    <a href="">ครั้งที่ 1 / 2559<br>
                                        26 เมษายน 2559<br>
                                        <span>รายงานการประชุมใหญ่<br>สามัญผู้ถือหุ้น</span> <i class="download"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="inv-report-list">
                                <div class="ic-report"></div>
                                <div class="inv-report-detail">
                                    <a href="">ครั้งที่ 1 / 2558<br>
                                        23 เมษายน 2558<br>
                                        <span>รายงานการประชุมใหญ่<br>สามัญผู้ถือหุ้น</span> <i class="download"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="inv-report-list">
                                <div class="ic-report"></div>
                                <div class="inv-report-detail">
                                    <a href="">ครั้งที่ 1 / 2557<br>
                                        24 เมษายน 2557<br>
                                        <span>รายงานการประชุมใหญ่<br>สามัญผู้ถือหุ้น</span> <i class="download"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="inv-report-list">
                                <div class="ic-report"></div>
                                <div class="inv-report-detail">
                                    <a href="">ครั้งที่ 1 / 2556<br>
                                        23 เมษายน 2556<br>
                                        <span>รายงานการประชุมใหญ่<br>สามัญผู้ถือหุ้น</span> <i class="download"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="inv-report-list">
                                <div class="ic-report"></div>
                                <div class="inv-report-detail">
                                    <a href="">ครั้งที่ 1 / 2555<br>
                                        26 เมษายน 2555<br>
                                        <span>รายงานการประชุมใหญ่<br>สามัญผู้ถือหุ้น</span> <i class="download"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                        </div>
                    </div>
                </div>

                <div id="corpBlock7" class="corp-block">
                    <div>
                        <h3 class="title-gov">การจ่ายเงินปันผล</h3>
                        <table class="table-historical-price table-shareholder" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th width="20%" style="text-align: center; vertical-align: middle;">วันที่คณะกรรมการมีมติ</th>
                                <th width="20%" style="text-align: center; vertical-align: middle;">วันที่ขึ้นเครื่องหมาย</th>
                                <th width="20%" style="text-align: center; vertical-align: middle;">วันที่จ่ายเงินปันผล</th>
                                <th width="20%" style=" vertical-align: middle;">เงินปันผล (บาท/หุ้น)</th>
                                <th style="text-align: center; vertical-align: middle;">งวดดำเนินงาน</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="even">
                                <td style="text-align: center;">11/08/16</td>
                                <td style="text-align: center;">24/08/16</td>
                                <td style="text-align: center;">09/09/16</td>
                                <td style="text-align: center;">0.35</td>
                                <td style="text-align: center;">01/01/16 - 30/06/16</td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">29/02/16</td>
                                <td style="text-align: center;">29/04/16</td>
                                <td style="text-align: center;">24/05/16</td>
                                <td style="text-align: center;">0.35</td>
                                <td style="text-align: center;">01/07/15 - 31/12/15</td>
                            </tr>
                            <tr class="even">
                                <td style="text-align: center;">13/08/15</td>
                                <td style="text-align: center;">26/08/15</td>
                                <td style="text-align: center;">10/09/15</td>
                                <td style="text-align: center;">0.25</td>
                                <td style="text-align: center;">01/01/15 - 30/06/15</td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">24/02/15</td>
                                <td style="text-align: center;">29/04/15</td>
                                <td style="text-align: center;">22/05/15</td>
                                <td style="text-align: center;">0.40</td>
                                <td style="text-align: center;">01/07/14 - 31/12/14 </td>
                            </tr>
                            <tr class="even">
                                <td style="text-align: center;">14/08/14</td>
                                <td style="text-align: center;">27/08/14</td>
                                <td style="text-align: center;">12/09/14</td>
                                <td style="text-align: center;">0.25</td>
                                <td style="text-align: center;">01/01/14 - 30/06/14</td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">27/02/14</td>
                                <td style="text-align: center;">29/04/14</td>
                                <td style="text-align: center;">22/05/14</td>
                                <td style="text-align: center;">0.15</td>
                                <td style="text-align: center;">-</td>
                            </tr>
                            <tr class="even">
                                <td style="text-align: center;">13/08/13</td>
                                <td style="text-align: center;">26/08/13</td>
                                <td style="text-align: center;">12/09/13</td>
                                <td style="text-align: center;">0.25</td>
                                <td style="text-align: center;">01/01/13 - 30/06/13</td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">28/01/13</td>
                                <td style="text-align: center;">26/04/13</td>
                                <td style="text-align: center;">22/05/13</td>
                                <td style="text-align: center;">0.15</td>
                                <td style="text-align: center;">01/10/12 - 31/12/12</td>
                            </tr>
                            <tr class="even">
                                <td style="text-align: center;">13/11/12</td>
                                <td style="text-align: center;">23/11/12</td>
                                <td style="text-align: center;">12/12/12</td>
                                <td style="text-align: center;">0.10</td>
                                <td style="text-align: center;">01/07/12 - 30/09/12</td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">14/08/12</td>
                                <td style="text-align: center;">24/08/12</td>
                                <td style="text-align: center;">12/09/12</td>
                                <td style="text-align: center;">0.20</td>
                                <td style="text-align: center;">01/01/12 - 30/06/12</td>
                            </tr>
                            <tr class="even">
                                <td style="text-align: center;">22/03/12</td>
                                <td style="text-align: center;">02/05/12</td>
                                <td style="text-align: center;">18/05/12</td>
                                <td style="text-align: center;">0.23</td>
                                <td style="text-align: center;">01/07/11 - 31/12/11</td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">10/08/11</td>
                                <td style="text-align: center;">23/08/11</td>
                                <td style="text-align: center;">09/09/11</td>
                                <td style="text-align: center;">0.17</td>
                                <td style="text-align: center;">01/01/11 - 30/06/11</td>
                            </tr>
                            <tr class="even">
                                <td style="text-align: center;">22/03/11</td>
                                <td style="text-align: center;">29/04/11</td>
                                <td style="text-align: center;">19/05/11</td>
                                <td style="text-align: center;">0.18</td>
                                <td style="text-align: center;">01/07/10 - 31/12/10</td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">11/08/10</td>
                                <td style="text-align: center;">24/08/10</td>
                                <td style="text-align: center;">10/09/10</td>
                                <td style="text-align: center;">0.16</td>
                                <td style="text-align: center;">01/01/10 - 30/06/10</td>
                            </tr>
                            <tr class="even">
                                <td style="text-align: center;">24/03/10</td>
                                <td style="text-align: center;">06/05/10</td>
                                <td style="text-align: center;">25/05/10</td>
                                <td style="text-align: center;">0.10</td>
                                <td style="text-align: center;">01/10/09 - 31/12/09</td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">10/11/09</td>
                                <td style="text-align: center;">23/11/09</td>
                                <td style="text-align: center;">08/12/09</td>
                                <td style="text-align: center;">0.08</td>
                                <td style="text-align: center;">01/07/09 - 30/09/09</td>
                            </tr>
                            <tr class="even">
                                <td style="text-align: center;">11/08/09</td>
                                <td style="text-align: center;">21/08/09</td>
                                <td style="text-align: center;">11/09/09</td>
                                <td style="text-align: center;">0.16</td>
                                <td style="text-align: center;">01/01/09 - 30/06/09</td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">23/03/09</td>
                                <td style="text-align: center;">04/05/09</td>
                                <td style="text-align: center;">26/05/09</td>
                                <td style="text-align: center;">0.12</td>
                                <td style="text-align: center;">01/07/08 - 31/12/08</td>
                            </tr>
                            <tr class="even">
                                <td style="text-align: center;">08/08/08</td>
                                <td style="text-align: center;">19/08/08</td>
                                <td style="text-align: center;">04/09/08</td>
                                <td style="text-align: center;">0.15</td>
                                <td style="text-align: center;">01/01/08 - 30/06/08</td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">24/03/08</td>
                                <td style="text-align: center;">06/05/08</td>
                                <td style="text-align: center;">26/05/08</td>
                                <td style="text-align: center;">0.25</td>
                                <td style="text-align: center;">01/06/07 - 31/12/07</td>
                            </tr>
                            </tbody></table>
                    </div>
                </div>

                <div id="corpBlock8" class="corp-block">
                    <div>
                        <h3 class="title-gov">ปฏิทินนักลงทุน</h3>

                        <div id="tabs-container" class="tabs-container">
                            <div class="tab-dd dropdown invest-calendar">
                                <ul class="tabs-menu">
                                    <li class="item current">
                                        <a href="#tab-1">
                                            ประกาศงบการเงิน
                                        </a>
                                    </li>
                                    <li class="item">
                                        <a href="#tab-2">
                                            แถลงข่าว
                                        </a>
                                    </li>
                                    <li class="item">
                                        <a href="#tab-3">
                                            ประชุมนักวิเคราะห์
                                        </a>
                                    </li>
                                    <li class="item">
                                        <a href="#tab-4">
                                            ประชุมใหญ่สามัญผู้ถือหุ้น
                                        </a>
                                    </li>
                                </ul>
                                <div class="line-tab"></div>
                            </div>

                            <div class="tab">
                                <div id="tab-1" class="tab-content">
                                    <div class="tab-block">
                                        <table class="table-shareholder3" cellpadding="0" cellspacing="0" width="100%">
                                            <thead>
                                                <tr class="">
                                                    <th width="50%">วันที่</th>
                                                    <th width="50%">งวดดำเนินงาน</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="">
                                                    <td>อ. 28 กุมภาพันธ์ 2560</td>
                                                    <td>ไตรมาส 4 ปี 2559 </td>
                                                </tr>
                                                <tr class="">
                                                    <td>พฤ. 11 พฤษภาคม 2560</td>
                                                    <td>ไตรมาส 1 ปี 2560</td>
                                                </tr>
                                                <tr class="">
                                                    <td>ศ. 11 สิงหาคม 2560</td>
                                                    <td>ไตรมาส 2 ปี 2560</td>
                                                </tr>
                                                <tr class="">
                                                    <td>จ. 13 พฤศจิกายน 2560</td>
                                                    <td>ไตรมาส 3 ปี 2560</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div id="tab-2" class="tab-content">
                                    <div class="tab-block">
                                        <table class="table-shareholder3" cellpadding="0" cellspacing="0" width="100%">
                                            <thead>
                                            <tr class="">
                                                <th width="38%">วันที่</th>
                                                <th width="62%">สถานที่</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr class="">
                                                <td>อ. 17 มกราคม 2560  เวลา 10.30 น.</td>
                                                <td>แกรนด์ เซ็นเตอร์ พอยท์ โฮเทล แอนด์ เรสซิเดนซ์ - เทอร์มินอล 21</td>
                                            </tr>
                                            </tbody></table>
                                    </div>
                                </div>
                                <div id="tab-3" class="tab-content">
                                    <div class="tab-block">
                                        <table class="table-shareholder3" cellpadding="0" cellspacing="0" width="100%">
                                            <thead>
                                            <tr class="">
                                                <th width="38%">วันที่</th>
                                                <th width="28%">งวดดำเนินงาน</th>
                                                <th width="42%">สถานที่</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr class="">
                                                <td class="tb-left">อ. 17 มกราคม 2560 เวลา 16.00 น</td>
                                                <td>แผนงานประจำปี 2560 </td>
                                                <td>อาคารคิวเฮ้าส์ ลุมพินี สาทร ชั้น 37 </td>
                                            </tr>
                                            <tr class="">
                                                <td class="tb-left">พ. 1 มีนาคม 2560</td>
                                                <td>ไตรมาส 4 ปี 2559</td>
                                                <td>อาคารคิวเฮ้าส์ ลุมพินี สาทร ชั้น 37 </td>
                                            </tr>
                                            <tr class="">
                                                <td class="tb-left">ศ. 12 พฤษภาคม 2560</td>
                                                <td>ไตรมาส 1 ปี 2560</td>
                                                <td>อาคารคิวเฮ้าส์ ลุมพินี สาทร ชั้น 37 </td>
                                            </tr>
                                            <tr class="">
                                                <td class="tb-left">อ. 15 สิงหาคม 2560</td>
                                                <td>ไตรมาส 2 ปี  2560</td>
                                                <td>อาคารคิวเฮ้าส์ ลุมพินี สาทร ชั้น 37 </td>
                                            </tr>
                                            <tr class="">
                                                <td class="tb-left">อ. 14 พฤศจิกายน 2560</td>
                                                <td>ไตรมาส 3 ปี  2560</td>
                                                <td>อาคารคิวเฮ้าส์ ลุมพินี สาทร ชั้น 37 </td>
                                            </tr>
                                            </tbody></table>
                                    </div>
                                </div>
                                <div id="tab-4" class="tab-content">
                                    <div class="tab-block">
                                        <table class="table-shareholder3" cellpadding="0" cellspacing="0" width="100%">
                                            <thead>
                                            <tr class="toprow">
                                                <th width="38%">วันที่</th>
                                                <th width="62%">สถานที่</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr class="">
                                                <td>พฤ. 27 เมษายน 2560 เวลา 13.30 น</td>
                                                <td>อาคารคิวเฮ้าส์ ลุมพินี สาทร ชั้น 4 </td>
                                            </tr>
                                            </tbody></table>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>


    <?php include('footer.php'); ?>
