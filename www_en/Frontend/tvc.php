<?php
header('Location: //'.$_SERVER['SERVER_NAME'].'/'.'en');
exit();

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

$tvcs = getTVC();
$page = 'tvc';
$page_index = 0;
$img = '';
?>
<?php
include('header.php');
//elementMetaTitleDisTag($page);
?>
<!-- CSS -->
<link rel="stylesheet" href="<?= file_path('css/tvc.css') ?>" type="text/css">
<link rel="stylesheet" href="<?= file_path('css/video.css') ?>" type="text/css">
<link rel="stylesheet" href="<?= file_path('css/lib/lity.min.css') ?>" type="text/css">
<!-- JS -->
<script src="<?= file_path('js/tvc.js') ?>"></script>
<script src="<?= file_path('js/lib/lity.min.js') ?>"></script>
<div id="content" class="content">
    <div class="container">

        
        <?php
        $SEO = getSEOUrl($actual_link);
        if($actual_link == @$SEO->url_page){
            echo '<h1 class="heading-title">'.$SEO->h1.'<h1>';
        }else{
         echo '<h1 class="heading-title">LH TVC</h1>'; 
     }
     ?>

        <?php
            $tvc_first = !empty($tvcs[0]) ? $tvcs[0] : null;
            $tvc_first_url = '';
            $youtube_first_img = '';
            if(!empty($tvc_first)){
                if($tvc_first->tvc_type == 'youtube'){
                    $tvc_first_url =  str_replace('watch?v=', 'embed/', $tvc_first->tvc_youtube_link);
                }elseif($tvc_first->tvc_type == 'vdo'){
                    $tvc_first_url =  backend_url('base',$tvc_first->tvc_youtube_link);
                }
                $youtube_first_img = backend_url('base',$tvc_first->tvc_thumbnail);
            }

            array_shift($tvcs);

        ?>

        <?php
            if(!empty($tvc_first)){
                ?>
                <div class="tpl1-block-1">
                    <div class="tpl1-block-item">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="tpl1-block-item-img">

                                    <?php
                                    if($tvc_first->tvc_type == 'youtube'){
                                        ?>
                                        <a href="<?=str_replace('?rel=0','&rel=0',$tvc_first_url);?>" data-lity>
                                            <span class="i-view-vdo"></span>
                                            <img src="<?= $youtube_first_img ?>" alt="" class="vdo-lg-cover">
                                        </a>
                                        <!-- <iframe class="lightbox" src="<?= $tvc_first_url ?>" width="1000" height="560" id="lbVdo1" style="border:none;" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> -->
                                        <?php
                                    }elseif($tvc_first->tvc_type == 'vdo'){
                                        ?>
                                        <!-- <a id="ban_vdo_first">
                                            <span class="i-view-vdo"></span>
                                            <img src="<?= $youtube_first_img ?>" class="vdo-lg-cover">
                                        </a> -->
                                        <a id="ban_vdo_first" href="<?=$tvc_first_url;?>" data-lity>
                                            <span class="i-view-vdo"></span>
                                            <img src="<?= $youtube_first_img ?>" alt="" class="vdo-lg-cover">
                                        </a>
                                        <script>
                                            // $(document).ready(function () {
                                            //     $('#ban_vdo_first').click(function () {
                                            //         $('#lbVdo_first').show();
                                            //         $.featherlight($('#lbVdo_first'),{});
                                            //         $('.featherlight-content #video_player_first')[0].play();
                                            //         $('#lbVdo_first').hide();
                                            //     });
                                            // })
                                        </script>
                                        <!-- <div id="lbVdo_first" style="position:relative;width: 100%;display: none;">
                                            <video id='video_player_first' preload='none' controls>
                                                <source src="<?= $tvc_first_url ?>" type="video/mp4">
                                            </video>
                                        </div> -->
                                    <?php
                                    }
                                    ?>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="tpl1-block-descrp">
                                    <p class="title green">“<?= $tvc_first->tvc_name ?>”</p>
                                    <p class="title"><?= $tvc_first->tvc_detail ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        <?php
            }
        ?>

        <?php
            if(count($tvcs) >= 1){
                ?>
                <div id="tpl1Block2List" class="tpl1-block-2">
                    <div class="row">
                        <div id="block2List"  class="col-slide">
                            <?php foreach ($tvcs as $i => $tvc) {
                                $i += 2;
                                $tvc_url = '';
                                $youtube_img = '';
                                if(!empty($tvc)){
                                    $youtube_img = backend_url('base',$tvc->tvc_thumbnail);
                                    if($tvc->tvc_type == 'youtube'){
                                        $tvc_url =  str_replace('watch?v=', 'embed/', $tvc->tvc_youtube_link);
                                    }elseif($tvc->tvc_type == 'vdo'){
                                        $tvc_url =  backend_url('base',$tvc->tvc_youtube_link);
                                    }
                                }
                                ?>
                                <div class="item">
                                    <div class="hm-tip">
                                        <a href="">
                                            <div class="review-img">

                                                <?php
                                                if($tvc->tvc_type == 'youtube'){
                                                    ?>
                                                    <!-- <a href="#" data-featherlight="#lbVdo<?= $i ?>">
                                                        <span class="i-view-vdo"></span>
                                                        <img src="<?= $youtube_img ?>" alt="">
                                                    </a> -->
                                                    <a href="<?=str_replace('?rel=0','&rel=0',$tvc_url);?>" data-lity>
                                                        <span class="i-view-vdo"></span>
                                                        <img src="<?= $youtube_img ?>" alt="" >
                                                    </a>
                                                    <!-- <iframe class="lightbox" src="<?= $tvc_url ?>" width="1000" height="560" id="lbVdo<?= $i ?>" style="border:none;" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> -->
                                                <?php
                                                }elseif($tvc->tvc_type == 'vdo'){
                                                ?>
                                                    <!-- <a id="ban_vdo<?= $i ?>">
                                                        <span class="i-view-vdo"></span>
                                                        <img src="<?= $youtube_img ?>">
                                                    </a> -->
                                                    <a href="<?=$tvc_url;?>" data-lity>
                                                        <span class="i-view-vdo"></span>
                                                        <img src="<?= $youtube_img ?>" alt="" >
                                                    </a>
                                                    <script>
                                                        // $(document).ready(function () {
                                                        //     $('#ban_vdo<?= $i ?>').click(function () {
                                                        //         $('#lbVdo<?= $i ?>').show();
                                                        //         $.featherlight($('#lbVdo<?= $i ?>'),{});
                                                        //         $('.featherlight-content #video_player<?= $i ?>')[0].play();
                                                        //         $('#lbVdo<?= $i ?>').hide();
                                                        //     });
                                                        // })
                                                    </script>
                                                    <!-- <div id="lbVdo<?= $i ?>" style="position:relative;width: 100%;display: none;">
                                                        <video id='video_player<?= $i ?>' preload='none' controls>
                                                            <source src="<?= $tvc_url ?>" type="video/mp4">
                                                        </video>
                                                    </div> -->

                                                    <?php
                                                }
                                                ?>


                                            </div>
                                            <p class="title green">“<?= $tvc->tvc_name ?>”</p>
                                            <p class="title"><?= $tvc->tvc_detail ?></p>
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
        <?php
            }
        ?>

    </div>
</div>

<?php include('footer.php'); ?>
