<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>

<?php include('header.php'); ?>
<!-- CSS -->
<link rel="stylesheet" href="<?= file_path('css/corporate-all.css') ?>" type="text/css">
<link rel="stylesheet" href="<?= file_path('css/corporate-menu.css') ?>" type="text/css">
<link rel="stylesheet" href="<?= file_path('css/smartcal-howto.css') ?>" type="text/css">

<!-- JS -->
<script src="<?= file_path('js/corporate-menu.js') ?>"></script>

<div id="content" class="content ethic-page smartcal-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">

                <h1>วิธีการใช้งาน</h1>
                <div id="corpMenu" class="corp-menu-tab">
                    <ul>
                        <li><a href="" onclick="link();">คำนวณยอดเงินที่ต้องผ่อนชำระต่อเดือน</a></li>
                        <li class="current"><a href="" onclick="link_in();">วิธีการใช้งาน</a></li>
                    </ul>
                </div>

                <div id="corpBlock2" class="corp-block current">
                    <div>
                        <div class="col-govern-50">
                            <h2 class="title-gov">วิธีการใช้งาน</h2>
                            <p>1. ใส่ยอดกู้ที่กู้จากธนาคารในช่องวงเงินกู้สีแดง ยอดเงินผ่อนขั้นต่ำต่อเดือน
                                จะปรากฏขึ้น สามารถแก้ไขจำนวนปีที่ผ่อนได้(ตั้งไว้ 30 ปี แต่แก้ไขได้ครับ)<br>
                                2.ใส่อัตราดอกเบี้ยที่ได้ในช่องอัตราดอกเบี้ย
                                <ul class="smartcal">
                                    <li>- ใส่อัตราดอกเบี้ยตามจริงที่ได้จากโปร เช่นได้ดอกพิเศษ 12 เดือน ก็ไล่ใส่
                                    ไป 12 งวด งวดที่ 13-360 ก็ใส่ดอก MLR ตามโปรได้เลยครับ</li>
                                    <li>- หากมีการ Re-finance ก็ให้ใส่ดอกที่ปรับแล้วตามจริง ตามงวดนั้นๆได้
                                    เลย</li>
                                </ul>
                                3.ใส่ยอดผ่อนแต่ละเดือน ในช่องค่าผ่อนชำระ โดยแนะนำให้ใส่ยอดขั้นต่ำที่ผ่อน
                                ไปก่อนยาวไป 360 ช่องเลยครับ (ใช้ coppy - past) เพื่อจะได้เห็นว่า ถ้าเรา
                                ผ่อนขั้นต่ำเท่านี้ จะใช้เวลากี่ปีหมด เวลาผ่อนจริง เราผ่อนงวดไหนเท่าไหร่
                                หรือโปะเท่าไหร่ ก็ใส่ไปตามจริงเลย โปรแกรมจะเปลี่ยนแปลงยอดให้เลยครับ<br>
                                4. จะดูว่าเมื่อไหร่ผ่อนหมด ให้ดูที่ช่อง"เงินต้นคงเหลือ" หากเป็นค่าติดลบ
                                (ตัวเลขจะเปลี่ยนเป็นสีแดง) แสดงว่าผ่อนหมดแล้วที่งวดนั้นๆครับ
                            </p>
                            <br>
                            <p><strong>
                                * ยอดผ่อนขั้นต่ำต่อเดือนจากโปรแกรมนี้จะน้อยกว่าที่ธนาคารกำหนด
                                เล็กน้อยครับ เพราะธนาคารจะใส่ค่าความเสี่ยงลงไปด้วย ดังนั้นให้ยึด
                                งวดผ่อนที่ธนาคารกำหนดจะตรงกว่า
                                </strong>
                            </p>

                        </div>
                        <div class="col-govern-50 col-govern-right">
                            <img src="<?= file_path('images/temp6/smart_1.jpg') ?>" alt="" class="content-img">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <a href="javascript:history.back()" class="link-back">< ย้อนกลับ</a>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript"> function link(){ window.location = 'calculator'; } </script>
<script type="text/javascript"> function link_in(){ window.location = 'smart-cal-howto'; } </script>



<?php include('footer.php'); ?>
