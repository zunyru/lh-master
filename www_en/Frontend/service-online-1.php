<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>

<?php include('header.php'); ?>
<!-- CSS -->
<link rel="stylesheet" href="css/service.css" type="text/css">

<div id="content" class="content service-page">
    <div class="container">
        <div class="row">
            <div class="col-md-11 col-md-offset-1">
                <h1 class="heading-title">Home</h1>
                <div class="customer-info">
                    <p class="service-title">ข้อมูลลูกค้า</p>

                    <div class="row">
                        <div class="col-md-4">
                            <p><span class="service-label">ชื่อ-สกุล ลูกค้า</span> : นายธนะดิฐ บุญประสิทธิ์</p>
                            <p><span class="service-label">โทรศัพท์ติดต่อ</span> : 085-099-6566, 02-890-6090</p>
                            <p><span class="service-label">Email</span> : tana456789@gmail.com</p>
                        </div>
                    </div>
                </div>

                <div class="customer-info">
                    <p class="service-title">รายละเอียดบ้าน</p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="">
                                <p>
                                    <span class="service-detail-label">หลังที่</span>: 1<br>
                                    <span class="service-detail-label">บ้านหลังที่</span>: 176/33<br>
                                    <span class="service-detail-label">แบบบ้าน</span>: คาลลา ลิลลี่<br>
                                    <span class="service-detail-label">สถานะของงานรับประกัน 1 ปี</span>: หมดประกัน <a href="" class="customer-view-btn">VIEW</a><br>
                                    <span class="service-detail-label">สถานะของงานรับประกัน 5 ปี</span>: หมดประกัน<br>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="">
                                <p>
                                    <span class="service-detail-label">โครงการ</span>: นันทวัน - เชียงใหม่<br>
                                    <span class="service-detail-label">แปลง</span>: 06A03<br>
                                    <span class="service-detail-label">วันที่โอน</span>: 30/12/2553<br>
                                    <span class="service-detail-label">พื้นที่บ้าน</span>: 109.90 ตร.ว.<br>
                                    <span class="service-detail-label">อัตราจัดเก็บค่าบริการสาธารณะ</span>: บาท / ต่อหลัง สิ้นสุด ณ วันที่<br>
                                </p>

                            </div>
                        </div>
                    </div>
                    <!--                        <a href="" class="btn btn-submit">ดูประวัติการแจ้งซ่อมทั้งหมด</a>-->

                    <div class="service-history-block">
                        <table class="tb-history tb-history-rps" width="100%">
                            <thead>
                            <tr>
                                <th>โครงการ</th>
                                <th>บ้านเลขที่</th>
                                <th>วันที่แจ้งซ่อม</th>
                                <th>วันที่นัดตรวจ</th>
                                <th class="tb-dst">สถานะ</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="line-rps">
                                <td>นันทวัน - เชียงใหม่</td>
                                <td>176/33</td>
                                <td>9/02/2016 11:38 น.</td>
                                <td>รอนัดหมาย</td>
                                <td class="tb-dst">ยกเลิกรายการแล้ว</td>
                            </tr>
                            <tr class="rps-row">
                                <td colspan="5" class="tb-rps">สถานะ: ยกเลิกรายการแล้ว</td>
                            </tr>

                            <tr class="line-rps">
                                <td>นันทวัน - เชียงใหม่</td>
                                <td>176/33</td>
                                <td>9/02/2016 11:38 น.</td>
                                <td>จันทร์ 16/02/2017 09;30 น.</td>
                                <td class="tb-dst">บันทึกข้อมูลเรียบร้อยแล้ว</td>
                            </tr>
                            <tr class="rps-row">
                                <td colspan="5" class="tb-rps">สถานะ: บันทึกข้อมูลเรียบร้อยแล้ว</td>
                            </tr>

                            <tr class="line-rps">
                                <td>นันทวัน - เชียงใหม่</td>
                                <td>176/33</td>
                                <td>9/02/2016 11:38 น.</td>
                                <td>รอนัดหมาย</td>
                                <td class="tb-dst">ยกเลิกรายการแล้ว</td>
                            </tr>
                            <tr class="rps-row">
                                <td colspan="5" class="tb-rps">สถานะ: ยกเลิกรายการแล้ว</td>
                            </tr>

                            <tr class="line-rps">
                                <td>นันทวัน - เชียงใหม่</td>
                                <td>176/33</td>
                                <td>9/02/2016 11:38 น.</td>
                                <td>รอนัดหมาย</td>
                                <td class="tb-dst">ยกเลิกรายการแล้ว</td>
                            </tr>
                            <tr class="rps-row">
                                <td colspan="5" class="tb-rps">สถานะ: ยกเลิกรายการแล้ว</td>
                            </tr>

                            <tr class="line-rps">
                                <td>นันทวัน - เชียงใหม่</td>
                                <td>176/33</td>
                                <td>9/02/2016 11:38 น.</td>
                                <td>จันทร์ 16/02/2017 09;30 น.</td>
                                <td class="tb-dst">บันทึกข้อมูลเรียบร้อยแล้ว</td>
                            </tr>
                            <tr class="rps-row">
                                <td colspan="5" class="tb-rps">สถานะ: บันทึกข้อมูลเรียบร้อยแล้ว</td>
                            </tr>

                            <tr class="line-rps">
                                <td>นันทวัน - เชียงใหม่</td>
                                <td>176/33</td>
                                <td>9/02/2016 11:38 น.</td>
                                <td>รอนัดหมาย</td>
                                <td class="tb-dst">ยกเลิกรายการแล้ว</td>
                            </tr>
                            <tr class="rps-row">
                                <td colspan="5" class="tb-rps">สถานะ: ยกเลิกรายการแล้ว</td>
                            </tr>

                            <tr class="line-rps">
                                <td>นันทวัน - เชียงใหม่</td>
                                <td>176/33</td>
                                <td>9/02/2016 11:38 น.</td>
                                <td>จันทร์ 16/02/2017 09;30 น.</td>
                                <td class="tb-dst">บันทึกข้อมูลเรียบร้อยแล้ว</td>
                            </tr>
                            <tr class="rps-row">
                                <td colspan="5" class="tb-rps">สถานะ: บันทึกข้อมูลเรียบร้อยแล้ว</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
<!--                <a href="" class="step-back">< Back</a>-->

            </div>
        </div>


    </div>
</div>


<?php include('footer.php'); ?>
