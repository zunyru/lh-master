<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

$highlight_id = $_GET['highlight_id'];

if(empty($highlight_id) || getHighlight($highlight_id) == false){
    echo 'not have a project for this project id';
}

$highlight = getHighlight($highlight_id);

?>
<?php include('header.php'); ?>

<!-- CSS -->
<link rel="stylesheet" href="css/review-details.css" type="text/css">
<!-- JS -->
<script src="js/review-details.js"></script>


<div class="page-nav">
    <div class="container">
        <!--            <a href="" class="back-to-prev"><i></i>LH REVIEW</a>-->
        <h1>LH Highlight</h1>
        <p class="title dsktp"><?= $highlight->highlights_name_th ?></p>
    </div>
</div>

<div class="page-banner">
    <div class="page-banner-content" style="background-image: url(<?= backend_url('base',$highlight->highlights_lead_img)?>);"></div>
</div>

<div id="content" class="content">
    <div class="container">

        <div class="review-content">

            <div class="page-nav rps">
                <p class="title"><?= $highlight->highlights_name_th ?></p>
            </div>

            <div class="content-block">
                <?= $highlight->highlights_content ?>
            </div>
<!--            <div class="content-block">-->
<!--                <div class="row">-->
<!--                    <div class="col-md-6 pull-right">-->
<!--                        <div class="content-img">-->
<!--                            <img src="images/temp2/review_3.jpg" alt="">-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-md-6">-->
<!--                        //= $highlight->highlights_content  -->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
        </div>

    </div>
</div>

<?php include('footer.php'); ?>
