$(document).ready(function () {
    //Page Load Start
    historyLists();
    yearHistory();
    //Page Load End
});


//Function Start

function historyLists() {
    $('#historyLists').owlCarousel({
        loop:false,
        autoplay: false,
        autoplayTimeout: 4000,
        autoplaySpeed: 1000,
        margin:5,
        // animateOut: 'fadeOut',
        nav:true,
        dots: false,
        lazyLoad:true,
        slideBy: 1,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:1
            },
            979:{
                items:2
            }


        }
    });
}

function yearHistory() {
    $('.history-block .item a').click(function(e) {
        $('.history-block .item a .htry-select').removeClass('active');
        e.preventDefault();
        $(this).children().addClass('active');
        // $(this).siblings().removeClass('active');
        var tab = $(this).attr('href');
        $('.year-details').not(tab).css('display', 'none');
        $(tab).fadeIn();
    });

}

//Function End