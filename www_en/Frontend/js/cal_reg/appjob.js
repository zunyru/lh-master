var app = angular.module('JobApp', ['ngSanitize']);

var path = window.location.origin 

app.controller('newsDetailController', function($scope,$http){
  var news_detail = path+"/www/build/api/service/register_be.php?n=open_news&id="+location.search.split('id=')[1];
  console.log(news_detail);

  $http.get(news_detail).success(function(response){
        $scope.news_detail = response[0];
    })

  $scope.dateFormat = function (d){

      var now = new Date(d);
      //console.log(now);
      var thmonth = new Array ("ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.", "ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");

      return now.getDate()+ " " + thmonth[now.getMonth()]+ " " + (now.getFullYear());
  }

  $scope.path_img_new = function (d){
      var path_img = path+ "/www/Backend/fileupload/news/";
      return path_img+d;
  }

});

app.controller('myController', function($scope,$http,$timeout){

	var url = path+"/www/build/api/service/register_be.php?n=open_register";
  var job = path+"/www/build/api/service/register_be.php?n=position";
  var province = path+"/www/build/api/service/register_be.php?n=prvinces";
  var area = path+"/www/build/api/service/register_be.php?n=location";
  var news = path+"/www/build/api/service/register_be.php?n=open_news";
  var opt_year  = path+"/www/build/api/service/register_be.php?n=open_optionnews";
  var profile_url = path+"/www/build/api/service/register_be.php?n=candidacyAdd";
  var language_url = path+"/www/build/api/service/register_be.php?n=candidacyAdd_language";
  var education_url = path+"/www/build/api/service/register_be.php?n=candidacyAdd_education";
  var family_url = path+"/www/build/api/service/register_be.php?n=candidacyAdd_family";
  var job_url = path+"/www/build/api/service/register_be.php?n=candidacyAdd_job";

    
  
    $scope.data = {

      txbRaceOther : '',
      txbConscription : '',
      txbReligionOther : '',
      jobcmbBirthProvince : '',
      jobcmbTitle : '',
      jobtxbFName : '',
      jobtxbLName : '',
      jobtxbNickname : '',
      jobrdoGender : 'M',
      jobtxbHeight : '0',
      jobtxbWeight : '0',
      jobcmbReligion : '',
      jobrdoRace : 'ไทย',
      jobrdoNationality : 'ไทย',
      jobcmbConscription : '',
      jobrdoMarried : 'โสด',
      jobtxbChildNum : '0',
      jobtxbTypingThai : '0',
      jobtxbTypingEng : '0',
      jobcmbContactProvince : '',
      jobtxbAddress : '',
      jobtxbContactTel : '',
      jobtxbContactEmail : '',
      jobtxbEmergencyPerson : '',
      jobtxbEmergencyTel : '',
      jobtxbInterestPosition : '',
      jobtxbExpectSalary : '0',
      jobtxbReferenceName : '',
      jobtxbReferenceCompany : '',
      jobtxbReferenceRelation : '',
      jobtxbReferenceTel : '',
      jobtxbReferencePosition :'',
      jobrdoWorkSite : 'Y',
      jobrdoWorkSiteFew : 'Y',
      jobrdoHasDisease : 'N',
      jobrdoHasAccuse : 'N',
      jobrdoHasBankrupt : 'N',
      jobrdoHasLayoff : 'N',
      jobtxbFurtherInfo : '',
      jobtxbIDNo : '',
      jobrdoJustGraduate : 'ใช่',
      jobrdoInterestPositionType : 'F',
      jobtxbFatherName : '',
      jobtxbFatherAge : '0',
      jobtxbFatherOcc : '',
      jobtxbFatherAddr : '',
      jobtxbMotherName : '',
      jobtxbMotherAge : '0',
      jobtxbMotherOcc : '',
      jobtxbMotherAddr : '',
      jobtxbCousin1Name : '',
      jobtxbCousin1Age : '0',
      jobtxbCousin1Occ : '',
      jobtxbCousin1Addr : '',
      jobtxbCousin2Name : '',
      jobtxbCousin2Age : '0',
      jobtxbCousin2Occ : '',
      jobtxbCousin2Addr : '',
      jobtxbCousin3Name : '',
      jobtxbCousin3Age : '0',
      jobtxbCousin3Occ : '',
      jobtxbCousin3Addr : '',
      jobtxbSpouseName : '',
      jobtxbSpouseAge : '0',
      jobtxbSpouseOcc : '',
      jobtxbSpouseAddr : '',
      jobtxbAddress : '',
      jobcmbContactProvince : '',
      jobtxbContactTel : '',
      jobtxbContactEmail : '',
      jobtxbEmergencyPerson : '',
      jobtxbEmergencyTel : '',
      jobcmbHighestGraduate : '',
      jobcmbStudyFrom1 : '',
      jobcmbStudyTo1 : '',
      jobtxbGraduateInstitute1 : '',
      jobtxbGraduateFaculty1 : '',
      jobtxbGraduateField1 : '',
      jobcmbGraduateGraduate1 : '',
      jobtxbGraduatePoint1 : '0',
      jobcmbStudyFrom2 : '',
      jobcmbStudyTo2 : '',
      jobtxbGraduateInstitute2 : '',
      jobtxbGraduateFaculty2 : '',
      jobtxbGraduateField2 : '',
      jobcmbGraduateGraduate2 : '',
      jobtxbGraduatePoint2 : '0',
      jobcmbStudyFrom3 : '',
      jobcmbStudyTo3 : '',
      jobtxbGraduateInstitute3 : '',
      jobtxbGraduateFaculty3 : '',
      jobtxbGraduateField3 : '',
      jobcmbGraduateGraduate3 : '',
      jobtxbGraduatePoint3 : '0',
      jobtxbWorkExperience : '0',
      jobcmbLastWorkField : '',
      jobtxbLastPosition : '',
      jobtxbLastSalary : '0',
      jobcmbWorkFrom1 : '',
      jobcmbWorkTo1 : '',
      jobtxbWorkPosition1 : '',
      jobtxbWorkCompany1 : '',
      jobtxbWorkSalary1 : '0',
      jobtxbWorkIncome1 : '0',
      jobtxbWorkIncomeOther1 : '',
      jobtxbWorkDetail1 : '',
      jobcmbWorkFrom2 : '',
      jobcmbWorkTo2 : '',
      jobtxbWorkPosition2 : '',
      jobtxbWorkCompany2 : '',
      jobtxbWorkSalary2 : '0',
      jobtxbWorkIncome2 : '0',
      jobtxbWorkIncomeOther2 : '',
      jobtxbWorkDetail2 : '',
      jobcmbWorkFrom3 : '',
      jobcmbWorkTo3 : '',
      jobtxbWorkPosition3 : '',
      jobtxbWorkCompany3 : '',
      jobtxbWorkSalary3 : '0',
      jobtxbWorkIncome3 : '0',
      jobtxbWorkIncomeOther3 : '',
      jobtxbWorkDetail3 : '',
      jobcmbWorkFrom4 : '',
      jobcmbWorkTo4 : '',
      jobtxbWorkPosition4 : '',
      jobtxbWorkCompany4 : '',
      jobtxbWorkSalary4 : '0',
      jobtxbWorkIncome4 : '0',
      jobtxbWorkIncomeOther4 : '',
      jobtxbWorkDetail4 : '',
      jobcmbWorkFrom5 : '',
      jobcmbWorkTo5 : '',
      jobtxbWorkPosition5 : '',
      jobtxbWorkCompany5 : '',
      jobtxbWorkSalary5 : '0',
      jobtxbWorkIncome5 : '0',
      jobtxbWorkIncomeOther5 : '',
      jobtxbWorkDetail5 : '',
      jobtxbLanguage1 : '',
      jobcmbLanguageTalk1 : '',
      jobcmbLanguageWrite1 : '',
      jobcmbLanguageRead1 : '',
      jobtxbLanguage2 : '',
      jobcmbLanguageTalk2 : '',
      jobcmbLanguageWrite2 : '',
      jobcmbLanguageRead2 : '',
      jobtxbLanguage3 : '',
      jobcmbLanguageTalk3 : '',
      jobcmbLanguageWrite3 : '',
      jobcmbLanguageRead3 : '',
      jobtxbLanguage4 : '',
      jobcmbLanguageTalk4 : '',
      jobcmbLanguageWrite4 : '',
      jobcmbLanguageRead4 : '',
      jobProfile_com : ''
    }


 $('#flPicture').change(function () {       

           var file = $('#flPicture').val();
           var checkfile = file.split('.').pop().toLowerCase();

        if(file != ''){ 

        var size = this.files[0].size;                
           
           if(checkfile == 'jpg') {

              if(size > 500*1024){
                  
                  $('#btnSend').prop('disabled',true);
                  $('#errsize').show();
                  $('#errpic').hide();  
              
              }else{

                   if($scope.jobform.$error.required) {
       
                        $('#btnSend').prop('disabled',true);
                        $('#errpic').hide();
                        $('#errsize').hide();

                    
                   }else{


                        $('#btnSend').prop('disabled',false);
                        $('#errpic').hide();
                        $('#errsize').hide();
                      
                      }
             
                }
               
            
            } else if(checkfile == 'tif'){

                if(size > 500*1024){
                  
                  $('#btnSend').prop('disabled',true);
                  $('#errsize').show();
                  $('#errpic').hide();  
              
                }else{

                    if($scope.jobform.$error.required) {
       
                          $('#btnSend').prop('disabled',true);
                          $('#errpic').hide();
                          $('#errsize').hide();
                    
                    }else{

                          $('#btnSend').prop('disabled',false);
                          $('#errpic').hide();
                          $('#errsize').hide();
              
                    }
              } 

            } else if(checkfile == 'gif'){
                
                if(size > 500*1024){
                  
                  $('#btnSend').prop('disabled',true);
                  $('#errsize').show();
                  $('#errpic').hide();  
              
                }else{

                    if($scope.jobform.$error.required) {
       
                          $('#btnSend').prop('disabled',true);
                          $('#errpic').hide();
                          $('#errsize').hide();
                    
                    } else{

                          $('#btnSend').prop('disabled',false);
                          $('#errpic').hide();
                          $('#errsize').hide();
          
                  
                    }
                }
               
            }else if(checkfile == 'png'){
                
                if(size > 500*1024){
                  
                  $('#btnSend').prop('disabled',true);
                  $('#errsize').show();
                  $('#errpic').hide();  
              
                }else{

                    if($scope.jobform.$error.required) {
       
                          $('#btnSend').prop('disabled',true);
                          $('#errpic').hide();
                          $('#errsize').hide();
                    
                    } else{

                          $('#btnSend').prop('disabled',false);
                          $('#errpic').hide();
                          $('#errsize').hide();
          
                  
                    }
                }
               
            }else{

                  $('#btnSend').prop('disabled',true);
                  $('#errpic').show();
                  $('#errsize').hide();
            }
      
       } else {

                if($scope.jobform.$error.required) {
       
                     $('#btnSend').prop('disabled',true);
                     $('#errpic').hide();
                     $('#errsize').hide();
                    
                }else{

                     $('#btnSend').prop('disabled',false);
                     $('#errpic').hide();
                     $('#errsize').hide();
                    
                 
                  }          
          }

    });

      $('#flResume').change(function () {       

      var Rfile = $('#flResume').val();

      if(Rfile != ''){      
        var Rsize = this.files[0].size; 

        if(Rsize > 1 * 1024 * 1024 ){
        
              $('#errresume').show();
              $('#btnSend').prop('disabled',true);

        }else{

            if($scope.jobform.$error.required) {

                  $('#errresume').hide();
                  $('#btnSend').prop('disabled',true);
            
            }else{
                   
                  $('#errresume').hide();
                  $('#btnSend').prop('disabled',false);
 
              }

          }
      
      }else{
          
            if($scope.jobform.$error.required) {

                    $('#errresume').hide();
                    $('#btnSend').prop('disabled',true);
            
            }else{

                    $('#errresume').hide();
                    $('#btnSend').prop('disabled',false);
                              
            }

      }


         
    });

		$http.get(url).success(function(response){
			$scope.positions = response;	
		})

    $http.get(job).success(function(response){
      $scope.jobs = response;
    })

    $http.get(province).success(function(response){
      $scope.provinces = response;
    })

    $http.get(area).success(function(response){
        $scope.area = response;
        //console.log($scope.area);
    })

    $http.get(news).success(function(response){
        $scope.news = response;
    })

    $http.get(opt_year).success(function(response){
        $scope.opt_year = response;
    })

    $scope.mounts = function (d){

        var values = '';
        if(d == 1){
            values = 'กรุงเทพมหานคร';
        }else if(d == 2){
            values = 'สมุทรปราการ';
        }else if(d == 3){
            values = 'นนทบุรี';
        }else if(d == 4){
            values = 'ปทุมธานี';
        }else if(d == 5){
            values = 'พระนครศรีอยุธยา'
        }else if(d == 6){
            values = 'อ่างทอง';
        }else if(d == 7){
            values = 'ลพบุรี';
        }else if(d == 8){
            values = 'สิงห์บุรี';
        }else if(d == 9){
            values = 'ชัยนาท';
        }else if(d == 10){
            values = 'สระบุรี';
        }else if(d == 11){
            values = 'ชลบุรี';
        }else if(d == 12){
            values = 'ระยอง';
        }else if(d == 13){
            values = 'จันทบุรี';
        }else if(d == 14){
            values = 'ตราด';
        }else if(d == 15){
            values = 'ฉะเชิงเทรา';
        }else if(d == 16){
            values = 'ปราจีนบุรี';
        }else if(d == 17){
            values = 'นครนายก';
        }else if(d == 18){
            values = 'สระแก้ว';
        }else if(d == 19){
            values = 'นครราชสีมา';
        }else if(d == 20){
            values = 'บุรีรัมย์';
        }else if(d == 21){
            values = 'สุรินทร์';
        }else if(d == 22){
            values = 'ศรีสะเกษ';
        }else if(d == 23){
            values = 'อุบลราชธานี';
        }else if(d == 24){
            values = 'ยโสธร';
        }else if(d == 25){
            values = 'ชัยภูมิ';
        }else if(d == 26){
            values = 'อำนาจเจริญ';
        }else if(d == 27){
            values = 'หนองบัวลำภู';
        }else if(d == 28){
            values = 'ขอนแก่น';
        }else if(d == 29){
            values = 'อุดรธานี';
        }else if(d == 30){
            values = 'เลย';
        }else if(d == 31){
            values = 'หนองคาย';
        }else if(d == 32){
            values = 'มหาสารคาม';
        }else if(d == 33){
            values = 'ร้อยเอ็ด';
        }else if(d == 34){
            values = 'กาฬสินธุ์';
        }else if(d == 35){
            values = 'สกลนคร';
        }else if(d == 36){
            values = 'นครพนม';
        }else if(d == 37){
            values = 'มุกดาหาร';
        }else if(d == 38){
            values = 'เชียงใหม่';
        }else if(d == 39){
            values = 'ลำพูน';
        }else if(d == 40){
            values = 'ลำปาง';
        }else if(d == 41){
            values = 'อุตรดิตถ์';
        }else if(d == 42){
            values = 'แพร่';
        }else if(d == 43){
            values = 'น่าน';
        }else if(d == 44){
            values = 'พะเยา';
        }else if(d == 45){
            values = 'เชียงราย';
        }else if(d == 46){
            values = 'แม่ฮ่องสอน';
        }else if(d == 47){
            values = 'นครสวรรค์';
        }else if(d == 48){
            values = 'อุทัยธานี';
        }else if(d == 49){
            values = 'กำแพงเพชร';
        }else if(d == 50){
            values = 'ตาก';
        }else if(d == 51){
            values = 'สุโขทัย';
        }else if(d == 52){
            values = 'พิษณุโลก';
        }else if(d == 53){
            values = 'พิจิตร ';
        }else if(d == 54){
            values = 'เพชรบูรณ์';
        }else if(d == 55){
            values = 'ราชบุรี ';
        }else if(d == 56){
            values = 'กาญจนบุรี';
        }else if(d == 57){
            values = 'สุพรรณบุรี ';
        }else if(d == 58){
            values = 'นครปฐม';
        }else if(d == 59){
            values = 'สมุทรสาคร';
        }else if(d == 60){
            values = 'สมุทรสงคราม ';
        }else if(d == 61){
            values = 'เพชรบุรี';
        }else if(d == 62){
            values = 'ประจวบคีรีขันธ์';
        }else if(d == 63){
            values = 'นครศรีธรรมราช';
        }else if(d == 64){
            values = 'กระบี่';
        }else if(d == 65){
            values = 'พังงา';
        }else if(d == 66){
            values = 'ภูเก็ต';
        }else if(d == 67){
            values = 'สุราษฎร์ธานี';
        }else if(d == 68){
            values = 'ระนอง';
        }else if(d == 69){
            values = 'ชุมพร';
        }else if(d == 70){
            values = 'สงขลา';
        }else if(d == 71){
            values = 'สตูล ';
        }else if(d == 72){
            values = 'ตรัง ';
        }else if(d == 73){
            values = 'พัทลุง ';
        }else if(d == 74){
            values = 'ปัตตานี';
        }else if(d == 75){
            values = 'ยะลา';
        }else if(d == 76){
            values = 'นราธิวาส';
        }else if(d == 77){
            values = 'ปทุมธานี';
        }
        return  values;
    };

    $scope.dateFormat2 = function (d){

      var now = new Date(d);
      //console.log(now);
      var thmonth = new Array ("ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.", "ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");

      return now.getDate()+ " " + thmonth[now.getMonth()]+ " " + (now.getFullYear()+543);
  }

    $scope.dateFormat = function (d){
      //console.log(d);
      //var now = new Date(d);
      
      var thmonth = new Array ("ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.", "ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");   
      var day = d;
      var d = day.substring (0,2);
      var m = day.substring (3,5);
      var y = day.substring (6,10);
      var mount = m-1; 
      //console.log(d+ " " + thmonth[mount]+ " " + y);
      return d+ " " + thmonth[mount]+ " " + y;
    };

    $scope.dateFormatY = function (d){
      var now = new Date(d)
      return  (now.getFullYear()+543)
    };

    $scope.dateFormatM = function (d){
            if(d){

                var now = d.substring(0, 2);
                var now2 = d.substring(3, 7);
                now2  = parseInt(now2)
                //console.log(now2);
                var thmonth = new Array ("ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.", "ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
                return thmonth[parseInt(now)-1]+ " " + now2;
            }else{
                return '';
            }
        }

    $scope.dateFormatY = function (d){
      var now = new Date(d)
      return  (now.getFullYear()+543)
    };

    $scope.parseDate = function(item){

            //console.log(item);
            var substr = item.substring(0,11);
            //console.log(substr);
            var data  = new Date(substr);
            //console.log("data = " + data);
            return data;

    };
    $scope.edu_ifvalue = function (d){

          var values = '';
          if(d == 1){
            values = 'ปวช.';
          }else if(d == 2){
            values = 'ปวส.';
          }else if(d == 3){
            values = 'ป.ตรี';
          }else if(d == 4){
             values = 'ป.โท'
          }else if(d == 5){
             values = 'ป.เอก'
          }
          return  values;
    };



    $scope.parseDate = function(item){

		//console.log(item);
		var substr = item.substring(0,11);
		//console.log(substr);
  		var data  = new Date(substr);
 		//console.log("data = " + data);		
 		return data;

		};

$scope.test = function(id,job_name){
   //    $('.tabs-menu').remove();
   //    $('.tab-dd').remove();
			// $('.tab-content .current').removeClass('current').hide()
   //          .next().show().addClass('current');
	  //   $('#backJobListsBtm2').hide();
   //    $('#backJobLists').hide();
   //    $('#choose-location').hide();
   //     $('.page-banner-md').remove();
   //    // $('.tab-dd').hide();
   //    $('.banner-parallax').remove();
   var MyPosition = $('#txbInterestPosition').val(job_name);
   
   $('#myjob').html(job_name);

   $('.page-banner-md').hide()
        $('.tab-dd').hide()
        $('#choose-location').hide();
        $('#table').hide();

        $('.heading-title').show()   
        $('.job-form').show()

            $scope.jobid = id;
            
};

$scope.myFunc = function(){
       
    var MyPosition = $('#txbInterestPosition').val(); 
      console.log($('#txbInterestPosition').val());
     
      if($scope.data.jobcmbReligion == 'อื่นๆ'){

            $scope.data.jobcmbReligion = $scope.data.jobcmbReligion + ' ' + $scope.data.txbReligionOther;
      
      }if($scope.data.jobrdoRace == 'อื่นๆ'){

            $scope.data.jobrdoRace = $scope.data.jobrdoRace + ' ' + $scope.data.txbRaceOther;
      
      }if($scope.data.jobrdoNationality == 'อื่นๆ'){

             $scope.data.jobrdoNationality = $scope.data.jobrdoNationality + ' ' +$scope.data.txbNationalityOther;
      
      }if($scope.data.jobcmbConscription == 'ยกเว้น'){

            $scope.data.jobcmbConscription = $scope.data.jobcmbConscription + ' ' + $scope.data.txbConscription;
      }
      if($scope.data.jobrdoWorkSite == 'N'){

            $scope.data.jobrdoWorkSite =  $scope.data.jobrdoWorkSite +' '+ $scope.data.jobrdoWorkSiteNo;
      
      }
      if($scope.data.jobrdoWorkSiteFew == 'N'){

            $scope.data.jobrdoWorkSiteFew =  $scope.data.jobrdoWorkSiteFew +' '+ $scope.data.jobrdoWorkSiteFewNo;
      
      }
      if($scope.data.jobrdoHasDisease == 'Y'){

            $scope.data.jobrdoHasDisease =  $scope.data.jobrdoHasDisease +' '+ $scope.data.jobrdoHasDiseaseYes;
      
      }
      if($scope.data.jobrdoHasAccuse == 'Y'){

            $scope.data.jobrdoHasAccuse =  $scope.data.jobrdoHasAccuse +' '+ $scope.data.jobrdoHasAccuseYes;
      
      }
      if($scope.data.jobrdoHasBankrupt == 'Y'){

            $scope.data.jobrdoHasBankrupt =  $scope.data.jobrdoHasBankrupt +' '+ $scope.data.jobrdoHasBankruptYes;
      
      }
      if($scope.data.jobrdoHasLayoff == 'Y'){

            $scope.data.jobrdoHasLayoff =  $scope.data.jobrdoHasLayoff +' '+ $scope.data.jobrdoHasLayoffYes;
      
      }
      if($scope.data.jobcmbWorkFrom1 != ''){

            $scope.data.jobcmbWorkFrom1 = '01/' + $scope.data.jobcmbWorkFrom1;
      }
      if($scope.data.jobcmbWorkTo1 != ''){

            $scope.data.jobcmbWorkTo1 = '01/' + $scope.data.jobcmbWorkTo1;
      }
      if($scope.data.jobcmbWorkFrom2 != ''){

            $scope.data.jobcmbWorkFrom2 = '01/' + $scope.data.jobcmbWorkFrom2;
      }
      if($scope.data.jobcmbWorkTo2 != ''){

            $scope.data.jobcmbWorkTo2 = '01/' + $scope.data.jobcmbWorkTo2;
      }
      if($scope.data.jobcmbWorkFrom3 != ''){

            $scope.data.jobcmbWorkFrom3 = '01/' + $scope.data.jobcmbWorkFrom3;
      }
      if($scope.data.jobcmbWorkTo3 != ''){

            $scope.data.jobcmbWorkTo3 = '01/' + $scope.data.jobcmbWorkTo3;
      }
      if($scope.data.jobcmbWorkFrom4 != ''){

            $scope.data.jobcmbWorkFrom4 = '01/' + $scope.data.jobcmbWorkFrom4;
      }
      if($scope.data.jobcmbWorkTo4 != ''){

            $scope.data.jobcmbWorkTo4 = '01/' + $scope.data.jobcmbWorkTo4;
      }
       if($scope.data.jobcmbWorkFrom5 != ''){

            $scope.data.jobcmbWorkFrom5 = '01/' + $scope.data.jobcmbWorkFrom5;
      }
      if($scope.data.jobcmbWorkTo5 != ''){ 

            $scope.data.jobcmbWorkTo5 = '01/' + $scope.data.jobcmbWorkTo5;
      }
      if($scope.data.jobcmbStudyFrom1 != ''){

            $scope.data.jobcmbStudyFrom1 = '01/' + $scope.data.jobcmbStudyFrom1;
      }
      if($scope.data.jobcmbStudyTo1 != ''){

            $scope.data.jobcmbStudyTo1 = '01/' + $scope.data.jobcmbStudyTo1;
      }
      if($scope.data.jobcmbStudyFrom2 != ''){

            $scope.data.jobcmbStudyFrom2 = '01/' + $scope.data.jobcmbStudyFrom2;
      }
      if($scope.data.jobcmbStudyTo2 != ''){

            $scope.data.jobcmbStudyTo2 = '01/' + $scope.data.jobcmbStudyTo2;
      }
      if($scope.data.jobcmbStudyFrom3 != ''){

            $scope.data.jobcmbStudyFrom3 = '01/' + $scope.data.jobcmbStudyFrom3;
      }
      if($scope.data.jobcmbStudyTo3 != ''){

            $scope.data.jobcmbStudyTo3 = '01/' + $scope.data.jobcmbStudyTo3;
      }
     


     
			var profile = {

                work_id : $scope.jobid,
                provine_id : $scope.data.jobcmbBirthProvince,
                prefix : $scope.data.jobcmbTitle,
                name : $scope.data.jobtxbFName,
                lastname : $scope.data.jobtxbLName,
                nickname : $scope.data.jobtxbNickname,
                sex : $scope.data.jobrdoGender,
                birthday : $scope.data.jobdateBirth,
                height : $scope.data.jobtxbHeight,
                weight : $scope.data.jobtxbWeight,
                religion : $scope.data.jobcmbReligion,
                race : $scope.data.jobrdoRace,
                nationslity : $scope.data.jobrdoNationality,
                draft : $scope.data.jobcmbConscription,
                card_namber : $scope.data.jobtxbIDNo,
                status : $scope.data.jobrdoMarried,
                moredata : $scope.data.jobtxbFurtherInfo,
                children : $scope.data.jobtxbChildNum,
                printTH : $scope.data.jobtxbTypingThai,
                printEN : $scope.data.jobtxbTypingEng,
                provine_contact_id : $scope.data.jobcmbContactProvince,
                address : $scope.data.jobtxbAddress,
                phone : $scope.data.jobtxbContactTel,
                email : $scope.data.jobtxbContactEmail,
                name_emer : $scope.data.jobtxbEmergencyPerson,
                phone_emer : $scope.data.jobtxbEmergencyTel,
                //position : $scope.data.jobtxbInterestPosition,
                position : MyPosition,
                type_job : $scope.data.jobrdoInterestPositionType,
                salary_hope : $scope.data.jobtxbExpectSalary,
                start_day : $scope.data.jobdateWork,
                name_person : $scope.data.jobtxbReferenceName,
                company : $scope.data.jobtxbReferenceCompany,
                relationship : $scope.data.jobtxbReferenceRelation,
                telperson : $scope.data.jobtxbReferenceTel,
                work_person : $scope.data.jobtxbReferencePosition,
                working_always : $scope.data.jobrdoWorkSite,
                working_sometimes : $scope.data.jobrdoWorkSiteFew,
                disease : $scope.data.jobrdoHasDisease,
                crime : $scope.data.jobrdoHasAccuse,
                bankrupt: $scope.data.jobrdoHasBankrupt,
                lay_off : $scope.data.jobrdoHasLayoff,
                other : '',
                profile_com : $scope.data.jobProfile_com
            
            }
            
      
// profile
      $http({

              url: profile_url,
              method: "POST",
              data: $.param(profile),
              headers : { 'Content-Type': 'application/x-www-form-urlencoded'
              } 
          }).then(function(response) {  
              console.log(response);
          
          if(response.data.status == '1'){
            swal({
                title: "ส่งแล้ว !",
                type: "success",
                text: "ข้อมูลได้ทำการส่งเรียบร้อยแล้ว",
                timer: 3000,
                showConfirmButton: false
              });
             //alert("ข้อมูลได้ทำการส่งเรียบร้อยแล้ว...555");
             setTimeout(function(){ window.location.href = 'job'; }, 2500);
             
            
          }else{
            alert('การส่งข้อมูลผิดพลาด กรุณาลองใหม่');
            swal({
                title: "เกิดข้อผิดพลาดในการส่ง !",
                type: "success",
                text: "มีปัญหาระหว่างการส่งข้อมูล",
                timer: 3000,
                showConfirmButton: false
              });
            window.location.href = 'job';
            setTimeout(function(){ window.location.href = 'job'; }, 2500);
          }
          $scope.id = response.data.id;

          console.log("my-id = "+ response.data.id);
          
                  

       var family = {
              
              id : $scope.id,
              name_father : $scope.data.jobtxbFatherName,
              age_father : $scope.data.jobtxbFatherAge,
              career_father : $scope.data.jobtxbFatherOcc,
              address_father : $scope.data.jobtxbFatherAddr,
              status_father : 'บิดา' ,
              name_mother : $scope.data.jobtxbMotherName,
              age_mother : $scope.data.jobtxbMotherAge,
              career_mother : $scope.data.jobtxbMotherOcc,
              address_mother : $scope.data.jobtxbMotherAddr,
              status_mother : 'มารดา' ,
              name_fraternity1 : $scope.data.jobtxbCousin1Name,
              age_fraternity1 : $scope.data.jobtxbCousin1Age,
              career_fraternity1 : $scope.data.jobtxbCousin1Occ,
              address_fraternity1 : $scope.data.jobtxbCousin1Addr,
              status_fraternity1 : 'พี่น้อง1' ,
              name_fraternity2 : $scope.data.jobtxbCousin2Name,
              age_fraternity2 : $scope.data.jobtxbCousin2Age,
              career_fraternity2 : $scope.data.jobtxbCousin2Occ,
              address_fraternity2 : $scope.data.jobtxbCousin2Addr,
              status_fraternity2 : 'พี่น้อง2' ,
              name_fraternity3 : $scope.data.jobtxbCousin3Name,
              age_fraternity3 : $scope.data.jobtxbCousin3Age,
              career_fraternity3 : $scope.data.jobtxbCousin3Occ,
              address_fraternity3 : $scope.data.jobtxbCousin3Addr,
              status_fraternity3 : 'พี่น้อง3',
              name_spouse : $scope.data.jobtxbSpouseName,
              age_spouse : $scope.data.jobtxbSpouseAge,
              career_spouse : $scope.data.jobtxbSpouseOcc,
              address_spouse : $scope.data.jobtxbSpouseAddr,
              status_spouse : 'คู่สมรส' ,
             }  
       //console.log(family);          
            
       var job = {
              id : $scope.id,
              finish :  $scope.data.jobrdoJustGraduate,
              exp :  $scope.data.jobtxbWorkExperience,
              line_last :  $scope.data.jobcmbLastWorkField,
              last_position :  $scope.data.jobtxbLastPosition,
              last_money :  $scope.data.jobtxbLastSalary,
              date_start1 :  $scope.data.jobcmbWorkFrom1,
              date_stop1 :  $scope.data.jobcmbWorkTo1,
              position1 :  $scope.data.jobtxbWorkPosition1,
              company1 :  $scope.data.jobtxbWorkCompany1,
              salary1 :  $scope.data.jobtxbWorkSalary1,
              other_salary1 :  $scope.data.jobtxbWorkIncome1,
              more_other_salary1 :  $scope.data.jobtxbWorkIncomeOther1,
              features1 :  $scope.data.jobtxbWorkDetail1,
              date_start2 :  $scope.data.jobcmbWorkFrom2,
              date_stop2 :  $scope.data.jobcmbWorkTo2,
              position2 :  $scope.data.jobtxbWorkPosition2,
              company2 :  $scope.data.jobtxbWorkCompany2,
              salary2 :  $scope.data.jobtxbWorkSalary2,
              other_salary2 :  $scope.data.jobtxbWorkIncome2,
              more_other_salary2 :  $scope.data.jobtxbWorkIncomeOther2,
              features2 :  $scope.data.jobtxbWorkDetail2,
              date_start3 :  $scope.data.jobcmbWorkFrom3,
              date_stop3 :  $scope.data.jobcmbWorkTo3,
              position3 :  $scope.data.jobtxbWorkPosition3,
              company3 :  $scope.data.jobtxbWorkCompany3,
              salary3 :  $scope.data.jobtxbWorkSalary3,
              other_salary3 :  $scope.data.jobtxbWorkIncome3,
              more_other_salary3 :  $scope.data.jobtxbWorkIncomeOther3,
              features3 :  $scope.data.jobtxbWorkDetail3,
              date_start4 :  $scope.data.jobcmbWorkFrom4,
              date_stop4 :  $scope.data.jobcmbWorkTo4,
              position4 :  $scope.data.jobtxbWorkPosition4,
              company4 :  $scope.data.jobtxbWorkCompany4,
              salary4 :  $scope.data.jobtxbWorkSalary4,
              other_salary4 :  $scope.data.jobtxbWorkIncome4,
              more_other_salary4 :  $scope.data.jobtxbWorkIncomeOther4,
              features4 :  $scope.data.jobtxbWorkDetail4,
              date_start5 :  $scope.data.jobcmbWorkFrom5,
              date_stop5 :  $scope.data.jobcmbWorkTo5,
              position5 :  $scope.data.jobtxbWorkPosition5,
              company5 :  $scope.data.jobtxbWorkCompany5,
              salary5 :  $scope.data.jobtxbWorkSalary5,
              other_salary5 :  $scope.data.jobtxbWorkIncome5,
              more_other_salary5 :  $scope.data.jobtxbWorkIncomeOther5,
              features5 :  $scope.data.jobtxbWorkDetail5,
          }
         
       var education = {
              id : $scope.id,
              top_graduate : $scope.data.jobcmbHighestGraduate,
              graduate_id1 : $scope.data.jobcmbGraduateGraduate1, 
              date_start1 : $scope.data.jobcmbStudyFrom1,
              date_stop1 : $scope.data.jobcmbStudyTo1,
              institution1 : $scope.data.jobtxbGraduateInstitute1,
              faculty1 : $scope.data.jobtxbGraduateFaculty1,
              branch1 : $scope.data.jobtxbGraduateField1,
              gpa1 : $scope.data.jobtxbGraduatePoint1,
              graduate_id2 : $scope.data.jobcmbGraduateGraduate2,
              date_start2 : $scope.data.jobcmbStudyFrom2,
              date_stop2 : $scope.data.jobcmbStudyTo2,
              institution2 : $scope.data.jobtxbGraduateInstitute2,
              faculty2 : $scope.data.jobtxbGraduateFaculty2,
              branch2 : $scope.data.jobtxbGraduateField2,
              gpa2 : $scope.data.jobtxbGraduatePoint2,
              graduate_id3 : $scope.data.jobcmbGraduateGraduate3,
              date_start3 : $scope.data.jobcmbStudyFrom3,
              date_stop3 : $scope.data.jobcmbStudyTo3,
              institution3 : $scope.data.jobtxbGraduateInstitute3,
              faculty3 : $scope.data.jobtxbGraduateFaculty3,
              branch3 : $scope.data.jobtxbGraduateField3,
              gpa3 : $scope.data.jobtxbGraduatePoint3,
          }
         
       var language = {
              id : $scope.id,
              language1 :  $scope.data.jobtxbLanguage1,
              talk1 :  $scope.data.jobcmbLanguageTalk1,
              write1 :  $scope.data.jobcmbLanguageWrite1,
              read1 :  $scope.data.jobcmbLanguageRead1,
              language2 :  $scope.data.jobtxbLanguage2,
              talk2 :  $scope.data.jobcmbLanguageTalk2,
              write2 :  $scope.data.jobcmbLanguageWrite2,
              read2 :  $scope.data.jobcmbLanguageRead2,
              language3 :  $scope.data.jobtxbLanguage3,
              talk3 :  $scope.data.jobcmbLanguageTalk3,
              write3 :  $scope.data.jobcmbLanguageWrite3,
              read3 :  $scope.data.jobcmbLanguageRead3,
              language4 :  $scope.data.jobtxbLanguage4,
              talk4 :  $scope.data.jobcmbLanguageTalk4,
              write4 :  $scope.data.jobcmbLanguageWrite4,
              read4 :  $scope.data.jobcmbLanguageRead4,
          }

// //family
            $http({

                  url: family_url,
                  method: "POST",
                  data: $.param(family),
                  headers : { 'Content-Type': 'application/x-www-form-urlencoded'
                
                } 
              }).then(function(response) {  
                    
                    //console.log(response);
                    
              }, function(response) { // optional
               
                  //console.log(response);
              });

 //job
            $http({

                  url: job_url,
                  method: "POST",
                  data: $.param(job),
                  headers : { 'Content-Type': 'application/x-www-form-urlencoded'
                
                } 
              }).then(function(response) {  
                    
                    //console.log(response);
                    
              }, function(response) { // optional
                
                    //console.log(response);
              });

 //language
            $http({

                  url: language_url,
                  method: "POST",
                  data: $.param(language),
                  headers : { 'Content-Type': 'application/x-www-form-urlencoded'
                
                } 
              }).then(function(response) {  
                    
                   //console.log(response);
                    
              }, function(response) { // optional
                
                  //console.log(response);
              });

 //education
            $http({

                  url: education_url,
                  method: "POST",
                  data: $.param(education),
                  headers : { 'Content-Type': 'application/x-www-form-urlencoded'
                
                } 
              }).then(function(response) {  
                    
                    //console.log(response);
                    
              }, function(response) { 
                  
                  //console.log(response);
              
              });


//file          

        var file = $('#flResume').val();
        console.log(file);
        
        if(file != ''){
         
         var file_data = $('#flResume').prop('files')[0];
                
                var cutfile = file.split('.').pop().toLowerCase();
                $scope.file = $scope.data.jobtxbIDNo +  '_resume' ;          
                var form_data = new FormData();                  
                form_data.append('file', file_data);
                form_data.append('id', $scope.id);
                form_data.append('attachment_file', $scope.file);
                
                $.ajax({
                    url: '/www/build/api/service/register_be.php?n=candidacyAdd_file', // point to server-side PHP script
                    dataType: 'json',  
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,                         
                    type: 'post',
                    success: function(response){
                          
                        //console.log(response);
                    }
                });
        }

//image
      var image = $('#flPicture').val();
      
      if(image != ''){

        var image_data = $('#flPicture').prop('files')[0];  
        //console.log(image_data);               
                var image = $('#flPicture').val();
                var cutimage = image.split('.').pop().toLowerCase();
                //$scope.image = $scope.data.jobtxbIDNo + '_image' + '.' + cutimage;  
                $scope.image = $scope.data.jobtxbIDNo + '_image' ;          
                var form_data1 = new FormData();                  
                form_data1.append('file_img', image_data);
                form_data1.append('id', $scope.id);
                form_data1.append('attachment_image', $scope.image);
               
             //console.log(image_data);
                $.ajax({
                    url: '/www/build/api/service/register_be.php?n=candidacyAdd_img',
                    dataType: 'json',  
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data1,                         
                    type: 'post',
                    success: function(response){
                          
                        //console.log(response);
                    }
                });
    }
                 
      }, function(response) { // optional error
          
         	// console.log(response);

    });



  
  };


});