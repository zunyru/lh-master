(function() {
    'use strict';
                                         
    var app = angular.module('calculator', []);

    app.controller('calculatorController', function ($scope,$timeout) {
    	
    	$scope.date = [];
    	$scope.loan = 0;
    	$scope.mlr = 6.875;
    	$scope.minimumpayment = 0;
    	$scope.year = 0;
    	$scope.c = {};
    	$scope.j = {};
    	
    	$scope.d = {};
    	$scope.e = {};
    	$scope.f = {};
    	$scope.g = {};
    	$scope.h = {};
    	$scope.i = {};

    	$scope.PMT = function (ir, np, pv, fv ) {
			 /*
			 ir - interest rate per month
			 np - number of periods (months)
			 pv - present value
			 fv - future value (residual value)
			 */
			 var pmt = ( ir * ( pv * Math.pow ( (ir+1), np ) + fv ) ) / ( ( ir + 1 ) * ( Math.pow ( (ir+1), np) -1 ) );
			 var pmt1 = parseFloat(pmt.toFixed(1))
			 $scope.minimumpayment = parseFloat(pmt1.toFixed(0))
			 return pmt1;
		}
		// ดอกเบี้ยต่องวด
		$scope.increase = function(num,index){
			$scope.d[index] = parseFloat(num);
			return parseFloat(num);
		}
		//เงินต้นต่องวด
		$scope.capital = function(num,index){
			$scope.e[index] = parseFloat(num);
			return parseFloat(num);
		}
		//เงินต้นรวม
		$scope.capitalSum = function(num,index){
			$scope.f[index] = parseFloat(num);
			return parseFloat(num);
		}
		//ดอกเบี้ยรวม
		$scope.increaseSum = function(num,index){
			$scope.g[index] = parseFloat(num);
			return parseFloat(num);
		}
		//ค่าผ่อนชำระรวม
		$scope.installmentSum = function(num,index){
			$scope.h[index] = parseFloat(num);
			return parseFloat(num);
		}
		//เงินต้นคงเหลือ
		$scope.balance = function(num,index){
			$scope.i[index] = parseFloat(num);
			return parseFloat(num);
		}

		$scope.print = function(){ 
			$("thead").css("text-align", "left");
			$timeout(function(){ 
				
	           	html2canvas($("#pdf"), {
	                onrendered: function(canvas) {
	                    // create a new window
	                    $('#content').hide()
	                    // create a new window
	                    var nWindow = window
						// append the canvas to the body
						$('#img_print').append(canvas)
			            // focus on the window
			            nWindow.focus();

			            // print the window
			            nWindow.print();

			            nWindow.close();
			            
						$('#content').show()
	                    $("thead").css("text-align", "center");
	                    $('#img_print').html('')

	                }
	            });
	         },200);
		}


    });

    app.$inject = ['$scope'];
    
    app.directive('format', ['$filter', function ($filter) {
	    return {
	        require: '?ngModel',
	        link: function (scope, elem, attrs, ctrl) {
	            if (!ctrl) return;


	            ctrl.$formatters.unshift(function (a) {
	                return $filter(attrs.format)(ctrl.$modelValue)
	            });


	            ctrl.$parsers.unshift(function (viewValue) {
	                var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g,'');
	                elem.val($filter(attrs.format)(plainNumber));
	                return parseFloat(plainNumber);
	            });
	        }
	    };
	}]);

	app.directive('numbersOnly', function () {
	    return {
	        require: '?ngModel',
	        link: function (scope, element, attr, ngModelCtrl) {
	            function fromUser(text) { 
	                if (text) {
	                    var transformedInput = text.replace(/[^0-9].[^0-9]/g, '');

	                    if (transformedInput !== text) {
	                        ngModelCtrl.$setViewValue(transformedInput);
	                        ngModelCtrl.$render();
	                    }
	                    return transformedInput;
	                }
	                return undefined;
	            }            
	            ngModelCtrl.$parsers.push(fromUser);
	        }
	    };
	});


})(); // Best Practice For Javascript