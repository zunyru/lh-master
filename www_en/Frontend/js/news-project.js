$(document).ready(function () {
    //Page Load Start
    bannerSlide();

    galleryLayout5();

    //Page Load End
    $('.gallery').featherlightGallery();
    $('.gallery2').featherlightGallery();

    $(document).ready(function()
    {
        $(document).resize();
    });
});

$(window).load(function () {
    galleryLayout5();
});


//Function Start
function bannerSlide() {
    var winH = $(window).height();
    //$('.banner-parallax').css('height', winH - 60);

    var isMulti = ($('#bannerSlide .item').length > 1) ? true : false;
    $('#bannerSlide').owlCarousel({
        loop:isMulti,
        autoplay: true,
        autoplayTimeout: 4000,
        autoplaySpeed: 1000,
        margin:5,
        animateOut: 'fadeOut',
        nav:false,
        dots: isMulti,
        video:true,
        lazyLoad:true,
        center:true,
        responsive:{
            0:{
                items:1
            },

        }
    });
}


function gridLayout5() {

    // var objWall = {
    //     wall1: new Freewall("#wall-1"),
    //     wall2: new Freewall("#wall-2"),
    //     wall3: new Freewall("#wall-3")
    // };
    //
    // var imgWall = {
    //     image1: 1,
    //     image2: 2,
    //     image3: 3
    // };
    // $.each(objWall, function(k, v) {
    //     objWall[k].reset({
    //         selector: '.item',
    //         animate: true,
    //         cellW: 300,
    //         cellH: 'auto',
    //         onResize: function() {
    //             objWall[k].fitWidth();
    //         }
    //     });
    //
    //     imgWall[k] = objWall[k].container.find('.item');
    //     imgWall[k].find('img').load(function() {
    //         objWall[k].fitWidth();
    //     });
    // });

    var numNewsBlock = $('.news-block').length;
    for(var n = 1; n <= numNewsBlock; n++) {
        $("#wall-" + n).each(function () {
            var wall = new Freewall(this);
            wall.reset({
                selector: '.item',
                animate: true,
                cellW: 300,
                cellH: 'auto',
                onResize: function () {
                    wall.fitWidth();
                }
            });

            var images = wall.container.find('.item');
            images.find('img').load(function () {
                wall.fitWidth();
            });
        });
    }
}

function galleryLayout5() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        var isMulti = ($('#tpl5-img-slide .item').length > 1) ? true : false;
        $('.tpl5-img-slide').each(function () {
            $(this).owlCarousel({
                loop:isMulti,
                margin: 30,
                // stagePadding: 40,
                nav:isMulti,
                dots: isMulti,
                autoHeight: false,
                responsive:{
                    0:{
                        items:1
                    }
                }
            });
        });
    }
    else {
        gridLayout5();
    }
}

function gridLayout4() {
    var wall = new Freewall("#wall-1");
    wall.reset({
        selector: '.item',
        animate: true,
        cellW: 280,
        cellH: 'auto',
        onResize: function() {
            wall.fitWidth();
        }
    });

    var images = wall.container.find('.item');
    images.find('img').load(function() {
        wall.fitWidth();
    });
}

function galleryLayout4() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        $('.tpl4-img-slide').each(function () {
            $(this).owlCarousel({
                loop:isMulti,
                margin: 30,
                // stagePadding: 40,
                nav:isMulti,
                dots: isMulti,
                autoHeight: false,
                responsive:{
                    0:{
                        items:1
                    }
                }
            });
        });


    }
    else {
        gridLayout4();
    }
}


//Function End