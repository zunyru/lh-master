$(document).ready(function () {
    //Page Load Start
    // bannerSlide();

    var winW = $(window).width();
    if( winW <= 768 ) {
        // $("#pjHomeBanner .item:first").remove();
    }

    var isMulti = ($('#pjHomeBanner.owl-carousel .item').length > 1) ? true : false;
    $('#pjHomeBanner').owlCarousel({
        loop:isMulti,
        autoplay: true,
        autoplayTimeout: 7000,
        autoplaySpeed: 1000,
        margin:5,
        animateOut: 'fadeOut',
        nav:false,
        dots: isMulti,
        video:true,
        lazyLoad:true,
        center:true,
        responsive:{
            0:{
                items:1
            }
        },
        onChanged: function (event) {
            setTimeout(function(){
                $('.slider').find('.center').addClass('animate');
            }, 1000);
            setTimeout(function(){
                $('.slider').find('.center').addClass('animate2');
            }, 1200);
            // setTimeout(function(){
            //     $('.slider').find('.center').addClass('animate3');
            // }, 6000);
            setTimeout(function () {
                $('#title').fadeOut('slow');
                $('#desc').fadeOut('slow');
            },5900);
        }
    });


    slideFurnishedImg();
    stickyMenuProject();

    var status_list = $('#status_list');
    $('.i-view-list').click(function(event){
        event.preventDefault();
        if(status_list.val()=='grid2'){
            grid2ToList();
        }else if(status_list.val()=='grid3'){
            grid3ToList();
        }
        $('#status_list').val('list');

        $('.furnishHome img').css('height','480px');
    });
    $('.i-view-grid').click(function(event){
        event.preventDefault();
        if(status_list.val()=='grid3'){
            grid3ToList();
            listToGrid2();
            grid2ToList();
            listToGrid2();
        }else if(status_list.val()=='list'){
            listToGrid2();
            grid2ToList();
            listToGrid2();
        }
        status_list.val('grid2');

        $('.furnishHome img').css('height','auto');
    });
    $('.i-view-grid-more').click(function(event){
        event.preventDefault();
        if(status_list.val()=='grid2'){
            grid2ToList();
            listToGrid3();
            grid3ToList();
            listToGrid3();
        }else if(status_list.val()=='list'){
            listToGrid3();
            grid3ToList();
            listToGrid3();
        }
        status_list.val('grid3');

        $('.furnishHome img').css('height','auto');
    });

    //CSS FOR All Window Size

    // $(".col-lg-6").css("width", "570px");
    // $(".col-lg-4").css("width", "380px");
    // $(".col-xs-6").css("width", "570px");
    // $(".col-xs-4").css("width", "380px");
    // $(window).on('resize', function(){
    //     var win = $(this); //this = window
    //
    //     $(".col-lg-6").css("width", (win.width()/2.63)+"px");
    //     $(".col-lg-4").css("width", (win.width()/3.94)+"px");
    //     $(".col-xs-6").css("width", (win.width()/2.63)+"px");
    //     $(".col-xs-4").css("width", (win.width()/3.94)+"px");
    // });
    //Page Load End
});


//Function Start
function bannerSlide() {
    var winH = $(window).height();
    var winW = $(window).width();

    var bannerH = $('#bannerSlide').height();
    var txtH = $('#bannerSlide h1').height();


    if( winW < 992 ) {
        var center = (bannerH - 55 ) / 2;
        $('#bannerSlide h1').css('top',center);
    }
    else {
        //$('.banner-parallax').css('height', winH - 60);
        var center = (winH - 270 ) / 2;
        $('#bannerSlide h1').css('top',center);
    }


    //var txtBlockH = $('#bannerSlide h1').height();


    var isMulti = ($('#bannerSlide.owl-carousel img').length > 1) ? true : false;
    $('#bannerSlide').owlCarousel({
        loop:isMulti,
        autoplay: true,
        autoplayTimeout: 7000,
        autoplaySpeed: 1000,
        margin:5,
        animateOut: 'fadeOut',
        nav:false,
        dots: true,
        video:true,
        lazyLoad:true,
        center:true,
        responsive:{
            0:{
                items:1
            },

        }
    });
}

function slideFurnishedImg() {
    $('.furnishHome').each(function () {
        var isMulti = ($(this).find('.item').length > 1) ? true : false;
        $(this).owlCarousel({
            loop:isMulti,
            autoplay: false,
            autoplayTimeout: 4000,
            autoplaySpeed: 1000,
            margin:5,
            animateOut: 'fadeOut',
            nav:false,
            dots: isMulti,
            video:true,
            lazyLoad:true,
            center:true,
            responsive:{
                0:{
                    items:1
                },

            }
        });
    });
}
function updateSize(){
    var maxWidth = 100; // Max width for the image
    var maxHeight = 100;    // Max height for the image
    var ratio = 0;  // Used for aspect ratio
    var width = $(this).width();    // Current image width
    var height = $(this).height();  // Current image height

    // Check if the current width is larger than the max
    if(width > maxWidth){
        ratio = maxWidth / width;   // get ratio for scaling image
        $(this).css("width", maxWidth); // Set new width
        $(this).css("height", height * ratio);  // Scale height based on ratio
        height = height * ratio;    // Reset height to match scaled image
        width = width * ratio;    // Reset width to match scaled image
    }

    // Check if current height is larger than max
    if(height > maxHeight){
        ratio = maxHeight / height; // get ratio for scaling image
        $(this).css("height", maxHeight);   // Set new height
        $(this).css("width", width * ratio);    // Scale width based on ratio
        width = width * ratio;    // Reset width to match scaled image
        height = height * ratio;    // Reset height to match scaled image
    }

}

function stickyMenuProject() {
    var winH = $(window).height();
    var pageH = winH - 70

    $('#more-info').click(function() {
        var margin_top_animate = 70;
        $('html, body').animate({
            scrollTop: $('.furnishedhome-block').offset().top - margin_top_animate
        }, 1000);
        $(this).removeClass('active');
    });

}


function listToGrid3(){
    $('#products .projecttype-list .projecttype-descrp').removeClass('projecttype-descrpgrid2').addClass('grid2');
    $('#products .projecttype-list .grid3').removeClass('grid3').addClass('group inner list-group-item-text');
    $('#products .projecttype-list .col-md-8').removeClass('col-md-8').addClass('thumbnail');
    $('#products .projecttype-list .col-md-4').removeClass('col-md-4').addClass('caption');
    $('#products .projecttype-list .row').removeClass('row').addClass('grid1');
    $('#products .projecttype-list').removeClass('projecttype-list').addClass('item col-xs-4 col-lg-4');
    // $('.furnishHome .item').removeClass('col-xs-4').removeClass('col-lg-4');
}
function listToGrid2(){
    $('#products .projecttype-list .projecttype-descrp').removeClass('projecttype-descrpgrid2').addClass('grid2');
    $('#products .projecttype-list .grid3').removeClass('grid3').addClass('group inner list-group-item-text');
    $('#products .projecttype-list .col-md-8').removeClass('col-md-8').addClass('thumbnail');
    $('#products .projecttype-list .col-md-4').removeClass('col-md-4').addClass('caption');
    $('#products .projecttype-list .row').removeClass('row').addClass('grid1');
    $('#products .projecttype-list').removeClass('projecttype-list').addClass('item col-xs-6 col-lg-6');
    // $('.furnishHome .item').removeClass('col-xs-6').removeClass('col-lg-6');
}
function grid3ToList(){
    $('#products .item .grid2').removeClass('grid2').addClass('projecttype-descrp');
    $('#products .item .grid2 .group').removeClass('group inner list-group-item-text').addClass('grid3');
    $('#products .item .thumbnail').removeClass('thumbnail').addClass('col-md-8');
    $('#products .item .caption').removeClass('caption').addClass('col-md-4');
    $('#products .item .grid1').removeClass('grid1').addClass('row');
    $('#products .item').removeClass('item col-xs-4 col-lg-4').addClass('projecttype-list');
}
function grid2ToList(){
    $('#products .item .grid2').removeClass('grid2').addClass('projecttype-descrp');
    $('#products .item .grid2 .group').removeClass('group inner list-group-item-text').addClass('grid3');
    $('#products .item .thumbnail').removeClass('thumbnail').addClass('col-md-8');
    $('#products .item .caption').removeClass('caption').addClass('col-md-4');
    $('#products .item .grid1').removeClass('grid1').addClass('row');
    $('#products .item').removeClass('item col-xs-6 col-lg-6').addClass('projecttype-list');
}

//Function End