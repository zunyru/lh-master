$(document).ready(function () {
    //Page Load Start
    bannerSlide();
    gallery();


    var winW = $(window).width();

    if( winW > 768 ) {
        homePlanSlide();
        colSlide();
    }


    //Page Load End
    $('.gallery').featherlightGallery();

    $(document).ready(function()
    {
        $(document).resize();
    });
});


$(window).load(function () {
    var winW = $(window).width();

    if( winW <= 768 ) {
        homePlanSlide();
        colSlide();
    }
});

//Function Start
function bannerSlide() {
    var winH = $(window).height();
    //$('.banner-parallax').css('height', winH - 60);

    var isMulti = ($('#bannerSlide .item').length > 1) ? true : false
    $('#bannerSlide').owlCarousel({
        loop:isMulti,
        autoplay: true,
        autoplayTimeout: 4000,
        autoplaySpeed: 1000,
        margin:5,
        animateOut: 'fadeOut',
        nav:isMulti,
        dots: isMulti,
        video:true,
        lazyLoad:true,
        center:true,
        responsive:{
            0:{
                items:1
            },

        }
    });
}

function gridLayout5() {
    var wall = new Freewall("#tpl5-img");
    wall.reset({
        selector: '.item',
        animate: true,
        cellW: 300,
        cellH: 'auto',
        onResize: function() {
            wall.fitWidth();
        }
    });

    var images = wall.container.find('.item');
    images.find('img').load(function() {
        wall.fitWidth();
    });
}

function gallery() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        // var stagePadding = 40;
        // $('#tpl5-img').owlCarousel({
        //     loop:true,
        //     margin: 30,
        //     stagePadding: stagePadding,
        //     nav:true,
        //     dots: true,
        //     autoHeight: true,
        //     responsive:{
        //         0:{
        //             items:1
        //         },
        //         768:{
        //             items:1
        //         }
        //     }
        // });
    }
    else {
        gridLayout5();
    }
}


function homePlanSlide() {

    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        var stagePadding = 0;
    }
    else {
        var stagePadding = 0;
    }

    var isMulti = ($('#homePlanSlide .item').length > 1) ? true : false;
    var homeSlider = $("#homePlanSlide");
    homeSlider.owlCarousel({
        loop:isMulti,
        margin: 10,
        stagePadding: stagePadding,
        nav:isMulti,
        dots: isMulti,
        autoHeight: false,
        responsive:{
            0:{
                items:1
            }
        }
    });
    homeSlider.on("click", ".tabs-menu-plan-slide a", function () {
        var value = $(this).attr('data-value');
        homeSlider.trigger('to.owl.carousel', [value, 'fade']);
    });
}

function colSlide() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        var stagePadding = 25;
        var margin = -10;
        var autoH = true;
    }
    else {
        var stagePadding = 0;
        var margin = 10;
        var autoH = false;
    }

    var isMulti = ($('#colSlide .item').length > 1) ? true : false
    $('#colSlide').owlCarousel({
        loop:isMulti,
        margin: margin,
        stagePadding: stagePadding,
        nav:isMulti,
        dots: isMulti,
        autoHeight: autoH,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:1
            },
            979:{
                items:2
            },
            1199:{
                items:3
            }
        }
    });
}


//Function End