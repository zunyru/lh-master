$(document).ready(function () {
    //Page Load Start

    var isMulti = ($('#pjHomeBanner .item').length > 1) ? true : false;
    $('#pjHomeBanner').owlCarousel({
        loop:isMulti,
        autoplay: true,
        autoplayTimeout: 4000,
        autoplaySpeed: 1000,
        margin:5,
        animateOut: 'fadeOut',
        nav:false,
        dots: isMulti,
        video:true,
        lazyLoad:true,
        center:true,
        margin:0,
        autoHeight:false,
        responsive:{
            0:{
                items:1
            }
        },
        onChanged: function (event) {
            setTimeout(function(){
                if(countSlide == 0){
                    $('.slider').find('.center').addClass('animate');
                }else{
                    $('.slider').find('.center').addClass('animate3');
                }
            }, 1000);
            setTimeout(function(){
                if(countSlide == 0) {
                    $('.slider').find('.center').addClass('animate2');
                }
            }, 1000);
            // setTimeout(function(){
            //     $('#img_logo').fadeOut('slow');
            //     $('#desc_logo').fadeOut('slow');
            // }, 7000);
            setTimeout(function () {
                if(countSlide == 0) {
                    $('.slider').find('.center').addClass('animate3');
                    countSlide++;
                }
            },1000);
        }
    });


    projectPromoSlide();
    stickyMenuProject();
    scrollNext();

    projectplanImg();
    prjProgress();
    radionAppoint();

    var winW = $(window).width();

    if( winW > 768 ) {
        gallery();
        slideFacilityImg();
    }

     $('#date_appointment').datepicker({
        format: "dd/mm/yy",
        startDate: new Date(),
        todayHighlight: true,
    });
    $('.gallery').featherlightGallery();

    colSlide();

    $(document).ready(function()
    {
        $(document).resize();
    });

    //Page Load End
});

$(window).load(function () {
    var winW = $(window).width();

    if( winW <= 768 ) {
        gallery();
        slideFacilityImg();
    }
});


//Function Start
function scrollNext() {
    $("#scrollNext").click(function() {
        $('html, body').animate({
            scrollTop: $("#sec-information").offset().top - 60
        }, 800);
    });
}
function gridLayout5() {
    var wall = new Freewall("#tpl5-img");
    wall.reset({
        selector: '.item',
        animate: true,
        cellW: 300,
        cellH: 'auto',
        onResize: function() {
            wall.fitWidth();
        }
    });

    var images = wall.container.find('.item');
    images.find('img').load(function() {
        wall.fitWidth();
    });
}

function gallery() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        //var stagePadding = 40;
        $('#tpl5-img').owlCarousel({
            loop:true,
            margin: 30,
            autoplay:true,
            autoplayTimeout:3000,
            autoplayHoverPause:true,
            //stagePadding: stagePadding,
            nav:true,
            dots: false,
            autoHeight: true,
            responsive:{
                0:{
                    items:1
                },
                768:{
                    items:1
                }
            }
        });
    }
    else {
        gridLayout5();
    }
}

function projectPromoSlide() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        //var stagePadding = 40;
        // var autoH = true;
    }
    else {
        //var stagePadding = 0;
        // var autoH = false;
    }

    // $('#projectPromoSlide').owlCarousel({
    //     loop:true,
    //     margin: 20,
    //     //stagePadding: stagePadding,
    //     nav:true,
    //     dots: true,
    //     // autoHeight: autoH,
    //     responsive:{
    //         0:{
    //             items:1
    //         }
    //     }
    // });
}


function slideFacilityImg() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        var stagePadding = 25;
        var margin = -10;
    }
    else {
        var stagePadding = 0;
        var margin = 10;
    }

    var isMulti = ($('#facilityImg .item').length > 3) ? true : false;
    $('#facilityImg').owlCarousel({
        loop:isMulti,
        margin: margin,
        stagePadding: stagePadding,
        nav:isMulti,
        dots: isMulti,
        autoHeight: false,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:1
            },
            979:{
                items:2
            },
            1199:{
                items:3
            }
        }
    });
}

function projectplanImg() {
    var isMulti = ($('#projectplanImg .item').length > 1) ? true : false;
    $('#projectplanImg').owlCarousel({
        loop:false,
        autoplay: false,
        autoplayTimeout: 4000,
        autoplaySpeed: 1000,
        margin:5,
        nav:isMulti,
        dots: isMulti,
        video:true,
        lazyLoad:true,
        center:true,
        responsive:{
            0:{
                items:1
            },

        }
    });
    isMulti = ($('#roomplanImg .item').length > 1) ? true : false;
    $('#roomplanImg').owlCarousel({
        loop:false,
        autoplay: false,
        autoplayTimeout: 4000,
        autoplaySpeed: 1000,
        margin:5,
        nav:isMulti,
        dots: isMulti,
        video:true,
        lazyLoad:true,
        center:true,
        responsive:{
            0:{
                items:1
            },

        }
    });
}


function prjProgress() {
    var isMulti = ($('#projProgress .item').length > 4) ? true : false;
    $('#projProgress').owlCarousel({
        loop:false,
        autoplay: false,
        autoplayTimeout: 4000,
        autoplaySpeed: 1000,
        margin:15,
        animateOut: 'fadeOut',
        nav:isMulti,
        dots: isMulti,
        lazyLoad:true,
        slideBy: 4,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:1
            },
            979:{
                items:4
            },
            1199:{
                items:4
            }

        }
    });
}

function stickyMenuProject() {
    var winH = $(window).height();
    var pageH = winH - 70

    $('#main-nav li a').click(function() {
        $('#main-nav li a').removeClass('active');
        var id = $(this).attr('id');
        $(this).addClass('active');
        // console.log(id);
        var margin_top_animate = $(this).attr('data-family') == "child" ? 115 : 70;
        $('html, body').animate({
            scrollTop: $('#sec-'+id).offset().top - margin_top_animate
        }, 1000);
    });


}

function toLocation() {
    window.open('https://maps.google.com?saddr=Current+Location&dirflg=d&daddr='+$('#lat').val()+','+$('#lng').val(),'_blank');
}
function toLocationByWalk() {
    window.open('https://maps.google.com?saddr=Current+Location&dirflg=w&daddr='+$('#lat').val()+','+$('#lng').val(),'_blank');
}

function radionAppoint() {

    $('.radio-checkmark input[type="radio"]').click(function() {
        if($('#check-appoint').is(':checked')) {
            $('input#date_appointment').css('display','block');
        }
        else {
            $('input#date_appointment').css('display','none');
        }
    });
}

function colSlide() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        var stagePadding = 25;
        var margin = -10;
        var autoH = true;
    }
    else {
        var stagePadding = 0;
        var margin = 10;
        var autoH = false;
    }

    var isMulti = ($('#colSlide.owl-carousel img').length > 1) ? true : false
    $('#colSlide').owlCarousel({
        loop:isMulti,
        margin: margin,
        stagePadding: stagePadding,
        nav:isMulti,
        dots: isMulti,
        autoHeight: autoH,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:1
            },
            979:{
                items:2
            },
            1199:{
                items:3
            }
        }
    });
}

//Function End