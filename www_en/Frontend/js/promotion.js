$(document).ready(function () {
    //Page Load Start
    scrollNext();
    masonryList();
    var winW = $(window).width();

    if( winW > 768 ) {
        col2Slide();
    }

    $(document).ready(function()
    {
        $(document).resize();
    });
    //Page Load End
});


$(window).load(function () {
    var winW = $(window).width();

    if( winW <= 768 ) {
        col2Slide();
    }
});

//Function Start
function scrollNext() {
    $("#scrollNext").click(function() {
        $('html, body').animate({
            scrollTop: $("#content").offset().top
        }, 800);
    });
}

function masonryList() {
    var wall = new Freewall(".masonry-lists");
    wall.reset({
        selector: '.list-item',
        animate: true,
        cellW: 180,
        cellH: 'auto',
        onResize: function() {
            wall.fitWidth();
        }
    });

    wall.container.find('.list-item img').load(function() {
        wall.fitWidth();
    });
}

function col2Slide() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        var stagePadding = 25;
        var margin = -10;
        var autoH = true;
    }
    else {
        var stagePadding = 0;
        var margin = 10;
        var autoH = false;
    }

    var isMulti = ($('#col2Slide .item').length > 3) ? true : false;
    $('#col2Slide').owlCarousel({
        loop:isMulti,
        margin: margin,
        stagePadding: stagePadding,
        nav:isMulti,
        dots: isMulti,
        autoHeight: autoH,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:2
            },
        }
    });
}

//Function End