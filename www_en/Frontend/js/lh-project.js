$(document).ready(function () {
    //Page Load Start
    var winW = $(window).width();

    if( winW > 768 ) {
        lhpSlide1();
        lhpSlide2();
        lhpSlide3();
    }

    //Page Load End
});

$(window).load(function () {
    var winW = $(window).width();

    if( winW <= 768 ) {
        lhpSlide1();
        lhpSlide2();
        lhpSlide3();
    }
});


//Function Start
function lhpSlide1() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        var stagePadding = 25;
        var margin = -10;
        var autoH = true;
        var slideBy = 1;
        var loop = true;
    }
    else {
        var stagePadding = 0;
        var margin = 10;
        var autoH = false;
        var slideBy = 3;
        var loop = false;
    }

    var isMulti = ($('#lhpSlide1.owl-carousel img').length > 1) ? true : false;
    $('#lhpSlide1').owlCarousel({
        loop:loop,
        margin: margin,
        stagePadding: stagePadding,
        nav:true,
        dots: true,
        slideBy: slideBy,
        autoHeight: autoH,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:1
            },
            979:{
                items:2
            },
            1199:{
                items:3
            }
        }
    });

}

function lhpSlide2() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        var stagePadding = 25;
        var margin = -10;
        var autoH = true;
        var slideBy = 1;
        var loop = true;
    }
    else {
        var stagePadding = 0;
        var margin = 10;
        var autoH = false;
        var slideBy = 3;
        var loop = false;
    }

    var isMulti = ($('#lhpSlide2.owl-carousel img').length > 1) ? true : false;
    $('#lhpSlide2').owlCarousel({
        loop:loop,
        margin: margin,
        stagePadding: stagePadding,
        nav:false,
        dots: false,
        slideBy: slideBy,
        autoHeight: autoH,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:1
            },
            979:{
                items:2
            },
            1199:{
                items:3
            }
        }
    });

}

function lhpSlide3() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        var stagePadding = 25;
        var margin = -10;
        var autoH = true;
        var slideBy = 1;
        var loop = true;
    }
    else {
        var stagePadding = 0;
        var margin = 10;
        var autoH = false;
        var slideBy = 3;
        var loop = false;
    }

    var isMulti = ( $('#lhpSlide3.owl-carousel .owl-item').length > 1) ? true : false;
    $('#lhpSlide3').owlCarousel({
        loop:loop,
        margin: margin,
        stagePadding: stagePadding,
        nav:true,
        dots: true,
        slideBy: slideBy,
        autoHeight: autoH,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:1
            },
            979:{
                items:2
            },
            1199:{
                items:3
            }
        }
    });

}


//Function End