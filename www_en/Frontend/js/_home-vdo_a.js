$(document).ready(function () {
    //Page Load Start
    bannerSlideHome();
    slideHomeCentertxt();
    vdoControl();
    homeHightlight();
    homeCommunity();
    homeTips();
    homeReview();

    var winW = $(window).width();
    var winH = $(window).height();

    var pageH = winH - 60;

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= pageH) {
            $('.main-navigation').css({'opacity': 1, 'visibility':'visible', 'display':'block' });
        }
        else {
            $('.main-navigation').css({'opacity': 0, 'visibility':'hidden', 'display':'none' });
        }

    });

    //Page Load End
});


//Function Start
function bannerSlideHome() {
    var winH = $(window).height();
    // $('.banner-parallax').css('height', winH - 60);

    $('#homeBanner').owlCarousel({
        loop:false,
        autoplay: true,
        autoplayTimeout: 7000,
        autoplaySpeed: 1000,
        margin:5,
        animateOut: 'fadeOut',
        nav:false,
        dots: true,
        video:true,
        lazyLoad:true,
        center:true,
        autoHeight: true,
        responsive:{
            0:{
                items:1
            },

        }
    });
}

function slideHomeCentertxt() {
    var winH = $(window).height();
    var slideTxt = $('.banner-txt').height();
    var center = (winH - 290) / 2;
    $('.banner-txt').css('top',center);

}

function vdoControl() {
    var winW = $(window).width();
    var winH = $(window).height();

    $('.rps-vdobg').css('height',winH);

    var pageH = winH - 170;
    $('.i-mute').css('top',pageH);

    // reponsive
    if( winW < 992 ) {
        var funBlockH = winH - 190;
        $('.i-mute').css('top',funBlockH);
    }

    $(window).resize(function(){
        var winW = $(window).width();
        var winH = $(window).height();
        var pageH = winH - 170;
        $('.i-mute').css('top',pageH);


        if( winW < 992 ) {
            var funBlockH = winH - 190;
            $('.i-mute').css('top',funBlockH);
        }

    });



    $('#vdoPlay').click(function(){
        $('#slideVdo')[0].play();
        $(this).fadeOut(200);
        $('#vdoPause').fadeIn(200);
        $('.rps-vdobg').fadeOut(200);

        $(".vdo-control").mouseenter(function(event) {
            event.stopPropagation();
            $('#vdoPause').addClass("btnshown");
        }).mouseleave(function(event)
        {
            event.stopPropagation();
            $('#vdoPause').removeClass("btnshown");
        });
    });

    $("#vdoPause").click(function(){
        $('#slideVdo').get(0).pause();
        $(this).fadeOut(200);
        $('#vdoPlay').fadeIn(200);
    });

    //Mute & Unmute
    $("video#slideVdo").prop('muted', false);

    $("#vdoMute").click( function (){
        if( $("video#slideVdo").prop('muted', false) )
        {
            $("video#slideVdo").prop('muted', true);
            $(this).fadeOut(200);
            $("#vdoUnmute").fadeIn(200).css('display', 'block');
        }
    });

    $("#vdoUnmute").click( function (){
        if( $("video#slideVdo").prop('muted', true) )
        {
            $("video#slideVdo").prop('muted', false);
            $(this).fadeOut(200);
            $("#vdoMute").fadeIn(200).css('display', 'block');
        }
    });

}

function homeHightlight() {
    var winW = $(window).width();

    // responsive
    if( winW < 768 ) {
        var stagePadding = 20;
        var margin = 0;
        var autoH = true;
    }
    else {
        var stagePadding = 0;
        var margin = 0;
        var autoH = true;
    }

    $('#homeHighlightSlide').owlCarousel({
        loop:true,
        margin: -17,
        stagePadding: stagePadding,
        nav:true,
        dots: true,
        autoHeight: autoH,
        responsive:{
            0:{
                items:1
            }
        }
    });
}

function homeCommunity() {
    var winW = $(window).width();

    // responsive
    // if( winW < 768 ) {
    //     var stagePadding = 50;
    // }
    // else {
    //     var stagePadding = 0;
    // }

    $('#homeCommunitySlide').owlCarousel({
        loop:false,
        margin: 40,
        //stagePadding: stagePadding,
        nav:true,
        dots: true,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:1
            },
            1199:{
                items:2
            }
        }
    });
}

function homeTips() {
    var winW = $(window).width();

    // responsive
    if( winW < 768 ) {
        var stagePadding = 50;
    }
    else {
        var stagePadding = 0;
    }

    $('#homeTipsSlide').owlCarousel({
        loop:true,
        margin: 10,
        stagePadding: stagePadding,
        nav:true,
        dots: true,
        autoHeight: true,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:1
            },
            979:{
                items:2
            },
            1199:{
                items:3
            }
        }
    });
}

function masonryList() {
    var wall = new Freewall(".review-lists");
    wall.reset({
        selector: '.list-item',
        animate: true,
        cellW: 180,
        cellH: 'auto',
        onResize: function() {
            wall.fitWidth();
        }
    });

    wall.container.find('.list-item img').load(function() {
        wall.fitWidth();
    });
}

function homeReview() {
    var winW = $(window).width();

    // responsive
    if( winW < 768 ) {
        var stagePadding = 40;
        $('.review-lists').owlCarousel({
            loop:true,
            margin: 30,
            //stagePadding: stagePadding,
            nav:true,
            dots: true,
            autoHeight: true,
            responsive:{
                0:{
                    items:1
                },
                768:{
                    items:1
                }
            }
        });
    }
    else {
        masonryList();
    }



}


//Function End