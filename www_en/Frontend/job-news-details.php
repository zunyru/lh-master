<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

$news_result = getJobNewsDetailsByID($_GET['id']);
function getJobNewsDetailsByID($news_id)
{
    try {
        $sql = "SELECT * FROM JOB_NEWS WHERE ID={$news_id}";
        $result = mssql_fetch_object(mssql_query($sql , $GLOBALS['db_conn']));
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}
?>
<?php include('header.php'); ?>
    <!-- CSS -->
    <link rel="stylesheet" href="<?= file_path('css/job-news.css') ?>" type="text/css">


<!--     <script src="<?= file_path('js/lib/angular.min.js') ?>"></script>
    <script src="<?= file_path('js/lib/angular-sanitize.js') ?>" ></script>
    <script src="<?= file_path('js/cal_reg/appjob.js') ?>"></script> -->

   <!--  <div id="content" class="content" ng-app="JobApp"  ng-controller="newsDetailController" > -->
    <div id="content" class="content" >
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div>
                        <h1>ข่าวสารสมัครงาน</h1>

                        <div class="jobnews-img">
                            <img src="<?=file_path('../Backend/fileupload/news/'.$news_result->NEWS_PIC);?>" alt="">
                        </div>

                        <h2 class="jobnews-title"><?=$news_result->TITLE;?></h2>
                        <p>

                        </p>
                        <br/>
                        <p ng-bind-html="news_detail.DETAIL"><?=$news_result->DETAIL;?></p>
                        <br/>
                        <p>วันที่โพส  <?=date( 'd/m/Y',strtotime($news_result->PUBLISH_DAT));?></p>
                    </div>
                    <!-- <a href="th/job" class="btn-backjob">< Back</a> -->

                </div>
            </div>
        </div>
    </div>

<?php include('footer.php'); ?>
