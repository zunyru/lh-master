
<?php

//require_once 'include/help/begin.php';
//require_once 'include/help/query_function.php';

?>
<?php include('header.php'); ?>
    
    <!-- CSS -->
    <link rel="stylesheet" href="js/cal_reg/css/job.css" type="text/css">

    <!-- JS -->
    <script src="js/lib/angular.min.js"></script>
    <script src="js/cal_reg/appjob.js"></script>
    <script src="js/cal_reg/job.js"></script>
    <script src="js/banner-md.js"></script>


    <style>
        .select-test select {
            background: transparent;
            border: none;
            width: 300px;
        }

        .select-test {
            background: #fff url(./images/news/i_sort_blk.png) no-repeat 95% center;
            cursor: pointer;
            font-size: 22px;
            padding: 8px 15px;
            position: relative;
            width: 300px;
            height: 42px;
            border-radius: 6px;
            border: 1px solid #dedede;
        }
    </style>


    <div class="page-banner-md banner-hide">
        <div id="bannerSlide">
            <div class="item">
                <div class="banner-parallax" style="background-image: url(images/temp3/banner_job.jpg);">
                </div>
            </div>
        </div>
    </div>

    <div id="content" class="content" ng-app="JobApp"  ng-controller="myController">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div>
                        <h1 class="heading-title" style="text-align: center;">ร่วมงานกับเรา</h1>

                        <div id="tabs-container" class="tabs-container">
                            <div class="tab-dd dropdown">
                                <ul class="tabs-menu">
                                    <li class="item current">
                                        <a href="#tab-1">
                                            ตำแหน่งงานที่เปิดรับสมัคร
                                        </a>
                                    </li>
                                    <li class="item">
                                        <a href="#tab-2">
                                            สวัสดิการที่คุณจะได้รับ
                                        </a>
                                    </li>
                                    <li class="item">
                                        <a href="#tab-3">
                                            ประสบการณ์ที่คุณจะได้จากเรา
                                        </a>
                                    </li>
                                    <li class="item">
                                        <a href="#tab-4">
                                            ข่าวสารสมัครงาน
                                        </a>
                                    </li>
                                    <li class="item">
                                        <a href="" onclick="location.href='<?php url('/contact-us.php'); ?>';">
                                            ติดต่อเรา
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="select-container" id="choose-location">
                                <div class="select-block selectProvince">
                                    <input type="hidden" name="selectProvince" value="">
                                    <div class="select-test">
                                        <select   ng-model="filter.provinces"  ng-change="filter.area=undefined" >
                                            <option value="{{undefined}}"  ng-selected="true" >เลือกดูตามจังหวัด</option>
                                            <option ng-repeat="dataArr in provinces" value="{{dataArr.PROVINCE_ID}}" >{{ dataArr.PROVINCE_NAME }}</option>
                                          </select>
                                    </div>
                                </div>

                                <div class="select-block selectArea">
                                    <input type="hidden" name="selectArea" value="">
                                    <div class="select-test">
                                        <select  ng-model="filter.area" >
                                            <option value="{{undefined}}" ng-selected="true" >เลือกดูตามพื้นที่ทำงาน</option>
                                            <option  ng-if="filter.provinces" ng-repeat="dataArr in area | filter:{PROVINCE_ID: filter.provinces} " value="{{dataArr.LOCATION_ID}}" >{{ dataArr.LOCATION_NAME }}</option>
                                          </select>
                                    </div>
                                </div>
                            </div>

                            <div class="tab">
                                <div id="tab-1" class="tab-content">
                                    <div class="tab-block">
                                        <div class="form current" id="table">
                                            <table class="project-search-list" width="100%">
                                                <thead>
                                                <tr>
                                                    <th>
                                                        <span class="hide-rps">วันที่ประกาศ</span>
                                                        <span class="hide-dsktp">วันที่</span>
                                                    </th>
                                                    <th>
                                                        <span class="hide-rps">ตำแหน่งงานว่าง</span>
                                                        <span class="hide-dsktp">ตำแหน่ง</span>
                                                    </th>
                                                    <th class="hide-dsktp">จังหวัด</th>
                                                    <th class="hide-rps">พื้นที่ทำงาน</th>
                                                    <th>
                                                        <span class="hide-rps">อัตราที่เปิดรับ</span>
                                                        <span class="hide-dsktp">อัตรา</span>
                                                    </th>
                                                    <th>
                                                        <span class="hide-rps">สมัครงานตำแหน่งนี้</span>
                                                        <span class="hide-dsktp">สมัครงาน</span>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <tr ng-repeat="position in positions | filter: {'PROVINCE_ID': filter.provinces} | filter:{'LOCATION_ID': filter.area}">
                                                    <td>{{ parseDate(position.POSTED_DAT) | date:'dd/MM/yyyy' }}</td>
                                                    <td>{{ position.TITLE }}</td>
                                                    <td class="txt-center">{{ position.PROVINCE_NAME }}</td>
                                                    <td class="hide-rps">{{ position.LOCATION_NAME }}</td>
                                                    <td class="txt-center">{{ position.RATES }}</td>
                                                    <td class="txt-center"><a class="formJob"  ng-click="test(position.ID)">สมัครงาน</a></td>
                                                </tr>


                                              
                                                <!-- <tr>
                                                    <td>14/10/2016</td>
                                                    <td>เจ้าหน้าที่การตลาด (Marketing)</td>
                                                    <td class="txt-center">กรุงเทพฯและปริมณฑล</td>
                                                    <td class="hide-rps">กรุงเทพฯและปริมณฑล</td>
                                                    <td class="txt-center">2</td>
                                                    <td class="txt-center"><a id="formJob">สมัครงาน</a></td>
                                                </tr>  -->
                                                </tbody>
                                            </table>

                                            <a href="" class="btn btn-submit">SEE MORE</a>
                                        </div>

                                        <div class="job-form" tyle="display: none;">




                                            <!-- ======================================= -->

    <div class="apply-job-container">

   

    <form action="" class="apply-job-form" name=jobform id="jobform" ng-submit="myFunc()"  novalidate>
        <div class="row">
         
            <ul id="progressbar">
                <li class="active">ข้อมูลส่วนตัว</li>
                <li>ข้อมูลการทำงาน</li>
                <li>ข้อมูลอื่นๆ</li>
            </ul>

        </div>
     <a id="backJobListsBtm" class="btn-backjob">&lt; Back</a>
        <fieldset>
            <div class="form-title">ข้อมูลส่วนตัว</div>
            <div class="personal_info form-section">           
                <div class="row">
                    <div class="col-md-6 form-row">
                         <div class="col-md-12 col-sm-3"><label>ชื่อ<span class="f-required">*</span></label></div>
                        <div class="col-md-4">
                            <div class="select-block">
                               <select name="cmbTitle" id="cmbTitle" ng-model=data.jobcmbTitle required> 
                                    <option value="" disabled>คำนำหน้าชื่อ</option>
                                    <option value="นาย">นาย</option>
                                    <option value="นาง">นาง</option>
                                    <option value="นางสาว">นางสาว</option>
                                </select>
                            </div>
                            <span ng-show="!jobform.cmbTitle.$pristine && jobform.cmbTitle.$error.required" style="color : red">โปรดใส่คำนำหน้าชื่อ</span>
                        </div><br>
                        
                        <div class="col-md-8">
                            <input type="text" class="textbox" name="txbFName" id="txbFName" ng-model="data.jobtxbFName"value="" maxlength="50" required>
                            <span ng-show="!jobform.txbFName.$pristine && jobform.txbFName.$error.required" style="color : red" >
                            โปรดกรอกชื่อจริง</span>
                        </div>

                    </div>
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>นามสกุล<span class="f-required">*</span></label></div>
                        <div class="col-md-12">
                            <input type="text" class="textbox" name="txbLName" id="txbLName" ng-model="data.jobtxbLName" value="" maxlength="50" required>
                            <span ng-show="!jobform.txbLName.$pristine && jobform.txbLName.$error.required" style="color : red" >
                            โปรดกรอกนามสกุล</span>
                        </div>
                        
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>ชื่อเล่น<span class="f-required">*</span></label></div>
                        <div class="col-md-12">
                            <input type="text" class="textbox w100-m" name="txbNickname" id="txbNickname" ng-model="data.jobtxbNickname" maxlength="30" value="" required>
                             <span ng-show="!jobform.txbNickname.$pristine && jobform.txbNickname.$error.required" style="color : red" >โปรดกรอกนามสกุล</span>
                        </div>
                    </div> 
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>เพศ<span class="f-required">*</span></label></div>
                        <div class="col-md-8">
                            <input type="radio" name="rdoGender" id="rdoGenderM" ng-model="data.jobrdoGender" 
                            value="M" required> <label for="rdoGenderM"><span></span>ชาย</label> &nbsp; &nbsp;
                            &nbsp; &nbsp;
                            <input type="radio" name="rdoGender" id="rdoGenderF" ng-model="data.jobrdoGender" 
                            value="F" required> <label for="rdoGenderF"><span></span>หญิง</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                      <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>วันเกิด<span class="f-required">*</span></label></div>
                        <div class="col-md-12 form-group has-feedback">
                            <input type="text" name="dateBirth" id="dateBirth" ng-model="data.jobdateBirth" 
                            class="w100-m hasDatepicker" readonly value="" required>
                            <span class="glyphicon glyphicon-calendar form-control-feedback" id="iconhbd" ></span> 
                        </div>
                    </div>

                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>ภูมิลำเนา<span class="f-required">*</span></label></div>
                        <div class="col-md-12">
                            <div class="select-block">
                                <select name="cmbBirthProvince" id="cmbBirthProvince" ng-model="data.jobcmbBirthProvince"  class="w100-m" required>
                                    <option value="" disabled>จังหวัด</option>
                                    <option value="{{ province.PROVINCE_ID }}" 
                                    ng-repeat="province in provinces">{{ province.PROVINCE_NAME }}</option>
                                </select>
                            </div>
                            <span ng-show="!jobform.cmbBirthProvince.$pristine && jobform.cmbBirthProvince.$error.required" style="color : red" > โปรดเลือกภูมิลำเนา </span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>ส่วนสูง<span class="f-required">* </span>(เซนติเมตร) </label></div>
                        <div class="col-md-12">
                            <input type="number" class="textbox" name="txbHeight" id="txbHeight" ng-model="data.jobtxbHeight" 
                              value="" min="0" ng-maxlength="3" ng-minlength="2" required> 
                            <span ng-show="!jobform.txbHeight.$pristine && jobform.txbHeight.$error.required" style="color : red" >
                          โปรดกรอกส่วนสูง </span>
                            <span ng-show="jobform.txbHeight.$error.minlength || jobform.txbHeight.$error.maxlength" 
                            style="color : red;" > โปรดกรอกส่วนสูงให้ถูกต้อง </span>                      
                        </div>   
                    </div>
                    
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>น้ำหนัก<span class="f-required">* </span>(กิโลกรัม) </label>
                        </div>  
                        <div class="col-md-12">
                            <input type="number" class="textbox" name="txbWeight" id="txbWeight"  ng-maxlength="5" min="0" ng-model="data.jobtxbWeight" required>
                            <span ng-show="!jobform.txbWeight.$pristine && jobform.txbWeight.$error.required" 
                            style="color : red" >
                          โปรดกรอกน้ำหนัก </span>
                            <span ng-show="jobform.txbWeight.$error.minlength || jobform.txbWeight.$error.maxlength" style="color : red" > โปรดกรอกน้ำหนักให้ถูกต้อง </span>
                        </div>
                    </div>
                </div>
               
                <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>ศาสนา<span class="f-required">*</span></label></div>
                        <div class="col-md-12">
                            <div class="select-block">
                                <select name="cmbReligion" id="cmbReligion" ng-model="data.jobcmbReligion" required>
                                    <option value="" disabled>ศาสนา</option>
                                    <option value="พุทธ">พุทธ</option>
                                    <option value="คริสต์">คริสต์</option>
                                    <option value="อิสลาม">อิสลาม</option>
                                    <option value="อื่นๆ">อื่น ๆ</option>
                                </select>
                            </div>
                            <span ng-show="!jobform.cmbReligion.$pristine && jobform.cmbReligion.$error.required" style="color : red" >โปรดเลือกศาสนา </span>
                        </div>
                    </div>
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"> <label> อื่น ๆ โปรดระบุ </label></div>
                        <div class="col-md-12">
                            <input type="text" class="textbox" name="txbReligionOther" 
                            id="txbReligionOther" maxlength="30"  ng-disabled="data.jobcmbReligion!='อื่นๆ'" 
                            ng-model = "data.txbReligionOther">
                        </div>
                    </div>
                </div>
                
                <div class="row">    
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>เชื้อชาติ</label></div> 
                        <div class="col-md-2">
                            <input type="radio" name="rdoNationality" id="rdoNationalityT" ng-model="data.jobrdoNationality" value="ไทย" checked=""> <label for="rdoNationalityT"><span></span>ไทย</label>
                        </div>
                        <div class="col-md-3"> 
                            <input type="radio" name="rdoNationality" id="rdoNationalityO" ng-model="data.jobrdoNationality" value="อื่นๆ"><label for="rdoNationalityO" class="txtOther">
                            <span></span>อื่นๆ ระบุ</label>                
                        </div>
                        <div class="col-md-7">
                            <input type="text" class="textbox" name="txbNationalityOther" ng-disabled="data.jobrdoNationality!='อื่นๆ'" id="txbNationalityOther" maxlength="30"  ng-model="data.txbNationalityOther">
                        </div>
                    </div>

                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>สัญชาติ</label></div>
                        <div class="col-md-2">
                            <input type="radio" name="rdoRace" id="rdoRaceT" value="ไทย" ng-model="data.jobrdoRace" 
                            > <label for="rdoRaceT"><span></span>ไทย</label>
                        </div>
                        <div class="col-md-3">
                            <input type="radio" name="rdoRace" id="rdoRaceO" value="อื่นๆ" ng-model="data.jobrdoRace">  <label for="rdoRaceO" class="txtOther"><span></span>อื่นๆ ระบุ</label>
                        </div>
                        <div class="col-md-7">
                             <input type="text" class="textbox" name="txbRaceOther" id="txbRaceOther" 
                             maxlength="30" ng-model="data.txbRaceOther" value="f" ng-disabled="data.jobrdoRace!='อื่นๆ'">
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>เกณฑ์ทหาร</label></div>
                        <div class="col-md-12">
                            <div class="select-block">
                                <select name="cmbConscription" id="cmbConscription" 
                                ng-model="data.jobcmbConscription">
                                    <option value="" disabled>เลือก</option>
                                    <option value="ผ่อนผัน">ผ่อนผัน</option>
                                    <option value="เกณฑ์แล้ว">เกณฑ์แล้ว</option>
                                    <option value="จบ ร.ด.">จบ ร.ด.</option>
                                    <option value="ยกเว้น">ยกเว้น</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>เลือกยกเว้น โปรดระบุเหตุผล </label></div>
                        <div class="col-md-12">
                        <input type="text" class="textbox w100-m" name="txbConscription" 
                        id="txbConscription" maxlength="100" ng-disabled="data.jobcmbConscription!='ยกเว้น'"
                         value="" ng-model="data.txbConscription" ></div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label class="text-left">เลขบัตรประจำตัวประชาชน<span class="f-required">*</span></label></div>
                        <div class="col-md-12">
                            <input type="number" class="textbox" name="txbIDNo" id="txbIDNo" ng-model="data.jobtxbIDNo"  min="0" ng-maxlength="13" ng-minlength="13" required> 
                            <span ng-show="!jobform.txbIDNo.$pristine && jobform.txbIDNo.$error.required" style="color : red" > โปรดกรอกเลขบัตรประจำตัวประชาชน </span>  <span ng-show="jobform.txbIDNo.$error.minlength || jobform.txbIDNo.$error.maxlength" style="color : red" >โปรดกรอกเลขบัตรประจำตัวประชาชนให้ถูกต้อง </span>                
                        </div>  
                            <div class="col-md-12" style="font-size: 19px;"> โปรดระบุตัวเลข 13 หลัก โดยไม่มีช่องว่าง หรือ - </div>
                     </div>               
                </div>         
            </div>


            <div class="form-title">ข้อมูลครอบครัว</div>
            <div class="family_info form-section">
                <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-12">
                            <label>สถานภาพสมรส</label>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-6">
                            <input type="radio" name="rdoMarried" id="rdoMarriedS"  ng-model="data.jobrdoMarried" value="โสด"> <label for="rdoMarriedS"><span></span>โสด</label>
                         </div>
                         <div class="col-md-4 col-sm-4 col-xs-6"> 
                            <input type="radio" name="rdoMarried" id="rdoMarriedM" ng-model="data.jobrdoMarried" value="สมรส"> <label for="rdoMarriedM"><span></span>สมรส</label>
                         </div>
                         <div class="col-md-4 col-sm-4 col-xs-6">
                            <input type="radio" name="rdoMarried" id="rdoMarriedD" ng-model="data.jobrdoMarried" value="หย่าร้าง"> <label for="rdoMarriedD"><span></span>หย่าร้าง</label>
                        </div>
                    </div>
                </div>
                <div class="row title">
                     <div class="col-md-12 form-row">
                            <div class="col-md-4"><label>ครอบครัว</label></div> 
                    </div>
                </div>
                
                <div class="row hide-mobile">
                    <div class="col-md-12 form-row">
                    <div class="col-md-2"></div>
                    <div class="col-md-3">ชื่อ-นามสกุล</div>
                    <div class="col-md-1">อายุ</div>
                    <div class="col-md-3">อาชีพ/ตำแหน่ง</div>
                    <div class="col-md-3">ที่อยู่/ที่ทำงาน</div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 form-row">
                        <div class="col-md-2"><label>บิดา</label></div>
                        <div class="show-mobile col-md-4"><label>ชื่อนาม-นามสกุล
                        </label></div>
                        <div class="col-md-3"><input type="text" class="textbox" name="txbFatherName" id="txbFatherName" maxlength="50" ng-model="data.jobtxbFatherName" value=""></div>
                        <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อายุ</label>
                        </div>
                        <div class="col-md-1"><input type="number" class="textbox" name="txbFatherAge" id="txbFatherAge" maxlength="2" min="0" value="" ng-model="data.jobtxbFatherAge"></div>
                        <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อาชีพ/ตำแหน่ง
                        </label></div>
                        <div class="col-md-3"><input type="text" class="textbox" name="txbFatherOcc" id="txbFatherOcc" maxlength="100" ng-model="data.jobtxbFatherOcc" value=""></div>
                        <div class="show-mobile col-md-4"><label style="padding-top: 10px;">ที่อยู่/ที่ทำงาน
                        </label></div>
                        <div class="col-md-3"><input type="text" class="textbox" name="txbFatherAddr" id="txbFatherAddr" maxlength="100" ng-model="data.jobtxbFatherAddr" value=""></div>
                   </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12 form-row">
                        <div class="col-md-2"><label>มารดา</label></div>
                        <div class="show-mobile col-md-4"><label>ชื่อนาม-นามสกุล</label></div>
                        <div class="col-md-3"><input type="text" class="textbox" name="txbMotherName" id="txbMotherName" maxlength="50" ng-model="data.jobtxbMotherName" value=""></div>
                        <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อายุ</label></div>
                        <div class="col-md-1"><input type="number" class="textbox" name="txbMotherAge" id="txbMotherAge" maxlength="2" min="0" value="" ng-model="data.jobtxbMotherAge"></div>
                        <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อาชีพ/ตำแหน่ง
                        </label></div>
                        <div class="col-md-3"><input type="text" class="textbox" name="txbMotherOcc" id="txbMotherOcc" maxlength="100" ng-model="data.jobtxbMotherOcc" value=""></div>
                        <div class="show-mobile col-md-4"><label style="padding-top: 10px;">ที่อยู่/ที่ทำงาน
                        </label></div>
                        <div class="col-md-3"><input type="text" class="textbox" name="txbMotherAddr" id="txbMotherAddr" maxlength="100" ng-model="data.jobtxbMotherAddr" value=""></div>
                   </div>
                </div>
                <div class="row">
                    <div class="col-md-12 form-row">
                        <div class="col-md-2"><label>พี่น้อง 1</label></div>
                        <div class="show-mobile col-md-4"><label>ชื่อนาม-นามสกุล</label></div>
                        <div class="col-md-3"><input type="text" class="textbox" name="txbCousin1Name" id="txbCousin1Name" maxlength="50" value="" ng-model="data.jobtxbCousin1Name"></div>
                        <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อายุ</label></div>
                        <div class="col-md-1"><input type="number" class="textbox" name="txbCousin1Age" id="txbCousin1Age" maxlength="2" min="0" value="" ng-model="data.jobtxbCousin1Age"></div>
                        <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อาชีพ/ตำแหน่ง
                        </label></div>
                        <div class="col-md-3"><input type="text" class="textbox" name="txbCousin1Occ" id="txbCousin1Occ" maxlength="100" value="" ng-model="data.jobtxbCousin1Occ"></div>
                        <div class="show-mobile col-md-4"><label style="padding-top: 10px;">ที่อยู่/ที่ทำงาน
                        </label></div>
                        <div class="col-md-3"><input type="text" class="textbox" name="txbCousin1Addr" id="txbCousin1Addr" maxlength="100" value="" ng-model="data.jobtxbCousin1Addr"></div>
                   </div>
                </div>
                <div class="row">
                    <div class="col-md-12 form-row">
                        <div class="col-md-2"><label>พี่น้อง 2</label></div>
                        <div class="show-mobile col-md-4"><label>ชื่อนาม-นามสกุล</label></div>
                        <div class="col-md-3"><input type="text" class="textbox" name="txbCousin2Name" id="txbCousin2Name" maxlength="50"  value="" ng-model="data.jobtxbCousin2Name"></div>
                        <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อายุ</label></div>
                        <div class="col-md-1"><input type="number" class="textbox" name="txbCousin2Age" id="txbCousin2Age" maxlength="2" value="" min="0" ng-model="data.jobtxbCousin2Age"></div>
                        <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อาชีพ/ตำแหน่ง
                        </label></div>
                        <div class="col-md-3"><input type="text" class="textbox" name="txbCousin2Occ" id="txbCousin2Occ" maxlength="100" value="" ng-model="data.jobtxbCousin2Occ"></div>
                        <div class="show-mobile col-md-4"><label style="padding-top: 10px;">ที่อยู่/ที่ทำงาน
                        </label></div>
                        <div class="col-md-3"><input type="text" class="textbox" name="txbCousin2Addr" id="txbCousin2Addr" maxlength="100" value="" ng-model="data.jobtxbCousin2Addr"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 form-row">
                        <div class="col-md-2"><label>พี่น้อง 3</label></div>
                        <div class="show-mobile col-md-4"><label>ชื่อนาม-นามสกุล</label></div>
                        <div class="col-md-3"><input type="text" class="textbox" name="txbCousin3Name" id="txbCousin3Name" maxlength="50" value="" ng-model="data.jobtxbCousin3Name"></div>
                        <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อายุ</label></div>
                        <div class="col-md-1"><input type="number" class="textbox" name="txbCousin3Age" id="txbCousin3Age" maxlength="2" min="0" value="" ng-model="data.jobtxbCousin3Age"></div>
                        <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อาชีพ/ตำแหน่ง
                        </label></div>
                        <div class="col-md-3"><input type="text" class="textbox" name="txbCousin3Occ" id="txbCousin3Occ" maxlength="100" value="" ng-model="data.jobtxbCousin3Occ"></div>
                        <div class="show-mobile col-md-4"><label style="padding-top: 10px;">ที่อยู่/ที่ทำงาน
                        </label></div>
                        <div class="col-md-3"><input type="text" class="textbox" name="txbCousin3Addr" id="txbCousin3Addr" maxlength="100" ng-model="data.jobtxbCousin3Addr" value=""></div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12 form-row">
                        <div class="col-md-2"><label>คู่สมรส</label></div>
                        <div class="show-mobile col-md-4"><label>ชื่อนาม-นามสกุล</label></div>
                        <div class="col-md-3"><input type="text" class="textbox" name="txbSpouseName" id="txbSpouseName" maxlength="50" value="" ng-model="data.jobtxbSpouseName"></div>
                        <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อายุ</label></div>
                        <div class="col-md-1"><input type="number" class="textbox" name="txbSpouseAge" id="txbSpouseAge" maxlength="2" value="" min="0" ng-model="data.jobtxbSpouseAge"></div>
                        <div class="show-mobile col-md-4"><label style="padding-top: 10px;">อาชีพ/ตำแหน่ง
                        </label></div>
                        <div class="col-md-3"><input type="text" class="textbox" name="txbSpouseOcc" id="txbSpouseOcc" maxlength="100" ng-model="data.jobtxbSpouseOcc" value=""></div>
                        <div class="show-mobile col-md-4"><label style="padding-top: 10px;">ที่อยู่/ที่ทำงาน
                        </label></div>
                        <div class="col-md-3"><input type="text" class="textbox" name="txbSpouseAddr" id="txbSpouseAddr" maxlength="100" ng-model="data.jobtxbSpouseAddr" value=""></div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12 form-row">
                        <div class="col-md-2"><label>จำนวนบุตร (คน)</label></div>
                        <div class="col-md-3"><input type="number" class="textbox" name="txbChildNum" id="txbChildNum" maxlength="2" min="0" value="" ng-model="data.jobtxbChildNum"></div>
         
                    </div>
                </div>
            </div>

            <div class="form-title">ข้อมูลการติดต่อ</div>
            <div class="contact_info form-section">
                <div class="row">
                    <div class="col-md-12 form-row"><div class="col-md-6 title" style="font-size:22px;">ที่อยู่ปัจจุบันที่ติดต่อได้</div></div>
                </div>
                
                <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>ที่อยู่<span class="f-required">*</span></label></div>
                        <div class="col-md-12">
                            <input type="text" class="textbox" name="txbAddress" id="txbAddress" ng-model="data.jobtxbAddress" maxlength="255" value="" required>
                            <span ng-show="!jobform.txbAddress.$pristine && jobform.txbAddress.$error.required" style="color : red" > โปรดกรอกที่อยู่</span>   
                        </div>
                    </div>

                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>จังหวัด<span class="f-required">*</span></label></div>
                        <div class="col-md-12">
                            <div class="select-block">
                                <select name="cmbContactProvince" id="cmbContactProvince" 
                                ng-model="data.jobcmbContactProvince" 
                                class="w100-m" required>
                                    <option value="" disabled>จังหวัด</option>
                                    <option value="{{ province.PROVINCE_ID }}" ng-repeat="province in provinces">{{ province.PROVINCE_NAME }}</option>
                                </select>
                            </div>
                              <span ng-show="!jobform.cmbContactProvince.$pristine && jobform.cmbContactProvince.$error.required" style="color : red" > โปรดเลือกจังหวัด </span>      
                        </div>
                    </div>
                </div>
              
                <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>เบอร์โทรศัพท์ที่สามารถติดต่อได้<span class="f-required">*</span></label></div>
                        <div class="col-md-12">
                            <input type="number" class="textbox w100-m" name="txbContactTel" id="txbContactTel" 
                            ng-model="data.jobtxbContactTel" ng-minlength="10" ng-maxlength="10" 
                            min="0" required>

                             <span ng-show="!jobform.txbContactTel.$pristine && jobform.txbContactTel.$error.required" style="color : red" > โปรดกรอกเบอร์โทรศัพท์ที่สามารถติดต่อได้ </span>
                             <span ng-show="jobform.txbContactTel.$error.minlength || jobform.txbContactTel.$error.maxlength" 
                             style="color : red" > เบอร์โทรศัพท์ที่สามารถติดต่อได้ไม่ถูกต้อง </span>    

                        </div>
                    </div>
                     <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>E-mail</label></div>
                        <div class="col-md-12">
                            <input type="email" class="textbox" name="txbContactEmail" id="txbContactEmail" maxlength="60" 
                            ng-model="data.jobtxbContactEmail" value="" style="width: 100%">
                        </div>
                    </div>
                </div>
                <div class="row">
                   
                </div>
                <div class="row">
                 <div class="col-md-12 form-row"><div class="col-md-6 title" style="font-size:22px;">  
                        บุคคลที่ติดต่อได้ในกรณีฉุกเฉิน</div></div>
                 </div>
                <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>ชื่อ<span class="f-required">*</span></label></div>
                        <div class="col-md-12">
                           <input type="text" class="textbox" name="txbEmergencyPerson" id="txbEmergencyPerson" 
                           ng-model="data.jobtxbEmergencyPerson" maxlength="100" value="" required>
                            <span ng-show="!jobform.txbEmergencyPerson.$pristine && jobform.txbEmergencyPerson.$error.required" 
                            style="color : red" > โปรดกรอกชื่อ </span>    
                        </div>
                    </div>
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>เบอร์โทรศัพท์<span class="f-required">*</span></label></div>
                        <div class="col-md-12">
                            <input type="number" class="textbox" name="txbEmergencyTel" id="txbEmergencyTel" 
                            ng-model="data.jobtxbEmergencyTel" min="0" ng-minlength="10" ng-maxlength="10" 
                            value="" required>

                            <span ng-show="!jobform.txbEmergencyTel.$pristine && jobform.txbEmergencyTel.$error.required" style="color : red" > โปรดกรอกเบอร์โทรศัพท์ </span>  
                            <span ng-show="jobform.txbEmergencyTel.$error.minlength || jobform.txbEmergencyTel.$error.maxlength" 
                             style="color : red" > เบอร์โทรศัพท์ไม่ถูกต้อง </span> 
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-title">ข้อมูลการศึกษา</div>
            <div class="education_info form-section">
                <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-12"><label>การศึกษาสูงสุด<span class="f-required">*</span></label></div>
                        <div class="col-md-12">
                            <div class="select-block">
                                <select name="cmbHighestGraduate" id="cmbHighestGraduate" ng-model="data.jobcmbHighestGraduate" 
                                required>
                                    <option value="" disabled>เลือกวุฒิการศึกษา</option>
                                    <option value="1">ปวส.</option>
                                    <option value="2">ปริญญาตรี</option>
                                    <option value="3">ปริญญาโท</option>
                                    <option value="4">ปริญญาเอก</option>
                                </select>
                            </div>
                            <span ng-show="!jobform.cmbHighestGraduate.$pristine && jobform.cmbHighestGraduate.$error.required" style="color : red" > โปรดเลือกวุฒิการศึกษา </span>    
                        </div>
                    </div>
                </div>
                <!-- ส่วนบนแก้ไข -->
                <div class="row hidden-lg hidden-md">
                    <div class="col-md-4 form-row" align="center"><br>
                     <button type="button" class="btn" id="btnMobile1" data-toggle="collapse" 
                     data-target="#MobiletabStudy1">ประวัติการศึกษา 1 
                     <span class="glyphicon glyphicon-triangle-bottom iconmobiledown"></span>
                     <span class='glyphicon glyphicon-triangle-top iconmobileup' 
                     style="display: none;"> </span></button>
                    </div>

                 <div id="MobiletabStudy1" class="collapse">

                        <div class="row">
                            <div class="col-md-12 form-row">
                                <div class="col-md-12">ระยะเวลา</div> 
                         </div> 
                         </div>      
                        
                    <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-12">
                                    ตั้งแต่
                                </div>
                                <div class="col-md-12">
                                    <input type="text" name="cmbStudyFrom1" id="cmbStudyFrom1" 
                                    ng-model="data.jobcmbStudyFrom1" value="" class="hasDatepicker" readonly>
                                    <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                    id="iconstudyFrom1" ></span> 
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12">
                                    ถึง
                                </div>
                                <div class="col-md-12">
                                    <input type="text" name="cmbStudyTo1" id="cmbStudyTo1" 
                                    ng-model="data.jobcmbStudyTo1" value="" class="hasDatepicker" readonly>
                                    <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                    id="iconstudyTo1" ></span> 
                                </div>
                            </div>
                    </div>
                       
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label class="text-left">สถาบันการศึกษา</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateInstitute1" id="txbGraduateInstitute1" ng-model="data.jobtxbGraduateInstitute1" maxlength="50" value=""></div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>คณะ</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateFaculty1" id="txbGraduateFaculty1" ng-model="data.jobtxbGraduateFaculty1" maxlength="50" value=""></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label class="text-left">กลุ่มสาขาวิชา</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateField1" id="txbGraduateField1" maxlength="50" ng-model="data.jobtxbGraduateField1" value=""></div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ระดับการศึกษา</label></div>
                                <div class="col-md-12">
                                    <div class="select-block">
                                        <select name="cmbGraduateGraduate1" id="cmbGraduateGraduate1" class="w100-m" 
                                        ng-model="data.jobcmbGraduateGraduate1">
                                            <option value="" disabled>เลือกวุฒิการศึกษา</option>
                                            <option value="1">ปวส.</option>
                                            <option value="2">ปริญญาตรี</option>
                                            <option value="3">ปริญญาโท</option>
                                            <option value="4">ปริญญาเอก</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label class="text-left">เกรดเฉลี่ย</label></div>
                                <div class="col-md-12"><input type="number" class="textbox" name="txbGraduatePoint1" id="txbGraduatePoint1" ng-model="data.jobtxbGraduatePoint1" min="0" maxlength="5" value=""></div>
                            </div>
                        </div>
                        <br>

                    </div>

                    <div class="col-md-4 form-row" align="center">
                     <button type="button" class="btn" id="btnMobile2" data-toggle="collapse" 
                     data-target="#MobiletabStudy2">ประวัติการศึกษา 2
                      <span class="glyphicon glyphicon-triangle-bottom iconmobiledown"></span>
                      <span class='glyphicon glyphicon-triangle-top iconmobileup' 
                     style="display: none;"> </span></button>
                    </div>

                    <div id="MobiletabStudy2"  class="collapse">

                         <div class="row">
                            <div class="col-md-12 form-row">
                                <div class="col-md-12">ระยะเวลา</div> 
                         </div> 
                         </div>      
                        
                    <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-12">
                                    ตั้งแต่
                                </div>
                                <div class="col-md-12">
                                    <input type="text" name="cmbStudyFrom2" id="cmbStudyFrom2" 
                                    ng-model="data.jobcmbStudyFrom2" value="" class="hasDatepicker" readonly>
                                    <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                    id="iconstudyFrom2" ></span> 
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12">
                                    ถึง
                                </div>
                                <div class="col-md-12">
                                    <input type="text" name="cmbStudyTo2" id="cmbStudyTo2" 
                                    ng-model="data.jobcmbStudyTo2" value="" class="hasDatepicker" readonly>
                                    <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                    id="iconstudyTo2" ></span> 
                                </div>
                            </div>
                    </div>
                       
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label class="text-left">สถาบันการศึกษา</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateInstitute2" id="txbGraduateInstitute2" ng-model="data.jobtxbGraduateInstitute2" maxlength="50" value=""></div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>คณะ</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateFaculty2" id="txbGraduateFaculty2" ng-model="data.jobtxbGraduateFaculty2" maxlength="50" value=""></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label class="text-left">กลุ่มสาขาวิชา</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateField2" id="txbGraduateField2" maxlength="50" ng-model="data.jobtxbGraduateField2" value=""></div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ระดับการศึกษา</label></div>
                                <div class="col-md-12">
                                    <div class="select-block">
                                        <select name="cmbGraduateGraduate2" id="cmbGraduateGraduate2" class="w100-m" 
                                        ng-model="data.jobcmbGraduateGraduate2">
                                            <option value="" disabled>เลือกวุฒิการศึกษา</option>
                                            <option value="1">ปวส.</option>
                                            <option value="2">ปริญญาตรี</option>
                                            <option value="3">ปริญญาโท</option>
                                            <option value="4">ปริญญาเอก</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label class="text-left">เกรดเฉลี่ย</label></div>
                                <div class="col-md-12"><input type="number" class="textbox" name="txbGraduatePoint2" id="txbGraduatePoint2" ng-model="data.jobtxbGraduatePoint2" min="0" maxlength="5" value=""></div>
                            </div>
                        </div>
                        <br>
                    </div>

                    <div class="col-md-4 form-row" align="center">
                     <button type="button"  data-toggle="collapse" 
                     data-target="#MobiletabStudy3" class="btn" id="btnMobile3">
                     ประวัติการศึกษา 3  
                     <span class="glyphicon glyphicon-triangle-bottom iconmobiledown"></span>
                     <span class='glyphicon glyphicon-triangle-top iconmobileup' 
                     style="display: none;"> </span></button>
                    </div>

                      <div id="MobiletabStudy3"  class="collapse">

                         <div class="row">
                            <div class="col-md-12 form-row">
                                <div class="col-md-12">ระยะเวลา</div> 
                         </div> 
                         </div>      
                        
                    <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-12">
                                    ตั้งแต่
                                </div>
                                <div class="col-md-12">
                                    <input type="text" name="cmbStudyFrom3" id="cmbStudyFrom3" 
                                    ng-model="data.jobcmbStudyFrom3" value="" class="hasDatepicker" readonly>
                                    <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                    id="iconstudyFrom3" ></span> 
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12">
                                    ถึง
                                </div>
                                <div class="col-md-12">
                                    <input type="text" name="cmbStudyTo3" id="cmbStudyTo3" 
                                    ng-model="data.jobcmbStudyTo3" value="" class="hasDatepicker" readonly>
                                    <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                    id="iconstudyTo3" ></span> 
                                </div>
                            </div>
                    </div>
                       
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label class="text-left">สถาบันการศึกษา</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateInstitute3" id="txbGraduateInstitute3" ng-model="data.jobtxbGraduateInstitute3" maxlength="50" value=""></div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>คณะ</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateFaculty3" id="txbGraduateFaculty3" ng-model="data.jobtxbGraduateFaculty3" maxlength="50" value=""></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label class="text-left">กลุ่มสาขาวิชา</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateField3" id="txbGraduateField3" maxlength="50" ng-model="data.jobtxbGraduateField3" value=""></div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ระดับการศึกษา</label></div>
                                <div class="col-md-12">
                                    <div class="select-block">
                                        <select name="cmbGraduateGraduate3" id="cmbGraduateGraduate3" class="w100-m" 
                                        ng-model="data.jobcmbGraduateGraduate3">
                                            <option value="" disabled>เลือกวุฒิการศึกษา</option>
                                            <option value="1">ปวส.</option>
                                            <option value="2">ปริญญาตรี</option>
                                            <option value="3">ปริญญาโท</option>
                                            <option value="4">ปริญญาเอก</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label class="text-left">เกรดเฉลี่ย</label></div>
                                <div class="col-md-12"><input type="number" class="textbox" name="txbGraduatePoint3" id="txbGraduatePoint3" ng-model="data.jobtxbGraduatePoint3" min="0" maxlength="5" value=""></div>
                            </div>
                        </div>

                    </div>

                </div>    

                <div class="col-md-12  hidden-sm hidden-xs" style="text-align: center;"> 
                    <div class="row">
                         <ul class="tabs-menu">
                            <li class="item current" id="headTabStudy1">
                                <a href="#">
                                    ประวัติการศึกษา 1
                                </a>
                            </li>
                            <li class="item" id="headTabStudy2">
                                <a href="#">
                                    ประวัติการศึกษา 2
                                </a>
                            </li>
                            <li class="item" id="headTabStudy3">
                                <a href="#">
                                    ประวัติการศึกษา 3
                                </a>
                            </li>              
                        </ul>
                    </div>
                </div>
                <div claas="col-md-12">

                    <div id="tabStudy1" style=" border-bottom:none" class="hidden-sm hidden-xs">

                        <div class="row">
                            <div class="col-md-12 form-row">
                                <div class="col-md-12">1. ระยะเวลา</div> 
                         </div> 
                         </div>      
                        
                    <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-12">
                                    ตั้งแต่
                                </div>
                                <div class="col-md-12">
                                    <input type="text" name="cmbStudyFrom1" id="cmbStudyFrom1" 
                                    ng-model="data.jobcmbStudyFrom1" value="" class="hasDatepicker" readonly>
                                    <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                    id="iconstudyFrom1" ></span> 
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12">
                                    ถึง
                                </div>
                                <div class="col-md-12">
                                    <input type="text" name="cmbStudyTo1" id="cmbStudyTo1" 
                                    ng-model="data.jobcmbStudyTo1" value="" class="hasDatepicker" readonly>
                                    <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                    id="iconstudyTo1" ></span> 
                                </div>
                            </div>
                    </div>
                       
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label class="text-left">สถาบันการศึกษา</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateInstitute1" id="txbGraduateInstitute1" ng-model="data.jobtxbGraduateInstitute1" maxlength="50" value=""></div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>คณะ</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateFaculty1" id="txbGraduateFaculty1" ng-model="data.jobtxbGraduateFaculty1" maxlength="50" value=""></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label class="text-left">กลุ่มสาขาวิชา</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateField1" id="txbGraduateField1" maxlength="50" ng-model="data.jobtxbGraduateField1" value=""></div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ระดับการศึกษา</label></div>
                                <div class="col-md-12">
                                    <div class="select-block">
                                        <select name="cmbGraduateGraduate1" id="cmbGraduateGraduate1" class="w100-m" 
                                        ng-model="data.jobcmbGraduateGraduate1">
                                            <option value="" disabled>เลือกวุฒิการศึกษา</option>
                                            <option value="1">ปวส.</option>
                                            <option value="2">ปริญญาตรี</option>
                                            <option value="3">ปริญญาโท</option>
                                            <option value="4">ปริญญาเอก</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label class="text-left">เกรดเฉลี่ย</label></div>
                                <div class="col-md-12"><input type="number" class="textbox" name="txbGraduatePoint1" id="txbGraduatePoint1" ng-model="data.jobtxbGraduatePoint1" min="0" maxlength="5"  value=""></div>
                            </div>
                        </div>

                    </div>

                    <div id="tabStudy2"  style="display:none; border-bottom:none" class="hidden-sm hidden-xs">

                         <div class="row">
                            <div class="col-md-12 form-row">
                                <div class="col-md-12">2. ระยะเวลา</div> 
                         </div> 
                         </div>      
                        
                    <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-12">
                                    ตั้งแต่
                                </div>
                                <div class="col-md-12">
                                    <input type="text" name="cmbStudyFrom2" id="cmbStudyFrom2" 
                                    ng-model="data.jobcmbStudyFrom2" value="" class="hasDatepicker" readonly>
                                    <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                    id="iconstudyFrom2" ></span> 
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12">
                                    ถึง
                                </div>
                                <div class="col-md-12">
                                    <input type="text" name="cmbStudyTo2" id="cmbStudyTo2" 
                                    ng-model="data.jobcmbStudyTo2" value="" class="hasDatepicker" readonly>
                                    <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                    id="iconstudyTo2" ></span> 
                                </div>
                            </div>
                    </div>
                       
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label class="text-left">สถาบันการศึกษา</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateInstitute2" id="txbGraduateInstitute2" ng-model="data.jobtxbGraduateInstitute2" maxlength="50" value=""></div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>คณะ</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateFaculty2" id="txbGraduateFaculty2" ng-model="data.jobtxbGraduateFaculty2" maxlength="50" value=""></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label class="text-left">กลุ่มสาขาวิชา</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateField2" id="txbGraduateField2" maxlength="50" ng-model="data.jobtxbGraduateField2" value=""></div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ระดับการศึกษา</label></div>
                                <div class="col-md-12">
                                    <div class="select-block">
                                        <select name="cmbGraduateGraduate2" id="cmbGraduateGraduate2" class="w100-m" 
                                        ng-model="data.jobcmbGraduateGraduate2">
                                            <option value="" disabled>เลือกวุฒิการศึกษา</option>
                                            <option value="1">ปวส.</option>
                                            <option value="2">ปริญญาตรี</option>
                                            <option value="3">ปริญญาโท</option>
                                            <option value="4">ปริญญาเอก</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label class="text-left">เกรดเฉลี่ย</label></div>
                                <div class="col-md-12"><input type="number" class="textbox" name="txbGraduatePoint2" id="txbGraduatePoint2" ng-model="data.jobtxbGraduatePoint2" min="0" maxlength="5" value=""></div>
                            </div>
                        </div>
                    </div>

                    <div id="tabStudy3"  style="display:none; border-bottom:none" class="hidden-sm hidden-xs">

                         <div class="row">
                            <div class="col-md-12 form-row">
                                <div class="col-md-12">3. ระยะเวลา</div> 
                         </div> 
                         </div>      
                        
                    <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-12">
                                    ตั้งแต่
                                </div>
                                <div class="col-md-12">
                                    <input type="text" name="cmbStudyFrom3" id="cmbStudyFrom3" 
                                    ng-model="data.jobcmbStudyFrom3" value="" class="hasDatepicker" readonly>
                                    <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                    id="iconstudyFrom3" ></span> 
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12">
                                    ถึง
                                </div>
                                <div class="col-md-12">
                                    <input type="text" name="cmbStudyTo3" id="cmbStudyTo3" 
                                    ng-model="data.jobcmbStudyTo3" value="" class="hasDatepicker" readonly>
                                    <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                    id="iconstudyTo3" ></span> 
                                </div>
                            </div>
                    </div>
                       
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label class="text-left">สถาบันการศึกษา</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateInstitute3" id="txbGraduateInstitute3" ng-model="data.jobtxbGraduateInstitute3" maxlength="50" value=""></div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>คณะ</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateFaculty3" id="txbGraduateFaculty3" ng-model="data.jobtxbGraduateFaculty3" maxlength="50" value=""></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label class="text-left">กลุ่มสาขาวิชา</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbGraduateField3" id="txbGraduateField3" maxlength="50" ng-model="data.jobtxbGraduateField3" value=""></div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ระดับการศึกษา</label></div>
                                <div class="col-md-12">
                                    <div class="select-block">
                                        <select name="cmbGraduateGraduate3" id="cmbGraduateGraduate3" class="w100-m" 
                                        ng-model="data.jobcmbGraduateGraduate3">
                                            <option value="" disabled>เลือกวุฒิการศึกษา</option>
                                            <option value="1">ปวส.</option>
                                            <option value="2">ปริญญาตรี</option>
                                            <option value="3">ปริญญาโท</option>
                                            <option value="4">ปริญญาเอก</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label class="text-left">เกรดเฉลี่ย</label></div>
                                <div class="col-md-12"><input type="number" class="textbox" name="txbGraduatePoint3" id="txbGraduatePoint3" ng-model="data.jobtxbGraduatePoint3" min="0" maxlength="5" value=""></div>
                            </div>
                        </div>

                    </div>
                </div>
                    
            </div>
             <center><input type="button" id="btnnext1" name="next" class="next" value="ต่อไป" /></center>
                
    </fieldset>       
    
    <fieldset>
        <div class="form-title">ข้อมูลการทำงาน</div>
        <div class="experience_info form-section">
         <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-md-9"><label>เพิ่งสำเร็จการศึกษา<span class="f-required">*</span></label></div><br>
                    <div class="col-md-12">
                        <input type="radio" name="rdoJustGraduate" id="rdoJustGraduateY" 
                        ng-model=data.jobrdoJustGraduate value="ใช่" required> &nbsp; <label for="rdoJustGraduateY"><span></span>ใช่</label> &nbsp; &nbsp; 
                        <input type="radio" name="rdoJustGraduate" id="rdoJustGraduateN" 
                        ng-model=data.jobrdoJustGraduate value="ไม่ใช่" required> &nbsp; <label for="rdoJustGraduateN"><span></span>ไม่ใช่</label>
                    </div>
                </div>

                <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>ประสบการณ์การทำงาน (ปี)</label></div>
                    <div class="col-md-12">
                        <input type="number" class="textbox" name="txbWorkExperience" id="txbWorkExperience" 
                        ng-model="data.jobtxbWorkExperience" min="0" maxlength="2">
                    </div>
                </div>
        </div>
           
            <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>สายงานล่าสุด</label></div>
                    <div class="col-md-12">
                        <div class="select-block">
                            <select name="cmbLastWorkField" id="cmbLastWorkField" 
                            ng-model="data.jobcmbLastWorkField" class="w100-m">
                                <option value="" disabled>เลือกสายงาน</option>
                                <option ng-repeat="job in jobs" 
                                value="{{ job.TITLE }}"> {{ job.TITLE }}
                                </option>
                            </select>
                        </div>
                    </div>
                </div>

                  <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>ตำแหน่งสุดท้าย</label></div>
                    <div class="col-md-12"><input type="text" class="textbox w100-m" name="txbLastPosition" id="txbLastPosition" maxlength="30" ng-model="data.jobtxbLastPosition" width="100%" value=""></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-md-12"><label>เงินเดือนสุดท้าย (บาท) </label></div>
                    <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbLastSalary" id="txbLastSalary" maxlength="7" min="0" ng-model="data.jobtxbLastSalary" value=""> </div>
                </div>
            </div><br>
            <!-- แก้ไขส่วนบน -->

            <!-- ส่วนmobile ประวัติทำงาน -->
             <div class="row hidden-lg hidden-md">
                <div class="col-md-4 form-row" align="center">
                 <button type="button" class="btn" id="btnWorkMobile1" data-toggle="collapse" 
                 data-target="#Mobiletab1">ประวัติการทำงาน 1
                 <span class="glyphicon glyphicon-triangle-bottom iconmobiledown"></span>
                 <span class='glyphicon glyphicon-triangle-top iconmobileup' 
                 style="display: none;"> </span></button>
                </div>
                 <div id="Mobiletab1" class="collapse">
                     
                    <div class="row">
                        <div class="col-md-12 form-row">
                            <div class="col-md-12">ระยะเวลา</div> 
                        </div> 
                    </div>
                    
                    <div class="row">
                         <div class="col-md-6 form-row">
                            <div class="col-md-12">ตั้งแต่</div>
                            <div class="col-md-12">
                                <input type="text" name="cmbWorkFrom1" id="cmbWorkFrom1" ng-model="data.jobcmbWorkFrom1" class="hasDatepicker" readonly value="">
                                <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                id="iconworkFrom1" ></span> 
                            </div>
                          </div>
                         <div class="col-md-6 form-row">
                            <div class="col-md-12">ถึง</div>
                            <div class="col-md-12">
                                <input type="text" name="cmbWorkTo1" id="cmbWorkTo1" ng-model="data.jobcmbWorkTo1" class="hasDatepicker" readonly value="">
                                <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                id="iconworkTo1" ></span>  

                            </div>  
                            <div class="col-md-12 form-row">
                                <label>หรือ</label> &nbsp; &nbsp;  
                                <input type="checkbox" id="chWorkTo1" name="chWorkTo1" > <label for="chWorkTo1"> 
                                <span style="height: 19px; width: 19px;"></span> ถึงปัจจุบัน </label>
                            </div>       
                         </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ตำแหน่ง</label></div>
                                <div class="col-md-12">

                                    <input type="text" class="textbox" name="txbWorkPosition1" id="txbWorkPosition1" ng-model="data.jobtxbWorkPosition1" maxlength="50" value="">
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>บริษัท</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbWorkCompany1" id="txbWorkCompany1" ng-model="data.jobtxbWorkCompany1" maxlength="50" value=""></div>
                            </div>
                                         
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>เงินเดือน (บาท) </label></div>
                                <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkSalary1" id="txbWorkSalary1" maxlength="7" ng-model="data.jobtxbWorkSalary1" value="" min="0"></div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>รายได้อื่น ๆ (บาท) </label></div>
                                <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkIncome1" id="txbWorkIncome1" maxlength="7"  
                                ng-model="data.jobtxbWorkIncome1" min="0" value=""></div>
                            </div>
                     
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ระบุรายได้อื่น ๆ</label></div>
                                <div class="col-md-12"><input type="text" class="textbox w100-m" 
                                name="txbWorkIncomeOther1" id="txbWorkIncomeOther1" ng-model="data.jobtxbWorkIncomeOther1" maxlength="100" value=""></div>
                            </div>
                             <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ลักษณะงานที่ทำ</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbWorkDetail1" 
                                id="txbWorkDetail1" ng-model="data.jobtxbWorkDetail1">
                                </div>
                            </div><br>
                        
                    </div>
                </div>

                <div class="col-md-4 form-row" align="center">
                 <button type="button" class="btn" id="btnWorkMobile2" data-toggle="collapse" 
                 data-target="#Mobiletab2">ประวัติการทำงาน 2
                 <span class="glyphicon glyphicon-triangle-bottom iconmobiledown"></span>
                 <span class='glyphicon glyphicon-triangle-top iconmobileup' 
                 style="display: none;"> </span></button>
                </div>
                  <div id="Mobiletab2" class="collapse">
                          
                    <div class="row">
                        <div class="col-md-12 form-row">
                            <div class="col-md-12">ระยะเวลา</div> 
                        </div> 
                    </div>
                    
                    <div class="row">
                         <div class="col-md-6 form-row">
                            <div class="col-md-12">ตั้งแต่</div>
                            <div class="col-md-12">
                                <input type="text" name="cmbWorkFrom2" id="cmbWorkFrom2" ng-model="data.jobcmbWorkFrom2" class="hasDatepicker" readonly value="">
                                <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                id="iconworkFrom2" ></span> 
                            </div>
                          </div>
                         <div class="col-md-6 form-row">
                            <div class="col-md-12">ถึง</div>
                            <div class="col-md-12">
                                <input type="text" name="cmbWorkTo2" id="cmbWorkTo2" ng-model="data.jobcmbWorkTo2" class="hasDatepicker" readonly value=""> 
                                <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                id="iconworkTo2" ></span> 
                            </div> 
                            <div class="col-md-12 form-row">
                                <label>หรือ</label> &nbsp; &nbsp;  
                                <input type="checkbox" id="chWorkTo2" name="chWorkTo2" > 
                                <label for="chWorkTo2"> 
                                <span style="height: 19px; width: 19px;"></span> ถึงปัจจุบัน </label>
                            </div>        
                         </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ตำแหน่ง</label></div>
                                <div class="col-md-12">

                                    <input type="text" class="textbox" name="txbWorkPosition2" id="txbWorkPosition2" ng-model="data.jobtxbWorkPosition2" maxlength="50" value="">
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>บริษัท</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbWorkCompany2" id="txbWorkCompany2" ng-model="data.jobtxbWorkCompany2" maxlength="50" value=""></div>
                            </div>
                                         
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>เงินเดือน (บาท) </label></div>
                                <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkSalary2" id="txbWorkSalary2" maxlength="7" ng-model="data.jobtxbWorkSalary2" 
                                value="" min="0"></div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>รายได้อื่น ๆ (บาท) </label></div>
                                <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkIncome2" id="txbWorkIncome2" maxlength="7"
                                ng-model="data.jobtxbWorkIncome2" min="0" value=""></div>
                            </div>
                     
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ระบุรายได้อื่น ๆ</label></div>
                                <div class="col-md-12"><input type="text" class="textbox w100-m" 
                                name="txbWorkIncomeOther2" id="txbWorkIncomeOther2" ng-model="data.jobtxbWorkIncomeOther2" maxlength="100" value=""></div>
                            </div>
                             <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ลักษณะงานที่ทำ</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbWorkDetail2" 
                                id="txbWorkDetail2" ng-model="data.jobtxbWorkDetail2">
                                </div>
                            </div><br>
                        
                    </div>  
                    </div>
                    
                    <div class="col-md-4 form-row" align="center">
                    <button type="button" class="btn" id="btnWorkMobile3" data-toggle="collapse" 
                     data-target="#Mobiletab3">ประวัติการทำงาน 3
                     <span class="glyphicon glyphicon-triangle-bottom iconmobiledown"></span>
                     <span class='glyphicon glyphicon-triangle-top iconmobileup' 
                        style="display: none;"> </span></button>
                    </div>

                    <div id="Mobiletab3" class="collapse">     
                        <div class="row">
                            <div class="col-md-12 form-row">
                                <div class="col-md-12">ระยะเวลา</div> 
                            </div> 
                        </div>
                    
                        <div class="row">
                             <div class="col-md-6">
                                <div class="col-md-12">ตั้งแต่</div>
                                <div class="col-md-12">
                                    <input type="text" name="cmbWorkFrom3" id="cmbWorkFrom3" ng-model="data.jobcmbWorkFrom3" class="hasDatepicker" readonly value="">
                                    <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                    id="iconworkFrom3" ></span> 
                                </div>
                              </div>
                             <div class="col-md-6 form-row">
                                <div class="col-md-12">ถึง</div>
                                <div class="col-md-12">
                                    <input type="text" name="cmbWorkTo3" id="cmbWorkTo3" ng-model="data.jobcmbWorkTo3" class="hasDatepicker" readonly value=""> 
                                    <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                    id="iconworkTo3" ></span> 
                                </div> 
                                <div class="col-md-12 form-row">
                                    <label>หรือ</label> &nbsp; &nbsp;  
                                    <input type="checkbox" id="chWorkTo3" name="chWorkTo3" >
                                     <label for="chWorkTo3"> 
                                    <span style="height: 19px; width: 19px;"></span> ถึงปัจจุบัน </label>
                                </div>        
                             </div>
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>ตำแหน่ง</label></div>
                                    <div class="col-md-12">

                                        <input type="text" class="textbox" name="txbWorkPosition3" id="txbWorkPosition3" ng-model="data.jobtxbWorkPosition3" maxlength="50" value="">
                                    </div>
                                </div>
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>บริษัท</label></div>
                                    <div class="col-md-12"><input type="text" class="textbox" name="txbWorkCompany3" id="txbWorkCompany3" ng-model="data.jobtxbWorkCompany3" maxlength="50" value=""></div>
                                </div>
                                             
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>เงินเดือน (บาท) </label></div>
                                    <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkSalary3" id="txbWorkSalary3" maxlength="7" ng-model="data.jobtxbWorkSalary3" 
                                    value="" min="0"></div>
                                </div>
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>รายได้อื่น ๆ (บาท) </label></div>
                                    <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkIncome3" id="txbWorkIncome3" maxlength="7" 
                                    ng-model="data.jobtxbWorkIncome3" min="0" value=""></div>
                                </div>
                         
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>ระบุรายได้อื่น ๆ</label></div>
                                    <div class="col-md-12"><input type="text" class="textbox w100-m" 
                                    name="txbWorkIncomeOther3" id="txbWorkIncomeOther3" ng-model="data.jobtxbWorkIncomeOther3" maxlength="100" value=""></div>
                                </div>
                                 <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>ลักษณะงานที่ทำ</label></div>
                                    <div class="col-md-12"><input type="text" class="textbox" name="txbWorkDetail3" 
                                    id="txbWorkDetail3" ng-model="data.jobtxbWorkDetail3">
                                    </div>
                                </div><br>
                            
                        </div>
                    </div>
                    
                        <div class="col-md-4 form-row" align="center">
                            <button type="button" class="btn" id="btnWorkMobile4" data-toggle="collapse" 
                             data-target="#Mobiletab4">ประวัติการทำงาน 4
                             <span class="glyphicon glyphicon-triangle-bottom iconmobiledown"></span>
                             <span class='glyphicon glyphicon-triangle-top iconmobileup' 
                                style="display: none;"> </span></button>
                        </div>
                       
                    <div id="Mobiletab4" class="collapse">
                       <div class="row">
                        <div class="col-md-12 form-row">
                            <div class="col-md-12">ระยะเวลา</div> 
                        </div> 
                    </div>
                    
                        <div class="row">
                             <div class="col-md-6 form-row">
                                <div class="col-md-12">ตั้งแต่</div>
                                <div class="col-md-12">
                                    <input type="text" name="cmbWorkFrom4" id="cmbWorkFrom4" ng-model="data.jobcmbWorkFrom4" class="hasDatepicker" readonly value="">
                                    <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                    id="iconworkFrom4" ></span> 
                                </div>
                              </div>
                             <div class="col-md-6 form-row">
                                <div class="col-md-12">ถึง</div>
                                <div class="col-md-12">
                                    <input type="text" name="cmbWorkTo4" id="cmbWorkTo4" ng-model="data.jobcmbWorkTo4" class="hasDatepicker" readonly value=""> 
                                    <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                    id="iconworkTo4" ></span> 
                                </div> 
                                    <div class="col-md-12 form-row">
                                    <label>หรือ</label> &nbsp; &nbsp;  
                                    <input type="checkbox" id="chWorkTo4" name="chWorkTo4" > 
                                    <label for="chWorkTo4"> 
                                    <span style="height: 19px; width: 19px;"></span> ถึงปัจจุบัน </label>
                                </div>        
                             </div>
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>ตำแหน่ง</label></div>
                                    <div class="col-md-12">

                                        <input type="text" class="textbox" name="txbWorkPosition4" id="txbWorkPosition4" ng-model="data.jobtxbWorkPosition4" maxlength="50" value="">
                                    </div>
                                </div>
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>บริษัท</label></div>
                                    <div class="col-md-12"><input type="text" class="textbox" name="txbWorkCompany4" id="txbWorkCompany4" ng-model="data.jobtxbWorkCompany4" maxlength="50" value=""></div>
                                </div>
                                             
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>เงินเดือน (บาท) </label></div>
                                    <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkSalary4" id="txbWorkSalary4" maxlength="7" ng-model="data.jobtxbWorkSalary4"
                                    value="" min="0"></div>
                                </div>
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>รายได้อื่น ๆ (บาท) </label></div>
                                    <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkIncome4" id="txbWorkIncome4" maxlength="7" 
                                    ng-model="data.jobtxbWorkIncome4" min="0" value=""></div>
                                </div>
                         
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>ระบุรายได้อื่น ๆ</label></div>
                                    <div class="col-md-12"><input type="text" class="textbox w100-m" 
                                    name="txbWorkIncomeOther4" id="txbWorkIncomeOther4" ng-model="data.jobtxbWorkIncomeOther4" maxlength="100" value=""></div>
                                </div>
                                 <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>ลักษณะงานที่ทำ</label></div>
                                    <div class="col-md-12"><input type="text" class="textbox" name="txbWorkDetail4" 
                                    id="txbWorkDetail4" ng-model="data.jobtxbWorkDetail4">
                                    </div>
                                </div><br>
                            
                        </div>
                    </div>

                        <div class="col-md-4 form-row" align="center">
                        <button type="button" class="btn" id="btnWorkMobile5" data-toggle="collapse" 
                         data-target="#Mobiletab5">ประวัติการทำงาน 5
                         <span class="glyphicon glyphicon-triangle-bottom iconmobiledown"></span>
                         <span class='glyphicon glyphicon-triangle-top iconmobileup' 
                            style="display: none;"> </span></button>
                        </div>

                    <div id="Mobiletab5" class="collapse">  
                        <div class="row">
                            <div class="col-md-12 form-row">
                                <div class="col-md-12">ระยะเวลา</div> 
                            </div> 
                        </div>
                    
                    <div class="row">
                         <div class="col-md-6 form-row">
                            <div class="col-md-12">ตั้งแต่</div>
                            <div class="col-md-12">
                                <input type="text" name="cmbWorkFrom5" id="cmbWorkFrom5" ng-model="data.jobcmbWorkFrom5" class="hasDatepicker" readonly value="">
                                <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                id="iconworkFrom5" ></span> 
                            </div>
                          </div>
                         <div class="col-md-6 form-row">
                            <div class="col-md-12">ถึง</div>
                            <div class="col-md-12">
                                <input type="text" name="cmbWorkTo5" id="cmbWorkTo5" ng-model="data.jobcmbWorkTo5" class="hasDatepicker" readonly value=""> 
                                <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                id="iconworkTo5" ></span> 
                            </div>
                                <div class="col-md-12 form-row">
                                <label>หรือ</label> &nbsp; &nbsp;  
                                <input type="checkbox" id="chWorkTo5" name="chWorkTo5" > 
                                <label for="chWorkTo5"> 
                                <span style="height: 19px; width: 19px;"></span> ถึงปัจจุบัน </label>
                            </div> 

                         </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ตำแหน่ง</label></div>
                                <div class="col-md-12">

                                    <input type="text" class="textbox" name="txbWorkPosition5" id="txbWorkPosition5" ng-model="data.jobtxbWorkPosition5" maxlength="50" value="">
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>บริษัท</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbWorkCompany5" id="txbWorkCompany5" ng-model="data.jobtxbWorkCompany5" maxlength="50" value=""></div>
                            </div>
                                         
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>เงินเดือน (บาท) </label></div>
                                <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkSalary5" id="txbWorkSalary5" maxlength="7" ng-model="data.jobtxbWorkSalary5" 
                                value="" min="0"></div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>รายได้อื่น ๆ (บาท) </label></div>
                                <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkIncome5" id="txbWorkIncome5" maxlength="7"  
                                ng-model="data.jobtxbWorkIncome5" min="0" value=""></div>
                            </div>
                     
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ระบุรายได้อื่น ๆ</label></div>
                                <div class="col-md-12"><input type="text" class="textbox w100-m" 
                                name="txbWorkIncomeOther5" id="txbWorkIncomeOther5" ng-model="data.jobtxbWorkIncomeOther5" maxlength="100" value=""></div>
                            </div>
                             <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ลักษณะงานที่ทำ</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbWorkDetail5" 
                                id="txbWorkDetail5" ng-model="data.jobtxbWorkDetail5">
                                </div>
                            </div>
                    </div>

            </div>
        </div>
            
            <div class="col-sm-12  hidden-xs hidden-sm" style="text-align: center;">
                <div class="row">
                     <ul class="tabs-menu">
                        <li class="item current" id="headTab1">
                            <a href="#">
                                ประวัติการทำงาน 1
                            </a>
                        </li>
                        <li class="item" id="headTab2">
                            <a href="#">
                                ประวัติการทำงาน 2
                            </a>
                        </li>
                        <li class="item" id="headTab3">
                            <a href="#">
                               ประวัติการทำงาน 3
                            </a>
                        </li>   
                        <li class="item" id="headTab4">
                            <a href="#">
                               ประวัติการทำงาน 4
                            </a>
                        </li>   
                        <li class="item" id="headTab5">
                            <a href="#">
                                ประวัติการทำงาน 5
                            </a>
                        </li>              
                    </ul>
                </div>
            </div>
           
             <div id="tab1" style=" border-bottom:none" class="hidden-xs hidden-sm">
                     
                    <div class="row">
                        <div class="col-md-12 form-row">
                            <div class="col-md-12">1. ระยะเวลา</div> 
                        </div> 
                    </div>
                    
                    <div class="row">
                         <div class="col-md-6 form-row">
                            <div class="col-md-12">ตั้งแต่</div>
                            <div class="col-md-12">
                                <input type="text" name="cmbWorkFrom1" id="cmbWorkFrom1" ng-model="data.jobcmbWorkFrom1" class="hasDatepicker" readonly value="">
                                <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                id="iconworkFrom1" ></span> 
                            </div>
                          </div>
                         <div class="col-md-6 form-row">
                            <div class="col-md-12">ถึง</div>
                            <div class="col-md-12">
                                <input type="text" name="cmbWorkTo1" id="cmbWorkTo1" 
                                 ng-model="data.jobcmbWorkTo1" class="hasDatepicker" readonly>
                                <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                id="iconworkTo1"></span>  
                            </div> 

                            <div class="col-md-12 form-row">
                                <label>หรือ</label> &nbsp; &nbsp;  
                                <input type="checkbox" id="chWorkTo1" name="chWorkTo1" > <label for="chWorkTo1"> 
                                <span style="height: 19px; width: 19px;"></span> ถึงปัจจุบัน </label>
                            </div>       
                         </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ตำแหน่ง</label></div>
                                <div class="col-md-12">

                                    <input type="text" class="textbox" name="txbWorkPosition1" id="txbWorkPosition1" ng-model="data.jobtxbWorkPosition1" maxlength="50" value="">
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>บริษัท</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbWorkCompany1" id="txbWorkCompany1" ng-model="data.jobtxbWorkCompany1" maxlength="50" value=""></div>
                            </div>
                                         
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>เงินเดือน (บาท) </label></div>
                                <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkSalary1" id="txbWorkSalary1" maxlength="7" ng-model="data.jobtxbWorkSalary1" value="" min="0"></div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>รายได้อื่น ๆ (บาท) </label></div>
                                <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkIncome1" id="txbWorkIncome1" maxlength="7"  
                                ng-model="data.jobtxbWorkIncome1" min="0" value=""></div>
                            </div>
                     
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ระบุรายได้อื่น ๆ</label></div>
                                <div class="col-md-12"><input type="text" class="textbox w100-m" 
                                name="txbWorkIncomeOther1" id="txbWorkIncomeOther1" ng-model="data.jobtxbWorkIncomeOther1" maxlength="100" value=""></div>
                            </div>
                             <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ลักษณะงานที่ทำ</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbWorkDetail1" 
                                id="txbWorkDetail1" ng-model="data.jobtxbWorkDetail1">
                                </div>
                            </div>
                        
                    </div>
                </div>

                <div id="tab2" style="display:none; border-bottom:none" class="hidden-xs hidden-sm">
                          
                    <div class="row">
                        <div class="col-md-12 form-row">
                            <div class="col-md-12">2. ระยะเวลา</div> 
                        </div> 
                    </div>
                    
                    <div class="row">
                         <div class="col-md-6 form-row">
                            <div class="col-md-12">ตั้งแต่</div>
                            <div class="col-md-12">
                                <input type="text" name="cmbWorkFrom2" id="cmbWorkFrom2" ng-model="data.jobcmbWorkFrom2" class="hasDatepicker" readonly value="">
                                <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                id="iconworkFrom2" ></span> 
                            </div>
                          </div>
                         <div class="col-md-6 form-row">
                            <div class="col-md-12">ถึง</div>
                            <div class="col-md-12">
                                <input type="text" name="cmbWorkTo2" id="cmbWorkTo2" ng-model="data.jobcmbWorkTo2" class="hasDatepicker" readonly value=""> 
                                <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                id="iconworkTo2" ></span> 
                            </div>
                              <div class="col-md-12 form-row">
                                <label>หรือ</label> &nbsp; &nbsp;  
                                <input type="checkbox" id="chWorkTo2" name="chWorkTo2"> 
                                <label for="chWorkTo2"> 
                                <span style="height: 19px; width: 19px;"></span> ถึงปัจจุบัน </label>
                            </div>        
                         </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ตำแหน่ง</label></div>
                                <div class="col-md-12">

                                    <input type="text" class="textbox" name="txbWorkPosition2" id="txbWorkPosition2" ng-model="data.jobtxbWorkPosition2" maxlength="50" value="">
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>บริษัท</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbWorkCompany2" id="txbWorkCompany2" ng-model="data.jobtxbWorkCompany2" maxlength="50" value=""></div>
                            </div>
                                         
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>เงินเดือน (บาท) </label></div>
                                <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkSalary2" id="txbWorkSalary2" maxlength="7" ng-model="data.jobtxbWorkSalary2" 
                                value="" min="0"></div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>รายได้อื่น ๆ (บาท) </label></div>
                                <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkIncome2" id="txbWorkIncome2" maxlength="7"
                                ng-model="data.jobtxbWorkIncome2" min="0" value=""></div>
                            </div>
                     
                            <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ระบุรายได้อื่น ๆ</label></div>
                                <div class="col-md-12"><input type="text" class="textbox w100-m" 
                                name="txbWorkIncomeOther2" id="txbWorkIncomeOther2" ng-model="data.jobtxbWorkIncomeOther2" maxlength="100" value=""></div>
                            </div>
                             <div class="col-md-6 form-row">
                                <div class="col-md-12"><label>ลักษณะงานที่ทำ</label></div>
                                <div class="col-md-12"><input type="text" class="textbox" name="txbWorkDetail2" 
                                id="txbWorkDetail2" ng-model="data.jobtxbWorkDetail2">
                                </div>
                            </div>
                        
                    </div>
                        
                    </div>

                     <div id="tab3" style="display:none; border-bottom:none" class="hidden-xs hidden-sm">

                             
                        <div class="row">
                            <div class="col-md-12 form-row">
                                <div class="col-md-12">3. ระยะเวลา</div> 
                            </div> 
                        </div>
                    
                        <div class="row">
                             <div class="col-md-6">
                                <div class="col-md-12">ตั้งแต่</div>
                                <div class="col-md-12">
                                    <input type="text" name="cmbWorkFrom3" id="cmbWorkFrom3" ng-model="data.jobcmbWorkFrom3" class="hasDatepicker" readonly value="">
                                    <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                    id="iconworkFrom3" ></span> 
                                </div>
                              </div>
                             <div class="col-md-6 form-row">
                                <div class="col-md-12">ถึง</div>
                                <div class="col-md-12">
                                    <input type="text" name="cmbWorkTo3" id="cmbWorkTo3" ng-model="data.jobcmbWorkTo3" class="hasDatepicker" readonly value=""> 
                                    <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                    id="iconworkTo3" ></span> 
                                </div> 
                                <div class="col-md-12 form-row">
                                    <label>หรือ</label> &nbsp; &nbsp;  
                                    <input type="checkbox" id="chWorkTo3" name="chWorkTo3"> 
                                    <label for="chWorkTo3"> 
                                    <span style="height: 19px; width: 19px;"></span> ถึงปัจจุบัน </label>
                                </div>        
                             </div>
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>ตำแหน่ง</label></div>
                                    <div class="col-md-12">

                                        <input type="text" class="textbox" name="txbWorkPosition3" id="txbWorkPosition3" ng-model="data.jobtxbWorkPosition3" maxlength="50" value="">
                                    </div>
                                </div>
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>บริษัท</label></div>
                                    <div class="col-md-12"><input type="text" class="textbox" name="txbWorkCompany3" id="txbWorkCompany3" ng-model="data.jobtxbWorkCompany3" maxlength="50" value=""></div>
                                </div>
                                             
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>เงินเดือน (บาท) </label></div>
                                    <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkSalary3" id="txbWorkSalary3" maxlength="7" ng-model="data.jobtxbWorkSalary3" 
                                    value="" min="0"></div>
                                </div>
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>รายได้อื่น ๆ (บาท) </label></div>
                                    <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkIncome3" id="txbWorkIncome3" maxlength="7" 
                                    ng-model="data.jobtxbWorkIncome3" min="0" value=""></div>
                                </div>
                         
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>ระบุรายได้อื่น ๆ</label></div>
                                    <div class="col-md-12"><input type="text" class="textbox w100-m" 
                                    name="txbWorkIncomeOther3" id="txbWorkIncomeOther3" ng-model="data.jobtxbWorkIncomeOther3" maxlength="100" value=""></div>
                                </div>
                                 <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>ลักษณะงานที่ทำ</label></div>
                                    <div class="col-md-12"><input type="text" class="textbox" name="txbWorkDetail3" 
                                    id="txbWorkDetail3" ng-model="data.jobtxbWorkDetail3">
                                    </div>
                                </div>
                            
                        </div>

                    </div>

                    <div id="tab4" style="display:none; border-bottom:none" class="hidden-xs hidden-sm">

                          
                        <div class="row">
                            <div class="col-md-12 form-row">
                                <div class="col-md-12">4. ระยะเวลา</div> 
                            </div> 
                        </div>
                    
                        <div class="row">
                             <div class="col-md-6 form-row">
                                <div class="col-md-12">ตั้งแต่</div>
                                <div class="col-md-12">
                                    <input type="text" name="cmbWorkFrom4" id="cmbWorkFrom4" ng-model="data.jobcmbWorkFrom4" class="hasDatepicker" readonly value="">
                                    <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                    id="iconworkFrom4" ></span> 
                                </div>
                              </div>
                             <div class="col-md-6 form-row">
                                <div class="col-md-12">ถึง</div>
                                <div class="col-md-12">
                                    <input type="text" name="cmbWorkTo4" id="cmbWorkTo4" ng-model="data.jobcmbWorkTo4" class="hasDatepicker" readonly value=""> 
                                    <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                    id="iconworkTo4" ></span> 
                                </div>  
                               <div class="col-md-12 form-row">
                                    <label>หรือ</label> &nbsp; &nbsp;  
                                    <input type="checkbox" id="chWorkTo4" name="chWorkTo4"> 
                                    <label for="chWorkTo4"> 
                                    <span style="height: 19px; width: 19px;"></span> ถึงปัจจุบัน </label>
                              </div>          
                             </div>
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>ตำแหน่ง</label></div>
                                    <div class="col-md-12">

                                        <input type="text" class="textbox" name="txbWorkPosition4" id="txbWorkPosition4" ng-model="data.jobtxbWorkPosition4" maxlength="50" value="">
                                    </div>
                                </div>
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>บริษัท</label></div>
                                    <div class="col-md-12"><input type="text" class="textbox" name="txbWorkCompany4" id="txbWorkCompany4" ng-model="data.jobtxbWorkCompany4" maxlength="50" value=""></div>
                                </div>
                                             
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>เงินเดือน (บาท) </label></div>
                                    <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkSalary4" id="txbWorkSalary4" maxlength="7" ng-model="data.jobtxbWorkSalary4"
                                    value="" min="0"></div>
                                </div>
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>รายได้อื่น ๆ (บาท) </label></div>
                                    <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkIncome4" id="txbWorkIncome4" maxlength="7" 
                                    ng-model="data.jobtxbWorkIncome4" min="0" value=""></div>
                                </div>
                         
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>ระบุรายได้อื่น ๆ</label></div>
                                    <div class="col-md-12"><input type="text" class="textbox w100-m" 
                                    name="txbWorkIncomeOther4" id="txbWorkIncomeOther4" ng-model="data.jobtxbWorkIncomeOther4" maxlength="100" value=""></div>
                                </div>
                                 <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>ลักษณะงานที่ทำ</label></div>
                                    <div class="col-md-12"><input type="text" class="textbox" name="txbWorkDetail4" 
                                    id="txbWorkDetail4" ng-model="data.jobtxbWorkDetail4">
                                    </div>
                                </div>
                            
                        </div>

                    </div>

                    <div id="tab5" style="display:none; border-bottom:none" class="hidden-xs hidden-sm">

                        
                        <div class="row">
                            <div class="col-md-12 form-row">
                                <div class="col-md-12">5. ระยะเวลา</div> 
                            </div> 
                        </div>
                    
                        <div class="row">
                             <div class="col-md-6 form-row">
                                <div class="col-md-12">ตั้งแต่</div>
                                <div class="col-md-12">
                                    <input type="text" name="cmbWorkFrom5" id="cmbWorkFrom5" ng-model="data.jobcmbWorkFrom5" class="hasDatepicker" readonly value="">
                                    <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                    id="iconworkFrom5" ></span> 
                                </div>
                              </div>
                             <div class="col-md-6 form-row">
                                <div class="col-md-12">ถึง</div>
                                <div class="col-md-12">
                                    <input type="text" name="cmbWorkTo5" id="cmbWorkTo5" ng-model="data.jobcmbWorkTo5" class="hasDatepicker" readonly value=""> 
                                    <span class="glyphicon glyphicon-calendar form-control-feedback" 
                                    id="iconworkTo5" ></span> 
                                </div>
                                  <div class="col-md-12 form-row">
                                    <label>หรือ</label> &nbsp; &nbsp;  
                                    <input type="checkbox" id="chWorkTo5" name="chWorkTo5"> 
                                    <label for="chWorkTo5"> 
                                    <span style="height: 19px; width: 19px;"></span> ถึงปัจจุบัน </label>
                                </div>         
                             </div>
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>ตำแหน่ง</label></div>
                                    <div class="col-md-12">

                                        <input type="text" class="textbox" name="txbWorkPosition5" id="txbWorkPosition5" ng-model="data.jobtxbWorkPosition5" maxlength="50" value="">
                                    </div>
                                </div>
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>บริษัท</label></div>
                                    <div class="col-md-12"><input type="text" class="textbox" name="txbWorkCompany5" id="txbWorkCompany5" ng-model="data.jobtxbWorkCompany5" maxlength="50" value=""></div>
                                </div>
                                             
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>เงินเดือน (บาท) </label></div>
                                    <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkSalary5" id="txbWorkSalary5" maxlength="7" ng-model="data.jobtxbWorkSalary5" 
                                    value="" min="0"></div>
                                </div>
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>รายได้อื่น ๆ (บาท) </label></div>
                                    <div class="col-md-12"><input type="number" class="textbox w80-m" name="txbWorkIncome5" id="txbWorkIncome5" maxlength="7"  
                                    ng-model="data.jobtxbWorkIncome5" min="0" value=""></div>
                                </div>
                         
                                <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>ระบุรายได้อื่น ๆ</label></div>
                                    <div class="col-md-12"><input type="text" class="textbox w100-m" 
                                    name="txbWorkIncomeOther5" id="txbWorkIncomeOther5" ng-model="data.jobtxbWorkIncomeOther5" maxlength="100" value=""></div>
                                </div>
                                 <div class="col-md-6 form-row">
                                    <div class="col-md-12"><label>ลักษณะงานที่ทำ</label></div>
                                    <div class="col-md-12"><input type="text" class="textbox" name="txbWorkDetail5" 
                                    id="txbWorkDetail5" ng-model="data.jobtxbWorkDetail5">
                                    </div>
                                </div>
                            
                        </div>

                </div>
            </div>
            
            <div class="">
                <div class="form-title">ความสามารถด้านภาษา</div>
                <div class="language_form form-section">
                    <div class="row hide-mobile">
                        <div class="row form-row">
                        <div class="col-md-6 form-row">
                            <div class="col-md-2">ภาษา</div>
                            <div class="col-md-10 col-sm-offset-1"></div>
                        </div>
                       
                         <div class="col-md-6 form-row">
                            <div class="col-md-4">การพูด</div>
                            <div class="col-md-4">การเขียน</div>
                            <div class="col-md-4">การอ่าน</div>
                        </div>
                         </div>
                    </div>
               
                    <div class="row">
                        <div class="row form-row">
                        <div class="col-md-6">
                            <div class="show-mobile col-md-12"><span>ภาษา</span></div>
                            <div class="col-md-1">1.</div>
                            <div class="col-md-11">
                            <input type="text" class="textbox" name="txbLanguage1" id="txbLanguage1" maxlength="30"  value="" ng-model="data.jobtxbLanguage1"></div> 
                        </div>

                        <div class="col-md-6 form-row">
                            <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การพูด</label></div>
                            <div class="col-md-4">
                                <div class="select-block">
                                    <select name="cmbLanguageTalk1" id="cmbLanguageTalk1" ng-model="data.jobcmbLanguageTalk1">
                                        <option value=""disabled>เลือก</option>
                                        <option value="ดีมาก">ดีมาก</option>
                                        <option value="ดี">ดี</option>
                                        <option value="พอใช้">พอใช้</option>
                                    </select>
                                </div>
                            </div>
                            <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การเขียน</label></div>
                            <div class="col-md-4">
                                <div class="select-block">
                                    <select name="cmbLanguageWrite1" id="cmbLanguageWrite1" ng-model="data.jobcmbLanguageWrite1">
                                        <option value="" disabled>เลือก</option>
                                        <option value="ดีมาก">ดีมาก</option>
                                        <option value="ดี">ดี</option>
                                        <option value="พอใช้">พอใช้</option>
                                    </select>
                                </div>
                            </div>
                            <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การอ่าน</label></div>
                            <div class="col-md-4">
                                <div class="select-block">
                                    <select name="cmbLanguageRead1" id="cmbLanguageRead1" ng-model="data.jobcmbLanguageRead1">
                                        <option value="" disabled>เลือก</option>
                                        <option value="ดีมาก">ดีมาก</option>
                                        <option value="ดี">ดี</option>
                                        <option value="พอใช้">พอใช้</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                       </div>
                    </div>
                   
                    <div class="row">
                      <div class="row form-row">
                          <div class="col-md-6"> 
                        <div class="show-mobile col-md-12"><span>ภาษา</span></div>
                            <div class="col-md-1">2.</div>
                            <div class="col-md-11">
                            <input type="text" class="textbox" name="txbLanguage2" id="txbLanguage2" maxlength="30"  value="" ng-model="data.jobtxbLanguage2">
                            </div> 
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การพูด</label></div>
                            <div class="col-md-4">
                                <div class="select-block">
                                    <select name="cmbLanguageTalk2" id="cmbLanguageTalk2" ng-model="data.jobcmbLanguageTalk2">
                                        <option value=""disabled>เลือก</option>
                                        <option value="ดีมาก">ดีมาก</option>
                                        <option value="ดี">ดี</option>
                                        <option value="พอใช้">พอใช้</option>
                                    </select>
                                </div>
                            </div>
                            <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การเขียน</label></div>
                            <div class="col-md-4">
                                <div class="select-block">
                                    <select name="cmbLanguageWrite2" id="cmbLanguageWrite2" ng-model="data.jobcmbLanguageWrite2">
                                        <option value=""disabled>เลือก</option>
                                        <option value="ดีมาก">ดีมาก</option>
                                        <option value="ดี">ดี</option>
                                        <option value="พอใช้">พอใช้</option>
                                    </select>
                                </div>
                            </div>
                            <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การอ่าน</label></div>
                            <div class="col-md-4">
                                <div class="select-block">
                                    <select name="cmbLanguageRead2" id="cmbLanguageRead2" ng-model="data.jobcmbLanguageRead2">
                                        <option value="" disabled>เลือก</option>
                                        <option value="ดีมาก">ดีมาก</option>
                                        <option value="ดี">ดี</option>
                                        <option value="พอใช้">พอใช้</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                    <div class="row">
                     <div class="row form-row">
                        <div class="col-md-6">
                            <div class="col-md-1"><span class="show-mobile">ภาษา</span>3.</div>
                            <div class="col-md-11"><input type="text" class="textbox" 
                            ng-model="data.jobtxbLanguage3" 
                            name="txbLanguage3" id="txbLanguage3" maxlength="30"  value=""></div>
                        </div>
                         <div class="col-md-6 form-row">
                            <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การพูด</label></div>
                            <div class="col-md-4">
                                <div class="select-block">
                                    <select name="cmbLanguageTalk3" id="cmbLanguageTalk3" ng-model="data.jobcmbLanguageTalk3">
                                        <option value="" disabled>เลือก</option>
                                        <option value="ดีมาก">ดีมาก</option>
                                        <option value="ดี">ดี</option>
                                        <option value="พอใช้">พอใช้</option>
                                    </select>
                                </div>
                            </div>
                            <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การเขียน</label></div>
                            <div class="col-md-4">
                                <div class="select-block">
                                    <select name="cmbLanguageWrite3" id="cmbLanguageWrite3" ng-model="data.jobcmbLanguageWrite3">
                                        <option value="" disabled>เลือก</option>
                                        <option value="ดีมาก">ดีมาก</option>
                                        <option value="ดี">ดี</option>
                                        <option value="พอใช้">พอใช้</option>
                                    </select>
                                </div>
                            </div>
                            <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การอ่าน</label></div>
                            <div class="col-md-4">
                                <div class="select-block">
                                    <select name="cmbLanguageRead3" id="cmbLanguageRead3" ng-model="data.jobcmbLanguageRead3">
                                        <option value=""disabled>เลือก</option>
                                        <option value="ดีมาก">ดีมาก</option>
                                        <option value="ดี">ดี</option>
                                        <option value="พอใช้">พอใช้</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                    <div class="row">
                     <div class="row form-row">
                        <div class="col-md-6">
                            <div class="col-md-1"><span class="show-mobile">ภาษา</span>4.</div>
                            <div class="col-md-11"><input type="text" class="textbox" name="txbLanguage4" id="txbLanguage4" ng-model="data.jobtxbLanguage4" maxlength="30"  value=""></div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การพูด</label></div>
                            <div class="col-md-4">
                                <div class="select-block">
                                    <select name="cmbLanguageTalk4" id="cmbLanguageTalk4" ng-model="data.jobcmbLanguageTalk4">
                                        <option value=""disabled>เลือก</option>
                                        <option value="ดีมาก">ดีมาก</option>
                                        <option value="ดี">ดี</option>
                                        <option value="พอใช้">พอใช้</option>
                                    </select>
                                </div>
                            </div>
                            <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การเขียน</label></div>
                            <div class="col-md-4">
                                <div class="select-block">
                                    <select name="cmbLanguageWrite4" id="cmbLanguageWrite4" ng-model="data.jobcmbLanguageWrite4">
                                        <option value="" disabled>เลือก</option>
                                        <option value="ดีมาก">ดีมาก</option>
                                        <option value="ดี">ดี</option>
                                        <option value="พอใช้">พอใช้</option>
                                    </select>
                                </div>
                            </div>
                            <div class="show-mobile col-md-3"><label style="padding-top: 10px;">การอ่าน</label></div>
                            <div class="col-md-4">
                                <div class="select-block">
                                    <select name="cmbLanguageRead4" id="cmbLanguageRead4" ng-model="data.jobcmbLanguageRead4" >
                                        <option value=""disabled>เลือก</option>
                                        <option value="ดีมาก">ดีมาก</option>
                                        <option value="ดี">ดี</option>
                                        <option value="พอใช้">พอใช้</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                    <div class="row">
                     <div class="row form-row">
                        <div class="col-md-6 col-xs-12 form-row">
                            <div class="col-md-2 col-sm-2 col-xs-12"> <label>พิมพ์ดีด </label></div>
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                 <input type="number" class="textbox" name="txbTypingThai" id="txbTypingThai" maxlength="2" style="width:40%" min="0" ng-model="data.jobtxbTypingThai" value=""> &nbsp; ไทย
                            </div>
                            <div class="col-md-4 col-sm-5 col-xs-6">
                                 <input type="number" class="textbox" min="0" name="txbTypingEng" id="txbTypingEng" maxlength="2" style="width:40%" min="0" ng-model="data.jobtxbTypingEng" value=""> &nbsp; อังกฤษ
                            </div>
                        </div>
                      </div>
                    </div>
                </div>

                <div class="form-title">ลักษณะงานที่สนใจ</div>
                <div class="job_interesting form-section">        
                    <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-md-12"><label>ตำแหน่งงาน<span class="f-required">*</span></label></div>
                            <div class="col-md-12">
                                <input type="text" class="textbox" name="txbInterestPosition" id="txbInterestPosition" 
                                ng-model="data.jobtxbInterestPosition" maxlength="50" value="" required>
                                <span ng-show="!jobform.txbInterestPosition.$pristine && jobform.txbInterestPosition.$error.required" style="color : red" > โปรดกรอกตำแหน่งงาน </span>  
                            </div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="col-md-12"><label>ประเภทงาน</label></div>
                            <div class="col-md-4 col-sm-6 col-xs-6">
                                <input type="radio" name="rdoInterestPositionType" id="rdoInterestPositionTypeF" ng-model="data.jobrdoInterestPositionType" value="F" checked=""> <label for="rdoInterestPositionTypeF">
                                <span></span>Full Time</label></div>
                             <div class="col-md-4 col-sm-6 col-xs-6">
                                <input type="radio" name="rdoInterestPositionType" id="rdoInterestPositionTypeP" ng-model="data.jobrdoInterestPositionType" value="P">  <label for="rdoInterestPositionTypeP">
                                <span></span>Part Time</label></div>
                            </div>
                    </div>
                   
                    
                    <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-md-12"><label>เงินเดือนที่คาดหวัง<span class="f-required">*</span> (บาท)</label>
                            </div>
                            <div class="col-md-12">
                                <input type="number" class="textbox" name="txbExpectSalary" id="txbExpectSalary" 
                                ng-model="data.jobtxbExpectSalary" min="0" ng-maxlength="9"  required> 
                                <span ng-show="!jobform.txbExpectSalary.$pristine && jobform.txbExpectSalary.$error.required" style="color : red" > &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; โปรดกรอกเงินเดือนที่คาดหวัง </span>
                                 <span ng-show="jobform.txbExpectSalary.$error.minlength || jobform.txbExpectSalary.$error.maxlength" style="color : red" > &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; เงินเดือนที่คาดหวังสูงเกินกำหนด </span>  
                            </div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="col-md-12"><label>พร้อมเริ่มงานตั้งแต่วันที่<span class="f-required">*</span></label></div>
                            <div class="col-md-12 form-group has-feedback">
                                <input type="text" name="dateWork" id="dateWork" ng-model="data.jobdateWork" readonly="" 
                                value="" class="hasDatepicker" required>
                                <span class="glyphicon glyphicon-calendar form-control-feedback" id="iconstartdate" ></span> 
                            </div>
                        </div>
                    </div>
                
                </div>

                <div class="form-title">บุคคลอ้างอิง (ไม่ใช่ญาติ หรือคนในครอบครัว)</div>
                <div class="ref_person form-section">
                    <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-md-12"><label>ชื่อ-นามสกุล</label></div>
                            <div class="col-md-12"><input type="text" class="textbox" name="txbReferenceName" id="txbReferenceName" ng-model="data.jobtxbReferenceName" maxlength="50" value=""></div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="col-md-12"><label>ตำแหน่ง</label></div>
                            <div class="col-md-12"><input type="text" class="textbox" name="txbReferencePosition" id="txbReferencePosition" ng-model="data.jobtxbReferencePosition" maxlength="50" value=""></div>
                        </div>
                    </div>
                    <div class="row">
                        
                        <div class="col-md-6 form-row">
                            <div class="col-md-12"><label>บริษัท</label></div>
                            <div class="col-md-12"><input type="text" class="textbox" name="txbReferenceCompany" id="txbReferenceCompany" ng-model="data.jobtxbReferenceCompany" maxlength="50" value=""></div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="col-md-12"><label>ความสัมพันธ์</label></div>
                            <div class="col-md-12"><input type="text" class="textbox" name="txbReferenceRelation" id="txbReferenceRelation" ng-model="data.jobtxbReferenceRelation" maxlength="50" value=""></div>
                        </div>
                    </div>
                    <div class="row">                    
                        <div class="col-md-6 form-row">
                            <div class="col-md-12"><label>เบอร์โทรติดต่อ</label></div>
                            <div class="col-md-12"><input type="number" class="textbox" 
                            name="txbReferenceTel" id="txbReferenceTel" min="0" maxlength="10" 
                            ng-model="data.jobtxbReferenceTel"></div>
                        </div>
                    </div>
                </div>
           </div>
           
            <div class="row" align="center">
                <div class="col-md-2"></div>
                <div class="col-md-4 form-row"> 
                    <input type="button" name="previous" id="btnprevious1" class="previous action-button" value="ย้อนกลับ" />
                </div>
                <div class="col-md-4 form-row">    
                   <input type="button" id="btnnext2" name="next" class="next action-button" 
                   value="ต่อไป" />
                </div>
            </div>

    </fieldset>

    <fieldset>

            <div class="form-title">ข้อมูลทั่วไป</div>
            <div class="common_info form-section">
                <div class="row">
                    <div class="col-md-12 form-row">
                        <div class="col-md-12">
                            <span>การไปปฏิบัติงาน ณ โครงการหมู่บ้านท่านขัดข้องหรือไม่<span class="f-required">*</span></span>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12 form-row">
                        <div class="col-md-3"><label>เป็นประจำ</label></div>
                            <div class="col-md-2">
                                <input type="radio" name="rdoWorkSite" id="rdoWorkSiteY" ng-model=data.jobrdoWorkSite value="Y" 
                                required> &nbsp; <label for="rdoWorkSiteY"><span></span>ไม่ขัดข้อง</label>
                            </div>
                            <div class="col-md-2">
                                <input type="radio" name="rdoWorkSite" id="rdoWorkSiteN" ng-model=data.jobrdoWorkSite value="N"  required> &nbsp; <label for="rdoWorkSiteN"><span></span>ขัดข้อง</label>
                            </div>
                            <div class="col-md-1"><label>โปรดระบุ</label></div>
                            <div class="col-md-4">                    
                                <input type="text" class="textbox w80-m" name="txbWorkSite" 
                                ng-model="data.jobrdoWorkSiteNo" ng-disabled="data.jobrdoWorkSite!='N'"
                                id="txbWorkSite" maxlength="50">
                            </div>
                        
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 form-row">
                        <div class="col-md-3"><label>เป็นครั้งคราว</label></div>
                            <div class="col-md-2">
                                <input type="radio" name="rdoWorkSiteFew" id="rdoWorkSiteFewY" value="Y" 
                                ng-model="data.jobrdoWorkSiteFew" required> &nbsp; <label for="rdoWorkSiteFewY"><span></span>ไม่ขัดข้อง</label>
                            </div>
                            <div class="col-md-2">
                                <input type="radio" name="rdoWorkSiteFew" id="rdoWorkSiteFewN" ng-model="data.jobrdoWorkSiteFew" value="N"  required> &nbsp; <label for="rdoWorkSiteFewN"><span></span>ขัดข้อง</label>
                            </div>
                        <div class="col-md-1"><label>โปรดระบุ</label></div>
                        <div class="col-md-4">
                            <input type="text" class="textbox w80-m" name="txbWorkSiteFew" id="txbWorkSiteFew" maxlength="50" ng-model="data.jobrdoWorkSiteFewNo" ng-disabled="data.jobrdoWorkSiteFew!='N'" value="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 form-row">
                        <div class="col-md-3"><label>ท่านมีโรคเเรื้อรัง/โรคประจำตัวหรือไม่<span class="f-required">*</span></label></div>
                            <div class="col-md-2">
                                <input type="radio" name="rdoHasDisease" id="rdoHasDiseaseN" ng-model="data.jobrdoHasDisease" 
                                value="N" required checked > &nbsp; <label for="rdoHasDiseaseN"><span></span>ไม่มี</label>
                            </div>
                            <div class="col-md-2">
                                <input type="radio" name="rdoHasDisease" id="rdoHasDiseaseY" ng-model="data.jobrdoHasDisease" 
                                value="Y" required> &nbsp; <label for="rdoHasDiseaseY"><span></span>มี</label>
                            </div>
                            <div class="col-md-1"><label>โปรดระบุ</label></div>
                            <div class="col-md-4">
                                <input type="text" class="textbox w80-m" ng-model="data.jobrdoHasDiseaseYes" name="txbHasDisease" id="txbHasDisease" maxlength="50" 
                                ng-disabled="data.jobrdoHasDisease!='Y'" value="">
                            </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 form-row">
                        <div class="col-md-3"><label>ท่านเคยต้องโทษทางคดีอาญาหรือคดีแพ่งหรือไม่<span class="f-required">*</span></label></div>
                            <div class="col-md-2">
                                <input type="radio" name="rdoHasAccuse" id="rdoHasAccuseN" value="N" ng-model="data.jobrdoHasAccuse"  
                                required > &nbsp; <label for="rdoHasAccuseN"><span></span>ไม่เคย</label>
                            </div>
                            <div class="col-md-2">
                                <input type="radio" name="rdoHasAccuse" id="rdoHasAccuseY" value="Y" ng-model="data.jobrdoHasAccuse" required> &nbsp; <label for="rdoHasAccuseY"><span></span>เคย</label>
                            </div>
                            <div class="col-md-1"><label>โปรดระบุ</label></div>
                            <div class="col-md-4">
                                <input type="text" class="textbox w80-m" name="txbHasAccuse" 
                                id="txbHasAccuse" maxlength="50" ng-model="data.jobrdoHasAccuseYes" 
                                value="" ng-disabled="data.jobrdoHasAccuse!='Y'">
                            </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 form-row">
                        <div class="col-md-3"><label>ท่ายเคยถูกศาลสั่งให้เป็นบุคคล้มละลาย หรือถูกศาลสั่งพิทักษ์ทรัพย์ 
                        หรือมีหนี้สินล้นพ้นตัวหรือไม่<span class="f-required">*</span></label></div>
                            <div class="col-md-2">
                                <input type="radio" name="rdoHasBankrupt" id="rdoHasBankruptN" value="N"  ng-model="data.jobrdoHasBankrupt"required > &nbsp; <label for="rdoHasBankruptN"><span></span>ไม่เคย</label>
                            </div>
                            <div class="col-md-2">
                                <input type="radio" name="rdoHasBankrupt" id="rdoHasBankruptY" value="Y" ng-model="data.jobrdoHasBankrupt" required> &nbsp; <label for="rdoHasBankruptY"><span></span>เคย</label>
                            </div>
                            <div class="col-md-1"><label>โปรดระบุ</label></div>
                            <div class="col-md-4">
                                <input type="text" class="textbox w80-m" name="txbHasBankrupt" id="txbHasBankrupt" maxlength="50" value="" ng-model="data.jobrdoHasBankruptYes" ng-disabled="data.jobrdoHasBankrupt!='Y'">
                            </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 form-row">
                        <div class="col-md-3"><label>ท่ายเคยมีประวัติถูกเลิกจ้างเพราะเหตุกระทำผิดมาก่อนหรือไม่<span class="f-required">*</span></label></div>
                            <div class="col-md-2">
                                <input type="radio" name="rdoHasLayoff" id="rdoHasLayoffN" ng-model="data.jobrdoHasLayoff" value="N"  required > 
                                &nbsp; <label for="rdoHasLayoffN"><span></span>ไม่เคย</label>
                            </div>
                            <div class="col-md-2">
                                <input type="radio" name="rdoHasLayoff" id="rdoHasLayoffY" ng-model="data.jobrdoHasLayoff" value="Y"  required > &nbsp; <label for="rdoHasLayoffY"><span></span>เคย</label>
                            </div>
                              <div class="col-md-1"><label>โปรดระบุ</label></div>
                            <div class="col-md-4">
                                <input type="text" class="textbox w80-m" name="txbHasLayoff"  id="txbHasLayoff" maxlength="50" ng-model="data.jobrdoHasLayoffYes" 
                                ng-disabled="data.jobrdoHasLayoff!='Y'">   
                            </div>
                    </div>
                </div>
            </div>

            <div class="form-title">ข้อมูลเพิ่มเติม</div>
            <div class="form-section">
                <textarea name="txbFurtherInfo" id="txbFurtherInfo" class="w100-m" 
                ng-model="data.jobtxbFurtherInfo"  style="height:150px;"></textarea>
            </div>
            <div class="form-title">เอกสารแนบ</div>
            <div class="document_form">
             
                <label style="font-size: 25px;">แนบ Resume</label><br>
                
                <div class="row">
                   <input type="file"  name="flResume" ng-model="data.jobflResume" id="flResume" 
                   class="file">
                    <div class="input-group col-md-12" style="padding-left: 10px; padding-right: 10px;">    
                        <input type="text" class="form-control input-lg" disabled style="background: white;">
                    <span class="input-group-btn">
                        <button class="browse btn input-lg" type="button" style="border-radius: 1px; 
                    background: rgba(111, 151, 85, 0.9); color: white;">
                    <span class="glyphicon glyphicon-folder-open"></span>&nbsp; &nbsp;  เลือกไฟล์ </button>
                  
                 </div>
                  <span id="errresume"  style="color : red; display: none;">ขนาดของ File เกิน 1 MB.</span>
                <div class="col-md-6">ขนาดของ File ไม่เกิน 1 MB. ในกรณีมีหลาย File ให้ Zip เป็น 1 File <br></div>
                </div>


                <label style="font-size: 25px;">แนบรูป</label> <br>
                <div class="row">    
                    <input type="file"  name="flPicture" ng-model="data.jobflPicture" id="flPicture" class="file">
                     <div class="input-group col-sm-12" style="padding-left: 10px; padding-right: 10px;">                    
                            <input type="text" class="form-control input-lg" style="background: white;" disabled>
                        <span class="input-group-btn">
                            <button class="browse btn input-lg" type="button" style="border-radius: 1px; 
                        background: rgba(111, 151, 85, 0.9); color: white;">
                        <span class="glyphicon glyphicon-folder-open"></span>&nbsp; &nbsp; เลือกไฟล์ </button>
                        </span>
                     </div>
                     <span id="errpic"  style="color : red; display: none;">โปรดอัพโหลดนามสกุล .jpg, .gif หรือ .tif </span>
                     <span id="errsize"   style="color : red; display: none;">ขนาดของ File เกิน 500 KB. </span>
                     <div class="col-md-6">ขนาดของ File ไม่เกิน 500 KB. ในรูปแบบของ .jpg, .gif หรือ .tif </div>  
                </div>
                       

                <div class="row">
                    <div class="col-sm-12 form-row">
                        <div class="col-md-12">
                        <input type="checkbox" id="chbAccept" name="chbAccept" 
                         value="Y" ng-model="data.jobchbAccept" required>
                        <label for="chbAccept" style="font-size: 24px;"> <span></span>ข้าพเจ้าขอรับรองว่า ข้อความทั้งหมดเป็นจริงทุกประการ การบิดเบือนหรือปิดบังข้อเท็จจริงใด ๆ ในใบสมัครนี้ ย่อมเป็นสาเหตุเพียงพอที่จะเลิกจ้างข้าพเจ้าได้ทันทีโดยไม่มีข้อแม้ใด ๆ ทั้งสิ้น</label>
                        </div>
                    </div>
                </div><br>
               </div> 
               <div class="row" align="center">
                        <div class="col-md-4 form-row">
                        <input type="button" name="previous" id="btnprevious2" 
                        class="previous action-button" value="ย้อนกลับ" />
                        </div>
                        <div class="col-md-4 form-row">
                        <input type ="button" name="btnPreview" id="btnPreview"  value=" ดูใบสมัครก่อนส่ง" 
                        data-toggle="modal" data-target="#myModal" ng-disabled="jobform.$error.required || jobform.$error.minlength || jobform.$error.maxlength">  
                        </div>
                        <div  class="col-md-4 form-row"> 
                        <input type = "submit" name="btnSend" id="btnSend" value="ส่งใบสมัคร" ng-disabled="jobform.$error.required || jobform.$error.minlength || jobform.$error.maxlength"
                        ng-submit="myFunc()">
                        </div>  
                                 
                </div>
                    <br>
                <div class="row">
                    <div class="col-sm-12 form-row">
                        สอบถามข้อมูลการสมัครงาน ติดต่อ คุณยุพาวณี 02-230-8359
                    </div>
                </div>
            </div>
            
            <a id="splash_app" class="iframe" href="app_preview.jsp?id=2240" style="display:none">Splash</a>
            <div class="clear"></div>
            </fieldset>

    </form>
<a id="backJobLists" class="btn-backjob">&lt; Back</a>
                                            <!-- ======================================= -->


                                            <?php //include('template-part/tpl-apply-job.php'); ?>




                                        </div>
                                    </div>
                                </div>







                                <div id="tab-2" class="tab-content">
                                    <div class="tab-block">
                                        <div class="indent">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <h2>ด้านอาชีพและอนาคตของพนักงาน</h2>
                                                    <ul class="job-lists">
                                                        <li>โบนัส</li>
                                                        <li>ส่วนลดซื้อสินค้าบริษัท ฯ</li>
                                                        <li>รางวัลที่ได้จากอายุงาน</li>
                                                        <li>กองทุนสำรองเลี้ยงชีพ</li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4">
                                                    <h2>ด้านความเป็นอยู่พนักงานและครอบครัว</h2>
                                                    <ul class="job-lists">
                                                        <li>ประกันสุขภาพพนักงาน (ผู้ป่วยใน, ผู้ป่วยนอก)</li>
                                                        <li>ประกันสุขภาพ (ครอบครัวพนักงาน)</li>
                                                        <li>การทำประกันขีวิตให้พนักงาน</li>
                                                        <li>การตรวจสุขภาพประจำปี</li>
                                                        <li>เงินช่วยเหลือด้านทุนการศึกษาบุตร</li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4">
                                                    <h2>ด้านอื่น ๆ</h2>
                                                    <ul class="job-lists">
                                                        <li>เงินช่วยพิธีมงคลสมรส</li>
                                                        <li>เงินช่วยพิธีมรณะกรรมพนักงาน และครอบครัวพนักงาน</li>
                                                        <li>ชุดฟอร์มพนักงาน</li>
                                                        <li>สันทนาการประจำปี</li>
                                                        <li>วันลาต่าง ๆ (ลากิจ ลาป่วย ลาคลอด ลาทำบัตรประชาชน ลาพิธีมงคลสมรส ลาเพื่อไปคัดเลือกในการเกณฑ์ทหาร ลาพักผ่อนประจำปี)</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab-3" class="tab-content">
                                    <div class="tab-block">
                                        <div class="indent">
                                            <h2>มีเหตุผลมากมายที่คุณจะเลือกเรา</h2>
                                            <ul class="job-lists">
                                                <li>เราเป็นหนึ่งในกลุ่มธุรกิจพัฒนาอสังหาริมทรัพย์ของประเทศ เรามอบที่อยู่อาศัยที่ดีและมีความสุขให้แก่ลูกค้า ที่ครบถ้วนและหลายหลาย และสำหรับคุณ นี่หมายถึงโอกาสการทำงานที่กว้างไกลและหลากหลาย</li>
                                                <li>นอกจากนี้ ชื่อเสียงของบริษัทฯ ที่ได้รับรางวัลระดับประเทศอย่างมากมาย คุณลองนึกภาพดูว่าคุณจะสามารถเรียนรู้อะไรได้บ้างจากบุคคลเหล่านี้ ที่มีทั้งทักษะ ความสามารถ และประสบการณ์จนทำให้ Land &amp; Houses ได้รับการยกย่องชมเชยเช่นนี้</li>
                                                <li>และที่สำคัญที่สุด บริษัทฯ ได้มีการฝึกอบรมพนักงานในบริษัทฯ อย่างต่อเนี่อง หลักสูตรการฝึกอบรมจะจัดขึ้นให้สอดคล้องกับลักษณะการทำงานแต่ละหน่วยงาน เพื่อจะส่งผลการพัฒนาศักยภาพของพนักงานอย่างสม่ำเสมอ</li>
                                                <li>ไม่ว่าสิ่งทีคุณกำลังหาอยู่คืออะไร เราเชื่อว่าคุณจะพบกับประสบการณ์ที่มีค่าจากการมาร่วมงานกับเรา</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab-4" class="tab-content">
                                    <div class="tab-block">
                                        <div class="indent">
                                            <p class="center">ไม่มีข้อมูล</p>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab-5" class="tab-content">
                                    <div class="tab-block">

                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>


                    </div>
                </div>

            </div>

        </div>
    </div>










<!-- popup -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">ใบสมัคร</h4>
        </div>
        <div class="modal-body">
            <h5>ข้อมูลส่วนตัว</h5><br>
            <div class="row">
                <div class="col-md-12 form-row">
                    <div class="col-md-2"><label>ชื่อ-สกุล</label></div>
                    <div class="col-sm-8">
                    {{  data.jobcmbTitle + "  " + data.jobtxbFName + " " + data.jobtxbLName  }}
                    </div>                   
                </div>

                
            </div><br>

            <div class="row">
                <div class="col-md-4 form-row">
                    <div class="col-md-4"><label>ชื่อเล่น</label></div>
                    <div class="col-sm-8">
                         {{ data.jobtxbNickname }} 
                    </div>
                </div>
               <div class="col-md-4 form-row">
                    <div class="col-md-4 col-sm-3"><label>เพศ</label></div>
                    <div class="col-md-8 col-sm-9 mb10-m">
                        {{ data.jobrdoGender }}
                    </div>
                </div>
                
                 <div class="col-md-4 form-row">
                    <div class="col-md-4 col-sm-3"><label>วันเกิด</label></div>
                    <div class="col-md-8 col-sm-9"> {{ data.jobdateBirth }}
                        
                    </div>
                </div>
                

            </div><br>

            <div class="row">

                <div class="col-md-4 form-row">
                    <div class="col-md-4 col-sm-3"><label>ส่วนสูง</label></div>
                    <div class="col-md-8 col-sm-9"> {{ data.jobtxbHeight}} &nbsp; เซนติเมตร</div>
                </div>
                <div class="col-md-4 form-row">
                    <div class="col-md-4 col-sm-3"><label>น้ำหนัก</label></div>
                    <div class="col-md-8 col-sm-9"> {{ data.jobtxbWeight }} &nbsp; กิโลกรัม
                    </div>
                </div>

                <div class="col-md-4 form-row">
                    <div class="col-md-4"><label>ศาสนา</label></div>
                    <div class="col-sm-8"> {{ data.jobcmbReligion + ' ' + data.txbReligionOther }}
                    </div>
                </div>

            </div><br>

            <div class="row">

                <div class="col-md-4 form-row">
                    <div class="col-md-4 col-sm-3"><label>ภูมิลำเนา</label></div>
                    <div class="col-md-8 col-sm-9">
                        {{ data.jobcmbBirthProvince }}
                    </div>
                </div>
                <div class="col-md-4 form-row">
                    <div class="col-md-4 col-sm-3">เชื้อชาติ</div>
                    <div class="col-sm-8">
                        {{ data.jobrdoNationality + ' ' + data.txbNationalityOther }}
                    </div>
                </div>
                <div class="col-md-4 form-row">
                    <div class="col-md-4 col-sm-3"><label>สัญชาติ</label></div>
                    <div class="col-sm-8">
                        {{ data.jobrdoRace + ' ' + data.txbRaceOther }}
                    </div>
                </div>
            </div><br>
            <div class="row">

                 <div class="col-md-4 form-row">
                    <div class="col-md-5 col-sm-3"><label>เกณฑ์ทหาร</label></div>
                    <div class="col-sm-7"> {{ data.jobcmbConscription +' '+ data.txbConscription }} </div>
                </div>
                 
                <div class="col-md-8 form-row">
                    <div class="col-md-4"><label class="text-left">เลขบัตรประจำตัวประชาชน</label>
                    </div>
                    <div class="col-sm-8"> {{ data.jobtxbIDNo }} </div>
                </div>
               
                
            </div><br>

            <h5>ข้อมูลครอบครัว</h5><br>

             <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-md-4 col-sm-4">
                        <label>สถานภาพสมรส</label>
                    </div>
                    <div class="col-md-2 col-sm-8"> {{ data.jobrdoMarried }} </div>
                </div>
            </div><br>

             <div class="row hide-mobile">
                <div class="col-sm-2">ครอบครัว</div>
                <div class="col-sm-3">ชื่อ-นามสกุล</div>
                <div class="col-sm-1">อายุ</div>
                <div class="col-sm-3">อาชีพ/ตำแหน่ง</div>
                <div class="col-sm-3">ที่อยู่/ที่ทำงาน</div>
            </div><br>
            
             <div class="row form-row">
                <div class="col-md-2"><label>บิดา</label></div>
                <div class="show-mobile col-sm-4 mb10-m">ชื่อนาม-นามสกุล</div>
                <div class="col-md-3 col-sm-8 mb10-m">  {{ data.jobtxbFatherName }} </div>
                <div class="show-mobile col-sm-4 mb10-m">อายุ</div>
                <div class="col-md-1 col-sm-8 mb10-m">  {{ data.jobtxbFatherAge }} </div>
                <div class="show-mobile col-sm-4 mb10-m">อาชีพ/ตำแหน่ง</div>
                <div class="col-md-3 col-sm-8 mb10-m"> {{ data.jobtxbFatherOcc  }} </div>
                <div class="show-mobile col-sm-4 mb10-m">ที่อยู่/ที่ทำงาน</div>
                <div class="col-md-3 col-sm-8 mb10-m">{{ data.jobtxbFatherAddr }}</div>
            </div><br>

            <div class="row form-row">
                <div class="col-md-2"><label>มารดา</label></div>
                <div class="show-mobile col-sm-4 mb10-m">ชื่อนาม-นามสกุล</div>
                <div class="col-md-3 col-sm-8 mb10-m">{{ data.jobtxbMotherName }}</div>
                <div class="show-mobile col-sm-4 mb10-m">อายุ</div>
                <div class="col-md-1 col-sm-8 mb10-m"> {{ data.jobtxbMotherAge }} </div>
                <div class="show-mobile col-sm-4 mb10-m">อาชีพ/ตำแหน่ง</div>
                <div class="col-md-3 col-sm-8 mb10-m">{{ data.jobtxbMotherOcc }}</div>
                <div class="show-mobile col-sm-4 mb10-m">ที่อยู่/ที่ทำงาน</div>
                <div class="col-md-3 col-sm-8 mb10-m"> {{ data.jobtxbMotherAddr }}  </div>
            </div><br>

            <div class="row form-row">
                <div class="col-md-2"><label>พี่น้อง 1</label></div>
                <div class="show-mobile col-sm-4 mb10-m">ชื่อนาม-นามสกุล</div>
                <div class="col-md-3 col-sm-8 mb10-m">{{ data.jobtxbCousin1Name }}</div>
                <div class="show-mobile col-sm-4 mb10-m">อายุ</div>
                <div class="col-md-1 col-sm-8 mb10-m">{{ data.jobtxbCousin1Age }}</div>
                <div class="show-mobile col-sm-4 mb10-m">อาชีพ/ตำแหน่ง</div>
                <div class="col-md-3 col-sm-8 mb10-m">{{ data.jobtxbCousin1Occ }}</div>
                <div class="show-mobile col-sm-4 mb10-m">ที่อยู่/ที่ทำงาน</div>
                <div class="col-md-3 col-sm-8 mb10-m">{{ data.jobtxbCousin1Addr }}</div>
            </div><br>

             <div class="row form-row">
                <div class="col-md-2"><label>พี่น้อง 2</label></div>
                <div class="show-mobile col-sm-4 mb10-m">ชื่อนาม-นามสกุล</div>
                <div class="col-md-3 col-sm-8 mb10-m">{{ data.jobtxbCousin2Name }}</div>
                <div class="show-mobile col-sm-4 mb10-m">อายุ</div>
                <div class="col-md-1 col-sm-8 mb10-m"> {{ data.jobtxbCousin2Age }}</div>
                <div class="show-mobile col-sm-4 mb10-m">อาชีพ/ตำแหน่ง</div>
                <div class="col-md-3 col-sm-8 mb10-m">{{ data.jobtxbCousin2Occ }}</div>
                <div class="show-mobile col-sm-4 mb10-m">ที่อยู่/ที่ทำงาน</div>
                <div class="col-md-3 col-sm-8 mb10-m">{{ data.jobtxbCousin2Addr }}</div>
            </div><br>

             <div class="row form-row">
                <div class="col-md-2"><label>พี่น้อง 3</label></div>
                <div class="show-mobile col-sm-4 mb10-m">ชื่อนาม-นามสกุล</div>
                <div class="col-md-3 col-sm-8 mb10-m">{{ data.jobtxbCousin3Name }}</div>
                <div class="show-mobile col-sm-4 mb10-m">อายุ</div>
                <div class="col-md-1 col-sm-8 mb10-m"> {{ data.jobtxbCousin3Age }}</div>
                <div class="show-mobile col-sm-4 mb10-m">อาชีพ/ตำแหน่ง</div>
                <div class="col-md-3 col-sm-8 mb10-m">{{ data.jobtxbCousin3Occ }} </div>
                <div class="show-mobile col-sm-4 mb10-m">ที่อยู่/ที่ทำงาน</div>
                <div class="col-md-3 col-sm-8 mb10-m">{{ data.jobtxbCousin3Addr }}</div>
            </div><br>

              <div class="row form-row">
                <div class="col-md-2"><label>คู่สมรส</label></div>
                <div class="show-mobile col-sm-4 mb10-m">ชื่อนาม-นามสกุล</div>
                <div class="col-md-3 col-sm-8 mb10-m">{{ data.jobtxbSpouseName }}</div>
                <div class="show-mobile col-sm-4 mb10-m">อายุ</div>
                <div class="col-md-1 col-sm-8 mb10-m">{{ data.jobtxbSpouseAge }} </div>
                <div class="show-mobile col-sm-4 mb10-m">อาชีพ/ตำแหน่ง</div>
                <div class="col-md-3 col-sm-8 mb10-m">{{ data.jobtxbSpouseOcc }}</div>
                <div class="show-mobile col-sm-4 mb10-m">ที่อยู่/ที่ทำงาน</div>
                <div class="col-md-3 col-sm-8 mb10-m">{{ data.jobtxbSpouseAddr }}</div>
            </div><br>
            <div class="row form-row">
                <div class="col-md-2 col-sm-4"><label>จำนวนบุตร</label></div>
                <div class="col-sm-4"> {{ data.jobtxbChildNum }} &nbsp; คน</div>
            </div><br>
            <h5>ข้อมูลการติดต่อ</h5><br>
            <h6>ที่อยู่ปัจจุบันที่ติดต่อได้</h6><br>
            <div class="row">
                <div class="col-sm-8 form-row">
                    <div class="col-md-2 col-sm-3"><label>ที่อยู่</label></div>
                    <div class="col-md-10 col-sm-9">
                        {{ data.jobtxbAddress }}
                    </div>
                </div>
                <div class="col-sm-4 form-row">
                    <div class="col-md-3 col-sm-3"><label>จังหวัด</label></div>
                    <div class="col-md-9 col-sm-9">
                        <label> ชลบุรี </label>
                    </div>
                </div>
            </div><br>

            <div class="row">
                <div class="col-sm-6 form-row">
                    <div class="col-md-7"><label>เบอร์โทรศัพท์ที่สามารถติดต่อได้</label></div>
                    <div class="col-md-5 col-md-offset-0 col-sm-9 col-sm-offset-3">
                      {{ data.jobtxbContactTel }}
                    </div>
                </div>
                 <div class="col-sm-6 form-row">
                    <div class="col-md-3 col-sm-3"><label>E-mail</label></div>
                    <div class="col-md-9 col-sm-9">
                       {{ data.jobtxbContactEmail }}
                    </div>
                </div>
            </div><br>
            <h6>บุคคลที่ติดต่อได้ในกรณีฉุกเฉิน</h6><br>
            <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-md-4 col-sm-3"><label>ชื่อ</label></div>
                    <div class="col-md-8 col-sm-9">
                        {{ data.jobtxbEmergencyPerson }}
                    </div>
                </div>
                <div class="col-md-6 form-row">
                    <div class="col-md-4 col-sm-4"><label>เบอร์โทรศัพท์</label></div>
                    <div class="col-md-8 col-sm-8 ">
                      {{ data.jobtxbEmergencyTel }}
                    </div>
                </div>
            </div><br>
           
            <h5>ข้อมูลการศึกษา</h5><br>
            <div class="row">
                <div class="col-sm-12 form-row">
                    <div class="col-md-2 col-sm-4"><label>การศึกษาสูงสุด</label></div>
                    <div class="col-md-6 col-sm-8">
                        {{ data.jobcmbHighestGraduate }}
                    </div>
                </div>
            </div><br>
            <h6>ประวัติการศึกษา</h6><br>

                <div class="row">
                        <div class="col-sm-12 form-row">
                            <div class="col-md-2"> <label>1. ระยะเวลา </label></div>
                            <div class="col-md-1 col-sm-4 mb10-m">
                                <label>ตั้งแต่</label>
                            </div>
                            <div class="col-md-2 col-sm-8 mb10-m">
                                {{ data.jobcmbStudyFrom1 }}
                            </div>
                            
                            <div class="col-md-1 col-sm-4">
                               <label>ถึง</label> 
                            </div>
                            <div class="col-md-3 col-sm-8">
                                {{ data.jobcmbStudyTo1 }}
                            </div>
                        </div>
                    </div><br>

                    <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label class="text-left">สถาบันการศึกษา</label></div>
                            <div class="col-sm-8">{{ data.jobtxbGraduateInstitute1 }}</div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label>คณะ</label></div>
                            <div class="col-sm-8">{{ data.jobtxbGraduateFaculty1 }}</div>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label class="text-left">กลุ่มสาขาวิชา</label></div>
                            <div class="col-sm-8">{{ data.jobtxbGraduateField1 }}</div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label>ระดับการศึกษา</label></div>
                            <div class="col-sm-8">
                             {{ data.jobcmbGraduateGraduate1 }}
                            </div>
                        </div>
                    </div><br>

                    <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label class="text-left">เกรดเฉลี่ย</label></div>
                            <div class="col-sm-8">{{ data.jobtxbGraduatePoint1 }}</div>
                        </div>
                    </div><br>
                     <div class="row">
                        <div class="col-sm-12 form-row">
                            <div class="col-md-2"> <label>2. ระยะเวลา </label></div>
                            <div class="col-md-1 col-sm-4 mb10-m">
                                <label>ตั้งแต่</label>
                            </div>
                            <div class="col-md-2 col-sm-8 mb10-m">
                                {{ data.jobcmbStudyFrom2 }}
                            </div>
                            
                            <div class="col-md-1 col-sm-4">
                               <label>ถึง</label> 
                            </div>
                            <div class="col-md-3 col-sm-8">
                                {{ data.jobcmbStudyTo2 }}
                            </div>
                        </div>
                    </div><br>

                    <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label class="text-left">สถาบันการศึกษา</label></div>
                            <div class="col-sm-8">{{ data.jobtxbGraduateInstitute2 }}</div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label>คณะ</label></div>
                            <div class="col-sm-8">{{ data.jobtxbGraduateFaculty2 }}</div>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label class="text-left">กลุ่มสาขาวิชา</label></div>
                            <div class="col-sm-8">{{ data.jobtxbGraduateField2 }}</div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label>ระดับการศึกษา</label></div>
                            <div class="col-sm-8">
                             {{ data.jobcmbGraduateGraduate2 }}
                            </div>
                        </div>
                    </div><br>

                    <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label class="text-left">เกรดเฉลี่ย</label></div>
                            <div class="col-sm-8">{{ data.jobtxbGraduatePoint2 }}</div>
                        </div>
                    </div><br>

                     <div class="row">
                        <div class="col-sm-12 form-row">
                            <div class="col-md-2"> <label>3. ระยะเวลา </label></div>
                            <div class="col-md-1 col-sm-4 mb10-m">
                                <label>ตั้งแต่</label>
                            </div>
                            <div class="col-md-2 col-sm-8 mb10-m">
                                {{ data.jobcmbStudyFrom3 }}
                            </div>
                            
                            <div class="col-md-1 col-sm-4">
                               <label>ถึง</label> 
                            </div>
                            <div class="col-md-3 col-sm-8">
                                {{ data.jobcmbStudyTo3 }}
                            </div>
                        </div>
                    </div><br>

                    <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label class="text-left">สถาบันการศึกษา</label></div>
                            <div class="col-sm-8">{{ data.jobtxbGraduateInstitute3 }}</div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label>คณะ</label></div>
                            <div class="col-sm-8">{{ data.jobtxbGraduateFaculty3 }}</div>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label class="text-left">กลุ่มสาขาวิชา</label></div>
                            <div class="col-sm-8">{{ data.jobtxbGraduateField3 }}</div>
                        </div>
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label>ระดับการศึกษา</label></div>
                            <div class="col-sm-8">
                             {{ data.jobcmbGraduateGraduate3 }}
                            </div>
                        </div>
                    </div><br>

                    <div class="row">
                        <div class="col-md-6 form-row">
                            <div class="col-sm-4"><label class="text-left">เกรดเฉลี่ย</label></div>
                            <div class="col-sm-8">{{ data.jobtxbGraduatePoint3 }}</div>
                        </div>
                    </div><br>

                <h5>ข้อมูลการทำงาน</h5><br>
                     <div class="row">
                        <div class="col-md-6 form-row">
                    <div class="col-md-6 col-sm-5"><label>เพิ่งสำเร็จการศึกษา</label></div>
                    <div class="col-md-6 col-sm-7"> {{ data.jobrdoJustGraduate }}</div>
                </div>
                <div class="col-md-6 form-row">
                    <div class="col-md-6 col-sm-6"><label>ประสบการณ์การทำงาน</label></div>
                    <div class="col-md-6 col-sm-6"> {{ data.jobtxbWorkExperience}} &nbsp; ปี</div>
                </div>
            </div><br>
                     <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-sm-4"> <label>สายงานล่าสุด</label></div>
                    <div class="col-sm-8"> {{ data.jobcmbLastWorkField  }}</div>
                </div>
            </div><br>

                    <div class="row">
                <div class="col-md-6 form-row">
                    <div class="col-sm-4"><label>ตำแหน่งสุดท้าย</label></div>
                    <div class="col-sm-8"> {{  data.jobtxbLastPosition }} </div>
                </div>
                <div class="col-md-6 form-row">
                    <div class="col-sm-4"><label>เงินเดือนสุดท้าย</label></div>
                    <div class="col-sm-8"> {{ data.jobtxbLastSalary }} &nbsp; บาท</div>
                </div>
            </div><br>
        <h6>ประวัติการทำงาน</h6><br>

                <div class="row">
                        <div class="col-md-12 form-row">
                        <div class="col-md-2"> <label>1. ระยะเวลา</label></div>
                            <div class="col-md-1 col-sm-2 mb10-m"><label>ตั้งแต่ </label></div>
                            <div class="col-md-2 col-sm-10 mb10-m"> {{  data.jobcmbWorkFrom1 }}</div>
                            <div class="col-md-1 col-sm-2 mb10-m"><label> ถึง </label></div>
                            <div class="col-md-3 col-sm-10 mb10-m"> {{ data.jobcmbWorkTo1 }} </div>
                        </div>
                       </div><br>

                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>ตำแหน่ง</label></div>
                                <div class="col-md-8 col-sm-9">
                                    <div>{{ data.jobtxbWorkPosition1 }}</div>
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-2 col-sm-3"><label>บริษัท</label></div>
                                <div class="col-md-10 col-sm-9">{{  data.jobtxbWorkCompany1 }}</div>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>เงินเดือน</label></div>
                                <div class="col-md-8 col-sm-9">{{ data.jobtxbWorkSalary1 }}  &nbsp; บาท</div>              
                             </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>รายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-9">{{ data.jobtxbWorkIncome1 }} &nbsp; บาท</div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-4"><label>ระบุรายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-8">{{ data.jobtxbWorkIncomeOther1}}</div>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-sm-12 form-row">
                                <div class="col-md-2 col-sm-4"><label>ลักษณะงานที่ทำ</label></div>
                                <div class="col-md-10 col-sm-8">{{ data.jobtxbWorkDetail1 }}</div>
                            </div>
                        </div><br>

                        <div class="row">
                        <div class="col-md-12 form-row">
                        <div class="col-md-2"> <label>2. ระยะเวลา</label></div>
                            <div class="col-md-1 col-sm-2 mb10-m"><label>ตั้งแต่ </label></div>
                            <div class="col-md-2 col-sm-10 mb10-m"> {{ data.jobcmbWorkFrom2 }}</div>
                            <div class="col-md-1 col-sm-2 mb10-m"><label> ถึง </label></div>
                            <div class="col-md-3 col-sm-10 mb10-m"> {{ data.jobcmbWorkTo2 }} </div>
                        </div>
                       </div><br>

                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>ตำแหน่ง</label></div>
                                <div class="col-md-8 col-sm-9">
                                    <div>{{ data.jobtxbWorkPosition2 }}</div>
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-2 col-sm-3"><label>บริษัท</label></div>
                                <div class="col-md-10 col-sm-9">{{ data.jobtxbWorkCompany2 }}</div>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>เงินเดือน</label></div>
                                <div class="col-md-8 col-sm-9">{{ data.jobtxbWorkSalary2 }}  &nbsp; บาท</div>              
                             </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>รายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-9">{{ data.jobtxbWorkIncome2 }} &nbsp; บาท</div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-4"><label>ระบุรายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-8">{{ data.jobtxbWorkIncomeOther2 }}</div>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-sm-12 form-row">
                                <div class="col-md-2 col-sm-4"><label>ลักษณะงานที่ทำ</label></div>
                                <div class="col-md-10 col-sm-8">{{ data.jobtxbWorkDetail2 }}</div>
                            </div>
                        </div><br>


                       <div class="row">
                        <div class="col-md-12 form-row">
                        <div class="col-md-2"> <label>3. ระยะเวลา</label></div>
                            <div class="col-md-1 col-sm-2 mb10-m"><label>ตั้งแต่ </label></div>
                            <div class="col-md-2 col-sm-10 mb10-m"> {{ data.jobcmbWorkFrom3 }}</div>
                            <div class="col-md-1 col-sm-2 mb10-m"><label> ถึง </label></div>
                            <div class="col-md-3 col-sm-10 mb10-m"> {{ data.jobcmbWorkTo3 }} </div>
                        </div>
                       </div><br>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>ตำแหน่ง</label></div>
                                <div class="col-md-8 col-sm-9">
                                    <div>{{ data.jobtxbWorkPosition3 }}</div>
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-2 col-sm-3"><label>บริษัท</label></div>
                                <div class="col-md-10 col-sm-9">{{ data.jobtxbWorkCompany3 }}</div>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>เงินเดือน</label></div>
                                <div class="col-md-8 col-sm-9">{{ data.jobtxbWorkSalary3 }}  &nbsp; บาท</div>              
                             </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>รายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-9">{{ data.jobtxbWorkIncome3 }} &nbsp; บาท</div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-4"><label>ระบุรายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-8">{{ data.jobtxbWorkIncomeOther3 }}</div>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-sm-12 form-row">
                                <div class="col-md-2 col-sm-4"><label>ลักษณะงานที่ทำ</label></div>
                                <div class="col-md-10 col-sm-8">{{ data.jobtxbWorkDetail3 }}</div>
                            </div>
                        </div><br>

                         <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>ตำแหน่ง</label></div>
                                <div class="col-md-8 col-sm-9">
                                    <div>{{ data.jobtxbWorkPosition3 }}</div>
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-2 col-sm-3"><label>บริษัท</label></div>
                                <div class="col-md-10 col-sm-9">{{ data.jobtxbWorkCompany3 }}</div>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>เงินเดือน</label></div>
                                <div class="col-md-8 col-sm-9">{{ data.jobtxbWorkSalary3 }}  &nbsp; บาท</div>              
                             </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>รายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-9">{{ data.jobtxbWorkIncome3 }} &nbsp; บาท</div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-4"><label>ระบุรายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-8">{{ data.jobtxbWorkIncomeOther3 }}</div>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-sm-12 form-row">
                                <div class="col-md-2 col-sm-4"><label>ลักษณะงานที่ทำ</label></div>
                                <div class="col-md-10 col-sm-8">{{ data.jobtxbWorkDetail3 }}</div>
                            </div>
                        </div><br>


                        <div class="row">
                        <div class="col-md-12 form-row">
                        <div class="col-md-2"> <label>4. ระยะเวลา</label></div>
                            <div class="col-md-1 col-sm-2 mb10-m"><label>ตั้งแต่ </label></div>
                            <div class="col-md-2 col-sm-10 mb10-m"> {{ data.jobcmbWorkFrom4 }}</div>
                            <div class="col-md-1 col-sm-2 mb10-m"><label> ถึง </label></div>
                            <div class="col-md-3 col-sm-10 mb10-m"> {{ data.jobcmbWorkTo4 }} </div>
                        </div>
                       </div><br>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>ตำแหน่ง</label></div>
                                <div class="col-md-8 col-sm-9">
                                    <div>{{ data.jobtxbWorkPosition4 }}</div>
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-2 col-sm-3"><label>บริษัท</label></div>
                                <div class="col-md-10 col-sm-9">{{ data.jobtxbWorkCompany4 }}</div>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>เงินเดือน</label></div>
                                <div class="col-md-8 col-sm-9">{{ data.jobtxbWorkSalary4 }}  &nbsp; บาท</div>              
                             </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>รายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-9">{{ data.jobtxbWorkIncome4 }} &nbsp; บาท</div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-4"><label>ระบุรายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-8">{{ data.jobtxbWorkIncomeOther4 }}</div>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-sm-12 form-row">
                                <div class="col-md-2 col-sm-4"><label>ลักษณะงานที่ทำ</label></div>
                                <div class="col-md-10 col-sm-8">{{ data.jobtxbWorkDetail4 }}</div>
                            </div>
                        </div><br>

                         <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>ตำแหน่ง</label></div>
                                <div class="col-md-8 col-sm-9">
                                    <div>{{ data.jobtxbWorkPosition4 }}</div>
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-2 col-sm-3"><label>บริษัท</label></div>
                                <div class="col-md-10 col-sm-9">{{ data.jobtxbWorkCompany4 }}</div>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>เงินเดือน</label></div>
                                <div class="col-md-8 col-sm-9">{{ data.jobtxbWorkSalary4 }}  &nbsp; บาท</div>              
                             </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>รายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-9">{{ data.jobtxbWorkIncome4 }} &nbsp; บาท</div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-4"><label>ระบุรายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-8">{{ data.jobtxbWorkIncomeOther4 }}</div>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-sm-12 form-row">
                                <div class="col-md-2 col-sm-4"><label>ลักษณะงานที่ทำ</label></div>
                                <div class="col-md-10 col-sm-8">{{ data.jobtxbWorkDetail4 }}</div>
                            </div>
                        </div><br>



                        <div class="row">
                        <div class="col-md-12 form-row">
                        <div class="col-md-2"> <label>5. ระยะเวลา</label></div>
                            <div class="col-md-1 col-sm-2 mb10-m"><label>ตั้งแต่ </label></div>
                            <div class="col-md-2 col-sm-10 mb10-m"> {{ data.jobcmbWorkFrom5 }}</div>
                            <div class="col-md-1 col-sm-2 mb10-m"><label> ถึง </label></div>
                            <div class="col-md-3 col-sm-10 mb10-m"> {{ data.jobcmbWorkTo5 }} </div>
                        </div>
                       </div><br>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>ตำแหน่ง</label></div>
                                <div class="col-md-8 col-sm-9">
                                    <div>{{ data.jobtxbWorkPosition5 }}</div>
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-2 col-sm-3"><label>บริษัท</label></div>
                                <div class="col-md-10 col-sm-9">{{ data.jobtxbWorkCompany5 }}</div>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>เงินเดือน</label></div>
                                <div class="col-md-8 col-sm-9">{{ data.jobtxbWorkSalary5 }}  &nbsp; บาท</div>              
                             </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>รายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-9">{{ data.jobtxbWorkIncome5 }} &nbsp; บาท</div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-4"><label>ระบุรายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-8">{{ data.jobtxbWorkIncomeOther5 }}</div>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-sm-12 form-row">
                                <div class="col-md-2 col-sm-4"><label>ลักษณะงานที่ทำ</label></div>
                                <div class="col-md-10 col-sm-8">{{ data.jobtxbWorkDetail5 }}</div>
                            </div>
                        </div><br>

                         <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>ตำแหน่ง</label></div>
                                <div class="col-md-8 col-sm-9">
                                    <div>{{ data.jobtxbWorkPosition5 }}</div>
                                </div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-2 col-sm-3"><label>บริษัท</label></div>
                                <div class="col-md-10 col-sm-9">{{ data.jobtxbWorkCompany5 }}</div>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>เงินเดือน</label></div>
                                <div class="col-md-8 col-sm-9">{{ data.jobtxbWorkSalary5 }}  &nbsp; บาท</div>              
                             </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-3"><label>รายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-9">{{ data.jobtxbWorkIncome5 }} &nbsp; บาท</div>
                            </div>
                            <div class="col-md-6 form-row">
                                <div class="col-md-4 col-sm-4"><label>ระบุรายได้อื่น ๆ</label></div>
                                <div class="col-md-8 col-sm-8">{{ data.jobtxbWorkIncomeOther5 }}</div>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-sm-12 form-row">
                                <div class="col-md-2 col-sm-4"><label>ลักษณะงานที่ทำ</label></div>
                                <div class="col-md-10 col-sm-8">{{ data.jobtxbWorkDetail5 }}</div>
                            </div>
                        </div><br>

                    <h5>ความสามารถด้านภาษา</h5><br>

                <div class="row hide-mobile">
                    <div class="col-sm-12">
                        <div class="col-sm-4 col-sm-offset-1"><label> ภาษา </label></div>
                        <div class="col-sm-2"><label>การพูด</label></div>
                        <div class="col-sm-2"><label>การเขียน</label></div>
                        <div class="col-sm-2"><label>การอ่าน</label></div>
                    </div>
                </div><br>

                <div class="row">
                    <div class="col-sm-12 form-row">
                        <div class="col-md-1 col-sm-3 mb10-m"><span class="show-mobile"><label>ภาษาที่ </label>
                        </span><label>1.</label></div>
                        <div class="col-md-4 col-sm-9 mb10-m"> {{ data.jobtxbLanguage1 }}</div>
                        <div class="show-mobile col-sm-3 mb10-m"><label>การพูด</label></div>
                        <div class="col-md-2 col-sm-9 mb10-m">{{ data.jobcmbLanguageTalk1 }}</div>
                        <div class="show-mobile col-sm-3 mb10-m"><label>การเขียน</label></div>
                        <div class="col-md-2 col-sm-9 mb10-m">{{ data.jobcmbLanguageWrite1 }}</div>      
                        <div class="show-mobile col-sm-3 mb10-m"><label>การอ่าน</label></div>
                        <div class="col-md-2 col-sm-9 mb10-m">{{ data.jobcmbLanguageRead1 }}</div>
                        </div>
                    </div><br>


                 <div class="row">
                    <div class="col-sm-12 form-row">
                        <div class="col-md-1 col-sm-3 mb10-m"><span class="show-mobile"><label>ภาษาที่ </label></span><label>2.
                        </label></div>
                        <div class="col-md-4 col-sm-9 mb10-m">{{ data.jobtxbLanguage2 }}</div>
                        <div class="show-mobile col-sm-3 mb10-m"><label>การพูด</label></div>
                        <div class="col-md-2 col-sm-9 mb10-m">{{ data.jobcmbLanguageTalk2 }}</div>
                        <div class="show-mobile col-sm-3 mb10-m"><label>การเขียน</label></div>
                        <div class="col-md-2 col-sm-9 mb10-m">{{ data.jobcmbLanguageWrite2 }}</div>      
                        <div class="show-mobile col-sm-3 mb10-m"><label>การอ่าน</label></div>
                        <div class="col-md-2 col-sm-9 mb10-m">{{ data.jobcmbLanguageRead2 }}</div>
                        </div>
                    </div><br>

                     <div class="row">
                    <div class="col-sm-12 form-row">
                        <div class="col-md-1 col-sm-3 mb10-m"><span class="show-mobile"><label>ภาษาที่ </label></span><label>3.
                        </label></div>
                        <div class="col-md-4 col-sm-9 mb10-m">{{ data.jobtxbLanguage3 }}</div>
                        <div class="show-mobile col-sm-3 mb10-m"><label>การพูด</label></div>
                        <div class="col-md-2 col-sm-9 mb10-m">{{ data.jobcmbLanguageTalk3 }}</div>
                        <div class="show-mobile col-sm-3 mb10-m"><label>การเขียน</label></div>
                        <div class="col-md-2 col-sm-9 mb10-m">{{ data.jobcmbLanguageWrite3 }}</div>      
                        <div class="show-mobile col-sm-3 mb10-m"><label>การอ่าน</label></div>
                        <div class="col-md-2 col-sm-9 mb10-m">{{ data.jobcmbLanguageRead3 }}</div>
                        </div>
                    </div><br>
                    <div class="row">
                    <div class="col-sm-12 form-row">
                        <div class="col-md-1 col-sm-3 mb10-m"><span class="show-mobile"><label>ภาษาที่ </label></span><label>4.
                        </label></div>
                        <div class="col-md-4 col-sm-9 mb10-m"> {{ data.jobtxbLanguage4 }}</div>
                        <div class="show-mobile col-sm-3 mb10-m"><label>การพูด</label></div>
                        <div class="col-md-2 col-sm-9 mb10-m">{{ data.jobcmbLanguageTalk4 }}</div>
                        <div class="show-mobile col-sm-3 mb10-m"><label>การเขียน</label></div>
                        <div class="col-md-2 col-sm-9 mb10-m">{{ data.jobcmbLanguageWrite4 }}</div>      
                        <div class="show-mobile col-sm-3 mb10-m"><label>การอ่าน</label></div>
                        <div class="col-md-2 col-sm-9 mb10-m">{{ data.jobcmbLanguageRead4 }}</div>
                        </div>
                    </div><br>
    

            <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-4 col-sm-2">พิมพ์ดีด </div>
                        <div class="col-md-4 col-sm-5">
                            ไทย {{ data.jobtxbTypingThai }}
                        </div>
                        <div class="col-md-4 col-sm-5">
                            อังกฤษ {{ data.jobtxbTypingEng }}
                        </div>
                    </div>
                </div><br>

        <h5>ลักษณะงานที่สนใจ</h5><br>

             <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-4 col-sm-3"><label>ตำแหน่งงาน</label></div>
                        <div class="col-md-8 col-sm-9">
                          {{ data.jobtxbInterestPosition }}
                        </div>
                    </div>
                    <div class="col-md-6 form-row">
                        <div class="col-md-5 col-sm-3"><label>ประเภทงาน</label></div>
                        <div class="col-md-7 col-sm-9">
                          {{ data.jobrdoInterestPositionType }}
                        </div>
                    </div>
                </div><br>
            <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-4 col-sm-5"><label>เงินเดือนที่คาดหวัง</label></div>
                        <div class="col-md-8 col-sm-7">
                          {{ data.jobtxbExpectSalary }} &nbsp; บาท 
                        </div>
                    </div>
                    <div class="col-md-6 form-row">
                        <div class="col-sm-5"><label>พร้อมเริ่มงานตั้งแต่วันที่</label></div>
                        <div class="col-sm-7">
                          {{ data.jobdateWork }}
                        </div>
                    </div>
                </div><br>

           <h5>บุคคลอ้างอิง (ไม่ใช่ญาติ หรือคนในครอบครัว)</h5><br>
            <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-4 col-sm-3"><label>ชื่อ-นามสกุล</label></div>
                        <div class="col-md-8 col-sm-9">{{ data.jobtxbReferenceName }}</div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-4 col-sm-3"><label>ตำแหน่ง</label></div>
                        <div class="col-md-8 col-sm-9">{{ data.jobtxbReferencePosition }}</div>
                    </div>
                    <div class="col-md-6 form-row">
                        <div class="col-md-4 col-sm-3"><label>บริษัท</label></div>
                        <div class="col-md-8 col-sm-9">{{ data.jobtxbReferenceCompany }}</div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-md-6 form-row">
                        <div class="col-md-4 col-sm-3"><label>ความสัมพันธ์</label></div>
                        <div class="col-md-8 col-sm-9">{{ data.jobtxbReferenceRelation }}</div>
                    </div>
                    <div class="col-md-6 form-row">
                        <div class="col-md-4 col-sm-4"><label>เบอร์โทรติดต่อ</label></div>
                        <div class="col-md-8 col-sm-8">{{ data.jobtxbReferenceTel }}</div>
                    </div>
               </div><br>

            <h5>ข้อมูลทั่วไป</h5><br>
                <div class="row">
                    <div class="col-sm-12">
                        <span>การไปปฏิบัติงาน ณ โครงการหมู่บ้านท่านขัดข้องหรือไม่</span></span>
                    </div>
                </div><br>

               <div class="row">
                    <div class="col-sm-12 form-row">
                        <div class="col-md-8"><label>เป็นประจำ</label></div>
                        <div class="col-md-4 mrl0">
                            <div class="col-sm-6">
                              {{ data.jobrdoWorkSite + ' ' + data.jobrdoWorkSiteNo }}
                            </div>
                           
                        </div>
                        
                    </div>
                </div><br>

               <div class="row">
                    <div class="col-sm-12 form-row">
                        <div class="col-md-8"><label>เป็นครั้งคราว</label></div>
                        <div class="col-md-4 mrl0">
                            <div class="col-sm-6">
                                {{ data.jobrdoWorkSiteFew + ' ' + data.jobrdoWorkSiteFewNo }}
                            </div>
                            
                        </div>
                        
                    </div>
                </div><br>

                <div class="row">
                    <div class="col-sm-12 form-row">
                        <div class="col-md-8"><label>ท่านมีโรคเเรื้อรัง/โรคประจำตัวหรือไม่</label></div>
                        <div class="col-md-4 mrl0">
                            <div class="col-sm-6">
                               {{ data.jobrdoHasDisease + ' ' + data.jobrdoHasDiseaseYes }}
                            </div>
                            
                        </div>
                        
                    </div>
                </div><br>

                <div class="row">
                    <div class="col-sm-12 form-row">
                        <div class="col-md-8"><label>ท่านเคยต้องโทษทางคดีอาญาหรือคดีแพ่งหรือไม่</label></div>
                        <div class="col-md-4 mrl0">
                            <div class="col-sm-6">
                                {{ data.jobrdoHasAccuse + ' ' + data.jobrdoHasAccuseYes }}
                            </div>
                        </div>
                    </div>
                </div><br>

                <div class="row">
                    <div class="col-sm-12 form-row">
                        <div class="col-md-8"><label>ท่ายเคยถูกศาลสั่งให้เป็นบุคคล้มละลาย หรือถูกศาลสั่งพิทักษ์ทรัพย์ หรือมีหนี้สินล้นพ้นตัวหรือไม่
                        </label></div>
                        <div class="col-md-4 mrl0">
                            <div class="col-sm-6">
                                {{ data.jobrdoHasBankrupt + ' ' + data.jobrdoHasBankruptYes }}
                            </div>     
                        </div>          
                    </div>
                </div><br>

                <div class="row">
                    <div class="col-sm-12 form-row">
                        <div class="col-md-8"><label>ท่ายเคยมีประวัติถูกเลิกจ้างเพราะเหตุกระทำผิดมาก่อนหรือไม่</label></div>
                        <div class="col-md-4 mrl0">
                            <div class="col-sm-6">
                               {{ data.jobrdoHasLayoff + ' ' + data.jobrdoHasLayoffYes  }}
                            </div>
                        </div>  
                    </div>
                </div><br>
            
            <h5>ข้อมูลเพิ่มเติม</h5><br>
                <div>
                    {{ data.jobtxbFurtherInfo }}
                </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  <!-- /.popup -->




<script>

     $( document ).ready(function() { 

        //step form
        var current_fs, next_fs, previous_fs; 
        
        $(".next").click(function(){
            
            current_fs = $(this).parents('fieldset');
            next_fs = $(this).parents('fieldset').next();
            
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
            
            next_fs.slideDown();
            current_fs.hide();
              
        });

        $(".previous").click(function(){
          
            current_fs = $(this).parents('fieldset');
            previous_fs = $(this).parents('fieldset').prev();
            
            $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

            previous_fs.slideDown(); 
            current_fs.hide();
        });

        //browse file

       $(document).on('click', '.browse', function(){
            var file = $(this).parent().parent().parent().find('.file');
            file.trigger('click');
        });
        $(document).on('change', '.file', function(){
            $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
        });

    //Mobile tab
      $('.btn').bind("click", function () {

            $(this).find('.iconmobiledown').hide();
            $(this).find('.iconmobileup').show();
            $(this).css('background-color','#6f9755');
            $(this).css('color','white');
            $(this).css('font-size','25px');
 
         if ($(this).attr('aria-expanded') == "true") {

            $(this).css('background-color','white');
            $(this).css('color','#6f9755');
            $(this).css('font-size','25px');
            $(this).find('.iconmobileup').hide();
            $(this).find('.iconmobiledown').show();

         
        }
     })


        $('#txbWorkSite').val("");
        $('#txbWorkSiteFew').val("");
        $('#txbHasDisease').val("");
        $('#txbHasAccuse').val("");
        $('#txbHasBankrupt').val("");
        $('#txbHasLayoff').val("");
        $('#txbConscription').val("");



    
    $("#chWorkTo1").click(function() {
       
        
        if($('#chWorkTo1').is(':checked')){
            
            $("input[name='cmbWorkTo1']").attr('disabled', true);
            $("#cmbWorkTo1").datepicker( "setDate" , new Date());
            
    
        }else{
        
            $("input[name='cmbWorkTo1']").attr('disabled', false);
        
        }
        
    
    });


    $("#chWorkTo2").click(function() {
       
        
        if($('#chWorkTo2').is(':checked')){
            
            $("input[name='cmbWorkTo2']").attr('disabled', true);
            $("#cmbWorkTo2").datepicker( "setDate" , new Date());
            
    
        }else{
        
            $("input[name='cmbWorkTo2']").attr('disabled', false);
        
        }
        
    
    });

    $("#chWorkTo3").click(function() {
       
        
        if($('#chWorkTo3').is(':checked')){
            
            $("input[name='cmbWorkTo3']").attr('disabled', true);
            $("#cmbWorkTo3").datepicker( "setDate" , new Date());
            
    
        }else{
        
            $("input[name='cmbWorkTo3']").attr('disabled', false);
        
        }
        
    
    });

      $("#chWorkTo4").click(function() {
       
        
        if($('#chWorkTo4').is(':checked')){
            
            $("input[name='cmbWorkTo4']").attr('disabled', true);
            $("#cmbWorkTo4").datepicker( "setDate" , new Date());
            
    
        }else{
        
            $("input[name='cmbWorkTo4']").attr('disabled', false);
        
        }
        
    
    });

     $("#chWorkTo5").click(function() {
       
        
        if($('#chWorkTo5').is(':checked')){
            
            $("input[name='cmbWorkTo5']").attr('disabled', true);
            $("#cmbWorkTo5").datepicker( "setDate" , new Date());
            
    
        }else{
        
            $("input[name='cmbWorkTo5']").attr('disabled', false);
        
        }
        
    
    });

  
        
//date

        $('#cmbStudyFrom1 ,#cmbStudyTo1, #cmbStudyFrom2, #cmbStudyTo2, #cmbStudyFrom3, #cmbStudyTo3')
        .datepicker();
        $('#cmbWorkFrom1 , #cmbWorkTo1 , #cmbWorkFrom2 , #cmbWorkTo2 , #cmbWorkFrom3 , #cmbWorkTo3 ,#cmbWorkFrom4 , #cmbWorkTo4 , #cmbWorkFrom5 , #cmbWorkTo5').datepicker();

 //เกณฑ์ทหารอื่นๆ

        $('#cmbConscription').change(function(){ 
    
            var value = $('#cmbConscription').val();
            
                if(value != 'ยกเว้น'){

                    $('#txbConscription').val("");
                }

        });

 // ศาสนาอื่นๆ

        $('#cmbReligion').change(function(){  
    
            var value = $('#cmbReligion').val();
            
                if(value != 'อื่นๆ'){

                    $('#txbReligionOther').val("");
                }
        });


// สัญชาติอื่นๆ

        $('input[name=rdoRace]').change(function(){         
            
            if ($("input[name=rdoRace").val() != 'อื่นๆ'){           
                            
                $('#txbRaceOther').val("");
            }
        });

//เชื้อชาติอื่นๆ 
        
        $('input[name=rdoNationality]').change(function(){         
            
            if ($("input[name=rdoNationality").val() != 'อื่นๆ'){            
                            
                $('#txbNationalityOther').val("");
            }
        });


/// ตอบคำถามอื่นๆ

        $('input[name=rdoWorkSite]').change(function(){         
            
            if ($("input[name=rdoWorkSite]").val() != ""){           
                            
                $('#txbWorkSite').val("");
            }
        });


        $('input[name=rdoWorkSiteFew]').change(function(){         
            
            if ($("input[name=rdoWorkSiteFew]").val() != ""){            
                            
                $('#txbWorkSiteFew').val("");
            }
        });

        $('input[name=rdoWorkSiteFew]').change(function(){         
            
            if ($("input[name=rdoWorkSiteFew]").val() != ""){            
                            
                $('#txbWorkSiteFew').val("");
            }
        });

        $('input[name=rdoHasDisease]').change(function(){         
            
            if ($("input[name=rdoHasDisease]").val() != ""){             
                            
                $('#txbHasDisease').val("");
            }
        });

        $('input[name=rdoHasAccuse]').change(function(){         
            
            if ($("input[name=rdoHasAccuse]").val() != ""){              
                            
                $('#txbHasAccuse').val("");
            }
        });

        $('input[name=rdoHasBankrupt]').change(function(){         
            
            if ($("input[name=rdoHasBankrupt]").val() != ""){            
                            
                $('#txbHasBankrupt').val("");
            }
    });

        $('input[name=rdoHasLayoff]').change(function(){         
            
            if ($("input[name=rdoHasLayoff]").val() != ""){              
                            
                $('#txbHasLayoff').val("");
            }
    });

   

    $('form').on('keyup change', 'input, select, textarea', function(){
        
              if($('#errresume').is(":visible")){
                
                    $('#btnSend').prop('disabled',true);
                    
              }if($('#errpic').is(":visible")){

                    $('#btnSend').prop('disabled',true);
              
              }if($('#errsize').is(":visible")){
                    
                    $('#btnSend').prop('disabled',true);

              }
    
    });


});

// date mm/yyyy

$('#cmbStudyFrom1 ,#cmbStudyTo1, #cmbStudyFrom2, #cmbStudyTo2, #cmbStudyFrom3, #cmbStudyTo3 ,#cmbWorkFrom1 , #cmbWorkTo1 , #cmbWorkFrom2 , #cmbWorkTo2 , #cmbWorkFrom3 , #cmbWorkTo3 ,#cmbWorkFrom4 , #cmbWorkTo4 , #cmbWorkFrom5 , #cmbWorkTo5 ').datepicker({
   
        format: "mm/yyyy",
        startView: "months", 
        minViewMode: "months"

});

//tab web
 var b = $('#cmbTitle').val();

   
    $('#headTabStudy1 a').click(function (e) {
            
            e.preventDefault()
             $('#headTabStudy1').addClass( "current" )
             $('#headTabStudy2').removeClass( "current" )
             $('#headTabStudy3').removeClass( "current" )
             $('#tabStudy1').show();
             $('#tabStudy2').hide();
             $('#tabStudy3').hide();
    })
    $('#headTabStudy2 a').click(function (e) {
            e.preventDefault()
             $('#headTabStudy2').addClass( "current" )
             $('#headTabStudy1').removeClass( "current" )
             $('#headTabStudy3').removeClass( "current" )
             $('#tabStudy2').show();
             $('#tabStudy1').hide();
             $('#tabStudy3').hide();
    })
    $('#headTabStudy3 a').click(function (e) {
            e.preventDefault()
             $('#headTabStudy3').addClass( "current" )
             $('#headTabStudy1').removeClass( "current" )
             $('#headTabStudy2').removeClass( "current" )
             $('#tabStudy3').show();
             $('#tabStudy1').hide();
             $('#tabStudy2').hide();
    })

     $('#headTab1 a').click(function (e) {
            
            e.preventDefault()
             $('#headTab1').addClass( "current" )
             $('#headTab2').removeClass( "current" )
             $('#headTab3').removeClass( "current" )
             $('#headTab4').removeClass( "current" )
             $('#headTab5').removeClass( "current" )
             $('#tab1').show();
             $('#tab2').hide();
             $('#tab3').hide();
             $('#tab4').hide();
             $('#tab5').hide();
    })
    $('#headTab2 a').click(function (e) {
            e.preventDefault()
             $('#headTab2').addClass( "current" )
             $('#headTab1').removeClass( "current" )
             $('#headTab3').removeClass( "current" )
             $('#headTab4').removeClass( "current" )
             $('#headTab5').removeClass( "current" )
             $('#tab2').show();
             $('#tab1').hide();
             $('#tab3').hide();
             $('#tab4').hide();
             $('#tab5').hide();
    })
    $('#headTab3 a').click(function (e) {
            e.preventDefault()
             $('#headTab3').addClass( "current" )
             $('#headTab1').removeClass( "current" )
             $('#headTab2').removeClass( "current" )
             $('#headTab4').removeClass( "current" )
             $('#headTab5').removeClass( "current" )
             $('#tab3').show();
             $('#tab1').hide();
             $('#tab2').hide();
             $('#tab4').hide();
             $('#tab5').hide();
    })
     $('#headTab4 a').click(function (e) {
            e.preventDefault()
             $('#headTab4').addClass( "current" )
             $('#headTab1').removeClass( "current" )
             $('#headTab2').removeClass( "current" )
             $('#headTab3').removeClass( "current" )
             $('#headTab5').removeClass( "current" )
             $('#tab4').show();
             $('#tab1').hide();
             $('#tab2').hide();
             $('#tab3').hide();
             $('#tab5').hide();
    })
      $('#headTab5 a').click(function (e) {
            e.preventDefault()
             $('#headTab5').addClass( "current" )
             $('#headTab1').removeClass( "current" )
             $('#headTab2').removeClass( "current" )
             $('#headTab3').removeClass( "current" )
             $('#headTab4').removeClass( "current" )
             $('#tab5').show();
             $('#tab1').hide();
             $('#tab2').hide();
             $('#tab3').hide();
             $('#tab4').hide();
    })

</script>


<?php include('footer.php'); ?>
