<?php
header('Location: //'.$_SERVER['SERVER_NAME'].'/'.'en');
exit();

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

$review_id = $_GET ['review_id'];

if (empty ( $review_id ) || getProjectReviewDetail ( $review_id ) == false) {
	header ( 'Location: ' . $router->generate ( 'review' ) );
}

$review = getProjectReviewDetail ( $review_id );

$other_reviews = getProjectReview ( $review_id );

$title_page = $review->project_review_name_th;
$description_page = $review->project_review_dis_th;

$image_page = str_replace('fileupload/images/project_review/','https://www.lh.co.th/www_en/Backend/fileupload/images/project_review/Thumbnails_', $review->project_review_img);

$pages = 'LH_PROJECT_REVIEW';
$pages_id = $review_id;

?>
<?php include('header.php'); ?>
<!-- CSS -->
<link rel="stylesheet" href="<?= file_path('css/review-details.css') ?>"
	type="text/css">
<!-- JS -->
<script src="<?= file_path('js/review-details.js') ?>"></script>


<div class="page-nav">
	<div class="container">
		
		<?php 
		$SEO = getSEOUrl($actual_link);
		if($actual_link == @$SEO->url_page){
			echo "<h1>".$SEO->h1."</h1>";
		}else{
			if(empty($review->h1)){
			  echo "<h1>LH PROJECT REVIEW</h1>";
			}else{
				echo "<h1>".$review->h1."</h1>";
			}  
		}
		?>
		<br>
		<p class="title dsktp" style="margin: 0;"><?= !empty($review) ? $review->project_review_name_th : ''  ?></p>
	</div>
</div>

<div id="content" class="content">
	<div class="container">

		<div class="review-content">

			<div class="page-nav rps">
				<p class="title"><?= !empty($review) ? $review->project_review_name_th : ''  ?></p>
			</div>

			 <div class="content-block">
                          <?php
                       $data='';
                       if(!empty($review))
                           $data= str_replace('../build/images/file_content_web/', '/www_en/build/images/file_content_web/',  $review->project_review_dis_content);


                     ?>
                     <?= $data ?>

                </div>

		</div>

		<div class="">
			<p class="heading-title">Other interesting stories.</p>
			<div class="row">
				<div id="relatePost" class="col-slide">
                        <?php
						foreach ( $other_reviews as $other_review ){
							if(isset($other_review->custom_url)){
								$url_name = $other_review->custom_url; 
							}else{
								$url_name =$other_review->project_review_name_th;
							}
							$url_see_more = $router->generate ( 'review-detail', [ 
									'review_name' => str_replace ( ' ', '-', $url_name ),
									'review_id' => $other_review->project_review_id 
							] );
							
							?>
                            <div class="item">
						<div class="relate-post">
							<div class="review-img">
								<a
									href="<?=$router->generate ( 'review-detail', [ 'review_name' => str_replace ( ' ', '-', $other_review->project_review_name_th ),'review_id' => $other_review->project_review_id ] );?>"> <img
									src="<?=str_replace('/images/project_review/','/images/project_review/Thumbnails_',backend_url('base',$other_review->project_review_img)) ?>"
									alt="">
								</a> <a href="<?= $url_see_more ?>">
									<p class="title"><?= $other_review->project_review_name_th ?></p>
									<p><?= $other_review->project_review_dis_th ?></p>
								</a> <a href="<?= $url_see_more ?>" class="btn btn-seemore">See more</a>
							</div>

						</div>
					</div>
                        <?php } ?>
                    </div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
        $( document ).ready(function() {
            $('video').attr("poster","https://www.lh.co.th/www/Frontend//images/logo/vdo_backgroup.png");
            // $('video').attr("muted","muted");
            // $('video').attr("playsinline","playsinline");
        });
    </script>

<?php include('footer.php'); ?>
