<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>

<?php include('header.php'); ?>
<!-- CSS -->
<link rel="stylesheet" href="css/corporate-all.css" type="text/css">
<link rel="stylesheet" href="css/corporate-menu.css" type="text/css">
<link rel="stylesheet" href="css/governance.css" type="text/css">
<link rel="stylesheet" href="css/investor.css" type="text/css">

<!-- JS -->
<script src="js/corporate-menu.js"></script>
<script src="js/corporate-menu.js"></script>

<div id="content" class="content corp-page share-info-page">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                <h1>รายนามนักวิเคราะห์</h1>
                <p>รายนามนักวิเคราะห์ด้านการลงทุนของ แลนด์ แอนด์ เฮ้าส์ ปรับปรุง : 26 พฤษภาคม 2558</p>
                <div>
                    <table class="table-historical-price table-analyst" cellpadding="0" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th style=" vertical-align: middle;">บริษัทหลักทรัพย์ </th>
                            <th style="text-align: left; vertical-align: middle;">รายนามนักวิเคราะห์</th>
                            <th style=" vertical-align: middle;">อีเมล์</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="tb-left">บริษัทหลักทรัพย์ บีเอ็นพี พารีบาส์ พีรีกริน (ประเทศไทย) จำกัด</td>
                            <td>คุณชุติมา วรมนตรี</td>
                            <td><a href="mailto:chutima.wor@fssia.com">chutima.wor@fssia.com</a></td>
                        </tr>
                        <tr>
                            <td class="tb-left">บริษัทหลักทรัพย์ บัวหลวง จำกัด</td>
                            <td>คุณนฤมล เอกสมุทร</td>
                            <td><a href="mailto:narumon.e@bualuang.co.th">narumon.e@bualuang.co.th</a></td>
                        </tr>
                        <tr>
                            <td class="tb-left">บริษัทหลักทรัพย์ ซี แอล เอส เอ (ประเทศไทย) จำกัด</td>
                            <td>คุณสรภพ ปั้นเปี่ยมรัษฎ์</td>
                            <td><a href="mailto:soraphob.panpiemras@clsa.com">soraphob.panpiemras@clsa.com</a></td>
                        </tr>
                        <tr>
                            <td class="tb-left">บริษัทหลักทรัพย์ เครดิตสวิส (ประเทศไทย) จำกัด</td>
                            <td>คุณอดุลย์ เศรษฐี</td>
                            <td><a href="mailto:atul.sethi.2@credit-suisse.com">atul.sethi.2@credit-suisse.com</a></td>
                        </tr>
                        <tr>
                            <td class="tb-left">บริษัทหลักทรัพย์ ดีบีเอส วิคเกอร์ (ประเทศไทย) จำกัด</td>
                            <td>คุณจันทร์เพ็ญ ศิริธนารัตนกุล</td>
                            <td><a href="mailto:chanpens@the.dbsvickers.com">chanpens@the.dbsvickers.com</a></td>
                        </tr>
                        <tr>
                            <td class="tb-left">บริษัทหลักทรัพย์ ดอยซ์ ทิสโก้ จำกัด</td>
                            <td>คุณณัฐวุฒิ ศิวะรุจิวงศ์</td>
                            <td><a href="mailto:nathavut@dbtisco.com">nathavut@dbtisco.com</a></td>
                        </tr>
                        <tr>
                            <td class="tb-left">บริษัทหลักทรัพย์ กสิกรไทย จำกัด (มหาชน)</td>
                            <td>คุณสรพงษ์ จักรธีรังกูร</td>
                            <td><a href="mailto:sorapong.j@kasikornsecurities.com">sorapong.j@kasikornsecurities.com</a></td>
                        </tr>
                        <tr>
                            <td class="tb-left">บริษัทหลักทรัพย์ Macquarie (ประเทศไทย) จำกัด</td>
                            <td>คุณแพ็ตตี้ ตอไมตรีจิตร</td>
                            <td><a href="mailto:patti.tomaitrichir@macquarie.com">patti.tomaitrichir@macquarie.com</a></td>
                        </tr>
                        <tr>
                            <td class="tb-left">บริษัทหลักทรัพย์ ภัทร จำกัด</td>
                            <td>คุณจิราภรณ์ ลินมณีโชติ</td>
                            <td><a href="mailto:jiraporn@phatrasecurities.com">jiraporn@phatrasecurities.com</a></td>
                        </tr>
                        <tr>
                            <td class="tb-left">บริษัทหลักทรัพย์ ไทยพาณิชย์ จำกัด</td>
                            <td>คุณสิริการย์ กฤษฏิ์นิพัทธ์</td>
                            <td><a href="mailto:sirikarn.krisnipat@scb.co.th">sirikarn.krisnipat@scb.co.th</a></td>
                        </tr>
                        <tr>
                            <td class="tb-left">บริษัทหลักทรัพย์ ธนชาติ จำกัด</td>
                            <td>คุณพรรณรายณ์ ติยะพิทยารัตน์</td>
                            <td><a href="mailto:phannarai.von@thanachartsec.co.th">phannarai.von@thanachartsec.co.th</a></td>
                        </tr>
                        <tr>
                            <td class="tb-left">บริษัทหลักทรัพย์ เคที ซีมีโก้ จำกัด</td>
                            <td>คุณวรรัตน์ เผ่าภคะ</td>
                            <td><a href="mailto:woraratp@ktzmico.com">woraratp@ktzmico.com</a></td>
                        </tr>
                        </tbody></table>
                </div>

            </div>
        </div>
    </div>


    <?php include('footer.php'); ?>
