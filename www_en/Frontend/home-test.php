<?php include('header.php'); ?>
<!-- CSS -->
<link rel="stylesheet" href="css/home2.css" type="text/css">
<!-- JS -->
<script src="js/home-test.js"></script>

<div class="page-banner page-scroll">
    <span id="scrollNext" class="i-scroll-next"></span>

    <div id="homeBanner" class="owl-carousel owl-theme">
        <div class="item">
            <div class="banner-parallax banner-dktp">
                <img src="images/home/banner_home1.jpg" alt="">
                <div class="bg-slide-home">
                    <div class="banner-txt">
                        <h1>Enjoy every moment</h1>
                        <h3>Work | Health I Inspiration | Idea | Relax</h3>
                    </div>
                </div>
            </div>
            <div class="banner-parallax banner-cover-rsp" style="background-image: url(images/home/banner_m_1.jpg);">
                <div class="bg-slide-home">
                    <div class="banner-txt">
                        <h1>Enjoy every moment</h1>
                        <h3>Work | Health I Inspiration | Idea | Relax</h3>
                    </div>
                </div>
            </div>
            <a href="http://www.lh.co.th" class="banner-activity" style="background-color: rgb(255, 255, 255); opacity: 0.7; top: initial; bottom: 20px;">
                <p class="title-acty">ขยับรับสุขก่อน รับ Gift Voucher <br>จาก Land&amp;House มูลค่า 100,000 บาท</p>
                <p>คอนโด The Room Sukhumvit 69<br>
                    พื้นที่ใช้สอย 82.2 ตร.ม. 2 ห้องนอน 2 ห้องน้ำ<br>
                    15 ล้านบาท*
                </p>                                            <span class="link-acty"></span>
                <span class="close-acty"></span>
            </a>
        </div>

        <div class="item">
            <div class="banner-parallax banner-dktp">
                <img src="images/home/banner_home2.jpg" alt="">
                <div class="bg-slide-home">
                    <div class="banner-txt">
                        <h1>Enjoy every moment</h1>
                        <h3>Work | Health I Inspiration | Idea | Relax</h3>
                    </div>
                </div>
            </div>
            <div class="banner-parallax banner-cover-rsp" style="background-image: url(images/home/banner_m_2.jpg);">
                <div class="bg-slide-home">
                    <div class="banner-txt">
                        <h1>Enjoy every moment</h1>
                        <h3>Work | Health I Inspiration | Idea | Relax</h3>
                    </div>
                </div>
            </div>
        </div>

        <div class="item">
            <div class="banner-parallax banner-dktp">
                <img src="images/home/banner_home3.jpg" alt="">
                <div class="bg-slide-home">
                    <div class="banner-txt">
                        <h1>Enjoy every moment</h1>
                        <h3>Work | Health I Inspiration | Idea | Relax</h3>
                    </div>
                </div>
            </div>
            <div class="banner-parallax banner-cover-rsp" style="background-image: url(images/home/banner_m_3.jpg);">
                <div class="bg-slide-home">
                    <div class="banner-txt">
                        <h1>Enjoy every moment</h1>
                        <h3>Work | Health I Inspiration | Idea | Relax</h3>
                    </div>
                </div>
            </div>
        </div>

        <div class="item">
            <div class="banner-parallax banner-dktp">
                <img src="images/home/banner_home6.jpg" alt="">
                <div class="bg-slide-home">
                    <div class="banner-txt">
                        <h1>Enjoy every moment</h1>
                        <h3>Work | Health I Inspiration | Idea | Relax</h3>
                    </div>
                </div>
            </div>
            <div class="banner-parallax banner-cover-rsp" style="background-image: url(images/home/banner_m_6.jpg);">
                <div class="bg-slide-home">
                    <div class="banner-txt">
                        <h1>Enjoy every moment</h1>
                        <h3>Work | Health I Inspiration | Idea | Relax</h3>
                    </div>
                </div>
            </div>
        </div>

        <div class="item">
            <div class="banner-parallax banner-dktp">
                <img src="images/home/banner_home7.jpg" alt="">
                <div class="bg-slide-home">
                    <div class="banner-txt">
                        <h1>Enjoy every moment</h1>
                        <h3>Work | Health I Inspiration | Idea | Relax</h3>
                    </div>
                </div>
            </div>
            <div class="banner-parallax banner-cover-rsp" style="background-image: url(images/home/banner_m_7.jpg);">
                <div class="bg-slide-home">
                    <div class="banner-txt">
                        <h1>Enjoy every moment</h1>
                        <h3>Work | Health I Inspiration | Idea | Relax</h3>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!--<a href="http://www.lh.co.th" class="banner-activity banner-rsp" style="background-color: rgb(255, 255, 255); opacity: 0.7; top: initial; bottom: 20px;">-->
<!--    <p class="title-acty">ขยับรับสุขก่อน รับ Gift Voucher <br>จาก Land&amp;House มูลค่า 100,000 บาท</p>-->
<!--    <p>คอนโด The Room Sukhumvit 69<br>-->
<!--        พื้นที่ใช้สอย 82.2 ตร.ม. 2 ห้องนอน 2 ห้องน้ำ<br>-->
<!--        15 ล้านบาท*-->
<!--    </p>                                            <span class="link-acty"></span>-->
<!--    <span class="close-acty"></span>-->
<!--</a>-->


<div id="content" class="content">
    <div class="container">
        <div id="homeHighlight" class="home-section">
            <p class="heading-title">LH Highlights</p>
            <div class="row">
                <div id="homeHighlightSlide" class="slide-highlight">
                    <div class="item">
                        <div class="">
                            <div class="col-md-8">
                                <a href="#" data-featherlight="#lbVdo1">
                                    <span class="i-view-vdo"></span>
                                    <img src="images/home/hm_hl_1.jpg" alt="">
                                </a>
                                <iframe class="lightbox" src="https://www.youtube.com/embed/GJkPSkZ80bo" width="1000"
                                        height="560" id="lbVdo1" style="border:none;" webkitallowfullscreen
                                        mozallowfullscreen allowfullscreen></iframe>

                            </div>
                            <div class="col-md-4">
                                <div class="highlight-descrp">
                                    <p class="title">ภาพยนตร์โฆษณาชุด: ก้าวให้ไกลตั้งแต่ก้าวแรก...</p>
                                    <p>มีคำคมเป็นล้านๆคา ที่คอยบอกคุณในทุกๆเช้า ว่าชีวิตสาคัญที่สุด...ที่ก้าวแรกแต่คาถามคือ ก้าวแรกที่ว่า...จะเริ่มเมื่อไหร่? เริ่มกับใคร?ก้าวให้ไกล ตั้งแต่ก้าวแรก กับ "บ้านหลังแรก"</p>
                                    <a href="" class="btn btn-seemore" style="text-align:
                                    center">See more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="">
                            <div class="col-md-8">
                                <img src="images/home/hm_hl_2.jpg" alt="">
                            </div>
                            <div class="col-md-4">
                                <div class="highlight-descrp">
                                    <p class="title">เปิดตัว 3 โครงการใหม่</p>
                                    <p>มารู้จัก Air Plus หนึ่งใน LH Smart แนวคิดเพื่อชีวิตที่ดีกว่าจาก Land & Houses</p>
                                    <a href="" class="btn btn-seemore">See more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="">
                            <div class="col-md-8">
                                <img src="images/home/hm_hl_3.jpg" alt="">
                            </div>
                            <div class="col-md-4">
                                <div class="highlight-descrp">
                                    <p class="title">THE BEST OF THONGLOR</p>
                                    <p>เพื่อให้บ้านได้ตอบสนองความสะดวกสบาย และสไตล์ของการใช้ชีวิต "บ้านพร้อมตกแต่ง" ทุกหลังออกแบบโดยมัณฑนากรผู้ชานาญการดีไซน์เฉพาะหลังสวยไม่ซ้าใคร</p>
                                    <a href="" class="btn btn-seemore">See more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="homeReview" class="home-section">
            <p class="heading-title">LH Review</p>
            <div class="row">
                <div class="review-lists">
                    <div class="list-item item">
                        <div class="review-img">
                            <a href="">
                                <img src="images/home/hm_review1.jpg" alt="">
                            </a>
                        </div>
                        <a href="">
                            <p class="title">The Landmark Ekamai - Ramindra</p>
                            <p>พาชม Luxury Townhome บนทำเลสะดวกที่ทวีค่าไม่สิ้นสุด เอกมัย-รามอินทรา</p>
                        </a>
                    </div>
                    <div class="list-item item">
                        <div class="review-img">
                            <a href="">
                                <img src="images/home/hm_review2.jpg" alt="">
                            </a>
                        </div>
                        <a href="">
                            <p class="title">Villaggio บางนา</p>
                            <p>พาชมต้นแบบโครงการสวย สไตล์ยุโรป  จากแลนด์ แอนด์ เฮ้าส์สถานที่ที่ทำให้การอยู่บ้าน กับการไปท่องเที่ยวกลายเป็นเรื่องเดียวกัน</p>
                        </a>
                    </div>
                    <div class="list-item item">
                        <div class="review-img">
                            <a href="">
                                <img src="images/home/hm_review3.jpg" alt="">
                            </a>
                        </div>
                        <a href="">
                            <p class="title">The Room Charoenkrung 30</p>
                            <p>พาชมคอนโดหรู บนทำเลผืนสุดท้าย “เจริญกรุง”</p>
                        </a>
                    </div>
                    <div class="list-item item">
                        <div class="review-img">
                            <a href="">
                                <img src="images/home/hm_review4.jpg" alt="">
                            </a>
                        </div>
                        <a href="">
                            <p class="title">Urban Cottage Style</p>
                            <p>เปลี่ยนบ้านเดี่ยวที่คุณเคยรู้จัก...นิยามของบ้านดีไซน์ใหม่ ที่เราจะพาไปชมแบบ 360 องศากัน</p>
                        </a>
                    </div>
                    <div class="list-item item">
                        <div class="review-img">
                            <a href="">
                                <img src="images/home/hm_review5.jpg" alt="">
                            </a>
                        </div>
                        <a href="">
                            <p class="title">Villaggio ปิ่นเกล้า – ศาลายา</p>
                            <p>พาชมหนึ่งเดียวบนทำเลปิ่นเกล้ากับดีไซน์ใหม่ล่าสุดที่จะทำให้ก้าวแรกของคุณไกลกว่าที่เคย</p>
                        </a>
                    </div>
                    <div class="list-item item">
                        <div class="review-img">
                            <a href="">
                                <img src="images/home/hm_review6.jpg" alt="">
                            </a>
                        </div>
                        <a href="">
                            <p class="title">Villaggio รังสิต – คลอง 3</p>
                            <p>“สวยที่สุด” คงไม่เกินจริงกับดีไซน์ทาวน์โฮมที่แตกต่าง และสมบูรณ์แบบหนึ่งเดียวบนทำเลรังสิต</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div id="homeCommunity" class="home-section">
            <p class="heading-title">LH Community</p>
            <div class="row">
                <div class="hm-commu-lists">
                    <div id="homeCommunitySlide">
                        <div class="item">
                            <div class="hm-commu-list">
                                <div class="col-md-6 pull-right">
                                    <div class="hm-commu-list-c2">
                                        <img src="images/home/hm_commu1.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-md-6 pull-left">
                                    <a href="">
                                        <div class="hm-commu-list-c1">
                                            <p class="title">REASON TO FALL IN LOVE</p>
                                            <p>แม้จะเป็นนักวิเคราะห์ระบบมือหนึ่งของบริษัท
                                                แต่เมื่อต้องซื้อบ้านที่ตัวเองรัก <span class="bold">คุณกฤติกา สุกิจปาณีนิจ</span>
                                                กลับเลือกที่จะให้ความรู้สึกภายใน
                                                เป็นตัวตัดสินไม่ได้ใช้เพียงข้อมูลอย่างเดียว</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="hm-commu-list">
                                <div class="col-md-6">
                                    <div class="hm-commu-list-c3">
                                        <img src="images/home/hm_commu2.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <a href="">
                                        <div class="hm-commu-list-c4">
                                            <p class="title">REASON TO FALL IN LOVE</p>
                                            <p>ไม่ใช่เรื่องง่ายที่จะตกหลุมรักใครสักคนโดยไม่เคยเห็นหน้า
                                                แต่สำหรับ<span class="bold">คุณคติ โฆษานันตชัย</span> โครงการ 333 Riverside
                                                กลับทำให้เขาตกหลุมรักได้ แม้ไม่เคยเห็นตัวจริงของโครงการ
                                                ทั้งยังเชื่อว่าที่นี่จะเป็นบ้านหลังใหม่สำหรับเขาและคุณแม่</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="homeTips" class="home-section">
            <p class="heading-title">LH Living Tips</p>
            <div class="row">
                <div id="homeTipsSlide" class="col-slide">
                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="">
                                    <img src="images/home/hm_img_10.jpg" alt="">
                                </a>
                            </div>
                            <a href="">
                                <p class="title">3IDEA แต่งห้องนอนให้สบายในฤดูฝน</p>
                                <p>ช่วงนี้ใครรู้สึกรักห้องนอนมากเป็นพิเศษบ้างคะ ถ้าคุณเป็นหนึ่ง
                                    ในนั้นล่ะก็ เรามาปรับลุคห้องนอนกันสักเล็กน้อยดีกว่าตาม 3
                                    ไอเดียนี้เลยนะคะ</p>
                            </a>
                            <a href="" class="btn btn-seemore">See more</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="">
                                    <img src="images/home/hm_img_11.jpg" alt="">
                                </a>
                            </div>
                            <a href="">
                                <p class="title">3IDEA แต่งห้องนอนให้สบายในฤดูฝน</p>
                                <p>ช่วงนี้ใครรู้สึกรักห้องนอนมากเป็นพิเศษบ้างคะ ถ้าคุณเป็นหนึ่ง
                                    ในนั้นล่ะก็ เรามาปรับลุคห้องนอนกันสักเล็กน้อยดีกว่าตาม 3
                                    ไอเดียนี้เลยนะคะ</p>
                            </a>
                            <a href="" class="btn btn-seemore">See more</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="">
                                    <img src="images/home/hm_img_12.jpg" alt="">
                                </a>
                            </div>
                            <a href="">
                                <p class="title">3IDEA แต่งห้องนอนให้สบายในฤดูฝน</p>
                                <p>ช่วงนี้ใครรู้สึกรักห้องนอนมากเป็นพิเศษบ้างคะ ถ้าคุณเป็นหนึ่ง
                                    ในนั้นล่ะก็ เรามาปรับลุคห้องนอนกันสักเล็กน้อยดีกว่าตาม 3
                                    ไอเดียนี้เลยนะคะ</p>
                            </a>
                            <a href="" class="btn btn-seemore">See more</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="hm-tip">
                            <div class="review-img">
                                <a href="">
                                    <img src="images/home/hm_img_11.jpg" alt="">
                                </a>
                            </div>
                            <a href="">
                                <p class="title">3IDEA แต่งห้องนอนให้สบายในฤดูฝน</p>
                                <p>ช่วงนี้ใครรู้สึกรักห้องนอนมากเป็นพิเศษบ้างคะ ถ้าคุณเป็นหนึ่ง
                                    ในนั้นล่ะก็ เรามาปรับลุคห้องนอนกันสักเล็กน้อยดีกว่าตาม 3
                                    ไอเดียนี้เลยนะคะ</p>
                            </a>
                            <a href="" class="btn btn-seemore">See more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include('footer.php'); ?>
