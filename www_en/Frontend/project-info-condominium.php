<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';
require_once 'include/help/hex2rgba.php';

$project_id = $_GET['project_id'];

if(empty($project_id) || getProjectByID($project_id) == false){
    header('Location: '.Helper::url_string('project-type-condominium.php'));
}

$lang = !empty($_GET['lang']) ? $_GET['lang'] : 'th';
$page = 'condo-detail';

$project_lang           = getProjectByID($project_id);

?>

<?php include('header.php'); ?>

<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '342952452528044'); // Insert your pixel ID here.
    fbq('track', 'PageView');
    fbq('track', 'Lead');
</script>
<noscript><img height="1" width="1" style="display:none"
 src="https://www.facebook.com/tr?id=342952452528044&ev=PageView&noscript=1"
 /></noscript>
 <!-- DO NOT MODIFY -->
 <!-- End Facebook Pixel Code -->

    <!-- CSS -->
    <link rel="stylesheet" href="<?= file_path('css/project-info-condominium2.css') ?>" type="text/css">
    <link rel="stylesheet" href="<?= file_path('js/cal_reg/css/sweetalert.css') ?>" type="text/css">
    <!-- JS -->
   <script type="text/javascript" src="<?= file_path('fancybox/jquery.mousewheel-3.0.4.pack.js') ?>"></script>
   <link rel="stylesheet" href="<?= file_path('fancybox/fancybox.min.css') ?>" />
   <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>
   <script src="<?= file_path('../build/js/jquery.validate.js') ?>" /></script>
    <script src="<?= file_path('js/cal_reg/jquery.mask.min.js') ?>" /></script>
    <script src="<?= file_path('js/cal_reg/sweetalert.min.js') ?>" /></script>
    <?php
    $project            = getProjectByID($project_id);
    if(!empty($project->latitude) && !empty($project->longtitude) && $project->project_status != 'SO'){
        ?>
        <script>
            /*function initMap() {
                // Create a map object and specify the DOM element for display.
                $(document).ready(function () {
                    var lat_proj = parseFloat($('#lat').val());
                    var lng_proj = parseFloat($('#lng').val());
                    var pos = {lat: lat_proj, lng: lng_proj};
                    var map = new google.maps.Map(document.getElementById('map'), {
                        center: pos,
                        scrollwheel: false,
                        zoom: 16,
                        zoomControlOptions: {
                            position: google.maps.ControlPosition.TOP_RIGHT
                        },
                        streetViewControl: false,
                        mapTypeControl: false,
                        styles: [{"elementType": "geometry", "stylers": [{"color": "#f5f5f5"}]}, {
                            "elementType": "labels.icon",
                            "stylers": [{"visibility": "off"}]
                        }, {
                            "elementType": "labels.text.fill",
                            "stylers": [{"color": "#616161"}]
                        }, {
                            "elementType": "labels.text.stroke",
                            "stylers": [{"color": "#f5f5f5"}]
                        }, {
                            "featureType": "administrative.land_parcel",
                            "elementType": "labels.text.fill",
                            "stylers": [{"color": "#bdbdbd"}]
                        }, {
                            "featureType": "administrative.locality",
                            "elementType": "labels.text.fill",
                            "stylers": [{"color": "#c19838"}]
                        }, {
                            "featureType": "administrative.neighborhood",
                            "elementType": "labels.text.fill",
                            "stylers": [{"color": "#c19838"}]
                        }, {
                            "featureType": "poi",
                            "elementType": "geometry",
                            "stylers": [{"color": "#eeeeee"}]
                        }, {
                            "featureType": "poi",
                            "elementType": "labels.text.fill",
                            "stylers": [{"color": "#757575"}]
                        }, {
                            "featureType": "poi.park",
                            "elementType": "geometry",
                            "stylers": [{"color": "#e5e5e5"}]
                        }, {
                            "featureType": "poi.park",
                            "elementType": "labels.text.fill",
                            "stylers": [{"color": "#9e9e9e"}]
                        }, {
                            "featureType": "road",
                            "elementType": "geometry",
                            "stylers": [{"color": "#ffffff"}]
                        }, {
                            "featureType": "road",
                            "elementType": "labels.icon",
                            "stylers": [{"visibility": "simplified"}]
                        }, {
                            "featureType": "road",
                            "elementType": "labels.text",
                            "stylers": [{"color": "#878787"}]
                        }, {
                            "featureType": "road",
                            "elementType": "labels.text.fill",
                            "stylers": [{"color": "#9f720a"}, {"visibility": "off"}]
                        }, {
                            "featureType": "road.arterial",
                            "elementType": "labels.text.fill",
                            "stylers": [{"color": "#757575"}]
                        }, {
                            "featureType": "road.highway",
                            "elementType": "geometry",
                            "stylers": [{"color": "#dadada"}]
                        }, {
                            "featureType": "road.highway",
                            "elementType": "labels.text.fill",
                            "stylers": [{"color": "#616161"}]
                        }, {
                            "featureType": "transit.line",
                            "elementType": "geometry",
                            "stylers": [{"color": "#e5e5e5"}]
                        }, {
                            "featureType": "transit.station",
                            "elementType": "geometry",
                            "stylers": [{"color": "#eeeeee"}]
                        }, {
                            "featureType": "water",
                            "elementType": "geometry",
                            "stylers": [{"color": "#c9c9c9"}]
                        }, {"featureType": "water", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]}]
                    });
var icon = {
    url: '<?= file_path('images/global/icon_google_map_gd.png') ?>',
                        scaledSize: new google.maps.Size(30, 42), // scaled size
                        origin: new google.maps.Point(0, 0), // origin
                        anchor: new google.maps.Point(0, 0) // anchor
                    };
                    var marker = new google.maps.Marker({
                        position: pos,
                        map: map,
                        icon: icon
                    });

                    var infowindow = new google.maps.InfoWindow({});

                    infowindow.close();
                    infowindow.setOptions({
                        content: '<?= Helper::checkLangEnglish($lang) ?
                        $project->project_name_en : $project->project_name_th;?>',
                        position: new google.maps.LatLng(lat_proj,lng_proj),
                        pixelOffset: new google.maps.Size(15,10),
                    });
                    infowindow.open(map);

                    google.maps.event.addListener(marker, 'click', function() {
                        window.open('https://maps.google.com?saddr=Current+Location&daddr='+lat_proj+','+lng_proj,'_blank');
                    });
                });
}*/
</script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDaz8JbxbUyy8acSAPy4ENzPhCaY4o_kj4&callback=initMap"
async defer></script> -->
<?php
}
?>

<script src="<?= file_path('js/project-info-condominium.js') ?>"></script>

<div class="page-banner page-scroll slider">
    <div id="pjHomeBanner" class="owl-carousel owl-theme">

        <?php
        $logo               = getProjectMaps($project_id);
        $project_concept    = getProjectConcept($project_id);
        //lead image by zun
        $banner_project_default = getAllBannerByProjectID_Default($project_id);
        $banner_projects    = getAllBannerByProjectID($project_id);
        $banner_homes       = null;

        if($banner_project_default > 0){

            foreach ($banner_projects as $i => $banner_project) {

                ?>

                <?php
                if ($banner_project->LEAD_IMAGE_PROJECT_FILE_TYPE == 'image_leade_banner') {
                    ?>
                    <div class="item slide">
                        <div class="banner-container banner-parallax">
                         <?=$banner_project->img_url != ''? '<a href="'.$banner_project->img_url.'" target="_blank">' : '';?>
                         <img class="<?= $i == 0 ? 'background-show' : '' ?>" src="<?= backend_url('base', $banner_project->LEAD_IMAGE_PROJECT_FILE_NAME) ?>" alt="<?=$banner_project->seo_lead?>">
                         <?=$banner_project->img_url != ''? '</a>' : '';?>
                     </div>
                     <?php if($i == 0) {
                        ?>

                        <?php
                    }?>
                </div>
                <?php
            } elseif ($banner_project->LEAD_IMAGE_PROJECT_FILE_TYPE == 'youtube') {
                $lead_img = backend_url('base', $banner_project->banner_img_thum);
                $lead_youtube_url = str_replace('watch?v=', 'embed/', $banner_project->LEAD_IMAGE_PROJECT_FILE_NAME);
                ?>
                <div class="item slide">
                    <div class="item-video">
                        <div class="banner-container banner-parallax" id="bannerVideo<?= $i ?>">
                            <div class="rps-vdobg">
                                <img src="<?= $lead_img ?>" alt="<?=$banner_project->seo_lead?>">
                            </div>

                            <div class="vdo-control">
                                <div class="play-block">
                                    <span id="vdoPlay<?= $i ?>" class="vdo-icon vdoControl vdoPlay"></span>
                                </div>
                            </div>

                            <script type="text/javascript">
                                var iframe_src = '<?= $lead_youtube_url ?>';
                                var youtube_video_id = iframe_src.match(/youtube\.com.*(\?v=|\/embed\/)(.{11})/).pop();
                                if (youtube_video_id.length == 11) {
                                    $('#vdoPlay<?=$i?>').click(function () {
                                        $("#slideVdo<?=$i?>").remove();
                                        var video_iframe = $('' +
                                            '<iframe id="video" width="100%" height="720px"' +
                                            ' src="<?= $lead_youtube_url . "?autoplay=0" ?>">' +
                                            '</iframe>');
                                        $('#bannerVideo<?=$i?>').find('.vdo-control').remove();
                                        $('.rps-vdobg').remove();
                                        $('#bannerVideo<?=$i?>').append(video_iframe);
                                        $(this).trigger('stop.autoplay.owl');
                                        $('#scrollNext').hide();
                                    });
                                }
                            </script>
                        </div>
                    </div>

                </div>

                <?php
            } elseif ($banner_project->LEAD_IMAGE_PROJECT_FILE_TYPE == 'vdo') {
                ?>
                <div class="item slide">
                    <script>
                        $(document).ready(function () {
                            $('#vdoPlay<?= $i ?>').click(function () {
                                $('#slideVdo<?= $i ?>').css('display', 'block');
                                $('#slideVdo<?= $i ?>')[0].play();
                                $(this).fadeOut(200);
                                $('#vdoPause<?= $i ?>').fadeIn(200);
                                $('.vid_bg').remove();

                                $(".vdo-control").mouseenter(function (event) {
                                    event.stopPropagation();
                                    $('#vdoPause<?= $i ?>').addClass("btnshown");
                                }).mouseleave(function (event) {
                                    event.stopPropagation();
                                    $('#vdoPause<?= $i ?>').removeClass("btnshown");
                                });
                                $('#scrollNext').hide();
                            });

                            $("#vdoPause<?= $i ?>").click(function () {
                                $('#slideVdo<?= $i ?>').get(0).pause();
                                $(this).fadeOut(200);
                                $('#vdoPlay<?= $i ?>').fadeIn(200);
                                $('#scrollNext').show();
                            });
                        });
                    </script>
                    <div class="banner-container banner-parallax">
                        <img class="vid_bg" src="<?= backend_url('base', $banner_project->banner_img_thum) ?>" alt="<?=$banner_project->seo_lead?>">
                        <video loop="loop" id="slideVdo<?= $i ?>" class="" style="display:none;">
                            <source src="<?= backend_url('base', $banner_project->LEAD_IMAGE_PROJECT_FILE_NAME) ?>" type="video/mp4">
                            </video>

                            <div class="vdo-control">
                                <div class="play-block">
                                    <span id="vdoPlay<?= $i ?>" class="vdo-icon vdoControl vdoPlay"></span>
                                    <span id="vdoPause<?= $i ?>" class="vdo-icon vdoControl vdoPause"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php
                } elseif ($banner_project->LEAD_IMAGE_PROJECT_FILE_TYPE == 'banner') {
                    ?>
                    <div class="item slide" <?php if (!empty($banner_project->lead_img_url)) echo "onclick=\"location.href='" . $banner_project->lead_img_url . "'\""; ?>>
                        <div class="banner-container banner-parallax">
                            <img class="background-show" src="<?= backend_url('base',$banner_project->LEAD_IMAGE_PROJECT_FILE_NAME)?>" alt="">

                            <?php
                            if(!empty($banner_project->banner_text)){
                                ?>
                                <div <?= !empty($banner_project->banner_link) ? 'href="'.$banner_project->banner_link.'"' : '' ?>
                                class="banner-activity"
                                <?php if(!empty($banner_project->backgroup_color)) {
                                    ?>
                                    style="background-color: <?php echo hex2rgba($banner_project->backgroup_color, 0.8);?>"
                                    <?php
                                }?>
                                >
                                <?= $banner_project->banner_text ?>
                                <?php if(!empty($banner_project->banner_link)) {
                                    ?>
                                    <span class="link-acty"></span>
                                    <?php
                                }?>
                                <span class="close-acty"></span>
                            </div>
                            <?php
                        }
                        ?>

                    </div>
                    <?php if($i == 0) {
                        ?>

                        <?php
                    }?>
                </div>
                <?php
            }
            ?>

            <?php
        }//END foreach

        ?>

        <!-- Lead image default -->
        <?php
    }else{
        foreach ($banner_projects as $i => $banner_project) {
            if ($banner_project->LEAD_IMAGE_PROJECT_FILE_TYPE == 'image') {
                //print_r($banner_project);
                ?>
                <div class="item slide">
                    <div class="banner-container banner-parallax">
                     <?=$banner_project->img_url != ''? '<a href="'.$banner_project->img_url.'" target="_blank">' : '';?>

                     <img class="<?= $i == 0 ? 'background-show' : '' ?>" src="<?= backend_url('base', $banner_project->LEAD_IMAGE_PROJECT_FILE_NAME) ?>" alt="<?=$banner_project->seo_lead?>">
                     <?=$banner_project->img_url != ''? '</a>' : '';?>
                 </div>
                 <?php if($i == 0) {
                    ?>

                    <?php
                }?>
            </div>
            <?php
        }
        }//END for image defualt
    }//END if check image defualt
    ?>
    <!-- // END Lead image default -->

</div>
<!-- zun -->
<div id="pjHomeBanner_default" class="owl-carousel owl-theme" style="position: relative!important;top:0!important;">
 <?php
 foreach ($banner_projects as $i => $banner_project) {
     if ($banner_project->LEAD_IMAGE_PROJECT_FILE_TYPE == 'image') {
           //print_r($banner_project);
         ?>
         <div class="item slide">
             <div class="banner-container banner-parallax">
              <?=$banner_project->img_url != ''? '<a href="'.$banner_project->img_url.'" target="_blank">' : '';?>

              <img class="<?= $i == 0 ? 'background-show' : '' ?>" src="<?= backend_url('base', $banner_project->LEAD_IMAGE_PROJECT_FILE_NAME) ?>" alt="<?=$banner_project->seo_lead?>">
              <?=$banner_project->img_url != ''? '</a>' : '';?> 
          </div>
          <?php if($i == 0) {
             ?>

             <?php
         }?>
     </div>
     <?php
 }
}
?>
</div> 
<!-- end zun -->
<span id="scrollNext" class="i-scroll-next"></span>
</div>
<?php
    //elementSharedFacebookProjectDetail($project_id,$banner_projects);
?>

<?php
$banner_homes = !empty($banner_homes) ? $banner_homes : $banner_projects;
if(!empty($banner_homes)){
    foreach ($banner_homes as $i => $banner){
        if(!empty($banner->banner_text)) {
            ?>
            <a <?= !empty($banner->banner_link) ? 'href="'.$banner->banner_link.'"' : '' ?>
            class="banner-activity banner-rsp"
            <?php if(!empty($banner->banner_backgroup)) {
                ?>
                style="background-color: <?php echo hex2rgba($banner_project->backgroup_color, 0.8);?>"
                <?php
            }?>
            >
            <?= $banner->banner_text ?>
            <?php if(!empty($banner->banner_link)) {
                ?>
                <span class="link-acty"></span>
                <?php
            }?>
            <span class="close-acty"></span>
        </a>
        <?php
        break;
    }
}
}
?>


<!-- Responsive Submenu-->
<div class="proj-rps-submenu-container projcondo-submenu">
    <p><?= Helper::checkLangEnglish($lang) ?
    'Project Details' : 'Project Details'; ?></p>
    <div class="proj-rps-submenu">
        <?php include('snippet/snippet-menu-project-info-condominium.php'); ?>
    </div>
</div>
<!-- //Responsive Submenu-->


<?php
$projectMap = getProjectMaps($project_id);
$project    = getProjectByID($project_id);
$project_price   = getProjectPrice($project_id);
$project_concept = getProjectConcept($project_id);
$project_promotion = getProjectPromotion($project_id);
$logo          = getProjectMaps($project_id);
$project_sub = getProjectSub($project_id,3);
?>

<div id="sec-information" class="content project-info-page">
    <div class="project-info-container">
        <div class="container">
            <!-- H1 -->
            <?php 
            $SEO = getSEOUrl($actual_link);
            if($actual_link == @$SEO->url_page){
                echo '<h1 class="heading-title header-margin">'.$SEO->h1.'</h1>'; 
            }
            ?>
            <!-- End H1 -->
            <div class="row">
                <div class="info-logo-img">
                    <img src="<?= is_object($logo) ? backend_url('base', $logo->project_logo) : '' ?>" alt="" >
                </div>
            </div>

            <?php if($project->project_status == 'SO') {
                ?>
                <div class="tagbox2_sold soldout-tag">
                    <div class="tag2">sold<br>out</div>
                </div>
                <?php
            }?>

            <?php if($project->project_status != 'SO'){ ?>

                <div class="row mb15">
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                        <p class="subtitle green">
                            <?= Helper::checkLangEnglish($lang) ?
                            'Project Details' : 'Project Details'; ?>
                        </p>
                    </div>
                    <div class="col-md-6">
                        <p><b><span class="line-space">
                            <?php if($project != false) {
                                $project->project_name_th = Helper::checkLangEnglish($lang) ?
                                $project->project_name_en : $project->project_name_th;
                                echo $project->project_name_th;
                            }
                            ?></span></b><br>
                            <?php
                            if(!empty($project->location)) {
                                ?>
                                <span class="line-space">
                                    <?= Helper::checkLangEnglish($lang) ?
                                    'Location' : "Project's location"; ?>
                                    <?= Helper::checkLangEnglish($lang) ?
                                    $project->location_en : $project->location ;   ?>
                                </span>
                                <?php
                            }
                            ?>
                            <span class="line-space">
                                <?php
                                @$projectAreaCondo = getProjectAreaCondo($project_sub->project_sub_id);
                                ?>
				<?php if(!isset($project->area_farm) || !isset($project->area_square_m) || !isset($project->area_square_w) ){ ?>
                                <br>
                                <?= Helper::checkLangEnglish($lang) ?'Land areas' : 'Land area'; ?>

                                <?php
                                if(!empty($project->area_farm) ){
                                    echo $project->area_farm;
                                }
                                if(!empty($project->area_square_m)){
                                    echo "-".$project->area_square_m;
                                }
                                if(!empty($project->area_square_w)){
                                    echo "-".$project->area_square_w;
                                }
                                ?>

                                <?= Helper::checkLangEnglish($lang) ?'Rai (Approximately)' : 'Rai (Approximately)'; ?>

                                <br>
                            <?php }?>
                                <?php
                                if(!empty($project->area_land)){
                                    ?>
                                    <?= Helper::checkLangEnglish($lang) ?
                                    'Number of Plan' : 'Number of Plan'; ?>
                                    <?= $project->area_land ?>
                                    <?= Helper::checkLangEnglish($lang) ?
                                    'Unit' : 'Unit'; ?>
                                    <br>
                                    <?php
                                } ?>
                                
                                <?php
                                if(!empty($projectAreaCondo->building_unit_id)){
                                    ?>
                                    <?= Helper::checkLangEnglish($lang) ?
                                    'Condominium' : 'Condominium'; ?>
                                    <?= $projectAreaCondo->building_unit_id ?>
                                    <?= Helper::checkLangEnglish($lang) ?
                                    'Building' : 'Building'; ?>
                                    <?php
                                }  
                                if(!empty($projectAreaCondo->floor)){
                                    ?>

                                    <?= $projectAreaCondo->floor ?>
                                    <?= Helper::checkLangEnglish($lang) ?
                                    'floor' : 'Storey'; ?>
                                    <?php
                                    echo "<br>";
                                } ?>
                                <?php
                                @$projectBuildUnits = getBuildingUnit($project_sub->project_sub_id);
                                foreach ($projectBuildUnits as $projectBuildUnit){
                                    ?>
                                    
                                        <?php if($projectBuildUnit->building_name !='เลือกอาคาร'){?>
                                            <?php if($projectBuildUnit->building_name !=''){?>
                                    <?= Helper::checkLangEnglish($lang) ?
                                    '' : ''; ?>

                                    <?= $projectBuildUnit->building_name ?>

                                    <?= Helper::checkLangEnglish($lang) ?
                                    'Building' : 'Building'; ?>
                                    <?= $projectBuildUnit->building_num_layer ?>

                                    <?= Helper::checkLangEnglish($lang) ?
                                    'Storey' : 'Storey'; ?>

                                    <?= $projectBuildUnit->building_unit ?>

                                    <?= Helper::checkLangEnglish($lang) ?
                                    'Units' : 'Units'; ?>
                                    <br>
                                    <?php
                                    }
                                } ?>
                                
                              <?php } ?>
                                <?php
                                if(!empty($projectAreaCondo->num_plan_condo)){
                                    ?>
                                    <?= Helper::checkLangEnglish($lang) ?
                                    'Total ' : 'Total '; ?><?= $projectAreaCondo->num_plan_condo ?>
                                    <?= Helper::checkLangEnglish($lang) ?
                                    'Unit Type' : 'Unit Type'; ?>
                                    <?php
				  echo "<br>";	
                                } ?>
                               
                                <?php
                                /*
                                $unitPlanCondos = getUnitPlanCondo($project_sub->project_sub_id);
                                $index = 1;
                                foreach($unitPlanCondos as $unitPlanCondo){
                                    if(!empty($unitPlanCondo->unit_plan_name_th)) {
                                        $unitPlanCondo->unit_plan_name_th = Helper::checkLangEnglish($lang) ? $unitPlanCondo->unit_plan_name_en : $unitPlanCondo->unit_plan_name_th;
                                        ?>
                                         <?= $unitPlanCondo->unit_plan_name_th ?>   <?= $unitPlanCondo->unit_plan_size ?>
                                        <?= Helper::checkLangEnglish($lang) ?
                                            'sq.m.' : 'ตารางเมตร'; ?>
                                        <br>
                                        <?php $index++;
                                    }
                                }
                                */
                                ?>
                            </span>
                            <?php if(!empty($project_price->project_price)) { ?>
                                <?php
                                $priceCondo = Helper::getProjectPrice($project_price->project_price,$project_price->price_mode,$lang)
                                ?>
                                <?php 
                                if($project_price->price_mode == '1'){
                                    $mode_price = 'Starting price from';
                                    $start = 'Start';
                                }else{
                                    $mode_price = 'Pricing from';
                                    $start = 'Pricing from';
                                }
                                ?>
                                <p class="subtitle">
                                    <?= Helper::checkLangEnglish($lang) ?
                                    $start : $mode_price; ?>
                                    <?php if($project_price != false) echo $priceCondo ?>
                                    <?= ' ' . Helper::getPriceModeName($project_price->price_mode,$project_price->project_price,$lang) ?>
                                </p>
                            <?php } ?>
                        </div>
                        <div class="col-md-2"></div>
                    </div>

                    <?php if(!empty($project_concept->concept_en) || !empty($project_concept->concept_th)) {
                        $project_concept->concept_th = Helper::checkLangEnglish($lang) ? $project_concept->concept_en : $project_concept->concept_th;
                        $project_con_en = 'Project Concept';
                        $project_con_th = 'Project Concept';

                        if ($project_concept->concept_en == '' ) { $project_con_en = '';}else{ $project_con_th = 'Project Concept';}
                        ?>
                        <div class="row mb15">
                            <div class="col-md-2"></div>
                            <div class="col-md-2">
                                <p class="subtitle green">
                                    <?= Helper::checkLangEnglish($lang) ?
                                    $project_con_en : $project_con_th;
                                    ?>
                                </p>
                            </div>
                            <div class="col-md-6">
                                <p class="distinctive1"><?php if ($project_concept != false) ?>
                                <?=!empty($project_concept->concept_th) ? str_replace(array("\n\r", "\n","n/g"), '<br/>', $project_concept->concept_th) : ''?>

                            </p>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                    <?php
                }?>

                <?php
                if(is_object($project_concept)){
                    $project_concept->distinctive_th = Helper::checkLangEnglish($lang) ?
                    $project_concept->distinctive_en : $project_concept->distinctive_th;
                }
                if(!empty($project_concept->distinctive_th)) {
                    $distinctive_en = 'Project selling point';
                    $distinctive_th = "Project's highlight";

                    if ($project_concept->distinctive_en == '' ) { $distinctive_en = '';}else{ $distinctive_th = "Project's highlight";}
                    ?>
                    <div class="row mb15">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <p class="subtitle green">
                                <?= Helper::checkLangEnglish($lang) ?
                                $distinctive_en : $distinctive_th; ?>
                            </p>
                        </div>
                        <div class="col-md-6 distinctive2">
                            <p><?php if($project_concept != false)  ?>
                            <?=!empty($project_concept->distinctive_th) ? str_replace(array("\n\r", "\n","n/g"), '<br/>', $project_concept->distinctive_th) : ''?>
                               <!--  <script type="text/javascript">
                                    $('.distinctive2').html(`<?php //echo $project_concept->distinctive_th?>`.replace(/\r?\n/g, '<br/>'))
                                </script> -->
                            </p>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                <?php } ?>

                <?php
                if(is_object($project_concept)){
                    $project_concept->facilities_th = Helper::checkLangEnglish($lang) ?
                    $project_concept->facilities_en : $project_concept->facilities_th;
                }
                if(!empty($project_concept->facilities_th)) {
                    ?>
                    <div class="row mb15">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <p class="subtitle green">
                                <?= Helper::checkLangEnglish($lang) ?
                                'Facilities' : 'Facilities'; ?>
                            </p>
                        </div>
                        <div class="col-md-6 distinctive3">
                            <p><?php if($project_concept != false)  ?>
                            <?=!empty( $project_concept->facilities_th) ? str_replace(array("\n\r", "\n","n/g"), '<br/>',  $project_concept->facilities_th) : ''?>
                                <!-- <script type="text/javascript">
                                    $('.distinctive3').html(`<?php //echo $project_concept->facilities_th?>`.replace(/\r?\n/g, '<br/>'))
                                </script> -->
                            </p>
                        </div>
                        <div class="col-md-2"></div>
                    </div>

                <?php } ?>

                <?php
                if(is_object($project_concept)){
                    $project_concept->security_th = Helper::checkLangEnglish($lang) ?
                    $project_concept->security_en : $project_concept->security_th;
                }

                if(!empty($project_concept->security_th)) {
                    ?>
                    <div class="row mb15">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <p class="subtitle green">
                                <?= Helper::checkLangEnglish($lang) ?
                                'Securities' : 'Security System'; ?>
                            </p>
                        </div>
                        <div class="col-md-6 distinctive4">
                            <p><?php if($project_concept != false)  ?>
                            <?=!empty($project_concept->security_th) ? str_replace(array("\n\r", "\n","n/g"), '<br/>', $project_concept->security_th) : ''?>
                                <!-- <script type="text/javascript">
                                    $('.distinctive4').html(`<?php //echo $project_concept->security_th?>`.replace(/\r?\n/g, '<br/>'))
                                </script> -->
                            </p>
                        </div>
                        <div class="col-md-2"></div>
                    </div>

                <?php } ?>

                <?php
                if(!empty($project->paking) || !empty($project->paking_en)){
                    ?>
                    <?php if(!empty($project->paking)){?>
                    <div class="row mb15">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <p class="subtitle green">
                                <?= Helper::checkLangEnglish($lang) ?
                                'Parking' : 'Car parking space'; ?>
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p>
                               
                                <?= Helper::checkLangEnglish($lang) ?
                                $project->paking_en : $project->paking; ?>
                                <?= Helper::checkLangEnglish($lang) ? ' (Approximately) ' : ' (Approximately) '; ?>
                                <?= Helper::checkLangEnglish($lang) ? '' : ''; ?>
                            </p>

                        </div>
                        <div class="col-md-2"></div>
                    </div>
                    <?php
                }
                }
                ?>

                <?php
                if($projectMap != false && !empty($projectMap->brochure)){
                    ?>
                    <div class="row mb15">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <p class="subtitle green">

                            </p>
                        </div>
                        <div class="col-md-6 distinctive3">
                            <a href="<?= backend_url('base',$projectMap->brochure) ?>"
                             target="_blank"
                             class="dl-brochure">
                             <i class="i-download"></i>
                             Download Brochure
                         </a>
                     </div>
                     <div class="col-md-2"></div>
                 </div>
                 <?php
             }
             ?>

             <?php
         }?>

     </div>
 </div>

 <div class="container">
    <?php if($project_promotion != false && $project->project_status != 'SO') {
        $img_promotion = getImagePromotion($project_promotion->promotion_id);
        ?>
        <div id="sec-promotion" class="block-margin">
            <p class="heading-title header-margin">
                <?= Helper::checkLangEnglish($lang) ?
                'Promotion' : 'Promotion'; ?>
            </p>
            <div class="row">
                <div id="projectPromoSlide">
                    <div class="item">
                        <div class="">
                            <div class="col-md-8">
                                <div class="review-img">
                                    <img src="<?php if($img_promotion != false) echo  backend_url('base',$img_promotion->img_promotion_name) ?>" alt="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="promo-descrp">
                                    <p class="title green">
                                        <?php if(Helper::checkLangEnglish($lang)){
                                           echo str_replace(array("\n\r", "\n","n/g"), '<br/>', $project_promotion->promotion_name_en);
                                       }else{
                                        echo str_replace(array("\n\r", "\n","n/g"), '<br/>', $project_promotion->promotion_name_th);
                                    } ?>

                                </p>
                                <p><?php if(Helper::checkLangEnglish($lang)){
                                   echo str_replace(array("\n\r", "\n","n/g"), '<br/>', $project_promotion->promotion_detail_en);
                               }else{
                                echo str_replace(array("\n\r", "\n","n/g"), '<br/>', $project_promotion->promotion_detail_th);
                            } ?></p>
                            <?php if(!empty($project_promotion->promotion_url) && $project_promotion->promotion_url != ''){
                                ?>
                                <a href="<?= $project_promotion->promotion_url ?>" class="btn btn-seemore">See more</a>
                                <?php
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
</div>

<?php
$hasLocation = !empty($project->map_link) || !empty($projectMap->map_img) || !empty($projectMap->map_pdf) && $project->project_status != 'SO';
?>
<?php  if($hasLocation) { ?>
    <input type="hidden" id="lat" value="<?= $project->map_link ?>">
    <input type="hidden" id="lng" value="<?= $project->longtitude ?>">
<?php } ?>

        <?php
        if($project->project_status != 'SO'){
            ?>
            <div id="sec-location" class="project-location-block block-margin">
	     <?php  if($hasLocation) { ?>
                <div class="container">
                    <p class="heading-title header-margin"><?= Helper::checkLangEnglish($lang) ?
                            'Location' : "Project's location"; ?></p>
                </div>
		<?php }?>

        

        <?php  if($hasLocation) { ?>
            <div class="map-container">
                <div id="map"></div>
                <!--                        <span class="i-getdirection" onclick="toLocationByWalk();"></span>-->
                <span class="i-getdirection-car" onclick="toLocation();"></span>

                <div class="map-custom">
                    <?php if(!empty($project->map_link) ){
                        ?>
                     <a  href="<?php echo $project->map_link ?>" class="map-google-jpg" target="_blank"><i class="map-jpg"></i><img src="<?= file_path('images/global/googlemap.png')?>"></a>
                    <?php } ?>
                    <?php if(!empty($projectMap) && (!empty($projectMap->map_img) || !empty($projectMap->map_pdf)) ){
                        ?>
                        <?php
                        if(!empty($projectMap->map_pdf)){
                            ?>
                            <a href="<?php echo backend_url('base',$projectMap->map_pdf); ?>" class="map-download-pdf" target="_blank"><i class="map-pdf"></i><span class="cal-txt">Map PDF</span></a>
                            <?php
                        }if(!empty($projectMap->map_img)){
                            ?>
                            <a href="<?php echo backend_url('base',$projectMap->map_img); ?>" class="map-download-jpg" target="_blank"><i class="map-jpg"></i><span class="cal-txt">Map JPG</span></a>
                            <?php
                        }
                        ?>

                        <?php
                    } ?>
                   
                </div>
            </div>
        <?php } ?>

                <div  class="condo-pd" ></div>
        <?php
            $projectNearBys_ck = getProjectNearBy($project_id);
            if($projectNearBys_ck[0]->nearby_name_th != ""){
        ?>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="title">
                        <?= Helper::checkLangEnglish($lang) ?
                        'Facilities' : 'Facilities around the project'; ?>
                        <span>
                            <?= Helper::checkLangEnglish($lang) ?
                            '(Approximate distance)' : '(Approximate distance)'; ?>
                        </span>
                    </p>

                    <?php
                    $projectNearBys = getProjectNearBy($project_id);
                    $leftProjectNearBys  = [];
                    $rightProjectNearBys = [];
                    foreach ((array)$projectNearBys as $i => $projectNearBy)
                    {
                        if($i%2 == 0){
                            $leftProjectNearBys[] = $projectNearBy;
                        }elseif ($i%2 == 1){
                            $rightProjectNearBys[] = $projectNearBy;
                        }
                    }
                    ?>
                    <div class="content-style">
                        <div class="col-md-6">
                            <ul class="tbs1">
                                <?php
                                foreach ($leftProjectNearBys as $projectNearBy) {
                                    if (!empty($projectNearBy->nearby_name_th) && !empty($projectNearBy->interval)) {
                                        ?>
                                        <li>
                                            <?= Helper::checkLangEnglish($lang) ?
                                            $projectNearBy->nearby_name_en : $projectNearBy->nearby_name_th;?>  <?= $projectNearBy->interval ?>
                                            <?php if($projectNearBy->nearby_unit == 'กม.'){?>    
                                                <?= (Helper::checkLangEnglish($lang) ? 'km' : 'km.') ?>
                                            <?php }elseif ($projectNearBy->nearby_unit == 'เมตร') { ?>
                                             <?= (Helper::checkLangEnglish($lang) ? 'm' : 'm') ?>
                                         <?php }else{?>
                                             <?= (Helper::checkLangEnglish($lang) ? 'km' : 'km.') ?>
                                         <?php }?>
                                     </li>
                                    <?php
                                }
                                ?>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul class="tbs1">
                            <?php
                            foreach ($rightProjectNearBys as $projectNearBy) {
                                if (!empty($projectNearBy->nearby_name_th) && !empty($projectNearBy->interval)) {
                                    ?>
                                    <li>
                                            <?= Helper::checkLangEnglish($lang) ?
                                            $projectNearBy->nearby_name_en : $projectNearBy->nearby_name_th;?>  <?= $projectNearBy->interval ?>
                                            <?php if($projectNearBy->nearby_unit == 'กม.'){?>    
                                                <?= (Helper::checkLangEnglish($lang) ? 'km' : 'km.') ?>
                                            <?php }elseif ($projectNearBy->nearby_unit == 'เมตร') { ?>
                                             <?= (Helper::checkLangEnglish($lang) ? 'm' : 'm') ?>
                                         <?php }else{?>
                                             <?= (Helper::checkLangEnglish($lang) ? 'km' : 'km.') ?>
                                         <?php }?>
                                     </li>
                                    <?php
                                }
                                ?>
                            <?php } ?>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
   <?php }?>
</div>
<?php
}
?>

<div class="container">

    <?php
    $galleryImgProjects = getGalleryImgProject($project_id);
    if($galleryImgProjects != false && $project->project_status != 'SO'){
        ?>

        <div id="sec-gallery" class="gallery-block block-margin">
            <p class="heading-title header-margin">
                <?= Helper::checkLangEnglish($lang) ?
                'GALLERY' : 'GALLERY'; ?>
            </p>
            <div class="tpl5-img">
                <div id="tpl5-img">
                    <?php
                    $i = 1;
                    foreach ($galleryImgProjects as $galleryImgProject) {
                        ?>
                        <div class="item tpl5-img-<?= $i ?>">
                            <a class="gallery" href="<?= backend_url('base',$galleryImgProject->galery_project_img_name) ?>">
                                <img src="<?= backend_url('base',$galleryImgProject->galery_project_img_name) ?>" alt="<?=$galleryImgProject->galery_project_img_seo?>">
                                <p class="title-gallery"> <?= Helper::checkLangEnglish($lang) ? $galleryImgProject->galery_project_img_desc_en : $galleryImgProject->galery_project_img_desc ; ?> </p>
                            </a>
                        </div>
                        <?php
                        $i++;
                    }
                    ?>
                </div>
            </div>
        </div>

    <?php } ?>

    <?php if($project->project_status == 'SO'){

        $projectNearByZones = getProjectRelate($project_id,$project->zone_id,3);
        if($projectNearByZones != false){
            ?>
            <div class="project-info-detail-block">
                <p class="heading-title">
                    <?= Helper::checkLangEnglish($lang) ?
                    'Other Projects' : 'Other projects which may be interest'; ?>
                </p>
                <div class="row">
                    <div id="colSlide" class="col-slide">
                        <?php

                        foreach ($projectNearByZones as $projectNearByZone) {
                            $project_id = $projectNearByZone->project_id;
                            $project_relate = getProjectByID($project_id);
                            $banner  = getBannerByProjectID($project_id);
                            $project_concept = getProjectConcept($project_id);
                            $project_price = getProjectPrice($project_id);
                            ?>
                            <div class="item">
                                <div class="project-onsale">
                                    <div class="project-info-other-img">
                                        <img src="<?= !empty($banner) ? backend_url('base',$banner->LEAD_IMAGE_PROJECT_FILE_NAME) : ''?>" alt="">
                                    </div>
                                    <?php
                                    $url_project_relate = $router->generate('condominium-detail',[
                                        'project_name'  =>  str_replace(' ','-',$projectNearByZone->project_name_th),
                                        'lang'          =>  'th'
                                    ]);
                                    ?>
                                    <a href="<?= $url_project_relate ?>">
                                        <p class="title"><?= $project_relate->project_name_th ?></p>
                                        <p class="gray mb0"><?= !empty($project_concept->concept_th) ? $project_concept->concept_th : '' ?></p>
                                        <p class="title green">
                                            <strong>
                                                <?php 
                                                if($project_price->price_mode == '1'){
                                                    $mode_price = 'Starting price from ';
                                                }else{
                                                    $mode_price = 'Price from';
                                                }
                                                ?>
                                                <?= Helper::checkLangEnglish($lang) ?
                                                'Start' : $mode_price; ?>
                                                <?= !empty($project_price->project_price) ? Helper::getProjectPrice($project_price->project_price,$project_price->price_mode,$lang) : '' ?>
                                                <?= is_object($project_price) ? Helper::getPriceModeName($project_price->price_mode,$project_price->project_price,$lang) : 'MB' ?>
                                            </strong>
                                        </p>
                                    </a>
                                </div>
                            </div>
                            <?php

                        }

                        ?>
                    </div>
                </div>
            </div>
            <br><br>
            <?php
        }
    } ?>

    <?php
    $project360 = get360Project($project_id);
    $vdoProject = getVDOProject($project_id);
    if((!empty($project360) || !empty($vdoProject)) && $project->project_status != 'SO'){
        ?>

        <div class="project-info-other-block block-margin" id="sec-media">
            <div class="row">

                <?php
                if($project360 != false) {
                    ?>

                    <div id="sec-virtual" class="col-md-6">
                        <p class="heading-title">
                            <?= Helper::checkLangEnglish($lang) ?
                            '360 Virtual Tour' : '360-degree image'; ?>
                        </p>
                        <div class="project-info-other-img">

                            <a id="various" href="<?= $project360->c360_project_url ?>"><span class="i-view-360"></span><img src="<?= backend_url('base', $project360->thumnail) ?>" alt=""></a>
                            <script type="text/javascript">
                                $(document).ready(function() {
                                    $("#various").fancybox({
                                        'width'             : '100%',
                                        'height'            : '100%',
                                        'autoScale'         : false,
                                        'transitionIn'      : 'none',
                                        'transitionOut'     : 'none',
                                        'type'              : 'iframe'
                                    });
                                });
                            </script>
                        </div>
                    </div>

                    <?php
                }
                if ($vdoProject != false) {
                    if($vdoProject->tvc_type == 'youtube'){
                        $youtube_img = backend_url('base',$vdoProject->thumnail);
                        $youtube_url = str_replace('watch?v=', 'embed/', $vdoProject->clip_project_url);
                        $parameterPef= strpos($youtube_url, '?') !== false ? '&' : '?';
                        ?>
                        <div id="sec-tvc" class="col-md-6">
                            <p class="heading-title">VDO</p>
                            <div class="project-info-other-img">
                                <a href="#" data-featherlight="#lbVdo1">
                                    <span class="i-view-vdo"></span>
                                    <img src="<?= $youtube_img ?>" alt="" style="">
                                </a>
                                <iframe class="lightbox" src="<?= $youtube_url . $parameterPef ."autoplay=0" ?>" width="1000"
                                    height="560" id="lbVdo1" style="border:none;" webkitallowfullscreen
                                    mozallowfullscreen allowfullscreen></iframe>

                                </div>
                                <!--                            <p class="title">Good Moment</p>-->
                            </div>
                            <?php
                        }elseif($vdoProject->tvc_type == 'vdo'){
                            ?>
                            <div id="sec-tvc" class="col-md-6">
                                <p class="heading-title">VDO</p>
                                <div class="project-info-other-img">
                                    <a id="tvc_vdo">
                                        <span class="i-view-vdo"></span>
                                        <img src="<?= backend_url('base',$vdoProject->thumnail) ?>" alt="" style="">
                                    </a>
                                    <script>
                                        $(document).ready(function () {
                                            $('#tvc_vdo').click(function () {
                                                $('#tvcVdo').show();
                                                $.featherlight($('#tvcVdo'),{});
                                                $('.featherlight-content #tvc_video_player')[0].play();
                                                $('#tvcVdo').hide();
                                            });
                                        })
                                    </script>
                                    <div id="tvcVdo" style="position:relative;width: 100%;display: none;">
                                        <video id='tvc_video_player' preload='none' controls>
                                            <source src="<?= backend_url('base',$vdoProject->clip_project_url) ?>" type="video/mp4">
                                            </video>
                                        </div>

                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                        <?php } ?>
                    </div>
                </div>

            <?php } ?>

            <?php
            $communities = getCommunityFeature($project_id);
            if($communities != false && $project->project_status != 'SO'){
                ?>
                <div id="sec-facility" class="facility-block block-margin">
                    <p class="heading-title header-margin">
                        <?= Helper::checkLangEnglish($lang) ?
                        'Facilities' : 'Facilities avaialble'; ?>
                    </p>
                    <div class="row">
                        <div id="facilityImg" class="facility-img-block">

                            <?php
                            foreach ($communities as $community) {
                                ?>
                                <div class="item">
                                    <div class="review-img">
                                        <a class="fac-img" href="<?= backend_url('base',$community->community_features_img) ?>">
                                            <img src="<?= backend_url('base',$community->community_features_img) ?>" alt="">
                                        </a>
                                    </div>

                                    <div class="projecttype-descrp">
                                        <p class="title"><?= Helper::checkLangEnglish($lang) ?
                                        $community->community_name_en : $community->community_name; ?></p>
                                        <p>
                                            <?= Helper::checkLangEnglish($lang) ?
                                            $community->community_features_desc_en : $community->community_features_desc; ?>
                                        </p>
                                    </div>

                                </div>
                                <?php
                            }
                            ?>

                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <?php
            @$master_plans = getMasterPlan($project_sub->project_sub_id);

            if(!empty($master_plans) && $project->project_status != 'SO'){
                ?>
                <div id="sec-projectplan" class="block-margin">
                    <p class="heading-title header-margin">
                        <?= Helper::checkLangEnglish($lang) ?
                        'Master plan' : 'Master plan'; ?>
                    </p>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="projectplanImg" class="projectplan-img-block">
                                <?php
                                foreach ($master_plans as $master_plan){
                                    $img_name = str_replace(' ','',$master_plan->master_plan_img_name);
                                    ?>
                                    <div class="item">
                                        <a
                                        <?php
                                        if(!empty($img_name)){
                                            ?>
                                            data-featherlight="<?= !empty($img_name) ? backend_url('base',$master_plan->master_plan_img_name) : '' ?>"
                                            <?php
                                        }
                                        ?>>
                                        <img src="<?= !empty($img_name) ? backend_url('base',$master_plan->master_plan_img_name) : '' ?>"
                                        alt="<?= $master_plan->master_plan_img_seo ?>" class="" style="width:initial;">
                                        <span class="i-zoom"></span>
                                        <p class="floorplan-txt">
                                            <?= Helper::checkLangEnglish($lang) ?
                                            $master_plan->master_plan_dis_en : $master_plan->master_plan_dis_th; ?></p>
                                        </a>
                                    </div>

                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <?php
            $roomPlans = getUnitPlanCondo($project_id);
            if($roomPlans != false && !empty($roomPlans) && $project->project_status != 'SO') {
                ?>
                <div id="sec-projectplan" class="block-margin">
                    <p class="heading-title header-margin">
                        <?= Helper::checkLangEnglish($lang) ?
                        'Unit Plan' : 'Unit Plan'; ?>
                    </p>
                    <div id="roomplanImg" class="roomplan-img-block">
                        <?php
                        foreach ($roomPlans as $roomPlan) {
                            $img_name = str_replace(' ','',$roomPlan->unit_plan_img_name);
                            ?>
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-7">
                                        <a
                                        <?php
                                        if(!empty($img_name)){
                                            ?>
                                            data-featherlight="<?= !empty($img_name) ? backend_url('base',$roomPlan->unit_plan_img_name) : '' ?>"
                                            <?php
                                        }
                                        ?>>
                                        <img src="<?= !empty($img_name) ? backend_url('base', $roomPlan->unit_plan_img_name) : '' ?>" alt="">
                                        <span class="i-zoom"></span>
                                    </a>
                                </div>
                                <div class="col-md-5">
                                    <div class="roomplan-detail">
                                        <p class="title"><span
                                            class="green">
                                            <?= Helper::checkLangEnglish($lang) ?
                                            'Type:' : 'Type:'; ?>
                                        </span>
                                        <?= Helper::checkLangEnglish($lang) ?
                                        $roomPlan->unit_plan_name_en : $roomPlan->unit_plan_name_th; ?>
                                    </p>
                                    <p class="title mb10"><span
                                        class="green">
                                        <?= Helper::checkLangEnglish($lang) ?
                                        'Size:' : 'Size:'; ?>
                                        </span><?= $roomPlan->unit_plan_size ?>
                                        <?= Helper::checkLangEnglish($lang) ?
                                        'sq.m.' : 'sq.m.'; ?>
                                    </p>
                                    <p class="title">
                                        <?= Helper::checkLangEnglish($lang) ?
                                        'Details' : 'Details'; ?>
                                    </p>
                                    <p><?= Helper::checkLangEnglish($lang) ?
                                    $roomPlan->unit_plan_dis_en : $roomPlan->unit_plan_dis_th; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>

            </div>
        </div>
        <?php
    }
    ?>

    <?php
    @$progress_groups = getProgressCondo($project_sub->project_sub_id);

    if($progress_groups != false && !empty($progress_groups[0]->computed) && $project->project_status != 'SO'){

        ?>

        <div id="sec-projectprogress" class="block-margin">
            <p class="heading-title header-margin">
                <?= Helper::checkLangEnglish($lang) ?
                'Project Progress' : 'Progress of the Project'; ?>
            </p>

            <div class="sort-container">
                <div class="sort-block">
                    <input type="hidden" name="sortSelect" value="">

                      <!--   <div class="sort" data-status="0">
                        <span> -->
                            <?php
                                /*
                                if(!empty($_GET['progress_month']) && $_GET['progress_month'] != 'all'){
                                    foreach($progress_groups as $progress_group) {
                                        $progressCondo = getProgressByProgressUpdateID($progress_group->progress_update_id);
                                        if($_GET['progress_month'] == $progressCondo->progress_update){
                                            ?>
                                            <?= Helper::checkLangEnglish($lang) ? Helper::DateEng($progressCondo->progress_update) : Helper::DateThai($progressCondo->progress_update) ?>
                                            <?php
                                        }
                                    }
                                }elseif(empty($_GET['progress_month']) || $_GET['progress_month'] == 'all'){
                                    echo Helper::checkLangEnglish($lang) ?
                                        'Choose All Month' : 'เลือกเดือนทั้งหมด';
                                }
                                */
                                ?>
                           <!--  </span> <i class="i-sort"></i>
                           </div> -->


                           <ul id="sortLists">
                            <li data-value=""
                            <?php
                            $url_project_progress_all = $router->generate('condominium-detail',[
                                'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                'lang'          =>  'th'
                            ])."?progress_month=all";
                            ?>
                            onclick="location.href='<?= $url_project_progress_all ?>'" >
                            <?= Helper::checkLangEnglish($lang) ?
                            'Choose All Month' : 'choose all the months'; ?>
                        </li>
                        <?php foreach ($progress_groups as $progress_group) {
                            $progressCondo = getProgressByProgressUpdateID($progress_group->progress_update_id);
                            $month_progress_update = $progressCondo->progress_update;
                            ?>
                            <li data-value="<?= $month_progress_update ?>"
                                <?php
                                $url_project_progress = $router->generate('condominium-detail',[
                                    'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                    'lang'          =>  'th'
                                ])."?progress_month=$month_progress_update";
                                ?>
                                onclick="location.href='<?= $url_project_progress ?>'" >
                                <?= Helper::checkLangEnglish($lang) ? Helper::DateEng($month_progress_update) : Helper::DateEng($month_progress_update) ?>
                            </li>
                            <?php
                        }?>
                    </ul>
                </div>
            </div>

            <?php
            if(!empty($_GET['progress_month'])){
                ?>
                <script>
                    $(document).ready(function () {
                        $('html, body').animate({
                            scrollTop: $('#projProgress').offset().top + 100
                        }, 1000);
                    });
                </script>
                <?php
            }
            ?>

            <div id="projProgress" class="project-progress-block">

                <?php
                $countProgress = 0;
                foreach ($progress_groups as $i => $progress_group) {
                    $progressCondo = getProgressByProgressUpdateID($progress_group->progress_update_id);

                    if( !empty($progress_group->progress_update_id) && ( empty($_GET['progress_month']) || $_GET['progress_month'] == 'all' || (!empty($_GET['progress_month']) && $_GET['progress_month'] == $progressCondo->progress_update )) )
                    {
                        $countProgress ++;
                        ?>
                        <div class="item">
                            <?php if ($i % 2 == 1) { ?>
                                <div class="prjprogress-list even">
                                    <div class="progress-detail">

                                        <?php
                                        $galleryProgress = getProgressByProgressUpdateMonth($project_sub->project_sub_id,$progressCondo->progress_update);
                                        foreach ( $galleryProgress as $index => $image){
                                            ?>
                                            <a class="prjprogress<?= $countProgress ?> banner-dktp" style="<?= $index!=0 ? 'display:none;' : '' ?>"
                                             href="<?= backend_url('base', $image->progress_update_img_name) ?>">
                                             <img src="<?= backend_url('base', $image->progress_update_img_name) ?>"
                                             alt="<?= Helper::checkLangEnglish($lang) ?
                                             $image->progress_update_dis_en : $image->progress_update_dis_th;?>">
                                         </a>
                                         <?php
                                     }
                                     ?>

                                     <p class="title"><?= Helper::checkLangEnglish($lang) ? Helper::DateEng($progressCondo->progress_update) : Helper::DateEng($progressCondo->progress_update) ?></p>
                                     <p class="subtitle green">
                                        <?= Helper::checkLangEnglish($lang) ?
                                        'Project Progress' : 'Progress'; ?>
                                        <?= str_replace('%','',$progressCondo->progress_update_mount) ?>%
                                    </p>
                                    <p><?= Helper::checkLangEnglish($lang) ?
                                    $progressCondo->progress_update_dis_en : $progressCondo->progress_update_dis_th;?></p>

                                    <?php
                                    $galleryProgress = getProgressByProgressUpdateMonth($project_sub->project_sub_id,$progressCondo->progress_update);
                                    foreach ( $galleryProgress as $index => $image){
                                        ?>
                                        <a class="prjprogress<?= $countProgress ?> banner-rsp" style="<?= $index!=0 ? 'display:none;' : '' ?>"
                                         href="<?= backend_url('base', $image->progress_update_img_name) ?>">
                                         <img src="<?= backend_url('base', $image->progress_update_img_name) ?>"
                                         alt="<?= Helper::checkLangEnglish($lang) ?
                                         $image->progress_update_dis_en : $image->progress_update_dis_th;?>">
                                     </a>
                                     <?php
                                 }
                                 ?>

                                 <script>
                                    $(document).ready(function () {
                                        $('.prjprogress<?= $countProgress ?>').featherlightGallery({
                                            afterContent: function() {
                                                this.$legend = this.$legend || $('<div class="legend"/>').insertAfter(this.$content);
                                                this.$legend.text(this.$currentTarget.find('img').attr('alt'));
                                            }
                                        });
                                    })
                                </script>

                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="prjprogress-list <?php
                        if ($i == 0) echo 'first';
                        else    echo '';
                        ?>">
                        <div class="progress-detail">
                            <p class="title"><?= Helper::checkLangEnglish($lang) ? Helper::DateEng($progressCondo->progress_update) : Helper::DateEng($progressCondo->progress_update) ?></p>
                            <p class="subtitle green">
                                <?= Helper::checkLangEnglish($lang) ?
                                'Project Progress' : 'Progress'; ?>
                                <?= str_replace('%','',$progressCondo->progress_update_mount) ?>%
                            </p>
                            <p><?= Helper::checkLangEnglish($lang) ?
                            $progressCondo->progress_update_dis_en : $progressCondo->progress_update_dis_th;?></p>
                            <?php
                            $galleryProgress_first = getProgressByProgressUpdateMonth($project_sub->project_sub_id,$progressCondo->progress_update);
                            foreach ( $galleryProgress_first as $index => $image){
                                ?>
                                <a class="prjprogress<?= $countProgress ?>" style="<?= $index!=0 ? 'display:none;' : '' ?>"
                                 href="<?= backend_url('base', $image->progress_update_img_name) ?>">
                                 <img src="<?= backend_url('base', $image->progress_update_img_name) ?>"
                                 alt="<?= Helper::checkLangEnglish($lang) ?
                                 $image->progress_update_dis_en : $image->progress_update_dis_th;?>">
                             </a>
                             <?php
                         }
                         ?>

                         <script>
                            $(document).ready(function () {
                                $('.prjprogress<?= $countProgress ?>').featherlightGallery({
                                    afterContent: function(aaa) {
                                        this.$legend = this.$legend || $('<div class="legend"/>').insertAfter(this.$content);
                                        this.$legend.text(this.$currentTarget.find('img').attr('alt'));
                                    }
                                });
                            })
                        </script>


                    </div>
                </div>
            <?php } ?>
        </div>
        <?php
    }
}
?>



<!--                    <div class="item">-->
    <!--                        <div class="prjprogress-list first">-->
        <!--                            <div class="progress-detail">-->
            <!--                                <p class="title">ก.พ. 2559</p>-->
            <!--                                <p class="subtitle green">ความคืบหน้า 100%</p>-->
            <!--                                <p>ภาพถ่ายโครงการเสร็จสมบูรณ์</p>-->
            <!---->
            <!--                                <a class="prjprogress" href="images/temp2/projectinfo_condo11.jpg">-->
                <!--                                    <img src="images/temp2/projectinfo_condo11.jpg" alt="">-->
                <!--                                </a>-->
                <!--                            </div>-->
                <!--                        </div>-->
                <!--                    </div>-->

            </div>
            <input type="hidden" id="countProgress" value="<?= $countProgress ?>">
        </div>
    <?php } ?>


    <?php if($project->project_status != 'SO'){ ?>

        <?php
        $project_contact    = getProjectContactByProjectID($project_id);
        $contact_day        = [
            'all'   =>  (Helper::checkLangEnglish($lang) ? 'all day' : 'every day'),
            '0'     =>  (Helper::checkLangEnglish($lang) ? 'week day' : 'official holidays'),
            '1'     =>  (Helper::checkLangEnglish($lang) ? 'monday' : 'monday'),
            '2'     =>  (Helper::checkLangEnglish($lang) ? 'tuesday' : 'tuesday'),
            '3'     =>  (Helper::checkLangEnglish($lang) ? 'wednesday' : 'wednesday'),
            '4'     =>  (Helper::checkLangEnglish($lang) ? 'thursday' : 'thursday'),
            '5'     =>  (Helper::checkLangEnglish($lang) ? 'friday' : 'friday'),
            '6'     =>  (Helper::checkLangEnglish($lang) ? 'saturday' : 'saturday'),
            '7'     =>  (Helper::checkLangEnglish($lang) ? 'sunday' : 'sunday'),
        ];
        $day_str  = (Helper::checkLangEnglish($lang) ? 'daily' : 'daily');
        $start_time_contact     = '';
        $end_time_contact       = '';
        $tell_contact           = '';
        $email_contact          = '';
        $arr_days = [];
        if($project_contact != false){
            $open_day = str_replace(' ','',$project_contact->open_day);
            if (strpos($open_day, ',') !== false) {
                $arr_days = explode(',',$open_day);
                if(count($arr_days) > 0 ){
                    $day_str .= (Helper::checkLangEnglish($lang) ? ' except ' : ' except ');
                }
                foreach ($arr_days as $i => $arr_day){
                    $day_str .= ' '.$contact_day[(string)$arr_day];
                    if(count($arr_days) != $i+1 ){
                        $day_str .= ',';
                    }
                }
            }
            $start_time_contact     = $project_contact->open_time_start;
            $end_time_contact       = $project_contact->open_time_end;
            $tell_contact           = $project_contact->telephone;
            $email_contact          = $project_contact->email;
        }

        ?>

        <div id="sec-contact" class="block-margin">
            <p class="heading-title header-margin">Contact & Appointment</p>

            <?php if($project_contact != false){
                ?>
                <span> <?= (Helper::checkLangEnglish($lang) ? 'Sales Office operating hours' : 'Contact the Sales office') ?>
                <?= (Helper::checkLangEnglish($lang) ? '' : '') ?>
                <?= $start_time_contact ?> - <?= $end_time_contact ?> <?= (Helper::checkLangEnglish($lang) ? '' : '') ?>
                <?= $day_str ?> <?= (Helper::checkLangEnglish($lang) ? 'Call' : 'Tel.') ?>
                <?= $tell_contact ?> Email: <?= $email_contact ?>

                <!-- <a href="http://edm.lh.co.th/Support/contact-form/new.contact-project-detail.php?project_id=<?//=$edm->project_type;?><?=$edm->project_edm_id;?>&brand_id=&website=LHWeb&mem_id=" target="_blank" data-family="child"><?//=(Helper::checkLangEnglish($lang) ? 'Please constant & appointment Click here' : 'สอบถามข้อมูลโครงการเพิ่มเติม คลิก'); ?></a> -->
            </span>
            <?php
        }?>

        <!-- contacform -->
        <?php include 'template-part/contact_form.php' ?>
        <!-- END  contacform-->

        <!-- edm send form -->
        <script src="<?=file_path('js/cal_reg/xmlTojson.js') ?>" type="text/javascript" ></script>
        <script src="<?=file_path('js/edm_send_form.js') ?>" type="text/javascript" ></script>
        <!-- END -->



    <?php } ?>

</div>
</div>

<div style="color: #999999;font-size: 15px;" class="container block-margin">
    <?php
    if($project_concept != false) {
        $information = Helper::checkLangEnglish($lang) ? $project_concept->information_en : $project_concept->information_th;
        echo $information;
    }
    ?>
</div>



<?php include('footer.php'); ?>
<script>
 $('#popappoint').css('display','block');

</script>
