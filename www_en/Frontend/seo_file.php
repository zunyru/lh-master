<?php
$seo = get_Mata_Title('home');
//var_dump($seo);
$title = $seo->title;
$description = $seo->description;
$keywords = 'บ้านเดี่ยว, ทาวน์โฮม, คอนโด, คอนโดมิเนียม, โครงการบ้านใหม่';
$url         = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$url_cut     = (substr($url, 0, strpos($url, "?"))==''?$url:substr($url, 0, strpos($url, "?")));
$banners        = getBannerByTypePage(1);
$image = backend_url('base', $banners[0]->lead_path) ;
$image = str_replace('fileupload/banner_file/','fileupload/banner_file/Thumbnails_',$image);

   //check page
if(isset($pages)){
 if($pages == 'LH_LANDING_PAGE'){
  $title = $landing_page->landing_page_name_th;
}elseif ($pages == 'LH_LIVING_TIPS') {
  $title = $title_page;
  $description = $description_page;
  $image = str_replace('fileupload/images/living_tips/','fileupload/images/living_tips/Thumbnails_',$image_page);
}elseif ($pages == 'LH_PROJECT_REVIEW') {
  $title = $title_page;
  $description = $description_page;
  $image = str_replace('fileupload/images/project_review/','fileupload/images/project_review/Thumbnails_',$image_page);
}else{
  $title = $title_page;
  $description = $description_page;
}
}

 //var_dump($mata_seo);
if(isset($mata_seo->title_seo)){
 $title = $mata_seo->title_seo;
}
if(isset($mata_seo->description_seo)){
 $description = $mata_seo->description_seo;
}
if(isset($mata_seo->keyword_seo)){
 $keywords = $mata_seo->keyword_seo;
}
if(isset($mata_seo->thumnail_seo)){
 $image = backend_url('base','').$mata_seo->thumnail_seo;
}

?>

<!-- for Google SEO-->
<title><?=$title;?></title>
<meta name     ="description"         content="<?=$description;?>" />
<meta name     ="keywords"            content="<?=$keywords;?>" />

<!-- for Facebook SEO -->
<meta property ="fb:app_id"           content="<?= $app_id;?>" /> 
<meta property ="og:type"             content="website" /> 
<meta property ="og:url"              content="<?= $url_cut;?>" /> 
<meta property ="og:title"            content="<?= $title;?>" />
<meta property ="og:description"      content="<?= $description; ?>" />
<meta property ="og:site_name"        content="<?= $title;?>" /> 
<meta property ="og:image"            content="<?= $image; ?>" />
<meta property ="og:image:type"       content="image/jpeg" />

<meta property="og:image:width"       content="600" />
<!-- <meta property="og:image:height"      content="315" /> -->
<meta property="og:image:alt"         content="<?=$title;?>" /> 

<!-- for Twitter title-->
<meta name="twitter:card"           content="summary_large_image" />
<meta name="twitter:title"          content="<?= $title;?>" />
<meta name="twitter:description"    content="<?= $description; ?>" />
<meta name="twitter:creator"        content="@lhhome">
<meta name="twitter:site"           content="<?= $url;?>">
<meta name="twitter:image"          content="<?= $image; ?>" />
<meta itemprop="image"              content="<?= $image ?>" /> 