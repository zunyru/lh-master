<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>
<?php include('header.php'); ?>
    <!-- CSS -->
<link rel="stylesheet" href="<?= file_path('css/complain.css') ?>" type="text/css">

    <div class="page-banner-md banner-hide">
        <div id="bannerSlide">
            <div class="item">
                <div class="banner-parallax">
                    <img src="<?= file_path('images/temp2/banner_complain.jpg') ?>" alt="">
                </div>
            </div>
        </div>
    </div>

    <div id="content" class="content rps-content-pd">
        <div class="container">
            <h1 class="heading-title">ร้องเรียนเรื่องบ้านและคอนโด</h1>

            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div>
<!--                       <p class="title banner-hide">คุณสามารถค้นหาชื่อโครงการที่คุณอาศัย และส่งข้อความร้องเรียนบ้านและคอนโดมาที่ แลนด์ แอนด์เฮ้าส์</p>-->
                        <div class="complain-form-block">
                            <form action="" class="complain-form form-container">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="step-label">
                                            step 2
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <p class="title">ผลการค้นหา</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-11">
                                        <div class="form">
                                            <p class="pmargin">คลิกที่ชื่อหมู่บ้านหรืออาคารชุดที่ต้องการเพื่อร้องเรียนหรือเสนอแนะ</p>

                                            <table class="project-search-list" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th class="tb-no">ลำดับ</th>
                                                        <th>ชื่อโครงการ</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="tb-no">1</td>
                                                        <td><a href="<?= $router->generate('complain-step',['step' => 3]) ?>">ชัยพฤกษ์ ขอนแก่น</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tb-no">2</td>
                                                        <td><a href="<?= $router->generate('complain-step',['step' => 3]) ?>">ชัยพฤกษ์ จตุโชติ - วัชรพล</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tb-no">3</td>
                                                        <td><a href="<?= $router->generate('complain-step',['step' => 3]) ?>">ชัยพฤกษ์ ตลิ่งชัน</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tb-no">4</td>
                                                        <td><a href="<?= $router->generate('complain-step',['step' => 3]) ?>">ชัยพฤกษ์ ติวานนท์ - วงแหวน (โครงการ 1)</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tb-no">5</td>
                                                        <td><a href="<?= $router->generate('complain-step',['step' => 3]) ?>">ชัยพฤกษ์ ติวานนท์ - วงแหวน (โครงการ 2)</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>

<!--                                            <a href="" class="btn btn-submit next">Next</a>-->
                                        </div>
                                    </div>
                                </div>

                            </form>

                            <a href="<?= $router->generate('complain-step',['step' => 1]) ?>" class="step-back">< Back</a>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


<?php include('footer.php'); ?>
