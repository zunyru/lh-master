<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>
<?php include('header.php'); ?>
    <!-- CSS -->
    <link rel="stylesheet" href="css/all-home-series-detail.css" type="text/css">
    <link rel="stylesheet" href="tablesaw/tablesaw.css" type="text/css">
    <!-- JS -->
    <script src="tablesaw/tablesaw.jquery.js"></script>
    <script src="tablesaw/tablesaw-init.js"></script>
    <script src="js/all-home-series-detail.js"></script>


    <div class="page-banner page-scroll">
        <span id="scrollNext" class="i-scroll-next"></span>

        <div id="bannerSlide">
            <div class="item">
                <div class="banner-parallax">
                    <img src="images/temp2/banner_all_home_detail.jpg" alt="">
                </div>
            </div>
            <div class="item">
                <div class="banner-parallax">
                    <img src="images/temp2/banner_all_home_detail.jpg" alt="">
                </div>
            </div>
        </div>
    </div>

    <div id="content" class="content project-info-sale-page">
        <div class="container">
            <div class="project-info-detail-block">
                <div class="row">
                    <div class="col-md-12">
                        <div class="info-logo-img">
                            <img src="images/logo/logo_chaiyp.jpg" alt="">
                            <p class="heading-title">แบบบ้าน WALNUT</p>
                        </div>

                    </div>

                    <div class="col-md-6">
                        <div class="content-style">
                            <p class="title grey">โครงการ  บ้านชัยพฤกษ์ ปิ่นเกล้า-กาญจนาฯ</p>
                            <p class="title grey mbr-15">ทำเล ราชพฤกษ์-ปิ่นเกล้า</p>
                            <div class="row">
                                <div class="tb-2col">
                                    <div class="col-md-6 cl">
                                        <table class="content-style tbs1" id="project-table" width="100%">
                                            <tr>
                                                <td>พื้นที่ใช้สอย</td>
                                                <td class="txt-right">136</td>
                                                <td class="txt-right">ตร.ม</td>
                                            </tr>
                                            <tr>
                                                <td>ห้องนอน</td>
                                                <td class="txt-right">62</td>
                                                <td class="txt-right">ห้อง</td>
                                            </tr>
                                            <tr>
                                                <td>ห้องน้ำ</td>
                                                <td class="txt-right">3</td>
                                                <td class="txt-right">ห้อง</td>
                                            </tr>
                                            <tr>
                                                <td>ที่จอดรถ</td>
                                                <td class="txt-right">3</td>
                                                <td class="txt-right">ห้อง</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-6 cr">
                                        <table class="content-style tbs1" id="project-table" width="100%">
                                            <tr>
                                                <td>ขนาดที่ดิน</td>
                                                <td class="txt-right">50</td>
                                                <td class="txt-right">ตร.ว</td>
                                            </tr>
                                            <tr>
                                                <td>ห้องนั่งเล่น</td>
                                                <td class="txt-right">-</td>
                                                <td class="txt-right">ห้อง</td>
                                            </tr>
                                            <tr>
                                                <td>ห้องทำงาน</td>
                                                <td class="txt-right">-</td>
                                                <td class="txt-right">ห้อง</td>
                                            </tr>
                                            <tr>
                                                <td>ห้องคนรับใช้</td>
                                                <td class="txt-right">-</td>
                                                <td class="txt-right">ห้อง</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <p class="title grey mtr-15">รายละเอียดอื่นๆ</p>
                        <div class="content-style pd37">

                            <div class="row">
                                <div class="col-md-6">
                                    <ul class="tbs1">
                                        <li>ตกแต่งสวนภายนอกบ้าน</li>
                                        <li>ติดตั้งเครื่องปรับอากาศ</li>
                                        <li>ครัวไทย</li>
                                        <li>ตกแต่งเฟอร์นิเจอร์</li>
                                    </ul>
                                </div>

                                <div class="col-md-6">
                                    <ul class="tbs1">
                                        <li>ตกแต่งห้องน้ำพร้อมสุขภัณฑ์</li>
                                        <li>แทงค์น้ำ / ปั๊มน้ำ</li>
                                        <li>ตกแต่งพื้นภายใน</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="gallery-block block-margin">
                <p class="heading-title header-margin">แกลอรี</p>
                <div class="tpl5-img">
                    <div id="tpl5-img">
                        <div class="item tpl5-img-1">
                            <a class="gallery" href="images/temp2/all_home_detail2.jpg">
                                <img src="images/temp2/all_home_detail2.jpg" alt="">
                                <p class="title-gallery">ห้องน้ำขนาดใหญ่</p>
                            </a>
                        </div>
                        <div class="item tpl5-img-2">
                            <a class="gallery" href="images/temp2/all_home_detail3.jpg">
                                <img src="images/temp2/all_home_detail3.jpg" alt="">
                                <p class="title-gallery">ห้องครัวขนาดใหญ่</p>
                            </a>
                        </div>
                        <div class="item tpl5-img-3">
                            <a class="gallery" href="images/temp2/all_home_detail4.jpg">
                                <img src="images/temp2/all_home_detail4.jpg" alt="">
                                <p class="title-gallery">ห้องนอน</p>
                            </a>
                        </div>
                        <div class="item tpl5-img-4">
                            <a class="gallery" href="images/temp2/all_home_detail5.jpg">
                                <img src="images/temp2/all_home_detail5.jpg" alt="">
                                <p class="title-gallery">ห้องนอน</p>
                            </a>
                        </div>
                        <div class="item tpl5-img-5">
                            <a class="gallery" href="images/temp2/all_home_detail6.jpg">
                                <img src="images/temp2/all_home_detail6.jpg" alt="">
                                <p class="title-gallery">ห้องนอน</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="project-info-detail-block">
                <p class="heading-title header-margin">แบบแปลนบ้าน</p>

                <div id="homePlanSlide">
                    <div class="item">
                        <div class="row">
                            <div class="col-md-6">
                                <a href="#" data-featherlight="images/temp/proj_info_furnished_img1.jpg">
                                    <img src="images/temp/proj_info_furnished_img1.jpg" alt="">
                                    <span class="i-zoom"></span>
                                </a>
                            </div>
                            <div class="col-md-6">
                                <div class="tab-dd dropdown">
                                    <div class="row">
                                        <ul class="tabs-menu-plan-slide dropdown-menu" aria-labelledby="dropdownMenu1">
                                            <li class="current">
                                                <a data-value="0">
                                                    ชั้นหนึ่ง
                                                </a>
                                            </li>
                                            <li class="">
                                                <a data-value="1">
                                                    ชั้นสอง
                                                </a>
                                            </li>
                                            <li class="">
                                                <a data-value="2">
                                                    ชั้นสาม
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab">
                                    <div id="tab-1" class="tab-content">
                                        <div class="tab-block content-style">
                                            <ul>
                                                <li>พื้นที่รับแขก และพื้นที่รับประทานอาหาร รายล้อมด้วยวิวสวน
                                                    กว้างขวางเป็นพิเศษพร้อม BAY WINDOW ขยายมุมมองกว้างขึ้น
                                                </li>
                                                <li>บานประตูและบานหน้าต่าง ขนาดกว้างใหญ่
                                                    เปิดเชื่อมพื้นที่สวนข้างบ้านเพิ่มอารมณ์
                                                </li>
                                                <li>พื้นที่ส่วนเตรียมอาหาร และห้องครวัไทยแยกส่วน จัดเตรียมเคาน์เตอร์
                                                    BUILT-IN ไว้อย่างลงตัว
                                                </li>
                                            </ul>
                                            <ul>
                                                หมายเหตุ: ตัวเลขขนาดในแปลนเป็นระยะโดยประมาณ รายละเอียดของแปลนและแบบบ้านนี้เพื่อเป็นแนวทางพิจารณา ท่านสามารถดูรายละเอียดของบ้านหลังจริง ณ โครงการ บริษัทฯ ขอสวนสิทธิ์ในการเปลี่ยนแปลงรายการวัสดุและรายละเอียดโดยไม่ต้องแจ้งให้ทราบล่วงหน้า.
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-md-6">
                                <a href="#" data-featherlight="images/temp/proj_info_furnished_img1.jpg">
                                    <img src="images/temp/proj_info_furnished_img1.jpg" alt="">
                                    <span class="i-zoom"></span>
                                </a>
                            </div>
                            <div class="col-md-6">
                                <div id="tabs-container" class="">
                                    <div class="tab-dd dropdown">
                                        <div class="row">
                                            <div class="">
                                                <ul class="tabs-menu-plan-slide dropdown-menu" aria-labelledby="dropdownMenu1">
                                                    <li class="">
                                                        <a data-value="0">
                                                            ชั้นหนึ่ง
                                                        </a>
                                                    </li>
                                                    <li class="current">
                                                        <a data-value="1">
                                                            ชั้นสอง
                                                        </a>
                                                    </li>
                                                    <li class="">
                                                        <a data-value="2">
                                                            ชั้นสาม
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab">
                                        <div id="tab-1" class="tab-content">
                                            <div class="tab-block content-style">
                                                <ul>
                                                    <li>พื้นที่รับแขก และพื้นที่รับประทานอาหาร รายล้อมด้วยวิวสวน
                                                        กว้างขวางเป็นพิเศษพร้อม BAY WINDOW ขยายมุมมองกว้างขึ้น
                                                    </li>
                                                    <li>บานประตูและบานหน้าต่าง ขนาดกว้างใหญ่
                                                        เปิดเชื่อมพื้นที่สวนข้างบ้านเพิ่มอารมณ์
                                                    </li>
                                                </ul>
                                                <ul>
                                                    หมายเหตุ: ตัวเลขขนาดในแปลนเป็นระยะโดยประมาณ รายละเอียดของแปลนและแบบบ้านนี้เพื่อเป็นแนวทางพิจารณา ท่านสามารถดูรายละเอียดของบ้านหลังจริง ณ โครงการ บริษัทฯ ขอสวนสิทธิ์ในการเปลี่ยนแปลงรายการวัสดุและรายละเอียดโดยไม่ต้องแจ้งให้ทราบล่วงหน้า.
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-md-6">
                                <a href="#" data-featherlight="images/temp/proj_info_furnished_img1.jpg">
                                    <img src="images/temp/proj_info_furnished_img1.jpg" alt="">
                                    <span class="i-zoom"></span>
                                </a>
                            </div>
                            <div class="col-md-6">
                                <div id="tabs-container" class="">
                                    <div class="tab-dd dropdown">
                                        <div class="row">
                                            <div class="">
                                                <ul class="tabs-menu-plan-slide dropdown-menu" aria-labelledby="dropdownMenu1">
                                                    <li class="">
                                                        <a data-value="0">
                                                            ชั้นหนึ่ง
                                                        </a>
                                                    </li>
                                                    <li class="">
                                                        <a data-value="1">
                                                            ชั้นสอง
                                                        </a>
                                                    </li>
                                                    <li class="current">
                                                        <a data-value="2">
                                                            ชั้นสาม
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab">
                                        <div id="tab-1" class="tab-content">
                                            <div class="tab-block content-style">
                                                <ul>
                                                    <li>พื้นที่รับแขก และพื้นที่รับประทานอาหาร รายล้อมด้วยวิวสวน
                                                        กว้างขวางเป็นพิเศษพร้อม BAY WINDOW ขยายมุมมองกว้างขึ้น
                                                    </li>
                                                </ul>
                                                <ul>
                                                    หมายเหตุ: ตัวเลขขนาดในแปลนเป็นระยะโดยประมาณ รายละเอียดของแปลนและแบบบ้านนี้เพื่อเป็นแนวทางพิจารณา ท่านสามารถดูรายละเอียดของบ้านหลังจริง ณ โครงการ บริษัทฯ ขอสวนสิทธิ์ในการเปลี่ยนแปลงรายการวัสดุและรายละเอียดโดยไม่ต้องแจ้งให้ทราบล่วงหน้า.
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="project-info-detail-block">
                <p class="heading-title">ตารางและเงื่อนไขพอสังเขป</p>
                <table class="tablesaw content-style table-1" data-tablesaw-mode="stack">
                    <thead>
                    <tr>
                        <th class="tb-title-1st" data-tablesaw-sortable-col>แบบบ้าน</th>
                        <th data-tablesaw-sortable-col>ขนาดที่ดิน</th>
                        <th data-tablesaw-sortable-col>ราคา</th>
                        <th data-tablesaw-sortable-col>ดาวน์ 10%</th>
                        <th data-tablesaw-sortable-col>เงินจอง</th>
                        <th data-tablesaw-sortable-col>ทำสัญญา</th>
                        <th data-tablesaw-sortable-col>เงินโอน</th>
                        <th data-tablesaw-sortable-col>ผ่อนเริ่มต้น/เดือน</th>
                        <th data-tablesaw-sortable-col>ดอกเบี้ย%</th>
                        <th data-tablesaw-sortable-col>ปี</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="tb-plan-detail">
                            WALNUT Private Zone ที่ดิน 50 -53 ตร.ว. ราคาเดียว
                            มาก่อนได้สิทธิ์ก่อน คัดพิเศษสำหรับงาน 2 Days Special เท่านั้น
                        </td>
                        <td>50</td>
                        <td>4,290,000</td>
                        <td>429,000</td>
                        <td>50,000</td>
                        <td>379,000</td>
                        <td>3,810,000</td>
                        <td>15,500</td>
                        <td>4.00</td>
                        <td>40</td>
                    </tr>
                    </tbody>
                </table>
                <p class="txt-remark">***บริษัทฯ ขอสงวนสิทธิ์ในการเปลี่ยนแปลงรายละเอียดดังกล่าว โดยมิต้องแจ้งให้ทราบล่วงหน้า</p>

            </div>


        </div>
    </div>


<?php include('footer.php'); ?>