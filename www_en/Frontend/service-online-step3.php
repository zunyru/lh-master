<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>
<?php include('header.php'); ?>
<!-- CSS -->
<link rel="stylesheet" href="<?= file_path('css/service-online.css') ?>" type="text/css">
<!-- JS -->
<!--    <script src="js/service-online.js"></script>-->

<div id="content" class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-11 col-md-offset-1">
                <div class="page-nav">
                    <a href="" class="back-to-prev">แจ้งซ่อมออนไลน์</a>
                    <h1 class="heading-title">ประวัติการแจ้งซ่อม</h1>
                </div>

                <div class="service-history-block">
                    <table class="tb-history" width="100%">
                        <thead>
                        <tr>
                            <th>โครงการ</th>
                            <th>บ้านเลขที่</th>
                            <th>วันที่แจ้งซ่อม</th>
                            <th>วันที่นัดตรวจ</th>
                            <th>สถานะ</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>นันทวัน - เชียงใหม่</td>
                            <td>176/33</td>
                            <td><a href="" class="lnk-detail">9/02/2016 11:38 น.</a></td>
                            <td>รอนัดหมาย</td>
                            <td>ยกเลิกรายการแล้ว</td>
                        </tr>
                        <tr>
                            <td>นันทวัน - เชียงใหม่</td>
                            <td>176/33</td>
                            <td><a href="" class="lnk-detail">9/02/2016 11:38 น.</a></td>
                            <td>16/02/2017</td>
                            <td>ดำเนินการแก้ไขเรียบร้อย</td>
                        </tr>
                        <tr>
                            <td>นันทวัน - เชียงใหม่</td>
                            <td>176/33</td>
                            <td><a href="" class="lnk-detail">9/02/2016 11:38 น.</a></td>
                            <td>รอนัดหมาย</td>
                            <td>รอดำเนินการแก้ไข</td>
                        </tr>
                        <tr>
                            <td>นันทวัน - เชียงใหม่</td>
                            <td>176/33</td>
                            <td><a href="" class="lnk-detail">9/02/2016 11:38 น.</a></td>
                            <td>16/02/2017</td>
                            <td>ยกเลิกรายการแล้ว</td>
                        </tr>
                        <tr>
                            <td>นันทวัน - เชียงใหม่</td>
                            <td>176/33</td>
                            <td><a href="" class="lnk-detail">9/02/2016 11:38 น.</a></td>
                            <td>รอนัดหมาย</td>
                            <td>ดำเนินการแก้ไขเรียบร้อย</td>
                        </tr>
                        <tr>
                            <td>นันทวัน - เชียงใหม่</td>
                            <td>176/33</td>
                            <td><a href="" class="lnk-detail">9/02/2016 11:38 น.</a></td>
                            <td>16/02/2017</td>
                            <td>รอดำเนินการแก้ไข</td>
                        </tr>
                        <tr>
                            <td>นันทวัน - เชียงใหม่</td>
                            <td>176/33</td>
                            <td><a href="" class="lnk-detail">9/02/2016 11:38 น.</a></td>
                            <td>รอนัดหมาย</td>
                            <td>รอดำเนินการแก้ไข</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <a href="" class="step-back">< Back</a>
            </div>
        </div>


    </div>
</div>


<?php include('footer.php'); ?>
