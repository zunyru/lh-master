<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>


<!--       <script src='https://www.google.com/recaptcha/api.js?hl=th'></script> -->
    <?php
    $page = 'contact-us';
    $page_index = 25;
    
    include('header.php');
    //elementMetaTitleDisTag($page);
    ?>
<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '342952452528044'); // Insert your pixel ID here.
    fbq('track', 'PageView');
    fbq('track', 'Lead');
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=342952452528044&ev=PageView&noscript=1"
    /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

<style>
    .file-caption-main .btn-file {
        overflow: visible;
    }

    .file-caption-main .btn-file {
        color: red;
        position: absolute;
        bottom: -32px;
        right: 30px;
    }
</style>
    <!-- CSS -->
    <link rel="stylesheet" href="<?= file_path('css/contact.css') ?>" type="text/css">
     <link rel="stylesheet" href="<?= file_path('js/cal_reg/css/sweetalert.css') ?>">
    <!-- JS -->
<!--    <script src="js/contact.js"></script>-->
    <script src="<?= file_path('js/banner-md.js') ?>"></script>
    <script src="<?= file_path('js/cal_reg/sweetalert.min.js') ?>"></script>

<!-- send -->
    <?php   
      if(isset($_SESSION['send'])){
         $_SESSION['send'];
        if($_SESSION['send']==1){

    ?>
       <script type="text/javascript">
           swal({
              title: "ส่งเรียบร้อยแล้ว! <br> ขอบคุณค่ะ",
              type: "success",
              html: true
            });
       </script>

    <?php
        }else{
    ?>
        <script type="text/javascript">
           swal({
              title: "ผิดพลาด ! <br> เกิดข้อผิดพลาดระหว่างดำเนินการส่ง",
              type: "error",
              html: true
            });
       </script>

    <?php
        }

        session_unset($_SESSION['send']);

    } ?>

    <div class="page-banner-md banner-hide">
        <div id="bannerSlide">
            <div class="item">
                <div class="banner-parallax">
                	 <img src="<?= file_path('images/temp2/banner_contact.jpg') ?>" alt="">
                </div>
            </div>
        </div>
    </div>

<div id="content" class="content contact-page">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div>
                    
                    <?php
                    $SEO = getSEOUrl($actual_link);
                    if($actual_link == @$SEO->url_page){
                        echo '<h1 class="heading-title">'.$SEO->h1.'<h1>';
                    }else{
                       echo '<h1 class="heading-title">contact for information</h1>'; 
                   }
                   ?>
                   <p class="txt-22"><b>Please fill in the information you would like to contact us.
                    <a href="http://www7.lh.co.th/LHComplaint/index.jsp">
                        <strong>(make a complaint about your home, click here)</strong></a></b></p>
                        <form action="mail-contactus" class="contact-form form-container" method="POST" id="commentForm" >
                            <div class="">
                               <!--  <div class="form-group">
                                    <input type="text" name="mailto" value="info@lh.co.th" class="form-control" placeholder="info@lh.co.th" readonly>
                                </div> -->
                                <div class="form-group">
                                    <input type="text" name="fullname" value="" class="form-control" placeholder="*Name and Surname" required>
                                </div>


                                <div class="form-group">
                                    <input type="email" name="emailre" id="emailre" class="form-control" placeholder="*Email" required>
                                     <!-- <span class="required">*</span> -->
                                </div>

                                <div class="form-group">
                                    <input type="tel" name="phonere" id="phonere" class="form-control" placeholder="*Telephone number" min="0" required>
                                    <!-- <span class="required">*</span> -->
                                </div>
                            
                                <div class="form-group">
                                    <textarea name="text" placeholder="*Specify the required information." id="" class="form-control" cols="30" rows="10" required></textarea>
                                </div>


                                <div class="g-recaptcha" data-sitekey="6LcSzSEUAAAAAPTUl4maci5Pl4SyWVMmnNhly0Qw"></div>
                                <span id="chacheck1" style="display: none; color: red;">to prove that you are not a robot !</span>
                                <div class="button-contact">
                                	<button type="submit" class="btn btn-submit btn-contact" id="btn_submit">sending information</button>
                                	<button type="reset" class="btn btn-submit btn-contact">Canceled</button>
								</div>

                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>
 <!-- Validate -->
    <script src="<?= file_path('../build/js/jquery.validate.js') ?>" /></script>
    <script>

        $(document).ready(function() {

            $("#commentForm").validate({
                rules: {
                    fullname: "required",
                    phonere: "required",
                    emailre: "required",
                    text : "required",
                    // project_review_dis_th: "required",
                    // community_img : "required",
                    // living_tip_content_th : "required",
                    // fileupload_en : "required",
                },
                messages: {
                    fullname: "Please enter your Name and Surname.!",
                    phonere : "Please enter your  Telephone number !",
                    emailre : "Please enter your Email !",
                    text : "Please enter Specify the required information !",
                    // project_review_dis_th : "กรุณากรอกรายละเอียด Project Review ไทย !",
                    // community_img : " &nbsp; กรุณาเลือกรูป !",
                    // living_tip_content_th : "กรุณากรอกรายละเอียด Living Tips !",
                    // fileupload_en : " &nbsp; กรุณาเลือกรูป",
                }
            });

            // $("#lead_img_file").rules("add", {
            //     required:true,
            //     messages: {
            //         required: " &nbsp; ไม่มีรูป !"
            //     }
            // });


            $('#phonere, #telradio').keydown(function(event){
            	var max = 10;
            	var phone = $(this).val().length;

            	if(phone >= max){

            		if(event.keyCode == 8 || event.keyCode == 46){

 						return true;

 					}

            			return false;

            	}
            });


        $('#commentForm').on('submit', function(){
        var googleResponse1 = $('#g-recaptcha-response-1').val();
        if(googleResponse1 == ''){

           // $("#btn_submit").attr('disabled',true);
            $('#chacheck1').show();
            return false;

        }else{

            //$("#btn_submit").attr('disabled',false);
            $('#chacheck1').hide();
        }


        })


        });


    </script>

<?php include('footer.php'); ?>
