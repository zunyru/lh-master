<?php


function getListSlugURL()
{
    try {
        $sql = "SELECT slug.slug_id,slug.slug_name,slug.menu_name,slug.file_name 
                FROM LH_SLUG_URL slug";
        $result   = mssql_query($sql , $GLOBALS['db_conn']);
        $urlLists = Helper::toArrayList($result);
        $slugObj  = [];
        foreach ($urlLists as $urlList){
            $slugObj[$urlList->slug_id] = [
                'file'  =>  $urlList->file_name,
                'slug'  =>  $urlList->slug_name,
            ];
        }
        return $slugObj;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}