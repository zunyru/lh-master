<?php

require_once dirname(dirname(__FILE__)).'/db/dbCon_mssql.php';
require_once 'kint-master/Kint.class.php';
require_once 'Helper.php';

function getProjectIdByType($type_id) {
    try {
        $sql = "SELECT DISTINCT P.project_id
                FROM LH_PROJECTS P
                JOIN LH_PROJECT_SUB PS ON P.project_id = PS.project_id
                JOIN LH_PRODUCTS PD ON PS.product_id = PD.product_id
                WHERE PD.product_id = '".$type_id."'
                AND CONVERT(date, getdate()) >= P.public_date AND P.project_data_status = 'PB' ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $project_ids = [];
        while ($row = mssql_fetch_object($result)) {
            $project_ids[] = $row->project_id;
        }
        return $project_ids;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectIDByProductView($product_view_id)
{
    try {
        $sql = "SELECT DISTINCT P.project_id,BR.order_seq
                FROM LH_PROJECTS P
                JOIN LH_PROJECT_SUB PS ON P.project_id = PS.project_id
                JOIN LH_PRODUCTS PD ON PS.product_id = PD.product_id
				LEFT JOIN LH_PROJECT_VIEW lh_proj_view ON lh_proj_view.project_id = P.project_id
                LEFT JOIN LH_BRANDS BR ON BR.brand_id = P.brand_id 
                WHERE lh_proj_view.product_view_id = '".$product_view_id."'
                AND CONVERT(date, getdate()) >= P.public_date AND P.project_status != 'SO' AND P.project_data_status = 'PB'
                ORDER BY BR.order_seq ASC ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $project_ids = [];
        while ($row = mssql_fetch_object($result)) {
            $project_ids[] = $row->project_id;
        }
        return $project_ids;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectIDByProductViewZone($product_view_id,$zone_id)
{
    try {
    $sql = "SELECT DISTINCT P.project_id,BR.order_seq
                FROM LH_PROJECTS P
                JOIN LH_PROJECT_SUB PS ON P.project_id = PS.project_id
                JOIN LH_PRODUCTS PD ON PS.product_id = PD.product_id
                        LEFT JOIN LH_PROJECT_VIEW lh_proj_view ON lh_proj_view.project_id = P.project_id
                LEFT JOIN LH_BRANDS BR ON BR.brand_id = P.brand_id 
                LEFT JOIN LH_PROJECT_ZONE Z ON Z.project_id = P.project_id
                WHERE lh_proj_view.product_view_id = '".$product_view_id."' AND Z.zone_id = '".$zone_id."'
                AND CONVERT(date, getdate()) >= P.public_date AND P.project_status != 'SO' AND P.project_data_status = 'PB'
                ORDER BY BR.order_seq ASC ";
    $result = mssql_query($sql , $GLOBALS['db_conn']);            
    $project_ids = [];
        while ($row = mssql_fetch_object($result)) {
            $project_ids[] = $row->project_id;
        }
        return $project_ids;  
     }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }              
}
function getProjectView($project_id)
{
    try {
        $sql = "SELECT DISTINCT P.project_id,PD.product_id
                FROM LH_PROJECTS P
                JOIN LH_PROJECT_SUB PS ON P.project_id = PS.project_id
                JOIN LH_PRODUCTS PD ON PS.product_id = PD.product_id
                LEFT JOIN LH_PROJECT_VIEW lh_proj_view ON lh_proj_view.project_id = P.project_id
                WHERE P.project_id = '".$project_id."'
                AND CONVERT(date, getdate()) >= P.public_date
                AND P.brand_id != 23 AND P.project_data_status = 'PB' ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function isLadawan($project_id)
{
    try {
        $sql = "SELECT lh_proj_view.product_view_id
                FROM LH_PROJECT_VIEW lh_proj_view
                WHERE lh_proj_view.project_id = '".$project_id."'
                AND lh_proj_view.product_view_id = 5 ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        if(!empty($result)){
            return true;
        }
        return false;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectIDByMultipleProductView($product_view_id1,$product_view_id2)
{
    try {
        $sql = "SELECT PS.project_sub_id
                FROM LH_PROJECTS P
                JOIN LH_PROJECT_SUB PS ON P.project_id = PS.project_id
                JOIN LH_PRODUCTS PD ON PS.product_id = PD.product_id
                LEFT JOIN LH_PROJECT_VIEW lh_proj_view ON lh_proj_view.project_id = P.project_id
                WHERE ( lh_proj_view.product_view_id = '".$product_view_id1."' OR lh_proj_view.product_view_id = '".$product_view_id2."' )
                AND CONVERT(date, getdate()) >= P.public_date
                AND P.brand_id != 23 AND P.project_data_status = 'PB' ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $project_sub_ids = [];
        while ($row = mssql_fetch_object($result)) {
            $project_sub_ids[] = $row->project_sub_id;
        }
        return $project_sub_ids;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectIDLadawanByMultipleProductView($product_view_id1,$product_view_id2)
{
    try {
        $sql = "SELECT DISTINCT P.project_id,PS.project_sub_id
                FROM LH_PROJECTS P
                JOIN LH_PROJECT_SUB PS ON P.project_id = PS.project_id
                JOIN LH_PRODUCTS PD ON PS.product_id = PD.product_id
                LEFT JOIN LH_PROJECT_VIEW lh_proj_view ON lh_proj_view.project_id = P.project_id
                WHERE ( lh_proj_view.product_view_id = '".$product_view_id1."' OR lh_proj_view.product_view_id = '".$product_view_id2."' )
                AND CONVERT(date, getdate()) >= P.public_date
                AND P.brand_id = 23 AND P.project_data_status = 'PB' ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $project_sub_ids = [];
        while ($row = mssql_fetch_object($result)) {
            $project_sub_ids[] = $row->project_sub_id;
        }
        return $project_sub_ids;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectIDLadawan()
{
    try {
        $sql = "SELECT DISTINCT P.project_id
                FROM LH_PROJECTS P
                JOIN LH_PROJECT_SUB PS ON P.project_id = PS.project_id
                JOIN LH_PRODUCTS PD ON PS.product_id = PD.product_id
                LEFT JOIN LH_PROJECT_VIEW lh_proj_view ON lh_proj_view.project_id = P.project_id
                WHERE CONVERT(date, getdate()) >= P.public_date
                AND P.brand_id = 23 AND P.project_status != 'SO' AND P.project_data_status = 'PB' ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $project_ids = [];
        while ($row = mssql_fetch_object($result)) {
            $project_ids[] = $row->project_id;
        }
        return $project_ids;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectIdByTypeLuxury() {
    try {
        $sql = "SELECT DISTINCT P.project_id
                FROM LH_PROJECTS P
                JOIN LH_PROJECT_SUB PS ON P.project_id = PS.project_id
                JOIN LH_PRODUCTS PD ON PS.product_id = PD.product_id
                WHERE P.brand_id = 23
                AND CONVERT(date, getdate()) >= P.public_date AND P.project_data_status = 'PB' ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $project_ids = [];
        while ($row = mssql_fetch_object($result)) {
            $project_ids[] = $row->project_id;
        }
        return $project_ids;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectSubIdCountryId() {
    try {
        $sql = "SELECT DISTINCT PS.project_sub_id,PS.project_id,PS.product_id,PZ.province_id
                FROM LH_PROJECTS P
                JOIN LH_PROJECT_SUB PS ON P.project_id = PS.project_id
                JOIN LH_PROJECT_ZONE PZ ON PZ.project_id = P.project_id
        WHERE CONVERT(date, getdate()) >= P.public_date
                AND PZ.province_id != 1  AND PZ.province_id != 2
                AND PZ.province_id != 3  AND PZ.province_id != 4
                AND PZ.province_id != 58 AND PZ.province_id != 59 AND PZ.province_id != 0
                AND P.brand_id  != 23    AND P.project_status != 'SO' AND P.project_data_status = 'PB' ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectSubZoneByProjectID($project_id){
    try {
        $sql = "SELECT DISTINCT PS.project_sub_id,PS.project_id,PS.product_id,PZ.province_id,P.zone_id
                FROM LH_PROJECTS P
                JOIN LH_PROJECT_SUB PS ON P.project_id = PS.project_id
                JOIN LH_PROJECT_ZONE PZ ON PZ.project_id = P.project_id
                WHERE CONVERT(date, getdate()) >= P.public_date
                AND P.project_id = '$project_id' AND P.project_data_status = 'PB' ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getBannerByProjectID($project_id){
    try {
        $sql = "select *
                from LH_LEAD_IMAGE_PROJECT_FILE lead_img
                where lead_img.PROJECT_ID = '".$project_id."'
                and lead_img.LEAD_IMAGE_PROJECT_FILE_ID =
                (
                    SELECT min(lead_img.LEAD_IMAGE_PROJECT_FILE_ID)
                    from LH_LEAD_IMAGE_PROJECT_FILE lead_img
                    where lead_img.PROJECT_ID = '".$project_id."'
                )";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getAllBannerByProjectID($project_id){
    try {
        $sql = "select *
                from LH_LEAD_IMAGE_PROJECT_FILE lead_img
                where lead_img.PROJECT_ID = '".$project_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getAllBannerByProjectID_Newproject($project_id){
    try {
        $sql = "select *
                from LH_LEAD_IMAGE_PROJECT_FILE lead_img
                where lead_img.PROJECT_ID = '".$project_id."' AND LEAD_IMAGE_PROJECT_FILE_TYPE = 'image' ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getAllBannerByProjectID_Default($project_id){
    try {
        $sql = "select count (LEAD_IMAGE_PROJECT_FILE_ID) AS num
                from LH_LEAD_IMAGE_PROJECT_FILE lead_img
                where lead_img.PROJECT_ID = '".$project_id."'
                AND LEAD_IMAGE_PROJECT_FILE_TYPE != 'image'
                AND CONVERT(date, getdate()) >= CONVERT(date, date_start)
                AND CONVERT(date, getdate()) <= CONVERT(date, date_end) ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $num = mssql_fetch_array($result);
        return $num['num'];
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}
function getProjectMaps($project_id)
{
    try {

        $sql = "SELECT * FROM LH_PROJECT_MAPS
                    WHERE project_id = '".$project_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}

function getProjectPrice($project_id)
{
    try {

//        $sql = "SELECT * FROM LH_PROJECT_PRICES
//                    WHERE project_id = '".$project_id."'";

        $sql = "SELECT p.*,b.order_seq FROM LH_PROJECT_PRICES p LEFT JOIN LH_PROJECTS j ON p.project_id = j.project_id 
LEFT JOIN LH_BRANDS b ON b.brand_id = j.brand_id
                    WHERE j.project_id = '".$project_id."'";

        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectBrand($project_id){
    try {

     $sql = "SELECT * FROM LH_PROJECTS p LEFT JOIN LH_BRANDS b ON p.brand_id = b.brand_id WHERE p.project_id = '".$project_id."'";
     $result = mssql_query($sql , $GLOBALS['db_conn']);
     $result = mssql_fetch_object($result);
     return $result;
                        
    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }                    
}

function getBrand_order_seq($order){
   try {

    $sql = "SELECT  brand_id,brand_name_th,order_seq FROM LH_BRANDS ORDER BY order_seq $order";
    $result = mssql_query($sql , $GLOBALS['db_conn']);
    return Helper::toArrayList($result);
                       
   }  catch (Exception $e) {
       echo $sql . "<br>" . $e->getMessage();
   }  
}


function getProjectConcept($project_id)
{
    try {

        $sql = "SELECT * FROM LH_PROJECT_CONCEPT
                    WHERE project_id = '".$project_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectPromotion($project_id)
{
    try {

        $sql = "SELECT *
                FROM LH_PROMOTION pr
                WHERE CONVERT(date, getdate()) BETWEEN pr.promotion_start_date AND pr.promotion_end_date
                AND project_id = '".$project_id."' AND pr.promotion_approve = 'Y' ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getPromotions()
{
    try {
        $sql = "select *
                from LH_PROMOTION pr
                JOIN LH_PROJECTS lh_proj ON lh_proj.project_id = pr.project_id
                WHERE CONVERT(date, getdate()) BETWEEN pr.promotion_start_date AND pr.promotion_end_date
                and pr.promotion_approve ='Y' AND lh_proj.project_data_status = 'PB' ORDER BY pr.promotion_end_date ASC";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getHomeSellByPlanID($plan_id)
{
    try {
        $sql = "select DISTINCT lh_proj.project_id
                from LH_HOME_SELL lh_hsell
                join LH_PROJECT_SUB lh_sub on lh_sub.project_sub_id = lh_hsell.project_sub_id
                join LH_PROJECTS lh_proj on lh_proj.project_id = lh_sub.project_id
                WHERE lh_hsell.plan_id = '".$plan_id."' AND lh_proj.project_data_status = 'PB'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getHomeSellExceptProject($project_id, $product_id,$home_sell_id)
{
    try {
        $sql = "select lh_hsell.home_sell_id,lh_hsell.plan_id,lh_hsell.number_converter
                from LH_HOME_SELL lh_hsell
                join LH_PROJECT_SUB lh_sub on lh_sub.project_sub_id = lh_hsell.project_sub_id
                join LH_PROJECTS lh_proj on lh_proj.project_id = lh_sub.project_id
                WHERE lh_sub.product_id = '$product_id'
                and lh_proj.project_id = '$project_id'
                and lh_hsell.home_sell_id != '$home_sell_id'
                AND lh_proj.project_data_status = 'PB'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getHomeSellByPlanIDExceptHomeSellID($plan_id,$home_sell_id)
{
    try {
        $sql = "select DISTINCT lh_proj.project_id
                from LH_HOME_SELL lh_hsell
                join LH_PROJECT_SUB lh_sub on lh_sub.project_sub_id = lh_hsell.project_sub_id
                join LH_PROJECTS lh_proj on lh_proj.project_id = lh_sub.project_id
                WHERE lh_hsell.plan_id = '".$plan_id."' AND lh_hsell.home_sell_id = '".$home_sell_id."'
                AND lh_proj.project_data_status = 'PB'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getNumberOfHomeSellInProject($project_id)
{
    try {
        $sql = "select count(DISTINCT lh_hsell.home_sell_id) count_home_sell
                from LH_PROJECTS lh_proj
                join LH_PROJECT_SUB lh_sub on lh_sub.project_id = lh_proj.project_id
                join LH_HOME_SELL lh_hsell on lh_hsell.project_sub_id = lh_sub.project_sub_id
                WHERE lh_proj.project_id = '".$project_id."' AND lh_proj.project_data_status = 'PB'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getImagePromotion($promotion_id)
{
    try {

        $sql = "SELECT *
                FROM LH_IMG_PROMOTION img_promo
                WHERE img_promo.promotion_id = '".$promotion_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getHomeSell($project_id,$product_id)
{
    try {
        $sql = "SELECT *
                FROM LH_PROJECT_SUB psub
                JOIN LH_HOME_SELL hsell ON psub.project_sub_id = hsell.project_sub_id
                WHERE  psub.project_id = '".$project_id."'
                ORDER BY hsell.order_request DESC";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getHomeSell_ByPrice($project_id,$product_id)
{
    try {
        $sql = "SELECT CAST(REPLACE(c.total_price,',','') AS INT) AS my_price,hsell.home_sell_id,hsell.home_sell_status,hsell.house,hsell.model_image,
hsell.soldout_update,hsell.plan_id,hsell.number_converter,hsell.features_convert,hsell.update_date,icon_id
                FROM LH_PROJECT_SUB psub
                JOIN LH_HOME_SELL hsell ON psub.project_sub_id = hsell.project_sub_id
                LEFT JOIN LH_CONDITION_HOME_SELL c ON c.home_sell_id = hsell.home_sell_id
                WHERE psub.project_id = '".$project_id."'
ORDER BY my_price ASC , hsell.update_date DESC ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}



function getHomeSellByBrandID($brand_id)
{
    try {
        $sql = "SELECT DISTINCT  proj.project_id,hsell.home_sell_id,hsell.project_sub_id,hsell.plan_id
                FROM LH_HOME_SELL hsell
                LEFT JOIN LH_PROJECT_SUB psub ON psub.project_sub_id = hsell.project_sub_id
                LEFT JOIN LH_PROJECTS proj ON psub.project_id = proj.project_id
                WHERE proj.brand_id = '".$brand_id."' AND lh_proj.project_data_status = 'PB'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getHomeSellHouseY($mode)
{
    try {
        $sql = "SELECT *, ";
        $sql .=" (CASE ";
        $sql .=" WHEN CHARINDEX( '-',lh_con.total_price)< 1 THEN CAST(REPLACE(lh_con.total_price,',','') AS INT) ";
        $sql .=" ELSE CAST(REPLACE((SUBSTRING(lh_con.total_price, 0,CHARINDEX( '-',lh_con.total_price))),',','') AS INT) ";
        $sql .=" END )as pricestr ";
        $sql.="FROM LH_HOME_SELL lh_home_sell
                JOIN LH_CONDITION_HOME_SELL lh_con on lh_home_sell.home_sell_id = lh_con.home_sell_id
                WHERE lh_home_sell.house = 'Y'
                ORDER BY pricestr ".$mode;
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectSubBySubID($project_sub_id)
{
    try {

        $sql = "SELECT *
                FROM LH_PROJECT_SUB lh_sub
                WHERE lh_sub.project_sub_id = '".$project_sub_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getGalleryPlanByPlanID($plan_id)
{
    try {
        $sql = "SELECT *
                FROM LH_GALERY_PLAN gl_plan
                WHERE gl_plan.plan_id = '".$plan_id."'
                ORDER BY gl_plan.galery_plan_id asc";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getGalleryFirstPlan($plan_id)
{
    try {
        $sql = "SELECT plan_img,plan_seo FROM LH_PLANS WHERE plan_id = '$plan_id'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getGallerysPlanByPlanID($plan_id)
{
    try {
        $sql = "SELECT *
                FROM LH_GALERY_PLAN g
                LEFT JOIN LH_PLANS p ON g.plan_id = p.plan_id
                WHERE p.plan_id  = '$plan_id'

       ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getHomeSellGalleryPlan($home_sell_id)
{
    try {

        $sql ="SELECT *
        FROM LH_GALERY_HOME_SELL_MAIN g LEFT JOIN LH_GALERY_PLAN p ON g.galery_plan_id = p.galery_plan_id
        WHERE g.home_sell_id = '$home_sell_id'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getAllGalleryPlanByPlanID($plan_id)
{
    try {
        $sql = "SELECT *
                FROM LH_GALERY_PLAN gl_plan
                WHERE gl_plan.plan_id = '".$plan_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getGalleryPlanOfSeries($series_id)
{
    try {
        $sql = "SELECT *
                FROM LH_SERIES s
                LEFT JOIN  LH_PLANS p ON s.series_id = p.series_id
                LEFT JOIN LH_GALERY_PLAN g ON p.plan_id = g.plan_id
                WHERE s.series_id = '$series_id' AND g.galery_plan_type = 'home_plan'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getGallerySeries($series_id)
{
    try {
        $sql = "SELECT *
                FROM LH_GALERY_SERIES g
                LEFT JOIN LH_SERIES s ON g.series_id = s.series_id
                WHERE s.series_id  = '$series_id'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectNearBy($project_id)
{
    try {
        $sql = "SELECT *
                FROM LH_PROJECT_NEARBY lnb
                WHERE lnb.project_id = '".$project_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectRelate($project_id,$zone_id,$product_id)
{
    try {
        $sql = "SELECT *
                FROM LH_PROJECTS lh_proj
                                JOIN LH_PROJECT_SUB lh_proj_sub ON lh_proj_sub.project_id = lh_proj.project_id
                WHERE lh_proj.zone_id = '".$zone_id."'
                AND lh_proj.project_id != '".$project_id."'
                AND lh_proj.project_status != 'SO'
                AND lh_proj_sub.product_id = '".$product_id."'
                AND lh_proj.brand_id != 23
                AND CONVERT(date, getdate()) >= lh_proj.public_date AND lh_proj.project_data_status = 'PB'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getGalleryHomeSellType($home_sell_id)
{
    try {
        $sql = "SELECT lh_gal_h_main.type_galery,lh_gal_h_main.galery_plan_id
                FROM LH_GALERY_HOME_SELL_MAIN lh_gal_h_main
                WHERE lh_gal_h_main.home_sell_id = '$home_sell_id'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getGalleryHomeSellTypeUpload($home_sell_id)
{
    try {
        $sql = "SELECT lh_up.*
                FROM LH_GALERY_HOME_SELL_MAIN lh_gal_h_main
                LEFT JOIN LH_UPLOAD_GALERY_HOME_SELL lh_up
                ON lh_gal_h_main.galery_home_sell_id = lh_up.galery_home_sell_id
                WHERE lh_gal_h_main.home_sell_id = '$home_sell_id'
                ORDER BY lh_gal_h_main.seq_imgge ASC ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getGalleryHomeSellTypeFurnishedHome($home_sell_id)
{
    try {

        $sql = "SELECT DISTINCT m.galery_funished_main_id ,m.project_id ,m.folder_name ,s.alt_seo,s.dis_en,s.dis_th,s.galery_funished_sub_id ,s.path_funished, s.type_image FROM LH_GALERY_FURNISHED_MAIN m
        LEFT JOIN LH_GALERY_FURNISHED_SUB s
        ON m.galery_funished_main_id = s.galery_funished_main_id
        LEFT JOIN LH_GALERY_HOME_SELL_MAIN h
        ON h.galery_funished_sub_id = s.galery_funished_sub_id
        WHERE h.home_sell_id  = '$home_sell_id'" ;
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectRelateLadawan($project_id,$zone_id,$product_id)
{
    try {
        $sql = "SELECT *
                FROM LH_PROJECTS lh_proj
                                JOIN LH_PROJECT_SUB lh_proj_sub ON lh_proj_sub.project_id = lh_proj.project_id
                WHERE lh_proj.zone_id = '".$zone_id."'
                AND lh_proj.project_id != '".$project_id."'
                AND lh_proj.project_status != 'SO'
                AND lh_proj_sub.product_id = '".$product_id."'
                AND lh_proj.brand_id = 23
                AND CONVERT(date, getdate()) >= lh_proj.public_date AND lh_proj.project_data_status = 'PB'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getPlanByPlanID($plan_id)
{
    try {
        $sql = "SELECT *
                FROM LH_PLANS lh_plan
                WHERE lh_plan.plan_id = '".$plan_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getPlanByPlanName($plan_name_th)
{
    try {
        $sql = "SELECT *
                FROM LH_PLANS lh_plan
                WHERE replace(lh_plan.plan_name_th, ' ', '-') = '".$plan_name_th."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getPlanByPlanName_code($decode_plan_name_th,$plan_name_th)
{
    try {
        $sql = "SELECT *
                FROM LH_PLANS lh_plan
                WHERE replace(lh_plan.plan_name_th, ' ', '-') = '".$plan_name_th."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $num = mssql_num_rows($result);
        if($num > 0){
           $full_result = mssql_fetch_object($result);
        }else{
          $sql = "SELECT *
                FROM LH_PLANS lh_plan
                WHERE replace(lh_plan.plan_name_th, ' ', '-') = '".$decode_plan_name_th."'";
          $result = mssql_query($sql , $GLOBALS['db_conn']);  
          $full_result = mssql_fetch_object($result);
        }
        
        return $full_result;

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}
function getSeriesBySeriesID($series_id)
{
    try {
        $sql = "select *
                from LH_SERIES lh_series
                where lh_series.series_id = '$series_id'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getIconActivity($icon_id)
{
    try {
        $sql = "SELECT *
                FROM LH_ICON_ACTIVITY lh_icon
                WHERE lh_icon.icon_id = '".$icon_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getIconActivity_homesell($icon_id)
{
    try {
        $sql = "SELECT *
                FROM LH_ICON_ACTIVITY lh_icon
                WHERE lh_icon.icon_id = '".$icon_id."'
                AND CONVERT(date, getdate()) BETWEEN lh_icon.icon_date_start AND lh_icon.icon_date_end ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}



function getHomeSellImgByHomeSellID($home_sell_id)
{
    try {
        $sql = "SELECT DISTINCT  g.home_sell_id,g.type_galery,g.seq_imgge,g.galery_funished_main_id,  
                s.path_funished,s.dis_th,s.dis_en,s.alt_seo,s.type_image
                 FROM LH_GALERY_HOME_SELL_MAIN g LEFT JOIN LH_HOME_SELL h ON g.home_sell_id = h.home_sell_id
                LEFT JOIN LH_GALERY_FURNISHED_SUB s ON s.galery_funished_sub_id = g.galery_funished_sub_id 
                WHERE s.type_image = 1 AND g.home_sell_id = '$home_sell_id'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}



function getHomeSellImgByHomeSellID_Banner($home_sell_id)
{
    try {
        $sql ="SELECT TOP(5) * FROM LH_GALERY_FURNISHED_MAIN m 
LEFT JOIN LH_GALERY_FURNISHED_SUB s ON m.galery_funished_main_id = s.galery_funished_main_id 
LEFT JOIN LH_GALERY_HOME_SELL_MAIN hm ON hm.galery_funished_main_id = m.galery_funished_main_id

WHERE hm.home_sell_id = '$home_sell_id' ";
/*
        $sql = "SELECT TOP(5) g.galery_home_sell_id ,g.galery_funished_sub_id,g.home_sell_id,g.type_galery,g.seq_imgge,g.galery_funished_main_id,  
                s.path_funished,s.dis_th,s.dis_en,s.alt_seo,s.type_image,m.folder_name
                 FROM LH_GALERY_HOME_SELL_MAIN g LEFT JOIN LH_HOME_SELL h ON g.home_sell_id = h.home_sell_id
                LEFT JOIN LH_GALERY_FURNISHED_SUB s ON s.galery_funished_sub_id = g.galery_funished_sub_id 
                                LEFT JOIN LH_GALERY_FURNISHED_MAIN m ON m.galery_funished_main_id = s.galery_funished_main_id
                WHERE s.type_image = 1 AND g.home_sell_id =  '$home_sell_id'";
*/                
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getHomeSellByHomeSellID($home_sell_id)
{
    try {
        $sql = "SELECT *
                FROM LH_HOME_SELL lh_hsell
                WHERE lh_hsell.home_sell_id = '".$home_sell_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getHomeSellExceptID($home_sell_id)
{
    try {
        $sql = "SELECT *
                FROM LH_HOME_SELL lh_hsell
                WHERE lh_hsell.home_sell_id != '$home_sell_id'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getConditionHomeSellByHomeSellID($home_sell_id)
{
    try {
        $sql = "SELECT *
                FROM LH_CONDITION_HOME_SELL lh_con
                WHERE lh_con.home_sell_id = '".$home_sell_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;

    }  catch (Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getGalleryImgProject($project_id)
{
    try {
        $sql = "SELECT *
                FROM LH_GALERY_PROJECT g_p
                LEFT JOIN LH_GALERY_PROJECT g_img ON g_p.project_id = g_img.galery_project_id
                LEFT JOIN LH_GALERY_IMG_PROJECT g_img_p ON g_img_p.galery_project_id = g_p.galery_project_id
                WHERE g_p.project_id ='".$project_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getVDOProject($project_id)
{
    try {
        $sql = "SELECT *
                FROM LH_PROJECT_CLIPS clip
                WHERE clip.project_id = '".$project_id."'
                ORDER BY clip.clip_project_id DESC";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function get360Project($project_id)
{
    try {
        $sql = "SELECT *
                FROM LH_360_PROJECT proj
                WHERE proj.project_id = '".$project_id."'
                ORDER BY proj.c360_project_id DESC";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function get360Plan($plan_id)
{
    try {
        $sql = "SELECT *
                FROM LH_360_FOR_PLAN lh_360_plan
                WHERE lh_360_plan.plan_id = '".$plan_id."'
                ORDER BY lh_360_plan.c360_plan_id DESC";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getVideoPlan($plan_id)
{
    try {
        $sql = "SELECT *
                FROM LH_CLIP_FOR_PLAN lh_clip_plan
                WHERE lh_clip_plan.plan_id = '".$plan_id."'
                ORDER BY lh_clip_plan.clip_plan_id desc";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectByID($project_id){
    try {
        $sql = "select *
                from LH_PROJECTS lh_project
                where lh_project.project_id = '".$project_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectByIDZone_id($project_id,$zone_id){
    try {
        $sql = "select z.zone_id , p.project_id ,p.brand_id,p.project_status,p.project_name_th,p.project_name_en
                from LH_PROJECTS p
                JOIN LH_PROJECT_ZONE z ON p.project_id = z.project_id
                where p.project_id = '".$project_id."' AND z.zone_id = '".$zone_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectByName($project_name_th){
    try {
        $sql = "select *
                from LH_PROJECTS lh_project
                where replace(lh_project.project_name_th, ' ', '-') = '".$project_name_th."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectSEOByID($project_id){
    try {
        $sql = "SELECT TOP(1) f.LEAD_IMAGE_PROJECT_FILE_NAME,pj_seo.project_title_seo_th,pj_seo.project_title_seo_en,
pj_seo.project_des_seo_th,pj_seo.project_des_seo_en,pj_seo.project_title_seo_en,pj_seo.project_des_seo_en,
pj_seo.project_keyword_th,pj_seo.project_keyword_en
                FROM LH_PROJECT_SEO pj_seo
                LEFT JOIN LH_LEAD_IMAGE_PROJECT_FILE f ON pj_seo.project_id = f.PROJECT_ID
                WHERE pj_seo.project_id = '".$project_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectSEOByID_Lang($project_id,$lang){
    try {
        if($lang == 'th') {
            $sql = "SELECT TOP(1) f.LEAD_IMAGE_PROJECT_FILE_NAME,pj_seo.project_title_seo_th,pj_seo.project_des_seo_th,pj_seo.project_title_seo_en,pj_seo.project_des_seo_en,pj_seo.project_keyword_th,pj_seo.project_keyword_en
                FROM LH_PROJECT_SEO pj_seo
                LEFT JOIN LH_LEAD_IMAGE_PROJECT_FILE f ON pj_seo.project_id = f.PROJECT_ID
                WHERE pj_seo.project_id = '" . $project_id . "'";
            $result = mssql_query($sql, $GLOBALS['db_conn']);
            $result = mssql_fetch_object($result);
            return $result;
        }else{
            $sql = "SELECT TOP(1) f.LEAD_IMAGE_PROJECT_FILE_NAME,pj_seo.project_title_seo_en,pj_seo.project_des_seo_en,pj_seo.project_title_seo_en,pj_seo.project_des_seo_en,pj_seo.project_keyword_th,pj_seo.project_keyword_en
                FROM LH_PROJECT_SEO pj_seo
                LEFT JOIN LH_LEAD_IMAGE_PROJECT_FILE f ON pj_seo.project_id = f.PROJECT_ID
                WHERE pj_seo.project_id = '" . $project_id . "'";
            $result = mssql_query($sql, $GLOBALS['db_conn']);
            $result = mssql_fetch_object($result);
            return $result;
        }
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}


function getBrandByID($brand_id){
    try {
        $sql = "select *
                from LH_BRANDS lh_brand
                where lh_brand.brand_id = '".$brand_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectZones($product_id)
{
    try {
        $sql = "SELECT DISTINCT PZ.zone_id,PZ.zone_name_en,PZ.zone_name_th
                FROM LH_PROJECTS P
                JOIN LH_ZONES PZ ON P.zone_id = PZ.zone_id
                LEFT JOIN LH_PROJECT_SUB PS ON P.project_id = PS.project_id
                LEFT JOIN LH_PRODUCTS PD ON PS.product_id = PD.product_id
                WHERE PD.product_id ='".$product_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectViewZones($product_id)
{
    try {
        $sql = "SELECT DISTINCT PZ.zone_id,PZ.zone_name_en,PZ.zone_name_th,PZ.create_date
                FROM LH_PROJECTS P
                JOIN LH_PROJECT_ZONE Z ON Z.project_id = P.project_id 
                JOIN LH_ZONES PZ ON Z.zone_id = PZ.zone_id
				LEFT JOIN LH_PROJECT_VIEW pv ON pv.project_id = P.project_id
                WHERE pv.product_view_id ='".$product_id."' 
                AND CONVERT(date, getdate()) >= P.public_date AND P.project_status != 'SO' AND P.project_data_status = 'PB'
                ORDER BY PZ.create_date ASC";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getZoneByName($zone_name_th)
{
    try {
        $sql = "SELECT *
                FROM LH_ZONES PZ
                WHERE replace(PZ.zone_name_th, ' ', '-') = '$zone_name_th'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getZoneByName_EN($zone_name_en)
{
    try {
        $sql = "SELECT *
                FROM LH_ZONES PZ
                WHERE replace(PZ.zone_name_en, ' ', '-') = '$zone_name_en'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getHomeSellZones()
{
    try {
        $sql = "SELECT DISTINCT PZ.zone_id,PZ.zone_name_en,PZ.zone_name_th
                FROM LH_HOME_SELL lh_home_sell
                JOIN LH_PROJECT_SUB PS ON lh_home_sell.project_sub_id = PS.project_sub_id
                JOIN LH_PROJECTS P ON P.project_id = PS.project_id
                JOIN LH_ZONES PZ ON P.zone_id = PZ.zone_id
                WHERE lh_home_sell.house = 'Y'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectZoneLadawans()
{
    try {
        $sql = "SELECT DISTINCT PZ.zone_id,PZ.zone_name_en,PZ.zone_name_th
                FROM LH_PROJECTS P
                JOIN LH_ZONES PZ ON P.zone_id = PZ.zone_id
                LEFT JOIN LH_PROJECT_VIEW pv ON pv.project_id = P.project_id
                WHERE P.brand_id = 23";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectCountryZones()
{
    try {
        $sql = "SELECT DISTINCT PZ.zone_id,PZ.zone_name_en,PZ.zone_name_th
                FROM LH_PROJECTS P
                JOIN LH_ZONES PZ ON P.zone_id = PZ.zone_id
                JOIN LH_PROJECT_ZONE PJZ ON PJZ.project_id = P.project_id
                LEFT JOIN LH_PROJECT_SUB PS ON P.project_id = PS.project_id
                LEFT JOIN LH_PRODUCTS PD ON PS.product_id = PD.product_id
                                WHERE PJZ.province_id != 1 AND PJZ.province_id != 2 AND
                                      PJZ.province_id != 3 AND PJZ.province_id != 4 AND
                                      PJZ.province_id != 58 AND PJZ.province_id != 59
                                      AND P.brand_id  != 23 AND PJZ.province_id != 0
                                      AND CONVERT(date, getdate()) >= P.public_date
                                      AND P.project_data_status = 'PB'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getPromotionProjectZones()
{
    try {
        $sql = "SELECT DISTINCT PZ.zone_id,PZ.zone_name_en,PZ.zone_name_th,PZ.create_date
                                FROM LH_PROMOTION pr
                JOIN LH_PROJECTS P ON P.project_id = pr.project_id
                LEFT JOIN LH_PROJECT_ZONE PJZ ON PJZ.project_id = P.project_id
                JOIN LH_ZONES PZ ON PJZ.zone_id = PZ.zone_id
                
                                WHERE CONVERT(date, getdate()) BETWEEN pr.promotion_start_date AND pr.promotion_end_date 
                                AND P.project_data_status = 'PB' 
                AND pr.promotion_approve ='Y'
        ORDER BY PZ.create_date ASC";
    $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getVDOPlan($plan_id)
{
    try {
        $sql = "SELECT *
                FROM LH_CLIP_FOR_PLAN lh_clip_plan
                WHERE lh_clip_plan.plan_id = '".$plan_id."'
                ORDER BY clip_plan_id DESC";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getPeculality($home_sell_id)
{
    try {
        $sql = "SELECT *
                FROM LH_PECULIARITY_SUB_HOME_SELL lh_pec
                JOIN LH_PECULIARITY lh_pec_detail on lh_pec.peculiarity_id = lh_pec_detail.peculiarity_id
                WHERE lh_pec.home_sell_id ='".$home_sell_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getPeculalityPlan($plan_id)
{
    try {
        $sql = "select *
                from LH_PECULIARITY_PLAN lh_pe_plan
                JOIN LH_PECULIARITY lh_pe on lh_pe_plan.peculiarity_id = lh_pe.peculiarity_id
                where lh_pe_plan.plan_id ='".$plan_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getGalleryFloorPLans($plan_id)
{
    try {
        $sql = "SELECT *
                FROM LH_GALERY_FLOOR_PLAN_IMG lh_gal_floor
                WHERE lh_gal_floor.plan_id = '".$plan_id."'
                AND lh_gal_floor.floor_plan_img_name != '' ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectIDPlan($plan_id)
{
    try {
        $sql = "select DISTINCT lh_p_sub.project_id,lh_p_sub.product_id
                from LH_PROJECT_SUB lh_p_sub
                join LH_HOME_SELL lh_h_sell on lh_p_sub.project_sub_id = lh_h_sell.project_sub_id
                where lh_h_sell.plan_id = '".$plan_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectSub($project_id,$product_id)
{
    try {
        $sql = "SELECT *
                FROM LH_PROJECT_SUB lh_sub
                WHERE lh_sub.project_id = '".$project_id."' and lh_sub.product_id = '".$product_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectSubByProjectID($project_id)
{
    try {
        $sql = "SELECT *
                FROM LH_PROJECT_SUB lh_sub
                WHERE lh_sub.project_id = '".$project_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getCommunityFeature($project_id)
{
    try {
        $sql = "SELECT *
                FROM LH_COMMUNITY lh_com
                LEFT JOIN LH_COMMUNITY_FEATURES lh_com_feature
                ON lh_com_feature.community_features_id = lh_com.community_features_id
                WHERE lh_com.project_id = '".$project_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getMasterPlan($project_sub_id)
{
    try {
        $sql = "SELECT *
                FROM LH_MASTER_PLAN_CONDO lh_master_plan
                WHERE lh_master_plan.project_sub_id = '".$project_sub_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getFloorPlanByMasterPlan($master_plan_id)
{
    try {
        $sql = "select *
                from LH_FLOOR_PLAN_IMG lh_floor_img
                WHERE lh_floor_img.master_plan_id ='".$master_plan_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getUnitPlanCondo($project_sub_id)
{
    try {
        $sql = "select *
                from LH_UNIT_PLAN_CONDO lh_unit_condo
                join LH_UNIT_PLAN_IMG lh_unit_img
                on lh_unit_condo.unit_plan_id = lh_unit_img.unit_plan_id
                where lh_unit_condo.project_id = '".$project_sub_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getHomeSellFloorPlan($home_sell_id)
{
    try {
        $sql = "SELECT *
                FROM LH_HOME_SELL lh_homesell
                JOIN LH_GALERY_FLOOR_PLAN_IMG lh_floor_plan
                on lh_homesell.plan_id = lh_floor_plan.plan_id
                where lh_homesell.home_sell_id = '".$home_sell_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getRoomPlan($project_sub_id)
{
    try {
        $sql = "SELECT  lh_galery_floor_plan.floor_plan_img_name,lh_galery_floor_plan.floor_plan_dis_th,lh_galery_floor_plan.floor_plan_dis_en
                FROM LH_MASTER_PLAN_CONDO lh_master_plan
                LEFT JOIN LH_FLOOR_PLAN_IMG lh_floor_img
                ON lh_master_plan.master_plan_id = lh_floor_img.master_plan_id
                LEFT JOIN LH_GALERY_FLOOR_PLAN_IMG lh_galery_floor_plan
                ON lh_galery_floor_plan.floor_plan_img_id = lh_floor_img.floor_plan_img_id
                WHERE lh_master_plan.project_sub_id = '".$project_sub_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProgressCondo($project_sub_id)
{
    try {
        $sql = "SELECT  YEAR(lh_progress.progress_update),MONTH(lh_progress.progress_update)
                ,MIN(lh_progress.progress_update_id) progress_update_id
                FROM LH_PROGRESS_UPDATE_CONDO lh_progress
                JOIN LH_PROGRESS_UPDATE_IMG lh_progress_img
                ON lh_progress.progress_update_id = lh_progress_img.progress_update_id
                WHERE lh_progress.project_sub_id = '".$project_sub_id."'
                GROUP BY    YEAR(lh_progress.progress_update),
                            MONTH(lh_progress.progress_update)";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProgressByProgressUpdateID($progress_update_id)
{
    try {
        $sql = "SELECT  *
                FROM LH_PROGRESS_UPDATE_CONDO lh_progress
                JOIN LH_PROGRESS_UPDATE_IMG lh_progress_img
                ON lh_progress.progress_update_id = lh_progress_img.progress_update_id
                WHERE lh_progress.progress_update_id = '".$progress_update_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProgressByProgressUpdateMonth($progress_sub_id,$dateString)
{
    try {
        $sql = "SELECT  *
                FROM LH_PROGRESS_UPDATE_CONDO lh_progress
                JOIN LH_PROGRESS_UPDATE_IMG lh_progress_img
                ON lh_progress.progress_update_id = lh_progress_img.progress_update_id
                WHERE lh_progress.project_sub_id = '".$progress_sub_id."'
                AND YEAR(lh_progress.progress_update) = YEAR('".$dateString."')
                AND MONTH(lh_progress.progress_update) = MONTH('".$dateString."')";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getTVC()
{
    try {
        $sql = "SELECT tvc_id,tvc_name,tvc_detail,tvc_youtube_link,tvc_url,tvc_public_date,tvc_thumbnail,tvc_type,tvc_thumbnail_youtube_seo,tvc_thumbnail_vdo_seo
                FROM LH_TVC
                WHERE CONVERT(date, getdate()) >= tvc_public_date
                ORDER BY tvc_public_date DESC,update_date DESC";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getMotivo()
{
    try {
        $sql = "SELECT * FROM LH_MOTIVO ORDER BY creat_date DESC";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getAll360Projects()
{
    try {
        $sql = "SELECT *
                FROM LH_360_PROJECT lh_360
                JOIN LH_PROJECTS lh_proj
                ON lh_360.project_id = lh_proj.project_id";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getAll360Plans()
{
    try {
        $sql = "SELECT *
                FROM LH_360_FOR_PLAN lh_360
                JOIN LH_PLANS lh_plan
                ON lh_360.plan_id = lh_plan.plan_id
                WHERE lh_360.c360_plan_url != '' ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getHomeSeries()
{
    try {
        $sql = "select *
                from LH_SERIES lh_series
                LEFT JOIN LH_SERIES_IMG lh_series_img
                on lh_series_img.series_id = lh_series.series_id
                WHERE lh_series.views = 'on'
                ORDER BY lh_series.series_id desc";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getHomeSeriesByID($series_id)
{
    try {
        $sql = "select *
from LH_SERIES lh_series
                LEFT JOIN LH_SERIES_IMG lh_series_img
                on lh_series_img.series_id = lh_series.series_id
                where lh_series.series_id = '".$series_id."'
                ORDER BY lh_series.series_id desc";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getHomeSellConditionByHomeSellID($homesell_id)
{
    try {
        $sql = "SELECT *
                FROM LH_CONDITION_HOME_SELL lh_con
                where lh_con.home_sell_id ='".$homesell_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getZoneByID($zone_id)
{
    try {
        $sql = "SELECT *
                FROM LH_ZONES
                WHERE LH_ZONES.zone_id ='".$zone_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectZoneByID($project_id)

{
    try {
        $sql = "SELECT z.zone_name_th
                FROM LH_ZONES z
                LEFT JOIN LH_PROJECT_ZONE p ON z.zone_id = p.zone_id
                WHERE p.project_id = '".$project_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getZoneByID_Dis($plan_id)
{
    try {
        $sql = "SELECT DISTINCT(z.zone_id),z.zone_name_th FROM LH_PLANS p
LEFT JOIN LH_HOME_SELL h ON p.plan_id = h.plan_id
LEFT JOIN LH_PROJECT_SUB s ON s.project_sub_id = h.project_sub_id
LEFT JOIN LH_PROJECTS j ON j.project_id = s.project_id
LEFT JOIN LH_PROJECT_ZONE pz ON pz.project_id =j.project_id
LEFT JOIN LH_ZONES z ON pz.zone_id = z.zone_id
WHERE p.plan_id = '".$plan_id."' AND  s.project_sub_id IS NOT NULL";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProvinceByID($province_id)
{
    try {
        $sql = "SELECT *
                FROM LH_PROVINCES lh_prov
                WHERE lh_prov.PROVINCE_ID = '".$province_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getPlanFunction($plan_id)
{
    try {
        $sql = "select *
                from LH_FUNCTION_PLAN_SUB lh_func_sub
                join LH_PLAN_FUNCTION lh_plan_func
                on lh_func_sub.function_plan_plan_id = lh_plan_func.function_plan_id
                where lh_func_sub.plan_id = '".$plan_id."'";

        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getPlanFunction_paln($plan_id)
{
    try {
        $sql = "select *
                from LH_FUNCTION_PLAN_SUB lh_func_sub
                join LH_PLAN_FUNCTION lh_plan_func
                on lh_func_sub.function_plan_plan_id = lh_plan_func.function_plan_id
                where lh_func_sub.plan_id = '".$plan_id."' AND NOT lh_plan_func.function_plan_id =3";

        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getPlanFunction_paln_praking($plan_id)
{
    try {
        $sql = "select *
                from LH_FUNCTION_PLAN_SUB lh_func_sub
                join LH_PLAN_FUNCTION lh_plan_func
                on lh_func_sub.function_plan_plan_id = lh_plan_func.function_plan_id
                where lh_func_sub.plan_id = '".$plan_id."' AND  lh_plan_func.function_plan_id =3";

        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getPlanSeries($series_id)
{
    try {
        $sql = "select *
                from LH_PLANS lh_plan 
                JOIN LH_SERIES lh_series 
                on lh_series.series_id = lh_plan.series_id
                where lh_series.series_id = '".$series_id."' AND lh_plan.views = 'on'
                ORDER BY lh_plan.plan_id desc";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getSeries($series_id)
{
    try {
        $sql = "SELECT *
                FROM LH_SERIES lh_series
                WHERE lh_series.series_id = '".$series_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getSeriesByName($series_name_th)
{
    try {
        $sql = "SELECT *
                FROM LH_SERIES lh_series
                WHERE replace(lh_series.series_name_th, ' ', '-') = '".$series_name_th."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getFunctionHomeSell($home_sell_id)
{
    try {
        $sql = "SELECT *
                FROM LH_HOME_SELL lh_hsell
                LEFT JOIN LH_FUNCTION_PLAN_SUB lh_psub
                ON lh_hsell.plan_id = lh_psub.plan_id
                JOIN LH_PLAN_FUNCTION lh_pfunction
                ON  lh_psub.function_plan_plan_id = lh_pfunction.function_plan_id
                WHERE lh_hsell.home_sell_id = '".$home_sell_id."' AND NOT lh_pfunction.function_plan_id =3";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getFunctionHomeSell_Parking($home_sell_id)
{
    try {
        $sql = "SELECT *
                FROM LH_HOME_SELL lh_hsell
                LEFT JOIN LH_FUNCTION_PLAN_SUB lh_psub
                ON lh_hsell.plan_id = lh_psub.plan_id
                JOIN LH_PLAN_FUNCTION lh_pfunction
                ON  lh_psub.function_plan_plan_id = lh_pfunction.function_plan_id
                WHERE lh_hsell.home_sell_id = '".$home_sell_id."' AND  lh_pfunction.function_plan_id =3";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getLeadImgProjectFile()
{
    try {
        $sql = "SELECT *
                FROM LH_LEAD_IMAGE_PROJECT_FILE lh_limg_file";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectReview($except_review_id = '')
{
    try {
        $sql = "SELECT  *
                FROM LH_PROJECT_REVIEW lh_proj_review
                where CONVERT(date, getdate()) >= lh_proj_review.project_update  ";
        if(!empty($except_review_id)){
            $sql .= " and lh_proj_review.project_review_id != '".$except_review_id."'";
        }
        $sql .= "ORDER BY lh_proj_review.project_update DESC ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}
function getProjectReviewForHome($except_review_id='')
{
    try {
        $sql = "SELECT
DATEDIFF(weekday, getdate(),project_update) AS num,
* FROM LH_PROJECT_REVIEW WHERE CONVERT(date,project_update) BETWEEN project_update AND getdate()
AND  order_seq = '$except_review_id'  ORDER BY num DESC  ";
         // $sql ="SELECT TOP 6 * FROM LH_PROJECT_REVIEW where order_seq !='' ORDER BY order_seq ASC";
         $result = mssql_query($sql,$GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectReviewForHome_ALL($except_review_id='')
{
    try {
          $sql ="SELECT TOP 6 * FROM LH_PROJECT_REVIEW where order_seq !='' ORDER BY order_seq ASC";
         $result = mssql_query($sql,$GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectReviewDetail($project_review_id)
{
    try {
        $sql = "SELECT *
                FROM LH_PROJECT_REVIEW lh_proj_review
                WHERE lh_proj_review.project_review_id = '".$project_review_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}


function getReviewByName($project_review_name_th)
{
    try {
        $project_review_name_th = preg_replace('~[\\\\/:*?"<>|]~', ' ', $project_review_name_th);
        $sql = "SELECT *
                FROM LH_PROJECT_REVIEW lh_proj_review
                WHERE (replace(custom_url, ' ', '-') = '".$project_review_name_th."') or (replace(lh_proj_review.project_review_name_th, ' ', '-') LIKE '%".$project_review_name_th."%')";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}


function getLeadImg()
{
    try {
        $sql = "SELECT *
                FROM LH_LEAD_IMAGE lh_lead
                WHERE CONVERT(date, getdate()) BETWEEN lh_lead.leade_imd_date_start AND lh_lead.leade_imd_date_end
                AND lh_lead.page_review='page' AND lh_lead.page_project_id = 'Home'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getAllHighlight()
{
   try {
                $sql  = " SELECT ";
                $sql .= " highlights_id,highlights_name_th,highlights_name_en,highlights_dis_th ";
                $sql .= " ,highlights_dis_en,highlights_lead_img,highlights_content,highlights_lead_img_type ";
                $sql .= " ,highlights_update,highlight_link,highlight_thumbnail,highlights_content_en ";
                $sql .= " ,highlights_lead_img_seo,highlights_thumbnail_seo " ;
                $sql .= " ,convert(varchar, highlights_time_update, 108) highlights_time_update ";
                $sql .= " ,CONVERT(DATETIME, CONVERT(CHAR(8), highlights_update, 112) + ' ' + CONVERT(CHAR(8), highlights_time_update, 108)) datetime_update ";
                $sql .= " FROM LH_HIGHLIGHTS  ";
                $sql .= " WHERE CONVERT(date, getdate()) BETWEEN start_date AND end_date ";
                $sql .= " ORDER BY highlights_update DESC ";
                $result = mssql_query($sql , $GLOBALS['db_conn']);
                return Helper::toArrayList($result);
            }
            catch(Exception $e) {
                echo $sql . "<br>" . $e->getMessage();
            }

}

function getHighlight($highlight_id)
{
    try {
        $sql = "select *
                from LH_HIGHLIGHTS lh_high
                where lh_high.highlights_id = '".$highlight_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getLogoByProductID($product_id)
{
    try {
        $sql = "SELECT b.brand_id,b.logo_brand_th,b.logo_brand_en FROM LH_PROJECTS pr
                LEFT JOIN LH_PROJECT_SUB pr_s ON pr.project_id = pr_s.project_id
                LEFT JOIN LH_PROJECT_ZONE z ON z.zone_id = pr.zone_id
                LEFT JOIN LH_BRANDS b ON b.brand_id = pr.brand_id
                LEFT JOIN LH_PRODUCTS pd ON pd.product_id = pr_s.product_id
                WHERE pd.product_id = '".$product_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getZoneBkkByProductID($product_id)
{
    try {
        $sql = "SELECT DISTINCT(zs.zone_name_th) AS zone_name ,zs.zone_id
                FROM LH_PROJECTS pr LEFT JOIN LH_PROJECT_SUB ps ON ps.project_id = pr.project_id
                LEFT JOIN LH_PROJECT_ZONE z ON z.project_id = ps.project_id
                LEFT JOIN LH_ZONES zs ON zs.zone_id = pr.zone_id
                LEFT JOIN LH_PROVINCES pv ON pv.PROVINCE_ID = z.province_id
                WHERE ps.product_id = '".$product_id."' AND pr.project_data_status = 'PB'
                AND (pv.PROVINCE_ID =1 OR pv.PROVINCE_ID = 2 OR pv.PROVINCE_ID = 3
                    OR pv.PROVINCE_ID = 4 OR pv.PROVINCE_ID = 58 OR pv.PROVINCE_ID = 59)";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getZoneCountryByProductID($product_id)
{
    try {
        $sql = "SELECT DISTINCT(zs.zone_name_th) AS zone_name,zs.zone_id
                FROM LH_PROJECTS pr LEFT JOIN LH_PROJECT_SUB ps ON ps.project_id = pr.project_id
                LEFT JOIN LH_PROJECT_ZONE z ON z.project_id = ps.project_id
                LEFT JOIN LH_ZONES zs ON zs.zone_id = pr.zone_id
                LEFT JOIN LH_PROVINCES pv ON pv.PROVINCE_ID = z.province_id
                WHERE ps.product_id = '".$product_id."' AND pr.project_data_status = 'PB'
                AND pv.PROVINCE_ID !=1 AND pv.PROVINCE_ID != 2 AND pv.PROVINCE_ID != 3
                AND pv.PROVINCE_ID != 4 AND pv.PROVINCE_ID != 58 AND pv.PROVINCE_ID != 59";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getNewProject()
{
    try {
        $sql = "SELECT lh_proj.*
                FROM LH_PROJECTS lh_proj
                WHERE lh_proj.project_status = 'NP' AND lh_proj.public_date <= getdate() ORDER BY lh_proj.public_date DESC";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getNewProjectOrderByPrice($method = 'desc')
{
    try {

        $sql =" SELECT lh_proj.*,lh_proj_price.project_price, ";
        $sql .=" (CASE ";
        $sql .=" WHEN CHARINDEX( '-',lh_proj_price.project_price)< 1 THEN CAST(REPLACE(lh_proj_price.project_price,',','') AS INT) ";
        $sql .=" ELSE CAST(REPLACE((SUBSTRING(lh_proj_price.project_price, 0,CHARINDEX( '-',lh_proj_price.project_price))),',','') AS INT) ";
        $sql .=" END )as pricestr ";
        $sql .=" FROM LH_PROJECTS lh_proj left join LH_PROJECT_PRICES lh_proj_price ";
        $sql .=" ON lh_proj.project_id=lh_proj_price.project_id ";
        $sql .=" WHERE lh_proj.project_status = 'NP' ";
        $sql .=" order by pricestr ".$method ;

        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getBannerNewProject()
{
    try {
        $sql = "SELECT *
                FROM LH_LEAD_IMAGE lh_lead
                WHERE lh_lead.page_review = 'page'
                AND lh_lead.page_project_id = 'new-project'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getLHGalleryImgProject($project_id)
{
    try {
        $sql = "SELECT *
                FROM LH_GALERY_PROJECT lh_gal
                JOIN LH_GALERY_IMG_PROJECT lh_img_gal ON lh_gal.galery_project_id = lh_img_gal.galery_project_id
                WHERE lh_gal.project_id = '".$project_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectByBrand($brand_id)
{
    try {
        $sql = "SELECT *
                FROM LH_PROJECTS lh_proj
                WHERE lh_proj.brand_id = '".$brand_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectArea($project_id)
{
    try {
        $sql = "SELECT lh_proj_sub.product_id,lh_proj_area.area_home,lh_proj_area.area_square_w_home,lh_proj_area.num_plan_home
                FROM LH_PROJECTS lh_proj
                JOIN LH_PROJECT_SUB lh_proj_sub
                ON lh_proj.project_id = lh_proj_sub.project_id
                JOIN LH_PROJECT_AREA_HOME lh_proj_area
                ON lh_proj_sub.project_sub_id = lh_proj_area.project_sub_id
                where lh_proj.project_id = '".$project_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectAreaByProjectIDANDProductID($project_id,$product_id)
{
    try {
        $sql = "SELECT lh_proj_sub.product_id,lh_proj_area.area_home,lh_proj_area.area_square_w_home,lh_proj_area.num_plan_home
                FROM LH_PROJECTS lh_proj
                JOIN LH_PROJECT_SUB lh_proj_sub
                ON lh_proj.project_id = lh_proj_sub.project_id
                JOIN LH_PROJECT_AREA_HOME lh_proj_area
                ON lh_proj_sub.project_sub_id = lh_proj_area.project_sub_id
                where lh_proj.project_id = '".$project_id."' AND lh_proj_sub.product_id = '".$product_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectAreaByProjectID($project_id)
{
    try {
        $sql = "SELECT lh_proj_sub.product_id,lh_proj_area.area_home,lh_proj_area.area_square_w_home,lh_proj_area.num_plan_home
                FROM LH_PROJECTS lh_proj
                JOIN LH_PROJECT_SUB lh_proj_sub
                ON lh_proj.project_id = lh_proj_sub.project_id
                JOIN LH_PROJECT_AREA_HOME lh_proj_area
                ON lh_proj_sub.project_sub_id = lh_proj_area.project_sub_id
                where lh_proj.project_id = '".$project_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProductByProductID($product_id)
{
    try {
        $sql = "SELECT *
                FROM LH_PRODUCTS lh_prod
                WHERE lh_prod.product_id = '".$product_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProducts()
{
    try {
        $sql = "SELECT *
                FROM LH_PRODUCTS lh_prod ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getLogoProjectMap($project_id)
{
    try {
        $sql = "select *
                from LH_PROJECT_MAPS pro_map
                where pro_map.PROJECT_ID = '".$project_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectConceptOnly($project_id)
{
    try {
        $sql = "select distinctive_th
                from LH_PROJECT_CONCEPT pro_con
                where pro_con.PROJECT_ID = '".$project_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectContactByProjectID($project_id)
{
    try {
        $sql = "SELECT *
                FROM LH_PROJECT_CONTACTS lh_contact
                WHERE lh_contact.project_id = '".$project_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getBannerByPageName($page_name)
{
    try {
        $sql = "SELECT *
                FROM LH_BANNER b
                LEFT JOIN LH_BANNER_SUB s ON b.banner_id = s.banner_id
                LEFT JOIN LH_BANNER_PAGE p ON b.banner_id = p.banner_id
                WHERE p.banner_page = '".$page_name."'
                                                ORDER BY    b.banner_id DESC";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getBannerByBannerProjectID($banner_project_id)
{
    try {
        $sql = "SELECT *
                FROM LH_BANNER b
                LEFT JOIN LH_BANNER_SUB s ON b.banner_id = s.banner_id
                LEFT JOIN LH_BANNER_PAGE p ON b.banner_id = p.banner_id
                WHERE p.banner_project_id = '".$banner_project_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getProjectAreaCondo($project_sub_id)
{
    try {
        $sql = "SELECT *
                FROM LH_PROJECT_AREA_CONDO lh_area_condo
                WHERE lh_area_condo.project_sub_id = '".$project_sub_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getBuildingUnit($project_sub_id)
{
    try {
        $sql = "SELECT lh_build_unit.*
                FROM LH_PROJECT_AREA_CONDO lh_area_condo
                JOIN LH_BUILDING_UNIT lh_build_unit
                ON lh_area_condo.project_area_condo_id = lh_build_unit.project_area_condo_id
                WHERE lh_area_condo.project_sub_id = '".$project_sub_id."'";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function get_Emd_Project ($project_id)
{
  try {
       $sql = "SELECT * FROM LH_EDM_PROJECT e LEFT JOIN LH_PROJECTS j ON e.project_id = j.project_id WHERE j.project_id = '$project_id'";
      $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getBannerByTypePage($type_page_id,$checkShowType = false){
    try {
        $sql = "SELECT  b.banner_main_id,b.type_page_id , b.type_show_id , b.color_code_activity,b.pic_activity,
                b.text_activity_th,b.text_activity_th,b.thumnail_path,b.url_activity,b.url_lead,b.url_vdo,b.seo_main,
                l.seo_lead,
                l.banner_lead_id,l.lead_path,l.lead_img_mobile,b.lead_img_mobile as lead_img_mobile_act,l.img_url
                FROM LH_BANNER_MAIN b
                LEFT JOIN LH_BANNER_LEAD l ON b.banner_main_id = l.banner_main_id
                WHERE b.type_page_id = '$type_page_id'
                AND CONVERT(date, getdate()) >= CONVERT(date, b.startdate)
                AND CONVERT(date, getdate()) <= CONVERT(date, b.enddate) ";
        if($checkShowType){
            $sql .= ' AND b.type_show_id != 0 ';
            $sql .= ' order by l.seq_leade_img asc ';
            $result = mssql_query($sql , $GLOBALS['db_conn']);
            $result = mssql_fetch_object($result);
            return $result;
        }
        $sql .= ' order by l.seq_leade_img asc ';
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getBannerByTypePage_MB($type_page_id,$mode){
    try {
        $sql = "SELECT  b.banner_main_id,b.type_page_id , b.type_show_id , b.color_code_activity,b.pic_activity,
                b.text_activity_th,b.text_activity_th,b.thumnail_path,b.url_activity,b.url_lead,b.url_vdo,l.seo_lead,
                l.banner_lead_id,l.lead_path,l.lead_img_mobile,b.lead_img_mobile as lead_img_mobile_act,l.img_url
                FROM LH_BANNER_MAIN b
                LEFT JOIN LH_BANNER_LEAD l ON b.banner_main_id = l.banner_main_id
                WHERE b.type_page_id = '1'
                AND b.lead_img_mobile = '$mode'
                AND b.type_show_id = 0
                AND l.lead_img_mobile = 'm' ";

        $sql .= ' ORDER BY b.type_show_id DESC ';
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        return Helper::toArrayList($result);
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getBannerByTypePage_MyBanner($type_page_id){
    try {
        if($type_page_id == 1){
           $p = 'h';
        }else{
            $p = 'p';
        }

        $sql = "SELECT  COUNT(b.banner_main_id) as num
                FROM LH_BANNER_MAIN b
                LEFT JOIN LH_BANNER_LEAD l ON b.banner_main_id = l.banner_main_id
                WHERE b.type_page_id = '$type_page_id'
                AND CONVERT(date, getdate()) <= b.enddate
                AND CONVERT(date, getdate()) >= b.startdate
                AND b.lead_img_mobile = '$p'
                AND b.type_show_id = 0 ";

        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $num = mssql_fetch_assoc($result);
       // return Helper::toArrayList($result);
        return $num['num'];
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getBannerByTypePage_MyBanner_MB($type_page_id,$mode){
    try {

        $sql = "SELECT  COUNT(b.banner_main_id) as num
                FROM LH_BANNER_MAIN b
                LEFT JOIN LH_BANNER_LEAD l ON b.banner_main_id = l.banner_main_id
                WHERE b.type_page_id = '$type_page_id'  AND CONVERT(date, getdate()) <= b.enddate AND b.lead_img_mobile != '$mode'
                AND CONVERT(date, getdate()) <= b.enddate
                AND CONVERT(date, getdate()) >= b.startdate 
                AND b.type_show_id = 0 
                AND l.lead_img_mobile = 'm' ";

        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $num = mssql_fetch_assoc($result);
        // return Helper::toArrayList($result);
        return $num['num'];
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

// share facebook
function get_Mata_Title ($page)
{
  try {
       $sql = "SELECT *
                FROM LH_MATA_PAGE 
                WHERE  page_name = '$page' ";
      $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

//get mata seo
function get_Mata_SEO($pages,$pages_id)
{
  try {
    if($pages == 'LH_LANDING_PAGE'){
       $tb_ = 'landing_page_id';
    }elseif ($pages == 'LH_LIVING_TIPS') {
       $tb_ = 'living_tip_id';
    }elseif ('LH_PROJECT_REVIEW') {
       $tb_ = 'project_review_id';
    }
       $sql = "SELECT title_seo ,description_seo ,thumnail_seo ,keyword_seo 
                FROM $pages 
                WHERE  $tb_ = '$pages_id' ";
      $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function get_MataPage_Title(){
    try {
        $sql = "SELECT *
                FROM LH_MATA_PAGE 
                WHERE  name_tag = 'default' ";
        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;        
    }catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function get_Img_Home ($type_page_id,$checkShowType = false)
{
  try {
       $home  = '';
       if($type_page_id == 1 ){
        //$home ="AND l.lead_img_mobile IS NULL ";
       }
       $sql = "SELECT TOP(1)  b.banner_main_id,b.type_page_id , b.type_show_id , b.color_code_activity,b.pic_activity,
                b.text_activity_th,b.text_activity_th,b.thumnail_path,b.url_activity,b.url_lead,b.url_vdo,
                l.banner_lead_id,l.lead_path,l.lead_img_mobile,b.lead_img_mobile as lead_img_mobile_act,l.img_url
                FROM LH_BANNER_MAIN b
                LEFT JOIN LH_BANNER_LEAD l ON b.banner_main_id = l.banner_main_id
                WHERE  b.type_page_id = '$type_page_id' ".$home;
      $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

//Banner ����

function getBannerMainBypageTypeNoDefault($pageType){
    try {

        $sql = " SELECT top(1) banner_main_id,type_page_id , type_show_id , color_code_activity,pic_activity,
                  text_activity_th,text_activity_th,thumnail_path,url_activity,url_lead,url_vdo
                  ,startdate,enddate
                FROM LH_BANNER_MAIN
                WHERE  lead_img_mobile <> 'd' and  type_page_id=$pageType
                        and (cast(GETDATE() as date) >= cast(startdate as date)
                        and cast(GETDATE() as date) <= cast(enddate as date))  ";

        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = mssql_fetch_object($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function getDefaultBannerList($pageType){
    try {

        $sql = "SELECT b.banner_main_id,b.type_page_id , b.type_show_id , b.color_code_activity,b.pic_activity,
                b.text_activity_th,b.text_activity_th,b.thumnail_path,b.url_activity,b.url_lead,b.url_vdo,
                l.banner_lead_id,l.lead_path,l.lead_img_mobile,b.lead_img_mobile as lead_img_mobile_act
                ,l.seq_leade_img as seqno
                FROM LH_BANNER_MAIN b
                LEFT JOIN LH_BANNER_LEAD l ON b.banner_main_id = l.banner_main_id
                WHERE  b.type_page_id = '$pageType' and l.lead_img_mobile='d' order by seqno asc";

        $result = mssql_query($sql , $GLOBALS['db_conn']);
        $result = Helper::toArrayList($result);
        return $result;
    }
    catch(Exception $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}

function  getBannerLeadByMainId($mainId){
    $sql=" select m.banner_main_id mainId,l.* ";
    $sql.=" from LH_BANNER_MAIN m left join LH_BANNER_LEAD l on m.banner_main_id = l.banner_main_id ";
    $sql.=" where m.banner_main_id= ".$mainId ;
    $sql.=" order by l.seq_leade_img asc ";
    try {
        $result = mssql_query ( $sql, $GLOBALS ['db_conn'] );
        return Helper::toArrayList ( $result );
    } catch ( Exception $e ) {
        echo $sql . "<br>" . $e->getMessage ();
    }
}

function getHomeSell_Image($home_sell_id){
    $sql = "SELECT p.plan_img,p.plan_seo,h.home_sell_id FROM LH_PLANS p
LEFT JOIN LH_HOME_SELL h ON h.plan_id = p.plan_id
WHERE h.home_sell_id = '$home_sell_id' ";
    try {
        $result = mssql_query ( $sql, $GLOBALS ['db_conn'] );
        $result = mssql_fetch_object($result);
        return $result;
    } catch ( Exception $e ) {
        echo $sql . "<br>" . $e->getMessage ();
    }
}
function getJobATTRIBUTE($id){
    $sql = "SELECT w.ATTRIBUTE,t.TITLE ,p.PROVINCE_NAME,l.LOCATION_NAME,w.RATES,w.POSTED_DAT FROM JOB_TITLE t 
        LEFT JOIN JOB_WORK w
        ON t.ID = w.POSITION_ID 
        LEFT JOIN JOB_LOCATION l ON l.LOCATION_ID = w.LOCATION_ID
        LEFT JOIN LH_PROVINCES p ON p.PROVINCE_ID = l.PROVINCE_ID
        WHERE w.ID = $id";
   try {
        $result = mssql_query ( $sql, $GLOBALS ['db_conn'] );
        $result = mssql_fetch_object($result);
        return $result;
    } catch ( Exception $e ) {
        echo $sql . "<br>" . $e->getMessage ();
    }
}

function getSEOUrl($url){
    $url_ = str_replace("%2F","/",urldecode($url));
    $url_ =  str_replace("%3A",":",urldecode($url_));
    $sql = "SELECT * FROM LH_SEO
    WHERE url_page = '$url' OR url_page = '$url_'";
    try {
        $result = mssql_query ( $sql, $GLOBALS ['db_conn'] );
        $result = mssql_fetch_object($result);
        
     return $result;
 } catch ( Exception $e ) {
    echo $sql . "<br>" . $e->getMessage ();
}
}

function getFOOTERUrl($url,$id){
    if($url != ''){
       $sql = "SELECT * FROM LH_FOOTERS
        WHERE url_page = '$url'";
    }else{
        $sql = "SELECT * FROM LH_FOOTERS
        WHERE footer_id = '$id'";
    } 
   try {
        $result = mssql_query ( $sql, $GLOBALS ['db_conn'] );
        $result = mssql_fetch_object($result);
        return $result;
    } catch ( Exception $e ) {
        echo $sql . "<br>" . $e->getMessage ();
    }
}



require_once 'query_search.php';
require_once 'query_helper.php';
require_once 'query_url.php';
