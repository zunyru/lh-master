<?php

require_once 'begin.php';
require_once 'query_function.php';

class Helper {

    public static function toArrayList($result)
    {
        $results = [];
        while ($row = mssql_fetch_object($result)) {
            $results[] = $row;
        }

        return $results;
    }

    public static function lang($lang,$content_th,$content_en)
    {
        if($lang == 'th'){
            return self::replaceEmpty($content_th);
        }
        return self::replaceEmpty($content_en);
    }

    public static function replaceEmpty($str)
    {
        if(!empty($str)){
            return $str;
        }
        return '';
    }

    public static function DateThai($strDate)
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        return "$strMonthThai $strYear";
    }

    public static function DateEng($strDate)
    {
        $strYear = date("Y",strtotime($strDate));
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","Jan","Fab","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
        $strMonthThai=$strMonthCut[$strMonth];
        return "$strMonthThai $strYear";
    }

    public static function url_string($str){
        if($str[0] == '/') $str = ltrim($str, '/');
        $full_url = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $full_path = str_replace(basename($full_url),'',$full_url);
        if($full_path == 'http:///') return $full_url.$str;
        else return str_replace(preg_replace("/(.*)\//", "",$full_url),"",$full_url).$str;
    }

    public static function nice_number($n) {

        // first strip any formatting;
        $n = (0+str_replace(",", "", $n));

        // is this a number?
        if (!is_numeric($n)) return false;

        // now filter it;
        if ($n > 1000000000000) return round(($n/1000000000000), 2).' trillion';
        elseif ($n > 1000000000) return round(($n/1000000000), 2).' billion';
        elseif ($n > 1000000) return round(($n/1000000), 2).' million';
        elseif ($n > 1000) return round(($n/1000), 2).' thousand';

        return number_format($n);
    }

    public static function million($n) {
        $n = str_replace(" ","",$n);
        $n = (0+str_replace(",", "", $n));
        if (!is_numeric($n)) return $n;
        elseif ($n < 1000000) return number_format($n);
        return round(($n/1000000), 2);
    }

    public static function getProjectPrice($price,$mode=1,$lang='th'){
        if($mode == 1){
            $price  = str_replace(" ","",$price);
            $price  = str_replace(",", "", $price);
            return self::million($price);
        }elseif ($mode == 2){
            $price  = str_replace(" ","",$price);
            $price  = str_replace(",", "", $price);
            $prices = explode('-',$price);
            if($lang == 'th'){
               $to = ' - ';
            }else{
               $to = ' to ';
            }
            if ( (!empty($prices[0]) && $prices[0] < 1000000) || (!empty($prices[1]) && $prices[1] < 1000000) ){
                return number_format($prices[0]).$to.number_format($prices[1]);
            }
            return self::million($prices[0]).$to.self::million($prices[1]);
        }
        return self::million($price);
    }

    public static function getPriceModeName($mode=1,$price,$lang='th'){
        if($mode == 1){
            $price = str_replace(" ","",$price);
            $price = (0+str_replace(",", "", $price));
            if ($price < 1000000)
                return $lang == 'th' ? 'Baht' : 'bath';
            return $lang == 'th' ? 'MB' : 'MB.';
        }elseif($mode == 2){
            $price = str_replace(" ","",$price);
            $price = (0+str_replace(",", "", $price));
            $prices = explode('-',$price);
            if ( (!empty($prices[0]) && $prices[0] < 1000000) || (!empty($prices[1]) && $prices[1] < 1000000) ){
                return $lang == 'th' ? 'Baht' : 'bath';
            }
            return $lang == 'th' ? 'MB' : 'MB.';
        }else{
            $price = str_replace(" ","",$price);
            $price = (0+str_replace(",", "", $price));
            if ($price < 1000000){
                return $lang == 'th' ? 'Baht / ตารางเมตร' : 'bath / sq.m.';
            }
            return $lang == 'th' ? 'MB / ตารางเมตร' : 'MB. / sq.m.';
        }
    }

    public static function sortByPrice(&$projects, $method = 'desc') {
    	$projects_arr = [ ];
    	foreach ( $projects as $i => $project_id ) {
    		$priceObj = getProjectPrice ( $project_id );
    		if (! empty ( $priceObj ) && $priceObj->price_mode == 1) {
    			$price = str_replace ( ' ', '', $priceObj->project_price );
    			$price = str_replace ( ',', '', $price );
    			$price_first = $price;
    			$price_second = $price;
    		} elseif (! empty ( $priceObj ) && $priceObj->price_mode == 2) {
    			$price = str_replace ( ' ', '', $priceObj->project_price );
    			$prices = explode ( '-', $price );
    			$price_first = str_replace ( ',', '', $prices [0] );
    			$price_second = str_replace ( ',', '', $prices [1] );
    		} else {
    			$price = str_replace ( ' ', '', $priceObj->project_price );
    			$price = str_replace ( ',', '', $price );
    			$price_first = $price;
    			$price_second = $price;
    		}
    		$projects_arr [$i] ['id'] = $project_id;
    		$projects_arr [$i] ['price_first'] = ( int ) $price_first;
    		$projects_arr [$i] ['price_second'] = ( int ) $price_second;
    	}
    
    	usort ( $projects_arr, function ($a, $b) {
    		return $a ['price_first'] - $b ['price_first'];
    	} );
    		$projects_arr = $method == 'asc' ? array_reverse ( $projects_arr ) : $projects_arr;
    
    		$projects = [ ];
    		foreach ( $projects_arr as $item ) {
    			$projects [] = $item ['id'];
    		}
    
    		// die(var_dump($projects_arr));
    }
    

     public static function sortByPriceReturnAssoc(&$projects, $method = 'desc')
    {
        $projects_arr = [];
        foreach ($projects as $i => $project_id){
            $priceObj               = getProjectPrice($project_id);
            $brandObj               = getProjectBrand($project_id);
            if( !empty($priceObj) && $priceObj->price_mode == 1 ){
                $price  =   str_replace(' ','',$priceObj->project_price);
                $price  =   str_replace(',','',$price);
                $price_first   =   $price;
                $price_second  =   $price;
            }elseif (  !empty($priceObj) &&  $priceObj->price_mode == 2 ){
                $price  =   str_replace(' ','',$priceObj->project_price);
                $prices =   explode('-',$price);
                $price_first   =   str_replace(',','',$prices[0]);
                $price_second  =   str_replace(',','',$prices[1]);
            }else{
                $price  =   str_replace(' ','',$priceObj->project_price);
                $price  =   str_replace(',','',$price);
                $price_first   =   $price;
                $price_second  =   $price;
            }
            $projects_arr[$i]['id']                = $project_id;
            $projects_arr[$i]['price_first']       = (int)$price_first;
            $projects_arr[$i]['price_second']      = (int)$price_second;
            $projects_arr[$i]['brand_order_seq']   = $brandObj->order_seq;
        }

        

        usort($projects_arr, function($a, $b) {
        	return $a['price_first'] - $b['price_first'];//ตัวแรกdiffกัน
        });

        $projects_arr = $method == 'asc' ? array_reverse($projects_arr) : $projects_arr;

        $projects_x = [];
        foreach ($projects_arr as $item ){
            $buff['project_id']         =   $item['id'];
            $buff['order_seq']          =   $item['brand_order_seq'];
            $projects_x[]                 =   $buff;
        }
        
        // // die(var_dump($projects));
        // // exit();
        
        $projects = [];
        $BrandObjOrder = getBrand_order_seq('ASC');
        //print_r($BrandObjOrder);
        //print_r($projects_arr);
        foreach ($BrandObjOrder as $Brand_seq) {  
            //print_r($Brand_seq->order_seq);      
            foreach ($projects_x as  $item) {
                //print_r($item['order_seq']);
               if($Brand_seq->order_seq == $item['order_seq']){
                 $buff['project_id']        =   $item['project_id'];
                 $buff['order_seq']        =   $item['order_seq'];
                 $projects[]                 =   $buff;
               }
            }
        }

        // die(var_dump($projects2));

        
    }

    public static function sortProjectSubByPrice(&$project_subs, $method = 'desc')
    {
        $project_sub_arr = [];
        foreach ($project_subs as $i => $project_sub_id){
            $priceSub               = getProjectSubBySubID($project_sub_id);
            $project_id             = $priceSub->project_id;
            $priceObj               = getProjectPrice($project_id);
            $price                  = '';
            if( $priceObj->price_mode == 1 ){
                $price  =   str_replace(' ','',$priceObj->project_price);
                $price  =   self::million($price);
            }elseif ( $priceObj->price_mode == 2 ){
                $price  =   str_replace(' ','',$priceObj->project_price);
                $prices =   explode('-',$price);
                $price  =   self::million($prices[0]);
            }
            $project_sub_arr[$i]['project_id']         = $project_id;
            $project_sub_arr[$i]['price']              = $price;
            $project_sub_arr[$i]['project_sub_id']     = $project_sub_id;
        }

        usort($project_sub_arr, function ($i, $j) use ($method) {
            $a = $i['price'];
            $b = $j['price'];
            $meth = $method == 'desc' ? $a > $b : $a < $b;
            if ($a == $b) return 0;
            elseif ($meth) return 1;
            else return -1;
        });
        $project_subs = [];
        foreach ($project_sub_arr as $item ){
            $buff['project_id']         =   $item['project_id'];
            $buff['project_sub_id']     =   $item['project_sub_id'];
            $project_subs[]             =   $buff;
        }

//        die(var_dump($projects_arr));
    }

    public static function sortByPriceHomeSell(&$home_sells, $method = 'desc')
    {
    	//die(var_dump($home_sells));
    	
    		 
       //die(var_dump($home_sells));
        	
    }

    public static function groupThousand($millionStr)
    {
        $millionStr = str_replace(',','',$millionStr);
        $millionStr = str_replace('$','',$millionStr);
        $millionStr = number_format((int)$millionStr);

        return $millionStr;
    }

    public static function getProductNameByType($product_id)
    {
        $product = getProductByProductID($product_id);

        return $product != false ? $product->product_name_en : '';
    }

    public static function getProductNameTHByType($product_id)
    {
        $product = getProductByProductID($product_id);

        return $product != false ? $product->product_name_th : '';
    }

    public static function getArrayProductOfProject($project_id)
    {
        $project_subs = getProjectSubByProjectID($project_id);
        $product_arr  = [];
        foreach($project_subs as $project_sub){
            $product_arr[]  =   $project_sub->product_id;
        }

        return $product_arr;
    }

    public static function checkLangEnglish($lang){
        if(!empty($lang) && $lang == 'en'){
            return true;
        }
        return false;
    }
}