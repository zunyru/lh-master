<?php

function getGalleryByCheckType($home_sell_id)
{
    $home_sell_gallery  = getGalleryHomeSellType($home_sell_id);
    $galleryHomeSells   = [];
    if($home_sell_gallery != false){
        switch ($home_sell_gallery->type_galery) {
            case 1:
            $original_gallerys  = getGalleryHomeSellTypeUpload($home_sell_id);
            foreach ($original_gallerys as $original_gallery){
                $buff['path'] = $original_gallery->path_upload_gallery;
                $buff['desc'] = $original_gallery->dis_th;
                $buff['seo']  = $original_gallery->alt_seo;
                $galleryHomeSells[] = $buff;
            }
            break;
            case 2:
            $original_gallerys  = getHomeSellGalleryPlan($home_sell_id);
            foreach ($original_gallerys as $original_gallery){
                $buff['path'] = $original_gallery->galery_plan_name;
                $buff['desc'] = $original_gallery->galery_plan_name_dis;
                $buff['seo']  = $original_gallery->galery_plan_img_seo;
                $galleryHomeSells[] = $buff;
            }
            break;
            case 3:
            $original_gallerys  = getGalleryHomeSellTypeFurnishedHome($home_sell_id);
            foreach ($original_gallerys as $original_gallery){
                $buff['path'] = $original_gallery->path_funished;
                $buff['desc'] = $original_gallery->dis_th;
                $buff['seo']  = $original_gallery->alt_seo;
                $galleryHomeSells[] = $buff;
            }
            break;
        }
    }

    return $galleryHomeSells;
}

function renderBannerPageList($type_page)
{
    $checkShowType  = getBannerByTypePage($type_page,true);
    $banners        = getBannerByTypePage($type_page);

    if($banners != false) {
            // image only
        if (empty($checkShowType)) {
         $num = getBannerByTypePage_MyBanner($type_page);
         if($num == 0){
             $type = 'd';
         }else{
             $type = 'p';
         }
         foreach ($banners as $i => $banner) {
            if($banner->lead_img_mobile == $type) {
                ?>
                <div class="item">
                    <div class="banner-parallax">
                        <?php if ($banner->img_url == '') { ?>
                        <img src="<?= backend_url('base', $banner->lead_path) ?>" alt="<?=$banner->seo_lead?>">
                        <?php } else { ?>
                        <a href="<?= $banner->img_url ?>" target="_blank">
                            <img src="<?= backend_url('base', $banner->lead_path) ?>" alt="<?=$banner->seo_lead?>">
                        </a>
                        <?php } ?>

                    </div>
                </div>
                <?php
            }
        }
    }

            // youtube
    elseif ($checkShowType->type_show_id == 3) {

        foreach ($banners as $i => $banner) {
            $lead_img = backend_url('base', $banner->thumnail_path);
            $lead_youtube_url = str_replace('watch?v=', 'embed/', $banner->url_vdo);
            ?>
            <div class="item">
                <div class="item-video">
                    <div class="banner-container banner-parallax" id="bannerVideo<?= $i ?>">
                        <div class="rps-vdobg">
                            <img src="<?= $lead_img ?>" alt="<?=$banner->seo_main?>">
                        </div>

                        <div class="vdo-control">
                            <div class="play-block">
                                <span id="vdoPlay<?= $i ?>" class="vdo-icon vdoControl vdoPlay"></span>
                            </div>
                        </div>

                        <script type="text/javascript">
                            var iframe_src = '<?= $lead_youtube_url ?>';
                            var youtube_video_id = iframe_src.match(/youtube\.com.*(\?v=|\/embed\/)(.{11})/).pop();
                            if (youtube_video_id.length == 11) {
                                $('#vdoPlay<?=$i?>').click(function () {
                                    $("#slideVdo<?=$i?>").remove();
                                    var video_iframe = $('' +
                                        '<iframe id="video" width="100%" height="720px"' +
                                        ' src="<?= $lead_youtube_url . "?autoplay=0" ?>">' +
                                        '</iframe>');
                                    $('#bannerVideo<?=$i?>').find('.vdo-control').remove();
                                    $('.rps-vdobg').remove();
                                    $('#bannerVideo<?=$i?>').append(video_iframe);
                                    $(this).trigger('stop.autoplay.owl');
                                });
                            }
                        </script>
                    </div>
                </div>

            </div>
            <?php
            break;
        }
        ?>
        <?php
    }
            // vdo
    elseif ($checkShowType->type_show_id == 2) {
        foreach ($banners as $i => $banner) {
            ?>
            <div class="item">
                <script>
                    $(document).ready(function () {
                        $('#vdoPlay<?= $i ?>').click(function () {
                            $('#slideVdo<?= $i ?>').css('display', 'block');
                            $('#slideVdo<?= $i ?>')[0].play();
                            $(this).fadeOut(200);
                            $('#vdoPause<?= $i ?>').fadeIn(200);
                            $('.rps-vdobg').fadeOut(200);

                            $(".vdo-control").mouseenter(function (event) {
                                event.stopPropagation();
                                $('#vdoPause<?= $i ?>').addClass("btnshown");
                            }).mouseleave(function (event) {
                                event.stopPropagation();
                                $('#vdoPause<?= $i ?>').removeClass("btnshown");
                            });
                        });

                        $("#vdoPause<?= $i ?>").click(function () {
                            $('#slideVdo<?= $i ?>').get(0).pause();
                            $(this).fadeOut(200);
                            $('#vdoPlay<?= $i ?>').fadeIn(200);
                        });
                    });
                </script>
                <div class="banner-container banner-parallax">
                    <img class="rps-vdobg" src="<?= backend_url('base', $banner->thumnail_path) ?>" alt="<?=$banner->seo_main?>">
                    <video loop="loop" id="slideVdo<?= $i ?>" class="" style="display:none;">
                        <source src="<?= backend_url('base', $banner->url_vdo) ?>" type="video/mp4">
                        </video>

                        <div class="vdo-control">
                            <div class="play-block">
                                <span id="vdoPlay<?= $i ?>" class="vdo-icon vdoControl vdoPlay"></span>
                                <span id="vdoPause<?= $i ?>" class="vdo-icon vdoControl vdoPause"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                break;
            }
            ?>
            <?php
        }
            // activity
        elseif ($checkShowType->type_show_id == 1) {
            foreach ($banners as $i => $banner) {
                ?>
                <div class="item" <?php if (!empty($banner->url_activity)) echo "onclick=\"location.href='" . $banner->url_activity . "'\""; ?>>
                    <div class="banner-parallax banner-dktp">
                        <?php if(!empty($banner->pic_activity)) {?>
                        <a href="<?= $banner->img_url ?>" target="_blank">
                         <img src="<?= backend_url('base', $banner->pic_activity) ?>" alt="<?= $banner->seo_lead ?>">
                        </a> 
                        <?php }?>

                        <?php if (!empty($banner->type_show_id) && $banner->type_show_id == 1) {
                            ?>
                            <div class="banner-activity"
                                <?php if(!empty($banner->color_code_activity)) {
                                    ?>
                                    style="background-color: <?php echo hex2rgba($banner->color_code_activity, 0.8);?>"
                                    <?php
                                }?>
                                >
                                <?= $banner->text_activity_th ?>
                                <?php if(!empty($banner->url_activity)) {
                                    ?>
                                    <span class="link-acty"></span>
                                    <?php
                                }?>
                                <span class="close-acty closeActivityDkpt"></span>
                            </div>
                            <?php
                        }?>

                    </div>
                    <div class="banner-parallax banner-rsp">
                        <img src="<?= backend_url('base', $banner->pic_activity) ?>" alt="">
                        <?php if (!empty($banner->type_show_id) && $banner->type_show_id == 1) {
                            ?>
                            <div class="banner-activity  banner-rsp"
                                <?php if(!empty($banner->color_code_activity)) {
                                    ?>
                                    style="background-color: <?php echo hex2rgba($banner->color_code_activity, 0.8);?>"
                                    <?php
                                }?>
                                >
                                <?= $banner->text_activity_th ?>
                                <?php if(!empty($banner->url_activity)) {
                                    ?>
                                    <span class="link-acty"></span>
                                    <?php
                                }?>
                                <span class="close-acty closeActivityDkpt"></span>
                            </div>
                            <?php
                        }?>
                    </div>
                </div>
                <?php
                break;
            }
            ?>
            <?php
        }


    }
}

function renderHiddenLeadImage($type_page,$id_element_banner)
{
    $checkShowType  = getBannerByTypePage($type_page,true);
    $banners        = getBannerByTypePage($type_page);

    if(!empty($checkShowType) && !empty($banners)){
        ?>
        <div id="hiddenLeadImage" style="display: none;">
            <?php
            if($checkShowType->type_show_id == 1){
                foreach ($banners as $i => $banner){
                    if($banner->type_show_id == 0) {
                        ?>
                        <div class="item">
                            <div class="banner-parallax">
                              <a href="<?=$banner->img_url?>" target="_blank"> 
                                <img src="<?= backend_url('base', $banner->lead_path) ?>" alt="<?=$banner->seo_lead?>">
                              </a>   
                            </div>
                            <div class="banner-parallax banner-cover-rsp"
                            <?php $img_banner_url = !empty($banner->thumnail_path) ? $banner->thumnail_path : $banner->lead_path ?>
                            style="background-image: url(<?= backend_url('base',$img_banner_url) ?>);">
                        </div>
                    </div>
                    <?php
                }
            }
        }
        ?>
    </div>
    <script>
    
        $(document).ready(function () {
            $('.closeActivityDkpt').click(function () {
                $('#<?= $id_element_banner ?>').html('');
                $('#<?= $id_element_banner ?>').removeAttr('class');
                $('#<?= $id_element_banner ?>').append($('#hiddenLeadImage').html());
                $('#<?= $id_element_banner ?>').trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
                $('#<?= $id_element_banner ?>').find('.owl-stage-outer').children().unwrap();
                var isMulti = ($('#<?= $id_element_banner ?> .item').length > 1) ? true : false;
                $('#<?= $id_element_banner ?>').owlCarousel({
                    loop: isMulti,
                    autoplay: true,
                    autoplayTimeout: 4000,
                    autoplaySpeed: 1000,
                    margin:5,
                    animateOut: 'fadeOut',
                    nav:false,
                    dots: isMulti,
                    video:true,
                    lazyLoad:true,
                    center:true,
                    responsive:{
                        0:{
                            items:1
                        },
                    }
                });
            });
        });
        
    </script>
    <?php
}
}

function renderActivityResponsive()
{
    if(!empty($banners)){
        foreach ($banners as $i => $banner){
            if(!empty($banner->type_show_id) && $banner->type_show_id == 1) {
                ?>
                <a <?= !empty($banner->url_activity) ? 'href="'.$banner->url_activity.'"' : '' ?>
                    class="banner-activity banner-rsp"
                    <?php if(!empty($banner->color_code_activity)) {
                        ?>
                        style="background-color: <?php echo hex2rgba($banner->color_code_activity, 0.8);?>"
                        <?php
                    }?>
                    >
                    <?= $banner->text_activity_th ?>
                    <?php if(!empty($banner->url_activity)) {
                        ?>
                        <span class="link-acty"></span>
                        <?php
                    }?>
                    <span class="close-acty closeActivityDkpt"></span>
                </a>
                <?php
                break;
            }
        }
    }
}

// *** Include the class


function elementSharedFacebookProjectDetail($project_id,$banner_projects)
{
    $banner_project = !empty($banner_projects[0]) ? $banner_projects[0] : null;
    $img_url        = '';
    if(!empty($banner_project) && is_object($banner_project)){
        if($banner_project->LEAD_IMAGE_PROJECT_FILE_TYPE == 'image'){
            $img_url = $banner_project->LEAD_IMAGE_PROJECT_FILE_NAME;
        }elseif ($banner_project->LEAD_IMAGE_PROJECT_FILE_TYPE == 'youtube'){
            $img_url = $banner_project->banner_img_thum;
        }elseif ($banner_project->LEAD_IMAGE_PROJECT_FILE_TYPE == 'vdo'){
            $img_url = $banner_project->banner_img_thum;
        }elseif ($banner_project->LEAD_IMAGE_PROJECT_FILE_TYPE == 'banner'){
            $img_url = $banner_project->LEAD_IMAGE_PROJECT_FILE_NAME;
        }
    }
    //fileupload/images/project_img/16052017004415-banner.jpg
    
    $img_url=str_replace('fileupload/images/project_img/', 'fileupload/images/project_img/Thumbnails_', $img_url);
   // $img_url = "fileupload/images/project_img/Thumbnails_16052017005806-banner.jpg";
    $image        = backend_url('base',$img_url);
    $project_seo    = getProjectSEOByID($project_id);
    $title = $project_seo->project_title_seo_th;
    $description =  $project_seo->project_des_seo_th;
    $url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $url_cut = (substr($url, 0, strpos($url, "?"))==''?$url:substr($url, 0, strpos($url, "?")));

    ?>

    <!-- for Facebook el -->
    <meta property="fb:app_id"          content="<?= '659883840880609';?>" /> 
    <meta property="og:type"            content="website" /> 
    <meta property="og:url"             content="<?= $url_cut;?>" /> 
    <meta property="og:title"           content="<?= $title;?>" />
    <meta property="og:description"     content="<?= $description; ?>" />
    <meta property="og:site_name"       content="<?= $title;?>" /> 
    <meta property="og:image"           content="<?= $image; ?>" /> 
    <meta property="og:image:type"      content="image/jpeg" />
    
    <meta property="og:image:width"       content="600" />
    <meta property="og:image:height"      content="315" />
    <meta property="og:image:alt"         content="<?=$title;?>" /> 

    <!-- for Twitter el'-->
    <meta name="twitter:card"           content="summary_large_image" />
    <meta name="twitter:title"          content="<?= $title;?>" />
    <meta name="twitter:description"    content="<?= $description; ?>" />
    <meta name="twitter:creator"        content="@lhhome">
    <meta name="twitter:site"           content="<?= $url;?>">
    <meta name="twitter:image"          content="<?= $image; ?>" />
    <meta itemprop="image"              content="<?= $image ?>" /> 

    <?php
}


function elementMetaTitleDisTag ($page,$page_index = false,$lang = false ){
    //$url = "http://www.lh.co.th/www/Backend/";
    $url = backend_url('base','');

    $mata = get_Mata_Title($page);
    if($lang == 'en') {
        $title = $mata->title_en;
        $description = $mata->description_en;
    }else{
        $title = $mata->title;
        $description = $mata->description;	    
    }

    if($page_index != 0){
        $img_face =get_Img_Home($page_index);

        if(is_object($img_face)){ 
            if(isset($img_face)) {
                
             $image = str_replace('fileupload/images/project_img/','fileupload/images/project_img/Thumbnails_',$img_face->lead_path);
             $image = str_replace('fileupload/banner_file/','fileupload/banner_file/Thumbnails_',$image);
             
          //$img = str_replace($mata->image, $mata->image . 'Thumbnails_', $img_face->lead_path);
         $image = $url . $image;
        }
      }else{
         $image = $url.$mata ->image;
      }
  }else{
    
   $image = $url.$mata ->image;
  }

?>

<?php 

$url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$url_cut = (substr($url, 0, strpos($url, "?"))==''?$url:substr($url, 0, strpos($url, "?")));

$title       = "บริษัท แลนด์ แอนด์ เฮ้าส์ จำกัด (มหาชน)";
$description = "Land & Houses บริษัทธุรกิจอสังหาริมทรัพย์ในประเทศไทย LHเปิดขายทั้งโครงการบ้านเดี่ยว ทาวน์โฮม คอนโดมิเนียมในกรุงเทพฯ ปริมณฑลและต่างจังหวัด";

?>

<!-- for Google by page -->
<title><?=$title;?></title>
<meta name="description"            content="<?= $description;?>" />
<meta name="keywords"               content="<?= 'บ้านเดี่ยว, ทาวน์โฮม, คอนโด, คอนโดมิเนียม, โครงการบ้านใหม่';?>" />

<!-- for Facebook by page -->
<meta property="fb:app_id"          content="<?= '659883840880609';?>" /> 
<meta property="og:type"            content="website" /> 
<meta property="og:url"             content="<?= $url_cut;?>" /> 
<meta property="og:title"           content="<?= $title;?>" />
<meta property="og:description"     content="<?= $description; ?>" />
<meta property="og:site_name"       content="<?= $title;?>" /> 
<meta property="og:image"           content="<?= $image; ?>" /> 
<meta property="og:image:type"      content="image/jpeg" />

<meta property="og:image:width"       content="600" />
<!-- <meta property="og:image:height"      content="315" /> -->
<meta property="og:image:alt"         content="<?=$title;?>" /> 

<!-- for Twitter by page'-->
<meta name="twitter:card"           content="summary_large_image" />
<meta name="twitter:title"          content="<?= $title;?>" />
<meta name="twitter:description"    content="<?= $description; ?>" />
<meta name="twitter:creator"        content="@lhhome">
<meta name="twitter:site"           content="<?= $url;?>">
<meta name="twitter:image"          content="<?= $image; ?>" />
<meta itemprop="image"              content="<?= $image ?>" /> 


<?php
}
?>

<?php 
   function MataTagShareSocialMedia ($title,$description,$keywords,$url,$image,$page){

     $app_id  = '659883840880609';
     $url_cut = (substr($url, 0, strpos($url, "?"))==''?$url:substr($url, 0, strpos($url, "?"))); 
?>
    <!-- for Google <?=$page?>-->
    <title><?=$title;?></title>
    <meta name="description"            content="<?=$description;?>" />
    <meta name="keywords"               content="<?=$keywords;?>" />
    
    <!-- for Facebook <?=$page?> -->
    <meta property="fb:app_id"          content="<?= $app_id;?>" /> 
    <meta property="og:type"            content="website" /> 
    <meta property="og:url"             content="<?= $url_cut;?>" /> 
    <meta property="og:title"           content="<?= $title;?>" />
    <meta property="og:description"     content="<?= $description; ?>" />
    <meta property="og:site_name"       content="<?= $title;?>" /> 
    <meta property="og:image"           content="<?= $image; ?>" /> 
    <meta property="og:image:type"      content="image/jpeg" />
    
    <meta property="og:image:width"       content="600" />
    <!-- <meta property="og:image:height"      content="315" /> -->
    <meta property="og:image:alt"         content="<?=$title;?>" /> 
    
    <!-- for Twitter <?=$page?>-->
    <meta name="twitter:card"           content="summary_large_image" />
    <meta name="twitter:title"          content="<?= $title;?>" />
    <meta name="twitter:description"    content="<?= $description; ?>" />
    <meta name="twitter:creator"        content="@lhhome">
    <meta name="twitter:site"           content="<?= $url;?>">
    <meta name="twitter:image"          content="<?= $image; ?>" />
    <meta itemprop="image"              content="<?= $image ?>" /> 


<?php
   }
?>

