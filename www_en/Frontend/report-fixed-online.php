<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>
<?php include('header.php'); ?>
<!-- CSS -->
<link rel="stylesheet" href="css/report-fixed-online.css" type="text/css">
<!-- JS -->
<script src="js/report-fixed.js"></script>
<script src="js/banner-md.js"></script>

<div id="content" class="content">
    <div class="container">

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div>
                    <h1 class="heading-title">แจ้งรายละเอียดการซ่อมบ้าน</h1>
                    <div class="complain-form-block">
                        <p class="title">ข้อมูลลูกค้า</p>

                        <div class="form">
                            <div class="form-group form-details">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="house">
                                            เลขที่เอกสาร :
                                        </label>
                                        <p>Auto Generated</p>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="house">
                                            วัน เวลาที่แจ้ง :
                                        </label>
                                        <p>27/11/2559 22:53</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="house">
                                            ชื่อ-สกุล ลูกค้า :
                                        </label>
                                        <p>นาง วีระอนงค์ ชวลิตธำรง</p>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="house">
                                            email :
                                        </label>
                                        <p>27/11/2559 22:53</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="house">
                                            โทรศัพท์ติดต่อ :
                                        </label>
                                        <p>087-9276955</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="complain-form-block">
                        <p class="title">โครงการ/แปลง ที่แจ้งซ่อม</p>
                        <div class="form">
                            <div class="form-group form-details">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="house">
                                            โครงการ :
                                        </label>
                                        <p>นันทวัน-เชียงใหม่</p>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="house">
                                            บ้านเลขที่ :
                                        </label>
                                        <p>176/33</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-2 col-md-1 number-report">
                                        1
                                    </div>
                                    <div class="col-xs-10 col-md-9">
                                        <input type="text" id="" name="" class="form-control" value="รายการที่แจ้ง" readonly>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-offset-1 col-md-8">
                                        <div class="form report-btns">
                                            <a href="" class="btn btn-submit">เพิ่มรายการ</a>
                                            <a href="" class="btn btn-submit btn-delete">ลบรายการ</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="complain-form-block">
                        <p class="title">รายละเอียดการนัดหมาย</p>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form">
                                    <div class="form-group">
                                        <div class="radio-block">
                                            <input type="radio" id="topic9" name="complainTopic" />
                                            <label for="topic9"><span></span>ระบุวันเวลาให้เจ้าหน้าที่ตรวจสอบ</label>
                                            <input type="text" name="" class="form-control other-complain">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form">
                                    <div class="form-group">
                                        <div class="form-time">
                                            <label for="topic10"><span></span>เวลา</label>
                                            <input type="text" name="" class="form-control other-complain">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form">
                                    <div class="form-group">
                                        <div class="radio-block">
                                            <input type="radio" id="topic9" name="complainTopic" />
                                            <label for="topic9"><span></span>ต้องการให้เจ้าหน้าที่ติดต่อกลับเพื่อนัดวันเวลาอีกครั้ง (หากไม่สะดวกตามวันเวลาข้างต้น)</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="complain-form-block">
                        <p class="title">กรณีชื่อผู้แจ้งไม่ตรงกับชื่อลูกค้า (กรุณาระบุ)</p>
                        <div class="form report-other">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="house">
                                                ชื่อ-สกุล ผู้แจ้ง :
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <input type="text" id="" name="" class="form-control" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="house">
                                                โทรศัพท์ติดต่อ :
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <input type="text" id="" name="" class="form-control" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="house">
                                                email :
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <input type="text" id="" name="" class="form-control" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="house">
                                                โทรศัพท์ติดต่อ (มือถือ) :
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <input type="text" id="" name="" class="form-control" value="">
                                            <span>* สำหรับการส่ง SMS ตอบกลับ</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-8">
                                        <div class="form report-btns">
                                            <a href="" class="btn btn-submit">Preview & Send</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <a href="<?php url('complain-step1.php') ?>" class="step-back">< Back</a>
                </div>
            </div>

        </div>

    </div>
</div>


<?php include('footer.php'); ?>
