<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

$home_sell_id = $_GET['home_sell_id'];
$project_id = $_GET['project_id'];
$product_id = $_GET['product_id'];

if($product_id ==2){
    $type = 'townhome';
}else {
    $type = 'singlehome';
}

if(empty($home_sell_id) || getHomeSellByHomeSellID($home_sell_id) == false){
    header('Location: '.Helper::url_string('project-type.php'));
}

        $home_sell = getHomeSellByHomeSellID($home_sell_id);

        //first item
        $planImg        = getGalleryFirstPlan($home_sell->plan_id);
        $lhPlan         = getPlanByPlanID($home_sell->plan_id);
        $anotherImgs    = getHomeSellImgByHomeSellID($home_sell->image_in_home_sell_id);
        $conditionHomeSell = getConditionHomeSellByHomeSellID($home_sell->home_sell_id);
        $project_sub = getProjectSubBySubID($home_sell->project_sub_id);
        $plan = getPlanByPlanID($home_sell->plan_id);
        $conditionHomeSell = getConditionHomeSellByHomeSellID($home_sell_id);

        //$pic_share = 'homesell';


         $img_url=str_replace('fileupload/images/galery_plan/', 'http://www.lh.co.th/www/Backend/fileupload/images/galery_plan/Thumbnails_', $home_sell->model_image);

?>
<?php include('header.php'); ?>

    <!-- CSS -->
    <link rel="stylesheet" href="<?= file_path('css/project-info-sale.css') ?>" type="text/css">
    <link rel="stylesheet" href="<?= file_path('tablesaw/tablesaw.css') ?>" type="text/css">
    <!-- JS -->
    <script src="<?= file_path('tablesaw/tablesaw.jquery.js') ?>"></script>
    <script src="<?= file_path('tablesaw/tablesaw-init.js') ?>"></script>
    <script src="<?= file_path('js/project-info-sale.js') ?>"></script>

    <script type="text/javascript" src="<?= file_path('fancybox/jquery.mousewheel-3.0.4.pack.js') ?>"></script>
    <link rel="stylesheet" href="<?= file_path('fancybox/fancybox.min.css') ?>" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>

    <div class="page-banner page-scroll banner-deskt">
        <span id="scrollNext" class="i-scroll-next"></span>



        <div id="bannerSlide">
            <div class="item">
                <div class="banner-container banner-parallax" style="background-image: url();">
                    <?php if($home_sell != false) { ?>
                    <img src="<?php echo backend_url('base',$home_sell->model_image) ?>"
                         alt="<?= $planImg->plan_seo ?>">
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

    <div class="banner-resp">
        <div id="projectSlide" class="owl-carousel owl-theme">
            <div class="gal-img item"><img src="<?php if($home_sell != false) echo backend_url('base',$home_sell->model_image) ?>"/></div>
        </div>
    </div>

    <?php
        $plan = getPlanByPlanID($home_sell->plan_id);
        $price = getProjectPrice($project_sub->project_id);
        $conditionHomeSell = getConditionHomeSellByHomeSellID($home_sell_id);
        $project_concept = getProjectConcept($project_sub->project_id);
        $project = getProjectByID($project_sub->project_id);
        $zone = getZoneByID($project->zone_id);
        $series = getSeriesBySeriesID($plan->series_id);

    ?>

    <div id="content" class="content project-info-sale-page">
        <div class="container">

            <div class="other-prj-info-sale-top">
                <div class="sort-block">
                    <input type="hidden" name="sortSelect" value="">
                    <div class="sort" data-status="0">
                        <span>Watch house and sell more plots</span> <i class="i-sort"></i>
                    </div>
                    <ul id="sortLists">
                        <?php
                        $homeSells = getHomeSellExceptProject($project->project_id, $project_sub->product_id,$home_sell_id);
                        if(!empty($homeSells)){
                            foreach($homeSells as $homeSell){
                                $lhPlan = getPlanByPlanID($homeSell->plan_id); ?>
                                <?php
                                $numberpaln='';
                                if($homeSell->number_converter !=''){
                                    $numberpaln = ' หมายเลขแปลง ';
                                }

                                ?>
                                <?php
                                $url_see_more = $router->generate('home-sell-detail',[
                                    'plan_name'         =>  str_replace(' ','-',$lhPlan->plan_name_th),
                                    'home_sell_id'      =>  $homeSell->home_sell_id,
                                    'project_name'      =>  str_replace(' ','-',$project->project_name_th),
                                    'product_id'        =>  $product_id,
                                    'product_name'      =>  $type,
                                ]);
                                ?>
                                <li data-value="<?= $lhPlan->plan_name_th.$numberpaln.$homeSell->number_converter ?>" onclick="linkToUrl('<?= $url_see_more ?>')">
                                    <?= $lhPlan->plan_name_th.$numberpaln.$homeSell->number_converter ?>
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>

            <div id="sec-information" class="project-info-detail-block">
                <!-- H1 -->
                <?php
                $SEO = getSEOUrl($actual_link);
                if($actual_link == @$SEO->url_page){
                    echo '<h1>'.$SEO->h1.'</h1>';
                }
                ?>
                <!-- End H1 -->
                <div class="row">

                    <div class="row">

                        <div class="col-xs-12 project-sale-logo">

                            <?php
                            $id_icon = $home_sell->icon_id;
                            if($id_icon != ''){
                                $icon = getIconActivity_homesell($id_icon);
                                if(is_object($icon)){
                                if($icon->icon_img !=''){
                                    if($home_sell->home_sell_status != 'SO'){
                            ?>
                            <img src="<?=backend_url('icon_activity',$icon->icon_img )?>" class="logo-activity-detail"  alt="acti">
                            <?php }}}}?>
                            <?php
                            if($home_sell->home_sell_status == 'SO'){
                                ?>
                                <div class="col-xs-2 tagbox2_sold soldout-tag-info-sale">
                                    <div class="tag2">sold</div>
                                </div>
                            <?php
                            }
                            ?>

                            <img src="<?= !empty($series) ? backend_url('series_logo',$series->series_logo) : '' ?>" alt="">
                            <a href="<?=$url_see_more_project?>"><p class="title grey">Project  <?php if($project != false) echo $project->project_name_th ?></p></a>
                            <?php if(!empty($plan->plan_name_th)) { ?> <p class="title"> House Plan <span class="detail"> <?= $plan->plan_name_th ?> </span></p><?php } ?>
                            <?php if(!empty($home_sell->number_converter)) { ?> <p class="title"> Number of Plot of land <span class="detail"> <?= $home_sell->number_converter ?> </span></p><?php } ?>
                            <?php if(!empty($home_sell->features_convert)) { ?> <p class="title" style="display: none;"> Type of plot of land  <span class="detail" style="display: none;"> <?= str_replace(array("\n\r", "\n","n/g"), '<br/>',$home_sell->features_convert); ?> </span></p><?php } ?>
                            <?php if(!empty($plan->plan_feature_th)) { ?>
                                <p class="title"> Highlights features
                                <span class="detail detail-floor">
                                 <?=!empty($plan->plan_feature_th) ? str_replace(array("\n\r", "\n","n/g"), '<br/>', $plan->plan_feature_th) : ''?>
                                </span>
                                </p><?php } ?>
                            <?php if(!empty($conditionHomeSell->total_price)) { ?> <p class="title green price">Price <?= Helper::getProjectPrice($conditionHomeSell->total_price) ?> MB</p><?php } ?>
                        </div>



                    </div>

                    <?php

                    $planFunctions     = getFunctionHomeSell($home_sell_id);
                    $planFunctions_parking = getFunctionHomeSell_Parking($home_sell_id);
                    $leftPlanFunction  = [];
                    $rightPlanFunction = [];
                    $leftPlanFunction[] = $plan->plan_useful;
                    $leftPlanFunction[] = $conditionHomeSell->land_size;
                    $leftPlanFunction[] = $planFunctions_parking[0];
                   //print_r($planFunctions->function_plan_id);
                    //$leftPlanFunction[] = $planFunction->function_plan_id;
                    //$conditionHomeSell->land_size;
                    foreach ($planFunctions as $i => $planFunction)
                    {
                        if($planFunction->function_plan_id != 3) {
                            $leftPlanFunction[] = $planFunction;
                        }
                    }

                    ?>


                    <div class="col-md-6">
                        <div class="content-style">
                            <!-- <a href="<?=$url_see_more_project?>"><p class="title grey">โครงการ  <?php if($project != false) echo $project->project_name_th ?></p></a> -->
                            <!-- <p class="title grey mbr-15">ทำเล <?php if($zone != false) echo $zone->zone_name_th ?></p> -->
                            <p class="title grey mbr-15">House  details  </p>
                            <div class="row">
                                <div class="tb-2col">
                                    <div class="col-md-12 cl">
                                        <table class="content-style tbs1" id="project-table" width="100%">
                                            <tbody>
                                            <?php  foreach ($leftPlanFunction as $i => $planFunction) { ?>
                                            <tr>
                                                <td><?php if($i == 0){echo 'Utility area'; }elseif($i == 1){echo 'Land size';}elseif($i==2){echo 'parking';}else{ echo $planFunction->function_plan_name_th;}?></td>
                                                <td><?php if($i == 0){echo $planFunction; }elseif($i == 1){ echo $conditionHomeSell->land_size;}elseif($i==2){echo $planFunctions_parking[0]->function_plan_sub_plan_count_room;}else{echo $planFunction->function_plan_sub_plan_count_room; }?></td>
                                                <td><?php if($i == 0){echo 'sq.m.';}elseif($i == 1){echo 'Sq.wah ';}elseif($i==2){echo 'cars';}else{ echo $planFunction->function_plan_pronoun; }?></td>
                                            </tr>
                                            <?php }?>
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>
                        </div>


                     </div>
                    <div class="col-md-6">
                        <div class="content-style pd37">
                            <p class="title grey">Other informations </p>
                            <div class="row">

                                <?php
                                $peculalitys = getPeculality($home_sell_id);
                                $leftPeculality  = [];
                                $rightPeculality = [];
                                foreach ($peculalitys as $i => $peculality)
                                {
                                    if($i%2 == 0){
                                        $leftPeculality[] = $peculality;
                                    }elseif ($i%2 == 1){
                                        $rightPeculality[] = $peculality;
                                    }
                                }
                                ?>

                                <div class="col-md-6">
                                    <ul class="tbs1">
                                        <?php
                                        foreach ($leftPeculality as $peculality){
                                            ?>
                                            <li><?= $peculality->peculiarity_name_th ?></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>

                                <div class="col-md-6">
                                    <ul class="tbs1">
                                        <?php
                                        foreach ($rightPeculality as $peculality){
                                            ?>
                                            <li><?= $peculality->peculiarity_name_th ?></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!--gallery-->
            <?php
            $home_sell_gallery  = getGalleryHomeSellType($home_sell_id);
            $galleryHomeSells   = [];
            if($home_sell_gallery != false){
                switch ($home_sell_gallery->type_galery) {
                    case 1:
                        $original_gallerys  = getGalleryHomeSellTypeUpload($home_sell_id);
                        foreach ($original_gallerys as $original_gallery){
                            $buff['path'] = $original_gallery->path_upload_gallery;
                            $buff['desc'] = $original_gallery->dis_th;
                            $buff['seo']  = $original_gallery->alt_seo;
                            $galleryHomeSells[] = $buff;
                        }
                        break;
                    case 2:
                        $original_gallerys  = getHomeSellGalleryPlan($home_sell_id);
                        foreach ($original_gallerys as $original_gallery){
                            $buff['path'] = $original_gallery->galery_plan_name;
                            $buff['desc'] = $original_gallery->galery_plan_name_dis;
                            $buff['seo']  = $original_gallery->galery_plan_img_seo;
                            $galleryHomeSells[] = $buff;
                        }
                        break;
                    case 3:
                        $original_gallerys  = getGalleryHomeSellTypeFurnishedHome($home_sell_id);
                        foreach ($original_gallerys as $original_gallery){
                            $buff['path'] = $original_gallery->path_funished;
                            $buff['desc'] = $original_gallery->dis_th;
                            $buff['seo']  = $original_gallery->alt_seo;
                            $galleryHomeSells[] = $buff;
                        }
                        break;
                }
            }

            if($home_sell_gallery != false) {
                ?>
                <div id="sec-gallery" class="gallery-block">
                    <p class="heading-title">GALLERY</p>
                    <div class="tpl5-img">
                        <div id="tpl5-img">
                            <?php
                            $i = 1;
                            foreach ($galleryHomeSells as $galleryHomeSell) {
                                ?>
                                <div class="item tpl5-img-<?= $i ?>">
                                    <a class="gallery"
                                       href="<?= backend_url('base', $galleryHomeSell['path']) ?>">
                                        <img src="<?= backend_url('base', $galleryHomeSell['path']) ?>"
                                             alt="<?= $galleryHomeSell['seo'] ?>">
                                        <p class="title-gallery"><?= $galleryHomeSell['desc'] ?></p>
                                    </a>
                                </div>
                                <?php
                                $i++;
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
<!--floor plan-->
            <?php
            $floor_plans = getHomeSellFloorPlan($home_sell_id);
            if($floor_plans != false){
            ?>
            <div id="sec-floorplan" class="project-info-detail-block">
                <p class="heading-title">House plan</p>

                <div id="homePlanSlide">

                    <?php
                    foreach ($floor_plans as $i => $floor_plan){
                    ?>

                        <div class="item">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="#" data-featherlight="<?= backend_url('base',$floor_plan->floor_plan_img_name)?>">
                                        <img src="<?= backend_url('base',$floor_plan->floor_plan_img_name)?>" alt="">
                                        <span class="i-zoom"></span>
                                    </a>
                                </div>
                                <div class="col-md-6">
                                    <div class="dropdown">
                                        <div class="row">
                                            <ul class="tabs-menu-plan-slide dropdown-menu" aria-labelledby="dropdownMenu1">
                                                <?php
                                                if(count($floor_plans) >= 1) {
                                                    ?>
                                                    <li class="<?= $i == 0 ? 'current' : '' ?>">
                                                        <a data-value="0">
                                                            First floor
                                                        </a>
                                                    </li>
                                                    <?php
                                                }
                                                if(count($floor_plans) >= 2) {
                                                    ?>
                                                    <li class="<?= $i == 1 ? 'current' : '' ?>">
                                                        <a data-value="1">
                                                            Second floor
                                                        </a>
                                                    </li>
                                                    <?php
                                                }
                                                if(count($floor_plans) >= 3) {
                                                    ?>
                                                    <li class="<?= $i == 2 ? 'current' : '' ?>">
                                                        <a data-value="2">
                                                            Third floor
                                                        </a>
                                                    </li>
                                                    <?php
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab">
                                        <div id="tab-1" class="tab-content">
                                            <div class="tab-block content-style str_floor_plan<?= $i ?>">
                                                <ul class="floor_plan<?=$i?>">

                                                     <?=!empty($floor_plan->floor_plan_dis_th) ? '<li>'.str_replace(array("\n\r", "\n","n/g"), '</li><li>',  $floor_plan->floor_plan_dis_th) : ''; echo '</li>';?>

                                                </ul>
                                                <ul style="color: #999999;font-size: 15px;">
                                                    Note: the figure in the plan is the estimated approximately. The details of plan and house plan for cosideration You can consider the real details of the real house at the project site. The company also reserbe the rights to change the materials and detail without prior notice 
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>


                        <?php
                    }
                    ?>
                </div>
            </div>
                <?php
            }
            ?>
<!--condition-->
            <?php
                $conditionHomeSell = getConditionHomeSellByHomeSellID($home_sell_id);
            ?>
            <div id="sec-tabledetail" class="project-info-detail-block">
                <div class="smartcal-block">
                    <p class="heading-title">Table and conditions </p>
                    <a href="<?= $router->generate('calculator') ?>" target="_blank"
                       class="calculate-link"><i class="i-smcal"></i><span class="cal-txt">Calculation  the amount of monthly installments</span></a>
                    <div class="clearfix"></div>
                </div>
                <table class="tablesaw content-style table-1" data-tablesaw-mode="stack">
                    <thead>
                    <tr>
                        <th class="tb-title-1st" data-tablesaw-sortable-col>House style</th>
                        <?php if(!empty($conditionHomeSell->land_size)) { ?>
                        <th data-tablesaw-sortable-col>Size of land</th>
                        <?php } if(!empty($conditionHomeSell->total_price)){ ?>
                        <th data-tablesaw-sortable-col>Price</th>
                        <?php } if(!empty($conditionHomeSell->down && !empty($conditionHomeSell->money))){ ?>
                        <th data-tablesaw-sortable-col>Down <?= $conditionHomeSell->down ?> %</th>
                        <?php } if(!empty($conditionHomeSell->payments)){ ?>
                        <th data-tablesaw-sortable-col>Reservation</th>
                        <?php } if(!empty($conditionHomeSell->contract)){ ?>
                        <th data-tablesaw-sortable-col>Contract</th>
                        <?php } if(!empty($conditionHomeSell->transfer_money)){ ?>
                        <th data-tablesaw-sortable-col>Transfer Amount</th>
                        <?php } if(!empty($conditionHomeSell->starting_pay)){ ?>
                        <th data-tablesaw-sortable-col>installment / month  since …..</th>
                        <?php } if(!empty($conditionHomeSell->interest)){ ?>
                        <th data-tablesaw-sortable-col>interest%</th>
                        <?php } if(!empty($conditionHomeSell->repayment_period)){ ?>
                        <th data-tablesaw-sortable-col>Years</th>
                        <?php } ?>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="tb-plan-detail">
                            <?= $plan->plan_name_th ?>
                        </td>
                        <?php if(!empty($conditionHomeSell->land_size)) { ?>
                        <td><?= $conditionHomeSell->land_size ?> Sq.wah</td>
                        <?php } if(!empty($conditionHomeSell->total_price)){ ?>
                        <td><?= Helper::groupThousand($conditionHomeSell->total_price) ?> baht</td>
                        <?php } if(!empty($conditionHomeSell->down) && !empty($conditionHomeSell->money)) {?>
                        <td><?= Helper::groupThousand($conditionHomeSell->money) ?> baht</td>
                        <?php } if(!empty($conditionHomeSell->payments)) {?>
                        <td><?= Helper::groupThousand($conditionHomeSell->payments) ?> baht</td>
                        <?php } if(!empty($conditionHomeSell->contract)) { ?>
                        <td><?= Helper::groupThousand($conditionHomeSell->contract) ?> baht</td>
                        <?php } if(!empty($conditionHomeSell->transfer_money))  { ?>
                        <td><?= Helper::groupThousand($conditionHomeSell->transfer_money) ?> baht</td>
                        <?php } if(!empty($conditionHomeSell->starting_pay)) {?>
                        <td><?= Helper::groupThousand($conditionHomeSell->starting_pay) ?> baht</td>
                        <?php } if(!empty($conditionHomeSell->interest)) {?>
                        <td><?= $conditionHomeSell->interest ?> %</td>
                        <?php } if(!empty($conditionHomeSell->repayment_period)) {?>
                        <td><?= $conditionHomeSell->repayment_period ?></td>
                        <?php } ?>
                    </tr>
                    </tbody>
                </table>
                <p class="txt-remark">* Please check the terms, conditions and prices with the Project again.                                  <br>** For Interest rate, please check with the Bank again</p>

            </div>

            <?php


            $plan360   = get360Plan($plan->plan_id);
            $videoPlan = getVideoPlan($plan->plan_id);

            if($plan360 != false || $videoPlan != false){
                ?>
                <div class="project-info-detail-block">
                    <div class="row">
                        <?php
                        if($plan360 != false && $plan360->c360_plan_url != ""){
                            ?>
                            <div class="col-md-6">
                                <p class="heading-title">360 degree photos / images</p>
                                <div class="project-info-block">
                                    <div class="project-info-other-img">
                                        <a id="various" href="<?= $plan360->c360_plan_url ?>"><span class="i-view-360"></span><img src="<?= backend_url('base',$plan360->c360_img) ?>" alt=""></a>
                                        <script type="text/javascript">
                                            $(document).ready(function() {
                                                $("#various").fancybox({
                                                    'width'             : '100%',
                                                    'height'            : '100%',
                                                    'autoScale'         : false,
                                                    'transitionIn'      : 'none',
                                                    'transitionOut'     : 'none',
                                                    'type'              : 'iframe'
                                                });
                                            });
                                        </script>

                                    </div>
                                    <!--                                    <p class="title">EMPERY</p>-->
                                </div>
                            </div>
                            <?php
                        }
                        if($videoPlan != false){
                        if($videoPlan->clip_type == 'youtube' && $videoPlan->thumnail !='') {
                            ?>
                            <div class="col-md-6">
                                <p class="heading-title">VDO</p>
                                <div class="project-info-block">
                                    <div class="project-info-other-img">
                                        <?php
                                        if($videoPlan->clip_type == 'youtube') {
                                            $youtube_img = backend_url('base',$videoPlan->thumnail);
                                            $youtube_url = str_replace('watch?v=', 'embed/', $videoPlan->clip_plan_url);
                                            ?>
                                            <a href="#" data-featherlight="#lbVdo1">
                                                <span class="i-view-vdo"></span>
                                                <img src="<?= $youtube_img ?>" alt="">
                                            </a>
                                            <iframe class="lightbox" src="<?= $youtube_url . "?autoplay=0" ?>" width="1000"
                                                    height="560" id="lbVdo1" style="border:none;" webkitallowfullscreen
                                                    mozallowfullscreen allowfullscreen></iframe>
                                        <?php
                                        }elseif($videoPlan->clip_type == 'vdo'){
                                        $youtube_img = backend_url('base',$videoPlan->thumnail);
                                        ?>
                                            <a id="ban_vdo">
                                                <span class="i-view-vdo"></span>
                                                <img src="<?= $youtube_img ?>"
                                                     alt="" class="hm-highl">
                                            </a>
                                            <script>
                                                $(document).ready(function () {
                                                    $('#ban_vdo').click(function () {
                                                        $('#lbVdo').show();
                                                        $.featherlight($('#lbVdo'),{});
                                                        $('.featherlight-content #video_player')[0].play();
                                                        $('#lbVdo').hide();
                                                    });
                                                })
                                            </script>
                                            <div id="lbVdo" style="position:relative;width: 100%;display: none;">
                                                <video id='video_player' preload='none' controls>
                                                    <source src="<?= backend_url('base',$videoPlan->clip_plan_url) ?>" type="video/mp4">
                                                </video>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }}
                        ?>
                    </div>
                </div>
                <?php
            }
            ?>

<!--other project info sale-->
            <div class="other-prj-info-sale">
                <div class="sort-block">
                    <input type="hidden" name="sortSelect" value="">
                    <div class="sort" data-status="0">
                        <span>
                            <?php
                            $zones = getProjectViewZones(1);
                            if(!empty($_GET['zone_id'])){
                                foreach($zones as $zone) {
                                    if($_GET['zone_id'] == $zone->zone_id){
                                        echo $zone->zone_name_th;
                                    }
                                }
                            }else{
                                echo 'Watch house and sell more plots';
                            }
                            ?>
                        </span> <i class="i-sort"></i>
                    </div>
                    <ul id="sortLists">
                        <?php
                        if(!empty($homeSells)){
                            foreach($homeSells as $homeSell){
                                $lhPlan = getPlanByPlanID($homeSell->plan_id);
                                ?>
                                <?php
                                $numberpaln='';
                                if($homeSell->number_converter !=''){
                                    $numberpaln = ' หมายเลขแปลง ';
                                }

                                ?>
                                <?php

                                $url_see_more = $router->generate('home-sell-detail',[
                                    'plan_name'         =>  str_replace(' ','-',$lhPlan->plan_name_th),
                                    'home_sell_id'      =>  $homeSell->home_sell_id,
                                    'project_name'      =>  str_replace(' ','-',$project->project_name_th),
                                    'product_id'        =>  $product_id,
                                    'product_name'      =>  $type,
                                ]);
                                ?>
                                <li data-value="<?= $lhPlan->plan_name_th.$numberpaln.$homeSell->number_converter ?>" onclick="linkToUrl('<?= $url_see_more ?>')">
                                    <?= $lhPlan->plan_name_th.$numberpaln.$homeSell->number_converter ?>
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </div>

                <a href="<?=$url_see_more_project;?>" class="step-back">< Go to Project</a>
            </div>

        </div>
    </div>

    <?php
      $project_contact    = getProjectContactByProjectID($project_id);
      $email_contact          = $project_contact->email;
    ?>

    <input type="hidden" name="mailcont" id="mailcont" class="form-control" value="<?= $email_contact ?>" readonly >


<?php include('footer.php'); ?>
<script type="text/javascript">
    $(document).ready(function () {
    //Page Load Start
    bannerSlide();
    gallery();
    stickyMenuProject();
    homePlanSlide();
    var winW = $(window).width();

    if( winW <= 768 ) {
        homePlanSlide();
    }
    // projectPromoSlide();
    //Page Load End
    $('.gallery').featherlightGallery();

    $(document).ready(function()
    {
        $(document).resize();
    });
});


$(window).load(function () {
    var winW = $(window).width();

    if( winW <= 768 ) {
        homePlanSlide();
    }
});

//Function Start
function bannerSlide() {

    var winW = $(window).width();

    if( winW <= 768 ) {
        bannerThumb();
    }
    else {

        var isMulti = ($('#bannerSlide.owl-carousel img').length > 1) ? true : false
        $('#bannerSlide').owlCarousel({
            loop:isMulti,
            autoplay: false,
            autoplayTimeout: 500,
            autoplaySpeed: 1000,
            margin:5,
            animateOut: 'fadeOut',
            nav:false,
            dots: isMulti,
            video:true,
            lazyLoad:true,
            center:true,
            responsive:{
                0:{
                    items:1
                },

            }
        });
    }



}

function gridLayout5() {
    var wall = new Freewall("#tpl5-img");
    wall.reset({
        selector: '.item',
        animate: true,
        cellW: 300,
        cellH: 'auto',
        onResize: function() {
            wall.fitWidth();
        }
    });

    var images = wall.container.find('.item');
    images.find('img').load(function() {
        wall.fitWidth();
    });
}

function gallery() {
    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        //var stagePadding = 40;
        $('#tpl5-img').owlCarousel({
            loop:true,
            margin: 30,
            //stagePadding: stagePadding,
            nav:true,
            dots: false,
            autoHeight: true,
            responsive:{
                0:{
                    items:1
                },
                768:{
                    items:1
                }
            }
        });
    }
    else {
        gridLayout5();
    }
}

function homePlanSlide() {

    var winW = $(window).width();

    // responsive
    if( winW <= 768 ) {
        var stagePadding = 0;
    }
    else {
        var stagePadding = 0;
    }

    var isMulti = ($('#homePlanSlide .item').length > 1) ? true : false
    var homeSlider = $("#homePlanSlide");
    homeSlider.owlCarousel({
        loop:isMulti,
        margin: 10,
        stagePadding: stagePadding,
        nav:isMulti,
        dots: isMulti,
        autoHeight: false,
        responsive:{
            0:{
                items:1
            }
        },
    });
    homeSlider.on("click", ".tabs-menu-plan-slide a", function () {
        var value = $(this).attr('data-value');
        homeSlider.trigger('to.owl.carousel', [value]);
    });
}


// function projectPromoSlide() {
    // var winW = $(window).width();
    //
    // // responsive
    // if( winW <= 768 ) {
    //     var stagePadding = 40;
    //     var margin = 0;
    //     var autoH = true;
    // }
    // else {
    //     var stagePadding = 0;
    //     var margin = 20;
    //     var autoH = false;
    // }
    //
    // $('#projectPromoSlide').owlCarousel({
    //     loop:true,
    //     margin: margin,
    //     stagePadding: stagePadding,
    //     nav:true,
    //     dots: true,
    //     autoHeight: autoH,
    //     responsive:{
    //         0:{
    //             items:1
    //         }
    //     }
    // });
// }


function stickyMenuProject() {
    var winH = $(window).height();
    var pageH = winH - 60

    $('#main-nav li a').click(function() {
        $('#main-nav li a').removeClass('active');
        var id = $(this).attr('id');
        $(this).addClass('active');
         console.log(id);
        var margin_top_animate = $(this).attr('data-family') == "child" ? 115 : 60;
        $('html, body').animate({
            scrollTop: $('#sec-'+id).offset().top - margin_top_animate
        }, 1000);
    });

}


function bannerThumb() {

    var sync1 = $('#projectSlide');
    var sync2 = $('#imgThumb');
    var flag = false;
    var duration = 300;

    sync1.owlCarousel({
        items: 1,
        animateOut: 'fadeOut',
        nav: false,
        dots: false,
        //autoplay: true
    })
        .on('change.owl.carousel', function (e) {
            if (e.namespace && e.property.name === 'position' && !flag) {
                flag = true;
                sync2.trigger('to.owl.carousel', [e.relatedTarget.relative(e.property.value), duration, true]);
                flag = false;
            }
        });

    sync2.owlCarousel({
        items: 3,
        nav: true,
        slideBy: 3,
        dots: false,
        margin: 10,
    })
        .on('click', '.owl-item', function (e) {
            e.preventDefault();
            sync1.trigger('to.owl.carousel', [$(this).index(), duration, true]);
        })
        .on('change.owl.carousel', function (e) {
            if (e.namespace && e.property.name === 'position' && !flag) {
                flag = true;
                sync1.trigger('to.owl.carousel', [e.relatedTarget.relative(e.property.value), duration, true]);
                flag = false;
            }
        });

    sync2.on('click', '.owl-item', function(e) {
        e.preventDefault();
        $('.item').removeClass('active');
        $(this).find('.item').addClass('active');
    });


}

function linkToUrl(url) {
    window.location = String(url);
}

//Function End
</script>