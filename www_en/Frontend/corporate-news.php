<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>

<?php include('header.php'); ?>
<!-- CSS -->
<link rel="stylesheet" href="css/corporate-news.css" type="text/css">


<div id="content" class="content news-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">

                <h1>ข่าวสารองค์กร</h1>

                <div class="row">
                    <div class="col-md-6">
                        <ul>
                            <li><div class="news-title">07 ธันวาคม 2559</div>
                                กำหนดเวลาการใช้สิทธิใบสำคัญแสดงสิทธิที่จะซื้อหุ้นสามัญ (LH-W3)</li>
                            <li><div class="news-title">10 พฤศจิกายน 2559</div>
                                คำอธิบายและวิเคราะห์ของฝ่ายจัดการ ไตรมาสที่ 3 สิ้นสุดวันที่ 30 ก.ย. 2559</li>
                            <li><div class="news-title">10 พฤศจิกายน 2559</div>
                                งบการเงินไตรมาสที่ 3/2559</li>
                            <li><div class="news-title">10 พฤศจิกายน 2559</div>
                                สรุปผลการดำเนินงานของบจ.และรวมของบริษัทย่อย ไตรมาสที่ 3 (F45-3)</li>
                            <li><div class="news-title">10 พฤศจิกายน 2559</div>
                                แต่งตั้งประธานกรรมการบริหารความเสี่ยง
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-offset-1 col-md-5">
                        <img src="images/news/news_1.jpg" alt="" class="content-img">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include('footer.php'); ?>
