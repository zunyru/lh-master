<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>
<?php include('header.php'); ?>
    <!-- CSS -->
    <link rel="stylesheet" href="<?= file_path('css/concept.css') ?>" type="text/css">
    <!-- JS -->
    <script src="<?= file_path('js/concept.js') ?>"></script>


<!--    <div class="page-banner page-scroll">-->
<!--        <span id="scrollNext" class="i-scroll-next"></span>-->
<!---->
<!--        <div id="bannerSlide">-->
<!--            <div class="item">-->
<!--                <div class="banner-container banner-parallax" style="background-image: url(images/temp3/banner_concept.jpg);">-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->

    <div id="content" class="content concept-page">
        <div class="container">
            <h1>แนวคิดบ้านสบาย</h1>

            <div class="concept-container">
                <div class="row">
                    <div class="concept-lists">

                        <div class="concept-block">
                            <div class="col-md-6">
                                <div class="review-img cont-pr">
                                    <img src="<?= file_path('images/temp3/concept_3.jpg') ?>" alt="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="concept-detail cc-ct-mg15">
                                    <p class="title">บ้านสร้างเสร็จก่อนขาย</p>
                                    <p>บ้านสบายสร้างเสร็จก่อนขายจากแลนด์ แอนด์ เฮ้าส์ ช่วยให้คุณกำหนดการย้ายบ้านได้อย่างชัดเจน</p>
                                </div>
                            </div>
                        </div>

                        <div class="concept-block cc-block-pt35">
                            <div class="col-md-6 pull-right">
                                <div class="review-img">
                                    <img src="<?= file_path('images/temp3/concept_1.jpg') ?>" alt="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="concept-detail cont-pr cc-ct-mg25">
                                    <p class="title">ผนังบ้านสบาย</p>
                                    <p>ผนังของบ้านสบาย แลนด์ แอนด์ เฮ้าส์ จำแนกออกได้เป็น 2 กลุ่ม คือผนังคอนกรีตมวลเบา Q-CON และ ผนังคอนกรีต เสริมเหล็ก ทั้งนี้ ขึ้นอยู่กับลักษณะของแบบบ้านผนังทั้งสองแบบ ล้วนมีความแข็งแรง ทนทาน และมีประสิทธิภาพในการป้อง กันความร้อนจากภายนอกได้เป็นอย่างดี</p>
                                </div>
                            </div>
                        </div>


                        <div class="concept-block cc-block-pt35">
                            <div class="col-md-6">
                                <div class="review-img cont-pr">
                                    <img src="<?= file_path('images/temp3/concept_4.jpg') ?>" alt="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="concept-detail cc-ct-mg25">
                                    <p class="title">ฝ้าเพดาน</p>
                                    <p>ฝ้าเพดานคือแผ่นวัสดุที่ปิดใต้หลังคาหรือพื้นของอีกชั้นหนึ่ง เพื่อช่วยปิดโครงสร้างต่างๆที่ไม่เรียบร้อยและไม่น่าดูของส่วนใต้หลังคาและเพดานชั้นบนฝ้าเพดานของบ้านสบาย แลนด์ แอนด์ เฮ้าส์ เลือกใช้แผ่นยิปซัมคุณภาพสูง มีน้ำหนักเบา ทนไฟ ช่วยกรองความร้อนจากหลังคา ป้องกันเสียงจากชั้นบน และช่วยตกแต่งบรรยากาศภายในห้องให้สวยงามน่าอยู่</p>
                                </div>
                            </div>
                        </div>

                        <div class="concept-block cc-block-pt35">
                            <div class="col-md-6 pull-right">
                                <div class="review-img">
                                    <img src="<?= file_path('images/temp3/concept_5.jpg') ?>" alt="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="concept-detail cont-pr cc-ct-mg25">
                                    <p class="title">กระจกเขียวตัดแสง</p>
                                    <p>บ้านสบายของ แลนด์ แอนด์ เฮ้าส์ ได้เตรียมรับมือกับความ ร้อนจากแสงอาทิตย์และรังสี UV โดยการเลือกใช้บานกระจก เขียวตัดแสง ซึ่งมีคุณสมบัติเด่น คือ สามารถป้องกันรังสี UV และดูดกลืนพลังงานความร้อนจากดวงอาทิตย์ที่ส่องมากระทบผิวกระจกได้ถึงร้อยละ 35-50 จึงช่วยลดภาระการใช้เครื่องปรับ อากาศลงและช่วยประหยัดค่าไฟฟ้า</p>
                                </div>
                            </div>
                        </div>

                        <div class="concept-block cc-block-pt35">
                            <div class="col-md-6">
                                <div class="review-img cont-pr">
                                    <img src="<?= file_path('images/temp3/concept_6.jpg') ?>" alt="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="concept-detail cc-ct-mg25">
                                    <p class="title">ห้องน้ำของบ้านสบาย</p>
                                    <p>ห้องน้ำของบ้านสบายจากแลนด์ แอนด์ เฮ้าส์ ได้รับการ ออกแบบโดยยึดสุขอนามัยที่ดีด้วยการแยกส่วนเปียก-แห้งออกจากกัน มีการระบายอากาศที่ดี มีช่องแสงเปิดรับแสงธรรมชาติ เพื่อช่วยฆ่าเชื้อโรคและช่วยลดมลพิษภายในห้องน้ำได้เป็นอย่างดี</p>
                                </div>
                            </div>
                        </div>

                        <div class="concept-block cc-block-pt35">
                            <div class="col-md-6 pull-right">
                                <div class="review-img">
                                    <img src="<?= file_path('images/temp3/concept_7.jpg') ?>" alt="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="concept-detail cont-pr cc-ct-mg25">
                                    <p class="title">เพิ่มประตูทางออกเฉลียงด้านข้างสู่สวน</p>
                                    <p>เปิดรับกับวิวของสวนสวยรอบบ้านได้ง่ายขึ้น กับประตูทางออก เฉลียง ด้านข้างสู่สวน ของบ้านสบายจากแลนด์ แอนด์ เฮ้าส์ ที่โปร่งโล่ง สบายตายิ่งกว่า</p>
                                </div>
                            </div>
                        </div>

                        <div class="concept-block cc-block-pt35">
                            <div class="col-md-6">
                                <div class="review-img cont-pr">
                                    <img src="<?= file_path('images/temp3/concept_8.jpg') ?>" alt="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="concept-detail cc-ct-mg25">
                                    <p class="title">ระบบประตู หน้าต่างอลูมิเนียมพิเศษ & ไวนิล</p>
                                    <p>วงกบ & บานประตู-หน้าต่าง อลูมิเนียมพิเศษและไวนิล ซึ่งได้รับการติดตั้งสำหรับบ้านสบาย ของ แลนด์ แอนด์ เฮ้าส์ มีคุณสมบัติเด่นคือ ช่วยขจัดปัญหาปลวกมอดแมลงไม่กัดกิน มีความทนทานต่อแสงแดด ลม และฝน ไม่มีปัญหาเรื่องการบิด งอหรือยืดหดตัวจากความชื้นและการรั่วซึมของน้ำฝน และ สามารถดูแลรักษาได้สะดวกง่ายดาย</p>
                                </div>
                            </div>
                        </div>

                        <div class="concept-block cc-block-pt35">
                            <div class="col-md-6 pull-right">
                                <div class="review-img">
                                    <img src="<?= file_path('images/temp3/concept_9.jpg') ?>" alt="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="concept-detail cc-ct-mg25">
                                    <p class="title">ตกแต่งสวนสวย</p>
                                    <p>บ้านสบายของแลนด์ แอนด์ เฮ้าส์ ได้จัดเตรียมสวนสวยโดย ภูมิสถาปนิกไว้ให้เพื่อการพร้อมเข้าอยู่ ภายในบ้านมากยิ่งขึ้น</p>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="">
                        <div class="concept-block">
                            <div class="col-md-6">
                                <div class="concept-detail">
                                    <ul>
                                        <li>
                                            <p class="title">ฝ้าระแนงใต้หลังคา</p>
                                            <p>บ้านสบายของ แลนด์ แอนด์ เฮ้าส์ ได้เลือกใช้ฝ้าระแนงสำเร็จรูป ซึ่งผลิตจากวัสดุ พิเศษที่มีความทนทาน จึงไม่มีปัญหาเรื่องปลวกกิน ไม่ผุกร่อน ไม่บวมน้ำ แข็งแรงทนทาน ไม่บิดงอ ไม่ซีดจาง และมีอายุการ ใช้งานยาวนาน สามารถดูแลรักษาทำความสะอาดได้ง่าย</p>
                                        </li>
                                        <li>
                                            <p class="title">มุ้งลวดผลิตด้วยวัสดุพิเศษ Fiber</p>
                                            <p>มุ้งลวดของบ้านสบาย แลนด์ แอนด์ เฮ้าส์ มีความคงทน สวยงาม สีดำกันการสะท้อนแสง ไม่เป็นสนิม ไม่ดูดซับ ความร้อน ทำความสะอาดง่าย</p>
                                        </li>
                                        <li>
                                            <p class="title">ระบบไฟฉุกเฉินและตัดไฟอัตโนมัติ</p>
                                            <p>ไฟฉุกเฉินแบบดาวน์ไลท์ ตามจุดต่างๆ เช่น ห้องนอนใหญ่โถง บันได เมื่อไฟฟ้าขัดข้อง เครื่องตัดกระแสไฟอัตโนมัติ ช่วยคุณได้ทันที</p>
                                        </li>
                                        <li>
                                            <p class="title">หลังคาบ้านสบาย ของ แลนด์ แอนด์ เฮ้าส์ </p>
                                            <p>ได้รับการออกแบบและก่อสร้างมาอย่างดี โดยเลือกใช้กระเบื้องมาตรฐานสูงและผ่านการตรวจรับรองคุณภาพทุกหลัง ปัญหาการรั่วซึมจึงเกิดขึ้นได้ยากนอกจากนั้น ยังได้ติดตั้ง แผ่นสะท้อนความร้อนใต้หลังคา เพื่อป้องกันความร้อนจากแสงแดดไว้ให้เรียบร้อย</p>
                                        </li>
                                        <li>
                                            <p class="title">ห้องพระหรือหิ้งพระเป็นสัดส่วน</p>
                                            <p>บ้านสบายของแลนด์ แอนด์ เฮาส์ ได้ออกแบบพื้นที่เป็นสัดส่วน สำหรับห้องพระหรือหิ้งพระโดยเฉพาะ ถูกต้องตามทิศและ การวางตำแหน่ง</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="concept-block">
                            <div class="col-md-6">
                                <div class="concept-detail">
                                    <ul>
                                        <li>
                                            <p class="title">ระบบสำรองน้ำของบ้านสบายจากแลนด์ แอนด์ เฮ้าส์ </p>
                                            <p>ได้มีการติดตั้ง ตอกเสาเข็มรองรับน้ำหนักไว้อย่างแข็งแรง รวมถึงได้เลือกใช้ถังเก็บน้ำ รวมถึงอุปกรณ์ข้อต่อ ก๊อก วาล์ว ที่มีคุณภาพ โดยมีท่อต่อไปยังเครื่องปั๊มน้ำ ที่ทำงานสูบจ่าย น้ำโดยอัตโนมัติ โดยเราได้เลือกใช้ปั๊มน้ำคุณภาพสูง มีระบบ ป้องกันมิเตอร์ไหม้ และมีถังแรงดันไดอะแฟรมที่ช่วยยืดอายุ การใช้งานของมอเตอร์</p>
                                        </li>
                                        <li>
                                            <p class="title">ระบบกำจัดปลวก</p>
                                            <p>บ้านสบาย ของแลนด์ แอนด์ เฮ้าส์ ได้เตรียมระบบกำจัดปลวก ใต้ดิน โดยเดินท่อ P.E ไว้ใต้รอบคานคอดิน จึงไม่ต้องเจาะ พื้นบ้าน สะดวกในการพ่นน้ำยากำจัดปลวก ไม่มีสารตกค้าง ภายในตัวบ้าน</p>
                                        </li>
                                        <li>
                                            <p class="title">ถังขยะริมรั้ว พร้อมตู้ไปรษณีย์ออกแบบพิเศษ</p>
                                            <p>จัดเตรียมจุดวางขยะไว้อย่างเป็นสัดส่วน พร้อมตู้ไปรษณีย์ที่มี ช่องรับและเก็บจดหมายไว้เป็นอย่างดี</p>
                                        </li>
                                        <li>
                                            <p class="title">แปลนบ้าน</p>
                                            <p>เราได้อำนวยความสะดวก เผื่อการต่อเติมบ้านของผู้อยู่อาศัย โดยเตรียมแปลนบ้านไว้ให้ในรูปแบบซีดีรอมเพื่อง่ายต่อการ เรียกใช้งาน</p>
                                        </li>
                                        <li>
                                            <p class="title">เครื่องปรับอากาศ</p>
                                            <p>บ้านสบายของแลนด์ แอนด์ เฮ้าส์ได้เพิ่มความสะดวกสบาย โดยติดตั้งเครื่องปรับอากาศไว้ทุกหลัง และได้ออกแบบพื้นที่ สำหรับวาง Air Compressor เพื่อความเหมาะสมและสวยงาม</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </div>
    </div>


<?php include('footer.php'); ?>