<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';
require_once 'include/help/hex2rgba.php';

$project_id = $_GET['project_id'];
$product_id = $_GET['product_id'];

if (empty($project_id) || getProjectByID($project_id) == false) {
    header('Location: '.Helper::url_string('project-type.php'));
}

$lang = !empty($_GET['lang']) ? $_GET['lang'] : 'th';

?>
<?php include('header.php'); ?>

<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '342952452528044'); // Insert your pixel ID here.
    fbq('track', 'PageView');
    fbq('track', 'Lead');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=342952452528044&ev=PageView&noscript=1"
   /></noscript>
   <!-- DO NOT MODIFY -->
   <!-- End Facebook Pixel Code -->


   <!-- CSS -->
   <link rel="stylesheet" href="<?= file_path('css/project-info-home.css') ?>" type="text/css">
   <link rel="stylesheet" href="<?= file_path('bxslider/jquery.bxslider.css') ?>" type="text/css">
   <link rel="stylesheet" href="<?= file_path('css/video.css') ?>" type="text/css">
   <link rel="stylesheet" href="<?= file_path('js/cal_reg/css/sweetalert.css') ?>" type="text/css">
   <!-- JS -->
   <script type="text/javascript" src="<?= file_path('fancybox/jquery.mousewheel-3.0.4.pack.js') ?>"></script>
   <link rel="stylesheet" href="<?= file_path('fancybox/fancybox.min.css') ?>" />
   <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>
   <script src="<?= file_path('../build/js/jquery.validate.js') ?>" /></script>
   <script src="<?= file_path('js/cal_reg/jquery.mask.min.js') ?>" /></script>
   <script src="<?= file_path('js/cal_reg/sweetalert.min.js') ?>" /></script>


   <?php
   $project            = getProjectByID($project_id);
   if(!empty($project->latitude) && !empty($project->longtitude) && $project->project_status != 'SO'){
    ?>
    <script>
        /*function initMap() {
            // Create a map object and specify the DOM element for display.
            $(document).ready(function () {
                var lat_proj = parseFloat($('#lat').val());
                var lng_proj = parseFloat($('#lng').val());
                var pos = {lat: lat_proj, lng: lng_proj};
                var map = new google.maps.Map(document.getElementById('map'), {
                    center: pos,
                    scrollwheel: false,
                    zoom: 16,
                    zoomControlOptions: {
                        position: google.maps.ControlPosition.TOP_RIGHT
                    },
                    streetViewControl: false,
                    mapTypeControl: false,
                    styles: [{"elementType": "geometry", "stylers": [{"color": "#f5f5f5"}]}, {
                        "elementType": "labels.icon",
                        "stylers": [{"visibility": "off"}]
                    }, {
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#616161"}]
                    }, {
                        "elementType": "labels.text.stroke",
                        "stylers": [{"color": "#f5f5f5"}]
                    }, {
                        "featureType": "administrative.land_parcel",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#bdbdbd"}]
                    }, {
                        "featureType": "administrative.locality",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#c19838"}]
                    }, {
                        "featureType": "administrative.neighborhood",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#c19838"}]
                    }, {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [{"color": "#eeeeee"}]
                    }, {
                        "featureType": "poi",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#757575"}]
                    }, {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [{"color": "#e5e5e5"}]
                    }, {
                        "featureType": "poi.park",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#9e9e9e"}]
                    }, {
                        "featureType": "road",
                        "elementType": "geometry",
                        "stylers": [{"color": "#ffffff"}]
                    }, {
                        "featureType": "road",
                        "elementType": "labels.icon",
                        "stylers": [{"visibility": "simplified"}]
                    }, {
                        "featureType": "road",
                        "elementType": "labels.text",
                        "stylers": [{"color": "#878787"}]
                    }, {
                        "featureType": "road",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#9f720a"}, {"visibility": "off"}]
                    }, {
                        "featureType": "road.arterial",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#757575"}]
                    }, {
                        "featureType": "road.highway",
                        "elementType": "geometry",
                        "stylers": [{"color": "#dadada"}]
                    }, {
                        "featureType": "road.highway",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#616161"}]
                    }, {
                        "featureType": "transit.line",
                        "elementType": "geometry",
                        "stylers": [{"color": "#e5e5e5"}]
                    }, {
                        "featureType": "transit.station",
                        "elementType": "geometry",
                        "stylers": [{"color": "#eeeeee"}]
                    }, {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [{"color": "#c9c9c9"}]
                    }, {"featureType": "water", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]}]
                });
var icon = {
    url: '<?= file_path('images/global/icon_google_map.png') ?>',
                    scaledSize: new google.maps.Size(30, 42), // scaled size
                    origin: new google.maps.Point(0, 0), // origin
                    anchor: new google.maps.Point(0, 0) // anchor
                };
                var marker = new google.maps.Marker({
                    position: pos,
                    map: map,
                    icon: icon,
                    title: '<?= $project->project_name_th?>',

                });

                var infowindow = new google.maps.InfoWindow({});

                infowindow.close();
                infowindow.setOptions({
                    content: '<?= $project->project_name_th?>',
                    position: new google.maps.LatLng(lat_proj,lng_proj),
                    pixelOffset: new google.maps.Size(15,10),
                });
                infowindow.open(map);


                google.maps.event.addListener(marker, 'click', function() {
                    window.open('https://maps.google.com?saddr=Current+Location&daddr='+lat_proj+','+lng_proj,'_blank');
                });


            });
}*/
</script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDaz8JbxbUyy8acSAPy4ENzPhCaY4o_kj4&callback=initMap"
async defer></script>
 -->
<?php
}
?>

<script src="<?= file_path('bxslider/jquery.bxslider.min.js') ?>"></script>
<script src="<?= file_path('js/project-info-home.js') ?>"></script>
<script src="<?= file_path('js/imagesloaded.pkgd.min.js') ?>"></script>



<div class="page-banner page-scroll slider" >

    <div id="pjHomeBanner" class="owl-carousel owl-theme" style="position: relative!important;top:0!important;">
        <?php
        $logo               = getProjectMaps($project_id);
        $project_concept    = getProjectConcept($project_id);
        //lead image by zun
        $banner_project_default = getAllBannerByProjectID_Default($project_id);
        $banner_projects    = getAllBannerByProjectID($project_id);
        $banner_homes       = null;

        if($banner_project_default > 0){

            foreach ($banner_projects as $i => $banner_project) {

                if ($banner_project->LEAD_IMAGE_PROJECT_FILE_TYPE == 'image_leade_banner') {
                    ?>
                    <div class="item slide">
                        <div class="banner-container banner-parallax">
                           <?=$banner_project->img_url != ''? '<a href="'.$banner_project->img_url.'" target="_blank">' : '';?>
                            <img class="<?= $i == 0 ? 'background-show' : '' ?>" src="<?= backend_url('base', $banner_project->LEAD_IMAGE_PROJECT_FILE_NAME) ?>" alt="<?=$banner_project->seo_lead?>">
                            <?=$banner_project->img_url != ''? '</a>' : '';?>
                        </div>
                        <?php if($i == 0) {
                            ?>

                            <?php
                        }?>
                    </div>
                    <?php
                } elseif ($banner_project->LEAD_IMAGE_PROJECT_FILE_TYPE == 'youtube') {
                    $lead_img = backend_url('base', $banner_project->banner_img_thum);
                    $lead_youtube_url = str_replace('watch?v=', 'embed/', $banner_project->LEAD_IMAGE_PROJECT_FILE_NAME);
                    ?>
                    <div class="item slide">
                        <div class="item-video">
                            <div class="banner-container banner-parallax" id="bannerVideo<?= $i ?>">
                                <div class="rps-vdobg">
                                    <img src="<?= $lead_img ?>" alt="<?=$banner_project->seo_lead?>">
                                </div>

                                <div class="vdo-control">
                                    <div class="play-block">
                                        <span id="vdoPlay<?= $i ?>" class="vdo-icon vdoControl vdoPlay"></span>
                                    </div>
                                </div>

                                <script type="text/javascript">
                                    var iframe_src = '<?= $lead_youtube_url ?>';
                                    var youtube_video_id = iframe_src.match(/youtube\.com.*(\?v=|\/embed\/)(.{11})/).pop();
                                    if (youtube_video_id.length == 11) {
                                        $('#vdoPlay<?=$i?>').click(function () {
                                            $("#slideVdo<?=$i?>").remove();
                                            var video_iframe = $('' +
                                                '<iframe id="video" width="100%" height="720px"' +
                                                ' src="<?= $lead_youtube_url . "?autoplay=0" ?>">' +
                                                '</iframe>');
                                            $('#bannerVideo<?=$i?>').find('.vdo-control').remove();
                                            $('.rps-vdobg').remove();
                                            $('#bannerVideo<?=$i?>').append(video_iframe);
                                            $(this).trigger('stop.autoplay.owl');
                                            $('#scrollNext').hide();
                                        });
                                    }
                                </script>
                            </div>
                        </div>

                    </div>

                    <?php
                } elseif ($banner_project->LEAD_IMAGE_PROJECT_FILE_TYPE == 'vdo') {
                    ?>
                    <div class="item slide">
                        <script>
                            $(document).ready(function () {
                                $('#vdoPlay<?= $i ?>').click(function () {
                                    $('#slideVdo<?= $i ?>').css('display', 'block');
                                    $('#slideVdo<?= $i ?>')[0].play();
                                    $(this).fadeOut(200);
                                    $('#vdoPause<?= $i ?>').fadeIn(200);
                                    $('.vid_bg').remove();

                                    $(".vdo-control").mouseenter(function (event) {
                                        event.stopPropagation();
                                        $('#vdoPause<?= $i ?>').addClass("btnshown");
                                    }).mouseleave(function (event) {
                                        event.stopPropagation();
                                        $('#vdoPause<?= $i ?>').removeClass("btnshown");
                                    });
                                    $('#scrollNext').hide();
                                });

                                $("#vdoPause<?= $i ?>").click(function () {
                                    $('#slideVdo<?= $i ?>').get(0).pause();
                                    $(this).fadeOut(200);
                                    $('#vdoPlay<?= $i ?>').fadeIn(200);
                                    $('#scrollNext').show();
                                });
                            });
                        </script>
                        <div class="banner-container banner-parallax">
                            <img class="vid_bg" src="<?= backend_url('base', $banner_project->banner_img_thum) ?>" alt="<?=$banner_project->seo_lead?>">
                            <video loop="loop" id="slideVdo<?= $i ?>" class="" style="display:none;">
                                <source src="<?= backend_url('base', $banner_project->LEAD_IMAGE_PROJECT_FILE_NAME) ?>" type="video/mp4">
                                </video>

                                <div class="vdo-control">
                                    <div class="play-block">
                                        <span id="vdoPlay<?= $i ?>" class="vdo-icon vdoControl vdoPlay"></span>
                                        <span id="vdoPause<?= $i ?>" class="vdo-icon vdoControl vdoPause"></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php
                    } elseif ($banner_project->LEAD_IMAGE_PROJECT_FILE_TYPE == 'banner') {
                        ?>
                        <div class="item slide" <?php if (!empty($banner_project->lead_img_url)) echo "onclick=\"location.href='" . $banner_project->lead_img_url . "'\""; ?>>
                            <div class="banner-container banner-parallax">
                                <img class="background-show" src="<?= backend_url('base',$banner_project->LEAD_IMAGE_PROJECT_FILE_NAME)?>" alt="">

                                <?php
                                if(!empty($banner_project->banner_text)){
                                    ?>
                                    <div <?= !empty($banner_project->banner_link) ? 'href="'.$banner_project->banner_link.'"' : '' ?>
                                        class="banner-activity"
                                        <?php if(!empty($banner_project->backgroup_color)) {
                                            ?>
                                            style="background-color: <?php echo hex2rgba($banner_project->backgroup_color, 0.8);?> "
                                            <?php
                                        }?>
                                        >
                                        <?= $banner_project->banner_text ?>
                                        <?php if(!empty($banner_project->banner_link)) {
                                            ?>
                                            <span class="link-acty"></span>
                                            <?php
                                        }?>
                                        <span class="close-acty"></span>
                                    </div>
                                    <?php
                                }
                                ?>

                            </div>
                            <?php if($i == 0) {
                                ?>

                                <?php
                            }?>
                        </div>
                        <?php
                    }
                    ?>

                    <?php
        }//END foreach

        ?>
        
        <!-- Lead image default -->
        <?php
    }else{
        foreach ($banner_projects as $i => $banner_project) {
            if ($banner_project->LEAD_IMAGE_PROJECT_FILE_TYPE == 'image') {
                //print_r($banner_project);
                ?>
                <div class="item slide">
                    <div class="banner-container banner-parallax">
                       <?=$banner_project->img_url != ''? '<a href="'.$banner_project->img_url.'" target="_blank">' : '';?>
                      
                        <img class="<?= $i == 0 ? 'background-show' : '' ?>" src="<?= backend_url('base', $banner_project->LEAD_IMAGE_PROJECT_FILE_NAME) ?>" alt="<?=$banner_project->seo_lead?>">
                       <?=$banner_project->img_url != ''? '</a>' : '';?> 
                    </div>
                    <?php if($i == 0) {
                        ?>

                        <?php
                    }?>
                </div>
                <?php
            }
        }//END for image defualt
    }//END if check image defualt
    ?> 
    <!-- // END Lead image default -->

</div>

<!-- zun default-->
<div id="pjHomeBanner_default" class="owl-carousel owl-theme" style="position: relative!important;top:0!important;">
   <?php
   foreach ($banner_projects as $i => $banner_project) {
       if ($banner_project->LEAD_IMAGE_PROJECT_FILE_TYPE == 'image') {
           //print_r($banner_project);
           ?>
           <div class="item slide">
               <div class="banner-container banner-parallax">
                  <?=$banner_project->img_url != ''? '<a href="'.$banner_project->img_url.'" target="_blank">' : '';?>
                 
                   <img class="<?= $i == 0 ? 'background-show' : '' ?>" src="<?= backend_url('base', $banner_project->LEAD_IMAGE_PROJECT_FILE_NAME) ?>" alt="<?=$banner_project->seo_lead?>">
                  <?=$banner_project->img_url != ''? '</a>' : '';?> 
               </div>
               <?php if($i == 0) {
                   ?>

                   <?php
               }?>
           </div>
           <?php
       }
   }
   ?>
</div> 
<!-- end zun -->
<span id="scrollNext" class="i-scroll-next"></span>
</div>

<?php
//elementSharedFacebookProjectDetail($project_id,$banner_projects);
?>

<?php
$banner_homes = !empty($banner_homes) ? $banner_homes : $banner_projects;
if(!empty($banner_homes)){
    foreach ($banner_homes as $i => $banner){
        if(!empty($banner->banner_text)) {
            ?>
            <div 
            class="banner-activity banner-rsp"
            <?php if(!empty($banner->backgroup_color)) {
                ?>
                style="background-color: <?php echo hex2rgba($banner_project->backgroup_color, 0.8);?>"
                <?php
            }?>
            >
            <?= $banner->banner_text ?>
            <?php if(!empty($banner->banner_link)) {
                ?>
                <span class="link-acty"></span>
                <?php
            }?>
            <span class="close-acty"></span>
        </div>
        <?php
        break;
    }
}
}
?>

<!-- Responsive Submenu-->
<div class="proj-rps-submenu-container">
    <p>Project Information</p>
    <div class="proj-rps-submenu">
        <?php include('snippet/snippet-menu-project-info-home.php'); ?>
    </div>
</div>
<!-- //Responsive Submenu-->

<?php
$projectMap = getProjectMaps($project_id);
$project = getProjectByID($project_id);
$project_price = getProjectPrice($project_id);
$project_concept = getProjectConcept($project_id);
$project_promotion = getProjectPromotion($project_id);
$brand = getBrandByID($project->brand_id);
$project_areas = getProjectAreaByProjectID($project_id);
$logo          = getProjectMaps($project_id);
//$icon_activities = !empty($home_sell->icon_id) ? getIconActivity($home_sell->icon_id) : null;
?>
<div id="sec-information" class="content project-info-page">

    <div class="project-info-container" <?php if($project->project_status == 'SO') { echo 'style="background: #fff;"'; } ?> >
        <div class="container">
            <div class="proj-container">
                <!-- H1 -->
                <?php 

                $SEO = getSEOUrl($actual_link);
                $url_ = str_replace("%2F","/",urldecode(@$SEO->url_page));
                $url_ =  str_replace("%3A",":",urldecode($url_));

                if($actual_link == @$SEO->url_page || $actual_link == $url_){
                    echo '<h1>'.$SEO->h1.'</h1>'; 
                }
                ?>
                <!-- End H1 -->
                <div class="row">
                    <div class="info-logo-img">
                        <img src="<?= is_object($logo) ? backend_url('base', $logo->project_logo) : '' ?>" alt="" >
                    </div>
                </div>



                <?php if($project->project_status == 'SO') {
                    ?>
                    <div class="tagbox2_sold soldout-tag">
                        <div class="tag2">sold<br>out</div>
                    </div>
                    <?php
                }?>

                <?php if($project->project_status != 'SO'){ ?>

                <div class="row mb15">
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                        <p class="subtitle green">Project Information</p>
                    </div>
                    <div class="col-md-6">
                        <p><b><span class="line-space "><?php if($project != false) echo $project->project_name_th ?></span></b><br>
                            <?php if(!empty($project->area_farm)) { ?>
                            <span class="line-space">Project's area  
                                <?php if((!empty($project->area_farm))&& ($project->area_farm != ''))  echo $project->area_farm.' rai' ?>
                                <?php if((!empty($project->area_square_m)) && ($project->area_square_m != ' ')) echo $project->area_square_m.' ngan' ?>
                                <?php if((!empty($project->area_square_w)) && ($project->area_square_w != ' ')) echo $project->area_square_w.'  Sq.wah' ?>
                            </span>
                            <?php if(!empty($project->area_land)) { ?>
                            <span class="line-space">Number of total plots for sale <?= $project->area_land ?> </span><br>
                            <?php } ?>
                            <br>
                            <?php } ?>
                            <?php if(!empty($project_areas[0]->area_home) || $project_areas[0]->area_home != " "){?>
                            <span class="line-space">Residential type in the project</span><br>

                            <?php 
			    	if(sizeof($project_areas) == 0){
                                      $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
                                      $uri_segments = explode('/', $uri_path); 
                                        if($uri_segments[2] != 'singlehome'){
                                          $type = 'townhouse';
                                        }else{
                                          $type = 'singlehome';
                                        }
                                        echo '<span class="line-space">Type'.$type;  
                                    }
			       foreach ($project_areas as $project_area){
                                $product_name       = Helper::getProductNameTHByType($project_area->product_id);
                                $num_plan_home      = str_replace(' ','',$project_area->num_plan_home);
                                $area_home          = str_replace(' ','',$project_area->area_home);
                                $area_square_w_home = str_replace(' ','',$project_area->area_square_w_home);
                                ?>

                               
                                <span class="line-space">Type <?= $product_name ?>
                                 <?php if($num_plan_home != 0 || !isset($num_plan_home)){?>  
                                    <?php if(!empty($project_area->num_plan_home)) {
                                        ?>
                                        Type of house plan in the project <?= $num_plan_home ?> types
                                        <?php
                                    }?>
                                </span><br>
                               <?php } ?>

                                <?php if(!empty($area_home) || !empty($area_square_w_home)) { ?>
                                <span class="line-space">
                                    <?php if(!empty($area_home)) {
                                        ?>
                                        Utility area ranging from  <?= $area_home ?> sq.m.
                                        <?php
                                    }?>
                                    <?php if(!empty($product_name)) {
                                        ?>
                                        Land size ranging from<?= $area_square_w_home ?> Sq.wah or more 
                                        <?php
                                    }?>
                                </span><br>

                                <?php }} ?>
                                <?php
                            }
                            ?>
                        </p>
                        <?php if(!empty($project_price->project_price) || !empty($project_price->project_price)) { ?>
                        <?php 
                            if($project_price->price_mode == '1'){
                                $mode_price = 'Starting price from';
                            }else{
                                $mode_price = 'price';
                            }
                        ?>
                        <?php if($project_price->project_price != 0 || !isset($project_price->project_price)){?>
                        <p class="subtitle"><?=$mode_price?> <?php if($project_price != false) echo Helper::getProjectPrice($project_price->project_price,$project_price->price_mode) ?> MB</p>
                        <?php } ?>
                        <?php } ?>
                    </div>
                    <div class="col-md-2"></div>
                </div>

                <?php if (!empty($project_concept->concept_en) || !empty($project_concept->concept_th)) { ?>
                <div class="row mb15">
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                        <p class="subtitle green">Project's concept</p>
                    </div>
                    <div class="col-md-6">

                        <?php if(!empty($project_concept->concept_th)) { ?>
                        <p class="subtitle distinctive1">
                            <?php if($project_concept != false)  {
                                ?>
                                <?=!empty($project_concept->concept_th) ? str_replace(array("\n\r", "\n","n/g"), '<br/>', $project_concept->concept_th) : ''?>
                                            <!-- <script type="text/javascript">
                                                $('.distinctive1').html(`<?php //echo $project_concept->concept_th?>`.replace(/\r?\n/g, '<br/>'));
                                            </script> -->
                                            <?php
                                        }?>
                                    </p>
                                    <?php } ?>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <?php } ?>

                            <?php if (!empty($project_concept->distinctive_th)) { ?>
                            <div class="row mb15">
                                <div class="col-md-2"></div>
                                <div class="col-md-2">
                                    <p class="subtitle green">Project's highlight</p>
                                </div>
                                <div class="col-md-6 distinctive2" >
                                    <ol>
                                        <p><?php if($project_concept != false) ?>
                                            <?=!empty($project_concept->distinctive_th) ? str_replace(array("\n\r", "\n","n/g"), '<br/>', $project_concept->distinctive_th) : ''?>
                                        <!-- <script type="text/javascript">
                                            $('.distinctive2').html(`<?php //echo $project_concept->distinctive_th?>`.replace(/\r?\n/g, '<br/>'))
                                        </script> -->

                                    </p>
                                </ol>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <?php } ?>

                        <?php if (!empty($project_concept->facilities_th)) { ?>
                        <div class="row mb15">
                            <div class="col-md-2"></div>
                            <div class="col-md-2">
                                <p class="subtitle green">Facilities</p>
                            </div>
                            <div class="col-md-6 distinctive3">
                                <p><?php if($project_concept != false)  ?>
                                 <?=!empty($project_concept->facilities_th) ? str_replace(array("\n\r", "\n","n/g"), '<br/>', $project_concept->facilities_th) : ''?>
                                   <!--  <script type="text/javascript">
                                        $('.distinctive3').html(`<?php //echo $project_concept->facilities_th?>`.replace(/\r?\n/g, '<br/>'))
                                    </script> -->
                                </p>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <?php } ?>

                        <?php if (!empty($project_concept->security_th)) { ?>
                        <div class="row mb15">
                            <div class="col-md-2"></div>
                            <div class="col-md-2">
                                <p class="subtitle green">Security System</p>
                            </div>
                            <div class="col-md-6 distinctive4">
                                <p><?php if($project_concept != false)  ?>
                                    <?=!empty($project_concept->security_th) ? str_replace(array("\n\r", "\n","n/g"), '<br/>', $project_concept->security_th) : ''?>
                                    <!-- <script type="text/javascript">
                                        $('.distinctive4').html(`<?php //echo $project_concept->security_th;?>`.replace(/\r?\n/g, '<br/>'))
                                    </script> -->
                                </p>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <?php } ?>

                        <?php
                        if($projectMap != false && !empty($projectMap->brochure)){
                            ?>
                            <div class="row mb15">
                                <div class="col-md-2"></div>
                                <div class="col-md-2">
                                    <p class="subtitle green"></p>
                                </div>
                                <div class="col-md-6 distinctive3">
                                    <a href="<?= backend_url('base',$projectMap->brochure) ?>"
                                       target="_blank"
                                       class="dl-brochure">
                                       <i class="i-download"></i>
                                       Download Brochure
                                   </a>
                               </div>
                               <div class="col-md-2"></div>
                           </div>
                           <?php
                       }
                       ?>

                       <?php
                   }?>

               </div>
           </div>
       </div>

       <div class="container">
        <div id="sec-promotion" class="block-margin" style="margin-top: 37px;">

            <?php if($project_promotion != false && $project->project_status != 'SO'){
                $img_promotion = getImagePromotion($project_promotion->promotion_id);
                ?>
                <p class="heading-title">Promotion</p>
                <div class="row">
                    <div id="projectPromoSlide">
                        <div class="item">
                            <div class="inMiddle">
                                <div class="col-md-8">
                                    <div class="review-img">
                                        <img src="<?php if ($img_promotion != false) echo backend_url('base', $img_promotion->img_promotion_name) ?>"
                                        alt="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="promo-descrp">
                                        <p class="title"><?= $project_promotion->promotion_name_th ?></p>
                                        <p><?=!empty($project_promotion->promotion_detail_th) ? str_replace(array("\n\r", "\n","n/g"), '<br/>', $project_promotion->promotion_detail_th) : ''?></p>
                                        <?php
                                        $promotion_url = str_replace(' ','',$project_promotion->promotion_url);
                                        if(!empty($promotion_url)){
                                            $promotion_url = strpos($promotion_url, 'https://') !== false || strpos($promotion_url, 'http://') !== false ? $promotion_url : "http://$promotion_url";
                                            ?>
                                            <a href="<?= $promotion_url ?>" class="btn btn-seemore" style="text-align:center">
                                                See more
                                            </a>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <?php }

                $homeSells = getHomeSell($project_id, $product_id);
                $homeSells_ByPrice = getHomeSell_ByPrice($project_id, $product_id);
                if ($homeSells != false && $project->project_status != 'SO') {
                    ?>
                    <div id="sec-home-sale" class="block-margin">
                        <p class="heading-title">House for sale</p>
                        <?php
                    //first item
                        $planImg = getGalleryFirstPlan($homeSells[0]->plan_id);
                        $lhPlan = getPlanByPlanID($homeSells[0]->plan_id);
                        $anotherImgs = getGalleryByCheckType($homeSells[0]->home_sell_id);
                        $conditionHomeSell = getConditionHomeSellByHomeSellID($homeSells[0]->home_sell_id);

                        ?>
                        <div class="row">
                            <div id="projectModelSlide">
                                <div class="col-md-8">
                                    <div class="project-model-block">
                                        <div id="tag_sold" <?= $homeSells[0]->home_sell_status == 'SO' ? '' : ' style="display: none;" ' ?> >
                                            <div class="tagbox2_sold">
                                                <div class="tag2">sold</div>
                                            </div>
                                        </div>
                                        <div id="projectSlide" class="owl-carousel owl-theme">
                                            <div class="gal-img item"><img
                                                src="<?php if ($homeSells[0] != false) echo backend_url('base', $homeSells[0]->model_image) ?>"
                                                />
                                            </div>
                                            <?php
                                            foreach ($anotherImgs as $anotherImg) {
                                                if($anotherImg['path'] != ''){
                                                ?>

                                                <div class="gal-img item"><img
                                                    src="<?= backend_url('base', $anotherImg['path']) ?>"
                                                    alt="<?= $anotherImg['seo'] ?>"/>
                                                </div>

                                                <?php
                                            }
                                            }
                                            ?>
                                        </div>
                                    </div>

                                    <div id="imgThumb" class="slide-thumb owl-thumbs owl-carousel owl-theme">
                                        <a class="item"><img
                                            src="<?php if ($homeSells[0] != false) echo backend_url('base', $homeSells[0]->model_image) ?>"
                                            />
                                        </a>
                                        <?php
                                        foreach ($anotherImgs as $anotherImg) {
                                            ?>
                                            <a class="item"><img
                                                src="<?= backend_url('base', $anotherImg['path']) ?>"
                                                alt="<?= $anotherImg['seo'] ?>"></a>
                                                <?php
                                            }
                                            ?>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">

                                        <div class="promo-descrp" id="desc_home">
                                            <?php
                                            $id_icon = $homeSells[0]->icon_id;
                                            if($id_icon != ''){
                                                $icon = getIconActivity_homesell($id_icon);
                                                if($icon->icon_img  != ''){
                                                    if($homeSells[0]->home_sell_status != 'SO'){
                                                        ?>
                                                        <img src="<?=backend_url('icon_activity',$icon->icon_img )?>" class="logo-activity"  alt="acti">
                                                        <?php }}}?>
                                                        <p class="title green">House style <?= $lhPlan->plan_name_th ?></p>
                                                        <?php if($homeSells[0]->number_converter != ''){?>
                                                        <p class="title dsktp">Number of Plot of land <?=$homeSells[0]->number_converter ?></p>
                                                        <?php }?>
                                                        <p class="title dsktp" style="display: none;">Type of plot of land</p>
                                                        <p style="display: none;"><?= $homeSells[0]->features_convert ?></p>
                                                        <p class="title green">
                                                            Price <?= !empty($conditionHomeSell) ? Helper::getProjectPrice($conditionHomeSell->total_price) : 0 ?>
                                                            <?= is_object($conditionHomeSell) ? Helper::getPriceModeName(1,$conditionHomeSell->total_price) : 'MB' ?>
                                                        </p>
                                                        <?php
                                                        if($product_id ==2){
                                                           $type = 'townhome';     
                                                       }else {
                                                           $type = 'singlehome';   
                                                       }   
                                                       $url_see_more = $router->generate('home-sell-detail',[
                                                        'plan_name'         =>  str_replace(' ','-',$lhPlan->plan_name_th),
                                                        'home_sell_id'      =>  $homeSells[0]->home_sell_id,
                                                        'project_name'      =>  str_replace(' ','-',$project->project_name_th),
                                                        'product_id'        =>  $product_id,
                                                        'product_name'      =>  $type,
                                                        ]);

                                                        ?>
                                                        <a href="<?= $url_see_more ?>"
                                                           class="btn btn-seemore" id="submenu">See more</a>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                       <?php
                                       if(!empty($homeSells_ByPrice)){?>
                                       <div id="other-home-sec" class="other-home-sec">
                                        <p class="heading-title">All Houses for sale <span id="index_plan"><span id="index_homesell" style="float: left;">1</span>/<?= count($homeSells) ?>
                                            </span></p>
                                            <input type="hidden" id="amount_value" value="<?= count($homeSells_ByPrice) ?>">
                                            <div class="row">
                                                <div id="otherHome" class="col-slide">

                                                    <?php
                                                    $i=0;

                                                    foreach ($homeSells_ByPrice as $i => $homeSell) {

                                                        $subPlanImg = getGalleryFirstPlan($homeSell->plan_id);
                                                        $lhSubPlan = getPlanByPlanID($homeSell->plan_id);
                                                        $anotherSubImgs = getGalleryByCheckType($homeSell->home_sell_id);
                                                        $conditionSubHomeSell = getConditionHomeSellByHomeSellID($homeSell->home_sell_id);

                                                        ?>

                                                        <div class="item">
                                                            <div class="item-homesell" style="cursor: pointer;">
                                                                <div class="other-home-img">
                                                                    <?php if($homeSell->home_sell_status == 'SO') {
                                                                        ?>
                                                                        <div class="tagbox2_sold xs-tag">
                                                                            <div class="tag2">sold</div>
                                                                        </div>
                                                                        <?php
                                                                    }?>
                                                                    <?php
                                                                    if($product_id ==2){
                                                                       $type = 'townhome';     
                                                                   }else {
                                                                       $type = 'singlehome';   
                                                                   }   
                                                                   $url_see_more = $router->generate('home-sell-detail',[
                                                                    'plan_name'         =>  str_replace(' ','-',$lhSubPlan->plan_name_th),
                                                                    'home_sell_id'      =>  $homeSell->home_sell_id,
                                                                    'project_name'      =>  str_replace(' ','-',$project->project_name_th),
                                                                    'product_id'        =>  $product_id,
                                                                    'product_name'      =>  $type,
                                                                    ]);
                                                                    ?>

                                                                     <a href="<?= $url_see_more ?>">
                                                                        <img src="<?= backend_url('base', $homeSell->model_image) ?>">

                                                                        <!--                                    <img src="-->
                                            <?php //if($planImg != false) echo base_url('plan_img',$planImg->galery_plan_name)
                                            ?><!--" alt="">-->
                                        </div>

                                        <?php
                                        $id_icon = $homeSell->icon_id;
                                        if($id_icon != ''){
                                            $icon = getIconActivity_homesell($id_icon);

                                            if(is_object($icon)){
                                                if($homeSell->home_sell_status != 'SO'){
                                                    ?>
                                                    <img src="<?=backend_url('icon_activity',$icon->icon_img )?>" class="logo-activity"  alt="acti">
                                                    <?php }}}?>

                                                    <p class="title">House style <?= $lhSubPlan->plan_name_th ?></p>
                                                    <?php if($homeSell->number_converter != ''){?>
                                                    <p class="gray mb0">Number of Plot of land <?= $homeSell->number_converter ?></p>
                                                    <?php } $i++ ?>
                                                    <p class="gray mb0" style="display: none;">Type of plot of land  <?= $homeSell->features_convert ?></p>
                                                    <p class="title green">
                                                        <b>
                                                            Price <?= !empty($conditionSubHomeSell) ? Helper::getProjectPrice($conditionSubHomeSell->total_price) : 0 ?>
                                                            <?= is_object($conditionSubHomeSell) ? Helper::getPriceModeName(1, $conditionSubHomeSell->total_price) : 'MB' ?>
                                                        </b></p>
                                                    </a>
                                                    <div id="data-slider" style="display: none">
                                                        <input type="hidden" id="home_sell_status"
                                                        value="<?= $homeSell->home_sell_status == 'SO' ? 'sold_out' : '' ?>">
                                                        <input type="hidden" id="index_value" value="<?= $i + 1 ?>">
                                                        <input type="hidden" id="title_home"
                                                        value="<?= $lhSubPlan->plan_name_th ?>">
                                                        <input type="hidden" id="format_home"
                                                        value="<?= $lhSubPlan->plan_name_th ?>">
                                                        <input type="hidden" id="id_home" value="<?= $homeSell->number_converter ?>">
                                                        <input type="hidden" id="identity_home"
                                                        value="<?= $homeSell->features_convert ?>">
                                                        <input type="hidden" id="price_home"
                                                        value="<?= Helper::getProjectPrice($conditionSubHomeSell->total_price) ?>  <?= is_object($conditionSubHomeSell) ? Helper::getPriceModeName(1, $conditionSubHomeSell->total_price) : 'ล้านบาท' ?>">
                                                        <input type="hidden" id="home_sell_id"
                                                        <?php
                                                        $url_see_more_another = $router->generate('home-sell-detail',[
                                                            'plan_name'         =>  str_replace(' ','-',$lhSubPlan->plan_name_th),
                                                            'home_sell_id'      =>  $homeSell->home_sell_id
                                                            ]);
                                                            ?>
                                                            value="<?= $url_see_more_another ?>">
                                                            <div id="img_list">
                                                                <input type="hidden" class="img_url"
                                                                value="<?= backend_url('base', $subPlanImg->plan_img) ?>">
                                                                <?php
                                                                foreach ($anotherSubImgs as $anotherSubImg) {
                                                                    ?>
                                                                    <input type="hidden" class="img_url"
                                                                    value="<?= backend_url('base', $anotherSubImg['path']) ?>">
                                                                    <?php
                                                                }
                                                                ?>

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <?php
                                            } ?>

                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>

                        <?php
                        $galleryImgProjects = getGalleryImgProject($project_id);
                        if($galleryImgProjects != false && $project->project_status != 'SO'){
                            ?>
                            <div id="sec-gallery" class="gallery-block block-margin">
                                <p class="heading-title">GALLERY</p>
                                <div class="tpl5-img">
                                    <div id="tpl5-img">
                                        <?php
                                        $i = 1;
                                        foreach ($galleryImgProjects as $galleryImgProject) {
                                            ?>
                                            <div class="item tpl5-img-<?= $i ?>">
                                                <a class="gallery"
                                                href="<?= backend_url('base', $galleryImgProject->galery_project_img_name) ?>">
                                                <img src="<?= backend_url('base', $galleryImgProject->galery_project_img_name) ?>"
                                                alt="<?=$galleryImgProject->galery_project_img_seo?>">
                                                <p class="title-gallery"><?= $galleryImgProject->galery_project_img_desc ?></p>
                                            </a>
                                        </div>
                                        <?php
                                        $i++;
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                    <?php if($project->project_status == 'SO'){

                        $projectNearByZones = getProjectRelate($project_id,$project->zone_id,$product_id);
                        if($projectNearByZones != false){
                            ?>
                            <div class="project-info-detail-block">
                                <p class="heading-title">Other projects which may be interest</p>
                                <div class="row">
                                    <div id="colSlide" class="col-slide">
                                        <?php

                                        foreach ($projectNearByZones as $projectNearByZone) {
                                            $project_near_id = $projectNearByZone->project_id;
                                            $project_relate = getProjectByID($project_near_id);
                                            $banner  = getBannerByProjectID($project_near_id);
                                            $project_concept = getProjectConcept($project_near_id);
                                            $project_price = getProjectPrice($project_near_id);
                                            ?>
                                            <div class="item">
                                                <div class="project-onsale">
                                                    <div class="project-info-other-img">
                                                        <?php
                                                        if($banner != false && ( $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'image' || $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'banner' ) ){
                                                            ?>
                                                            <img src="<?= backend_url('base',$banner->LEAD_IMAGE_PROJECT_FILE_NAME)?>"
                                                            alt="<?= $banner->LEAD_IMAGE_PROJECT_FILE_SEO ?>">
                                                            <?php
                                                        }elseif($banner != false && $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'youtube'){
                                                            $lead_img = backend_url('base', $banner->banner_img_thum);
                                                            $lead_youtube_url = str_replace('watch?v=', 'embed/', $banner->LEAD_IMAGE_PROJECT_FILE_NAME);
                                                            ?>

                                                            <a href="#" data-featherlight="#lbVdo<?= $i ?>">
                                                                <span class="i-view-vdo"></span>
                                                                <img src="<?= $lead_img ?>"
                                                                alt="<?= $banner->LEAD_IMAGE_PROJECT_FILE_SEO ?>" class="hm-highl">
                                                            </a>
                                                            <iframe class="lightbox" src="<?= $lead_youtube_url . "?autoplay=0" ?>"
                                                                width="1000"
                                                                height="560" id="lbVdo<?= $i ?>" style="border:none;" webkitallowfullscreen
                                                                mozallowfullscreen allowfullscreen></iframe>

                                                                <?php
                                                            } elseif ($banner != false && $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'vdo') {
                                                                ?>
                                                                <a id="ban_vdo<?= $i ?>">
                                                                    <span class="i-view-vdo"></span>
                                                                    <img src="<?= backend_url('base', $banner->banner_img_thum) ?>"
                                                                    alt="<?= $banner->LEAD_IMAGE_PROJECT_FILE_SEO ?>" class="hm-highl">
                                                                </a>
                                                                <script>
                                                                    $(document).ready(function () {
                                                                        $('#ban_vdo<?= $i ?>').click(function () {
                                                                            $('#lbVdo<?= $i ?>').show();
                                                                            $.featherlight($('#lbVdo<?= $i ?>'),{});
                                                                            $('.featherlight-content #video_player<?= $i ?>')[0].play();
                                                                            $('#lbVdo<?= $i ?>').hide();
                                                                        });
                                                                    })
                                                                </script>
                                                                <div id="lbVdo<?= $i ?>" style="position:relative;width: 100%;display: none;">
                                                                    <video id='video_player<?= $i ?>' preload='none' controls>
                                                                        <source src="<?= backend_url('base',$banner->LEAD_IMAGE_PROJECT_FILE_NAME) ?>" type="video/mp4">
                                                                        </video>
                                                                    </div>

                                                                    <?php } ?>
                                                                </div>
                                                                <?php
                                                                if($projectNearByZone->brand_id == 3){
                                                                    $template = 'ladawan-detail';
                                                                    $mode = 1;
                                                                }elseif($projectNearByZone->product_id == 2){
                                                                    $template = 'town-home-detail';
                                                                    $mode = 2;
                                                                }else{
                                                                    $template = 'single-home-detail';
                                                                    $mode = 1;
                                                                }
                                                                $url =$router->generate($template ,[
                                                                    'project_name'  =>  str_replace(' ','-',$project_relate->project_name_th),
                                                                    'product_id'    =>  $mode,
                                                                    'lang'          =>  'en'
                                                                ]);
                                                                ?>
                                                                <a href="<?php echo $url ?>">
                                                                    <p class="title"><?= $project_relate->project_name_th ?></p>
                                                                    <p class="gray mb0"><?= !empty($project_concept->concept_th) ? $project_concept->concept_th : '' ?></p>
                                                                    <p class="title green">
                                                                        <b>Price
                                                                            <?php if($project_price != false) echo Helper::getProjectPrice($project_price->project_price,$project_price->price_mode) ?>
                                                                                 MB</b>
                                                                            </p>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <?php

                                                            }

                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br><br>
                                                <?php
                                            }
                                        } ?>

                                        <?php if($project->project_status != 'SO'){ ?>

                                        <div class="project-info-other-block block-margin">
                                            <div class="row">

                                                <?php
                                                $project360 = get360Project($project_id);
                                                if ($project360 != false) {
                                                    ?>

                                                    <div id="sec-virtual" class="col-md-6">
                                                        <p class="heading-title">360-degree image</p>
                                                        <div class="project-info-other-img">
                                                            <a id="various" href="<?= $project360->c360_project_url ?>"><span class="i-view-360"></span><img src="<?= backend_url('base', $project360->thumnail) ?>" alt=""></a>
                                                            <script type="text/javascript">
                                                                $(document).ready(function() {
                                                                    $("#various").fancybox({
                                                                        'width'				: '100%',
                                                                        'height'			: '100%',
                                                                        'autoScale'			: false,
                                                                        'transitionIn'		: 'none',
                                                                        'transitionOut'		: 'none',
                                                                        'type'				: 'iframe'
                                                                    });
                                                                });
                                                            </script>
                                                        </div>
                                                        <!--                            <p class="title">Good Moment</p>-->
                                                    </div>

                                                    <?php
                                                }
                                                $vdoProject = getVDOProject($project_id);
                                                if ($vdoProject != false) {
                                                    if($vdoProject->tvc_type == 'youtube'){
                                                        $youtube_img = backend_url('base',$vdoProject->thumnail);
                                                        $youtube_url = str_replace('watch?v=', 'embed/', $vdoProject->clip_project_url);
                                                        $parameterPef= strpos($youtube_url, '?') !== false ? '&' : '?';
                                                        ?>
                                                        <div id="sec-tvc" class="col-md-6">
                                                            <p class="heading-title">VDO</p>
                                                            <div class="project-info-other-img">
                                                                <a href="#" data-featherlight="#lbVdo1">
                                                                    <span class="i-view-vdo"></span>
                                                                    <img src="<?= $youtube_img ?>" alt="" style="">
                                                                </a>
                                                                <iframe class="lightbox" src="<?= $youtube_url . $parameterPef ."autoplay=0" ?>" width="1000"
                                                                    height="560" id="lbVdo1" style="border:none;" webkitallowfullscreen
                                                                    mozallowfullscreen allowfullscreen></iframe>

                                                                </div>
                                                                <!--                            <p class="title">Good Moment</p>-->
                                                            </div>
                                                            <?php
                                                        }elseif($vdoProject->tvc_type == 'vdo'){
                                                            ?>
                                                            <div id="sec-tvc" class="col-md-6">
                                                                <p class="heading-title">VDO</p>
                                                                <div class="project-info-other-img">
                                                                    <a id="tvc_vdo">
                                                                        <span class="i-view-vdo"></span>
                                                                        <img src="<?= backend_url('base',$vdoProject->thumnail) ?>" alt="" style="">
                                                                    </a>
                                                                    <script>
                                                                        $(document).ready(function () {
                                                                            $('#tvc_vdo').click(function () {
                                                                                $('#tvcVdo').show();
                                                                                $.featherlight($('#tvcVdo'),{});
                                                                                $('.featherlight-content #tvc_video_player')[0].play();
                                                                                $('#tvcVdo').hide();
                                                                            });
                                                                        })
                                                                    </script>
                                                                    <div id="tvcVdo" style="position:relative;width: 100%;display: none;">
                                                                        <video id='tvc_video_player' preload='none' controls>
                                                                            <source src="<?= backend_url('base',$vdoProject->clip_project_url) ?>" type="video/mp4">
                                                                            </video>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <?php
                                                            }
                                                            ?>
                                                            <?php } ?>
                                                        </div>
                                                    </div>

                                                    <?php } ?>

                                                </div>
                                            </div>

                                            <?php if($project->project_status != 'SO'){ ?>

                                            <?php
                                            $hasLocation = !empty($project->map_link) || !empty($projectMap->map_img) || !empty($projectMap->map_pdf) && $project->project_status != 'SO';
                                            ?>
                                            <?php  if($hasLocation) { ?>
                                            <input type="hidden" id="lat" value="<?= $project->map_link ?>">
                                            <input type="hidden" id="lng" value="<?= $project->longtitude ?>">
                                            <?php } ?>


                        <div id="sec-location" class="project-location-block block-margin">

                            <?php
                            $project->latitude = str_replace(' ','',$project->latitude);
                            $project->longtitude = str_replace(' ','',$project->longtitude);
                           
                                ?>
                                 <?php  if($hasLocation) { ?>
                                <div class="container">
                                    <p class="heading-title">Location of the Project</p>
                                </div>
                                 <?php }?>
                                

                                <?php  if($hasLocation) { ?>
                                    <div class="map-container2 container">
                                        <div id="map"></div>
                                        <!--                    <span class="i-getdirection" onclick="toLocationByWalk();"></span>-->
                                        <span class="i-getdirection-car" onclick="toLocation();"></span>
                                        <div class="map-custom"> 
                                             <?php
                                                if(!empty($project->map_link)){
                                                    ?>
                                           <a  href="<?php echo $project->map_link ?>" class="map-google-jpg" target="_blank"><i class="map-jpg"></i><img src="<?= file_path('images/global/googlemap.png')?>"></a>
                                        <?php }?>
                                            <?php if(!empty($projectMap) && (!empty($projectMap->map_img) || !empty($projectMap->map_pdf)) ){
                                                ?>
                                                <?php
                                                if(!empty($projectMap->map_pdf)){
                                                    ?>
                                                    <a href="<?php echo backend_url('base',$projectMap->map_pdf); ?>" class="map-download-pdf" target="_blank"><i class="map-pdf"></i><span class="cal-txt">Map PDF</span></a>
                                                    <?php
                                                }if(!empty($projectMap->map_img)){
                                                    ?>
                                                    <a href="<?php echo backend_url('base',$projectMap->map_img); ?>" class="map-download-jpg" target="_blank"><i class="map-jpg"></i><span class="cal-txt">Map JPG</span></a>
                                                    <?php
                                                }
                                                ?>

                                                <?php
                                            } ?>
                                            
                                        </div>
                                    </div>
                                <?php } ?>

                                


                            <?php
                            $projectNearBys = getProjectNearBy($project_id);
                            if ($projectNearBys != false && !empty($projectNearBys[0]->nearby_name_th) ) {
                                ?>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="title">Facilities around the project<span > (approximate distance)</span></p>
                                            <?php
                                            $leftProjectNearBys  = [];
                                            $rightProjectNearBys = [];
                                            foreach ($projectNearBys as $i => $projectNearBy)
                                            {
                                                if($i%2 == 0){
                                                    $leftProjectNearBys[] = $projectNearBy;
                                                }elseif ($i%2 == 1){
                                                    $rightProjectNearBys[] = $projectNearBy;
                                                }
                                            }
                                            ?>
                                            <div class="content-style">
                                                <div class="col-md-6">
                                                    <ul class="tbs1">
                                                        <?php
                                                        foreach ($leftProjectNearBys as $projectNearBy) {
                                                            if (!empty($projectNearBy->nearby_name_th) && !empty($projectNearBy->interval)) {
                                                                ?>
                                                                <li>
                                                                <?= $projectNearBy->nearby_name_th ?> <?= $projectNearBy->interval ?>
                                                                <?php 
                                                                if($projectNearBy->nearby_unit == 'เมตร'){
                                                                    echo 'm.';
                                                                }else{
                                                                    echo 'km.';
                                                                }

                                                                ?>
                                                                </li>
                                                               <?php
                                                           }
                                                           ?>
                                                       <?php } ?>
                                                   </ul>
                                               </div>
                                               <div class="col-md-6">
                                                <ul class="tbs1">
                                                    <?php
                                                    foreach ($rightProjectNearBys as $projectNearBy) {
                                                        if (!empty($projectNearBy->nearby_name_th) && !empty($projectNearBy->interval)) {
                                                            ?>
                                                            <li>
                                                                <?= $projectNearBy->nearby_name_th ?> <?= $projectNearBy->interval ?>
                                                                <?php 
                                                                if($projectNearBy->nearby_unit == 'เมตร'){
                                                                    echo 'm.';
                                                                }else{
                                                                    echo 'km.';
                                                                }

                                                                    ?>
                                                                                        </li>
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                    <?php } ?>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>


                                                        <?php
                                                        $project_contact    = getProjectContactByProjectID($project_id);
                                                        $contact_day        = [
                                                        'all'   =>  'every working day',
                                                        '0'     =>  'official holidays',
                                                        '1'     =>  'Monday',
                                                        '2'     =>  'Tuesday',
                                                        '3'     =>  'Wednesday',
                                                        '4'     =>  'Thursday',
                                                        '5'     =>  'Friday',
                                                        '6'     =>  'Saturday',
                                                        '7'     =>  'Sunday',
                                                        ];
                                                        $day_str  = 'every day';
                                                        $start_time_contact     = '';
                                                        $end_time_contact       = '';
                                                        $tell_contact           = '';
                                                        $email_contact          = '';
                                                        $arr_days = [];
                                                        if($project_contact != false){
                                                            $open_day = str_replace(' ','',$project_contact->open_day);
                                                            if (strpos($open_day, ',') !== false) {
                                                                $arr_days = explode(',',$open_day);
                                                                if(count($arr_days) > 0 ){
                                                                    $day_str .= ' except ';
                                                                }
                                                                foreach ($arr_days as $i => $arr_day){
                                                                    $day_str .= ' '.$contact_day[(string)$arr_day];
                                                                    if(count($arr_days) != $i+1 ){
                                                                        $day_str .= ',';
                                                                    }
                                                                }
                                                            }
                                                            $start_time_contact     = $project_contact->open_time_start;
                                                            $end_time_contact       = $project_contact->open_time_end;
                                                            $tell_contact           = $project_contact->telephone;
                                                            $email_contact          = $project_contact->email;
                                                        }

                                                        ?>
                                                        <div class="container">
                                                            <div id="sec-contact" class="block-margin">
                                                                <p class="heading-title">Contact & Appointment</p>
                                                                <?php if($project_contact != false){
                                                                    ?>
                                                                    <span> Contact the Sales office <?= $day_str ?> from <?= $start_time_contact ?> - <?= $end_time_contact ?>   Tel. <?= $tell_contact ?> EMAIL : <?= $email_contact ?>

                    
                     <!-- <a href="http://edm.lh.co.th/Support/contact-form/new.contact-project-detail.php?project_id=<?php //echo $edm->project_type;?><?//=$edm->project_edm_id;?>&brand_id=&website=LHWeb&mem_id=" target="_blank" data-family="child">สอบถามข้อมูลโครงการเพิ่มเติม คลิก</a> -->
		     
             </span>
             <?php
         }?>
         
         <!-- contacform -->
         <?php include 'template-part/contact_form.php' ?>
         <!-- END  contacform-->

        </div>

    </div>
    
    
    
        <!-- edm send form -->
    <script src="<?=file_path('js/cal_reg/xmlTojson.js') ?>" type="text/javascript" ></script>
    <script src="<?=file_path('js/edm_send_form.js') ?>" type="text/javascript" ></script>
    <!-- END -->


</div>

</div>

<?php } ?>

<div style="color: #999999;font-size: 15px;" class="container block-margin">
    <?php
    if($project_concept != false) {
        $information = Helper::checkLangEnglish($lang) ? $project_concept->information_en : $project_concept->information_th;
        echo $information;
    }
    ?>
</div>



<?php include('footer.php'); ?>
<script>
    $('#popappoint').css('display','');
</script>