<?php
header('Location: //'.$_SERVER['SERVER_NAME'].'/'.'en');
exit();

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

$reviews = getProjectReview();
$page = 'review';
$page_index = 0;
$img = '';
?>
<?php
include('header.php');



?>
<!-- CSS -->
<link rel="stylesheet" href="<?= file_path('css/review.css') ?>" type="text/css">
<!-- JS -->
<script src="<?= file_path('js/review.js') ?>"></script>
<script src="<?= file_path('js/imagesloaded.pkgd.min.js') ?>"></script>


<div id="content" class="content">
    <div class="container">
        <?php 
        $SEO = getSEOUrl($actual_link);
        if($actual_link == @$SEO->url_page){
            echo "<h1 class='heading-title'>".$SEO->h1."</h1>";
        }else{
         echo "<h1 class='heading-title'>LH PROJECT REVIEW</h1>";  
        }
     ?>
     

        <div id="reviewList" class="home-section">
            <div class="row">
                <div class="review-lists">
                    <?php
                    if($reviews != false){
                        foreach ($reviews as $review){
                            if($review->custom_url != ''){
                                $url_name = $review->custom_url; 
                            }else{
                                $url_name =$review->project_review_name_th;
                            }
                            $url_see_more = $router->generate('review-detail',[
                                'review_name'  =>  str_replace(' ','-',$url_name),
                                'review_id'    =>  $review->project_review_id
                            ]);

                        ?>
                        <div class="list-item item">
                            <div class="review-img">
                                <a href="<?= $url_see_more ?>">
                                    <img src="<?= backend_url('base',$review->project_review_img) ?>" alt="">
                                </a>
                            </div>
                            <a href="<?= $url_see_more ?>">
                                <p class="title"><?= $review->project_review_name_th ?></p>
                                <p><?= $review->project_review_dis_th ?></p>
                            </a>
                            <a href="<?= $url_see_more ?>" class="btn btn-seemore">See more</a>
                        </div>
                <?php
                    }
                }
                ?>

                </div>
            </div>
        </div>

    </div>
</div>


<?php include('footer.php'); ?>
