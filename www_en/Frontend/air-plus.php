<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';
$page = 'airplus';
$page_index = 0;
$img = '';
?>
<?php
include('header.php');

?>
<!-- CSS -->
<link rel="stylesheet" href="<?= file_path('css/air-plus.css') ?>" type="text/css">
<!-- JS -->
<script src="<?= file_path('js/air-plus.js') ?>"></script>

<div class="page-banner">
    <div id="bannerSlide">
        <div class="item">
            <div class="banner-container banner-parallax">
                <img src="<?= file_path('images/temp3/air_1.jpg') ?>" alt="" class="vdo-banner">
                <video loop="loop" id="slideVdo" class="" style="display:none;">
                    <source src="<?= file_path('files/vdo/airplus.mp4') ?>" type="video/mp4">
                </video>

                <div class="vdo-control">
                    <div class="play-block">
                        <span id="vdoPlay" class="vdo-icon vdoControl vdoPlay"></span>
                        <span id="vdoPause" class="vdo-icon vdoControl vdoPause"></span>
                    </div>
                </div>
            </div>

<!--            <a href="#" data-featherlight="#lbVdo1">-->
<!--                <span class="i-view-vdo"></span>-->
<!--            </a>-->
<!--            <div class="banner-container banner-parallax" style="background-image: url(images/temp3/air_1.jpg);">-->
<!--            <iframe class="lightbox" src="https://www.youtube.com/embed/GJkPSkZ80bo" width="1000"-->
<!--                        height="560" id="lbVdo1" style="border:none;" webkitallowfullscreen-->
<!--                        mozallowfullscreen allowfullscreen></iframe>-->
<!--            </div>-->
        </div>
    </div>
</div>

                <div id="content" class="content airplus-page">
                    <div class="container">
                        <div class="airplus-content">

                            <?php
                            $SEO = getSEOUrl($actual_link);
                            if($actual_link == @$SEO->url_page){
                                echo '<h1 >'.$SEO->h1.'<h1>';
                            }else{
                               echo ' <h1>The  Air Plus Technology</h1>'; 
                           }
                           ?>
                           <div class="row">
                            <div class="col-md-4">
                                <img src="<?= file_path('images/temp3/air_02.jpg') ?>" alt="">
                            </div>
                            <div class="col-md-6 col-md-offset-1">
                                <p class="title">Where does the Waste air in the house come from?</p>
                                <ul class="ap-li">
                                    <li>All activities in the house can create the air poisoning without    knowing  such as the smell of home paint,   smoke and fat caused from home-made cooking or even the dirt that comes from pets</li>
                                    <li>If we open the air-conditioning all the day, in fact the cool air from the air-conditioning  actually  comes from  pulling the air in the room to process and then release the used air  again.  So we breathe the old used air into our lungs all the time. That’s why some people do not feel refreshed or feel sleepy when they wake up in the morning</li>
                                    <li>In some case , we have to close the house for a long time such as when we have to travel for a long period of  time,  which cause the air inside the house cannot ventilate or transfer , thus makes the  “Dead Air” which brings the bad smell to the house.</li>
                                    <li>The dampness from the atmosphere in the new modern house which often avoid the sun and  rain by design to be closing the house entirely which then cause the house  to create the smelling damp and musty  and also becomes a source of   culture of  bacteria, germ such as fungus and  fomites. </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

    <div class="airplus-lists-container">
        <div class="container">
            <div class="airplus-lists">
                <div class="row">
                    <div class="col-md-4">
                        <div class="airplus-list">
                            <span class="apicon i-aplist-1"></span>
                            <p class="title">Automatic Control</p>
                            <p>The technology  AirPlus  is designed to work 24 hours automatically , through  solar cell to allow  the fresh air to be ventilated into the house all the time.  The use of solar cell also save the use of electricity and is eco-friendly. Besides , the automatic ventilation system , the users can set the open-shut down the system as needed through the Controller Unit device. </p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="airplus-list">
                            <span class="apicon i-aplist-2"></span>
                            <p class="title">Smart Power Manager</p>
                            <p>1. <strong>Solar power  : </strong> The solar energy is a clean energy <br><br>
                                2. <strong>Hybrid :</strong> the combined  use of energy consumption  among the solar and electricity power in the house<br><br>
                                3. <strong>Sufficient :</strong>save the use of energy from electricity </p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="airplus-list">
                            <span class="apicon i-aplist-3"></span>
                            <p class="title">Well-planned ventilation</p>
                            <p>In the house, an installation of  a Ventilation tube to exhaust the air inside the house through the air duct to the outside of the house.  So the air  constantly exchanges and circulates throughout the house which then also reduce the accumulate temperature and moisture </p>
                        </div>
                    </div>
                </div>


                <span class="img-leafs"></span>
                <span class="img-girl"></span>
            </div>
        </div>
    </div>

</div>


<?php include('footer.php'); ?>
