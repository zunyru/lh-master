<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';
require_once 'include/help/query_independent.php';
require_once 'include/help/Helper.php';

?>
<?php include('header.php'); ?>
	
 <!-- JS -->
    <script src="<?= file_path('js/lib/html2canvas.js') ?>"></script>
    <script src="<?= file_path('js/lib/angular.min.js') ?>"></script>
    <script src="<?= file_path('js/cal_reg/app.js') ?>"></script>

    <link rel="stylesheet" href="<?= file_path('css/corporate-all.css') ?>" type="text/css">
    <link rel="stylesheet" href="<?= file_path('css/corporate-menu.css') ?>" type="text/css">
    <link rel="stylesheet" href="<?= file_path('css/smartcal-howto.css') ?>" type="text/css">


    <style>
        
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, 
        .table>thead>tr>th{

            border : none;

        }
        .table-striped>tbody>tr:nth-of-type(even){
            background-color: #f3f3f3;
        }
        .apply-job-container label a{
            color: #4d4c4d;
        }
         .apply-job-container label a:hover {
            color: #6f9755;
        }
        .numbox{
            border: 1px #dedede solid;
            border-radius: 5px;
        }
        .box-input{
			margin: 10px 0;
		}

		.input-desktop{
			width: 45%; 
			text-align: right;
			float: right;
		}

		.input-mobile{
			width: 100%; 
			text-align: right;
			float: right; 
			height: 40px;
			padding: 15px;
		}

		input[type="text"]:disabled {
			background: #e8e8e8;
		}

		#table-calculator td {
		    min-width: 110px;
		}

		#table-calculator tr > td:nth-child(1){
			min-width: 35px;
		}
		#table-calculator tr > td:nth-child(2) {
		    min-width: 50px;
		}
        #img_print canvas{
            width: 100%;
        }
        
        /* A4 Landscape*/
        @page {
            margin: 5%
        }
        @media print {
            .noprint {display:none !important;}
            a:link:after, a:visited:after {  
              display: none;
              content: "";    
            }
        }
        
    </style>




        <div id="content" class="content ethic-page smartcal-page"  ng-app="calculator" style="text-align: left;padding-right: 5px;">
            <div class="container" ng-controller="calculatorController" >
                
                <div class="row">
                    <div class="col-sm-12">

                        <h1>Smart Calculator</h1>
                        <div id="corpMenu" class="corp-menu-tab">
                            <ul>
                                <li class="current"><a href="" onclick="#">คำนวณยอดเงินที่ต้องผ่อนชำระต่อเดือน</a></li>
                                <li><a href="/smart-cal-howto">วิธีการใช้งาน</a></li>
                            </ul>
                        </div>

                        <div id="pdf" >

                            
                            

                            <!-- desktop input -->

                                <dir style=" padding: 0px" class="hidden-xs">
                                    <div style="float: left;">
                                        <h1 style="color: #7e7e7e;margin: 0px 0px 15px 0px;">คำนวณยอดเงินที่ต้องผ่อนชำระต่อเดือน</h1>
                                    </div>
                                    <div style="float: right;" >
                                        <a href="#" ng-click="print()">
                                            <img src="<?= file_path('js/cal_reg/images/global/icon_printer.png') ?>" style="width: 20px;display: inline;">
                                            <span style="margin: 10px;color: #818181;">Print</span>
                                        </a>
                                    </div>
                                </dir>

                                <div class="row hidden-xs" style="display:inline-block;margin-top: 15px;">
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 form-row box-input" >
                                        <label style="font-size: 20px;">วงเงินกู้ (บาท)</label> &nbsp;
                                        <input type="text" name="number" min="0" class="numbox input-desktop" ng-init="k[0] = 0" ng-model="k[0]"  format="number">
                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 form-row box-input" >
                                        <label style="font-size: 20px;">ระยะเวลากู้ (ปี)</label>
                                         &nbsp; <input type="text" name="year" min="0" max="30" ng-model="year" class="numbox input-desktop"  format="number">
                                         
                                    </div>
                                    
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 form-row box-input" >
                                        <label style="font-size: 20px;">MLR (%)</label> &nbsp; 
                                        <input type="text" class="numbox input-desktop" name="mlr" min="0" ng-model="mlr" numbers-only> 
                                    </div>

                                    
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 form-row box-input" >
                                        <label style="font-size: 20px;">ค่าผ่อนขั้นต่ำ (ต่องวด)</label> &nbsp;
                                        <input type="text" class="numbox input-desktop" name="minimumpayment" value="{{PMT(mlr/1200 , year*12 , k[0] , 0 , 0) | number:0 }}" ng-model="minimumpayment"  disabled format="number">
                                    </div>
                                </div>
                            <!-- ./desktop input -->

                             <!-- mobile input -->
                                <dir style=" padding: 0px" class="visible-xs">
                                    <div style="float: left;width: calc(100% - 40px);">
                                        <h1 style="color: #7e7e7e;margin: 0px 0px 15px 0px;font-size: 42px;">คำนวณยอดเงินที่ต้องผ่อนชำระต่อเดือน</h1>
                                    </div>
                                    <div style="float: right;margin-top: 55px;">
                                        <a href="#" ng-click="print()">
                                            <img src="<?= file_path('js/cal_reg/images/global/icon_printer.png') ?>" style="width: 35px;display: inline;">
                                        </a>
                                    </div>
                                </dir>

                                <div class="row visible-xs" style="display:inline-block;margin-top: 15px; margin: 20px;">
                                    <div class="col-xs-12 box-input" style="padding: 0px;">
                                        <label style="font-size: 25px;">วงเงินกู้ (บาท)</label> 
                                    </div>
                                    <div>
                                        <input type="text" name="" min="0" class="numbox input-mobile" ng-init="k[0] = 0" ng-model="k[0]" format="number">
                                    </div>


                                    <div class="col-xs-12 box-input" style="padding: 0px;">
                                        <label style="font-size: 25px;">ระยะเวลากู้ (ปี)</label>
                                    </div>
                                    <div>
                                        <input type="text" name="" min="0" max="50" ng-model="year" class="numbox input-mobile"  format="number">
                                    </div>
                                    

                                    <div class="col-xs-12 box-input" style="padding: 0px;" >
                                        <label style="font-size: 25px;">MLR (%)</label>
                                    </div>
                                    <div>
                                        <input type="text" class="numbox input-mobile" name="" min="0"  ng-model="mlr" numbers-only> 
                                    </div>

                                    
                                    <div class="col-xs-12 box-input" style="padding: 0px;">
                                        <label style="font-size: 25px;">ค่าผ่อนขั้นต่ำ (ต่องวด)</label> 
                                    </div>
                                    <div>
                                        <input type="text" class="numbox input-mobile" name="" value="{{PMT(mlr/1200 , year*12 , k[0] , 0 , 0) | number:0 }}" ng-model="minimumpayment"  disabled format="number">
                                    </div>
                                </div>
                            <!-- ./mobile input -->
                                
                                <div style="overflow-x: auto;">
                                    <table class="table table-striped table-responsive" id="table-calculator" cellpadding="5" width="100%" style="text-align: center; margin-top: 33px;" ng-if="year >= 1" >
                                        <thead style="color: white; background-color:#6f9755;text-align: center;padding-right: 5px;">

                                            <tr>
                                                <td style="width: 4%;">ปีที่</td>
                                                <td style="width: 6%;">งวดที่</td>
                                                <td style="width: 10%;">ระบุค่าผ่อนชำระ <br> (บาท)</td>
                                                <td style="width: 12%;">ดอกเบี้ยต่องวด <br> (บาท)</td>
                                                <td style="width: 12%;">เงินต้นต่องวด <br> (บาท)</td>
                                                <td style="width: 11%;">เงินต้นรวม <br> (บาท)</td>
                                                <td style="width: 11%;">ดอกเบี้ยรวม <br> (บาท)</td>
                                                <td style="width: 12%;">ค่าผ่อนชำระรวม <br> (บาท)</td>
                                                <td style="width: 12%;">เงินต้นคงเหลือ <br> (บาท)</td>
                                                <td style="width: 13%;">ระบุดอกเบี้ย <br> (%)</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-if="year != 0" style="border: none;">
                                                <td>1</td>
                                                <td>1</td>
                                                <td>
                                                <input type="text" name="" min="0" ng-init="c[0] = 0" ng-model="c[0]" value="0"
                                                style="text-align: center; width: 95px;" class="numbox" format="number"></td>
                                                <td>{{ increase((k[0]*j[0]/100)/12,0) | number:2 }}</td> 

                                                <td>{{ capital(c[0]-(k[0]*j[0]/100)/12,0) | number:2 }}</td> 
                                                <!-- {{ capital(c[0]-(k[0]*j[0]/100)/12,0) }} -->

                                                <td>{{ capitalSum(c[0]-(k[0]*j[0]/100)/12,0) | number:2 }}</td>
                                                <!-- {{ capitalSum(c[0]-(k[0]*j[0]/100)/12,0) }} -->

                                                <td>{{ increaseSum((k[0]*j[0]/100)/12,0) | number:2 }}</td>
                                                <!-- {{ increaseSum((k[0]*j[0]/100)/12,0) }} -->

                                                <td>{{ installmentSum(c[0],0) | number:2 }}</td>
                                                <!-- {{ installmentSum(c[0],0) }} -->

                                                <td>{{ balance(k[0]-(c[0]-(k[0]*j[0]/100)/12),0) | number:2 }}</td>
                                                <!-- {{ balance(k[0]-(c[0]-(k[0]*j[0]/100)/12),0) }} -->

                                                <td><input type="text" style="width: 65%; text-align: center;" name="" ng-init="j[0] = 0" ng-model="j[0]" class="numbox" numbers-only></td>
                                            </tr>
                                            <tr ng-repeat="line in [].constructor(year*12) track by $index" ng-if="!$last">
                                                <td ng-if="($index+1) % 12 == 0" >{{ (($index+1) / 12)+1 }}</td><td ng-if="($index+1) % 12 != 0"></td>
                                                <td>{{$index+2}}</td>
                                                <td><input type="text" name="" min="0" ng-init="c[$index+1] = 0;j[$index+1] = 0" ng-model="c[$index+1]" value="0" style="text-align: center;width: 95px;" class="numbox" format="number" ></td>

                                                <td ng-style="increase((i[$index]*j[$index+1]/100)/12,$index+1) < 0 && {'color':'red'}">{{ increase((i[$index]*j[$index+1]/100)/12,$index+1)  | number:2 }}</td> 

                                                <td ng-style="capital(c[$index+1]-d[$index+1],$index+1) < 0 && {'color':'red'}">
                                                {{ capital(c[$index+1]-d[$index+1],$index+1)| number:2 }}</td>

                                                <td ng-style="capitalSum(f[$index]+e[$index+1],$index+1) < 0 && {'color':'red'} ">
                                                {{ capitalSum(f[$index]+e[$index+1],$index+1)| number:2 }}</td>

                                                <td ng-style="increaseSum(g[$index]+d[$index+1],$index+1) < 0 && {'color':'red'}">
                                                {{ increaseSum(g[$index]+d[$index+1],$index+1) | number:2 }}</td>

                                                <td ng-style="installmentSum(h[$index]+c[$index+1],$index+1) < 0 && {'color':'red'}">
                                                {{ installmentSum(h[$index]+c[$index+1],$index+1) | number:2 }}
                                                </td>

                                                <td ng-style="balance(i[$index]-e[$index+1],$index+1) < 0 && {'color':'red'}">
                                                {{ balance(i[$index]-e[$index+1],$index+1) | number:2 }}</td>
                                                
                                                <td><input type="text" style="width: 65%; text-align: center;" name="" ng-model="j[$index+1]" class="numbox" numbers-only></td>
                                            </tr>
                                        </tbody>
                                    </table> 
                                </div>

                            </div>

                        <label style=" margin-top: 30px;">( ติดต่อสอบถามเพิ่มเติมได้ที่ info@lh.co.th )</label><br><br>
                        <a href="javascript:history.back()" class="link-back">< ย้อนกลับ</a>

                    </div>
                </div>
            </div>
        </div>

                
        <dir id="img_print"></dir>




      <?php include('footer.php'); ?>