<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';
require_once 'include/help/hex2rgba.php';

if(empty($_GET['zone_id'])){
    $project_ids = getProjectIDByProductView(4);
}else{
 $project_ids = getProjectIDByProductViewZone(4,$_GET['zone_id']);
} 
//Helper::sortByPrice($project_ids,'asc');
Helper::sortByPriceReturnAssoc($project_ids,'asc');

$project_subs = [];
foreach($project_ids as $sorted_project_id){

    $project_sub    = getProjectSubZoneByProjectID($sorted_project_id['project_id']);
    $project_subs[] = $project_sub;
}

$page = 'major-cities';
$page_index = 7;

?>
<?php
include('header.php');
//elementMetaTitleDisTag($page,$page_index);
?>
<!-- JS -->
<link rel="stylesheet" href="<?= file_path('css/project-type.css') ?>" type="text/css">
<link rel="stylesheet" href="<?= file_path('css/video.css') ?>" type="text/css">

<script src="<?= file_path('js/project-type.js') ?>"></script>

<div class="page-banner page-scroll">
    <span id="scrollNext" class="i-scroll-next"></span>

    <div id="bannerSlide" class="owl-carousel">
        <?php
            // count filter
        $countProjectZone = 0;
        foreach ($project_subs as $i => $project_sub) {
            $project_id = $project_sub->project_id;
            if(empty($_GET['zone_id'])){
                $project = getProjectByID($project_id);
            }else{
                $project = getProjectByIDZone_id($project_id,$_GET['zone_id']);
            }
            if( empty($_GET['zone_id']) || (!empty($_GET['zone_id']) && $_GET['zone_id'] == $project->zone_id ) ) {
                $countProjectZone++;
            }
        }?>

        <?php
        renderBannerPageList(7);
        ?>

    </div>
</div>

<?php
renderHiddenLeadImage(7,'bannerSlide');
renderActivityResponsive();
?>
<?php  
$zones = getProjectViewZones(4);
if(!empty($_GET['zone_id'])){
    foreach($zones as $zone) {
        if($_GET['zone_id'] == $zone->zone_id){
           $zone_names = $zone->zone_name_th;
        }
    }
} 
if(!empty($_GET['zone_id'])){
    $SEO = getSEOUrl($actual_link);
    if($actual_link == @$SEO->url_page){
      $title_home = $SEO->h1;
    }else{
      $title_home = "Fully  projects in the provinces :  Zone ".$zone_names;
    } 
}else{
    $SEO = getSEOUrl($actual_link);
    if($actual_link == @$SEO->url_page){
     $title_home = $SEO->h1;
    }else{
     $title_home = "All projects in the provinces of Land and Houses";   
    }
     
}
?>

<div id="content" class="content project-type-page">
    <div class="container">
        <h1 style="cursor: pointer;text-transform: none;" onclick="location.href='<?= $router->generate('country-list') ?>'">
            <?=$title_home?>
        </h1>
        <div class="project-type-block">
            <h2>Recommended  quality Projects in the provinces <?= $countProjectZone ?> projects</h2>
            <div class="sort-container">
                <div class="sort-block">
                    <input type="hidden" name="sortSelect" value="">
                    <div class="sort" data-status="0">
                        <span>
                            <?php
                            $zones = getProjectViewZones(4);
                            if(!empty($_GET['zone_id'])){
                                foreach($zones as $zone) {
                                    if($_GET['zone_id'] == $zone->zone_id){
                                        echo $zone->zone_name_th;
                                    }
                                }
                            }else{
                                echo 'Select All interested locations';
                            }
                            ?>
                        </span> <i class="i-sort"></i>
                    </div>
                    <ul id="sortLists">
                        <li data-value="เลือกทำเลที่สนใจทั้งหมด" onclick="location.href='<?= $router->generate('country-list') ?>'">
                            Select All interested locations
                        </li>
                        <?php
                        foreach($zones as $zone) {?>
                        <li data-value="<?= $zone->zone_name_th ?>" onclick="location.href='<?= $router->generate('country-list-zone',['zone_name' => str_replace(' ','-',$zone->zone_name_th)]) ?>'">
                            <?= $zone->zone_name_th ?>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>

            <div class="view-block">
                <ul>
                    <li><a href="" class="i-view-list active"></a></li>
                    <li><a href="" class="i-view-grid"></a></li>
                    <li><a href="" class="i-view-grid-more"></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>

        </div>

        <input type="hidden" id="status_list" value="list">

        <div id="products" class="list-group">
            <?php
            foreach ($project_subs as $i => $project_sub) {
                $project_id = $project_sub->project_id;
                $product_arr= Helper::getArrayProductOfProject($project_id);

                $project = getProjectByID($project_id);if(empty($_GET['zone_id'])){
                    $project = getProjectByID($project_id);
                }else{
                    $project = getProjectByIDZone_id($project_id,$_GET['zone_id']);
                }
                $banner  = getBannerByProjectID($project_id);
                $brand   = getBrandByID($project->brand_id);
                $price   = getProjectPrice($project_id);

                $logo           = getProjectMaps($project_id);
                $project_concept = getProjectConcept($project_id);
                $conceptOnly    = getProjectConceptOnly($project_id);

                        // filter by zone
                if( empty($_GET['zone_id']) || (!empty($_GET['zone_id']) && $_GET['zone_id'] == $project->zone_id ) ){
                    ?>
                    <div class="projecttype-list">
                        <div class="row veralign-middle">
                            <div class="col-md-8">
                                <?php
                                if($project->project_status == 'NP'){
                                    ?>
                                    <div class="tagbox2">
                                        <div class="tag2">
                                            New<br>Project
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>

                                <?php
                                if(in_array(1,$product_arr) || in_array(2,$product_arr) || !in_array(3,$product_arr)){
                                    $url_see_more = isLadawan($project->project_id) ?
                                    $router->generate('ladawan-detail',[
                                        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                        'product_id'    =>  1
                                    ]) :
                                    $router->generate('single-home-detail',[
                                        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                        'product_id'    =>  1
                                    ]);
                                }
                                else{
                                    $url_see_more = isLadawan($project->project_id) ?
                                    $router->generate('ladawan-detail',[
                                        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                        'product_id'    =>  3
                                    ]) :
                                    $router->generate('condominium-detail',[
                                        'project_name'  =>  str_replace(' ','-',$project->project_name_th),
                                        'lang'          =>  'th'
                                    ]);
                                }
                                ?>
                                <?php
                                if($banner != false && ( $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'image' || $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'banner' ) ){
                                    ?>
                                    <a href="<?= $url_see_more ?>" >
                                        <img src="<?= backend_url('base',$banner->LEAD_IMAGE_PROJECT_FILE_NAME)?>"
                                        alt="<?= $banner->LEAD_IMAGE_PROJECT_FILE_SEO ?>">
                                    </a>
                                    <?php
                                }elseif($banner != false && $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'youtube'){
                                    $lead_img = backend_url('base', $banner->banner_img_thum);
                                    $lead_youtube_url = str_replace('watch?v=', 'embed/', $banner->LEAD_IMAGE_PROJECT_FILE_NAME);
                                    ?>

                                    <a href="#" data-featherlight="#lbVdo<?= $i ?>">
                                        <span class="i-view-vdo"></span>
                                        <img src="<?= $lead_img ?>"
                                        alt="<?= $banner->LEAD_IMAGE_PROJECT_FILE_SEO ?>" class="hm-highl">
                                    </a>
                                    <iframe class="lightbox" src="<?= $lead_youtube_url . "?autoplay=0" ?>"
                                        width="1000"
                                        height="560" id="lbVdo<?= $i ?>" style="border:none;" webkitallowfullscreen
                                        mozallowfullscreen allowfullscreen></iframe>

                                        <?php
                                    } elseif ($banner != false && $banner->LEAD_IMAGE_PROJECT_FILE_TYPE == 'vdo') {
                                        ?>
                                        <a id="ban_vdo<?= $i ?>">
                                            <span class="i-view-vdo"></span>
                                            <img src="<?= backend_url('base', $banner->banner_img_thum) ?>"
                                            alt="<?= $banner->LEAD_IMAGE_PROJECT_FILE_SEO ?>" class="hm-highl">
                                        </a>
                                        <script>
                                            $(document).ready(function () {
                                                $('#ban_vdo<?= $i ?>').click(function () {
                                                    $('#lbVdo<?= $i ?>').show();
                                                    $.featherlight($('#lbVdo<?= $i ?>'),{});
                                                    $('.featherlight-content #video_player<?= $i ?>')[0].play();
                                                    $('#lbVdo<?= $i ?>').hide();
                                                });
                                            })
                                        </script>
                                        <div id="lbVdo<?= $i ?>" style="position:relative;width: 100%;display: none;">
                                            <video id='video_player<?= $i ?>' preload='none' controls>
                                                <source src="<?= backend_url('base',$banner->LEAD_IMAGE_PROJECT_FILE_NAME) ?>" type="video/mp4">
                                                </video>
                                            </div>

                                            <?php } ?>

                                        </div>
                                        <div class="col-md-4">
                                            <div class="projecttype-descrp">
                                                <?php if(!empty($logo->project_logo)) {
                                                    ?>
                                                    <img src="<?= is_object($logo) ? backend_url('base', $logo->project_logo) : '' ?>" title="<?= $logo->project_logo_seo ?>" alt="<?= $logo->project_logo_seo ?>" class="lo-150">
                                                    <?php
                                                }?>
                                                <h1><?php echo $project->project_name_th?></h1>
                                                <p class="concept<?= $i ?> concept-in-list">
                                                 <?=!empty($project_concept->concept_th) ? str_replace(array("\n\r", "\n","n/g"), '<br/>', $project_concept->concept_th) : ''?>
                                                <!-- <script type="text/javascript">
                                                    $('.concept<?= $i ?>').html(`<?php //!empty($project_concept->concept_th) ? $project_concept->concept_th : ''?>`.replace(/\r?\n/g, '<br/>'));
                                                </script> -->
                                            </p>
                                            <?php if(!empty($project_price->project_price)) { ?>
                                            <br>
                                            <p>
                                                <strong>

                                                    <?php
                                                    $project_price   = getProjectPrice($project_id);
                                                    $priceCondo = Helper::getProjectPrice($project_price->project_price,$project_price->price_mode)
                                                    ?>
                                                    <?php 
                                                    if($project_price->price_mode == '1'){
                                                        $mode_price = 'Starting price from ';
                                                    }else{
                                                        $mode_price = 'Pricing from';
                                                    }
                                                    ?>
                                                    <?=$mode_price;?>
                                                    <?= $priceCondo.' ' ?>
                                                    <?= is_object($project_price) ? Helper::getPriceModeName($project_price->price_mode,$project_price->project_price) : 'MB' ?>

                                                </strong>
                                            </p>
                                           <?php }?>

                                            
                                            <a href="<?= $url_see_more ?>" class="btn btn-seemore">
                                                See more
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>

                </div>
            </div>

        </div>
    </div>


    <?php include('footer.php'); ?>