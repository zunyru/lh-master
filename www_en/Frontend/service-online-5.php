<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>

<?php include('header.php'); ?>
    <!-- CSS -->
    <link rel="stylesheet" href="css/service.css" type="text/css">
    <!-- JS -->
    <script src="js/service.js"></script>

    <div id="content" class="content service-page">
        <div class="container">
            <div class="row">
                <div class="col-md-11 col-md-offset-1">
                    <h1 class="heading-title">DIY</h1>
                    <p class="txt-grey">Home DIY by "Land and Houses"</p>

                    <div id="tpl1Block2List" class="tpl1-block-2">
                        <div class="row">
                            <div id="block2List"  class="col-slide">
                                <div class="item">
                                    <div class="hm-tip">
                                        <a href="">
                                            <div class="review-img">
                                                <a href="#" data-featherlight="#lbVdo2">
                                                    <span class="i-view-vdo"></span>
                                                    <img src="https://img.youtube.com/vi/GJkPSkZ80bo/0.jpg" alt="">
                                                </a>
                                                <iframe class="lightbox" src="https://www.youtube.com/embed/GJkPSkZ80bo" width="1000" height="560" id="lbVdo2" style="border:none;" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                            </div>
                                            <p class="">การ maintenance ประตูรั้ว</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="hm-tip">
                                        <a href="">
                                            <div class="review-img">
                                                <a href="#" data-featherlight="#lbVdo2">
                                                    <span class="i-view-vdo"></span>
                                                    <img src="https://img.youtube.com/vi/GJkPSkZ80bo/0.jpg" alt="">
                                                </a>
                                                <iframe class="lightbox" src="https://www.youtube.com/embed/GJkPSkZ80bo" width="1000" height="560" id="lbVdo2" style="border:none;" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                            </div>
                                            <p class="">การดูแลผลิตภัณฑ์โครเมี่ยม</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="hm-tip">
                                        <a href="">
                                            <div class="review-img">
                                                <a href="#" data-featherlight="#lbVdo2">
                                                    <span class="i-view-vdo"></span>
                                                    <img src="https://img.youtube.com/vi/GJkPSkZ80bo/0.jpg" alt="">
                                                </a>
                                                <iframe class="lightbox" src="https://www.youtube.com/embed/GJkPSkZ80bo" width="1000" height="560" id="lbVdo2" style="border:none;" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                            </div>
                                            <p class="">การตรวจสอบปลวก</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="hm-tip">
                                        <a href="">
                                            <div class="review-img">
                                                <a href="#" data-featherlight="#lbVdo2">
                                                    <span class="i-view-vdo"></span>
                                                    <img src="https://img.youtube.com/vi/GJkPSkZ80bo/0.jpg" alt="">
                                                </a>
                                                <iframe class="lightbox" src="https://www.youtube.com/embed/GJkPSkZ80bo" width="1000" height="560" id="lbVdo2" style="border:none;" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                            </div>
                                            <p class="">การตรวจสอบปลวก</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>


        </div>
    </div>


<?php include('footer.php'); ?>
