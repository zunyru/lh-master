<?php

require_once 'include/help/begin.php';
require_once 'include/help/query_function.php';

?>
<?php include('header.php'); ?>
<!-- CSS -->
<link rel="stylesheet" href="css/report-fixed.css" type="text/css">
<!-- JS -->
<script src="js/report-fixed.js"></script>
<script src="js/banner-md.js"></script>

<div id="content" class="content">
    <div class="container">

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div>
                    <h1 class="heading-title">แจ้งซ่อมออนไลน์</h1>
                    <div class="complain-form-block">
                        <p class="title">ข้อมูลลูกค้า</p>

                        <div class="form">
                            <div class="form-group form-details">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="house">
                                            ชื่อ-สกุล ลูกค้า :
                                        </label>
                                        <p>นาง วีระอนงค์ ชวลิตธำรง</p>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="house">
                                            email :
                                        </label>
                                        <p>-</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="house">
                                            โทรศัพท์ติดต่อ  :
                                        </label>
                                        <p>087-9276955</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="complain-form-block">
                        <p class="title">รายละเอียดบ้าน</p>

                        <div class="form">
                            <div class="form-group form-details">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="house">
                                            หลังที่ :
                                        </label>
                                        <p>1</p>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="house">
                                            โครงการ :
                                        </label>
                                        <p>นันทวัน-เชียงใหม่</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="house">
                                            บ้านเลขที่ :
                                        </label>
                                        <p>176/33</p>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="house">
                                            แปลง :
                                        </label>
                                        <p>06A03</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="house">
                                            แบบ้าน :
                                        </label>
                                        <p>คาลลา ลิลลี</p>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="house">
                                            วันที่โอน :
                                        </label>
                                        <p>30/12/2553</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="house" class="label-lng">
                                            สถานะของงานรับประกัน 1 ปี :
                                        </label>
                                        <p>หมดประกัน VIEW</p>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="house">
                                            พื้นที่บ้าน :
                                        </label>
                                        <p>109.90 ตร.ว.</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="house" class="label-lng">
                                            สถานะของงานรับประกัน 5 ปี :
                                        </label>
                                        <p>หมดประกัน</p>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="house" class="label-lng">
                                            อัตราจัดเก็บค่าบริการสาธารณะ :
                                        </label>
                                        <p>บาท /ต่อหลัง สิ้นสุด ณ วันที่</p>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-offset-5 col-md-3">
                                        <a href="" class="btn btn-submit see-detail">ดูรายละเอียด</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <table class="project-search-list" width="100%">
                        <tbody>
                        <tr>
                            <td>โครงการ</td>
                            <td>บ้านเลขที่</td>
                            <td>วันที่แจ้งซ่อม</td>
                            <td>วันที่นัดตรวจ</td>
                            <td>สถานะ</td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="complain-form-block">
                        <div class="row">
                            <div class="col-md-offset-6 col-md-6">
                                <label for="house">
                                    ไม่มีข้อมูล
                                </label>
                            </div>
                        </div>
                    </div>
                    <a href="<?php url('complain-step1.php') ?>" class="step-back">< Back</a>
                </div>
            </div>

        </div>

    </div>
</div>


<?php include('footer.php'); ?>
