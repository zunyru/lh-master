<?php
header('Content-type: text/html; charset=utf-8');

class Function_  {

		//Date From Sort
		//'2015/07/18-2015/07/20' preg_replace
	public function Date($time='')
	{
			if($time <> '' && $time <> 'All'){
				$date = preg_replace('/[^a-zA-Z0-9ก-๙เแ\_\&\!\.\?\{}\@\#\$\%\^\*\=\<>\()\|\:\;\'\t\n\/]/i',' ',$time);
				$day = explode (' ',$date);
				$day_arr = array_filter($day);

				return $day_arr;
		}else{
			return '@';
		}
	}
	//Date Format
	public function Date_Format($format='',$date='')
	{
		return date($format,strtotime(str_replace('/', '-',$date)));
	}
	//encode('P@ssw0rd','MFEC');
	public function encode($string,$key='MFEC') {
				$key = md5($key);
				$strLen = strlen($string);
				$keyLen = strlen($key);
				$j = 0;$hash='';
				for ($i = 0; $i < $strLen; $i++) {
						$ordStr = ord(substr($string,$i,1));
						if ($j == $keyLen) { $j = 0; }
						$ordKey = ord(substr($key,$j,1));
						$j++;
						$hash .= strrev(base_convert(dechex($ordStr + $ordKey),16,36));
				}
				return $hash;
	}
	//decode('x473o4z516u2q4c4','MFEC');
	public function decode($string,$key='MFEC') {
				$key = md5($key);
				$strLen = strlen($string);
				$keyLen = strlen($key);
				$j = 0;$hash='';
				for ($i = 0; $i < $strLen; $i+=2) {
						$ordStr = hexdec(base_convert(strrev(substr($string,$i,2)),36,16));
						if ($j == $keyLen) { $j = 0; }
						$ordKey = ord(substr($key,$j,1));
						$j++;
						$hash .= chr($ordStr - $ordKey);
				}
				return $hash;
	}
	//CURL
	public function CURL($url='')
	{
				$curl = curl_init();
				curl_setopt($curl, CURLOPT_PROXY, self::_proxy_());
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
			//curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			//curl_setopt($curl, CURLOPT_FAILONERROR, true);
			  curl_setopt($curl, CURLOPT_HEADER, TRUE);
				curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
				curl_setopt($curl, CURLOPT_URL, $url);
				curl_setopt($curl, CURLOPT_REFERER, $url);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
				$str = curl_exec($curl);
			  curl_close($curl);

				return $str;
	}
	/*
		$type=$_FILES['filUpload']['type'];
		$name= $_FILES['filUpload']['name'];
		$size=$_FILES['filUpload']['size'];
		$tmp=$_FILES['filUpload']['tmp_name'];
		$name_after = Image name at been uploading;
		$Imagsize = 200;
		$_path = '../image/image_profile/';
		*/
	function _filUpload($type,$name,$size,$tmp,$name_after,$Imagsize=200,$_path=''){

				$name_image = date('Y_m_d_H_i');
				$name_image .= md5('image');
				$upload_after = $name_after; // Image name at been uploading
				$path = $_path;

				$file_exts = array('jpg', 'png', 'gif', 'bmp','jpeg','PNG','JPG','JPEG','GIF','BMP');
				$upload_exts = end(explode('.',$name));


					if ((($type == 'image/gif')
					|| ($type == 'image/jpeg')
					|| ($type == 'image/bmp')
					|| ($type == 'image/png')
					|| ($type == 'image/pjpeg'))
					&& ($size < 5000000)
					&& in_array($upload_exts,$file_exts))
					{
						$name_image .= '.'.$upload_exts;

						if ($_FILES['filUpload']['error'] > 0)
						{
								return 'error';
						}else{
								// Enter your path to upload file here
								if(file_exists($path.$upload_after))
								{
										unlink ($path.$upload_after); //Delete Image name at been uploading
								}

										$source = $tmp;
										$destination = $path.$name_image;
										$maxsize = $Imagsize;

										$size = getimagesize($source);
										$width_orig = $size[0];
										$height_orig = $size[1];
										unset($size);
										$height = $maxsize+1;
										$width = $maxsize;
										while($height > $maxsize){
											$height = round($width*$height_orig/$width_orig);
											$width = ($height > $maxsize)?--$width:$width;
										}
										unset($width_orig,$height_orig,$maxsize);
										$images_orig    = imagecreatefromstring( file_get_contents($source) );
										$photoX         = imagesx($images_orig);
										$photoY         = imagesy($images_orig);
										$images_fin     = imagecreatetruecolor($width,$height);
										imagesavealpha($images_fin,true);
										$trans_colour   = imagecolorallocatealpha($images_fin,0,0,0,127);
										imagefill($images_fin,0,0,$trans_colour);
										unset($trans_colour);
										ImageCopyResampled($images_fin,$images_orig,0,0,0,0,$width+1,$height+1,$photoX,$photoY);
										unset($photoX,$photoY,$width,$height);
										imagepng($images_fin,$destination);
										unset($destination);
										ImageDestroy($images_orig);
										ImageDestroy($images_fin);

										return $name_image;
						}
					}
		}
		//DateDiff('2008-08-01','2008-08-31')
	public function DateDiff($strDate1,$strDate2)
	{
			return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
	}
	//TimeDiff('00:00','19:00')
	public function TimeDiff($strTime1,$strTime2)
	{
			return (strtotime($strTime2) - strtotime($strTime1))/  ( 60 * 60 ); // 1 Hour =  60*60
	}
	//DateTimeDiff('2008-08-01 00:00','2008-08-01 19:00')
	public function DateTimeDiff($strDateTime1,$strDateTime2)
	{
			return (strtotime($strDateTime2) - strtotime($strDateTime1))/  ( 60 * 60 ); // 1 Hour =  60*60
	}
	//randomPassword();
	public function randomPassword() {
		$alphabet = 'abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789';
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
	}
	//
}

?>
